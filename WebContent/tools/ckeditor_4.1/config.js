/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	//config.uiColor = '#cccccc';
	config.height = 800; 
	config.width = 800;
	//config.uiColor = '#FF4444'; 
	//config.contentsCss = '/css/cms.richtextck.css';
	//config.contentsCss = '/css/cms.richtextck.css';
	//config.contentsCss = '/cms.richtext_ck.css';
	//config.contentsCss = ['/css/cms.richtext_ck.css', '/css/cms.richtextck.css'];
	
	//config.filebrowserImageBrowseUrl = '/admin/imagemgr/index.jsp?txt=ckeditor&br=ckeditor';
	//config.filebrowserDocBrowseUrl = '/admin/docmgr/index.jsp?txt=ckeditor&br=ckeditor';	
};
