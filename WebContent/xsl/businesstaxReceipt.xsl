<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	version="1.0"> 
	
<xsl:template match="root">
<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<fo:layout-master-set>
	    <fo:simple-page-master 
	             master-name="all" 
	             page-height="11.5in" 
	             page-width="8.5in" 
	             margin-top="0.4in" 
	             margin-bottom="0.3in" 
		     margin-left="0.5in" 
		     margin-right="0.5in">
	        <fo:region-body margin-top="3in" margin-bottom="1in"/> 
		<fo:region-before extent="3in"/>
		<fo:region-after extent="1in"/>
	   </fo:simple-page-master>
	</fo:layout-master-set>
	<fo:page-sequence master-reference="all" >
<!-- Header start   -->         
    <fo:static-content flow-name="xsl-region-before">
	
	<fo:block text-align="start" line-height="1em + 2pt">
	<fo:table table-layout="fixed" >
				<fo:table-column column-width="4cm"/>
				<fo:table-column column-width="10cm"/>
				<fo:table-column column-width="5cm"/>
				<fo:table-body>
				   
				   <fo:table-row space-after.optimum="5pt">
				       <fo:table-cell number-columns-spanned="3">
				           <fo:table table-layout="fixed" >
					      <fo:table-column column-width="6cm"/>
					      <fo:table-column column-width="6cm"/>
					      <fo:table-column column-width="7cm"/>
                                              <fo:table-body>
				                  <fo:table-row >
				                      <fo:table-cell >
				                          <fo:block ></fo:block>
      			                              </fo:table-cell>
 				 	              <fo:table-cell>
				 	                 <fo:block background-color="#C9C299" text-align="center" font-family="sans-serif" font-size="16pt"  font-weight="bold">Business Tax Receipt</fo:block>
      			                              </fo:table-cell>
				 	              <fo:table-cell >
				 	                 <fo:block ></fo:block>
      			                              </fo:table-cell>
      			                         </fo:table-row>
      			                      </fo:table-body>
      			                   </fo:table>  
      			               </fo:table-cell>    
                                    </fo:table-row> 
				   
				    <fo:table-row space-after.optimum="5pt">
				       <fo:table-cell text-align="center" number-columns-spanned="3">
				           <fo:block ><xsl:text> &#160; </xsl:text></fo:block>
                       </fo:table-cell>    
                    </fo:table-row> 
				   
				   <fo:table-row>
				      <fo:table-cell>
				         <fo:block space-after="2.5mm" margin-left="2mm">
				            <fo:external-graphic src="/f02/EPALSAPPS/XSL_IMAGES/CITYLOGO.jpg" height="3.25cm" width="3.0cm"/>
				         </fo:block>
      			              </fo:table-cell>
	 		              <fo:table-cell>
				         <fo:block text-align="start" font-family="sans-serif" font-size="14pt" wrap-option="no-wrap"  font-weight="bold" >
				         <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				         DEPARTMENT OF COMMUNITY DEVELOPMENT
				         <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				         <xsl:value-of select="data/main/department"/> 
				            <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				          275 East Olive Ave. 
				           <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				          Burbank, Calif. 90210 
				        </fo:block>
				      </fo:table-cell>				      
				      
				      <fo:table-cell>
			                  <fo:block text-align="start" font-family="sans-serif" font-size="10pt" wrap-option="no-wrap">
				              <fo:block> <xsl:text>&#xA;</xsl:text> </fo:block>
				              <xsl:text> &#160; </xsl:text>
				              <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				              <xsl:text> &#160; </xsl:text>
				              <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				              <xsl:text> &#160; </xsl:text>
				              <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				              Permit No<xsl:text> &#160; </xsl:text><xsl:text> &#160; </xsl:text><xsl:text> &#160; </xsl:text>: <fo:inline font-weight="bold"><xsl:value-of select="data/main/actnbr"/> </fo:inline> 
				              <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				              Type : <xsl:value-of select="data/main/acttype"/> 
				              <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				              <xsl:value-of select="data/main/rundate"/> 
				              <xsl:text> &#160; </xsl:text>
				              Page<xsl:text> &#160; </xsl:text><fo:page-number/> of <fo:page-number-citation ref-id="last-page"/>
				              <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				         </fo:block>
				      </fo:table-cell>				      
				  </fo:table-row>

				  
				  <fo:table-row>
				      <fo:table-cell number-columns-spanned="3">
				          <fo:table table-layout="fixed" >
					      <fo:table-column column-width="2.5cm"/>
					      <fo:table-column column-width="11.5cm"/>
					      <fo:table-column column-width="5cm"/>
						  <fo:table-body>
						      <fo:table-row>
						          <fo:table-cell>
						             <fo:block font-family="sans-serif" font-size="9pt"> Job Address: </fo:block>
				                          </fo:table-cell>	
				                         <fo:table-cell>
				                             <fo:block font-family="sans-serif" font-size="9pt" wrap-option="no-wrap" >
				                                  <xsl:value-of select="data/main/address"/>
				                             </fo:block>
				                         </fo:table-cell>		      
				                         <fo:table-cell text-align="start">
				                            <fo:block font-family="sans-serif" font-size="9pt" >
					   	               PRE<xsl:text> &#160; </xsl:text><xsl:text> &#160; </xsl:text>: <xsl:value-of select="data/main/planreviewer"/>
						            </fo:block>
						         </fo:table-cell>
						       </fo:table-row>  
						       <fo:table-row>
						          <fo:table-cell>
						             <fo:block font-family="sans-serif" font-size="9pt"> Valuation: </fo:block>
				                          </fo:table-cell>	
				                         <fo:table-cell>
				                             <fo:block font-family="sans-serif" font-size="9pt" wrap-option="no-wrap" >
				                                  <xsl:value-of select="format-number(data/main/valuation, '$#,##0.00 ')"/>
				                             </fo:block>
				                         </fo:table-cell>		      
				                         <fo:table-cell text-align="start">
				                            <fo:block font-family="sans-serif" font-size="9pt" >
					   	               Entered By <xsl:text> &#160; </xsl:text><xsl:text> &#160; </xsl:text>: <xsl:value-of select="data/main/enteredby"/>
						            </fo:block>
						         </fo:table-cell>
						       </fo:table-row>  
						       <fo:table-row>
						          <fo:table-cell >
						             <fo:block font-family="sans-serif" font-size="9pt"> Job Description  : </fo:block>
				                          </fo:table-cell>	
				                         <fo:table-cell >
				                             <fo:block font-family="sans-serif" font-size="9pt" >
				                                  <xsl:value-of select="data/main/actdesc"/>
				                             </fo:block>
				                         </fo:table-cell>		      
						         <fo:table-cell >
						   	     <fo:block font-family="sans-serif" font-size="9pt" > </fo:block>
						   	 </fo:table-cell>		      
						   
						   </fo:table-row>  
                                                    </fo:table-body>
					     </fo:table>
				         </fo:table-cell>	
        		          </fo:table-row>	

		      		</fo:table-body>
		      	    </fo:table>
		      	</fo:block>    
 
   </fo:static-content> 
<!-- Header end -->    

<!-- Footer Begin -->
    <fo:static-content flow-name="xsl-region-after">
			<fo:block text-align="start" line-height="1em + 2pt">
   			   <fo:leader leader-alignment="reference-area" leader-pattern="rule" />
			   <fo:table table-layout="fixed" >
				   <fo:table-column column-width="2cm"/>
				   <fo:table-column column-width="6cm"/>
				   <fo:table-column column-width="3cm"/>
				   <fo:table-column column-width="2.5cm"/>
				   <fo:table-column column-width="2.5cm"/>
				   <fo:table-column column-width="2.5cm"/>
				   <fo:table-body>
                       <fo:table-row>
        	               <fo:table-cell number-columns-spanned="3">
		                       <fo:block font-family="sans-serif" font-size="9pt"></fo:block>
		                   </fo:table-cell>			        
						   <fo:table-cell>
						       <fo:block text-align="right" font-family="sans-serif" font-size="9pt"  font-weight="bold">Bus.Tax</fo:block>
					       </fo:table-cell>
						   <fo:table-cell>
					           <fo:block text-align="right" font-family="sans-serif" font-size="9pt" font-weight="bold">Tax Paid</fo:block>
						   </fo:table-cell>
						   <fo:table-cell>
							   <fo:block text-align="right" font-family="sans-serif" font-size="9pt" font-weight="bold">Balance Due</fo:block>
  					       </fo:table-cell>
          	           </fo:table-row>
			           <fo:table-row >
        	               <fo:table-cell number-columns-spanned="3">
		                       <fo:block text-align="center" font-family="sans-serif" font-size="9pt" font-weight="bold">Total Business Tax paid for this Permit :</fo:block>
		                   </fo:table-cell>			        
				            <fo:table-cell>
				              <fo:block text-align="right" font-family="sans-serif" font-size="9pt">
				              	   <xsl:value-of select="data/contractors/taxsum"/>   
					      </fo:block>
				            </fo:table-cell>	
				            <fo:table-cell>
				              <fo:block text-align="right" font-family="sans-serif" font-size="9pt">
				              	  <xsl:value-of select="data/contractors/paidsum"/>   
					      </fo:block>
				            </fo:table-cell>	
				            <fo:table-cell>
				              <fo:block text-align="right" font-family="sans-serif" font-size="9pt">
				              	     <xsl:value-of select="data/contractors/duesum"/>   
					      </fo:block>
				            </fo:table-cell>	
				        </fo:table-row>  


		           </fo:table-body>
		     </fo:table>
		 </fo:block>    
    </fo:static-content> 
<!-- Footer End -->	


    <fo:flow flow-name="xsl-region-body">
		<xsl:apply-templates select="data"/>
		<!-- To get number of pages -->
		<fo:block id="last-page"/>
		
    </fo:flow>
    </fo:page-sequence>
</fo:root>
</xsl:template>

<xsl:template match="data">
	<fo:block text-align="start" font-size="12pt" font-family="sans-serif">
            <fo:leader leader-alignment="reference-area" leader-pattern="rule" /> 
	    <xsl:for-each select="contractors/contractor"> 
	        <fo:table table-layout="fixed" >
	           <fo:table-column column-width="18.5cm"/>
                      <fo:table-body>
                         <fo:table-row keep-together="always">
			   <fo:table-cell>
	                <fo:block text-align="start" white-space-collapse="false">
		 <fo:table table-layout="fixed" >
		    <fo:table-column column-width="2cm"/>
		    <fo:table-column column-width="6cm"/>
		    <fo:table-column column-width="3cm"/>
		    <fo:table-column column-width="2.5cm"/>
		    <fo:table-column column-width="2.5cm"/>
		    <fo:table-column column-width="2.5cm"/>
		    <fo:table-header  background-color="#C9C299" space-after.optimum="5pt">
		       <fo:table-row>
			   <fo:table-cell>
				  <fo:block font-family="sans-serif" font-size="9pt" font-weight="bold">License No</fo:block>
			   </fo:table-cell>
			   <fo:table-cell>
				  <fo:block font-family="sans-serif" font-size="9pt" font-weight="bold">Contractor Name</fo:block>
			   </fo:table-cell>
			   <fo:table-cell>
			      <fo:block text-align="right" font-family="sans-serif" font-size="9pt" font-weight="bold">Valuation</fo:block>
			   </fo:table-cell>		      
			   <fo:table-cell>
				  <fo:block text-align="right" font-family="sans-serif" font-size="9pt"  font-weight="bold">Bus.Tax</fo:block>
		           </fo:table-cell>
			   <fo:table-cell>
		          <fo:block text-align="right" font-family="sans-serif" font-size="9pt" font-weight="bold">Tax Paid</fo:block>
			   </fo:table-cell>
			   <fo:table-cell>
				  <fo:block text-align="right" font-family="sans-serif" font-size="9pt" font-weight="bold">Balance Due</fo:block>
			   </fo:table-cell>
		       </fo:table-row>
		    </fo:table-header>
		    <fo:table-body>
		
	               <fo:table-row >
	                  <fo:table-cell>
	                      <fo:block text-align="center" font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="licno"/>
		              </fo:block>
 		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="name"/>
		              </fo:block>
		            </fo:table-cell>
  		            <fo:table-cell>
		              <fo:block text-align="right" font-family="sans-serif" font-size="9pt">
		              	<xsl:if test="string-length(valuation)>0" >
			              <xsl:value-of select="format-number(valuation, '$#,##0.00 ')"/>   
			        </xsl:if>      
		              </fo:block>
		            </fo:table-cell>	
		            <fo:table-cell>
		              <fo:block text-align="right" font-family="sans-serif" font-size="9pt">
		              	<xsl:if test="string-length(tax)>0" >
			              <xsl:value-of select="format-number(tax, '$#,##0.00 ')"/>   
			        </xsl:if>      
		              </fo:block>
		            </fo:table-cell>	
		            <fo:table-cell>
		              <fo:block text-align="right" font-family="sans-serif" font-size="9pt">
		              	<xsl:if test="string-length(paid)>0" >
			              <xsl:value-of select="format-number(paid, '$#,##0.00 ')"/>   
			        </xsl:if>      
		              </fo:block>
		            </fo:table-cell>	
		            <fo:table-cell>
		              <fo:block text-align="right" font-family="sans-serif" font-size="9pt">
		              	<xsl:if test="string-length(due)>0" >
			              <xsl:value-of select="format-number(due, '$#,##0.00 ')"/>   
			        </xsl:if>      
		              </fo:block>
		            </fo:table-cell>	
		        </fo:table-row>  
    
		        <fo:table-row >
        	            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt"></fo:block>
		            </fo:table-cell>			        
		            <fo:table-cell number-columns-spanned="5">
		              <fo:block font-family="sans-serif" font-size="9pt"   space-after.optimum="5pt">
			              <xsl:value-of select="workdesc"/>
		              </fo:block>
		            </fo:table-cell>		            
		        </fo:table-row>
		
		         <fo:table-row >
		            <fo:table-cell number-columns-spanned="3">
	                       <fo:block text-align="start" white-space-collapse="false">
		                  <fo:table table-layout="fixed" >
		                     <fo:table-column column-width="2cm"/>
		                     <fo:table-column column-width="2.5cm"/>
		                     <fo:table-column column-width="2cm"/>
		                     <fo:table-column column-width="2cm"/>
		                     <fo:table-column column-width="2.5cm"/>
		                      <fo:table-header  space-after.optimum="5pt">
		                         <fo:table-row >
			                    <fo:table-cell >
				               <fo:block font-family="sans-serif" font-size="9pt" ></fo:block>
			                    </fo:table-cell>
			                    <fo:table-cell background-color="#C9C299">
				               <fo:block font-family="sans-serif" font-size="9pt" font-weight="bold">Transaction No</fo:block>
			                    </fo:table-cell>
			                    <fo:table-cell background-color="#C9C299">
			                        <fo:block font-family="sans-serif" font-size="9pt" font-weight="bold">Date</fo:block>
			                    </fo:table-cell>		      
			                    <fo:table-cell background-color="#C9C299">
				               <fo:block font-family="sans-serif" font-size="9pt"  font-weight="bold">Method</fo:block>
		                            </fo:table-cell>
			                    <fo:table-cell background-color="#C9C299">
		                                <fo:block text-align="right" font-family="sans-serif" font-size="9pt" font-weight="bold">Amount</fo:block>
			                    </fo:table-cell>
		                       </fo:table-row>
		                   </fo:table-header>
	                           <fo:table-body>
	                              <xsl:for-each select="transactions/transaction">
	                                   <fo:table-row >
					      <fo:table-cell>
					         <fo:block font-family="sans-serif" font-size="9pt"></fo:block>
 		                              </fo:table-cell>
		                              <fo:table-cell>
		                                 <fo:block text-align="center" font-family="sans-serif" font-size="9pt">
			                             <xsl:value-of select="id"/>
		                                 </fo:block>
		                              </fo:table-cell>
  		                              <fo:table-cell>
		                                 <fo:block font-family="sans-serif" font-size="9pt">
		              	                         <xsl:value-of select="date"/>   
			                         </fo:block>
		                              </fo:table-cell>	
		                              <fo:table-cell>
		                                 <fo:block font-family="sans-serif" font-size="9pt">
		              	                     <xsl:value-of select="method"/>   
			                         </fo:block>
		                              </fo:table-cell>	
		                              <fo:table-cell>
		                                 <fo:block text-align="right" font-family="sans-serif" font-size="9pt">
		              	 		     <xsl:value-of select="amount"/>   
			                         </fo:block>
		                              </fo:table-cell>	
      		                          </fo:table-row>  
	                              </xsl:for-each>
                                   </fo:table-body>
                                 </fo:table>  
                             </fo:block>  
		            </fo:table-cell>		            
		        </fo:table-row>

		    </fo:table-body>	    
		</fo:table>				
	</fo:block>
		            </fo:table-cell>		            
		        </fo:table-row>

		    </fo:table-body>	    
		</fo:table>				


  </xsl:for-each>
 </fo:block>	
</xsl:template>

<xsl:template match="main/address">
	<fo:block space-before.optimum="5pt" text-align="start" >
	    <fo:table table-layout="fixed" >
		 <fo:table-column column-width="4cm"/>
		 <fo:table-column column-width="15cm"/>
		 <fo:table-body>
		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Base Address </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="."/> 
		              </fo:block>
		            </fo:table-cell>
		        </fo:table-row>
		 </fo:table-body>
	    </fo:table>
	</fo:block>		          
</xsl:template>

<xsl:template match="main/valuation">
	<fo:block space-before.optimum="5pt" text-align="start" >
	    <fo:table table-layout="fixed" >
		 <fo:table-column column-width="4cm"/>
		 <fo:table-column column-width="15cm"/>
		 <fo:table-body>
		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Valuation </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
		              	<xsl:if test="string-length(.)>0" >
			                <xsl:value-of select="format-number(., '$#,##0.00 ')"/>
			        </xsl:if>        
		              </fo:block>
		            </fo:table-cell>
		        </fo:table-row>
		 </fo:table-body>
	    </fo:table>
	</fo:block>		          
</xsl:template>

<xsl:template match="main/actdesc">
	<fo:block space-before.optimum="5pt" text-align="start" >
	    <fo:table table-layout="fixed" >
		 <fo:table-column column-width="4cm"/>
		 <fo:table-column column-width="15cm"/>
		 <fo:table-body>
		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Job Description </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="."/> 
		              </fo:block>
		            </fo:table-cell>
		        </fo:table-row>
		 </fo:table-body>
	    </fo:table>
	</fo:block>		          
</xsl:template>

</xsl:stylesheet>


