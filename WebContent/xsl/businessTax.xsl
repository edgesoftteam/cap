<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" version="1.0">
	<xsl:template match="root">
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
			<fo:layout-master-set>
				<fo:simple-page-master master-name="all" page-height="11.0in" page-width="8.5in" margin-top="0.3in" margin-bottom="0.3in" margin-left="0.5in" margin-right="0.5in">
					<fo:region-body margin-top="1.875in" margin-bottom="1.5in"/>
					<fo:region-before extent="2.2in"/>
					<fo:region-after extent="1.6in"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-reference="all">
				<fo:static-content flow-name="xsl-region-before"/>
				<fo:flow flow-name="xsl-region-body">
					<xsl:apply-templates select="data"/>
					<fo:block id="last-page"/>
					<fo:block-container height="8cm" width="20cm" top="14.8cm" left="0.55cm" position="absolute">
						<fo:block text-align="start">
							<fo:table table-layout="fixed">
								<fo:table-column column-width="1.5cm"/>
								<fo:table-column column-width="2cm"/>
								<fo:table-column column-width="12.2cm"/>
								<fo:table-column column-width="0.8cm"/>
								<fo:table-column column-width="2cm"/>
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell>
											<fo:block font-family="sans-serif" font-size="11pt" wrap-option="no-wrap" font-weight="bold">
										DATE <xsl:value-of select="data/businessTaxGroup/businessTax/ISSUED_DATE"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell>
								</fo:table-cell>
										<fo:table-cell>
								</fo:table-cell>
										<fo:table-cell>
											<fo:block font-family="sans-serif" font-size="11pt" wrap-option="no-wrap" font-weight="bold">
										BT - <xsl:value-of select="data/businessTaxGroup/businessTax/BUSINESS_ACCOUNT_NUMBER"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell>
								</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
						<fo:block text-align="start" space-before.optimum="2.1cm">
							<fo:table table-layout="fixed">
								<fo:table-column column-width="16cm"/>
								<fo:table-column column-width="3cm"/>
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell>
											<fo:block font-family="sans-serif" font-size="11pt" font-weight="bold">
										&#160;&#160;&#160;&#160;&#160;<xsl:value-of select="data/businessTaxGroup/businessTax/ACT_TYPE"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell>
											<fo:block font-family="sans-serif" font-size="11pt" font-weight="bold">
												<xsl:value-of select="data/businessTaxGroup/businessTax/CLASS_CODE"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
						<fo:block text-align="start" space-before.optimum="6mm">
							<fo:table table-layout="fixed">
								<fo:table-column column-width="19cm"/>
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell>
											<fo:block font-family="sans-serif" font-size="11pt" font-weight="bold">
										&#160;&#160;&#160;&#160;&#160;<xsl:value-of select="data/businessTaxGroup/businessTax/BUSINESS_NAME"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>

									<xsl:if test="data/businessTaxGroup/businessTax/BUSINESS_OWNER_NAME !=''" >

										<fo:table-row>
										<fo:table-cell>
											<fo:block font-family="sans-serif" font-size="11pt" font-weight="bold">
										&#160;&#160;&#160;&#160;&#160;<xsl:value-of select="data/businessTaxGroup/businessTax/BUSINESS_OWNER_NAME"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>

									</xsl:if>
									<xsl:if test="data/businessTaxGroup/businessTax/CORPORATE_NAME !=''" >
										<fo:table-row>
											<fo:table-cell>
												<fo:block font-family="sans-serif" font-size="11pt" font-weight="bold" >
													&#160;&#160;&#160;&#160;&#160;<xsl:value-of select="data/businessTaxGroup/businessTax/CORPORATE_NAME"/>
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
									</xsl:if>


									<fo:table-row>
										<fo:table-cell>
											<fo:block font-family="sans-serif" font-size="11pt" font-weight="bold">
										&#160;&#160;&#160;&#160;&#160;<xsl:value-of select="data/businessTaxGroup/businessTax/BUSINESS_ADDRESS"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell>
											<fo:block font-family="sans-serif" font-size="11pt" font-weight="bold">
										&#160;&#160;&#160;&#160;&#160;<xsl:value-of select="data/businessTaxGroup/businessTax/BUSINESS_CITY_STATE_ZIP"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
						<fo:block text-align="start">
							<fo:table table-layout="fixed">
								<fo:table-column column-width="15.15cm"/>
								<fo:table-column column-width="4cm"/>
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell>
								</fo:table-cell>
										<fo:table-cell>
											<fo:block font-family="sans-serif" font-size="11pt" wrap-option="no-wrap" font-weight="bold">
												<xsl:if test="data/businessTaxGroup/businessTax/HOME_OCCUPATION='Y'">  					 		
											OFFICE ONLY!
										</xsl:if>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:block-container>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
	<xsl:template match="data">
		<fo:block text-align="start" space-before.optimum="6mm">
			<fo:table table-layout="fixed" start-indent="11mm">
				<fo:table-column column-width="19cm"/>
				<fo:table-body>
					<fo:table-row>
						<fo:table-cell>
							<fo:block font-family="sans-serif" font-size="11pt" font-weight="bold">
								<xsl:value-of select="businessTaxGroup/businessTax/BUSINESS_NAME"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<xsl:if test="businessTaxGroup/businessTax/ATTN !=''" >
						<fo:table-row>
							<fo:table-cell>
								<fo:block font-family="sans-serif" font-size="11pt" font-weight="bold" >
									ATTN: <xsl:value-of select="businessTaxGroup/businessTax/ATTN"/>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</xsl:if>					
					
					<fo:table-row>
						<fo:table-cell>
							<fo:block font-family="sans-serif" font-size="11pt" font-weight="bold">
								<xsl:value-of select="businessTaxGroup/businessTax/MAILING_ADDRESS"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell>
							<fo:block font-family="sans-serif" font-size="11pt" font-weight="bold">
								<xsl:value-of select="businessTaxGroup/businessTax/MAIL_CITY_STATE_ZIP"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell>
							<fo:block text-align="start" font-size="12pt" font-family="sans-serif">
								<xsl:if test="string-length(businessTaxGroup/businessTax/conditionsgrp/conditions/condition)>0">
									<fo:block font-family="sans-serif" font-size="11pt" font-weight="bold" space-before.optimum="2.4cm" space-after.optimum="5pt" text-align="left">
										<fo:list-block white-space-collapse="false">
											<xsl:apply-templates select="businessTaxGroup/businessTax/conditionsgrp"/>
										</fo:list-block>
									</fo:block>
								</xsl:if>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>
		</fo:block>
	</xsl:template>
	<xsl:template match="conditions">
		<fo:list-item>
			<fo:list-item-label end-indent="label-end()">
				<fo:block font-family="sans-serif" font-size="9pt">
				</fo:block>
			</fo:list-item-label>
			<fo:list-item-body start-indent="5mm">
				<fo:block font-family="sans-serif" font-size="9pt" space-after.optimum="5pt">
					<xsl:value-of select="condition"/>
				</fo:block>
			</fo:list-item-body>
		</fo:list-item>
	</xsl:template>
</xsl:stylesheet>
