<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	version="1.0"> 
	
<xsl:template match="root">
<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<fo:layout-master-set>
	    <fo:simple-page-master 
	             master-name="all" 
	             page-height="11.5in" 
	             page-width="8.5in" 
	             margin-top="0.4in" 
	             margin-bottom="0.3in" 
		     margin-left="0.5in" 
		     margin-right="0.5in">
	        <fo:region-body margin-top="2.7in" margin-bottom="0in"/> 
		<fo:region-before extent="2.7in"/>
		
	   </fo:simple-page-master>
	</fo:layout-master-set>
	<fo:page-sequence master-reference="all" >

<!-- Header start   -->         
    
    <fo:static-content flow-name="xsl-region-before">
       	<fo:block text-align="start" line-height="1em + 2pt">
	<fo:table table-layout="fixed" >
				<fo:table-column column-width="4cm"/>
				<fo:table-column column-width="10cm"/>
				<fo:table-column column-width="5cm"/>
				<fo:table-body>
				   
				   <fo:table-row space-after.optimum="5pt">
				       <fo:table-cell number-columns-spanned="3">
				           <fo:table table-layout="fixed" >
					      <fo:table-column column-width="6cm"/>
					      <fo:table-column column-width="6cm"/>
					      <fo:table-column column-width="7cm"/>
                                              <fo:table-body>
				                  <fo:table-row >
				                      <fo:table-cell >
				                          <fo:block ></fo:block>
      			                              </fo:table-cell>
 				 	              <fo:table-cell>
				 	                 <fo:block background-color="#DDD8BD" 
				 	                           text-align="center" 
				 	                           font-family="sans-serif" 
				 	                           font-size="16pt"  
				 	                           font-weight="bold" >Contractor Tax 
				 	                 </fo:block>
				 	                           
      			                              </fo:table-cell>
				 	              <fo:table-cell >
				 	                 <fo:block ></fo:block>
      			                              </fo:table-cell>
      			                         </fo:table-row>
      			                      </fo:table-body>
      			                   </fo:table>  
      			               </fo:table-cell>    
                                    </fo:table-row> 
				   
				    <fo:table-row space-after.optimum="5pt">
				       <fo:table-cell text-align="center" number-columns-spanned="3">
				             <fo:block ><xsl:text> &#160; </xsl:text></fo:block>
                                       </fo:table-cell>    
                                    </fo:table-row> 
				   
				   <fo:table-row>
				      <fo:table-cell>
				         <fo:block space-after="2.5mm" margin-left="2mm">
				            <fo:external-graphic src="/f02/EPALSAPPS/XSL_IMAGES/CITYLOGO.jpg" height="3.25cm" width="3.0cm"/>
				         </fo:block>
      			              </fo:table-cell>
	 		              <fo:table-cell>
				         <fo:block text-align="start" font-family="sans-serif" font-size="14pt" wrap-option="no-wrap"  font-weight="bold" >
				         <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				         DEPARTMENT OF COMMUNITY DEVELOPMENT
				         <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				         <xsl:value-of select="data/main/department"/> 
				            <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				          275 East Olive Ave. 
				           <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				          Burbank, Calif. 90210 
				        </fo:block>
				      </fo:table-cell>				      
				      
				      <fo:table-cell>
			                  <fo:block text-align="start" font-family="sans-serif" font-size="10pt" wrap-option="no-wrap">
				              <fo:block> <xsl:text>&#xA;</xsl:text> </fo:block>
				              <xsl:text> &#160; </xsl:text>
				              <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				              <xsl:text> &#160; </xsl:text>
				              <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				              <xsl:text> &#160; </xsl:text>
				              <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				              Permit No<xsl:text> &#160; </xsl:text><xsl:text> &#160; </xsl:text><xsl:text> &#160; </xsl:text>: <fo:inline font-weight="bold"><xsl:value-of select="data/main/actnbr"/> </fo:inline> 
				              <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				              Type : <xsl:value-of select="data/main/acttype"/> 
				              <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				              <xsl:value-of select="data/main/rundate"/> 
				              <xsl:text> &#160; </xsl:text>
				              Page<xsl:text> &#160; </xsl:text><fo:page-number/> of <fo:page-number-citation ref-id="last-page"/>
				              <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				         </fo:block>
				      </fo:table-cell>				      
				  </fo:table-row>

				  
				  <fo:table-row>
				      <fo:table-cell number-columns-spanned="3">
				          <fo:table table-layout="fixed" >
					      <fo:table-column column-width="2.5cm"/>
					      <fo:table-column column-width="11.5cm"/>
					      <fo:table-column column-width="5cm"/>
						  <fo:table-body>
						      <fo:table-row>
						          <fo:table-cell>
						             <fo:block font-family="sans-serif" font-size="9pt"> Job Address: </fo:block>
				                          </fo:table-cell>	
				                         <fo:table-cell>
				                             <fo:block font-family="sans-serif" font-size="9pt" wrap-option="no-wrap" >
				                                  <xsl:value-of select="data/main/address"/>
				                             </fo:block>
				                         </fo:table-cell>		      
				                         <fo:table-cell text-align="start">
				                            <fo:block font-family="sans-serif" font-size="9pt" >
					   	               PRE<xsl:text> &#160; </xsl:text><xsl:text> &#160; </xsl:text>: <xsl:value-of select="data/main/planreviewer"/>
						            </fo:block>
						         </fo:table-cell>
						       </fo:table-row>  
						       <fo:table-row>
						          <fo:table-cell>
						             <fo:block font-family="sans-serif" font-size="9pt"></fo:block>
				                          </fo:table-cell>	
				                         <fo:table-cell>
				                             <fo:block font-family="sans-serif" font-size="9pt" wrap-option="no-wrap" >
				                                  
				                             </fo:block>
				                         </fo:table-cell>		      
				                         <fo:table-cell text-align="start">
				                            <fo:block font-family="sans-serif" font-size="9pt" >
					   	               Entered By <xsl:text> &#160; </xsl:text><xsl:text> &#160; </xsl:text>: <xsl:value-of select="data/main/enteredby"/>
						            </fo:block>
						         </fo:table-cell>
						       </fo:table-row>  
                                                    </fo:table-body>
					     </fo:table>
				         </fo:table-cell>	
        		          </fo:table-row>	

		      		</fo:table-body>
		      	    </fo:table>
		      	    <fo:leader leader-alignment="reference-area" leader-pattern="rule" /> 
		      	</fo:block>    
                   </fo:static-content> 
<!-- Header end -->    




    <fo:flow flow-name="xsl-region-body">
		<xsl:apply-templates select="data"/>
		<!-- To get number of pages -->
		<fo:block id="last-page"/>
		
    </fo:flow>
    </fo:page-sequence>
</fo:root>
</xsl:template>

<xsl:template match="data">
	<fo:block text-align="start" font-size="12pt" font-family="sans-serif">
            <xsl:apply-templates select="main"/>
	    <xsl:apply-templates select="main/address"/>
	    <xsl:apply-templates select="main/valuation"/>
	    <xsl:apply-templates select="main/actdesc"/>                       
	    
		<fo:table table-layout="fixed" >
		    <fo:table-column column-width="2cm"/>
		    <fo:table-column column-width="5.5cm"/>
		    <fo:table-column column-width="3cm"/>
		    <fo:table-column column-width="3.2cm"/>
		    <fo:table-column column-width="2.3cm"/>
		    <fo:table-column column-width="2.5cm"/>
		    <fo:table-header  space-after.optimum="5pt">
		       
		       <fo:table-row>
			   <fo:table-cell number-columns-spanned="2">
				  <fo:block background-color="#DDD8BD" font-family="sans-serif" font-size="12pt" font-weight="bold">Contractor Tax</fo:block>
			   </fo:table-cell>
			   <fo:table-cell number-columns-spanned="4">
				  <fo:block font-family="sans-serif" font-size="9pt" font-weight="bold"><xsl:text> &#160; </xsl:text></fo:block>
			   </fo:table-cell>
		       </fo:table-row>
		       
		       <fo:table-row>
			   <fo:table-cell>
				  <fo:block font-family="sans-serif" font-size="9pt" font-weight="bold"><xsl:text> &#160; </xsl:text></fo:block>
			   </fo:table-cell>
		       </fo:table-row>

		       <fo:table-row background-color="#DDD8BD">
			   <fo:table-cell>
				  <fo:block font-family="sans-serif" font-size="9pt" font-weight="bold">License No</fo:block>
			   </fo:table-cell>
			   <fo:table-cell>
				  <fo:block font-family="sans-serif" font-size="9pt" font-weight="bold">Contractor Name</fo:block>
			   </fo:table-cell>
			   <fo:table-cell>
			      <fo:block text-align="right" font-family="sans-serif" font-size="9pt" font-weight="bold">Valuation</fo:block>
			   </fo:table-cell>		      
			   <fo:table-cell>
				  <fo:block text-align="right" font-family="sans-serif" font-size="9pt"  font-weight="bold">Contractor Tax</fo:block>
		           </fo:table-cell>
			   <fo:table-cell>
		          <fo:block text-align="right" font-family="sans-serif" font-size="9pt" font-weight="bold">Tax Paid</fo:block>
			   </fo:table-cell>
			   <fo:table-cell>
				  <fo:block text-align="right" font-family="sans-serif" font-size="9pt" font-weight="bold">Balance Due</fo:block>
			   </fo:table-cell>
		       </fo:table-row>
		    </fo:table-header>
		    
		    <fo:table-body>
		      <xsl:for-each select="contractors/contractor"> 
	               <fo:table-row >
	                    <fo:table-cell>
	                      <fo:block text-align="center" font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="licno"/>
		              </fo:block>
 		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="name"/>
		              </fo:block>
		            </fo:table-cell>
  		            <fo:table-cell>
		              <fo:block text-align="right" font-family="sans-serif" font-size="9pt">
		              	<xsl:if test="string-length(valuation)>0" >
			              <xsl:value-of select="format-number(valuation, '$#,##0.00 ')"/>   
			        </xsl:if>      
		              </fo:block>
		            </fo:table-cell>	
		            <fo:table-cell>
		              <fo:block text-align="right" font-family="sans-serif" font-size="9pt">
		              	<xsl:if test="string-length(tax)>0" >
			              <xsl:value-of select="format-number(tax, '$#,##0.00 ')"/>   
			        </xsl:if>      
		              </fo:block>
		            </fo:table-cell>	
		            <fo:table-cell>
		              <fo:block text-align="right" font-family="sans-serif" font-size="9pt">
		              	<xsl:if test="string-length(paid)>0" >
			              <xsl:value-of select="format-number(paid, '$#,##0.00 ')"/>   
			        </xsl:if>      
		              </fo:block>
		            </fo:table-cell>	
		            <fo:table-cell>
		              <fo:block text-align="right" font-family="sans-serif" font-size="9pt">
		              	<xsl:if test="string-length(due)>0" >
			              <xsl:value-of select="format-number(due, '$#,##0.00 ')"/>   
			        </xsl:if>      
		              </fo:block>
		            </fo:table-cell>	
		        </fo:table-row>  
    
		        <fo:table-row >
        	            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt"></fo:block>
		            </fo:table-cell>			        
		            <fo:table-cell number-columns-spanned="5">
		              <fo:block font-family="sans-serif" font-size="9pt" space-after.optimum="5pt">
			              <xsl:value-of select="workdesc"/>
		              </fo:block>
		            </fo:table-cell>		            
		        </fo:table-row>
		     </xsl:for-each>  

                        	   

			           <fo:table-row >
   
					   <fo:table-cell number-columns-spanned="2">
					       <fo:block font-family="sans-serif" font-size="9pt" font-weight="bold"><xsl:text> &#160; </xsl:text></fo:block>
					   </fo:table-cell>

 
 
                                       <fo:table-cell number-columns-spanned="4">
		                           <fo:block text-align="right" font-family="sans-serif" font-size="9pt" font-weight="bold">
		                              <fo:leader leader-alignment="reference-area" leader-pattern="rule" />
		                           </fo:block>
		                       </fo:table-cell>			        
				       
				  </fo:table-row>  

			           <fo:table-row >
        	                       <fo:table-cell number-columns-spanned="3">
		                           <fo:block text-align="right" font-family="sans-serif" font-size="9pt" font-weight="bold">TOTAL:</fo:block>
		                       </fo:table-cell>			        
				       
				       <fo:table-cell>
				           <fo:block text-align="right" font-family="sans-serif" font-size="9pt" font-weight="bold">
				               <xsl:value-of select="//taxsum"/>   
					   </fo:block>
				       </fo:table-cell>	
				       <fo:table-cell>
				           <fo:block text-align="right" font-family="sans-serif" font-size="9pt" font-weight="bold">
				              <xsl:value-of select="//paidsum"/>   
					   </fo:block>
				       </fo:table-cell>	
				       <fo:table-cell>
				           <fo:block text-align="right" font-family="sans-serif" font-size="9pt" font-weight="bold">
				               <xsl:value-of select="//duesum"/>   
					   </fo:block>
				       </fo:table-cell>	
				  </fo:table-row>  

                  </fo:table-body>	    
		</fo:table>				
	    </fo:block>

	<fo:block text-align="start" font-size="12pt" font-family="sans-serif" break-before="page">
		<fo:table table-layout="fixed" >
		    <fo:table-column column-width="2.5cm"/>
		    <fo:table-column column-width="3.5cm"/>
		    <fo:table-column column-width="2.7cm"/>
		    <fo:table-column column-width="2.5cm"/>
		    <fo:table-header  space-after.optimum="5pt">
		          
		       <fo:table-row>
			   <fo:table-cell number-columns-spanned="2">
				  <fo:block background-color="#DDD8BD" font-family="sans-serif" font-size="12pt" font-weight="bold">Contractor Tax Transactions</fo:block>
			   </fo:table-cell>
			   <fo:table-cell number-columns-spanned="2">
				  <fo:block font-family="sans-serif" font-size="9pt" font-weight="bold"><xsl:text> &#160; </xsl:text></fo:block>
			   </fo:table-cell>
		       </fo:table-row>
		       
		       <fo:table-row>
			   <fo:table-cell>
				  <fo:block font-family="sans-serif" font-size="9pt" font-weight="bold"><xsl:text> &#160; </xsl:text></fo:block>
			   </fo:table-cell>
        	       </fo:table-row>
      
		    </fo:table-header>
		    
		    <fo:table-body>
		      
		      <xsl:for-each select="//transaction"> 


		       <fo:table-row  keep-with-next="always">
			   
			   <fo:table-cell>
				  <fo:block font-family="sans-serif" font-size="9pt" font-weight="bold">Transaction No</fo:block>
			   </fo:table-cell>
			   <fo:table-cell>
				  <fo:block text-align="center" font-family="sans-serif" font-size="9pt" font-weight="bold">
				      <xsl:value-of select="id"/>
				  </fo:block>
			   </fo:table-cell>
			   
			   <fo:table-cell>
			      <fo:block text-align="right" font-family="sans-serif" font-size="9pt" font-weight="bold">Posted on</fo:block>
			   </fo:table-cell>		      
			   <fo:table-cell>
				  <fo:block text-align="center" font-family="sans-serif" font-size="9pt" font-weight="bold">
				      <xsl:value-of select="date"/>
				  </fo:block>
		           </fo:table-cell>
		       </fo:table-row>
		       
		       
		       <fo:table-row background-color="#DDD8BD" keep-with-next="always">
			   <fo:table-cell>
				  <fo:block text-align="center" font-family="sans-serif" font-size="9pt" >Date</fo:block>
			   </fo:table-cell>
			   <fo:table-cell >
				  <fo:block text-align="center" font-family="sans-serif" font-size="9pt" >Transaction Type</fo:block>
			   </fo:table-cell>
			   <fo:table-cell>
			      <fo:block text-align="center" font-family="sans-serif" font-size="9pt" >Method</fo:block>
			   </fo:table-cell>		      
			   <fo:table-cell>
				  <fo:block text-align="right" font-family="sans-serif" font-size="9pt" >Amount</fo:block>
		           </fo:table-cell>
		       </fo:table-row>

		       <fo:table-row keep-with-next="always">
			   <fo:table-cell>
			       <fo:block text-align="center" font-family="sans-serif" font-size="9pt">
				     <xsl:value-of select="date"/>
			       </fo:block>
			   </fo:table-cell>
		           <fo:table-cell>
			       <fo:block font-family="sans-serif" font-size="9pt">
					 <xsl:value-of select="type"/>   
				 </fo:block>
			      </fo:table-cell>	
			      <fo:table-cell>
				 <fo:block text-align="center" font-family="sans-serif" font-size="9pt">
				     <xsl:value-of select="method"/>   
				 </fo:block>
			      </fo:table-cell>	
			      <fo:table-cell>
				 <fo:block text-align="right" font-family="sans-serif" font-size="9pt" font-weight="bold">
				     <xsl:value-of select="amount"/>   
				 </fo:block>
			      </fo:table-cell>	
			  </fo:table-row>  
		       
		       <fo:table-row keep-with-next="always">
           	           <fo:table-cell >
		  	       <fo:block font-family="sans-serif" font-size="9pt"><xsl:text> &#160; </xsl:text></fo:block>
			   </fo:table-cell>
                       </fo:table-row>

		       <fo:table-row background-color="#DDD8BD" keep-with-next="always">
			   <fo:table-cell>
				  <fo:block font-family="sans-serif" font-size="9pt" >License No</fo:block>
			   </fo:table-cell>
			   <fo:table-cell number-columns-spanned="2">
				  <fo:block font-family="sans-serif" font-size="9pt" >Name</fo:block>
			   </fo:table-cell>
			   <fo:table-cell >
			      <fo:block text-align="right" font-family="sans-serif" font-size="9pt" font-weight="bold">Amount</fo:block>
			   </fo:table-cell>		      
		       </fo:table-row>


		       <fo:table-row keep-with-next="always">
			   <fo:table-cell>
			       <fo:block font-family="sans-serif" font-size="9pt">
				     <xsl:value-of select="../../licno"/>
			       </fo:block>
			   </fo:table-cell>
		           <fo:table-cell number-columns-spanned="2">
			       <fo:block font-family="sans-serif" font-size="9pt">
					 <xsl:value-of select="../../name"/>   
				 </fo:block>
			    </fo:table-cell>	
			      <fo:table-cell>
				 <fo:block text-align="right" font-family="sans-serif" font-size="9pt" font-weight="bold">
				     <xsl:value-of select="amount"/>   
				 </fo:block>
			      </fo:table-cell>	
			</fo:table-row>  

		       <fo:table-row keep-with-next="always">
			   <fo:table-cell number-columns-spanned="2">
				  <fo:block font-family="sans-serif" font-size="9pt" font-weight="bold"><xsl:text> &#160; </xsl:text></fo:block>
			   </fo:table-cell>
        	          
			   <fo:table-cell number-columns-spanned="2">
				  <fo:block font-family="sans-serif" font-size="9pt" font-weight="bold">
				    <fo:leader leader-alignment="reference-area" leader-pattern="rule" />
				  </fo:block>
			   </fo:table-cell>
        	       </fo:table-row>

		       <fo:table-row keep-with-next="always">
			   
			   <fo:table-cell >
				  <fo:block font-family="sans-serif" font-size="9pt" font-weight="bold"><xsl:text> &#160; </xsl:text></fo:block>
			   </fo:table-cell>
 
 			   <fo:table-cell number-columns-spanned="2">
 				  <fo:block text-align="right" font-family="sans-serif" font-size="9pt" font-weight="bold">Transaction Total:</fo:block>
 			   </fo:table-cell>
 
			   <fo:table-cell>
			       <fo:block text-align="right" font-family="sans-serif" font-size="9pt" font-weight="bold">
			          <xsl:value-of select="amount"/>   
			       </fo:block>
			   </fo:table-cell>	
        	       </fo:table-row>
		       <fo:table-row keep-with-next="always">
           	           <fo:table-cell >
		  	       <fo:block font-family="sans-serif" font-size="9pt" font-weight="bold"><xsl:text> &#160; </xsl:text></fo:block>
			   </fo:table-cell>
                       </fo:table-row>
		       <fo:table-row >
           	           <fo:table-cell >
		  	       <fo:block font-family="sans-serif" font-size="9pt" font-weight="bold"><xsl:text> &#160; </xsl:text></fo:block>
			   </fo:table-cell>
                       </fo:table-row>

		     </xsl:for-each>  
                  </fo:table-body>	    
		</fo:table>				
	    </fo:block>

</xsl:template>

<xsl:template match="main/address">
	<fo:block space-before.optimum="5pt" text-align="start" >
	    <fo:table table-layout="fixed" >
		 <fo:table-column column-width="4cm"/>
		 <fo:table-column column-width="15cm"/>
		 <fo:table-body>
		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Base Address </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="."/> 
		              </fo:block>
		            </fo:table-cell>
		        </fo:table-row>
		 </fo:table-body>
	    </fo:table>
	</fo:block>		          
</xsl:template>

<xsl:template match="main/valuation">
	<fo:block space-before.optimum="5pt" text-align="start" >
	    <fo:table table-layout="fixed" >
		 <fo:table-column column-width="4cm"/>
		 <fo:table-column column-width="15cm"/>
		 <fo:table-body>
		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Valuation </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
		              	<xsl:if test="string-length(.)>0" >
			                <xsl:value-of select="format-number(., '$#,##0.00 ')"/>
			        </xsl:if>        
		              </fo:block>
		            </fo:table-cell>
		        </fo:table-row>
		 </fo:table-body>
	    </fo:table>
	</fo:block>		          
</xsl:template>

<xsl:template match="main/actdesc">
	<fo:block space-before.optimum="5pt" text-align="start" >
	    <fo:table table-layout="fixed" >
		 <fo:table-column column-width="4cm"/>
		 <fo:table-column column-width="15cm"/>
		 <fo:table-body>
		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Job Description </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="."/> 
		              </fo:block>
		            </fo:table-cell>
		        </fo:table-row>
		 </fo:table-body>
	    </fo:table>
	</fo:block>		          
</xsl:template>

<xsl:template match="main">
	<fo:block text-align="start" >
	    <fo:table table-layout="fixed" >
		 <fo:table-column column-width="4cm"/>
		 <fo:table-column column-width="10cm"/>
		 <fo:table-column column-width="2cm"/>
		 <fo:table-column column-width="3cm"/>
		 <fo:table-body>
		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Project No </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			                <xsl:value-of select="prjnbr"/>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Applied </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>: </xsl:text>
			              <xsl:if test="string-length(applieddate)>0" >
			              	 <xsl:value-of select="substring(applieddate,6,2)" /> <xsl:text>/</xsl:text>  <xsl:value-of select="substring(applieddate,9,2)" /><xsl:text>/</xsl:text><xsl:value-of select="substring(applieddate, 1,4)" />
			              </xsl:if>	
		              </fo:block>
		            </fo:table-cell>
		          </fo:table-row>

			  <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Activity Type </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			               <xsl:value-of select="acttype"/>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Issued </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>: </xsl:text>
			              <xsl:if test="string-length(issueddate)>0" >
			              	 <xsl:value-of select="substring(issueddate,6,2)" /> <xsl:text>/</xsl:text>  <xsl:value-of select="substring(issueddate,9,2)" /><xsl:text>/</xsl:text><xsl:value-of select="substring(issueddate, 1,4)" />
			              </xsl:if>				              
		              </fo:block>
		            </fo:table-cell>
		          </fo:table-row>
   

			  <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Project Name </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			               <xsl:value-of select="prjname"/>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Completed </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>: </xsl:text>
			              <xsl:if test="string-length(completeddate)>0" >
			              	 <xsl:value-of select="substring(completeddate,6,2)" /> <xsl:text>/</xsl:text>  <xsl:value-of select="substring(completeddate,9,2)" /><xsl:text>/</xsl:text><xsl:value-of select="substring(completeddate, 1,4)" />
			              </xsl:if>				              
			              
		              </fo:block>
		            </fo:table-cell>
		          </fo:table-row>

			  <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Parcel Number </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			               <xsl:value-of select="parcelnumber"/> 
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>To Expire </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>: </xsl:text>
			              <xsl:if test="string-length(expiredate)>0" >
			              	 <xsl:value-of select="substring(expiredate,6,2)" /> <xsl:text>/</xsl:text>  <xsl:value-of select="substring(expiredate,9,2)" /><xsl:text>/</xsl:text><xsl:value-of select="substring(expiredate, 1,4)" />
			              </xsl:if>				              
		              </fo:block>
		            </fo:table-cell>
		          </fo:table-row>
		</fo:table-body>
	    </fo:table>
	</fo:block>
    </xsl:template>

</xsl:stylesheet>


