<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" version="1.0"> 
	<xsl:template match="root">   
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
			<fo:layout-master-set>
				<fo:simple-page-master master-name="all" page-height="11.0in" page-width="8.5in" margin-top="0.3in" margin-bottom="0.3in" margin-left="0.5in" margin-right="0.5in">
					<fo:region-body margin-top="1.0in" margin-bottom="1.5in"/>
					<fo:region-before extent="2.2in"/> 
					<fo:region-after extent="1.6in"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-reference="all">
				<fo:static-content flow-name="xsl-region-before">
				</fo:static-content>
    <fo:flow flow-name="xsl-region-body">					
					<xsl:apply-templates select="data"/>
					 
		<fo:table table-layout="fixed" text-align="center" border-color="black" border-style="solid" border-width="0.0pt">
		  <fo:table-column column-width="3cm"/>
		  <fo:table-column column-width="15cm"/>

		 <fo:table-body>
		   <fo:table-row>
		      <fo:table-cell>
				 <fo:block color="rgb(0,0,0)"  text-align="center" font-size="9pt" font-weight="bold">  
		                  <fo:external-graphic src="D:\BlWorkspace\ELMS-3.6\EPALSWeb\WebContent\jsp\images\site_logo_shield.jpg" content-width="2cm"
		                      content-height="2cm" text-align="center" /></fo:block>
              </fo:table-cell> 
              <fo:table-cell>  
              		<fo:block color="rgb(0,0,0)" text-align="center" font-size="9pt" font-weight="bold"> CITY OF BURBANK - LICENSE AND CODE SERVICES DIVISION</fo:block>
                    <fo:block color="rgb(0,0,0)"  text-align="center" font-size="9pt" font-weight="bold"> BUSINESS LICENSE BILL</fo:block>
             		<fo:block id="last-page"/>
						<fo:block text-align="start" font-size="9pt" font-weight="bold" > 
							<fo:block text-align="center" margin-left="5.5cm"> 
								<xsl:text>  FISCAL YEAR&#160;&#160; </xsl:text>
								<xsl:value-of select="data/batchPrintGroup/businessLicenseGroup/FisicalYear/START_YEAR"/>
								<xsl:text> - </xsl:text>
								<xsl:value-of select="data/batchPrintGroup/businessLicenseGroup/FisicalYear/END_YEAR"/> 
							<xsl:text> &#160;&#160;  &#160;&#160;  &#160;&#160; &#160;&#160;
								 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
					 			 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
					 			 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
					 			 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
					 			&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
					 			&#160;&#160;&#160; </xsl:text> 	
					 			 <!-- Removed the Total fee amount for last year -->
						 </fo:block>
					</fo:block>
			   </fo:table-cell>			   
	   	   </fo:table-row> 
	    </fo:table-body> 
	  </fo:table>  
	   	
   	<fo:table table-layout="fixed" text-align="center" border-color="black" border-style="solid" border-width="0.0pt">
  		 <fo:table-column column-width="20cm"/>
		 <fo:table-column column-width="20cm"/>

		 <fo:table-body>
			 <fo:table-row>
			     <fo:table-cell>  
			          <fo:block font-size="8pt" font-family="sans-serif" line-height="1pt"  line-width="16pt" space-before.optimum="1pt" space-after.optimum="1pt"  text-align="center"  font-weight="bold">
						   <xsl:apply-templates select="feesgrp"/>
					   </fo:block>
	            </fo:table-cell> 
	        </fo:table-row> 
	    </fo:table-body> 
   </fo:table> 
   
	       <xsl:if test="string-length(totalFeeAmt)>0" >
		          <fo:block font-size="8pt" font-family="sans-serif" line-height="16pt"  line-width="16pt" space-before.optimum="1pt" margin-left="8.5cm" space-after.optimum="5pt"   font-weight="bold">
		    	         <xsl:text> TOTAL AMOUNT DUE </xsl:text>
		            	 <xsl:value-of select="format-number(totalFeeAmt, '= $#,##0.00 ')"/> 
				 </fo:block>
		   </xsl:if>

        <fo:block color="rgb(0,0,0)"  space-after="0.3cm" text-align="center" font-size="9pt" font-weight="bold">TEN PERCENT (10%) PENALTY WILL BE IMPOSED IF NOT PAID BY JULY 31,
               <xsl:value-of select="data/batchPrintGroup/businessLicenseGroup/businessLicense/RENEWAL_DT"/> 
        </fo:block>

    	<fo:block text-align="start" margin-left=".6cm"  space-after="1.1cm" line-height="18pt" font-size="8.6pt" font-weight="bold"> 
				<xsl:value-of select="data/batchPrintGroup/businessLicenseGroup/businessLicense/BUSINESS_TYPE"/>   
				<xsl:text> &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; 
			    		   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
						   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
						   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
						   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; 
				 </xsl:text>
				 <xsl:value-of select="data/batchPrintGroup/businessLicenseGroup/businessLicense/CLASS_CODE"/>   
				<xsl:text> &#160; </xsl:text>
				<xsl:value-of select="data/batchPrintGroup/businessLicenseGroup/businessLicense/DECAL_CODE"/>  
		</fo:block>
				
		<fo:block text-align="start" margin-left=".6cm" font-size="8.6pt" font-weight="bold"> 
				<xsl:value-of select="data/batchPrintGroup/businessLicenseGroup/businessLicense/BUSINESS_NAME"/>   
		</fo:block>
		
		<fo:block text-align="start" margin-left=".6cm" font-size="8.6pt" font-weight="bold"> 
				<xsl:value-of select="data/batchPrintGroup/businessLicenseGroup/businessLicense/MAILING_ADDRESS"/> 
				<xsl:text> &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; 
						   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
						   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
						   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
						   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; 
						   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; 
						   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; 
						   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; 
						   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; 
						   WRITE THIS NUMBER
				  </xsl:text> 
		
		<fo:block text-decoration="underline">
			  <xsl:text> &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; 
						 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
						 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
						 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
						 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; 
						 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; 
						 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; 
						 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; 
						 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; 
						 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
						 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
						 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
						 ON YOUR CHECK =
			  </xsl:text>
	   </fo:block>
									  
	   </fo:block>
						
	   <fo:block text-align="start"  space-after="2.0cm" margin-left=".6cm" font-size="8.6pt" font-weight="bold"> 
			<xsl:value-of select="data/batchPrintGroup/businessLicenseGroup/businessLicense/MAILING_ADDRESS_CITYSTATEZIP"/>   
			<xsl:text> &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; 
					   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
					   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
					   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
					   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; 
					   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; 
					   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; 
					   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; 
					   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; 
					   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
			</xsl:text>
			<xsl:value-of select="data/batchPrintGroup/businessLicenseGroup/businessLicense/BUSINESS_LICENSE_NUMBER"/>  
	</fo:block>		
				
	 <fo:block text-align="start" margin-left=".6cm" font-size="8.6pt" font-weight="bold"> 
			<xsl:text> BUSINESS ADDRESS:   &#160;&#160; </xsl:text>
		    <xsl:value-of select="data/batchPrintGroup/businessLicenseGroup/businessLicense/BUSINESS_ADDRESS" />   
			<xsl:text> &#160;&#160;  &#160;&#160;  &#160;&#160; &#160;&#160;
			    	   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
					   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
					   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
					   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
					   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
		    		   Any question please call : </xsl:text>  
	 </fo:block> 
					
	<fo:block text-align="start" margin-left=".6cm" font-size="8.6pt" font-weight="bold"> 
			<xsl:text>   &#160;&#160;  &#160;&#160;  &#160;&#160; &#160;&#160;
						 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
			 			 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
			 			 &#160;&#160;&#160;&#160;&#160;&#160;&#160;
	    	</xsl:text>			
			<xsl:value-of select="data/batchPrintGroup/businessLicenseGroup/businessLicense/CITYSTATEZIP" />   
			<xsl:text> &#160;&#160;  &#160;&#160;  &#160;&#160; &#160;&#160;
		   			   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
					   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
					   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
					   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
					   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
					   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
					   (818) 238-5280</xsl:text>  
 	</fo:block>
  </fo:flow>
 </fo:page-sequence>
			
</fo:root>
 </xsl:template>

	  <!-- This is feesgrp start -->
	   <xsl:template match="fees"> 	   
    	   <fo:block font-size="8pt" font-family="sans-serif" line-height="16pt"  line-width="16pt" space-before.optimum="1pt" space-after.optimum="5pt"  text-align="center"  font-weight="bold">
			          <xsl:value-of select="feedesc"/>  
				           	<xsl:if test="string-length(amount)>0" >
					           	   <xsl:value-of select="format-number(amount, '= $#,##0.00 ')"/> 
				           	</xsl:if>    
	        </fo:block>
	  </xsl:template>
     <!-- feesgrp  end -->     
</xsl:stylesheet>	 