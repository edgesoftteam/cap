<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	version="1.0">	
<xsl:template match="root">
<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">

	<fo:layout-master-set>
	    <fo:simple-page-master master-name="all"
			page-height="11in" page-width="8.5in"
		margin-top="0.5in" margin-bottom="0.1in" 
		margin-left="0.5in" margin-right="0.5in">
			<fo:region-body margin-top="1.0in" margin-bottom="0.1in"/>
			<fo:region-before extent="1.0in"/>
			<fo:region-after extent="0.1in"/>
	    </fo:simple-page-master>
	</fo:layout-master-set>
	<fo:page-sequence master-reference="all" >

    <fo:static-content flow-name="xsl-region-before">
<!-- Header start   -->         
    
			<fo:block text-align="start" line-height="0.5em + 1pt">
			    <fo:table table-layout="fixed" background-color="#C9C299">
				<fo:table-column column-width="4cm"/>
				<fo:table-column column-width="10cm"/>
				<fo:table-column column-width="5cm"/>
				<fo:table-body>
				  <fo:table-row>
				      <fo:table-cell>
				<fo:block space-before="1.5mm" margin-left="2mm">
				    <fo:external-graphic src="/f02/EPALSAPPS/XSL_IMAGES/CITYLOGO_FIR.jpg"
				              
					      width="1.7cm"
					      height="1.6cm"/>
				</fo:block>
      
				      </fo:table-cell>
				      <fo:table-cell>
				        <fo:block text-align="start" font-family="sans-serif" font-size="16pt" wrap-option="no-wrap"
				               font-weight="bold">
				           
				           <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				           	<xsl:text> &#160;</xsl:text>
				           <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
						<xsl:text> &#160;</xsl:text>
				           <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				           	<xsl:text> &#160;</xsl:text>
				           <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				           FIELD INSPECTION REPORT
				            <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				            <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
						<xsl:text> &#160;</xsl:text>
				        </fo:block>
				      </fo:table-cell>	
				      <fo:table-cell>
				        <fo:block text-align="start" font-family="sans-serif" font-size="9pt" wrap-option="no-wrap">
				           
				           <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
						<xsl:text>&#160;</xsl:text>
				           <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
						<xsl:text>&#160;</xsl:text>
				           <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
						 <fo:block  font-weight="bold"  font-family="sans-serif" font-size="9pt" wrap-option="no-wrap"><xsl:text> Inspector : &#160;</xsl:text> <xsl:value-of select="//processteam/inspector"/> </fo:block>
				           <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
						<xsl:text>&#160;</xsl:text>

				           <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				           	Page<xsl:text> &#160; </xsl:text><fo:page-number/> of <fo:page-number-citation ref-id="last-page"/>
				           <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
						<xsl:text>&#160;</xsl:text>

				           <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
						<xsl:text>Permit No:  &#160;</xsl:text>
							<fo:inline font-weight="bold">
								<xsl:value-of select="//main/actnbr"/> 
							</fo:inline>
				           <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
						<xsl:text>&#160;</xsl:text>

				           <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				           	<xsl:text> Run Date :  </xsl:text> <xsl:value-of select="//main/rundate"/>
				           <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				            <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				            <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
						<xsl:text>&#160;</xsl:text>
				        </fo:block>
				      </fo:table-cell>	
				      
				      
				  </fo:table-row>
		      		</fo:table-body>
		      	    </fo:table>
		      	</fo:block>    
    
    
<!-- Header end -->    
    

    </fo:static-content> 
    <fo:static-content flow-name="xsl-region-after">
<!-- footer start   -->         
			<fo:block text-align="start">
 		      	</fo:block>    
    
     </fo:static-content> 
	
    <fo:flow flow-name="xsl-region-body">
		<xsl:apply-templates select="data"/>
		<!-- To get number of pages -->
		<fo:block id="last-page"/>
		
    </fo:flow>
	</fo:page-sequence>
</fo:root>
</xsl:template>

<xsl:template match="data">
	<fo:block text-align="start" font-size="12pt" font-family="sans-serif">
		
		<xsl:apply-templates select="main"/>
		

	<fo:block space-before.optimum="5pt" text-align="start" >
	    <fo:table table-layout="fixed" >
		 <fo:table-column column-width="9cm"/>
		 <fo:table-column column-width="10cm"/>
		 <fo:table-body>
		        <fo:table-row>
		            <fo:table-cell>
  				     <!-- Applicantgrp start -->
				     			<fo:block  text-align="start" >
				     			    <fo:table table-layout="fixed" >
				     				 <fo:table-column column-width="2cm"/>
				     				 <fo:table-column column-width="7cm"/>
				     				 <fo:table-body>
				     					<xsl:apply-templates select="applicantgrp"/>
				     				</fo:table-body>
				     			    </fo:table>
				     			</fo:block>
				     <!-- Applicantgrp End -->

		            </fo:table-cell>
		            <fo:table-cell>
		            	<!-- Owner start -->
			             <xsl:apply-templates select="owner"/>   
			    	<!-- Owner End -->
		            </fo:table-cell>
		        </fo:table-row>
		 </fo:table-body>
	    </fo:table>
	</fo:block>		          


	<xsl:apply-templates select="assessor"/>

<!-- peoplegrp start -->
			<fo:block  space-before.optimum="5pt" text-align="start" >
			    <fo:table table-layout="fixed" >
				 <fo:table-column column-width="3cm"/>
				 <fo:table-column column-width="2cm"/>
				 <fo:table-column column-width="7cm"/>
				 <fo:table-column column-width="3cm"/>
				 <fo:table-column column-width="2cm"/>
				 <fo:table-column column-width="3cm"/>
				 <fo:table-body>
					<xsl:apply-templates select="peoplegrp"/>
				</fo:table-body>
			    </fo:table>
			</fo:block>
<!-- peoplegrp End -->

	<xsl:apply-templates select="main/actdesc"/>
	
<!-- This is Active Holds start -->
	  <xsl:if test="string-length(activeholds/activehold/created)>0">
	      <fo:block font-size="11pt"  background-color="#C9C299"
		    font-family="sans-serif" 
		    line-height="16pt"
		    space-before.optimum="5pt"
		    space-after.optimum="5pt"
		    text-align="left"
		    font-weight="bold" >
		     Active Holds
	      </fo:block>
		  <fo:block text-align="start" space-after.optimum="-10pt">
		     <fo:table table-layout="fixed" >
				<fo:table-column column-width="4cm"/>
				<fo:table-column column-width="4cm"/>
				<fo:table-column column-width="4cm"/>
				<fo:table-column column-width="3cm"/>
				<fo:table-column column-width="4cm"/>
				<fo:table-body>
			  	   <fo:table-row>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt" font-weight="bold">Date</fo:block>
				      </fo:table-cell>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt" font-weight="bold">Type</fo:block>
				      </fo:table-cell>		      
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt" font-weight="bold">Issuer</fo:block>
				      </fo:table-cell>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt" font-weight="bold">Status</fo:block>
				      </fo:table-cell>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt" font-weight="bold">Status Date</fo:block>
				      </fo:table-cell>
				   </fo:table-row>
       		    </fo:table-body>
		     </fo:table>
		  </fo:block>
		  <fo:leader leader-alignment="reference-area" leader-pattern="rule" />
		  <fo:block text-align="start" >
		     <fo:table table-layout="fixed" >
			    <fo:table-column column-width="4cm"/>
			    <fo:table-column column-width="4cm"/>
			    <fo:table-column column-width="4cm"/>
			    <fo:table-column column-width="3cm"/>
			    <fo:table-column column-width="4cm"/>
			    <fo:table-body>
				   <xsl:apply-templates select="activeholds"/>
			    </fo:table-body>
			 </fo:table>
		  </fo:block>
       </xsl:if>
       
<!-- This is commentsgrp start -->
<xsl:if test="string-length(commentsgrp/comments/comments)>0" >
	      <fo:block font-size="11pt"  background-color="#C9C299"
		    font-family="sans-serif" 
		    line-height="16pt"
		    space-before.optimum="5pt"
		    space-after.optimum="5pt"
		    text-align="left"
		    font-weight="bold" >
		Comments
	      </fo:block>	
	</xsl:if>	
			<fo:block text-align="start"  white-space-collapse="false">
			    <fo:table table-layout="fixed" >
				 <fo:table-column column-width="18cm"/>
				 <fo:table-body>
					<xsl:apply-templates select="commentsgrp"/>
				</fo:table-body>
			    </fo:table>
			</fo:block>
<!-- commentsgrp  end -->

<!-- This is feesgrp start -->
	      
	      <fo:block font-size="11pt"  background-color="#C9C299"
		    font-family="sans-serif" 
		    line-height="16pt"
		    space-before.optimum="5pt"
		    space-after.optimum="5pt"
		    text-align="left"
		    font-weight="bold" >
		Fees
	      </fo:block>	
			<fo:block text-align="start" space-after.optimum="-10pt">
			    <fo:table table-layout="fixed" >
				<fo:table-column column-width="7cm"/>
				<fo:table-column column-width="2cm"/>
				<fo:table-column column-width="2cm"/>
				<fo:table-column column-width="2cm"/>
				<fo:table-column column-width="3cm"/>
				<fo:table-column column-width="3cm"/>
				<fo:table-body>
				  <fo:table-row>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt"
				               font-weight="bold"    >
				        Fee Description
				        </fo:block>
				      </fo:table-cell>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt"
				               font-weight="bold"   >
				        
				        </fo:block>
				      </fo:table-cell>		      
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt"
				               font-weight="bold"   >
				        Units
				        </fo:block>
				      </fo:table-cell>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt"
				               font-weight="bold"   >
				        Fee/Units
				        </fo:block>
				      </fo:table-cell>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt"
				               font-weight="bold"    >
				        Amount
				        </fo:block>
				      </fo:table-cell>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt"
				                font-weight="bold"  >
				        Paid
				        </fo:block>
				      </fo:table-cell>
				  </fo:table-row>
		      		</fo:table-body>
		      	    </fo:table>
		      	</fo:block>

		<fo:leader leader-alignment="reference-area" leader-pattern="rule" />
			
			<fo:block text-align="start" >
			    <fo:table table-layout="fixed" >
				 <fo:table-column column-width="7cm"/>
				 <fo:table-column column-width="2cm"/>
				 <fo:table-column column-width="2cm"/>
				 <fo:table-column column-width="2cm"/>
				 <fo:table-column column-width="3cm"/>
				 <fo:table-column column-width="3cm"/>
				 <fo:table-body>
					<xsl:apply-templates select="feesgrp"/>
				</fo:table-body>
			    </fo:table>
			</fo:block>
<!-- feesgrp  end -->


<!-- Plancheck, Permit Fees start -->

			<fo:block space-before.optimum="5pt" text-align="start" >
			    <fo:table table-layout="fixed" border-color="black" border-style="solid" border-width="1.0pt">
				<fo:table-column column-width="4cm"/>
				 <fo:table-column column-width="4cm"/>
				 <fo:table-column column-width="4cm"/>
				 <fo:table-column column-width="6cm"/>


				 <fo:table-body>
				 <fo:table-row>
				    <fo:table-cell>
				      <fo:block font-family="sans-serif" font-size="9pt"  text-align="center" font-weight="bold" background-color="#C9C299">
					    <xsl:text> Plan Check </xsl:text>
				      </fo:block>
				    </fo:table-cell>
				    <fo:table-cell>
				    <fo:block font-family="sans-serif" font-size="9pt"  text-align="center" font-weight="bold" background-color="#C9C299">
					    <xsl:text> Development Fee </xsl:text>
				      </fo:block>
				    </fo:table-cell>
				    <fo:table-cell>
				      <fo:block font-family="sans-serif" font-size="9pt"  text-align="center" font-weight="bold" background-color="#C9C299">
					      <xsl:text> Permit </xsl:text>
				      </fo:block>
				    </fo:table-cell>
				    <fo:table-cell>
				      <fo:block font-family="sans-serif" font-size="9pt" text-align="center" font-weight="bold" background-color="#C9C299">
					      <xsl:text> Total </xsl:text>
				      </fo:block>
				    </fo:table-cell>
				 </fo:table-row>  
				 </fo:table-body>
			    </fo:table>
			</fo:block>
			
			<fo:block  text-align="start" >
			    <fo:table table-layout="fixed" border-color="black" border-style="solid" border-width="0.5pt">
				<fo:table-column column-width="4cm"/>
				 <fo:table-column column-width="4cm"/>
				 <fo:table-column column-width="4cm"/>
				 <fo:table-column column-width="6cm"/>


				 <fo:table-body>
				 
				 <fo:table-row>
				    <fo:table-cell>
				      <fo:block font-family="sans-serif" font-size="9pt">
				<!-- Plan Check Fees start -->
						
					<fo:block text-align="start" >
					    <fo:table table-layout="fixed" border-color="black" border-style="solid" border-width="0.5pt">
						 <fo:table-column column-width="2cm"/>
						 <fo:table-column column-width="2cm"/>
						 <fo:table-body>
					<fo:table-row>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
						    <xsl:text> Fees: </xsl:text>
					      </fo:block>
					    </fo:table-cell>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
						<xsl:choose>
						<xsl:when test="string-length(plancheckfees/fees)>0" >
						     <xsl:value-of select="format-number(plancheckfees/fees, '$#,##0.00 ')"/> 
					        </xsl:when>
						     <xsl:otherwise> 
							   <xsl:text>$0.00 </xsl:text> 
						     </xsl:otherwise>
						</xsl:choose>
					      </fo:block>
					      </fo:table-cell>
					</fo:table-row>  
					<fo:table-row>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
						    <xsl:text>   &#160;</xsl:text>
					      </fo:block>
					    </fo:table-cell>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
						      <xsl:text> &#160; </xsl:text>
					      </fo:block>
					      </fo:table-cell>
					</fo:table-row>  
					<fo:table-row>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
						     <xsl:text> Payments: </xsl:text>
					      </fo:block>
					    </fo:table-cell>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
					      	<xsl:choose>
						<xsl:when test="string-length(plancheckfees/payments)>0" >
						     <xsl:value-of select="format-number(plancheckfees/payments, '$#,##0.00 ')"/> 
						</xsl:when>
						<xsl:otherwise> 
						   <xsl:text>$0.00 </xsl:text> 
						</xsl:otherwise>
						</xsl:choose>     
					      </fo:block>
					    </fo:table-cell>
					</fo:table-row>  
					<fo:table-row>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
						    <xsl:text>&#160;  </xsl:text>
					      </fo:block>
					    </fo:table-cell>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
						      <xsl:text> &#160; </xsl:text>
					      </fo:block>
					      </fo:table-cell>
					</fo:table-row>  
					<fo:table-row>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
						     <xsl:text> Balance Due: </xsl:text>
					      </fo:block>
					    </fo:table-cell>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
					      	<xsl:choose>
						<xsl:when test="string-length(plancheckfees/balance)>0" >
						     <xsl:value-of select="format-number(plancheckfees/balance, '$#,##0.00 ')"/> 
						</xsl:when>
						<xsl:otherwise> 
						   <xsl:text>$0.00 </xsl:text> 
						</xsl:otherwise>
						</xsl:choose>     
					      </fo:block>
					    </fo:table-cell>
					</fo:table-row>  

						</fo:table-body>
					    </fo:table>
					</fo:block>
				<!--  Plan Check Fees end -->	
				      </fo:block>
				    </fo:table-cell>
				    <fo:table-cell>
				      <fo:block font-family="sans-serif" font-size="9pt">
				      
				<!-- Development Fees start -->	
					<fo:block text-align="start" >
					    <fo:table table-layout="fixed" border-color="black" border-style="solid" border-width="0.5pt">
						 <fo:table-column column-width="2cm"/>
						 <fo:table-column column-width="2cm"/>
						 <fo:table-body>
					<fo:table-row>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
						    <xsl:text> Fees: </xsl:text>
					      </fo:block>
					    </fo:table-cell>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
					      	<xsl:choose>
						<xsl:when test="string-length(developmentfees/fees)>0" >
						      <xsl:value-of select="format-number(developmentfees/fees, '$#,##0.00 ')"/>
						</xsl:when>
						<xsl:otherwise> 
						   <xsl:text>$0.00 </xsl:text> 
						</xsl:otherwise>
						</xsl:choose>      
					      </fo:block>
					      </fo:table-cell>
					</fo:table-row>  
					<fo:table-row>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
						    <xsl:text>  &#160; </xsl:text>
					      </fo:block>
					    </fo:table-cell>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
						      <xsl:text> &#160;  </xsl:text>
					      </fo:block>
					      </fo:table-cell>
					</fo:table-row>  
					<fo:table-row>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
						     <xsl:text> Payments: </xsl:text>
					      </fo:block>
					    </fo:table-cell>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
					     	 <xsl:choose>
						<xsl:when test="string-length(developmentfees/payments)>0" >
						    <xsl:value-of select="format-number(developmentfees/payments, '$#,##0.00 ')"/>
					      	 </xsl:when>
						<xsl:otherwise> 
						   <xsl:text>$0.00 </xsl:text> 
						</xsl:otherwise>
						</xsl:choose>	    
					      </fo:block>
					    </fo:table-cell>
					</fo:table-row>  
					<fo:table-row>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
						    <xsl:text>  &#160; </xsl:text>
					      </fo:block>
					    </fo:table-cell>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
						      <xsl:text> &#160; </xsl:text>
					      </fo:block>
					      </fo:table-cell>
					</fo:table-row>  
					<fo:table-row>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
						     <xsl:text> Balance Due: </xsl:text>
					      </fo:block>
					    </fo:table-cell>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
					      	<xsl:choose>
						<xsl:when test="string-length(developmentfees/balance)>0" >
						     <xsl:value-of select="format-number(developmentfees/balance, '$#,##0.00 ')"/> 
						</xsl:when>
						<xsl:otherwise> 
						   <xsl:text>$0.00 </xsl:text> 
						</xsl:otherwise>
						</xsl:choose>     
					      </fo:block>
					    </fo:table-cell>
					</fo:table-row>  

						</fo:table-body>
					    </fo:table>
					</fo:block>
						
						
				<!-- Development Fees end -->
				 
				      </fo:block>
				    </fo:table-cell>
				    <fo:table-cell>
				      <fo:block font-family="sans-serif" font-size="9pt">
				<!-- Permit Fees start -->	
					<fo:block text-align="start" >
					    <fo:table table-layout="fixed" border-color="black" border-style="solid" border-width="0.5pt">
						 <fo:table-column column-width="2cm"/>
						 <fo:table-column column-width="2cm"/>
						 <fo:table-body>
					<fo:table-row>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
						    <xsl:text> Fees: </xsl:text>
					      </fo:block>
					    </fo:table-cell>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
					      	<xsl:choose>
						<xsl:when test="string-length(permitfees/fees)>0" >
						      <xsl:value-of select="format-number(permitfees/fees, '$#,##0.00 ')"/>
						</xsl:when>
						<xsl:otherwise> 
						   <xsl:text>$0.00 </xsl:text> 
						</xsl:otherwise>
						</xsl:choose>      
					      </fo:block>
					      </fo:table-cell>
					</fo:table-row>  
					<fo:table-row>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
						    <xsl:text>  &#160; </xsl:text>
					      </fo:block>
					    </fo:table-cell>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
						      <xsl:text> &#160;  </xsl:text>
					      </fo:block>
					      </fo:table-cell>
					</fo:table-row>  
					<fo:table-row>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
						     <xsl:text> Payments: </xsl:text>
					      </fo:block>
					    </fo:table-cell>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
					     	 <xsl:choose>
						<xsl:when test="string-length(permitfees/payments)>0" >
						    <xsl:value-of select="format-number(permitfees/payments, '$#,##0.00 ')"/>
					      	 </xsl:when>
						<xsl:otherwise> 
						   <xsl:text>$0.00 </xsl:text> 
						</xsl:otherwise>
						</xsl:choose>	    
					      </fo:block>
					    </fo:table-cell>
					</fo:table-row>  
					<fo:table-row>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
						    <xsl:text>  &#160; </xsl:text>
					      </fo:block>
					    </fo:table-cell>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
						      <xsl:text> &#160; </xsl:text>
					      </fo:block>
					      </fo:table-cell>
					</fo:table-row>  
					<fo:table-row>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
						     <xsl:text> Balance Due: </xsl:text>
					      </fo:block>
					    </fo:table-cell>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
					      	<xsl:choose>
						<xsl:when test="string-length(permitfees/balance)>0" >
						     <xsl:value-of select="format-number(permitfees/balance, '$#,##0.00 ')"/> 
						</xsl:when>
						<xsl:otherwise> 
						   <xsl:text>$0.00 </xsl:text> 
						</xsl:otherwise>
						</xsl:choose>     
					      </fo:block>
					    </fo:table-cell>
					</fo:table-row>  

						</fo:table-body>
					    </fo:table>
					</fo:block>
						
						
				<!-- Permit Fees end -->						
				      </fo:block>
				    </fo:table-cell>
				    <fo:table-cell>
				      <fo:block font-family="sans-serif" font-size="9pt">
				<!-- Total Fees start -->
						 
					<fo:block text-align="start" >
					    <fo:table table-layout="fixed" border-color="black" border-style="solid" border-width="0.5pt">
						 <fo:table-column column-width="3cm"/>
						 <fo:table-column column-width="3cm"/>
						 <fo:table-body>
					<fo:table-row>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
						    <xsl:text> Fees: </xsl:text>
					      </fo:block>
					    </fo:table-cell>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
					      	<xsl:choose>
						<xsl:when test="string-length(totalfees/fees)>0" >
						      <xsl:value-of select="format-number(totalfees/fees, '$#,##0.00 ')"/> 
						</xsl:when>
						<xsl:otherwise> 
						   <xsl:text>$0.00 </xsl:text> 
						</xsl:otherwise>
						</xsl:choose>      
					      </fo:block>
					      </fo:table-cell>
					</fo:table-row>  
					<fo:table-row>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
						    <xsl:text> Adjustments: </xsl:text>
					      </fo:block>
					    </fo:table-cell>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
					      	<xsl:choose>
						<xsl:when test="string-length(totalfees/adjustments)>0" >
						      <xsl:value-of select="format-number(totalfees/adjustments, '$#,##0.00 ')"/> 
					        </xsl:when>
						     <xsl:otherwise> 
							   <xsl:text>$0.00 </xsl:text> 
						     </xsl:otherwise>
						</xsl:choose>
					      </fo:block>
					      </fo:table-cell>
					</fo:table-row>  
					<fo:table-row>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
						     <xsl:text> Payments: </xsl:text>
					      </fo:block>
					    </fo:table-cell>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
					      	<xsl:choose>
						<xsl:when test="string-length(totalfees/payments)>0" >
						     <xsl:value-of select="format-number(totalfees/payments, '$#,##0.00 ')"/> 
					        </xsl:when>
						     <xsl:otherwise> 
							   <xsl:text>$0.00 </xsl:text> 
						     </xsl:otherwise>
						</xsl:choose>
					      </fo:block>
					    </fo:table-cell>
					</fo:table-row>  
					<fo:table-row>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
						    <xsl:text> Extend Credit: </xsl:text>
					      </fo:block>
					    </fo:table-cell>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
					      	<xsl:choose>
						<xsl:when test="string-length(totalfees/extend)>0" >
						     <xsl:value-of select="format-number(totalfees/extend, '$#,##0.00 ')"/> 
					        </xsl:when>
						     <xsl:otherwise> 
							   <xsl:text>$0.00 </xsl:text> 
						     </xsl:otherwise>
						</xsl:choose>
					      </fo:block>
					      </fo:table-cell>
					</fo:table-row>  

					<fo:table-row>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
						     <xsl:text> Balance Due: </xsl:text>
					      </fo:block>
					    </fo:table-cell>
					    <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="9pt" text-align="right">
					      	<xsl:choose>
						<xsl:when test="string-length(totalfees/balance)>0" >
						     <xsl:value-of select="format-number(totalfees/balance, '$#,##0.00 ')"/> 
						</xsl:when>
						<xsl:otherwise> 
						   <xsl:text>$0.00 </xsl:text> 
						</xsl:otherwise>
						</xsl:choose>     
					      </fo:block>
					    </fo:table-cell>
					</fo:table-row>  

						</fo:table-body>
					    </fo:table>
					</fo:block>
						 
						 
				<!-- Total Fees end -->						
				      </fo:block>
				    </fo:table-cell>
				 </fo:table-row>  
				 
				 
				 </fo:table-body>
			    </fo:table>
			</fo:block>
		
<!-- Plan chk, Permit Fees end -->

	
<!-- This is ledgergrp start -->
	      <fo:block font-size="11pt"  background-color="#C9C299"
		    font-family="sans-serif" 
		    line-height="16pt"
		    space-before.optimum="5pt"
		    space-after.optimum="5pt"
		    text-align="left"
		    font-weight="bold" >
		 
	      </fo:block>	
			<fo:block text-align="start" space-after.optimum="-10pt">
			    <fo:table table-layout="fixed" >
				<fo:table-column column-width="3cm"/>
				<fo:table-column column-width="7cm"/>
				<fo:table-column column-width="4cm"/>
				<fo:table-column column-width="3cm"/>
				<fo:table-body>
				  <fo:table-row>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt"
				               font-weight="bold"    >
				        Date
				        </fo:block>
				      </fo:table-cell>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt"
				               font-weight="bold"   >
				        Transaction Type
				        </fo:block>
				      </fo:table-cell>		      
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt"
				               font-weight="bold"   >
				        Method
				        </fo:block>
				      </fo:table-cell>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt"
				               font-weight="bold"   >
				        Amount
				        </fo:block>
				      </fo:table-cell>
				      
				  </fo:table-row>
		      		</fo:table-body>
		      	    </fo:table>
		      	</fo:block>

		<fo:leader leader-alignment="reference-area" leader-pattern="rule" />
			
			<fo:block text-align="start" >
			    <fo:table table-layout="fixed" >
				 <fo:table-column column-width="3cm"/>
				 <fo:table-column column-width="7cm"/>
				 <fo:table-column column-width="4cm"/>
				 <fo:table-column column-width="3cm"/>
				 <fo:table-body>
					<xsl:apply-templates select="ledgergrp"/>
				</fo:table-body>
			    </fo:table>
			</fo:block>
		<!-- ledgergrp  end -->		
		
<!-- This is conditionsgrp start -->
	<xsl:if test="string-length(conditionsgrp/conditions/condition)>0" >
	      <fo:block font-size="11pt"  background-color="#C9C299"
		    font-family="sans-serif" 
		    line-height="16pt"
		    space-before.optimum="5pt"
		    space-after.optimum="5pt"
		    text-align="left"
		    font-weight="bold" >
		Conditions
	      </fo:block>	
	</xsl:if>
		
			<fo:block text-align="start"  white-space-collapse="false">
			    <fo:table table-layout="fixed" >
				 <fo:table-column column-width="18cm"/>
				 <fo:table-body>
					<xsl:apply-templates select="conditionsgrp"/>
				</fo:table-body>
			    </fo:table>
			</fo:block>
<!-- conditionsgrp  end -->				

<!-- This is Permitsgrp start -->
<xsl:if test="string-length(permitsgrp/permit/actnbr)>0" >
	      <fo:block font-size="11pt"  background-color="#C9C299"
		    font-family="sans-serif" 
		    line-height="16pt"
		    text-align="left"
		    font-weight="bold" >
		Permits
	      </fo:block>	
	</xsl:if>	
	
			<fo:block text-align="start" space-after.optimum="-10pt">
			    <fo:table table-layout="fixed" white-space-collapse="false">
				 <fo:table-column column-width="3cm"/>
				 <fo:table-column column-width="2cm"/>
				 <fo:table-column column-width="3cm"/>
				 <fo:table-column column-width="2cm"/>
				 <fo:table-column column-width="2cm"/>
				 <fo:table-column column-width="3cm"/>
				 <fo:table-column column-width="3cm"/>

				<fo:table-header>
				  <fo:table-row>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt"
				               font-weight="bold"    >
				        Type
				        </fo:block>
				      </fo:table-cell>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt"
				               font-weight="bold"    >
				        Number
				        </fo:block>
				      </fo:table-cell>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt"
				               font-weight="bold"   >
				        Status
				        </fo:block>
				      </fo:table-cell>		      
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt"
				               font-weight="bold"   >
				        Issued Date
				        </fo:block>
				      </fo:table-cell>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt"
				               font-weight="bold"   >
				        Comp. Date
				        </fo:block>
				      </fo:table-cell>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt"
				               font-weight="bold"    >
				        Payments
				        </fo:block>
				      </fo:table-cell>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt"
				                font-weight="bold"  >
				        Balance Due
				        </fo:block>
				      </fo:table-cell>
				  </fo:table-row>
		      		</fo:table-header>
				 <fo:table-body>
					<xsl:apply-templates select="permitsgrp"/>
				</fo:table-body>
			    </fo:table>
			</fo:block>
<!-- permitsgrp  end -->				


<!-- This is Inspectionsgrp start -->
<xsl:if test="string-length(inspectionsgrp/inspection/item)>0" >
	      <fo:block font-size="11pt"  background-color="#C9C299"
		    font-family="sans-serif" 
		    line-height="16pt"
		    space-before.optimum="15pt"
		    space-after.optimum="5pt"
		    text-align="left"
		    font-weight="bold" >
		Inspections
	      </fo:block>	
			<fo:block text-align="start">
			    <fo:table table-layout="fixed" >
				 <fo:table-column column-width="1cm"/>
				 <fo:table-column column-width="6cm"/>
				 <fo:table-column column-width="2cm"/>
				 <fo:table-column column-width="2cm"/>
				 <fo:table-column column-width="4.5cm"/>
				 <fo:table-column column-width="3.5cm"/>

				<fo:table-header>
				  <fo:table-row  >
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt"
				               font-weight="bold"  space-after.optimum="5pt"  >
				        Item
				        </fo:block>
				      </fo:table-cell>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt"
				               font-weight="bold"    >
				        Description
				        </fo:block>
				      </fo:table-cell>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt"
				               font-weight="bold"   >
				        Permit
				        </fo:block>
				      </fo:table-cell>		      
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt"
				               font-weight="bold"   >
				        Date
				        </fo:block>
				      </fo:table-cell>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt"
				               font-weight="bold"   >
				        Action
				        </fo:block>
				      </fo:table-cell>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt"
				               font-weight="bold"    >
				        Inspector
				        </fo:block>
				      </fo:table-cell>
				  </fo:table-row>
		      		</fo:table-header>
				 <fo:table-body>
					<xsl:apply-templates select="inspectionsgrp"/>
				</fo:table-body>
			    </fo:table>
			</fo:block>
	</xsl:if>	
<!-- inspectionsgrp  end -->				
 			
<!-- This is planningStatusGroup start -->
<xsl:if test="string-length(planningstatusgroup/planningstatus/statusdate)>0" >
	   <fo:block font-size="11pt"  background-color="#C9C299"
		    font-family="sans-serif" 
		    line-height="16pt"
		    space-before.optimum="15pt"
		    space-after.optimum="5pt"
		    text-align="left"
		    font-weight="bold" >
		Planning Status
	    </fo:block>	
	
	    <fo:block text-align="start">
		<fo:table table-layout="fixed" >
		     <fo:table-column column-width="3cm"/>
		     <fo:table-column column-width="16cm"/>

		    <fo:table-header>
		      <fo:table-row  >
			  <fo:table-cell>
			    <fo:block font-family="sans-serif" font-size="9pt"
				   font-weight="bold"  space-after.optimum="5pt"  >
			    Date
			    </fo:block>
			  </fo:table-cell>
			  <fo:table-cell>
			    <fo:block font-family="sans-serif" font-size="9pt"
				   font-weight="bold"    >
			    Status
			    </fo:block>
			  </fo:table-cell>
		      </fo:table-row>
		    </fo:table-header>
		     <fo:table-body>
			    <xsl:apply-templates select="planningstatusgroup"/>
		    </fo:table-body>
		</fo:table>
	    </fo:block>
</xsl:if>	

<!-- planningresolutiongroup  end -->				

<!-- This is planningResolutionGroup start -->
<xsl:if test="string-length(planningresolutiongroup/planningresolution/resolutionnbr)>0" >
	   <fo:block font-size="11pt"  background-color="#C9C299"
		    font-family="sans-serif" 
		    line-height="16pt"
		    space-before.optimum="15pt"
		    space-after.optimum="5pt"
		    text-align="left"
		    font-weight="bold" >
		Planning Resolutions
	    </fo:block>	
	
	    <fo:block text-align="start">
		<fo:table table-layout="fixed" >
		     <fo:table-column column-width="3cm"/>
		     <fo:table-column column-width="13cm"/>
		     <fo:table-column column-width="3cm"/>

		    <fo:table-header>
		      <fo:table-row  >
			  <fo:table-cell>
			    <fo:block font-family="sans-serif" font-size="9pt"
				   font-weight="bold"  space-after.optimum="5pt"  >
			    Resolution #
			    </fo:block>
			  </fo:table-cell>
			  <fo:table-cell>
			    <fo:block font-family="sans-serif" font-size="9pt"
				   font-weight="bold"    >
			    Description
			    </fo:block>
			  </fo:table-cell>
			  <fo:table-cell>
			    <fo:block font-family="sans-serif" font-size="9pt"
				   font-weight="bold"    >
			    Date Approved
			    </fo:block>
			  </fo:table-cell>
			</fo:table-row>
		    </fo:table-header>
		     <fo:table-body>
			    <xsl:apply-templates select="planningresolutiongroup"/>
		    </fo:table-body>
		</fo:table>
	    </fo:block>
</xsl:if>	

<!-- planningresolutiongroup  end -->				

<!-- This is planningEnvReviewGroup start -->
<xsl:if test="string-length(planningenvreviewgroup/submitteddate)>0" >
	   <fo:block font-size="11pt"  background-color="#C9C299"
		    font-family="sans-serif" 
		    line-height="16pt"
		    space-before.optimum="15pt"
		    space-after.optimum="5pt"
		    text-align="left"
		    font-weight="bold" >
		Planning Environmental Review
	    </fo:block>	
	
	    <fo:block text-align="start">

		<fo:table table-layout="fixed" >
		     <fo:table-column column-width="5cm"/>
		     <fo:table-column column-width="4cm"/>
		     <fo:table-column column-width="5cm"/>
		     <fo:table-column column-width="4cm"/>

		    <fo:table-body>
		      <fo:table-row  >
			  <fo:table-cell>
			    <fo:block font-family="sans-serif" font-size="9pt"
				   font-weight="bold"  space-after.optimum="5pt"  >
			    Application Submittal Date
			    </fo:block>
			  </fo:table-cell>
			  <fo:table-cell>
			    <fo:block font-family="sans-serif" font-size="9pt"
				   font-weight="bold"    >
			    	<xsl:value-of select="planningenvreviewgroup/submitteddate"/>
			    </fo:block>
			  </fo:table-cell>
			  <fo:table-cell>
			    <fo:block font-family="sans-serif" font-size="9pt"
				   font-weight="bold"    >
			    Determination Date
			    </fo:block>
			  </fo:table-cell>
			  <fo:table-cell>
			    <fo:block font-family="sans-serif" font-size="9pt"
				   font-weight="bold"    >
			    	<xsl:value-of select="planningenvreviewgroup/determinationdate"/>
			    </fo:block>
			  </fo:table-cell>			  
			</fo:table-row>
		      <fo:table-row  >
			  <fo:table-cell>
			    <fo:block font-family="sans-serif" font-size="9pt"
				   font-weight="bold"  space-after.optimum="5pt"  >
			    Req Determination Date
			    </fo:block>
			  </fo:table-cell>
			  <fo:table-cell>
			    <fo:block font-family="sans-serif" font-size="9pt"
				   font-weight="bold"    >
			    	<xsl:value-of select="planningenvreviewgroup/reqdeterminationdate"/>
			    </fo:block>
			  </fo:table-cell>
			  <fo:table-cell>
			    <fo:block font-family="sans-serif" font-size="9pt"
				   font-weight="bold"  space-after.optimum="5pt"  >
			    
			    </fo:block>
			  </fo:table-cell>
			  <fo:table-cell>
			    <fo:block font-family="sans-serif" font-size="9pt"
				   font-weight="bold"    >
			    	
			    </fo:block>
			  </fo:table-cell>
		      </fo:table-row>
		    </fo:table-body>
		</fo:table>

		<fo:table table-layout="fixed" >
		     <fo:table-column column-width="3cm"/>
		     <fo:table-column column-width="8cm"/>
		     <fo:table-column column-width="8cm"/>

		    <fo:table-header>
		      <fo:table-row  >
			  <fo:table-cell>
			    <fo:block font-family="sans-serif" font-size="9pt"
				   font-weight="bold"  space-after.optimum="5pt"  >
			    Action Date
			    </fo:block>
			  </fo:table-cell>
			  <fo:table-cell>
			    <fo:block font-family="sans-serif" font-size="9pt"
				   font-weight="bold"    >
			    Env Determination
			    </fo:block>
			  </fo:table-cell>
			  <fo:table-cell>
			    <fo:block font-family="sans-serif" font-size="9pt"
				   font-weight="bold"    >
			    Action
			    </fo:block>
			  </fo:table-cell>
			</fo:table-row>
		    </fo:table-header>
		     <fo:table-body>
			    <xsl:apply-templates select="planningenvreviewgroup"/>
		    </fo:table-body>
		</fo:table>
	    </fo:block>
</xsl:if>	

<!-- planningenvreviewgroup  end -->	

<!-- This is BusTaxGrp start -->
<!--<xsl:if test="string-length(bustaxgrp/bustax/licno)>0" >-->
	      <fo:block font-size="11pt"  background-color="#C9C299" font-family="sans-serif" line-height="16pt"
		    space-before.optimum="10pt" space-after.optimum="10pt" text-align="left" font-weight="bold" > Business Tax </fo:block>	
	
			<fo:block text-align="start" white-space-collapse="false" space-after.optimum="-10pt">
			    <fo:table table-layout="fixed" >
				 <fo:table-column column-width="2cm"/>
				 <fo:table-column column-width="6cm"/>
				 <fo:table-column column-width="2cm"/>
				 <fo:table-column column-width="2.5cm"/>
				 <fo:table-column column-width="2cm"/>
				 <fo:table-column column-width="2cm"/>
				 <fo:table-column column-width="2cm"/>

				<fo:table-header space-after.optimum="5pt">
				  <fo:table-row>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt" font-weight="bold">License No</fo:block>
				      </fo:table-cell>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt" font-weight="bold">Name</fo:block>
				      </fo:table-cell>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt" font-weight="bold">Primary</fo:block>
				      </fo:table-cell>		      
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt" font-weight="bold">Valuation</fo:block>
				      </fo:table-cell>		      
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt"  font-weight="bold">Tax</fo:block>
				      </fo:table-cell>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt" font-weight="bold">Paid</fo:block>
				      </fo:table-cell>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt" font-weight="bold">Balance Due
				        </fo:block>
				      </fo:table-cell>
				  </fo:table-row>
		        </fo:table-header>
		      	
		      	<fo:table-body>
					<xsl:apply-templates select="bustaxgrp"/>
				</fo:table-body>
			    
			  </fo:table>				
		      	</fo:block>
<!--</xsl:if>--> 			     
<!-- BusTaxGrp  end -->	 			

	</fo:block>	
</xsl:template>

<xsl:template match="activehold">
		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="created"/>
		              </fo:block>
		            </fo:table-cell>
		            
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="type"/>
		              </fo:block>
		            </fo:table-cell>
		            
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="issuer"/>
		              </fo:block>
		            </fo:table-cell>
	              
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="status"/>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="statusdate"/>
		              </fo:block>
		            </fo:table-cell>
		          </fo:table-row>
		          <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
		              </fo:block>
		            </fo:table-cell>
                    <fo:table-cell number-columns-spanned="4">
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="description"/>
		              </fo:block>
		            </fo:table-cell>
 	             </fo:table-row>

</xsl:template>
 
<xsl:template match="comments">
		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="comments"/>
		              </fo:block>
		            </fo:table-cell>
		        </fo:table-row>  
</xsl:template>

<xsl:template match="fees">
		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="feedesc"/>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="units"/>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
		              	<xsl:if test="string-length(feeperunits)>0" >
			               <xsl:value-of select="format-number(feeperunits, '$#,##0.00 ')"/>  
			        </xsl:if>       
		              </fo:block>
		            </fo:table-cell>		            
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
		              	<xsl:if test="string-length(amount)>0" >
		           	   <xsl:value-of select="format-number(amount, '$#,##0.00 ')"/> 
		           	</xsl:if>   
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
		              	<xsl:if test="string-length(paid)>0" >
			                <xsl:value-of select="format-number(paid, '$#,##0.00 ')"/> 
			        </xsl:if>        
		              </fo:block>
		            </fo:table-cell>
		          </fo:table-row>
</xsl:template>

<xsl:template match="ledger">
		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:if test="string-length(pymntdate)>0" >
			              	 <xsl:value-of select="substring(pymntdate,6,2)" /> <xsl:text>/</xsl:text>  <xsl:value-of select="substring(pymntdate,9,2)" /><xsl:text>/</xsl:text><xsl:value-of select="substring(pymntdate, 1,4)" />
			              </xsl:if>				              
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="pymntdesc"/>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="pymntmethod"/>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
		              	<xsl:if test="string-length(pymntamnt)>0" >
			              <xsl:value-of select="format-number(pymntamnt, '$#,##0.00 ')"/>   
			        </xsl:if>      
		              </fo:block>
		            </fo:table-cell>	
		        </fo:table-row>  
</xsl:template>
 
<xsl:template match="conditions">
		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="condition"/>
		              </fo:block>
		            </fo:table-cell>
		        </fo:table-row>  
</xsl:template>

<xsl:template match="permit">
		        <fo:table-row >
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="acttype"/>
		              </fo:block>
 		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="actnbr"/>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="actstatus"/>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:if test="string-length(issdate)>0" >
			              	 <xsl:value-of select="substring(issdate,6,2)" /> <xsl:text>/</xsl:text>  <xsl:value-of select="substring(issdate,9,2)" /><xsl:text>/</xsl:text><xsl:value-of select="substring(issdate, 1,4)" />
			              </xsl:if>				              
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:if test="string-length(compdate)>0" >
			              	 <xsl:value-of select="substring(compdate,6,2)" /> <xsl:text>/</xsl:text>  <xsl:value-of select="substring(compdate,9,2)" /><xsl:text>/</xsl:text><xsl:value-of select="substring(compdate, 1,4)" />
			              </xsl:if>				              
		              </fo:block>
		            </fo:table-cell>
		            
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
		              	<xsl:if test="string-length(paidamt)>0" >
			              <xsl:value-of select="format-number(paidamt, '$#,##0.00 ')"/>   
			        </xsl:if>      
		              </fo:block>
		            </fo:table-cell>	
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
		              	<xsl:if test="string-length(amtdue)>0" >
			              <xsl:value-of select="format-number(amtdue, '$#,##0.00 ')"/>   
			        </xsl:if>      
		              </fo:block>
		            </fo:table-cell>	
		        </fo:table-row>  
		        <fo:table-row >
		            <fo:table-cell number-columns-spanned="7">
		              <fo:block font-family="sans-serif" font-size="9pt"   space-after.optimum="5pt">
			             <xsl:text> &#160; </xsl:text> <xsl:text> &#160; </xsl:text>  <fo:inline  font-weight="bold"><xsl:value-of select="pcreq"/> </fo:inline><xsl:text> &#160; </xsl:text> <xsl:text> PERMIT TO DO: </xsl:text>  <fo:inline><xsl:value-of select="permittodo"/> </fo:inline>
		              </fo:block>
		            </fo:table-cell>
		        </fo:table-row>  
		        
</xsl:template>

<xsl:template match="inspection">
		        <fo:table-row >
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="item"/>
		              </fo:block>
 		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="description"/>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="permit"/>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:if test="string-length(inspdate)>0" >
			              	 <xsl:value-of select="substring(inspdate,6,2)" /><xsl:text>/</xsl:text>
			              	 <xsl:value-of select="substring(inspdate,9,2)" /><xsl:text>/</xsl:text>
			              	 <xsl:value-of select="substring(inspdate, 1,4)" />
			              </xsl:if>				              
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="action"/>
		              </fo:block>
 		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="inspector"/>
		              </fo:block>
		            </fo:table-cell>
		        </fo:table-row>  
		        <fo:table-row >
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			               
		              </fo:block>
		            </fo:table-cell>		        
		            <fo:table-cell number-columns-spanned="5">
		              <fo:block font-family="sans-serif" font-size="9pt" space-after.optimum="5pt">
			             <xsl:value-of select="cmntstring"/> 
		              </fo:block>
		            </fo:table-cell>
		        </fo:table-row>  
</xsl:template>

<xsl:template match="planningstatus">
		        <fo:table-row >
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="statusdate"/>
		              </fo:block>
 		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="status"/>
		              </fo:block>
		            </fo:table-cell>
		        </fo:table-row>  
		        <fo:table-row >		        
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			               
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt"   space-after.optimum="5pt">
			             <xsl:value-of select="commentdata"/> 
		              </fo:block>
		            </fo:table-cell>
		        </fo:table-row>		        
</xsl:template>

<xsl:template match="planningresolution">
		        <fo:table-row >
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="resolutionnbr"/>
		              </fo:block>
 		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt" space-after.optimum="5pt">
			              <xsl:value-of select="description"/>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt" space-after.optimum="5pt">
			             <xsl:value-of select="resolutiondate"/> 
		              </fo:block>
		            </fo:table-cell>
		        </fo:table-row>  
</xsl:template>

<xsl:template match="planningenvreview">
		        <fo:table-row >
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="actiondate"/>
		              </fo:block>
 		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="determination"/>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt"  space-after.optimum="5pt">
			              <xsl:value-of select="action"/>
		              </fo:block>
		            </fo:table-cell>
		        </fo:table-row>  
		        <fo:table-row >		        
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			               
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell number-columns-spanned="2">
		              <fo:block font-family="sans-serif" font-size="9pt"   space-after.optimum="9pt">
			             <xsl:value-of select="commentdata"/> 
		              </fo:block>
		            </fo:table-cell>
		        </fo:table-row>		        
</xsl:template>

<xsl:template match="bustax">
		        <fo:table-row >
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="licno"/>
		              </fo:block>
 		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="name"/>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		                <xsl:if test="primary='Y'">
				            <fo:block>
				                <fo:external-graphic src="/opt/obcdata/xsl/signedoff.jpg" content-width="0.3cm"
					                                 content-height="0.3cm" width="0.3cm" height="0.3cm"/>
			 	            </fo:block>
			 	        </xsl:if>   
		                <xsl:if test="primary='N'">
				            <fo:block>
				               <fo:external-graphic src="/opt/obcdata/xsl/empty.jpg" content-width="0.3cm"
					                                 content-height="0.3cm" width="0.3cm" height="0.3cm"/>
			 	            </fo:block>
				        </xsl:if>   
		            </fo:table-cell>
  		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
		              	<xsl:if test="string-length(valuation)>0" >
			              <xsl:value-of select="format-number(valuation, '$#,##0.00 ')"/>   
			        </xsl:if>      
		              </fo:block>
		            </fo:table-cell>	
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
		              	<xsl:if test="string-length(tax)>0" >
			              <xsl:value-of select="format-number(tax, '$#,##0.00 ')"/>   
			        </xsl:if>      
		              </fo:block>
		            </fo:table-cell>	
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
		              	<xsl:if test="string-length(paid)>0" >
			              <xsl:value-of select="format-number(paid, '$#,##0.00 ')"/>   
			        </xsl:if>      
		              </fo:block>
		            </fo:table-cell>	
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
		              	<xsl:if test="string-length(due)>0" >
			              <xsl:value-of select="format-number(due, '$#,##0.00 ')"/>   
			        </xsl:if>      
		              </fo:block>
		            </fo:table-cell>	
		        </fo:table-row>  
		        
		        <fo:table-row >
		        
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt"></fo:block>
		            </fo:table-cell>			        
		            <fo:table-cell >
		              <fo:block font-family="sans-serif" font-size="9pt"   space-after.optimum="5pt">
			            <xsl:text> Permit No: </xsl:text> <xsl:value-of select="permitno"/>
		              </fo:block>
		            </fo:table-cell>
		            
		            <fo:table-cell >
		              <fo:block font-family="sans-serif" text-align="end" font-size="9pt"   space-after.optimum="5pt">
			               <xsl:text> Work desc: </xsl:text>  
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell number-columns-spanned="4">
		              <fo:block font-family="sans-serif" font-size="9pt"   space-after.optimum="5pt">
			              <xsl:value-of select="workdesc"/>
		              </fo:block>
		            </fo:table-cell>		            
		        </fo:table-row>  
</xsl:template>

<xsl:template match="owner">
	<fo:block space-before.optimum="5pt" text-align="start" >
	    <fo:table table-layout="fixed" >
		 <fo:table-column column-width="2cm"/>
		 <fo:table-column column-width="8cm"/>
		 <fo:table-body>
		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Owner </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="name"/> <xsl:value-of select="address"/>
		              </fo:block>
		            </fo:table-cell>
		        </fo:table-row>
		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Address </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="addr"/>
		              </fo:block>
		            </fo:table-cell>
		        </fo:table-row>
		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Phone </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>(</xsl:text><xsl:value-of select="substring(phone, 1, 3)" /> <xsl:text>)</xsl:text>  <xsl:value-of select="substring(phone, 4, 3)" /><xsl:text>-</xsl:text><xsl:value-of select="substring(phone, 7, 4)" />  
		              </fo:block>
		            </fo:table-cell>
		        </fo:table-row>		        
		 </fo:table-body>
	    </fo:table>
	</fo:block>		          
</xsl:template>

<xsl:template match="people">
		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			               <xsl:value-of select="peopletype"/>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="licno"/> 
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="name"/>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			            <xsl:text>(</xsl:text><xsl:value-of select="substring(phone, 1, 3)" /> <xsl:text>)</xsl:text>  <xsl:value-of select="substring(phone, 4, 3)" /><xsl:text>-</xsl:text><xsl:value-of select="substring(phone, 7, 4)" />
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="actnbr"/>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="acttype"/>
		              </fo:block>
		            </fo:table-cell>
		        </fo:table-row>		          
</xsl:template>
<xsl:template match="applicant">
		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Applicant </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="name"/>
		              </fo:block>
		            </fo:table-cell>
		          </fo:table-row>
		          
</xsl:template>

<xsl:template match="main/address">
	<fo:block space-before.optimum="5pt" text-align="start" >
	    <fo:table table-layout="fixed" >
		 <fo:table-column column-width="4cm"/>
		 <fo:table-column column-width="15cm"/>
		 <fo:table-body>
		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Base Address </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="."/> 
		              </fo:block>
		            </fo:table-cell>
		        </fo:table-row>
		 </fo:table-body>
	    </fo:table>
	</fo:block>		          
</xsl:template>
<xsl:template match="main/valuation">
	<fo:block space-before.optimum="5pt" text-align="start" >
	    <fo:table table-layout="fixed" >
		 <fo:table-column column-width="4cm"/>
		 <fo:table-column column-width="15cm"/>
		 <fo:table-body>
		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Valuation </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
		              	<xsl:if test="string-length(.)>0" >
			                <xsl:value-of select="format-number(., '$#,##0.00 ')"/>
			        </xsl:if>        
		              </fo:block>
		            </fo:table-cell>
		        </fo:table-row>
		 </fo:table-body>
	    </fo:table>
	</fo:block>		          
</xsl:template>
<xsl:template match="main/actdesc">
	<fo:block space-before.optimum="5pt" text-align="start" >
	    <fo:table table-layout="fixed" >
		 <fo:table-column column-width="4cm"/>
		 <fo:table-column column-width="15cm"/>
		 <fo:table-body>
		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Permit to do </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="."/> 
		              </fo:block>
		            </fo:table-cell>
		        </fo:table-row>
		 </fo:table-body>
	    </fo:table>
	</fo:block>		          
</xsl:template>

<xsl:template match="main">
	<fo:block text-align="start" line-height="15pt">
	    <fo:table table-layout="fixed" >
		 <fo:table-column column-width="3cm"/>
		 <fo:table-column column-width="4cm"/>
		 <fo:table-column column-width="2cm"/>
		 <fo:table-column column-width="4cm"/>
		 <fo:table-column column-width="2cm"/>
		 <fo:table-column column-width="4cm"/>		 
		 <fo:table-body>
		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Permit No </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			                <xsl:text>:&#160;</xsl:text> 
			                <fo:inline font-weight="bold">
				                <xsl:value-of select="actnbr"/>
				            </fo:inline>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Project No </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			                <xsl:text>:&#160;</xsl:text> <xsl:value-of select="prjnbr"/>
		              </fo:block>
		            </fo:table-cell>		            
 		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Type </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			                <xsl:text>:&#160;</xsl:text> <xsl:value-of select="acttype"/>
		              </fo:block>
		            </fo:table-cell>
		          </fo:table-row>

		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Address </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			                <xsl:text>:&#160;</xsl:text>
			                <fo:inline font-weight="bold">
			                	<xsl:value-of select="address"/>
			                </fo:inline>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>&#160; </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			                
		              </fo:block>
		            </fo:table-cell>		            
 		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Status </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			                <xsl:text>:&#160;</xsl:text> <xsl:value-of select="actstatus"/>
		              </fo:block>
		            </fo:table-cell>
		          </fo:table-row>

		        <fo:table-row>
	            
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Plan Rev. Engineer</xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		            <fo:block font-family="sans-serif" font-size="9pt">
			                <xsl:text>:&#160;</xsl:text>
			                <fo:inline>
			                	<xsl:value-of select="//processteam/planreviewengineer"/> 
			                </fo:inline>
			               </fo:block> 
		            </fo:table-cell>

		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt" wrap-option="no-wrap">
			              <xsl:text>Permit Tech </xsl:text>  
		              </fo:block>
		            </fo:table-cell>		            
 		            <fo:table-cell>
	 		            <fo:block font-family="sans-serif" font-size="9pt">
 				            <xsl:text>:&#160;</xsl:text> <xsl:value-of select="//processteam/permittech"/>
 				        </fo:block>
		            </fo:table-cell>
		             <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt" text-align="end">
			              <xsl:text>To Expire &#160;</xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>: &#160;</xsl:text> 
			              <xsl:if test="string-length(expiredate)>0" >
			              	 <xsl:value-of select="substring(expiredate,6,2)" /> <xsl:text>/</xsl:text>  <xsl:value-of select="substring(expiredate,9,2)" /><xsl:text>/</xsl:text><xsl:value-of select="substring(expiredate, 1,4)" />
			              </xsl:if>				                
		              </fo:block>
		            </fo:table-cell>
		          </fo:table-row>
		</fo:table-body>
	    </fo:table>
	</fo:block>
	
	<fo:block text-align="start" line-height="15pt">
	    <fo:table table-layout="fixed" >
		 <fo:table-column column-width="3cm"/>
		 <fo:table-column column-width="2cm"/>
		 <fo:table-column column-width="2cm"/>
		 <fo:table-column column-width="2cm"/>
		 <fo:table-column column-width="2cm"/>
		 <fo:table-column column-width="2cm"/>		 
		 <fo:table-column column-width="2cm"/>	
		 <fo:table-column column-width="2cm"/>	
		 <fo:table-body>
		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Applied </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>: &#160;</xsl:text> 
			              <xsl:if test="string-length(applieddate)>0" >
			              	 <xsl:value-of select="substring(applieddate,6,2)" /> <xsl:text>/</xsl:text>  <xsl:value-of select="substring(applieddate,9,2)" /><xsl:text>/</xsl:text><xsl:value-of select="substring(applieddate, 1,4)" />
			              </xsl:if>				                
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt" text-align="end">
			              <xsl:text>Issued  &#160;</xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>: &#160;</xsl:text> 
			              <xsl:if test="string-length(issueddate)>0" >
			              	 <xsl:value-of select="substring(issueddate,6,2)" /> <xsl:text>/</xsl:text>  <xsl:value-of select="substring(issueddate,9,2)" /><xsl:text>/</xsl:text><xsl:value-of select="substring(issueddate, 1,4)" />
			              </xsl:if>				                
		              </fo:block>
		            </fo:table-cell>
		            
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Owner&#160; </xsl:text>
			              <xsl:text>&#160;&#160;:</xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt" wrap-option="no-wrap">
		              
			              <xsl:value-of select="../owner/name"/> <xsl:value-of select="../owner/address"/>
		              </fo:block>
		            </fo:table-cell>
		            
		            
		            <!--<fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt" text-align="end">
			              <xsl:text>Completed &#160;</xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>: &#160;</xsl:text> 
			              <xsl:if test="string-length(completeddate)>0" >
			              	 <xsl:value-of select="substring(completeddate,6,2)" /> <xsl:text>/</xsl:text>  <xsl:value-of select="substring(completeddate,9,2)" /><xsl:text>/</xsl:text><xsl:value-of select="substring(completeddate, 1,4)" />
			              </xsl:if>				                
		              </fo:block>
		            </fo:table-cell>		            
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt" text-align="end">
			              <xsl:text>To Expire &#160;</xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>: &#160;</xsl:text> 
			              <xsl:if test="string-length(expiredate)>0" >
			              	 <xsl:value-of select="substring(expiredate,6,2)" /> <xsl:text>/</xsl:text>  <xsl:value-of select="substring(expiredate,9,2)" /><xsl:text>/</xsl:text><xsl:value-of select="substring(expiredate, 1,4)" />
			              </xsl:if>				                
		              </fo:block>
		            </fo:table-cell>-->		            
		        </fo:table-row>

		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Valuation </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			                <xsl:text>: &#160;</xsl:text> 
					<xsl:if test="string-length(valuation)>0" >
						<xsl:value-of select="format-number(valuation, '$#,##0.00 ')"/>
					</xsl:if>  			                
		              </fo:block>
		            </fo:table-cell>
		            
		             <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt" text-align="end">
			              <xsl:text>Completed</xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;: &#160;</xsl:text> 
			              <xsl:if test="string-length(completeddate)>0" >
			              	 <xsl:value-of select="substring(completeddate,6,2)" /> <xsl:text>/</xsl:text>  <xsl:value-of select="substring(completeddate,9,2)" /><xsl:text>/</xsl:text><xsl:value-of select="substring(completeddate, 1,4)" />
			              </xsl:if>				                
		              </fo:block>
		            </fo:table-cell>		            
		            
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Address&#160;: </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt" wrap-option="no-wrap">
		              
			              <xsl:value-of select="../owner/addr"/>
		              </fo:block>
		            </fo:table-cell>	
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text> </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			                
		              </fo:block>
		            </fo:table-cell>			            
		          </fo:table-row>
		          
		          	<fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			               <xsl:text>Project Name </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			                <xsl:text>: &#160;</xsl:text> 
					  <xsl:value-of select="prjname"/>		                
		              </fo:block>
		            </fo:table-cell>
		            
		             <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt" text-align="end">
			              <xsl:text> &#160;</xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text></xsl:text> 
			              			                
		              </fo:block>
		            </fo:table-cell>		            
		            
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Phone&#160;&#160;&#160;&#160;: </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
		              
			                 <xsl:text>(</xsl:text><xsl:value-of select="substring(../owner/phone, 1, 3)" /> <xsl:text>)</xsl:text>  <xsl:value-of select="substring(../owner/phone, 4, 3)" /><xsl:text>-</xsl:text><xsl:value-of select="substring(../owner/phone, 7, 4)" />  
		            </fo:block>
		            </fo:table-cell>	
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text> </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			                
		              </fo:block>
		            </fo:table-cell>			            
		          </fo:table-row>
		          
		          
		          
		          

		       
		          
		          <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Description </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell number-columns-spanned="8">
		              <fo:block font-family="sans-serif" font-size="9pt" wrap-option="no-wrap">
			                <xsl:text>: &#160;</xsl:text> <xsl:value-of select="prjdescription"/>
		              </fo:block>
		            </fo:table-cell>
                  </fo:table-row>
		</fo:table-body>
	    </fo:table>
	</fo:block>	
</xsl:template>
 
<xsl:template match="assessor">
	<fo:block space-before.optimum="5pt" text-align="start" >
	    <fo:table table-layout="fixed" >
		 <fo:table-column column-width="4cm"/>
		 <fo:table-column column-width="5cm"/>
		 <fo:table-column column-width="4cm"/>
		 <fo:table-column column-width="5cm"/>
		 
		 <fo:table-body>
		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Data1SqFt.: </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="data1sqft"/>  
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Zoning Code : </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="zoningcode"/>  
		              </fo:block>
		            </fo:table-cell>

		        </fo:table-row>
		 </fo:table-body>
	    </fo:table>
	</fo:block>		          
</xsl:template> 

</xsl:stylesheet>


