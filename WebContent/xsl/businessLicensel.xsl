<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" version="1.0"> 
	<xsl:template match="root">   
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
			<fo:layout-master-set>
				<fo:simple-page-master master-name="all" page-height="11.0in" page-width="8.5in" margin-top="0.3in" margin-bottom="0.3in" margin-left="0.5in" margin-right="0.5in">
					<fo:region-body margin-top="1.5in" margin-bottom="1.5in"/>
					<fo:region-before extent="2.2in"/> 
					<fo:region-after extent="1.6in"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-reference="all">
				<fo:static-content flow-name="xsl-region-before">
				</fo:static-content>
				<fo:flow flow-name="xsl-region-body">
					<xsl:apply-templates select="data"/>
					<fo:block id="last-page"/>
					<fo:block-container height="6cm" width="20cm" top="15.9cm" left="0.45cm" position="absolute"> 
						<fo:block text-align="start" margin-left=".3cm" font-size="9pt" font-weight="bold" space-after="1.8cm"> 
							<fo:block text-align="start" margin-left="13.4cm"> 
							<xsl:text> Business License&#160; </xsl:text>
							<xsl:value-of select="data/businessLicenseGroup/businessLicense/ACCOUNT_NUMBER"/>   
							</fo:block>
								<fo:block><xsl:text>&#xA;</xsl:text></fo:block>
							<xsl:text> FISCAL YEAR&#160;&#160; </xsl:text>
							<xsl:value-of select="data/businessLicenseGroup/FisicalYear/START_YEAR"/>
								<xsl:text> - </xsl:text>
							<xsl:value-of select="data/businessLicenseGroup/FisicalYear/END_YEAR"/> 
							<xsl:text> &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; 
									   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
									   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
									   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
									   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; 
									   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; 
									   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
									   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
									   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
									   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
									   &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; </xsl:text>
							<xsl:value-of select="data/businessLicenseGroup/businessLicense/CLASS_CODE"/>   
								<xsl:text> &#160; </xsl:text>
							<!-- <xsl:value-of select="data/businessLicenseGroup/businessLicense/DECAL_CODE"/> -->
								<fo:block><xsl:text>&#xA;</xsl:text></fo:block>
								<fo:block><xsl:text>&#xA;</xsl:text></fo:block>
							<xsl:text> EXPIRES&#160;&#160;&#160;&#160;&#160;&#160; </xsl:text>
							<xsl:value-of select="data/businessLicenseGroup/businessLicense/EXPIRATION_DATE"/>
						</fo:block>
						<fo:block text-align="start" margin-left=".6cm"  space-after="1.1cm" line-height="18pt" font-size="8.6pt" font-weight="bold"> 
							<xsl:value-of select="data/businessLicenseGroup/businessLicense/BUSINESS_TYPE"/>   
						</fo:block>
						<fo:block text-align="start" margin-left=".6cm" font-size="8.6pt" font-weight="bold"> 
							<xsl:value-of select="data/businessLicenseGroup/businessLicense/BUSINESS_NAME"/>   
						</fo:block>
						<fo:block text-align="start" margin-left=".6cm" font-size="8.6pt" font-weight="bold"> 
							<xsl:value-of select="data/businessLicenseGroup/businessLicense/BUSINESS_ADDRESS"/>   
						</fo:block>
						<fo:block text-align="start" margin-left=".6cm" font-size="8.6pt" font-weight="bold"> 
							<xsl:value-of select="data/businessLicenseGroup/businessLicense/CITYSTATEZIP"/>   
						</fo:block>		
					</fo:block-container>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
	<xsl:template match="data">
		<fo:block text-align="start" font-size="12pt" font-family="sans-serif">
			<xsl:if test="string-length(businessLicenseGroup/businessLicense/conditionsgrp/conditions/condition)>0" >
				<fo:block font-size="11pt" font-family="sans-serif" space-before.optimum="5pt" space-after.optimum="5pt" text-align="left" font-weight="bold" >
					<fo:list-block  white-space-collapse="false" provisional-label-separation="5mm">
						<xsl:apply-templates select="businessLicenseGroup/businessLicense/conditionsgrp"/>
					</fo:list-block>
				</fo:block>
			</xsl:if> 
		</fo:block>
	</xsl:template>
	<xsl:template match="conditions">
		<fo:list-item>
			<fo:list-item-label end-indent="label-end()">
				<fo:block font-family="sans-serif" font-size="9pt">
				</fo:block>					  
			</fo:list-item-label>
			<fo:list-item-body start-indent="5mm">
				<fo:block font-family="sans-serif" font-size="9pt"  space-after.optimum="5pt">
					<xsl:value-of select="condition"/>
				</fo:block>
			</fo:list-item-body>
		</fo:list-item>
	</xsl:template>
</xsl:stylesheet>	 