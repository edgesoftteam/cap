<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	version="1.0"> 
<xsl:template match="root">
<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">

	<fo:layout-master-set>
	    <fo:simple-page-master master-name="all"
			page-height="11.5in" page-width="8.5in"
		margin-top="0.5in" margin-bottom="0.5in" 
		margin-left="0.5in" margin-right="0.5in">
			<fo:region-body margin-top=".75in" margin-bottom=".5in"/>
			<fo:region-before extent=".75in"/>
			<fo:region-after extent=".5in"/>
	    </fo:simple-page-master>
	</fo:layout-master-set>
	<fo:page-sequence master-reference="all" >

   <fo:static-content flow-name="xsl-region-before">
	<fo:block text-align="start">
	    <fo:table border="1" table-layout="fixed">
	    <fo:table-column column-width="19cm"/>
		 <fo:table-body>
		        <fo:table-row>
		            <fo:table-cell>
              				<fo:block>
						<fo:leader leader-alignment="reference-area" leader-pattern="rule" color="gray"  rule-thickness="2pt" />			              
              				</fo:block>
		              		<fo:block font-family="sans-serif" font-size="20pt" font-weight="bold" font-style="italic">
			              		<xsl:text>Service Requests - </xsl:text>
                                                <xsl:value-of select="//displaytext"/>
		              		</fo:block>
              				<fo:block>
						<fo:leader leader-alignment="reference-area" leader-pattern="rule" rule-style="double" rule-thickness="4pt"/>			              
              				</fo:block>
	        	    </fo:table-cell>  
		        </fo:table-row>				
		</fo:table-body>
	    </fo:table>
	 </fo:block> 
    </fo:static-content> 
    
    <fo:static-content flow-name="xsl-region-after">
    

    </fo:static-content> 
	
    <fo:flow flow-name="xsl-region-body">
		<xsl:apply-templates select="data/main/wogrp"/>
		<!-- To get number of pages -->
		<fo:block id="last-page"/>
		
    </fo:flow>
	</fo:page-sequence>
</fo:root>
</xsl:template>

<xsl:template match="wo">
	<fo:block text-align="start">
	    <fo:table border="1" table-layout="fixed">
	    <fo:table-column column-width="3cm"/>
	    <fo:table-column column-width="4cm"/>
	    <fo:table-column column-width="3cm"/>
	    <fo:table-column column-width="4cm"/>
	    <fo:table-column column-width="5cm"/>	    
		 <fo:table-body>
		        <fo:table-row height="0.7cm">
		            <fo:table-cell>
              				<fo:block font-family="sans-serif" font-size="10pt" font-weight="bold" font-style="italic">
						<xsl:text>Service Request</xsl:text>			              
              				</fo:block>
	        	    </fo:table-cell>
		            <fo:table-cell>
              				<fo:block font-family="sans-serif" font-size="10pt" font-weight="bold" font-style="italic">
						<xsl:text>Requester</xsl:text>			              
              				</fo:block>
	        	    </fo:table-cell>  	        
		            <fo:table-cell>
              				<fo:block font-family="sans-serif" font-size="10pt" font-weight="bold" font-style="italic">
						<xsl:text>Phone</xsl:text>			              
              				</fo:block>
	        	    </fo:table-cell> 
		            <fo:table-cell>
              				<fo:block font-family="sans-serif" font-size="10pt" font-weight="bold" font-style="italic">
						<xsl:text>Status</xsl:text>			              
              				</fo:block>
	        	    </fo:table-cell> 
		            <fo:table-cell>
              				<fo:block font-family="sans-serif" font-size="10pt" font-weight="bold" font-style="italic">
						<xsl:text>Description</xsl:text>			              
              				</fo:block>
	        	    </fo:table-cell>  	        	    
		        </fo:table-row>	
		        <fo:table-row>
		            <fo:table-cell>
              				<fo:block  font-family="sans-serif" font-size="10pt">
						<xsl:value-of select="actnbr"/> 			              
              				</fo:block>              				
	        	    </fo:table-cell>
		            <fo:table-cell>
              				<fo:block  font-family="sans-serif" font-size="10pt">
						<xsl:value-of select="requester"/> 			              
              				</fo:block>                				
	        	    </fo:table-cell>  	        
		            <fo:table-cell>
              				<fo:block  font-family="sans-serif" font-size="10pt">
						<xsl:value-of select="phone"/> 			              
              				</fo:block>               				
	        	    </fo:table-cell> 
		            <fo:table-cell>
              				<fo:block  font-family="sans-serif" font-size="10pt">
						<xsl:value-of select="status"/> 			              
              				</fo:block>               				
	        	    </fo:table-cell> 
		            <fo:table-cell number-rows-spanned="7" border-color="black" border-style="solid" border-width="0.5pt">
              				<fo:block  font-family="sans-serif" font-size="10pt">
						<xsl:value-of select="notes"/> 			              
              				</fo:block>   
              				<fo:block  font-family="sans-serif" font-size="10pt">
						 <xsl:text>&#160;</xsl:text>			              
              				</fo:block>
              				<fo:block  font-family="sans-serif" font-size="10pt">
						 <xsl:value-of select="description"/> 				              
              				</fo:block>                				
	        	    </fo:table-cell>  	        	    
		        </fo:table-row>	
		        
		        <fo:table-row>
		            <fo:table-cell number-columns-spanned="4">
              				<fo:block  font-family="sans-serif" font-size="10pt">
						<fo:leader leader-alignment="reference-area" leader-pattern="rule" />			              
              				</fo:block>              				
	        	    </fo:table-cell>
		        </fo:table-row>	
		        <fo:table-row height="0.7cm">
		            <fo:table-cell>
              				<fo:block font-family="sans-serif" font-size="10pt" font-weight="bold" font-style="italic">
						<xsl:text>Repair Class</xsl:text>			              
              				</fo:block>
	        	    </fo:table-cell>
		            <fo:table-cell>
              				<fo:block font-family="sans-serif" font-size="10pt" font-weight="bold" font-style="italic">
						<xsl:text>Priority</xsl:text>			              
              				</fo:block>
	        	    </fo:table-cell>  	        
		            <fo:table-cell>
              				<fo:block font-family="sans-serif" font-size="10pt" font-weight="bold" font-style="italic">
						<xsl:text>Reported Date</xsl:text>			              
              				</fo:block>
	        	    </fo:table-cell> 
		            <fo:table-cell>
              				<fo:block font-family="sans-serif" font-size="10pt" font-weight="bold" font-style="italic">
						<xsl:text>Completed Date</xsl:text>			              
              				</fo:block>
	        	    </fo:table-cell> 
		        </fo:table-row>	
		        <fo:table-row>
		            <fo:table-cell>
              				<fo:block  font-family="sans-serif" font-size="10pt">
						<xsl:value-of select="repairclass"/> 			              
              				</fo:block>              				
	        	    </fo:table-cell>
		            <fo:table-cell>
              				<fo:block  font-family="sans-serif" font-size="10pt">
						<xsl:value-of select="importance"/> 			              
              				</fo:block>                				
	        	    </fo:table-cell>  	        
		            <fo:table-cell>
              				<fo:block  font-family="sans-serif" font-size="10pt">
						<xsl:if test="string-length(eststartdate)>0" >
							<xsl:value-of select="substring(eststartdate,6,2)" /> <xsl:text>/</xsl:text>  <xsl:value-of select="substring(eststartdate,9,2)" /><xsl:text>/</xsl:text><xsl:value-of select="substring(eststartdate, 1,4)" />
						</xsl:if>              				
              				</fo:block>               				
	        	    </fo:table-cell> 
		            <fo:table-cell>
              				<fo:block  font-family="sans-serif" font-size="10pt">
						<xsl:if test="string-length(estcompletedate)>0" >
							<xsl:value-of select="substring(estcompletedate,6,2)" /> <xsl:text>/</xsl:text>  <xsl:value-of select="substring(estcompletedate,9,2)" /><xsl:text>/</xsl:text><xsl:value-of select="substring(estcompletedate, 1,4)" />
						</xsl:if>  			              
              				</fo:block>               				
	        	    </fo:table-cell> 
		        </fo:table-row>	
		        <fo:table-row>
		            <fo:table-cell number-columns-spanned="4">
              				<fo:block  font-family="sans-serif" font-size="10pt">
						<fo:leader leader-alignment="reference-area" leader-pattern="rule" />			              
              				</fo:block>              				
	        	    </fo:table-cell>
		        </fo:table-row>
		        <fo:table-row height="0.7cm">
		            <fo:table-cell>
              				<fo:block font-family="sans-serif" font-size="10pt" font-weight="bold" font-style="italic">
						<xsl:text>Location</xsl:text>			              
              				</fo:block>
	        	    </fo:table-cell>
		            <fo:table-cell number-columns-spanned="2">
              				<fo:block font-family="sans-serif" font-size="10pt" font-weight="bold" font-style="italic">
						<xsl:text>Employee Supervisor</xsl:text>			              
              				</fo:block>
	        	    </fo:table-cell>  	        
		            <fo:table-cell>
              				<fo:block font-family="sans-serif" font-size="10pt" font-weight="bold" font-style="italic">
						<xsl:text>Assigned To</xsl:text>			              
              				</fo:block>
	        	    </fo:table-cell> 
		        </fo:table-row>	
		        <fo:table-row>
		            <fo:table-cell>
              				<fo:block  font-family="sans-serif" font-size="10pt">
						<xsl:value-of select="location"/> 			              
              				</fo:block>              				
	        	    </fo:table-cell>
		            <fo:table-cell number-columns-spanned="2">
              				<fo:block  font-family="sans-serif" font-size="10pt">
						 			              
              				</fo:block>                				
	        	    </fo:table-cell>  	        
		            <fo:table-cell>
              				<fo:block  font-family="sans-serif" font-size="10pt">
						<xsl:value-of select="supervisor"/> 			              
              				</fo:block>               				
	        	    </fo:table-cell> 
		        </fo:table-row>	
		        <fo:table-row>
		            <fo:table-cell number-columns-spanned="5">
              				<fo:block  font-family="sans-serif" font-size="10pt">
						<fo:leader leader-alignment="reference-area" leader-pattern="rule" rule-style="double" rule-thickness="6pt" />			              
              				</fo:block>              				
	        	    </fo:table-cell>
		        </fo:table-row>			        
		</fo:table-body>
	    </fo:table>
	 </fo:block> 	
</xsl:template>
</xsl:stylesheet>
