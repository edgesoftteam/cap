<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%
   String projectId = (String)request.getAttribute("projectId");
   String contextRoot = request.getContextPath();
   String psaInfo = (String)session.getAttribute("psaInfo");
   if (psaInfo == null ) psaInfo = "";
   String lsoAddress = (String)session.getAttribute("lsoAddress");
   if (lsoAddress == null ) lsoAddress = "";
   int i=0;
%>
<script language="JavaScript">

function teamLead(checked,name) {
	var i,j,ii;
	if (checked) {
	  i = name.indexOf("[")+1;
	  j = name.indexOf("]");
	  ii = name.substring(i,j);
	  for (j=0;j<document.processTeamForm.count.value;j++)
	  	 if (j != ii)
	  	 	document.processTeamForm.elements['processTeamRecord['+j+'].lead'].checked = false;
	}
	return true;
}

function saveTeam() {
    document.forms[0].action='<%=contextRoot%>/saveProcessTeam.do';
 	document.forms[0].submit();
}

function goBack() {
  parent.f_content.location.href="<bean:write name='processTeamForm' property='backUrl'/>";
}
function add() {
  parent.f_content.location.href='<%=contextRoot%>/insertRow.do';
}
</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form action="/checkBoxSelected">
<html:hidden property="psaId"/>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">
          <logic:equal value="P" name="processTeamForm" property="psaType">Modify Process Team</logic:equal>
          <logic:equal value="A" name="processTeamForm" property="psaType">Modify Activity Team</logic:equal></font>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%></font><br><br>
         </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle">Modify</td>

                      <td width="1%" class="tablebutton"><nobr>
                     	<html:button property="Back" value="Back" onclick="goBack()" styleClass="button" />&nbsp;&nbsp;
                      	<security:editable levelId="<%=projectId%>" levelType="P" editProperty="checkUser">
                      	<html:button property="Add" value="Add" onclick="add()" styleClass="button" />&nbsp;&nbsp;
                      	<html:button property="Save" value="Save" onclick="saveTeam()" styleClass="button" />&nbsp;&nbsp;
                      	</security:editable>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                   	<td class="tablelabel">Remove</td>
                      <td class="tablelabel"> Title</td>
                      <td class="tablelabel"> Name</td>
                      <td class="tablelabel"> Team Leader</td>

                    </tr>
                    <nested:iterate id="rec" name="processTeamForm" property="processTeamRecord" type="elms.app.common.ProcessTeamRecord">
<%
	i++;
%>
                    <tr valign="top">
                      <td class="tabletext">
                       <nested:checkbox  property="checkRemove" />
                      </td>
                      <td class="tabletext">
                         <nested:write  property="title" />
					  </td>
                      <td class="tabletext">
                         <nested:write  property="name" />
                      </td>
                      <td class="tabletext">
                        <nested:checkbox  property="lead" onclick="return teamLead(this.checked,this.name);" />
                      </td>
					</nested:iterate>
					<input type=hidden name="count" value="<%=i%>">
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>


