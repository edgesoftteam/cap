
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%
   String contextRoot = request.getContextPath();
   String psaInfo = (String)session.getAttribute("psaInfo");
   if (psaInfo == null ) psaInfo = "";
   String lsoAddress = (String)session.getAttribute("lsoAddress");
   if (lsoAddress == null ) lsoAddress = "";
   int i=0;
%>
<script language="JavaScript">
function teamLead(checked,name) {
	var i,j,ii;
	if (checked) {
	  i = name.indexOf("[")+1;
	  j = name.indexOf("]");
	  ii = name.substring(i,j);
	  for (j=0;j<document.processTeamForm.count.value;j++)
	  	 if (j != ii)
	  	 	document.processTeamForm.elements['ActivityTeamObject['+j+'].lead'].checked = false;
	}
	return true;
}

</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form action="/checkBoxSelected">
<html:hidden property="psaId"/>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr>
    <td colspan="3">
	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                       <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                                <td width="95%" class="tabletitle">
                                  <logic:equal name="editable" value="true">
                                  <a href="<%=contextRoot%>" class="hdrs">Activity Team</a>
								  </logic:equal>
                                  <logic:notEqual name="editable" value="true">
                                  Activity Team
                                  </logic:notEqual>
                               </td>

                                <td width="4%" class="tablelabel">

                                  <logic:equal name="editable" value="true">
                                  	<a href="<%=contextRoot%>" class="hdrs">View</a><img src="../images/site_blt_plus_000000.gif" width="21" height="9">
                                  </logic:equal>

                                </td>
                               <td width="4%" class="tablelabel"><nobr>
	                                    <html:reset value="Back" styleClass="button" onclick="history.back();"/>
	                           </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                        	<tr>
                      			<td class="tablelabel"> Title</td>
                      			<td class="tablelabel"> Name</td>
                      			<td class="tablelabel"> Team Leader</td>
                   			</tr>
                            <logic:iterate id="processTeamRecord" name="activityTeamList" type="elms.app.common.ActivityTeamObject" scope="request">
                            <tr class="tabletext" valign="top">
                                <td class="tabletext">&nbsp;<bean:write name="processTeamRecord" property="title"/></td>
                                <td class="tabletext">&nbsp;<bean:write name="processTeamRecord" property="name"/></td>
                               <td class="tabletext">
                        		<html:checkbox name="processTeamRecord"  property="lead" onclick="return teamLead(this.checked,this.name);" />
                     			 </td>
                     	   </tr>
                           </logic:iterate>
                           <input type=hidden name="count" value="<%=i%>">
                        </table>
                        </td>
                    </tr>
                </table>
    </td>
</tr>

        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>



