<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%
    String contextRoot = request.getContextPath();
    String psaInfo = (String)session.getAttribute("psaInfo");
    if (psaInfo == null ) psaInfo = "";
    String lsoAddress = (String)session.getAttribute("lsoAddress");
    if (lsoAddress == null ) lsoAddress = "";
    String psaId = (String) session.getAttribute(elms.common.Constants.PSA_ID);
    if(psaId ==null) psaId = "";

%>
<script>
function onclickProcess(strval){
   if (strval == 'vp') document.location='<%=contextRoot%>/viewProject.do?projectId='+document.processTeamForm.psaId.value;
   else if (strval == 'va') document.location='<%=contextRoot%>/viewActivity.do?activityId='+document.processTeamForm.psaId.value;
   else if (strval == 'ir') document.location='<%=contextRoot%>/insertRow.do';
   else if (strval == 'snpt') document.location='<%=contextRoot%>/saveNewProcessTeam.do';
}
function saveTeam() {

	if(document.forms[0].elements['processTeamCollectionRecord[0].title'].value==""){
	  alert("Please select title");
	  document.forms[0].elements['processTeamCollectionRecord[0].title'].focus();
	  return false;
	}
	else if(document.forms[0].elements['processTeamCollectionRecord[0].name'].value==""){
	  alert("Please select name");
	  document.forms[0].elements['processTeamCollectionRecord[0].name'].focus();
	  return false;
	}else{
    document.forms[0].action='<%=contextRoot%>/saveNewProcessTeam.do';
 	document.forms[0].submit();}
}

function goBack() {
  parent.f_content.location.href="<bean:write name='processTeamForm' property='backUrl'/>";
}
function add() {
  parent.f_content.location.href='<%=contextRoot%>/insertRow.do';
}

if(document.addEventListener){ //code for Moz
document.addEventListener("keypress",keyCapt,false);
}else{
document.attachEvent("onkeyup",keyCapt); //code for IE
}

function keyCapt(e){
if(typeof window.event!="undefined"){
e=window.event;//code for IE
}

if(e.keyCode == 13){
saveTeam();
}

}
</script>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form  action="/populateUsers">
<html:hidden property="psaId"/>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">
          <logic:equal value="P" name="processTeamForm" property="psaType">Add Process Team</logic:equal>
          <logic:equal value="A" name="processTeamForm" property="psaType">Add Activity Team</logic:equal></font>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%></font><br><br>
         </td>        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle">Add</td>

                      <td width="1%" class="tablelabel"><nobr>
                     	<html:button property="Back" value="Back" onclick="goBack()" styleClass="button" />&nbsp;&nbsp;
                      	<security:editable levelId="<%=psaId%>" levelType="P" editProperty="checkUser">
                      	<html:button property="Add" value="Add" onclick="add()" styleClass="button" />&nbsp;&nbsp;
                      	<html:button property="Save" value="Save" onclick="saveTeam()" styleClass="button" />&nbsp;&nbsp;
                      	</security:editable>
                        </nobr>
                      </td>
                   </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel" width="30%">Title</td>
                      <td class="tablelabel" width="70%">Name</td>

                    </tr>
                    <nested:iterate  id="rec" name="processTeamForm" property="processTeamCollectionRecord" type="elms.app.common.ProcessTeamCollectionRecord">
                     <%
                       elms.app.common.ProcessTeamCollectionRecord rec = (elms.app.common.ProcessTeamCollectionRecord)pageContext.getAttribute("rec");
                       java.util.List titleList = rec.getTitleList();
                       pageContext.setAttribute("titleList", titleList);
                       java.util.List nameList = rec.getNameList();
                       pageContext.setAttribute("nameList", nameList);
                       pageContext.setAttribute("rec", rec);
                     %>
                    <tr valign="top">

                      <td class="tabletext" width="30%">
                        <nested:select property="title" styleClass="textbox" onchange="document.forms[0].submit();">
                          <html:options collection="titleList" property="titleValue" labelProperty="titleLabel"/>
                        </nested:select>
                       </td>
                      <td class="tabletext" width="70%">
                        <nested:select property="name" styleClass="textbox" onchange="document.forms[0].submit();">
                          <html:options collection="nameList" property="nameValue" labelProperty="nameLabel"/>
                        </nested:select>
                      </td>

                    </tr>
                    </nested:iterate>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

