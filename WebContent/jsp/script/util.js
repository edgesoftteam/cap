// util.js


/* This function is for stripping leading and trailing spaces */
function trim(str) {
     if (str != null) {
        var i;         
		for (i=0; i<str.length; i++) {
            if (str.charAt(i)!=" ") {
                str=str.substring(i,str.length); 
                break;           
			 }        
		}             
		for (i=str.length-1; i>=0; i--) { 
           if (str.charAt(i)!=" ") {  
              str=str.substring(0,i+1); 
                break; 
           }      
	   }              
 	  if (str.charAt(0)==" ") {    
        return "";       
	  } else { 
           return str;  
       }   
	 }
     return "";
}

function getDateObject(dtStr)
{
	//alert(dtStr);
	var dtCh = "/";
	var pos1=dtStr.indexOf(dtCh);
	var pos2=dtStr.indexOf(dtCh,pos1+1);
	var strMonth=dtStr.substring(0,pos1);
	var strDay=dtStr.substring(pos1+1,pos2);
	var strYear=dtStr.substring(pos2+1);
	if (strYear.length != 4)
	{
		strYear = "20" + strYear;
	}
	strYr=strYear;
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1);
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1);
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1);
	}
	month=parseInt(strMonth);
	day=parseInt(strDay);
	year=parseInt(strYr);
	//alert(month);
	//alert(day);
	//alert(year);
	
	return new Date(year,month,day);
}

function move(fbox, tbox){
     var arrFboxValue = new Array();
	 var arrFboxText = new Array();
     var arrTboxValue = new Array();
	 var arrTboxText = new Array();
	 var tboxLength=0;
	 var fboxLength=0;

	for(tboxLength=0; tboxLength<tbox.options.length; tboxLength++){
	arrTboxValue[tboxLength]= tbox.options[tboxLength].value;
	arrTboxText[tboxLength]= tbox.options[tboxLength].text;
	}
	for(i=0; i<fbox.options.length; i++) {
		if(fbox.options[i].selected && fbox.options[i].value != ""){
		arrTboxValue[tboxLength]=fbox.options[i].value;
		arrTboxText[tboxLength]=fbox.options[i].text;
		tboxLength++;
		}
		else{
		arrFboxValue[fboxLength]=fbox.options[i].value;
		arrFboxText[fboxLength]=fbox.options[i].text;
		fboxLength++;
		}
	}
     fbox.length = 0;
     tbox.length = 0;
	 var c=0;
     for(c=0; c<arrFboxValue.length; c++) {
          var no = new Option();
          no.value = arrFboxValue[c]
          no.text = arrFboxText[c]
          fbox[c] = no;
     }
     for(c=0; c<arrTboxValue.length; c++) {
             var no = new Option();
             no.value = arrTboxValue[c];
             no.text = arrTboxText[c];
			 tbox[c] = no;
     }
}

function selectAll(box) {
	if(box.length>0){
    	 for(var i=0; i<box.length; i++) {
		     box[i].selected = true;
	     }
	}
}
function unselectAll(box) {
	if(box.length>0){
    	 for(var i=0; i<box.length; i++) {
		     box[i].selected = false;
	     }
	}
}

function validateAlphabet(){ 
	/* 
	 * Key Codes
	 * 	a = 97
	 *  z = 122;
	 *	A = 65
	 *	Z = 90;
	 *	space = 32;
	 *
	 * */
	if((event.keyCode>96 && event.keyCode<123) || (event.keyCode>64 && event.keyCode<91) || event.keyCode == 32){
		event.returnValue = true;
	}else{
		event.returnValue = false;
	}
}
function validateAlphaNumeric(){
	/* 
	 * Key Codes
	 * 	a = 97
	 *  z = 122;
	 *	A = 65
	 *	Z = 90;
	 *	space = 32;
	 * 	0 = 48
	 * 	9 = 57
	 * 	
	 * */
	if((event.keyCode>96 && event.keyCode<123) || (event.keyCode>64 && event.keyCode<91) || (event.keyCode>47 && event.keyCode<58) || event.keyCode == 32){
		event.returnValue = true;
	}else{
		event.returnValue = false;
	}
}

function DisplayHyphenForZip(str)
{
	if (( event.keyCode<48 || event.keyCode>57 ))
	{
		event.returnValue = false;
	}
	else
	{
		if ((document.forms[0].elements[str].value.length == 5))
		{
			document.forms[0].elements[str].value = document.forms[0].elements[str].value +'-';
 		}
 		if (document.forms[0].elements[str].value.length > 9 )  event.returnValue = false;
	}
	return true;
}

