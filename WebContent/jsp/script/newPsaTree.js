// Arrays for nodes and icons
var nodes       = new Array();
var openNodes   = new Array();
var icons       = new Array(6);
var highlightColor = "#B0C0D0";


// Loads all icons that are used in the tree
function preloadIcons() {
    icons[0] = new Image();
    icons[0].src = "img/plus.gif";
    icons[1] = new Image();
    icons[1].src = "img/plusbottom.gif";
    icons[2] = new Image();
    icons[2].src = "img/minus.gif";
    icons[3] = new Image();
    icons[3].src = "img/minusbottom.gif";
    icons[4] = new Image();
    icons[4].src = "img/folder.gif";
    icons[5] = new Image();
    icons[5].src = "img/folderopen.gif";
    icons[6] = new Image();
    icons[6].src = "img/folderL.gif";
    icons[7] = new Image();
    icons[7].src = "img/folderopenL.gif";
    icons[8] = new Image();
    icons[8].src = "img/folderS.gif";
    icons[9] = new Image();
    icons[9].src = "img/folderopenS.gif";
    icons[10] = new Image();
    icons[10].src = "img/folderO.gif";
    icons[11] = new Image();
    icons[11].src = "img/folderopenO.gif";
}
// Create the tree
function createTree(arrName, startNode, openNode) {
    nodes = arrName;
    if (nodes.length > 0) {
        preloadIcons();
        if (startNode == null) startNode = 0;
        if (openNode != 0 || openNode != null) setOpenNodes(openNode);
		
        if (startNode !=0) {
            var nodeValues = nodes[getArrayId(startNode)].split("|");
            
            if (nodeValues[6] == 'C') {
            	 
              document.write("<nobr><img src=\"img/folderopen.gif\" align=\"absbottom\" alt=\"\" /><a href=\"" + nodeValues[3] + "\" class='tree1' title='" + nodeValues[4] + "' onmouseover=\"window.status='';return true;\" onmouseout=\"window.status=' ';return true;\"><font class='treetextdiff'>" + nodeValues[2] + "</font></a></nobr><br />");
            }else {
           
               document.write("<nobr><img src=\"img/folderopen.gif\" align=\"absbottom\" alt=\"\" /><a href=\"" + nodeValues[3] + "\" class='tree1' title='" + nodeValues[4] + "' onmouseover=\"window.status='';return true;\" onmouseout=\"window.status=' ';return true;\"><font class='treetext'>" + nodeValues[2] + "</font></a></nobr><br />");
            }
        } //else document.write("<img src=\"img/base.gif\" align=\"absbottom\" alt=\"\" /> <br />");

        var recursedNodes = new Array();
        addNode(startNode, recursedNodes);
    }
}
// Returns the position of a node in the array
function getArrayId(node) {
    for (i=0; i<nodes.length; i++) {
        var nodeValues = nodes[i].split("|",1);
        if (nodeValues[0]==node) return i;
    }
}
// Returns the lsoId
function getParentofNode(node) {
        if(nodes.length <= 0) return null;
        var nodeValues = nodes[node].split("|",2);
        return nodeValues[1];

}
// Returns the parent node of any node
function getParentId(node) {
    if(nodes.length <= 0) return null;
    for (i=0; i<nodes.length; i++) {
        var nodeValues = nodes[i].split("|",4);
        var nodeValuesEmelents = nodeValues[3].split("=");
        if (nodeValuesEmelents[1]==node) return nodeValues[1];
    }
}

// Highlights named node and returns parent
function highlightAndGetParent(node) {
    if(nodes.length <= 0) return null;
    var result = null
    for (i=0; i<nodes.length; i++) {
        var nodeValues = nodes[i].split("|");
        if (nodeValues[7]==node) {
            document.getElementById('NODE'+nodeValues[0]).style.background=highlightColor;
            result = nodeValues[1];
        }
        else {
            document.getElementById('NODE'+nodeValues[0]).style.background='';
        }
    }
    return result;
}

// Puts in array nodes that will be open
function setOpenNodes(openNode) {
    for (i=0; i<nodes.length; i++) {
        var nodeValues = nodes[i].split("|",2);
        if (nodeValues[0]==openNode) {
            openNodes.push(nodeValues[0]);
            setOpenNodes(nodeValues[1]);
        }
    }
}
// Checks if a node is open
function isNodeOpen(node) {
    for (i=0; i<openNodes.length; i++)
        if (openNodes[i]==node) return true;
    return false;
}
// Checks if a node has any children
function hasChildNode(i,parentNode) {
    if ( i+1 < nodes.length) {
        var nextNode = nodes[i+1].split("|",2);
        if (nextNode[1] == parentNode) return true;
    }
    return false;
}
// Checks if a node is the last sibling
function lastSibling (j,node, parentNode) {
    var lastChild = 0;
    for (i=j+1; i< nodes.length; i++) {
        var nodeValues = nodes[i].split("|",2);
        if (nodeValues[1] == parentNode)
            return false;
        if (nodeValues[1] == 0)
            return true;
    }
    return true;
}

// Adds a new node in the tree
function addNode(parentNode, recursedNodes) {

    for (var i = 0; i < nodes.length; i++) {
		
        var nodeValues = nodes[i].split("|");
        if (nodeValues[1] == parentNode) {
            var docWriter = "";
            var ls  = lastSibling(i,nodeValues[0], nodeValues[1]);
            var hcn = hasChildNode(i,nodeValues[0]);
            var ino = isNodeOpen(nodeValues[0]);

            // Write out line & empty icons
            for (g=0; g<recursedNodes.length; g++) {
                if (recursedNodes[g] == 1)
                    docWriter += "<img src=\"img/line.gif\" align=\"absbottom\" style='border-style:none' alt=\"\" />";
                else
                    docWriter += "<img src=\"img/empty.gif\" align=\"absbottom\" style='border-style:none' alt=\"\" />";
            }

            // put in array line & empty icons
            if (ls) recursedNodes.push(0);
            else recursedNodes.push(1);

            // Write out join icons
            if (hcn) {
                if (ls) {
                    docWriter += "<a href=\"javascript: oc(" + nodeValues[0] + ", 1,'"+nodeValues[5]+"');\"><img id=\"join" + nodeValues[0] + "\" src=\"img/";
                        if (ino)
                            docWriter += "minus";
                        else
                            docWriter += "plus";
                    docWriter += "bottom.gif\" align=\"absbottom\" style='border-style:none' alt=\"Open/Close node\" /></a>";
                } else {
                    docWriter += "<a href=\"javascript: oc(" + nodeValues[0] + ", 0,'"+nodeValues[5]+"');\"><img id=\"join" + nodeValues[0] + "\" src=\"img/";
                    if (ino)
                        docWriter += "minus";
                    else
                        docWriter += "plus";
                    docWriter += ".gif\" align=\"absbottom\" style='border-style:none' alt=\"Open/Close node\" /></a>";
                }
            } else {
                if (ls)
                    docWriter += "<img src=\"img/join.gif\" align=\"absbottom\" style='border-style:none' alt=\"\" />";
                else
                    docWriter += "<img src=\"img/joinbottom.gif\" align=\"absbottom\" style='border-style:none' alt=\"\" />";
            }
            // Write out folder & page icons
            if (hcn) {
                docWriter +="<nobr><img style='border-style:none' id=\"icon" + nodeValues[0] + "\" src=\"img/folder";
                if (ino)
                    docWriter +="open";
                docWriter += nodeValues[5]+".gif\" align=\"absbottom\" alt=\"Folder\" />";
                if (nodeValues[6] == 'C') {
                  docWriter += "<a href=\"" + nodeValues[3] + "\" target='f_content' class='tree1' title='" + nodeValues[4] + "' id='NODE" + nodeValues[0] + "' onclick=\"highlightNodeId('" + nodeValues[0] + "')\" onmouseover=\"window.status='';return true;\"   onmouseout=\"window.status=' ';return true;\"><font class='treetextdiff'>";
                }else{
                   docWriter += "<a href=\"" + nodeValues[3] + "\" target='f_content' class='tree1' title='" + nodeValues[4] + "' id='NODE" + nodeValues[0] + "' onclick=\"highlightNodeId('" + nodeValues[0] + "')\" onmouseover=\"window.status='';return true;\"   onmouseout=\"window.status=' ';return true;\"><font class='treetext'>";
                }
            }
            else {
                docWriter += "<nobr><img id=\"icon" + nodeValues[0] + "\" src=\"img/folder"+nodeValues[5]+".gif\" align=\"absbottom\" style='border-style:none' alt=\"Folder\" />";
                if (nodeValues[6] == 'C') {
                   docWriter += "<a href=\"" + nodeValues[3] + "\" target='f_content' class='tree1' title='" + nodeValues[4] + "' id='NODE" + nodeValues[0] + "' onclick=\"highlightNodeId('" + nodeValues[0] + "')\" onmouseover=\"window.status='';return true;\"   onmouseout=\"window.status=' ';return true;\"><font class='treetextdiff'>";
                }else{
                   docWriter += "<a href=\"" + nodeValues[3] + "\" target='f_content' class='tree1' title='" + nodeValues[4] + "' id='NODE" + nodeValues[0] + "' onclick=\"highlightNodeId('" + nodeValues[0] + "')\" onmouseover=\"window.status='';return true;\"   onmouseout=\"window.status=' ';return true;\"><font class='treetext'>";
                }
            }
            // Write out node name
            docWriter += nodeValues[2];

            // End link
            docWriter += "</font></a></nobr><br />";

            // If node has children write out divs and go deeper
            if (hcn) {
                docWriter += "<div id=\"div" + nodeValues[0] + "\"";
                if (!ino) docWriter += " style=\"display: none;\"";
                document.write(docWriter + ">");
                docWriter = "";
                addNode(nodeValues[0], recursedNodes);
                document.write("</div>");
            } else {
                document.write(docWriter);
                docWriter = "";
            }
            // remove last line or empty icon
            recursedNodes.pop();
        }
    }
}
// Opens or closes a node
function oc(node, bottom, level) {
    var theDiv = document.getElementById("div" + node);
    var theJoin = document.getElementById("join" + node);
    var theIcon = document.getElementById("icon" + node);
    var theLevel = 0;
    var theSrc_Open = "img/folderopen.gif";
    var theSrc_Close = "img/folder.gif";
    if (level != undefined) {
        theSrc_Open = "img/folderopen" + level + ".gif";
        theSrc_Close = "img/folder" + level + ".gif";
    }

  if(theDiv!=null && theJoin!=null && theIcon!=null){
    if (theDiv.style.display == 'none') {
        if (bottom==1) theJoin.src = icons[3].src;
        else theJoin.src = icons[2].src;
        theIcon.src = theSrc_Open;
//        theIcon.src = icons[5+theLevel].src;
        theDiv.style.display = '';
    } else {
        if (bottom==1) theJoin.src = icons[1].src;
        else theJoin.src = icons[0].src;
        theIcon.src = theSrc_Close;
//        theIcon.src = icons[4+theLevel].src;
        theDiv.style.display = 'none';
    }
  }
}

// highlights node with specified id
function highlightNodeId(n) {
    if(nodes.length <= 0) return null;
    for (var i = 0; i < nodes.length; i++) {
        var nodeValues = nodes[i].split("|");
        document.getElementById('NODE'+nodeValues[0]).style.background='';
    }
    document.getElementById('NODE'+n).style.background=highlightColor;
}

// highlights named node
function highlightNode(node) {
    if(nodes.length <= 0) return null;
    for (i=0; i<nodes.length; i++) {
        var nodeValues = nodes[i].split("|");
        if (nodeValues[7]==node) {
            document.getElementById('NODE'+nodeValues[0]).style.background=highlightColor;
        }
        else {
            document.getElementById('NODE'+nodeValues[0]).style.background='';
        }
    }
}

function scrollToNode(node) {

    var nodeId = -1;

    for (i=0; i<nodes.length; i++) {
        var nodeValues = nodes[i].split("|");
        if (nodeValues[7]==node) {
            nodeId = nodeValues[0];
            break;
        }
    }

    if (nodeId < 0) { return false; }

    var windowWidth, windowHeight;

    if (document.layers){
       windowWidth=window.outerWidth;
       windowHeight=window.outerHeight;
    }
    if (document.all){
       windowWidth=document.body.clientWidth;
       windowHeight=document.body.clientHeight;
    }

    var elem = document.getElementById('NODE'+nodeId);
    var elemTop = elem.offsetTop;
    window.scrollTo(0, elemTop);
}

// Push and pop not implemented in IE(crap!    don�t know about NS though)
if(!Array.prototype.push) {
    function array_push() {
        for(var i=0;i<arguments.length;i++)
            this[this.length]=arguments[i];
        return this.length;
    }
    Array.prototype.push = array_push;
}
if(!Array.prototype.pop) {
    function array_pop(){
        lastElement = this[this.length-1];
        this.length = Math.max(this.length-1,0);
        return lastElement;
    }
    Array.prototype.pop = array_pop;
}
