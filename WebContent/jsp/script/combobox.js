function ComboBox(name,field) {
	
// The parameter "name" must have the same value as the name we give to the object.
// If not errors will be raised as this parameter is used to reference the Object properties and function.

// Also an ID is randomly calculated which is used as a prefix for the name of the HTML controls.
// This ID is random for the following reason. IE and other browsers remember values entered in a textfield.
// If the textfield stays the same a popup comes out w/ previous given values that covers our layer.
	
	this.obj = name;

//////////////////
//  Properties  //
//////////////////

	this.filter			= true;

	this.selectedText	= "";
	this.selectedValue	= "";
	
	this.height			= "100";
	this.width			= "200";
	
	this.fontName		= "Verdana";
	this.fontSize		= "12px";
	
	this.fontColor		= "black";
	this.bgColor		= "white";
	this.borderColor	= "gray";
	this.fontColor		= "black";
	
	this.hiliteFColor	= "white";
	this.hiliteBColor	= "gray";
	
////////////////////////////
//	Read-only Properties  //
////////////////////////////
	
	this.getLength		= function() {return _noe;};
	
	this.hasFocusIndex	= function() {return _hasfocusIndex;};
	this.selectedIndex	= function() {return _selectedIndex;};

/////////////////////////
//  Private Variables  //
/////////////////////////
	
	var _noe		= 0;
	var _elements	= new Array();
	

	var _filter		= null;
	var _contents	= "";

	var _noe_f		= 0
	var _indexes	= [];

///////////////////////
//  State Variables  //
///////////////////////

	var _selectedIndex	= -1;
	var _hasfocusIndex	= -1;

	var _expanded		= false;

//////////////
// Handlers //
//////////////

	var _objTextbox;
	var _objScrollDiv;

///////////////////////
//  Event Variables  //
///////////////////////	

//	var _filterChanged = false; 

////////////////////////
//	Public Functions  //
////////////////////////

	this.add = function(text, value) { _elements[_noe++] = [text, (value)?value:text]; }
	 
	this.getText  = function(index) { return (index<_noe)?_elements[index][0]:""; }
	this.getValue = function(index) { return (index<_noe)?_elements[index][1]:""; }

	this.toString = function() {
		// Process event
		var _command = "return " + this.obj + ".handle(event);";
		var _close = "return " + this.obj + ".collapse();";

		// Final output
		var _out = 	"<style type='text/css'>" +
						"<!--" +
						"input#" + this.obj + "_text {font:" + this.fontSize + " " + this.fontName + ";color:" + this.fontColor + ";background-color:" + this.bgColor + ";width:" + (this.width-20) + ";height:" + this.height + ";border:0px;padding:0px;}" +
												
						"#" + this.obj + "_scroll_div {background-color:" + this.bgColor + ";position:absolute;visibility:hidden;width:" + this.width + ";height:" + this.height + ";overflow:auto;font:" + this.fontSize + " " + this.fontName + ";border:1px solid " + this.borderColor + ";white-space:nowrap;cursor:pointer;cursor:hand;padding:0px}" +
						
						"." + this.obj + "_over {color:" + this.hiliteFColor + ";background-color:" + this.hiliteBColor + ";}" +
						"." + this.obj + "_out  {color:" + this.fontColor + ";background-color:" + this.bgColor + ";}" +
						"-->" +
					"</style>" +
					"<div id=" + this.obj + "_scroll_div></div>" +
					"<div style='width:" + this.width + "; border:1px solid " + this.borderColor + ";'>" +
						"<table border='0' cellpadding='0' cellspacing='0' width='100%'>" +
							"<tr>" +
								"<td valign='bottom'><img id='" + this.obj + "_anchor' name='" + this.obj + "_anchor' width='1' height='1'></td>" +
								"<td><input type='text' id='" + this.obj + "_text' name='" + this.obj + "_text' value='" + this.selectedText + "' onclick=\"" + _command + "\" onkeydown=\"" + _command + "\" onkeyup=\"" + _command + "\" onkeypress=\"" + _command + "\" autocomplete='off' style='padding:0px;margin:0px;width:328px;height:18px'/></td>" +
								"<td><input type='hidden' name='" + this.obj + "_value'/></td>" +
								"<td><img id='" + this.obj + "_button' name='" + this.obj + "_button' src='../images/emptybutton.gif' border='0' onclick=\"" + _close + "\" hspace='1' vspace='1'/></td>" +
							"</tr>" +
						"</table>" +
					"</div>";
		
		return _out;
	}

	this.build = function() {
		document.write(this);

		// Handlers
		_objTextbox 	= MM_findObj(this.obj + "_text");
		_objScrollDiv	= MM_findObj(this.obj + "_scroll_div");
		
		//alert("Please  APN :::: " +document.getElementById(field).options[document.getElementById(field).selectedIndex].value);
		this.selectedValue = document.getElementById(field).options[document.getElementById(field).selectedIndex].value;
		
		// Selected value
		for (var i=0; i<_noe; i++) {
			if (_elements[i][1]==this.selectedValue) {
				_selectedIndex = i;
				_objTextbox.value = _elements[i][0];
				break;
			}
		}
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////
//	Private Functions  //
/////////////////////////

// These function are in fact declared as public, that is, there are visible from outside.
// There are declared this way so that they can reference the current object.
// Otherwise the "this" operator can not be used and will produce an "undefined" error.
	
	this.expand = function() {
		//if(MM_findObj(this.obj + "_text").value == "") {
		//	return;
		//}
		
		P7_Snap(this.obj + "_anchor", this.obj + "_scroll_div", 0, 2);
		MM_showHideLayers(this.obj + "_scroll_div", "", "show")
	//	overlib("<div id=" + this.obj + "_scroll_div></div>", WIDTH, this.width, FGCOLOR, this.bgColor, BGCOLOR, this.borderColor, CELLPAD, 0, STICKY, ANCHOR, this.obj + "_anchor", ANCHORALIGN, "LL", "UL");
	//	_expanded = true;
		bt = document.getElementById("problemCombo_button");
		bt.src = "../images/cross.gif";
	}

	this.collapse = function() {
		MM_showHideLayers(this.obj + "_scroll_div", "", "hide")
		_expanded = false;
		bt = document.getElementById("problemCombo_button");
		bt.src = "../images/emptybutton.gif";
	}
	
	this.scrollTo = function(index) {
		var _el = MM_findObj(this.obj + "_td_" + index);
		if (_el != null && _el.scrollIntoView) {
			// http://www.faqts.com/knowledge_base/view.phtml/aid/8554
			_el.scrollIntoView();
		}
	}

	this.select = function(index) {
		var _text	= (index<_noe_f)?_elements[_indexes[index]][0]:"";
		var _value	= (index<_noe_f)?_elements[_indexes[index]][1]:"";

		_selectedIndex = index;
		MM_findObj(this.obj + "_text").value	= _text;
		MM_findObj(this.obj + "_value").value	= _value;
		filter = _value;
		this.collapse();

		p = document.getElementById(field);
		p.selectedIndex=0;
		for(i=0; i<p.length; i++) {
		  	selText = p.options[i].text;
		  	if(_text==selText) {
		  		p.selectedIndex=i;
		  		break;
		  	}
		}
	}

	this.setContents = function(filter) {
		MM_setTextOfLayer(this.obj + "_scroll_div", '', this.getContents(filter));
	}

	this.getContents = function(filter) {
		if (filter != _filter) {
			_contents = ""; 
		
			var _temp = new Array();
			var _re = new RegExp("(" + filter + ")", "ig")
			
			_noe_f = 0; _indexes = [];
			for (var i=0; i<_noe; i++) {
				if (!this.filter || _elements[i][0].toLowerCase().indexOf(filter.toLowerCase())>=0) {
					_temp.push("<div id='" + this.obj + "_td_" + i + "' title='" + this.obj + "_td_" + i + "' class='" + this.obj + ((i==_hasfocusIndex)?"_over":"_out") + "' onmouseover='" + this.obj + ".over(" + _noe_f + ",this);' onmouseout='" + this.obj + ".out(" + _noe_f + ",this);' onclick='" + this.obj + ".select(" + _noe_f + ");'>" + _elements[i][0].replace(_re, "$1".bold()) + "</div>");
					_indexes[_noe_f++] = i; 
				}
			}
			_contents = _temp.join(''); 

		_filter = filter;

		//	alert("different");
		} else {
		//	alert("same");
		}
		
		return _contents;
	}
	
	
	this.handle = function(event) {
		var UPARROW = "38", DOWNARROW = "40", ENTER = "13", ESCAPE = "27", SPACE = "32";
		
		var objTextfield = MM_findObj(this.obj + "_text");

		var keyCode = String((String(event.which)=="undefined")?event.keyCode:event.which);
	//	var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
	//	alert(keyCode);
	//	alert(event.srcElement);

		switch (event.type) {
			case "click"	:
				objTextfield.select();
				if (!_expanded) {
					this.expand();
					this.setContents(objTextfield.value);
				} else {
					this.collapse();
				}
				break;

			case "keydown"	:
				if (keyCode==UPARROW) 	this.focusPrevious();
				if (keyCode==DOWNARROW) this.focusNext();
				break;

			case "keyup"	:
				if (keyCode==ENTER) {
					this.select(_hasfocusIndex);
				} else if (keyCode!=UPARROW && keyCode!=DOWNARROW) {
				//	<changed reason="Netscape 6 seems to behave strangely">
				//		<from>
				//			if (!_expanded) this.expand();
				//		</from>
				//		<to>
							this.expand();
				//		</to>
				//	</changed>
					this.setContents(objTextfield.value);
					if (_noe_f>0) this.over(0);
					else this.collapse();
				}				
				break;

			case "keypress"	:
				if (keyCode==ENTER) return false; //Disable Submit on ENTER
				break;
		}
		
		this.debug();
		return true;
	}
	
	this.over = function(index, obj) { if (!obj) obj = MM_findObj(this.obj + "_td_" + _indexes[index]); obj.className = this.obj + "_over"; _hasfocusIndex = index;}
	this.out  = function(index, obj) { if (!obj) obj = MM_findObj(this.obj + "_td_" + _indexes[index]); obj.className = this.obj + "_out" ; }
	
	this.focusPrevious = function() {
		this.out(_hasfocusIndex);
		_hasfocusIndex = (_hasfocusIndex > 0)? _hasfocusIndex-1 : _noe_f-1;
		this.over(_hasfocusIndex);
		
		this.scrollTo(_indexes[_hasfocusIndex]);
	}
	
	this.focusNext = function() {
		this.out(_hasfocusIndex);
		_hasfocusIndex = (_hasfocusIndex < _noe_f-1)? _hasfocusIndex+1 : 0;
		this.over(_hasfocusIndex);
		
		this.scrollTo(_indexes[_hasfocusIndex]);
	}

	this.debug = function() {
		//window.status = (_expanded?"Expanded":"Collapsed") + " " + _indexes.join(",") + ' :: ' + _objScrollDiv.scrollTop ;
	}

}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function MM_findObj(n, d) { //v4.01
	var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
   		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_setTextOfTextfield(objName,x,newText) { //v3.0
  var obj = MM_findObj(objName); if (obj) obj.value = newText;
}

function MM_setTextOfLayer(objName,x,newText) { //v4.01
  if ((obj=MM_findObj(objName))!=null) with (obj)
    if (document.layers) {document.write((newText)); document.close();}
    else innerHTML = unescape(newText);
}

function MM_showHideLayers() { //v6.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}

function MWJ_findObj( oName, oFrame, oDoc ) {
    /* this function is slightly bigger than the DreamWeaver
    function but is more efficient as it can also find
    anchors, frames, variables, functions, and check through
    any frame structure

    if not working on a layer, document should be set to the
    document of the working frame
    if the working frame is not set, use the window object
    of the current document
    WARNING: - cross frame scripting will cause errors if
    your page is in a frameset from a different domain */
    if( !oDoc ) { if( oFrame ) { oDoc = oFrame.document; } else {
        oDoc = window.document; } }

    //check for images, forms, layers
    if( oDoc[oName] ) { return oDoc[oName]; }

    //check for pDOM layers
    if( oDoc.all && oDoc.all[oName] ) { return oDoc.all[oName]; }

    //check for DOM layers
    if( oDoc.getElementById && oDoc.getElementById(oName) ) {
        return oDoc.getElementById(oName); }

    //check for form elements
    for( var x = 0; x < oDoc.forms.length; x++ ) {
        if( oDoc.forms[x][oName] ) { return oDoc.forms[x][oName]; } }

    //check for anchor elements
    //NOTE: only anchor properties will be available,
    //NOT link properties!
    for( var x = 0; x < oDoc.anchors.length; x++ ) {
        if( oDoc.anchors[x].name == oName ) {
            return oDoc.anchors[x]; } }

    //check for any of the above within a layer in layers browsers
    for( var x = 0; document.layers && x < oDoc.layers.length; x++ ) {
        var theOb = MWJ_findObj( oName, null, oDoc.layers[x].document );
            if( theOb ) { return theOb; } }

    //check for frames, variables or functions
    if( !oFrame && window[oName] ) { return window[oName]; }
    if( oFrame && oFrame[oName] ) { return oFrame[oName]; }

    //if checking through frames, check for any of the above within
    //each child frame
    for( var x = 0; oFrame && oFrame.frames &&
      x < oFrame.frames.length; x++ ) {
        var theOb = MWJ_findObj( oName, oFrame.frames[x],
          oFrame.frames[x].document ); if( theOb ) { return theOb; } }

    return null;
}

function P7_Snap() { //v2.65 by PVII
 var x,y,ox,bx,oy,p,tx,a,b,k,d,da,e,el,tw,q0,xx,yy,w1,pa='px',args=P7_Snap.arguments;a=parseInt(a);
 if(document.layers||window.opera){pa='';}for(k=0;k<(args.length);k+=4){
 if((g=MM_findObj(args[k]))!=null){if((el=MM_findObj(args[k+1]))!=null){
 a=parseInt(args[k+2]);b=parseInt(args[k+3]);x=0;y=0;ox=0;oy=0;p="";tx=1;
 da="document.all['"+args[k]+"']";if(document.getElementById){
 d="document.getElementsByName('"+args[k]+"')[0]";if(!eval(d)){
 d="document.getElementById('"+args[k]+"')";if(!eval(d)){d=da;}}
 }else if(document.all){d=da;}if(document.all||document.getElementById){while(tx==1){
 p+=".offsetParent";if(eval(d+p)){x+=parseInt(eval(d+p+".offsetLeft"));y+=parseInt(eval(d+p+".offsetTop"));
 }else{tx=0;}}ox=parseInt(g.offsetLeft);oy=parseInt(g.offsetTop);tw=x+ox+y+oy;
 if(tw==0||(navigator.appVersion.indexOf("MSIE 4")>-1&&navigator.appVersion.indexOf("Mac")>-1)){
  ox=0;oy=0;if(g.style.left){x=parseInt(g.style.left);y=parseInt(g.style.top);}else{
  w1=parseInt(el.style.width);bx=(a<0)?-5-w1:-10;a=(Math.abs(a)<1000)?0:a;b=(Math.abs(b)<1000)?0:b;
  x=document.body.scrollLeft+event.clientX+bx;y=document.body.scrollTop+event.clientY;}}
 }else if(document.layers){x=g.x;y=g.y;q0=document.layers,dd="";for(var s=0;s<q0.length;s++){
  dd='document.'+q0[s].name;if(eval(dd+'.document.'+args[k])){x+=eval(dd+'.left');y+=eval(dd+'.top');
  break;}}}e=(document.layers)?el:el.style;xx=parseInt(x+ox+a),yy=parseInt(y+oy+b);
 if(navigator.appVersion.indexOf("MSIE 5")>-1 && navigator.appVersion.indexOf("Mac")>-1){
  xx+=parseInt(document.body.leftMargin);yy+=parseInt(document.body.topMargin);}
 e.left=xx+pa;e.top=yy+pa;}}}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// If Push and pop is not implemented by the browser
if (!Array.prototype.push) {
	Array.prototype.push = function array_push() {
		for(var i=0;i<arguments.length;i++)
			this[this.length]=arguments[i];
		return this.length;
	}
};
if (!Array.prototype.pop) {
	Array.prototype.pop = function array_pop() {
		lastElement = this[this.length-1];
		this.length = Math.max(this.length-1,0);
		return lastElement;
	}
};