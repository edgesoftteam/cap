var strValue;
function validateFunction()
{
		    if (document.forms[0].elements['address'].value == '')
		    {
		    	alert('Select Address');
		    	document.forms[0].elements['address'].focus();
		    	strValue=false;
		    }
    	    else if (document.forms[0].elements['activityType'].value == '') {
		    	alert('Select Activity Type');
		    	document.forms[0].elements['activityType'].focus();
		    	strValue=false;
		    }
    	    else if (document.forms[0].elements['description'].value == '') {
		    	alert('Please enter a Description.');
		    	document.forms[0].elements['description'].focus();
		    	strValue=false;
		    }
		    else
		    {
		    	strValue=true;
			}
		    if (strValue == true)
		    {
				if (document.forms[0].elements['startDate'].value != '')
		    	{
		    		strValue=validateData('date',document.forms[0].elements['startDate'],'Start Date is a required field');
		    	}
		    }
		    if (strValue == true)
		    {
		    	 if   ( ( document.forms[0].elements['activityType'].value == 'ABEST' ) ||
		    	        ( document.forms[0].elements['activityType'].value == 'BLDG' ) ||
		    	        (document.forms[0].elements['activityType'].value == 'BLDGFS' ) ||
		    	        (document.forms[0].elements['activityType'].value == 'DEMO' ) ||
		    	        (document.forms[0].elements['activityType'].value == 'ELEC'  ) ||
		    	        (document.forms[0].elements['activityType'].value == 'FENCE') ||
		    	        (document.forms[0].elements['activityType'].value == 'GAME' ) ||
		    	        (document.forms[0].elements['activityType'].value == 'GRAD') ||
				        (document.forms[0].elements['activityType'].value == 'LAND') ||
				        (document.forms[0].elements['activityType'].value == 'MECH') ||
				        (document.forms[0].elements['activityType'].value == 'PARK') ||
				        (document.forms[0].elements['activityType'].value == 'PAVING') ||
				        (document.forms[0].elements['activityType'].value == 'PLUM') ||
				        (document.forms[0].elements['activityType'].value == 'RETAIN') ||
				        (document.forms[0].elements['activityType'].value == 'ROOF') ||
				        (document.forms[0].elements['activityType'].value == 'SDBL') ||
				        (document.forms[0].elements['activityType'].value == 'SIGN') ||
				        (document.forms[0].elements['activityType'].value == 'TENT') ||
				        (document.forms[0].elements['activityType'].value == 'TREE') )    {
		    	         strValue=validateData('req',document.forms[0].elements['valuation'],'Valuation is a required field');
		    	   }
		     }
		    if (strValue == true)
		    {
		    	desctiption = document.forms[0].elements['description'].value;
		    	if (desctiption.length > 2000 )
		    	{
		    		alert("Description is more than 2000 charcters");
                    document.forms[0].elements['description'].focus();
		    	    strValue=false;		    		
		    	}
		    }
		    return strValue;
}


function DisplayPeriod()
{

	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46))
	{
		event.returnValue = false;
	}
	else
	{
		if (document.forms[0].valuation.value.length == 9 )
		{
			document.forms[0].valuation.value = document.forms[0].valuation.value +'.';
 		}
 		if (document.forms[0].valuation.value.length > 11 )  event.returnValue = false;
	}
}
function formatPhoneNumber(name) {

	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46))
	{
	    event.returnValue = false;
	}
	else
	{
	    if (document.forms[0].elements[name].value.length == 3 || document.forms[0].elements[name].value.length == 7 ) {
		document.forms[0].elements[name].value = document.forms[0].elements[name].value + '-';
 	    }
 	    if (document.forms[0].valuation.value.length > 12 )  event.returnValue = false;
	}
}

function checkStatus()
{
 	if (document.forms[0].elements['activityStatus'].value == '6')	{// permit issued.
	     var cur = new Date();
	     document.forms[0].elements['issueDate'].value=document.forms['activityForm'].dateChange.value;
    }
	else if (document.forms[0].elements['activityStatus'].value == '4')	{//permit final
	      var cur = new Date();
	      document.forms[0].elements['issueDate'].value=document.forms['activityForm'].dateChange.value;
	      document.forms[0].elements['completionDate'].value=document.forms['activityForm'].dateChange.value;
    }
	else if (document.forms[0].elements['activityStatus'].value == '36')	{//admin completed
	      var cur = new Date();
	      document.forms[0].elements['issueDate'].value=document.forms['activityForm'].dateChange.value;
	      document.forms[0].elements['completionDate'].value=document.forms['activityForm'].dateChange.value;
    }
}

function checkNum(data) {
   var validNum = "0123456789.";
   var i = count = 0;
   var dec = ".";
   var space = " ";
   var val = "";
   for (i = 0; i < data.length; i++)
      if (validNum.indexOf(data.substring(i, i+1)) != "-1")
         val += data.substring(i, i+1);
   return val;
}

function dollarFormatted(amount) {
	return '$' + commaFormatted(amount);
}

function commaFormatted(amount) {
		var delimiter = ","; // replace comma if desired
		var a = amount.split('.',2)
		var d = a[1];
		var i = parseInt(a[0]);
		if(isNaN(i)) { return ''; }
		var minus = '';
		if(i < 0) { minus = '-'; }
		i = Math.abs(i);
		var n = new String(i);
		var a = [];
		while(n.length > 3)
		{
		var nn = n.substr(n.length-3);
		a.unshift(nn);
		n = n.substr(0,n.length-3);
		}
		if(n.length > 0) { a.unshift(n); }
		n = a.join(delimiter);
		if(d.length < 1) { amount = n; }
		else { amount = n + '.' + d; }
		amount = minus + amount;
		return amount;
}
// end of function CommaFormatted()
function formatCurrency(field) {
    var i = parseFloat(checkNum(field.value));
    if(isNaN(i)) { i = 0.00; }
    var minus = '';
    if(i < 0) { minus = '-'; }
    i = Math.abs(i);
    i = parseInt((i + .005) * 100);
    i = i / 100;
    s = new String(i);
    if(s.indexOf('.') < 0) { s += '.00'; }
    if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
    s = minus + s;
    field.value = dollarFormatted(s);
    return true;
}
function formatCurrency(field) {
    var i = parseFloat(checkNum(field.value));
    if(isNaN(i)) { i = 0.00; }
    var minus = '';
    if(i < 0) { minus = '-'; }
    i = Math.abs(i);
    i = parseInt((i + .005) * 100);
    i = i / 100;
    s = new String(i);
    if(s.indexOf('.') < 0) { s += '.00'; }
    if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
    s = minus + s;
    field.value = dollarFormatted(s);
    return true;
}
