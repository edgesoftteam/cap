var strValue;
function validateFunction()
{
         
	    //strValue=false;
		if (document.forms[0].elements['address'].value == '')	{
		  	alert('Select Address');
		  	document.forms[0].elements['address'].focus();
		   	strValue=false;
		}else if (document.forms[0].elements['startDate'].value == ''){
		    	alert('Applied date is required');
		    	document.forms[0].elements['startDate'].focus();
		    	strValue=false;
		}else if(document.forms[0].elements['activityStatus'].value == ''){
		    	alert('Select activity Status');
		    	document.forms[0].elements['activityStatus'].focus();
		    	strValue=false;
		}
		else if (document.forms[0].elements['startDate'].value != '') {
    		strValue=validateData('date',document.forms[0].elements['startDate'],'Invalid date format');
    	}else{
    		strValue=true;
    	}
        if (strValue == true) {   	
 		    if (document.forms[0].elements['issueDate'].value != ''){
    		   strValue=validateData('date',document.forms[0].elements['issueDate'],'Invalid date format');
    	    }
    	}   
         if (strValue == true){   	
     	   if (document.forms[0].elements['completionDate'].value != '') {
    		   strValue=validateData('date',document.forms[0].elements['completionDate'],'Invalid date format');
    	   }
        }	   
        if (strValue == true)
       {   	
     	    if (document.forms[0].elements['expirationDate'].value != '')
    	   {
    		    strValue=validateData('date',document.forms[0].elements['expirationDate'],'Invalid date format');
    	    }
    	}    
 
     if (strValue == true)
     {
        if (document.forms[0].elements['activityStatus'].value == '6')//permit issued
        {
			if (document.forms[0].elements['issueDate'].value != '')
    		{
    			strValue=validateData('date',document.forms[0].elements['issueDate'],'Issue Date is a required field');
    		}
    		else
    		{
	   		    alert ("Issue Date is Required");
	   		    var chkStatus  = checkStatus();
	   		    if(chkStatus == false){
	   		    	strValue = false;
	   		    }else{
	   		    	strValue = true;
	   		    }
    		    document.forms[0].elements['issueDate'].focus();
    			strValue=false;
    		}
    	}
    }
    if (strValue == true)
     {
        if (document.forms[0].elements['activityStatus'].value == '1015')//permit issued
        {
			if (document.forms[0].elements['issueDate'].value != '')
    		{
    			strValue=validateData('date',document.forms[0].elements['issueDate'],'Issue Date is a required field');
    		}
    		else
    		{
	   		    alert ("Issue Date is Required");
	   		    checkStatus();
    		    document.forms[0].elements['issueDate'].focus();
    			strValue=false;
    		}
    	}
    }
    if (strValue == true)
    {
       if (document.forms[0].elements['activityStatus'].value == '4')//permit final
       {

			if (document.forms[0].elements['completionDate'].value != '')
    		{
    			strValue=validateData('date',document.forms[0].elements['completionDate'],'Completion Date is a required field');
    		}
    		else
    		{
    		    alert ("Completion Date is Required");
	   		    checkStatus();    		    
    		    document.forms[0].elements['completionDate'].focus();
    			strValue=false;
    		}
    	}
    }
    if (strValue == true)
    {
       desctiption = document.forms[0].elements['description'].value;
       if (desctiption.length > 2000 )
       {
	     alert("Description is more than 2000 charcters");
         document.forms[0].elements['description'].focus();
	     strValue=false;		    		
       }
    }
    
    if (strValue == true)
    {
    	if(document.forms[0].elements['activityStatus'].value == '4'){//permit final
    	      if (document.forms[0].elements['outStandingFees'].value == 'Y' ) {
    	            alert("An activity with unpaid fees can not be finaled.");
    	            strValue = false;
    	       }     
    	      else   strValue = true;
    	}
    	else if(document.forms[0].elements['activityStatus'].value == '6' ){//permit issued
    	      if (document.forms[0].elements['outStandingFees'].value == 'Y' ) {
    	            alert("An activity with unpaid fees can not be issued.");
    	            strValue = false;
    	       }     
    	      else   strValue = true;
    	} 
    	else if(document.forms[0].elements['activityStatus'].value == '36' ){//Admin Complete
    	      if (document.forms[0].elements['outStandingFees'].value == 'Y' ) {
    	            alert("An activity with unpaid fees can not be issued.");
    	            strValue = false;
    	       }     
    	      else   strValue = true;
    	} 
    	else if(document.forms[0].elements['activityStatus'].value == '1053' ){//permit issued
    	      if (document.forms[0].elements['outStandingFees'].value == 'Y' ) {
    	            alert("An activity with unpaid fees can not be issued.");
    	            strValue = false;
    	       }     
    	      else   strValue = true;
    	}              	      
   }
   return strValue;
}

var chkStatus;
function checkStatus()
{
chkStatus = true;
 	if (document.forms[0].elements['activityStatus'].value == '1053'){//Business License Activity issued
 		if (document.forms[0].elements['outStandingFees'].value == 'Y' ){
 		 	alert("An activity with unpaid fees can not be issued.");
			chkStatus = false;
		}else if (document.forms[0].elements['issueDate'].value == ''){
			var mydate = new Date();
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var thetoday=mydate.getDate();
			
			document.forms[0].elements['issueDate'].value=themonth+"/"+thetoday+"/"+theyear;
			chkStatus = false;	    			    
		}else{
			chkStatus = true;
		}    
    }else if (document.forms[0].elements['activityStatus'].value == '6')	{//permit issued
		if (document.forms[0].elements['outStandingFees'].value == 'Y' ){
			alert("An activity with unpaid fees can not be completed.");
			chkStatus = false;
		}else if (document.forms[0].elements['issueDate'].value == '') {
			var cur = new Date();
			document.forms[0].elements['issueDate'].value=document.forms[0].dateChange.value;
			chkStatus = false;
		}else{
			chkStatus = true;
		}    	 
    }else if (document.forms[0].elements['activityStatus'].value == '4'){//permit final
		if (document.forms[0].elements['outStandingFees'].value == 'Y' ){
			alert("An activity with unpaid fees can not be completed."); 
			chkStatus = false;
		}else if (document.forms[0].elements['completionDate'].value == ''){
			var cur = new Date();
			document.forms[0].elements['completionDate'].value=document.forms[0].dateChange.value;
			chkStatus = false;
		}else{
			chkStatus = true;
		} 
    }else if (document.forms[0].elements['activityStatus'].value == '36'){//admin complete
		if (document.forms[0].elements['outStandingFees'].value == 'Y' ){
			alert("An activity with unpaid fees can not be completed.");
			chkStatus = false; 
		}else if (document.forms[0].elements['completionDate'].value == ''){
			var cur = new Date();
			document.forms[0].elements['completionDate'].value=document.forms[0].dateChange.value;
			chkStatus = false;
		}else{
			chkStatus = true;
		}     
	}
	return chkStatus;
}


function issueDateChange()
{	var cur = new date(document.forms[0].elements['issueDate'].value);

	if (document.forms[0].elements['issueDate'].value == document.forms[0].elements['oldIssueDate'].value){
	      document.forms[0].elements['issueDate'].value = document.forms[0].elements['oldIssueDate'].value; 
     }	   
	 else{
	     document.forms[0].elements['issueDate'].value = cur.getMonth()+4+"/"+cur.getDate()+"/"+cur.getYear();
	 }    
}

function DisplayPeriod()
{

	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46))
	{
		event.returnValue = false;
	}
	else
	{
		if (document.forms[0].valuation.value.length == 9 )
		{
			document.forms[0].valuation.value = document.forms[0].valuation.value +'.';
 		}
 		if (document.forms[0].valuation.value.length > 11 )  event.returnValue = false;
	}
}
function formatPhoneNumber(name) {
	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46)) {
		event.returnValue = false;
	}else{
		if (document.forms[0].elements[name].value.length == 3 || document.forms[0].elements[name].value.length == 7  )	{
			document.forms[0].elements[name].value = document.forms[0].elements[name].value +'-';
 		}
 		if (document.forms[0].elements[name].value.length > 12 )  event.returnValue = false;
	}
}
