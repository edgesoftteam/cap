/* 
 * Copyright Notice. 
 * This code was originally created by yCode: http://www.yCode.com  
 * Any modifications to this code can only be made upon purchase from
 * yCode provided that this notice remains in the source code. 
 *
 */

//notes: select box has to have Style="position:absolute" in order for
// clipping to work...

//every combo box has to react to windows resize message; store combo
// boxes in this array;
var comboBoxHandlesArray = new Array();

//===============================================================
//ComboBoxObject() constructor.  Takes combo box ID as its argument
// as well as the name of the variable used for the combo box to set 
// up several call back functions.   
//--------------------------------------------------------------- 
function ComboBoxObject(strDropDownListID, strComboBoxName)
{
	this._myDropDownList = document.all[strDropDownListID];

	this._myDropDownList.selectedIndex = -1;

	//ComboBoxObject member functions
	this.retrieveSelection		= retrieveSelection;
	this.adjustComboBox			= adjustComboBox;
	this.processComboBoxSelect	= processComboBoxSelect;
	this.handleData				= handleData; 
	this.onlyAllowedEntries		= onlyAllowedEntries;
	this.setDefaultByValue		= setDefaultByValue;
	this.setDefaultByText		= setDefaultByText;
	this.setDefaultIndex		= setDefaultIndex;
	this.setSort				= setSort;
	this.doLookup				= doLookup;
	this.setDoLookup			= setDoLookup;
	this.setCaseInsensitive		= setCaseInsensitive;
	this.focus					= focus;
	this.addValue				= addValue;
	this.deleteValue			= deleteValue;
	this.retrieveComboBoxValue	= retrieveComboBoxValue;

	//flag to allow only provided entries in the list
	//NOTE: this only warns the user once
	this._bOnlyAllowedEntries	= false;


	//flag to set the list sorted
	this._bIsSorted				= false;

	//flag whether to look up values as the user types in the combo box
	this._bDoLookup				= false;
	//flag whether this lookup is case insensitive
	this._bIsCaseInsensitive	= false;

	//TODO: fix this when the new service pack is out
	//serves as a helper variable to store the old value in the text box
	//(necessary since keyCode can't catch the arrow key)
	//http://support.microsoft.com/support/kb/articles/q233/2/80.ASP
	//Event Keycode Does Not Return Proper Value for ARROW Keys
	this._strPrevValue			= "";

	
	var strInputCellID = strDropDownListID + "_text";
	if (document.all[strInputCellID] != null)
	{
		alert("The following id: '" + strInputCellID +"' is used internally by the Combo Box!\r\n"+
			"Use of this id in your page may cause malfunction.  Please use another id for your controls.");
	}

	var strInputCell = "<INPUT type='text' id=" + strInputCellID + 
					   " name=" + strInputCellID +
					   " onblur='" + strComboBoxName + ".handleData()' " + 
					   " onkeyup='" + strComboBoxName + ".doLookup()' " +
					   " style='display: none; position: absolute' value='' class='textbox' >";

	this._myDropDownList.insertAdjacentHTML("afterEnd", strInputCell);
	this._myEditCell = document.all[strInputCellID];

	var strHiddenCellID = strDropDownListID + "_value";
	if (document.all[strHiddenCellID] != null)
	{
		alert("The following id: '" + strHiddenCellID + "' is used internally by the Combo Box!\r\n"+
			"Use of this id in your page may cause malfunction.  Please use another id for your controls.");
	}


	var strHiddenCell = "<INPUT type='hidden' "+ 
					" id=" + strHiddenCellID +
					" name=" + strHiddenCellID + " >";

	this._myDropDownList.insertAdjacentHTML("afterEnd", strHiddenCell);
	this._myHiddenCell = document.all[strHiddenCellID];

	this._bIsAdjusting = false;
	this.adjustComboBox();

	comboBoxHandlesArray[comboBoxHandlesArray.length] = this;
}

//============================================================
//onlyAllowedEntries() -- sets a flag on weather to allow 
//  only entries that already exist in the drop down list
//------------------------------------------------------------
function onlyAllowedEntries(bOnlyAllowed)
{
	this._bOnlyAllowedEntries = bOnlyAllowed;
}

//============================================================
//setDoLookup() -- sets a flag on weather to do lookup of 
// values stored in the drop down list
//------------------------------------------------------------
function setDoLookup(bDoLookup)
{
	this._bDoLookup = bDoLookup;
}

//====================================================================
//setCaseInsensitive() -- flag whether this lookup is case insensitive
//--------------------------------------------------------------------
function setCaseInsensitive(bCaseInsensitive)
{
	this._bIsCaseInsensitive = bCaseInsensitive;
}

//============================================================
//setSort() -- makes list sorted if the first argument is true 
// and no sorting will be done if it's false.  The second 
// argument, bSortNumbers, specifies whether to treat values 
// as numbers
//------------------------------------------------------------
function setSort(bSort, bSortNumbers)
{
	this._bIsSorted = bSort;
	var iIndex;

	if (bSort)
	{
		if (setSort.arguments.length == 1)
		{
			bSortNumbers = false;
		}

		var len = this._myDropDownList.options.length;

		//do simple buble sort
		var minIndex;
		var minOptionText;
		var oOption;

		var tempValue;
		var j;

		for (iIndex=0; iIndex<len; iIndex++)
		{
			minIndex = iIndex;
			minOptionText = this._myDropDownList.options(iIndex).text;

			if (bSortNumbers)
			{
				minOptionText = parseFloat(minOptionText);
			}

			for (j=iIndex+1; j<len; j++)
			{
				tempValue = this._myDropDownList.options(j).text;
				if (bSortNumbers)
				{
					tempValue = parseFloat(tempValue);
				}

				if (minOptionText > tempValue)
				{
					minIndex = j;
					minOptionText = this._myDropDownList.options(j).text;
				}
			}

			//now swap iIndex with minIndex
			if (iIndex != minIndex)
			{
				oOption			= document.createElement("OPTION");
				oOption.text	= minOptionText;
				oOption.value	= this._myDropDownList.options(minIndex).value;

				//has to come before renewals
				var curIndex = this._myDropDownList.selectedIndex; 

				this._myDropDownList.options.remove(minIndex);
				this._myDropDownList.add(oOption, iIndex);

				//now update the current selection in the list box
				if (curIndex == minIndex)
				{
					this._myDropDownList.selectedIndex = iIndex;
				}
				else
				if ( (curIndex < minIndex) && (curIndex >= iIndex) )
				{
					this._myDropDownList.selectedIndex = curIndex + 1;
				}
			
			} //end of if (iIndex != minIndex)
		} //end of for
	} // end of if (bSort)
}
//============================================================
//handleData() -- can do any data handling for user input
//------------------------------------------------------------
function handleData()
{
	var myDropDownList	= this._myDropDownList;
	var myEditCell		= this._myEditCell;
	var myHiddenCell	= this._myHiddenCell;
	var iIndex;

	myHiddenCell.value = myEditCell.value;
	myDropDownList.selectedIndex = -1;

	if (myEditCell.value == "")
	{
		return;
	}

	var len = myDropDownList.options.length;
	
	//check for duplicates and return if a duplicate exists;
	//at the same time, update the value for the hidden field
	for (iIndex=0; iIndex<len; iIndex++)
	{
		var str1 = myDropDownList.options(iIndex).text;
		var str2 = myEditCell.value;

		if (this._bIsCaseInsensitive)
		{
			str1 = str1.toUpperCase();
			str2 = str2.toUpperCase();
		}
		
		if (str1 == str2)
		{
			myDropDownList.selectedIndex = iIndex;
			myHiddenCell.value = myDropDownList.options(iIndex).value;
			return;
		}
	}

	if (this._bOnlyAllowedEntries)
	{
		myDropDownList.focus();	

		alert("'" + myEditCell.value + "' is not allowed");
		this._myDropDownList.selectedIndex = -1;
		this._myEditCell.select();
		
		return;
	}

}

//============================================================
//addValue() -- adds a value to the combo box; if the argument
// is not present or is empty, adds the current combo box value.
//------------------------------------------------------------
function addValue( strValue )
{
	if ((strValue == null) || (strValue == "") )
	{
		strValue = this._myEditCell.value;
	}

	var len = this._myDropDownList.options.length;
	var iIndex;

	this._myEditCell.value = strValue;
	this._myEditCell.select();
	this._myEditCell.focus();

		//check for duplicates and return if a duplicate exists
	for (iIndex=0; iIndex<len; iIndex++)
	{
		if (this._myDropDownList.options(iIndex).text == strValue)
		{
			this._myDropDownList.selectedIndex = iIndex;
			return;
		}
	}

	var oOption		= document.createElement("OPTION");
	oOption.text	= strValue;
	oOption.value	= strValue;
	this._myDropDownList.add(oOption);
	this._myDropDownList.selectedIndex = len;

	if (this._bIsSorted)
	{
		this.setSort(true);
	}

	window.resizeBy(0, -1);
	this.adjustComboBox();
	window.resizeBy(0, 1);
}


//============================================================
//deleteValue() -- deletes the value from the combo box; if the 
// argument is not present or is empty, deletes the current combo 
// box value.
//------------------------------------------------------------
function deleteValue( strValue )
{
	if ((strValue == null) || (strValue == "") )
	{
		strValue = this._myEditCell.value;
	}

	var len = this._myDropDownList.options.length;
	var iIndex;

	var foundIndex = -1;
	for (iIndex=0; iIndex<len; iIndex++)
	{
		if (this._myDropDownList.options(iIndex).text == strValue)
		{
			foundIndex = iIndex;
			break;
		}
	}

	if (foundIndex == -1)
	{
		return;
	}
	
	
	this._myDropDownList.remove(foundIndex);

	this._myDropDownList.selectedIndex = -1;
	this._myEditCell.value = "";

	this.adjustComboBox();
}
//==================================================================
//adjustComboBox() -- positions the edit field over the drop down list
//have to work on clip rectangle since zIndex is not supported by
//the SELECT
//------------------------------------------------------------------
function adjustComboBox()
{
	if (!this._bIsAdjusting)
	{
		this._bIsAdjusting = true;
		this._myEditCell.style.display="none";
		this._myDropDownList.style.position="static";

		//multiply top and left margins by 1 to convert from string to a number
		this._myEditCell.style.posLeft		= calculate_absolute_X(this._myDropDownList); 
		this._myEditCell.style.posTop		= calculate_absolute_Y(this._myDropDownList) + 1;
		this._myEditCell.style.posWidth		= this._myDropDownList.offsetWidth - 16;  // 16 THIS IS THE WIDTH OF THE DROP DOWN ARROW
		this._myEditCell.style.posHeight	= this._myDropDownList.offsetHeight;

		this._myDropDownList.style.position	="absolute";
		this._myDropDownList.style.posLeft	= this._myEditCell.style.posLeft;
		this._myDropDownList.style.posTop	= this._myEditCell.style.posTop;

		this.ComboWidth = this._myDropDownList.offsetWidth;
		var strClipRectangle = "rect(0 " + 
							(this._myDropDownList.offsetWidth) + " " +
							this._myDropDownList.offsetHeight + " " +
							(this._myEditCell.style.posWidth - 2 ) + ")";

		this._myDropDownList.style.clip = strClipRectangle;
		this._myEditCell.style.display="";
		this._bIsAdjusting = false;
	}
}

//=============================================================
//processComboBoxSelect() sets the edit field value with the
//selection from the drop down box
//-------------------------------------------------------------
function processComboBoxSelect()
{
   
	var tempIndex = this._myDropDownList.selectedIndex;
	var tempOption = this._myDropDownList.options[tempIndex];
	this._myEditCell.value  = tempOption.text;

	this._myEditCell.focus();
	this._myEditCell.select();

		//this is necessary to make sure that onchange is always 
		//fired EVEN IF the select box value didn't change
	this._myDropDownList.selectedIndex=-1;
}

//==========================================================
//retrieveSelection() -- returns the value of the combo box
//----------------------------------------------------------
function retrieveSelection()
{
	return this._myEditCell.value;
}

//==========================================================
//retrieveComboBoxValue() -- returns the value posted by the form
//----------------------------------------------------------
function retrieveComboBoxValue()
{
	return this._myHiddenCell.value;
}

/*============================================================
  setDefaultByText() -- sets the value that is shown initially 
  in the combo box if this value is present among the list box 
  text strings specified by option tags; if the value is not 
  found; the combo box becomes empty.  Match is case sensitive.
------------------------------------------------------------*/
function setDefaultByText(strText)
{
	var len = this._myDropDownList.options.length;
	var iIndex;

	for (iIndex=0; iIndex<len; iIndex++)
	{
		if (this._myDropDownList.options(iIndex).text == strText)
		{
			this._myDropDownList.selectedIndex = iIndex;
			this._myEditCell.value = this._myDropDownList.options(iIndex).text;
			this._myHiddenCell.value = this._myDropDownList.options(iIndex).value;
			return;
		}
	}

	this._myDropDownList.selectedIndex = -1;
	this._myEditCell.value = "";
}

/*============================================================
  setDefaultByValue() -- sets the value that is shown initially 
  in the combo box if this value is present among the list box 
  list of values specified by option tags; if the value is not 
  found; the combo box becomes empty. Match is case sensitive.
  ------------------------------------------------------------*/
function setDefaultByValue(strValue)
{
	var len = this._myDropDownList.options.length;
	var iIndex;

	for (iIndex=0; iIndex<len; iIndex++)
	{
		if (this._myDropDownList.options(iIndex).value == strValue)
		{
			this._myDropDownList.selectedIndex = iIndex;
			this._myEditCell.value = this._myDropDownList.options(iIndex).text;
			this._myHiddenCell.value = this._myDropDownList.options(iIndex).value;
			return;
		}
	}

	this._myDropDownList.selectedIndex = -1;
	this._myEditCell.value = "";
}


//============================================================
//setDefaultIndex() -- sets the value that is shown initially 
//in the combo box to the value of the list option with this 
//index value.  Combo box becomes  blank if this index is out 
//of range.
//------------------------------------------------------------
function setDefaultIndex(iIndex)
{
	var len = this._myDropDownList.options.length;
	if ((iIndex >=0) && (iIndex < len))
	{
		this._myDropDownList.selectedIndex = iIndex;
		this._myEditCell.value = this._myDropDownList.options(iIndex).text;
		this._myHiddenCell.value = this._myDropDownList.options(iIndex).value;
		return;
	}

	this._myEditCell.value = "";
}

//================================================================
//doLookup() -- looks up the value if the list box (if exists) 
//as the user types in the combo box
//----------------------------------------------------------------
function doLookup()
{
	if (this._bDoLookup)
	{
		//TODO: fix this when the fix is ready
		//event.keyCode returns funky values; therefore use the char itself
		//http://support.microsoft.com/support/kb/articles/q233/2/80.ASP
		//Event Keycode Does Not Return Proper Value for ARROW Keys
		//if ((event.keyCode < 47) || (event.keyCode > 255))
		//{
		//	return;
		//}
		

		if (event.keyCode < 32)  //backspace; del is not supported due to the bug above
		{
			return;
		}

		var curText		= this._myEditCell.value;
		var prevText	= this._strPrevValue;
		var iIndex;

		if ((curText == "")
			|| (curText == prevText) //say, if an arrow key was pressed	
			)
		{
			this._strPrevValue = curText;
			return;
		}

		var len = this._myDropDownList.options.length;
		var tempString;
		
		for (iIndex=0; iIndex<len; iIndex++)
		{
			tempString = this._myDropDownList.options(iIndex).text;

			if (this._bIsCaseInsensitive)
			{
				tempString = tempString.toUpperCase();
				curText = curText.toUpperCase();
			}

			if (tempString.indexOf(curText) == 0)
			{
				var helperString = this._myDropDownList.options(iIndex).text;

				this._myEditCell.value = this._myEditCell.value + helperString.substr(curText.length);
				this._myDropDownList.selectedIndex = iIndex;
				this._strPrevValue = this._myEditCell.value;

				var tmpRange = this._myEditCell.createTextRange();
				tmpRange.moveStart("character", curText.length);
				tmpRange.select();
				return;
			}
		}
	}
}

//================================================================
//focus() -- sets the focus to the combo box
//----------------------------------------------------------------
function focus()
{
	this._myEditCell.focus();
	this._myEditCell.select();	
}

//================================================================
//every combo box has to react to windows resize message; this 
// function would do just that.  Handles to Combo Boxes are stored
// in comboBoxHandlesArray
//----------------------------------------------------------------
function AdjustComboBoxes()
{
	var iIndex;
	for (iIndex=0; iIndex < comboBoxHandlesArray.length; iIndex++)
	{
		comboBoxHandlesArray[iIndex].adjustComboBox();
	}
}
