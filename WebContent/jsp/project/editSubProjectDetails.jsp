<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import='elms.agent.*,java.lang.String.*' %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
<%
String contextRoot = request.getContextPath();
java.util.List sptList = (java.util.List) request.getAttribute("sptList");
pageContext.setAttribute("sptList", sptList);
java.util.List spstList = (java.util.List) request.getAttribute("spstList");
pageContext.setAttribute("spstList", spstList);
String subProjectId = request.getParameter("subProjectId");
if(subProjectId == null) subProjectId = "";
String subProjectName = (String)request.getAttribute("subProjectName");
if(subProjectName == null) subProjectName = "";
String subProjectNumber = (String)request.getAttribute("subProjectNumber");
if(subProjectNumber == null) subProjectNumber = "";
java.util.List subProjectStatuses = (java.util.List) request.getAttribute("subProjectStatuses");
if (subProjectStatuses == null) subProjectStatuses = new java.util.ArrayList();
pageContext.setAttribute("subProjectStatuses", subProjectStatuses);
String lsoAddress = (String)session.getAttribute("lsoAddress");
if (lsoAddress == null ) lsoAddress = "";
%>
<script>
function onchangeprocess()
{
  document.forms[0].action='<%=contextRoot%>/editSubProjectDetails.do?from=ptList';
  document.forms[0].submit();
  return true;
}
function onclickprocess()
{
  if(document.forms[0].subType.value == ""){
	  alert("Please select Sub Type");
	  document.forms[0].subType.focus();
	  return false;
	}
  document.forms[0].action='<%=contextRoot%>/saveSubProjectDetails.do';
  document.forms[0].submit();
  return true;
}
function onclickprocessForBTBL()
{
  document.forms[0].action='<%=contextRoot%>/saveSubProjectDetails.do';
  document.forms[0].submit();
  return true;
}
</script>
</head>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form   name="subProjectForm" type="elms.control.beans.SubProjectForm" action="">
<html:hidden property="subProjectId" value="<%=subProjectId%>"/>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><font class="con_hdr_3b">Edit Sub-Project Manager&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><br><br>
                </td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                            <tr>
	                                <td width="99%" class="tabletitle">Sub Project Details</td>
	                                <td width="1%" class="tablebutton"><nobr>
	                                    <html:reset value="Cancel" styleClass="button" onclick="history.back();"/>
										<%if((subProjectName.startsWith("BT")) || (subProjectName.startsWith("BL"))){%>
	                                    <html:button property="Update" value="Update" styleClass="button" onclick="return onclickprocessForBTBL()"></html:button>
										<%}else{ %>
										<html:button property="Update" value="Update" styleClass="button" onclick="return onclickprocess()"></html:button>
										<%} %>
	                                </td>
                               </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr valign="top">
                                <td class="tablelabel">Sub Project #</td>
                                <td class="tabletext"><%=subProjectNumber%></td>
                                <td class="tablelabel">Sub Project Name</td>
                                <td class="tabletext"><%=subProjectName%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Type</td>
                                <td class="tabletext">
                                    <html:select property="type" styleClass="textbox" onchange="return onchangeprocess()">
                                        <html:options collection="sptList" property="subProjectTypeId" labelProperty="description"/>
                                   </html:select>
                               </td>
                               <td class="tablelabel">Sub-Type</td>
                               <td class="tabletext"><html:select property="subType" styleClass="textbox"><html:option value="">Please Select</html:option> <html:options collection="spstList" property="subProjectSubTypeId" labelProperty="description"/> </html:select></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Description</td>
                                <td class="tabletext"> <html:textarea property="description"  style="height:150;width:350;" styleClass="textbox"/> </td>
                                <td class="tablelabel">Status</td>
                                <td class="tabletext"><html:select styleClass="textbox" property="status">
                                	<html:options collection="subProjectStatuses" property="statusId" labelProperty="description"/>
                                	</html:select></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Case Log</td>
                                <td class="tabletext"><html:select styleClass="textbox" property="caseLogId">
                                	<html:options collection="caseLogList" property="statusId" labelProperty="description"/>
                                	</html:select></td>
                                <td class="tablelabel">Label</td>
                                <td class="tabletext"><html:text styleClass="textbox" property="label" maxlength="25"/> </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </td>
        <td width="1%"></td>
    </tr>
</table>
</html:form>
</body>
</html:html>
