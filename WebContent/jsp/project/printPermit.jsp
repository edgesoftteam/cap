<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="javax.sql.*,sun.jdbc.rowset.*,java.util.*, elms.util.*" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<app:checkLogon/>
<html:html>
<head>
<html:base/>
<title>City of Burbank : Online Business Center : Print permit</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>

<SCRIPT>
function forwardAsp(window){
var macroName;
macroName ='';
//alert(document.frmPrintPermit.activityNumber.value);
//alert(document.frmPrintPermit.printerName.value);
if (document.frmPrintPermit.activityType.value == 'AFTH' )
	{
		macroName='afterhours.mac';
	}
else if (document.frmPrintPermit.activityType.value == 'CLSE' )
	{
		macroName='encroachment.mac';
	}
else if (document.frmPrintPermit.activityType.value == 'CLSB' )
	{
		macroName='excavation.mac';
	}
else if (document.frmPrintPermit.activityType.value == 'CENTP' )
	{
		macroName='fire.mac';
	}
else if (document.frmPrintPermit.activityType.value == 'GARA' )
	{
		macroName='garagesale.mac';
	}
else if (document.frmPrintPermit.activityType.value == 'CLSD' )
	{
		macroName='heavyhaul.mac';
	}
else if (document.frmPrintPermit.activityType.value == 'INROW' )
	{
		macroName='publicrowuse.mac';
	}
else if ((document.frmPrintPermit.activityType.value == 'SPEV') || (document.frmPrintPermit.activityType.value == 'FILM') || (document.frmPrintPermit.activityType.value == 'PHOTO'))
	{
		macroName='specialevents.mac';
	}
else if (document.frmPrintPermit.activityType.value == 'TEMPRK' )
	{
		macroName='temporaryparking.mac';
	}
else if (document.frmPrintPermit.activityType.value == 'TREE' )
	{
		macroName='temporaryparking.mac';
	}
else if (document.frmPrintPermit.activityType.value == 'CLSC' )
	{
		macroName='utility.mac';
	}
else if (document.frmPrintPermit.activityType.value == 'INSP' )
	{
		macroName='fieldinspection.mac';
	}
else if (document.frmPrintPermit.activityType.value == 'LEDGER' )
	{
		macroName='ledger.mac';
	}
else
	{
		macroName='general.mac';
	}
str = "param=" + "abc" + document.frmPrintPermit.activityNumber.value+document.frmPrintPermit.printerName.value;
//str= str & 'http://sql2/aspexec.asp?param=' + document.frmPrintPermit.activityNumber.value + '|' + document.frmPrintPermit.printerName.value + '&macName=' & macroName;
//alert(str);
str= document.frmPrintPermit.activityNumber.value + "|" + document.frmPrintPermit.printerName.value + "&macName=" + macroName;
//alert(str);
document.frmPrintPermit.action="http://10.100.7.24:8080/elms/reports.jsp?param=" + str;
document.frmPrintPermit.target='_new';
document.frmPrintPermit.submit();
}
</script>
<%
		String activityNumber = request.getParameter("actNbr");
		String activityType= request.getParameter("actType");
%>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="frmPrintPermit" method="post" action="">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="center"><font class="con_hdr_3b"><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td background="../images/site_bg_B7C1CB.jpg">Print Permit</td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" cellpadding="2" cellspacing="1">
                    <tr>
                      <td width="20%" class="tablelabel">Activity
                        Number</td>
                      <td width="80%" class="tabletext" >
                        <input type="text" size="20" name="activityNumber" value="<%=activityNumber%>" class="textbox">
                      </td>
                    </tr>
                    <tr>
                      <td width="20%" class="tablelabel">Select Printer
                        </td>
                      <td width="80%" class="tabletext" >
                        <select name="printerName" class="textbox">
                          <option value="">&nbsp;</option>
                          <option value="BSAdmin">BS Admin</option>
                          <option value="BSPermit">BS Permit</option>
                          <option value="BSInspection">BS Inspection</option>
                          <option value="BWPrinter">IT Printer</option>
                          <option value="FloraPrinter">Flora Printer</option>
                          <option value="CPrinter">Color Printer</option>
                        </select>
                      </td>
                    </tr>
                    <input type="hidden" name="activityType" value="<%=activityType%>">
                    <tr class="tabletext">
                      <td colspan="2" align="CENTER">
                        <input type="button" value="Print" name="Submit" class="button" onclick="forwardAsp('window2');">
                        &nbsp;
                        <input type="button" value="Cancel" name="Cancel" class="button" onclick="javascript:history.back()">
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</form>
</body>
</html:html>