<%@ page import='elms.agent.*' %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon />
<%     String contextRoot = request.getContextPath();  %>
<html:html>
<head>
<html:base />
<title>City of Burbank : Online Business Center : Add Plan Check</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0"
	marginwidth="0" marginheight="0">

<%
java.util.List planCheckStatuses = LookupAgent.getPlanCheckStatuses();
pageContext.setAttribute("planCheckStatuses", planCheckStatuses);
// Getting the employees as plan check engineers from the groups  1-Inspector, 6-Supervisor, 7-Engineer , 11- Permit Tech
java.util.List engineerUsers = ActivityAgent.getEngineerUsers(-1);
pageContext.setAttribute("engineerUsers", engineerUsers);
String activityId = (String)request.getAttribute("activityId");
if(activityId == null) activityId ="";
String lsoAddress = (String)session.getAttribute("lsoAddress");
if (lsoAddress == null ) lsoAddress = "";
String psaInfo = (String)session.getAttribute("psaInfo");
if (psaInfo == null ) psaInfo = "";
String planCheckId =(String)request.getAttribute("planCheckId");
if(planCheckId ==null) planCheckId = "";

%>
<script language="JavaScript" src="../script/formValidations.js"></script>
<script language="JavaScript" src="../script/calendar.js"></script>
<script>
var strValue;
function validateFunction() {
   	    if (document.forms['planCheckForm'].elements['planCheckStatus'].value == '-1')  {
	    	alert('Select Plan Check Status');
	    	document.forms['planCheckForm'].elements['planCheckStatus'].focus();
	    	strValue=false;
	    } else if (document.forms['planCheckForm'].elements['engineerId'].value == '-1')  {
	    	alert('Select Engineer');
	    	document.forms['planCheckForm'].elements['engineerId'].focus();
	    	strValue=false;
		}else if (document.forms[0].elements['planCheckDate'].value == '')  {
	    	alert('Select Plan Check Date');
	    	document.forms[0].elements['planCheckDate'].focus();
	    	return false;
	    }
		else strValue=true;
		return strValue;
}

function onclickprocess(strval)
{
    if (strval == 'dp') {
      document.forms['planCheckForm'].action='<%=contextRoot%>/deletePlanCheck.do?psaId=<%=activityId%>';
      document.forms['planCheckForm'].submit();
    }

}
function goBack(button)
{
	//button.value='Please Wait..';
	button.disabled=true;
	document.location.href='<%=contextRoot%>/viewActivity.do?activityId=<%=activityId%>';
}
</script>
<html:form action="/savePlanCheck" onsubmit="return validateFunction();">
	<html:hidden property="activityId" />
	<html:hidden property="planCheckId" value="<%=planCheckId%>" />

	<table width="100%" border="0" cellspacing="10" cellpadding="0">
		<TBODY>
			<tr>
				<td><font class="con_hdr_3b">Add Plan Check&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font
					class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br></td>
			</tr>
			<tr valign="top">
				<td width="99%">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="99%" background="../images/site_bg_B7C1CB.jpg"><font
											class="con_hdr_2b">Edit</td>
										<td width="1%"><img src="../images/site_hdr_split_1_tall.jpg"
											width="26" height="25"></td>
										<td width="1%" class="tablelabel">
											<nobr>
												<html:button property="back" value="Back" styleClass="button" onclick="goBack(this);" />&nbsp;
												<html:submit value="Save" styleClass="button" onclick="return validateFunction();" /> &nbsp;
											</nobr>
										</td>
									</tr>
								</table>
								</td>
							</tr>
							<tr>
								<td background="../images/site_bg_B7C1CB.jpg">
								<table width="100%" border="0" cellspacing="1" cellpadding="2">
									<tr>
										<td class="tablelabel"><font
											class="con_text_1">Plan Check Status</td>
										<td class="tablelabel"><font
											class="con_text_1">Date</td>
										<td class="tablelabel"><font
											class="con_text_1">Engineer</td>
									</tr>
									<tr valign="top">
										<td class="tabletext"><html:select property="planCheckStatus"
											styleClass="textbox">
											<html:options collection="planCheckStatuses" property="code"
												labelProperty="description" />
										</html:select></td>
										<td class="tabletext"><nobr> <html:text property="planCheckDate"
											size="25" styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"/> <a
											href="javascript:show_calendar('planCheckForm.planCheckDate');"
											onmouseover="window.status='Calendar';return true;"
											onmouseout="window.status='';return true;"> <img
											src="../images/calendar.gif" width="16" height="15" border=0></a></nobr>
										</td>
										<td class="tabletext"><html:select property="engineerId"
											styleClass="textbox">
											<html:options collection="engineerUsers" property="userId"
												labelProperty="engineerId" />
										</html:select></td>
									</tr>
									<tr>
										<td class="tablelabel"><font
											class="con_text_1">Comments</td>
										<td class="tabletext" colspan="2"><html:textarea property="comments" cols="75" rows="7" styleClass="textbox" /></td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
			</tr>
	</table>
</html:form>
</body>
</html:html>

