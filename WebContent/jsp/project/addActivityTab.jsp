<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="elms.control.beans.ActivityForm,elms.util.*,java.text.SimpleDateFormat,java.util.*,elms.agent.*,elms.common.Constants"%>
<%@ page import="elms.util.db.*"%>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested"%>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security"%>

<%
String contextRoot = request.getContextPath();
int count = (Integer) request.getAttribute("count");
pageContext.setAttribute("count", count);


%>
<%!
List streets = new ArrayList(); // for the Street list
%>
<%
 streets = (List) AddressAgent.getStreetArrayList();
 request.setAttribute("streets",streets);
%>
<app:checkLogon />
<html:html>
<head>
<html:base />


<title><%=Wrapper.getCityName()%> : <%=Wrapper.getPrivateLabel()%></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">

<script language="JavaScript" src="../script/calendar.js"></script>
<script language="JavaScript" src="../script/formValidations.js"></script>
<script language="JavaScript" src="../script/addActivity.js"></script>

<link rel="stylesheet" href="../css/CalendarControl.css" type="text/css">
<script language="JavaScript" src="../script/CalendarControl.js"></script>

<script language="javascript">

function fromSubProjectName()
{
    document.forms[0].action='<%=contextRoot%>/addActivityTab.do?from=subProjectName';
    document.forms[0].elements['subProjectType'].disabled=true;
    document.forms[0].elements['activityType'].value = "-1";
    document.forms[0].submit();
    return true;
}
function fromActivityType()
{
    document.forms[0].action='<%=contextRoot%>/addActivityTab.do?from=activityType';
    document.forms[0].submit();
    return true;

}

function checkDate(label,val,place){
	var strValue;
	if(val!=''){
	var dtStr=val;
	//alert(val);
	var daysInMonth = DaysArray(12);
	var pos1=dtStr.indexOf(dtCh);
	var pos2=dtStr.indexOf(dtCh,pos1+1);
	var strMonth=dtStr.substring(0,pos1);
	var strDay=dtStr.substring(pos1+1,pos2);
	var strYear=dtStr.substring(pos2+1);
	strYr=strYear;
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1);
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1);
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1);
		}
	month=parseInt(strMonth);
	day=parseInt(strDay);
	year=parseInt(strYr);
	//alert(val);
	if (pos1==-1 || pos2==-1){
		alert("The date format for "+label +" should be : mm/dd/yyyy");
		place.focus();
		strValue = false;
		return strValue;
	}
	if (month<1 || month>12){
		alert("Please enter a valid month for "+label +"");
		place.focus();
		strValue= false;
		return strValue;
	}
	if (day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		alert("Please enter a valid day for "+label +"");
		place.focus();
		strValue= false;
		return strValue;
	}
	if (strYear.length != 4 || year==0 ){
		alert("Please enter a valid 4 digit year for "+label +"");
		place.focus();
		strValue= false;
		return strValue;
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		alert("Please enter a valid date for "+label +"");
		place.focus();
		strValue= false;
		return strValue;
	}
	}
	strValue= true;
	return strValue;
}
function checkInt(label,val,place){
	var strValue;
		if(isNaN(val))	{
		       alert("Please enter "+label+" with Numeric value");
		       place.focus();
		       strValue= false;

	     }else {strValue= true; }  return strValue;
}
function checkString(label,val,place){
	var strValue;
	   var words = val.length;
	    if(words>254)	{
		       alert("Please enter "+label+" with less then 255 characters value");
		       place.focus();
		       strValue= false;

	     }else {strValue= true; }	    return strValue;
}


function saveCustom(count){

	 if (count>0) {

			for (i=0;i<count;i++) {

				var fieldType = document.forms[0].elements['customFieldList[' + i + '].fieldType'].value ;
				var fieldValue = document.forms[0].elements['customFieldList[' + i + '].fieldValue'].value ;
				var fieldLabel = document.forms[0].elements['customFieldList[' + i + '].fieldLabel'].value ;
				var fieldReq = document.forms[0].elements['customFieldList[' + i + '].fieldRequired'].value ;
				var place= document.forms[0].elements['customFieldList[' + i + '].fieldValue'];

				if(fieldType == 'INTEGER'){         if(fieldReq=='Y'  && fieldValue==''){   alert(fieldLabel+" is a required field"); place.focus(); return false; }
					  if(checkInt(fieldLabel,fieldValue,place)){
					 			}else { return false; }
				}

				if(fieldType == 'STRING'){ 			if(fieldReq=='Y'  && fieldValue==''){   alert(fieldLabel+" is a required field"); place.focus(); return false;  }
						 	if(checkString(fieldLabel,fieldValue,place)){
				                   }else { return false; }
				}

				if(fieldType == 'DATE'){ 	         if(fieldReq=='Y'  && fieldValue==''){   alert(fieldLabel+" is a required field"); place.focus(); return false;  }
				   		if(checkDate(fieldLabel,fieldValue,place)){
									}else { return false; }
				}

			} //for

			document.forms[0].action='<%=contextRoot%>/saveActivityTab.do';
	        document.forms[0].elements['Save'].disabled=true;
	        document.forms[0].submit();
	        return true;

	 } // end count
			else {
			         return false;
				}
}
function save(){
count = <%=count%>;
 if(count>0){
		saveCustom(count);
  }else{
	       document.forms[0].action='<%=contextRoot%>/saveActivityTab.do';
           document.forms[0].elements['Save'].disabled=true;
           document.forms[0].submit();
           return true;
	}
}

function fromCrossStreet()
{
    document.forms[0].action='<%=contextRoot%>/addActivityTab.do?from=crossStreet';
    document.forms[0].elements['crossStreetName2'].disabled=true;
    document.forms[0].submit();
    return true;
}


function checkLocationType(locationType){
	if(locationType == 1){
		document.getElementById("address").style.display =  '';
		document.getElementById("crossStreet").style.display =  'none';
		document.getElementById("range").style.display =  'none';
		document.getElementById("landmark").style.display =  'none';
		document.getElementById("parcel").style.display =  'none';
		document.getElementById("map").style.display =  'none';
	} else if(locationType == 2){
		document.getElementById("address").style.display =  'none';
		document.getElementById("crossStreet").style.display =  '';
		document.getElementById("range").style.display =  'none';
		document.getElementById("landmark").style.display =  'none';
		document.getElementById("parcel").style.display =  'none';
		document.getElementById("map").style.display =  'none';
	} else if(locationType == 3){
		document.getElementById("address").style.display =  'none';
		document.getElementById("crossStreet").style.display =  'none';
		document.getElementById("range").style.display =  '';
		document.getElementById("landmark").style.display =  'none';
		document.getElementById("parcel").style.display =  'none';
		document.getElementById("map").style.display =  'none';
	} else if(locationType == 4){
		document.getElementById("address").style.display =  'none';
		document.getElementById("crossStreet").style.display =  'none';
		document.getElementById("range").style.display =  'none';
		document.getElementById("landmark").style.display =  '';
		document.getElementById("parcel").style.display =  'none';
		document.getElementById("map").style.display =  'none';
	} else if(locationType == 5){
		document.getElementById("address").style.display =  'none';
		document.getElementById("crossStreet").style.display =  'none';
		document.getElementById("range").style.display =  'none';
		document.getElementById("landmark").style.display =  'none';
		document.getElementById("parcel").style.display =  '';
		document.getElementById("map").style.display =  'none';
	} else if(locationType == 6){
		document.getElementById("address").style.display =  'none';
		document.getElementById("crossStreet").style.display =  'none';
		document.getElementById("range").style.display =  'none';
		document.getElementById("landmark").style.display =  'none';
		document.getElementById("parcel").style.display =  'none';
		document.getElementById("map").style.display =  '';
	}
}
</script>
</head>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="checkLocationType(document.forms[0].locationType.value);">

<html:form name="activityForm" type="elms.control.beans.ActivityForm" action="">
	<table width="100%" border="0" cellspacing="10" cellpadding="0">
		<tr valign="top">
			<td width="99%">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><font class="con_hdr_3b">Add Permit</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
					<br>
					</td>
				</tr>
				<tr>
					<td><font class="con_text_red1"><html:errors/></font></td>
				</tr>
				<tr>
					<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="99%" class="tabletitle"></td>
									<td width="1%" class="tablebutton"><nobr>
										<html:button property="Save" value=" Save " styleClass="button" onclick="return save();" />&nbsp;</nobr>
									</td>
								</tr>
							</table>
							</td>
						</tr>
						<tr>
							<td background="../images/site_bg_B7C1CB.jpg">
							<table width="100%" border="0" cellspacing="1" cellpadding="2">
								<tr>
									<td class="tablelabel">Location Type<font class="con_text_red1">*</font></td>
									<td class="tabletext" valign="top">
										<html:select styleClass="textbox" property="locationType" onchange="checkLocationType(this.value);">
											<html:option value="1">Address</html:option>
											<html:option value="2">Cross Street</html:option>
											<html:option value="3">Address Range</html:option>
											<html:option value="4">Landmark</html:option>
											<html:option value="5">Parcel Number</html:option>
											<html:option value="6">Map</html:option>
										</html:select>
									</td>
									<td class="tabletext" colspan="3">
										<table border="0">
											<tr id="address">
												<td class="tabletext" >
													<html:text size="10" property="streetNumber" styleClass="textbox" />
													<html:select property="streetName" styleClass="textbox">
														<html:option value="-1">Please Select</html:option>
														<html:options collection="streets" property="streetId" labelProperty="streetName" />
													</html:select>
												</td>
											</tr>
											<tr id="crossStreet">
												<td class="tabletext" >
													<html:select property="crossStreetName1" styleClass="textbox" onchange="return fromCrossStreet()">
														<html:option value="-1">Please Select</html:option>
														<html:options collection="streets" property="streetId" labelProperty="streetName" />
													</html:select>
													<html:select property="crossStreetName2" styleClass="textbox">
														<html:options collection="crossStreets" property="streetId" labelProperty="streetName" />
													</html:select>
												</td>
											</tr>
											<tr id="range">
												<td class="tabletext" >
													<table>
														<tr>
															<td class="tabletext">
																<html:text size="6" property="rangeStreetNumberStart1" styleClass="textbox" /> to
																<html:text size="6" property="rangeStreetNumberEnd1" styleClass="textbox" />
																<html:select property="rangeStreetName1" styleClass="textbox">
																	<html:option value="-1">Please Select</html:option>
																	<html:options collection="streets" property="streetId" labelProperty="streetName" />
																</html:select>
															</td>
														</tr>
														<tr>
															<td class="tabletext">
																<html:text size="6" property="rangeStreetNumberStart2" styleClass="textbox" /> to
																<html:text size="6" property="rangeStreetNumberEnd2" styleClass="textbox" />
																<html:select property="rangeStreetName2" styleClass="textbox">
																	<html:option value="-1">Please Select</html:option>
																	<html:options collection="streets" property="streetId" labelProperty="streetName" />
																</html:select>
															</td>
														</tr>
														<tr>
															<td class="tabletext">
																<html:text size="6" property="rangeStreetNumberStart3" styleClass="textbox" /> to
																<html:text size="6" property="rangeStreetNumberEnd3" styleClass="textbox" />
																<html:select property="rangeStreetName3" styleClass="textbox">
																	<html:option value="-1">Please Select</html:option>
																	<html:options collection="streets" property="streetId" labelProperty="streetName" />
																</html:select>
															</td>
														</tr>
														<tr>
															<td class="tabletext">
																<html:text size="6" property="rangeStreetNumberStart4" styleClass="textbox" /> to
																<html:text size="6" property="rangeStreetNumberEnd4" styleClass="textbox" />
																<html:select property="rangeStreetName4" styleClass="textbox">
																	<html:option value="-1">Please Select</html:option>
																	<html:options collection="streets" property="streetId" labelProperty="streetName" />
																</html:select>
															</td>
														</tr>
														<tr>
															<td class="tabletext">
																<html:text size="6" property="rangeStreetNumberStart5" styleClass="textbox" /> to
																<html:text size="6" property="rangeStreetNumberEnd5" styleClass="textbox" />
																<html:select property="rangeStreetName5" styleClass="textbox">
																	<html:option value="-1">Please Select</html:option>
																	<html:options collection="streets" property="streetId" labelProperty="streetName" />
																</html:select>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr id="landmark">
												<td class="tabletext" >
													<html:select property="landmark" styleClass="textbox">
														<html:option value="-1">Please Select</html:option>
														<html:options collection="landmarks" property="addressId" labelProperty="name" />
													</html:select>
												</td>
											</tr>
											<tr id="parcel">
												<td class="tabletext" >
													<html:text size="20" property="parcel1" styleClass="textbox" /> to
													<html:text size="20" property="parcel2" styleClass="textbox" />
												</td>
											</tr>
											<tr id="map">
												<td class="tabletext" >
													<a href="http://maps.google.com" target="_new"> Open Map for Selection</a>
												</td>
											</tr>
										</table>
          						</tr>

								<tr>
									<td class="tablelabel">Sub-Project <font class="con_text_red1">*</font></td>
									<td class="tabletext" valign="top">
										<html:select styleClass="textbox" property="subProjectName" onchange="return fromSubProjectName()">
											<html:option value="-1">Please Select</html:option>
											<html:options collection="subProjectNames" property="projectTypeId" labelProperty="description" />
										</html:select>
									</td>
									<td class="tablelabel">Sub-Project Sub-Type<font class="con_text_red1">*</font></td>
									<td class="tabletext">
										<html:select property="subProjectType" styleClass="textbox">
											<html:options collection="subProjectTypes" property="subProjectTypeId" labelProperty="description" />
										</html:select>
									</td>
								</tr>
								<tr>
									<td class="tablelabel">Activity Type <font class="con_text_red1">*</font></td>
									<td class="tabletext" valign="top">
										<html:select styleClass="textbox" property="activityType" onchange="return fromActivityType()">
											<html:option value="-1">Please Select</html:option>
											<html:options collection="activityTypes" property="type" labelProperty="description" />
										</html:select>
									</td>
									<td class="tablelabel">Activity Sub-Type </td>
									<td class="tabletext"  valign="top">
								       <html:select  multiple="true" size="5" styleClass="textbox"  property="activitySubTypes">
									    	   <html:options  collection="activitySubTypes"  property="id"  labelProperty="description" />
									    </html:select>
									</td>
								</tr>
								<tr>
									<td class="tablelabel">Activity Status<font class="con_text_red1">*</font></td>
									<td class="tabletext" colspan = "3">
										<html:select property="activityStatus" styleClass="textbox">
											<html:options collection="activityStatuses" property="status" labelProperty="code" />
										</html:select>
									</td>
								</tr>
								<tr>
									<td class="tablelabel">Applied Date<font class="con_text_red1">*</font></td>
									<td class="tabletext" valign="top"><nobr> <html:text property="startDate" size="10" maxlength="10" styleClass="textboxd" onfocus="showCalendarControl(this);" onkeypress="return validateDate();" onblur="DateValidate(this);"/> </nobr></td>
									<td class="tablelabel"><nobr>Expiration Date</nobr></td>
									<td class="tabletext" valign="top"><nobr> <html:text property="expirationDate" maxlength="10" styleClass="textboxd" onfocus="showCalendarControl(this);" onkeypress="return validateDate();" onblur="DateValidate(this);" /> </nobr></td>
								</tr>
								<tr>
									<td class="tablelabel">Plan Check</td>
									<td class="tabletext" valign="top"><html:checkbox property="planCheckRequired" />&nbsp;(Check if required)</td>
									<td class="tablelabel">Valuation</td>
									<td class="tabletext" valign="top"><html:text property="valuation" size="15" styleClass="textbox" onblur="formatCurrency(this);" onkeypress="return DisplayPeriod();" /></td>
								</tr>

								<tr>
									<td class="tablelabel">Work Description<font class="con_text_red1">*</font></td>
									<td class="tabletext" valign="top" colspan="3"><html:textarea rows="5" cols="65" property="description" /></td>
								</tr>

				  			    <%  int i=0 ;   %>
                    <nested:iterate  property="customFieldList" >

                              <nested:hidden property="fieldId" />
                              <nested:hidden property="fieldType" />
                              <nested:hidden property="fieldLabel" />
                              <nested:hidden property="fieldRequired" />

                          <%if(i % 2==0) {  %>   <tr class="tabletext">
							<nested:equal property="fieldType" value="DATE">
                                  	<td class="tablelabel"><nested:write property="fieldLabel" /></td>
                                  	<td class="tabletext" valign="top">
                                  		<nested:text  styleClass="textbox" property="fieldValue" onfocus="showCalendarControl(this);" />
                                  	</td>
                                  </nested:equal>
                                  <nested:notEqual property="fieldType" value="DATE">
                                  	<td class="tablelabel"><nested:write property="fieldLabel" /></td>
                                  	<td class="tabletext" valign="top"> <nested:text  styleClass="textbox" property="fieldValue" /></td>
                                  </nested:notEqual>

                          <% } else  {%>
                          		<nested:equal property="fieldType" value="DATE">
                          		<td class="tablelabel"><nested:write property="fieldLabel" /></td>
                                  <td class="tabletext" valign="top">
                                  	<nested:text  styleClass="textbox" property="fieldValue" onfocus="showCalendarControl(this);"/>

                                  </td> </nested:equal>
                                  <nested:notEqual property="fieldType" value="DATE">
                                  <td class="tablelabel"><nested:write property="fieldLabel" /></td>
                                  <td class="tabletext" valign="top"> <nested:text  styleClass="textbox" property="fieldValue" /></td>
                                  </nested:notEqual> <% }%>

			             <%if(i % 2==1) {  %>     </tr>    <% } i++ ;%>

                    </nested:iterate>
                    <% if(count % 2==1){%>   <td class="tabletext" colspan="6" > </td> </tr>     <% } %>

							</table>
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
</html:form>
</body>
</html:html>
