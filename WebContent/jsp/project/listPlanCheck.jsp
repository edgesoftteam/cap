<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>

<html:html>
<head>
<html:base />
<title>City of Burbank : Online Business Center : Plan Check Manager</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%
    String lsoAddress = (String)session.getAttribute("lsoAddress");
    if (lsoAddress == null ) lsoAddress = "";
%>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form action="/addPlanCheck" >

<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <TBODY><tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <TBODY><tr>
            <td><font class="con_hdr_3b">Plan Check History&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <TBODY><tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" background="../images/site_bg_B7C1CB.jpg">Plan
                        Check List</td>
                      <td width="1%"><img src="../images/site_hdr_split_1.jpg" width="17" height="16"></td>
                      <td width="1%" class="tablelabel"><a href="javascript:document.forms[0].submit();" class="hdrs"><font class="con_hdr_link">Add<img src="../images/site_blt_plus_000000.gif" width="21" height="9" border="0"></a></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <TBODY><tr>
                      <td class="tablelabel"><nobr>Plan
                        Check Status</nobr></td>
                      <td class="tablelabel"> Date</td>
                      <td class="tablelabel"> Engineer</td>
                      <td class="tablelabel"> Comments</td>
                    </tr>
                    <nested:iterate  property="planChecks" type="elms.app.project.PlanCheck">
                    <tr valign="top">
                      <td class="tabletext"><nested:write  property="statusDesc" /></td>
                      <td class="tabletext"> <nested:write property="date"/></td>
                      <td class="tabletext"><nested:write property="engineerName"/></td>
                      <td class="tabletext"> <nested:write  property="comments"/></td>
                    </tr>
                    </nested:iterate>
                  </TBODY></table>
                </td>
              </tr>
            </TBODY></table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </TBODY></table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <TBODY><tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </TBODY></table>
    </td>
  </tr>
</TBODY></table>
</html:form>
</body>
</html:html>
