<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<script language="JavaScript" src="../script/calendar.js"></script>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<SCRIPT language="JavaScript">
var strValue;
function validateFunction()
{
	//strValue=false;
    strValue=validateData('req',document.forms['projectForm'].elements['projectName'],'Project Name is a required field');
    if (strValue == true)
    {
    	strValue=validateData('req',document.forms['projectForm'].elements['description'],'Description is a required field');
    }
    if (strValue == true)
    {
    	if (document.forms['projectForm'].elements['use'].value == '')
    	{
    		alert('Select Use');
    		document.forms['projectForm'].elements['use'].focus();
    		strValue=false;
    	}
    	else
    	{
    		strValue=true;
    	}
    }
    if (strValue == true)
    {
    	strValue=validateData('req',document.forms['projectForm'].elements['status'],'Status is a required field');
    }
    if (strValue == true)
    {
    	//alert(document.forms['projectForm'].elements['expirationDate'].value);
		if (document.forms['projectForm'].elements['expirationDate'].value != '')
    	{
    		strValue=validateData('date',document.forms['projectForm'].elements['expirationDate'],'Invalid date format');
    	}
    }
    if (strValue == true)
    {
    	strValue=validateData('req',document.forms['projectForm'].elements['startDate'],'Applied Date is a required field');
    }
    if (strValue == true)
    {
    	//alert(document.forms['projectForm'].elements['expirationDate'].value);
		if (document.forms['projectForm'].elements['startDate'].value != '')
    	{
    		strValue=validateData('date',document.forms['projectForm'].elements['startDate'],'Invalid date format');
    	}
    }
     if (strValue == true)
    {
    	//alert(document.forms['projectForm'].elements['expirationDate'].value);
		if (document.forms['projectForm'].elements['completionDate'].value != '')
    	{
    		strValue=validateData('date',document.forms['projectForm'].elements['completionDate'],'Invalid date format');
    	}
    }
    if (strValue == true)
    {
    	if (document.forms['projectForm'].elements['status'].value == '5')
		{
		      if ( document.forms['projectForm'].elements['openActivitiesCount'].value != '0')
    	      {
    		         alert('A project with Open Activities can not have a "Final" status.');
    		          strValue = false;
    		  }
    		  else if (document.forms['projectForm'].elements['finalStatusAllowed'].value == 'N')
    		  {
 				      alert('A project with hard (active) holds can not have a "Final" status.');
				      strValue = false;
			  }else strValue = true;
	     }else strValue = true;
    }
    return strValue;
}
</SCRIPT>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%
String projectId = (String)request.getAttribute("levelId");
if(projectId == null) projectId = "";
String level= (String) request.getAttribute("level");
if(level == null) level = "";
String ownerName = (String)request.getAttribute("ownerName");
if(ownerName == null) ownerName = "";
String streetNumber = (String)request.getAttribute("streetNumber");
if(streetNumber == null) streetNumber = "";
String streetName = (String)request.getAttribute("streetName");
if(streetName == null) streetName = "";
String projectNumber = (String)request.getAttribute("projectNumber");
if(projectNumber == null) projectNumber = "";

String useId = (String) request.getAttribute("useId");
String useDesc = (String) request.getAttribute("useDesc");



java.util.List projectStatuses = (java.util.List) request.getAttribute("projectStatuses");
if (projectStatuses == null) projectStatuses = new java.util.ArrayList();
pageContext.setAttribute("projectStatuses", projectStatuses);
String lsoAddress = (String)session.getAttribute("lsoAddress");
if (lsoAddress == null ) lsoAddress = "";

//For Editing Project name LC which contains BL/BT
String editableProjName = (String) request.getAttribute("editableProjName");
if(editableProjName == null)  editableProjName = "";
%>
<html:form action="/saveProjectDetails" onsubmit="return validateFunction();">
<html:hidden property="projectId" value="<%=projectId%>"/>
<html:hidden property="finalStatusAllowed" />
<html:hidden property="openActivitiesCount" />
<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><font class="con_hdr_3b">Edit Project&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><br><br>
                </td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle">Project Details</td>
                                <td width="1%" class="tablebutton"><nobr>
                                	<html:reset value="Cancel" styleClass="button" onclick="history.back();"/>
                                	<html:submit value="Update" styleClass="button"/> &nbsp;</nobr>
								</td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr valign="top">
                                <td class="tablelabel">Project #</td>
                                <td class="tabletext"><%=projectNumber%></td>
                                <td class="tablelabel">Owner</td>
                                <td class="tabletext"><%=ownerName%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Project Name</td>

								<%if(editableProjName.equalsIgnoreCase("false")){%>
									<td class="tabletext"><html:text property="projectName" size="50" maxlength="256" styleClass="textbox" readonly="true" /></td>
								<%}else{%>
									<td class="tabletext">
                                   		<html:select property="projectName" styleClass="textbox">
                             		 	 <html:options collection="projectNames"  property="name"  labelProperty="description" />
                          		   		</html:select>
									</td>
								<%}%>
                                <td class="tablelabel">Address</td>
                                <td class="tabletext"><%=streetNumber%>&nbsp;<%=streetName%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Description</td>
								<%if(editableProjName.equalsIgnoreCase("false")){%>
									<td class="tabletext"><html:text property="description" size="50" maxlength="256" styleClass="textbox" readonly="true" /></td>
								<%}else{%>
                                <td class="tabletext"><html:text property="description" size="50" maxlength="256" styleClass="textbox"/></td>
								<%}%>
                                <td class="tablelabel"> Use</td>
                                <td class="tabletext"><%=useDesc%>&nbsp;
                                <html:hidden property ="use" value="<%=useId%>"/></td>

                            </tr>
                            <tr valign="top">
	                            <td class="tablelabel">Status</td>
				                <td class="tabletext"  valign="top">
				                   <html:select  styleClass="textbox"  property="status"  >
				               	      <html:option value="">Please Select</html:option>
				                      <html:options  collection="projectStatuses"  property="statusId" labelProperty="description" />
				                   </html:select>
		                        </td>
                                <td class="tablelabel">Expiration Date</td>
                                <td class="tabletext" valign="top"><nobr>
                                <html:text property="expirationDate" maxlength="10" styleClass="textboxd"  onkeypress="return validateDate();" onblur="DateValidate(this);"/> <a href="javascript:show_calendar('projectForm.expirationDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;"> <img src="../images/calendar.gif" width="16" height="15" border=0/></a></nobr></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Applied Date</td>
                                <td class="tabletext" valign="top"><nobr>
                                <html:text property="startDate" maxlength="10" styleClass="textboxd"  onkeypress="return validateDate();" onblur="DateValidate(this);" /> <a href="javascript:show_calendar('projectForm.startDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;"> <img src="../images/calendar.gif" width="16" height="15" border=0/></a></nobr></td>
                                <td class="tablelabel">Completion Date</td>
                                <td class="tabletext" valign="top"><nobr>
                                <html:text property="completionDate" maxlength="10" styleClass="textboxd"  onkeypress="return validateDate();" onblur="DateValidate(this);"/> <a href="javascript:show_calendar('projectForm.completionDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;"> <img src="../images/calendar.gif" width="16" height="15" border=0/></a></nobr></td>
                            </tr>
                            <tr valign="top">
                              <td class="tablelabel">Microfilm</td>
				              <td class="tabletext"  valign="top">
				                 <html:select  styleClass="textbox"  property="microfilm"  >
				                    <html:options  collection="microfilmStatuses"  property="code" labelProperty="description" />
				                 </html:select>
		                      </td>
                              <td class="tablelabel">Label</td>
                              <td class="tabletext"><html:text property="label" size="25" maxlength="25" styleClass="textbox"/></td>
                           </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </td>
        <td width="1%"></td>
    </tr>
</table>
</html:form>
</body>
</html:html>
