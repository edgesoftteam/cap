<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<app:checkLogon/>
<html:html>
<head>
<html:base/>
<title>City of Burbank : Online Business Center : Add Activity</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%
 	String contextRoot = request.getContextPath();
    //String subProjectId = request.getParameter("subProjectId");

    String lsoAddress = (String)session.getAttribute("lsoAddress");
    if (lsoAddress == null ) lsoAddress = "";
    String psaInfo = (String)session.getAttribute("psaInfo");
    if (psaInfo == null ) psaInfo = "";
    String completionDateLabel = (String)request.getAttribute("completionDateLabel");
    if (completionDateLabel == null) completionDateLabel = "";
    String expirationDateLabel = (String)request.getAttribute("expirationDateLabel");
    if (expirationDateLabel == null) expirationDateLabel = "";

%>
<script language="JavaScript" src="../script/calendar.js"></script>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<SCRIPT language="JavaScript">
var strValue;
function validateFunction()
{
		    if (document.forms['activityForm'].elements['address'].value == '')
		    {
		    	alert('Select Address');
		    	document.forms['activityForm'].elements['address'].focus();
		    	strValue=false;
		    }
    	    else if (document.forms['activityForm'].elements['activityType'].value == '') {
		    	alert('Select Activity Type');
		    	document.forms['activityForm'].elements['activityType'].focus();
		    	strValue=false;
		    }
		    else
		    {
		    	strValue=true;
			}
		    if (strValue == true)
		    {
				if (document.forms['activityForm'].elements['startDate'].value != '')
		    	{
		    		strValue=validateData('date',document.forms['activityForm'].elements['startDate'],'Start Date is a required field');
		    	}
		    }
		    if (strValue == true)
		    {
		    	 if   ( ( document.forms['activityForm'].elements['activityType'].value == 'ABEST' ) ||
		    	        ( document.forms['activityForm'].elements['activityType'].value == 'BLDG' ) ||
		    	        (document.forms['activityForm'].elements['activityType'].value == 'BLDGFS' ) ||
		    	        (document.forms['activityForm'].elements['activityType'].value == 'DEMO' ) ||
		    	        (document.forms['activityForm'].elements['activityType'].value == 'ELEC'  ) ||
		    	        (document.forms['activityForm'].elements['activityType'].value == 'FENCE') ||
		    	        (document.forms['activityForm'].elements['activityType'].value == 'GAME' ) ||
		    	        (document.forms['activityForm'].elements['activityType'].value == 'GRAD') ||
				        (document.forms['activityForm'].elements['activityType'].value == 'LAND') ||
				        (document.forms['activityForm'].elements['activityType'].value == 'MECH') ||
				        (document.forms['activityForm'].elements['activityType'].value == 'PARK') ||
				        (document.forms['activityForm'].elements['activityType'].value == 'PAVING') ||
				        (document.forms['activityForm'].elements['activityType'].value == 'PLUM') ||
				        (document.forms['activityForm'].elements['activityType'].value == 'RETAIN') ||
				        (document.forms['activityForm'].elements['activityType'].value == 'ROOF') ||
				        (document.forms['activityForm'].elements['activityType'].value == 'SDBL') ||
				        (document.forms['activityForm'].elements['activityType'].value == 'SIGN') ||
				        (document.forms['activityForm'].elements['activityType'].value == 'TENT') ||
				        (document.forms['activityForm'].elements['activityType'].value == 'TREE') )    {
		    	         strValue=validateData('req',document.forms['activityForm'].elements['valuation'],'Valuation is a required field');
		    	   }
		     }
		    if (strValue == true)
		    {
		    	if (document.forms['activityForm'].elements['valuation'].value != '')
		    	{
		    		strValue=validateData('curr',document.forms['activityForm'].elements['valuation'],'Valuation Field has invalid characters');
		    	}
		    }
		    if (strValue == true)
		    {
		    	document.forms[0].action='<%=contextRoot%>/saveActivity.do';
		 		document.forms[0].submit();
		    }else{
                 return false;
           }
}
function DisplayPeriod()
{

	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46))
	{
		event.returnValue = false;
	}
	else
	{
		if (document.forms[0].valuation.value.length == 9 )
		{
			document.forms[0].valuation.value = document.forms[0].valuation.value +'.';
 		}
 		if (document.forms[0].valuation.value.length > 11 )  event.returnValue = false;
	}
}
function reSubmit(){
  document.forms['activityForm'].action='<%=contextRoot%>/addActivity.do';
  document.forms['activityForm'].submit();
  return true;
}
function addVehicle(){
  document.location.href='<%=contextRoot%>/addVehicle.do';
  //document.forms['activityForm'].action='<%=contextRoot%>/addActivity.do';
  //document.forms['activityForm'].submit();
  //return true;
}

function save() {
  document.forms['activityForm'].action='<%=contextRoot%>/saveActivity.do';
  document.forms['activityForm'].submit();
  return true;
}
function cancel(){
   document.forms['activityForm'].action='<%=contextRoot%>/cancelAddActivity.do';
   document.forms['activityForm'].submit();
   return true;
}
</SCRIPT>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form   name="activityForm" type="elms.control.beans.ActivityForm" action="">
<html:hidden property="subProjectId"/>
<html:hidden property="origin"/>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Add Activity&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br><br></td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" background="../images/site_bg_B7C1CB.jpg">Add Activity</td>

                      <td width="1%" class="tablelabel"><nobr>
                          <html:button  property="Cancel"  value="Cancel" styleClass="button" onclick="cancel();"/>
                          <!--<html:submit  value="Cancel" property="cancel" styleClass="button"/>
                          <html:submit  value="Save" property="Save" styleClass="button"/>-->
                          <html:button property="Save" value="Save" styleClass="button" onclick="return validateFunction();save();"/>
                       &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">

                    <tr>
                      <td class="tablelabel">Address</td>
                      <td class="tabletext"  valign="top">
                          <html:select  styleClass="textbox"  property="address" >
                          	   <html:option value="">Please Select</html:option>
                               <html:options  collection="addresses"  property="addressId" labelProperty="description" />
                          </html:select>
                      </td>

                      <td class="tablelabel">Activity Type </td>
                      <td class="tabletext"  valign="top">
                          <html:select  styleClass="textbox"  property="activityType" onchange="reSubmit();" >
                          	   <html:option value="">Please Select</html:option>
                               <html:options  collection="activityTypes"  property="type" labelProperty="description" />
                          </html:select>
                      </td>
                   </tr>
                   <tr>
                      <td class="tablelabel">Description</td>
                      <td class="tabletext" valign="top">
                          <html:text  property="description" size="40" maxlength="256" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" >Applied Date</td>
                      <td class="tabletext"  valign="top"><nobr>
                        <html:text  property="startDate" size="10" maxlength="10"  styleClass="textboxd"/>
					    <html:link href="javascript:show_calendar('activityForm.startDate');"
                        onmouseover="window.status='Calendar';return true;"
                        onmouseout="window.status='';return true;">
                        <img src="../images/calendar.gif" width="16" height="15" border=0/></html:link></nobr>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel"><nobr><%=completionDateLabel%></nobr></td>
                      <td class="tabletext" valign="top"><nobr>
                          <html:text  property="completionDate" maxlength="10"  styleClass="textboxd" onkeypress="return validateDate();"/>
                        <html:link href="javascript:show_calendar('activityForm.completionDate');"
                        onmouseover="window.status='Calendar';return true;"
                        onmouseout="window.status='';return true;"><img src="../images/calendar.gif" width="16" height="15" border=0/></html:link></nobr>
                      </td>
                     <td class="tablelabel"><nobr><%=expirationDateLabel%></nobr></td>
                      <td class="tabletext" valign="top"><nobr>
                          <html:text  property="expirationDate" maxlength="10"  styleClass="textboxd" onkeypress="return validateDate();"/>
                        <html:link href="javascript:show_calendar('activityForm.expirationDate');"
                        onmouseover="window.status='Calendar';return true;"
                        onmouseout="window.status='';return true;"><img src="../images/calendar.gif" width="16" height="15" border=0/></html:link></nobr>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel">Valuation</td>
                      <td class="tabletext" valign="top">
                          <html:text property="valuation" size="15" styleClass="textbox" onkeypress="return DisplayPeriod();"/>
                      </td>
                      <td class="tablelabel">Plan Check Required</td>
                      <td class="tabletext" valign="top">
                          <html:checkbox property="planCheckRequired"/>
                      </td>

                    </tr>
</html:form>
</html:html>
