<%@ page import='elms.agent.*' %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon />
<%     String contextRoot = request.getContextPath();  %>
<html:html>
<head>
<html:base />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0"
	marginwidth="0" marginheight="0">
<html:errors/>
<%
   java.util.List planCheckStatuses = LookupAgent.getPlanCheckStatuses();
   pageContext.setAttribute("planCheckStatuses", planCheckStatuses);
   // Getting the employees as plan check engineers from the groups  1-Inspector, 6-Supervisor, 7-Engineer , 11- Permit Tech
   int deptId = (Integer)session.getAttribute("deptId"); 
   java.util.List engineerUsers = ActivityAgent.getEngineerUsers(deptId);
   pageContext.setAttribute("engineerUsers", engineerUsers);
   String lsoAddress = (String)session.getAttribute("lsoAddress");
   String activityId = (String)request.getAttribute("activityId");
   if(activityId ==null) activityId = "";
   if (lsoAddress == null ) lsoAddress = "";
   String psaInfo = (String)session.getAttribute("psaInfo");
   if (psaInfo == null ) psaInfo = "";
%>
<script language="JavaScript" src="../script/formValidations.js"></script>
<script language="JavaScript" src="../script/calendar.js"></script>
<script language="JavaScript">
var strValue;
function validateFunction() {
   	    if (document.forms[0].elements['planCheckStatus'].value == '-1')  {
	    	alert('Select Plan Check Status');
	    	document.forms[0].elements['planCheckStatus'].focus();
	    	return false;
	    } else if (document.forms[0].elements['engineerId'].value == '-1')  {
	    	alert('Select Engineer');
	    	document.forms[0].elements['engineerId'].focus();
	    	 return false;
		} else if (document.forms[0].elements['planCheckDate'].value == '')  {
	    	alert('Select Plan Check Date');
	    	document.forms[0].elements['planCheckDate'].focus();
	    	return false;
	    }
else return true;
}

function onclickprocess(strval)
{
    if (strval == 'dp') {
      document.forms['planCheckForm'].action='<%=contextRoot%>/deletePlanCheck.do?psaId=<%=activityId%>';
      document.forms['planCheckForm'].submit();
    }

}
function goBack(button)
{
	//button.value='Please Wait..';
	button.disabled=true;
	document.location.href='<%=contextRoot%>/viewActivity.do?activityId=<%=activityId%>';
}
</script>

<html:form action="/savePlanCheck" onsubmit="return validateFunction();">
	<html:hidden property="activityId" />

	<table width="100%" border="0" cellspacing="10" cellpadding="0">
		<TBODY>
			<tr>
				<td><font class="con_hdr_3b">Add Plan Check&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font
					class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br></td>
			</tr>
			<tr valign="top">
				<td width="99%">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="99%" class="tabletitle">Add</td>
										<td width="1%" class="tablebutton">
											<nobr>
												<html:button property="back" value="Back" styleClass="button" onclick="goBack(this);" />&nbsp;
												<html:submit value="Save" styleClass="button" onclick="return validateFunction();" /> &nbsp;
											</nobr>
										</td>
									</tr>
								</table>
								</td>
							</tr>
							<tr>
								<td background="../images/site_bg_B7C1CB.jpg">
								<table width="100%" border="0" cellspacing="1" cellpadding="2">
									<tr>
										<td class="tablelabel">Plan Check Status</td>
										<td class="tablelabel">Date</td>
										<td class="tablelabel">Engineer</td>
									</tr>
									<tr valign="top">
										<td class="tabletext"><html:select property="planCheckStatus"
											styleClass="textbox">
											<html:options collection="planCheckStatuses" property="code"
												labelProperty="description" />
										    </html:select>
										</td>
                                        <td class="tabletext"  valign="top"><nobr>
                                           <html:text  property="planCheckDate" size="10" maxlength="10"  styleClass="textboxd" onkeypress="return validateDate();" onblur="DateValidate(this);"/>
					                       <html:link href="javascript:show_calendar('forms[0].planCheckDate');"
                                            onmouseover="window.status='Calendar';return true;"
                                            onmouseout="window.status='';return true;">
                                            <img src="../images/calendar.gif" width="16" height="15" border=0/></html:link></nobr>
                                        </td>
                           			    <td class="tabletext"><html:select property="engineerId"
											styleClass="textbox">
											<html:options collection="engineerUsers" property="userId"
												labelProperty="engineerId" />
										    </html:select>
										</td>
									</tr>
									<tr>
										<td class="tablelabel">Comments</td>
										<td class="tabletext" colspan="2"><html:textarea
											property="comments" cols="75" rows="7" styleClass="textbox" /></td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
				<td width="1%">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="32">&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
				</table>
				</td>
			</tr>
	</table>
</html:form>
<html:form action="/deletePlanCheck" >
	<table width="100%" border="0" cellspacing="10" cellpadding="0">
		<TBODY>
			<tr valign="top">
				<td width="99%">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<TBODY>
								<tr>
									<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
										  <td width="98%" class="tabletitle">Plan Check History</td>
										  <td width="1%" class="tablebutton"><nobr>
										  <html:submit value="Delete" styleClass="button" />
										</tr>
									</table>
									</td>
								</tr>
								<tr>
									<td background="../images/site_bg_B7C1CB.jpg">
									<table width="100%" border="0" cellspacing="1" cellpadding="2">
										<TBODY>
											<tr>
												<td class="tablelabel"><nobr>Remove</nobr></td>
												<td class="tablelabel"><nobr>Plan Check Status</nobr></td>
												<td class="tablelabel">Date</td>
												<td class="tablelabel">Engineer</td>
												<td class="tablelabel">Comments</td>
											</tr>
											<nested:iterate property="planChecks" type="elms.app.project.PlanCheck">
												<tr valign="top">
													<td class="tabletext">
														<html:multibox property="selectedPlanCheck">
															<nested:write property="planCheckId" />
														</html:multibox></td>
													<td class="tabletext"><a
														href='<%=contextRoot%>/editPlanCheck.do?planCheckId=<nested:write property="planCheckId"/>'><nested:write
														property="statusDesc" /></a></td>
													<td class="tabletext"><nested:write
														property="date" /></td>
													<td class="tabletext"><nested:write
														property="engineerName" /></td>
													<td class="tabletext"><nested:write
														property="comments" /></td>
												</tr>
											</nested:iterate>
										</TBODY>
									</table>
									</td>
								</tr>
							</TBODY>
						</table>
						</td>
					</tr>
					</TBODY>
				</table>
				</td>
				<td width="1%">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<TBODY>
						<tr>
							<td height="32">&nbsp;</td>
						</tr>
					</TBODY>
				</table>
				</td>
			</tr>
		</TBODY>
	</table>
</html:form>
</body>
</html:html>

