<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>
<html:html>

<%
String levelId = (String)request.getAttribute("activityId");
%>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<html:form name="copyActivityForm" type="elms.control.beans.project.CopyActivityForm" scope="request" action="/saveCopyActivity" >
<input type="hidden" name="oldActivityId" value="<bean:write name="copyActivityForm" property="copyActivity.activity.activityId" />">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Copy Activity&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><br><br>
                </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" background="../images/site_bg_B7C1CB.jpg">Activity Manager
                        </td>

                      <td width="1%" class="tablelabel"><nobr>
                          <html:reset  value="Cancel" styleClass="button" onclick="history.back();" />
                          <security:editable levelId="<%=levelId%>" levelType="A" editProperty="checkUser">
                          <html:submit  value="Copy" styleClass="button" />
                          </security:editable>
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr valign="top">
                      <td class="tablelabel">Activity #</td>
                      <td class="tabletext"><bean:write name="copyActivityForm" property="copyActivity.activity.activityDetail.activityNumber" /></td>
                      <td class="tablelabel">Activity Type</td>
                      <td class="tabletext"><bean:write name="copyActivityForm" property="copyActivity.activity.activityDetail.activityType.description" /></td>
                     </tr>
                    <tr valign="top">
                    <tr valign="top">
                      <td class="tablelabel">Conditions</td>
                      <td class="tabletext" valign="top"><nobr><html:checkbox property="copyActivity.conditions"/></nobr></td>
                      <td class="tablelabel">Activity People</td>
                      <td class="tabletext" valign="top"><nobr><html:checkbox property="copyActivity.activityPeople"/></nobr></td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel">Holds</td>
                      <td class="tabletext" valign="top"><nobr><html:checkbox property="copyActivity.holds"/></nobr></td>
                      <td class="tablelabel">Fees</td>
                      <td class="tabletext" valign="top"><nobr><html:checkbox property="copyActivity.fees"/></nobr></td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel">Comments</td>
                      <td class="tabletext" valign="top"><nobr><html:checkbox property="copyActivity.comments"/></nobr></td>
                      <td class="tablelabel">Plan Checks</td>
                      <td class="tabletext" valign="top"><nobr><html:checkbox property="copyActivity.planChecks"/></nobr></td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel">Activity Team</td>
                      <td class="tabletext" valign="top"><nobr><html:checkbox property="copyActivity.activityTeam"/></nobr></td>
                      <td class="tablelabel"></td>
                      <td class="tabletext" valign="top"><nobr></nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

