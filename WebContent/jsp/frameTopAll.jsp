<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ page import="elms.security.*,elms.common.*"%>
<html:html>
<head>
<html:base/>
<title>City of Burbank : Online Business Center</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="css/elms.css" type="text/css">
</head>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" background="images/site_bg_e5e5e5.jpg">
<%
String contextRoot = request.getContextPath();
String hl = "";
//hl = request.getParameter("hl");

%>
<script>

function dohref(strHref)
{
   if (strHref == 'home') parent.location.href="<%=contextRoot%>/gateway.do";
   else if (strHref == 'assessor') parent.f_content.location.href="<%=contextRoot%>/viewAssessor.do";
   else if (strHref == 'login') parent.f_content.location.href="<%=contextRoot%>/jsp/index.jsp";
   else if (strHref == 'blic') parent.f_content.location.href="<%=contextRoot%>/businessSearch.do";
   else if (strHref == 'sierra') parent.f_content.location.href="<%=contextRoot%>/sierraSearch.do";
   else if (strHref == 'gis') parent.f_content.location.href="<%=contextRoot%>/gisSearch.do"
}
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="top" width="1%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td valign="top"><img src="images/site_logo_shield.jpg" width="68" height="61"></td>
                <td valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td colspan="2"><img src="images/site_logo_text.jpg" width="133" height="41"></td>
                    </tr>
                    <tr>
                        <td bgcolor="#003366" width="99%"><font class="white2b">


                  <script>

					function getMap() {
                          msgWindow=open('','mapwindow','resizable=yes,status=yes, width=850,height=650,screenX=400,screenY=0,top=0,left=0,scrollbars');
                          msgWindow.location.href = '<%=contextRoot%>/viewMap.do';
                          if (msgWindow.opener == null) msgWindow.opener = self;
					}


					function makeArray() {
					  var args = makeArray.arguments;
					  for (var i = 0; i < args.length; i++) {
						this[i] = args[i];
					  }
					  this.length = args.length;
					}

					function fixDate(date) {
					  var base = new Date(0);
					  var skew = base.getTime();
					  if (skew > 0)
						date.setTime(date.getTime() - skew);
					}

					function getString(date) {
					  var months = new makeArray("January", "February", "March",
												 "April",   "May",      "June",
												 "July",    "August",   "September",
												 "October", "November", "December");
					  return months[date.getMonth()] + " " +
							 date.getDate() + ", " + ( date.getYear());
					}

					var cur = new Date();
					fixDate(cur);
					var str = getString(cur);
					document.write(str);
					</script> </td>
                        <td bgcolor="#003366" width="1%"><img src="images/spacer.gif" width="1" height="20"></td>
                    </tr>
                </table>
                </td>
            </tr>
        </table>
        </td>
        <html:form action="/quickSearch">
        <td valign="top" width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td background="images/site_top_bg.jpg"><img src="images/site_top_img.jpg" width="297" height="41"></td>
                <td background="images/site_top_bg.jpg"><!--<div align="right"><img src="images/spacer.gif" width="1" height="10"><br><font class="white2b">Quick-Search
              <html:text  property="searchText" size="16" styleClass="textbox"/>
              <html:submit  property="search" value="Find" styleClass="button"/>
              &nbsp;&nbsp;&nbsp;&nbsp;</div>--></td>
            </tr>
            <tr>
                <td background="images/site_nav_bg.jpg" colspan="2">
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">

                    <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                    <td width="79" align="center"
                        <%
                            if(hl.equals("assessor")) out.println("background=\"images/site_nav_bg_on.jpg\"");
                            else out.println("onmouseover=\"background='images/site_nav_bg_on.jpg'\" onmouseout=\"background='images/spacer.gif'\"");
                        %>>
                    <img src="images/spacer.gif" width="1" height="6"><br>
                    <font class="blue2b"><a href="javascript:dohref('assessor')" class="topnav">Assessor</a></td>

	                <td width="1"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
	                <td width="58" align="center"
                       <%
                             if(hl.equals("gis"))  out.println("background=\"images/site_nav_bg_on.jpg\"");
                             else  out.println("onmouseover=\"background='images/site_nav_bg_on.jpg'\" onmouseout=\"background='images/spacer.gif'\"");
                       %> >
	                <img src="images/spacer.gif" width="1" height="6"><br>
	                <font class="blue2b"><html:link href="javascript:dohref('gis')"  styleClass="topnav">GIS</html:link></td>


                    <td width="1"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                    <td width="66" align="center"
                        <%
                            if(hl.equals("license")) out.println("background=\"images/site_nav_bg_on.jpg\"");
                            else out.println("onmouseover=\"background='images/site_nav_bg_on.jpg'\" onmouseout=\"background='images/spacer.gif'\"");
                        %> >
        			<img src="images/spacer.gif" width="1" height="6"><br>
                    <font class="blue2b"><a href="javascript:dohref('blic')" class="topnav">Bus. Tax</a></td>

                    <td width="1"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                    <td width="58" align="center"
                        <%
                            if(hl.equals("license")) out.println("background=\"images/site_nav_bg_on.jpg\"");
                            else out.println("onmouseover=\"background='images/site_nav_bg_on.jpg'\" onmouseout=\"background='images/spacer.gif'\"");
                        %> >
        			<img src="images/spacer.gif" width="1" height="6"><br>
                    <font class="blue2b"><a href="javascript:dohref('sierra')" class="topnav">Sierra</a></td>

                    <td width="1"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                    <td width="58" align="center"
                        <%
                            if(hl.equals("login")) out.println("background=\"images/site_nav_bg_on.jpg\"");
                            else out.println("onmouseover=\"background='images/site_nav_bg_on.jpg'\" onmouseout=\"background='images/spacer.gif'\"");
                        %> >
        			<img src="images/spacer.gif" width="1" height="6"><br>
                    <font class="blue2b"><a href="javascript:dohref('login')" class="topnav">Login</a></td>

                    <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                    <td width="1" align="center" background="images/site_bg_cccccc.jpg"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                    </tr>
                </table>
                </td>
            </tr>
        </table>
        </td>
    </tr>
    <tr>
        <td valign="top" colspan="2">&nbsp;</td>
    </tr>
</table>
</html:form>
</body>
</html:html>
