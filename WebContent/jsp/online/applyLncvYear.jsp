<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@page import="elms.util.StringUtils"%>
<%@page import="java.util.Calendar"%>
<%
response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevent caching at the proxy server
%>

<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%

 	String contextRoot = request.getContextPath();
	int dtcount = Integer.parseInt((String) (request.getAttribute("dtcount")!=null? request.getAttribute("dtcount"):"1"));
	int dtsize =96;
	String lncv = "Y";
	String vehicleStat = (String) request.getAttribute("checkVehicleStatus");
	int actId = Integer.parseInt((String) (request.getAttribute("actId")!=null? request.getAttribute("actId"):"0"));
	String exisitingDt = (String) request.getAttribute("exisitingDt");
	
	String btn = (String) request.getAttribute("btn");
	Calendar yr = Calendar.getInstance();
	int jan= yr.get(Calendar.YEAR)+1;
	String endYear = "01/31/"+jan ;
	
	String startYear = "01/01/"+jan ;
	
	 
%>
<html:html>
<head>
<SCRIPT type="text/javascript">
    window.history.forward();
    function noBack() { window.history.forward(); }
</SCRIPT>
<link rel="stylesheet" href="<%=contextRoot%>/jsp/script/jquery-ui-1.8.2.custom/development-bundle/themes/base/jquery.ui.all.css">
<script src="<%=contextRoot%>/jsp/script/jquery-ui-1.8.2.custom/js/jquery-1.6.4.min.js" type="text/javascript"></script>
<script src="<%=contextRoot%>/jsp/script/jquery-ui-1.8.2.custom/js/jquery.alerts.js" type="text/javascript"></script>
<script src="<%=contextRoot%>/jsp/script/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.core.js"></script>
<script src="<%=contextRoot%>/jsp/script/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.widget.js"></script>
<script src="<%=contextRoot%>/jsp/script/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.datepicker.js" type="text/javascript"></script>
<link rel="stylesheet" href="<%=contextRoot%>/jsp/script/jquery-ui-1.8.2.custom/development-bundle/demos/demos.css">
<link rel="stylesheet" href="<%=contextRoot%>/jsp/script/jquery-ui-1.8.2.custom/js/jquery.alerts.css">
<link rel="stylesheet" href="../css/elms.css" type="text/css">	

<script language="javascript">
//lncv dates
$(function() {
	var dtc = <%=dtcount%>;
	var $mexisitingDates = new Array(<%=exisitingDt%>);


function checkAvailability(mydate){
	var $return=true;
	var $returnclass ="available";
	$checkdate = $.datepicker.formatDate('mm/dd/yy', mydate);
	for(var i = 0; i < $mexisitingDates.length; i++)	{ 
	 if($mexisitingDates[i] == $checkdate){
		$return = false;
		$returnclass= "unavailable";
		
		}
	}
	
	return [$return,$returnclass];
}

	  
	 $(document).ready(function () {
			//$( "#datepicker" ).datepicker();
			
		$('input').filter('.datepickerclass').datepicker({ minDate: '<%=startYear%>',maxDate: '<%=endYear%>',beforeShowDay: checkAvailability  });
		$('#adddt').hide();

		 $('input[type=text]').bind("cut copy paste", function(e) {
	        e.preventDefault();
	        //alert("You cannot paste text into this textbox!");
	       /// $('input').bind("contextmenu", function(e) {
	           // e.preventDefault();
	       // });
	    });
		
	});

	 $('#moredt').click(function() {
		 
			if(dtc%3 ==1){
		    var spcount = $('#spcount').val();
			
			var indst = parseInt(spcount) + 1;
			var inded = parseInt(spcount) +3 ;
			var spval = parseInt(inded);
			if(parseInt(spval) > <%=dtsize%>){
				spval	=  parseInt(inded) - <%=dtsize%>;
				inded = parseInt(inded) -  parseInt(spval);
			}

			if($('#spcount').val() == ''){
				 spcount = <%=dtcount%>;
				 indst = parseInt(spcount) + 3;
				 inded = parseInt(spcount) +5 ;
				 spval = parseInt(inded);
			}

			for(var i=indst;i<=inded;i++){
				$('#lncvdt'+i).show();
				//document.getElementById('spcount').value = i;
				 $("#spcount").val(i);
				 
				if(i%3 == 0){
					var j = i /3;
					$('#pblock'+j).show();
				}
			}
		}//end mod 1
		
			if(dtc%3 ==2){
				
			    var spcount = $('#spcount').val();
				
				var indst = parseInt(spcount) + 1;
				var inded = parseInt(spcount) +3 ;
				var spval = parseInt(inded);
				if(parseInt(spval) > <%=dtsize%>){
					spval	=  parseInt(inded) - <%=dtsize%>;
					inded = parseInt(inded) -  parseInt(spval);
				}

				if($('#spcount').val() == 'NaN'){
					 spcount = <%=dtcount%>;
					 indst = parseInt(spcount) + 3;
					 inded = parseInt(spcount) +5 ;
					 spval = parseInt(inded);
				}
				
				for(var i=indst;i<=inded;i++){
					$('#lncvdt'+i).show();
					//document.getElementById('spcount').value = i;
					 $("#spcount").val(i);
					 
					if(i%3 == 0){
						var j = i /3;
						$('#pblock'+j).show();
					}
				}
			}//end mod 2
			
				if(dtc%3 ==0){
				
			    var spcount = $('#spcount').val();
				
				var indst = parseInt(spcount) + 1;
				var inded = parseInt(spcount) +3 ;
				var spval = parseInt(inded);
				if(parseInt(spval) > <%=dtsize%>){
					spval	=  parseInt(inded) - <%=dtsize%>;
					inded = parseInt(inded) -  parseInt(spval);
				}

				if($('#spcount').val() == 'NaN'){
					 spcount = <%=dtcount%>;
					 indst = parseInt(spcount) + 3;
					 inded = parseInt(spcount) +5 ;
					 spval = parseInt(inded);
				}
				
				for(var i=indst;i<=inded;i++){
					$('#lncvdt'+i).show();
					//document.getElementById('spcount').value = i;
					 $("#spcount").val(i);
					 
					if(i%3 == 0){
						var j = i /3;
						$('#pblock'+j).show();
					}
				}
			}//end mod 0
			
			
		});	
	  
	});

function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}


function goPay(){
	 var payfull = "N";


	 if(document.forms[0].payfull != undefined){
		 if(document.forms[0].payfull.checked == true){
			 payfull = "Y";
		 }
	}
	 var pcount =0;
	 var count = 0;
	 var ucount =0;
	 var newlist = new Array();
	 var hash = new Object();
	 for(var i=<%=dtcount%>;i<=<%=dtsize%>;i++){
		 if( $('#lncvDate'+i).val() != ''){
			 pcount++;
			 if (hash[ $('#lncvDate'+i).val()] != 1){
					newlist = newlist.concat( $('#lncvDate'+i).val());
					hash[ $('#lncvDate'+i).val()] = 1;
				}
				else { count++; }
		 }
	 }

	// start
		
	 // document.getElementById('unavialableDt').value="";
	  $('#unavialableDt').val('');

      var ublk = "";
      var pblk =0;
      var oblk =1;
      for(var i=<%=dtcount%>;i<=<%=dtsize%>;i++){
    	  if(  $('#pblk'+i).val() != undefined){
    		  pblk = $('#pblk'+i).val();
    		  if( $('#lncvDate'+i).val() != ''){
        		  var k = i+1;
        		  var m= k+1;
        		  if( $('#lncvDate'+k).val() != '' &&  $('#lncvDate'+m).val() != ''){
        			  //alert(document.getElementById('lncvDate'+m).value);
        			  //alert("m");
        			  var lcdt = $('#lncvDate'+m).val();// document.getElementById('lncvDate'+m).value;
        			  ublk = getNextDate(lcdt);
        			  $('#unavialableDt').get(0).value += ublk +",";
        			  $('#unavialableDt').get(0).value += getNextDate(ublk) +",";
      		    	 //  $('#unavialableDt').val() += ublk +",";
      		    	   //$('#unavialableDt').val() += getNextDate(ublk) +",";
        		  }else   
        		  if( $('#lncvDate'+k).val() != '' &&  $('#lncvDate'+m).val() == ''){
        			  //alert(document.getElementById('lncvDate'+k).value);
        			  //alert("k");
        			  var lcdt =  $('#lncvDate'+k).val();
        			  ublk = getNextDate(lcdt);
        			  $('#unavialableDt').get(0).value += ublk +",";
        			  $('#unavialableDt').get(0).value += getNextDate(ublk) +",";
      		    	 //  $('#unavialableDt').val() += ublk +",";
      		    	//   $('#unavialableDt').val() += getNextDate(ublk) +",";
        			  
        		  }else {
        			  //alert(document.getElementById('lncvDate'+i).value);
        			 // alert("i");
        			  var lcdt =  $('#lncvDate'+i).val();
        			  ublk = getNextDate(lcdt);
        			  $('#unavialableDt').get(0).value += ublk +",";
        			  $('#unavialableDt').get(0).value += getNextDate(ublk) +",";
        			  
      		    	  // $('#unavialableDt').val() += ublk +",";
      		    	  // $('#unavialableDt').val() += getNextDate(ublk) +",";
        		  }  
        	    
    		  }
    	  }
			
 		 
      }  	      
		//end	
		
	
	 var una =  $('#unavialableDt').val();


	for(var i=<%=dtcount%>;i<=<%=dtsize%>;i++){
		 if( $('#lncvDate'+i).val() != ''){
			  var lcdt=  $('#lncvDate'+i).val();
				// alert(una.indexOf(lcdt));
			  if ( una.indexOf(lcdt) != -1){
				  ucount++ ; 
			    }
			}
		 }

	
	
	
	 if(count >0){
		 alert("You cannot choose the same dates.Please enter a different date.");
	 }else if(pcount == 0){
		 alert("Please choose a date in order to proceed.");
	 }//else if(pcount%3 !=0){
		// alert("Please choose an entire permit block in order to proceed.");
	 //}
	else if(ucount >0){
		alert("A permit block must have more than 2 days (48 hours) spaced before and after it and any other permit block.");
		 return false;
	 }
	 else{
	 	 document.forms[0].action='<%=contextRoot%>/onlineLncvYear.do?action=pay&payfull='+payfull;
		 document.forms[0].submit();
	 }

}


function dispMin(){
	
	var lncv = '<%=lncv%>';
	var vehiclest = '<%=vehicleStat%>';
	
		
			if(lncv == 'Y' && vehiclest == 'Y'){
				for(var i=<%=dtcount%>;i<=3;i++){
					$('#lncvdt'+i).show();
					//document.getElementById('spcount').value = i;
					 $("#spcount").val(i);
				}
				for(var i=<%=dtcount%>+3;i<=<%=dtsize%>;i++){
						if(i%3 == 0){
							var j = i /3;
							if($('#pblock'+j)!= undefined ){
								$('#pblock'+j).hide();
							}
						}
						if($('#lncvdt'+i)!= undefined ){
								$('#lncvdt'+i).hide();
						}
						
						
					}
				}
			
			if(<%=dtcount%> %3 == 2){
				 var spcount = $('#spcount').val();
				 var modsp = parseInt(spcount) +1 ;
				 $("#spcount").val(modsp);
				 
			
			}
			
			if(<%=dtcount%> %3 == 0){
				 var spcount = $('#spcount').val();
				 var modsp = parseInt(spcount) +2 ;
				 $("#spcount").val(modsp);
				
			}
	}



function getNextDate(val){ 
	 if(val == '') return '';
	 var nDate = new Date(val);
	 var d = nDate.getDate();
	 var m = nDate.getMonth();
	 
	 var y = nDate.getFullYear();
     var NextDate= new Date(y, m, parseInt(d+1));
	
	 var c = NextDate.getDate();
	 if(c <10) c= "0"+c;
	 var mo = NextDate.getMonth()+1;
	 if(mo <10) mo= "0"+mo;
	 var Ndate= mo+"/"+c+"/"+NextDate.getFullYear();
	//alert(Ndate);
	
	return Ndate; // Form1 is the Formname
 }

function getNextBlockDate(val){ 
	 if(val == '') return '';
	 var nDate = new Date(val);
	 var d = nDate.getDate();
	 var m = nDate.getMonth();
	 var y = nDate.getYear();
	 var NextDate= new Date(y, m, parseInt(d+2));
	 var c = NextDate.getDate();
	 if(c <10) c= "0"+c;
	 
	 var mo = NextDate.getMonth()+1;
	 if(mo <10) mo= "0"+mo;
	 var Ndate= mo+"/"+c+"/"+NextDate.getYear();
	 
	return Ndate; // Form1 is the Formname
	
 }
 
function getNextBlockSpDate(val){ 
	 if(val == '') return '';
	 var nDate = new Date(val);
	 var d = nDate.getDate();
	 var m = nDate.getMonth();
	 var y = nDate.getYear();
	 var NextDate= new Date(y, m, parseInt(d+3));
	 var c = NextDate.getDate();
	 if(c <10) c= "0"+c;
	 var mo = NextDate.getMonth()+1;
	 if(mo <10) mo= "0"+mo;
	 var Ndate= mo+"/"+c+"/"+NextDate.getYear();
	return Ndate; // Form1 is the Formname
 }

// dates
function nxtDates(val,pos){
	
	var extDt = "<%=exisitingDt%>";
	  var i = pos+1;
	  var dt = val;
	  dt = getNextDate(val);
	  
	  
	
	  if(val.indexOf("12/31") != 0){
		  if(extDt.indexOf(dt) != -1){
			// document.getElementById('lncvDate'+i).value = '';
				$('#lncvDate'+i).val('');
			}else {
		  	  //document.getElementById('lncvDate'+i).value = dt;
		  	 	$('#lncvDate'+i).val(dt);
		   }
			
		  i = i+1;
		  if(val.indexOf("12/30") != 0){
			  if(extDt.indexOf(getNextDate(dt)) != -1){
					//document.getElementById('lncvDate'+i).value = '';
					$('#lncvDate'+i).val('');
			   }else {
				  // document.getElementById('lncvDate'+i).value = getNextDate(dt);
				   $('#lncvDate'+i).val(getNextDate(dt));

					   
				}
		  }else{
			  //document.getElementById('lncvDate'+i).value = '';
			  $('#lncvDate'+i).val('');
		  }
		  
	  }else {
		 // document.getElementById('lncvDate'+i).value = '';
		 $('#lncvDate'+i).val('');
          i = i+1; 	
          // document.getElementById('lncvDate'+i).value = '';
           $('#lncvDate'+i).val('');
		 
	  }
	  
	  var spcount = $('#spcount').val();
     
	  if(val.indexOf("12/29") == 0 || val.indexOf("12/30") == 0 || val.indexOf("12/31") == 0){
	  	  $('#adddt').hide();
	  }else {
		  $('#adddt').show();
	  }
     
	
   
  }	

function check(){
	var carCode= event.keyCode;
	event.returnValue = false;
}

function checkkey(id,val){
	
	if(event.keyCode == 8 || event.keyCode == 46){
		if(val==3){
			//document.getElementById('lncvDate'+id).value = '';
			$('#lncvDate'+id).val('');
		}else if(val==2){
			//document.getElementById('lncvDate'+id).value = '';
			$('#lncvDate'+id).val('');
			
			var g = parseInt(id) +1;
			//document.getElementById('lncvDate'+g).value = '';
			$('#lncvDate'+g).val('');
			
		}else {
			//document.getElementById('lncvDate'+id).value = '';
			$('#lncvDate'+id).val('');
			var g = parseInt(id) +1;
			//document.getElementById('lncvDate'+g).value = '';
			$('#lncvDate'+g).val('');
			var r = parseInt(g) +1;
			//document.getElementById('lncvDate'+r).value = '';
			$('#lncvDate'+r).val('');
		}	
	}else {	
	var carCode= event.keyCode;
	event.returnValue = false;
	}
}

function openHelp()
{
	window.open("<%=contextRoot%>/docs/LNCV_HELP.pdf");
}
</script>
</head>
<body  onpageshow="if (event.persisted) noBack();" onunload="" text="#000000" leftmargin="5" bottommargin="0"  scroll="yes" topmargin="0" marginwidth="0" marginheight="40" rightmargin="0" onLoad="dispMin();noBack();">
<html:base/>
<center>
<style type="text/css">
    .FireTitle          { FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  td.FireHeading        { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none; PADDING: 10px; BACKGROUND-COLOR:#D9D9D9 }
    .FireHeading        { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireGlance         { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  td.FireResource       { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none; PADDING: 6px }
    .FireResource       { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
  td.FireResourceStatic { FONT-SIZE: 12px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none; PADDING: 6px }
    .FireResourceStatic { FONT-SIZE: 12px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireSelect         { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireText           { FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireHighlight      { FONT-SIZE: 15px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #990000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireMessage        { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
  td.FireMenu           { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none; PADDING: 2px; BACKGROUND-COLOR:#000000 }
    .FireMenu           { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:link      { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:visited   { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:active    { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:hover     { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireMenu1          { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:link     { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:visited  { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:active   { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:hover    { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #FFFFFF; FONT-WEIGHT: bold; TEXT-DECORATION: none }
     td.FireHeading2     { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #FFFFFF; FONT-WEIGHT: bold; TEXT-DECORATION: none; PADDING: 10px; }
   
  @page Section1 {
                   size:8.5in 11.0in;
                   margin:0.5in 0.5in 0.5in 0.5in;
                 }
 p.MsoNormal, li.MsoNormal, div.MsoNormal {
                margin:0in;
                margin-bottom:.0001pt;
                font-size:12.0pt;
                font-family:"Arial"
              }
 table.atable {
                border-collapse:collapse;
                border:1px solid #B3B3B3
              }
 tr           {
                page-break-inside:avoid
              }
 div.Section1 {
                page:Section1;
              }


.panelVisited      { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #003265 }
.panelVisitedColor { background-color:#62A85F }
.panelActive       { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #FFFFFF }
.panelActiveColor  { background-color:#EAC563 }
.panel             { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #FFFFFF }
.panelColor        { background-color:#BB2F2F }

</style>

<html:form action="/onlineLncv">
<input type="hidden" name="spcount" id="spcount" />
<input type="hidden" name="activityId"  value="<%=actId%>"/>
<input type="hidden" name="unavialableDt" id="unavialableDt" />
<input type="hidden" name="lastdt" id="lastdt" />
<input type="hidden" name="lastpblk" id="lastpblk" />


<table align="center" cellpadding="0" cellspacing="0" >

<td align="center" valign="top" width="99%">
	<br><br> <br><br>
<fieldset style="width: 450px;" >
<br><br>
<table cellpadding="15" cellspacing="0" width="100%">
<html:errors />
<tr>

  <% if(vehicleStat.equalsIgnoreCase("N")){ %>
			 <tr>
		        <td align="left" > <font class="panelVisited" style="font-size:12px">Please modify your profile and make sure your Vehicle Number and your Drivers License Number are filled out properly as you will need both in order to proceed with your application for an LNCV Permit.
		        </td>
		    </tr>
		<% } else { %>
</table>		
<table cellpadding="2" cellspacing="2" border="4" height="100%" width="100%" style="border-width: 1px; 
	border-spacing: ;
	border-style: outset;
	border-color: gray;
	border-collapse: separate;
	background-color: white;">
	 <tr>
        <td align="center"> <font class="panelVisited" style="font-size:30px"><font class="panelVisited" style="font-size:12px">Choose the parking dates
        <img  width="12" height="12"  src="<%=contextRoot%>/jsp/online/images/help.png" style="cursor:pointer;cursor:hand" title="What is this ?" onclick="openHelp();"> </td>
    </tr>




   	<tr id="adddt">
		<td align="center" ><font class="panelVisited">Add more dates &nbsp; <img src="<%=contextRoot%>/jsp/online/images/add.png" alt="Add More Dates" title="Add More Dates" id="moredt"/>
	</td>
 	</tr>
		
		<% if(dtcount%3 == 1){ 
			int u =0;
		%>
		   		<% for (int i=dtcount;i<=dtsize;i++){
		   			
		   			%>
		
						
							<% if(i%3 == 1){
							 	int j = (i-1) /3 ;
							    j= j+1;
							    
							%>
							 <tr id="pblock<%=j%>">
								<td class="tabletext" colspan="2" bgcolor="#e5e5e5" align="center"><font class="FireHeading">Permit Block   &nbsp;</td>
								<input type="hidden" id="pblk<%=i%>" name="pblk<%=i%>" value="<%=j%>">
							</tr>
							<%     } %>
							
							<% if(i%3 == 1){
								 u = u+1;
							%>
							 <tr >
								<td id="lncvdt<%=i%>"  colspan="2">
								<font class="panelVisited">LNCV Date <%=i%> &nbsp;
									<input type="text" id="lncvDate<%=i%>" name="lncvDate<%=i%>"  class="datepickerclass" onchange="nxtDates(this.value,<%=i%>);"  onkeypress="check();" onkeydown="checkkey(<%=i%>,<%=u%>);"/>
									
								</td>
							</tr>
							<% }else {
							  u = u+1;
							  
							  
							%>
							<tr >
								<td id="lncvdt<%=i%>"  colspan="2">
								<font class="panelVisited">LNCV Date <%=i%> &nbsp;
									<input type="text" id="lncvDate<%=i%>" name="lncvDate<%=i%>"  onkeypress="check();" onkeydown="checkkey(<%=i%>,<%=u%>);" />
								</td>
							</tr>
								<% if(u==3) u=0; }
							 	
							%>
		
					<%  } %>
			<%   } %>
			
			<% if(dtcount%3 == 2){
				int u =0;
				%>
		   		<% for (int i=dtcount;i<=dtsize;i++){ %>
		
		
							<% if(i%3 == 2){
							 	int j = (i-1) /3 ;
							    j= j+1;
							  
							%>
							 <tr id="pblock<%=j%>">
								<td class="tabletext" colspan="2" bgcolor="#e5e5e5" align="center"><font class="FireHeading">Permit Block   &nbsp;</td>
								<input type="hidden" id="pblk<%=i%>" name="pblk<%=i%>" value="<%=j%>">
							</tr>
							<%     } %>
							
							<% if(i%3 == 2){
								 u = u+1;
							%>
							 <tr >
								<td id="lncvdt<%=i%>"  colspan="2">
								<font class="panelVisited">LNCV Date <%=i%> &nbsp;
									<input type="text" id="lncvDate<%=i%>" name="lncvDate<%=i%>"  class="datepickerclass" onchange="nxtDates(this.value,<%=i%>);"  onkeypress="check();" onkeydown="checkkey(<%=i%>,<%=u%>);"/>
								</td>
							</tr>
							<% }else {
								u = u+1;
								
							%>
							<tr >
								<td id="lncvdt<%=i%>"  colspan="2">
								<font class="panelVisited">LNCV Date <%=i%> &nbsp;
									<input type="text" id="lncvDate<%=i%>" name="lncvDate<%=i%>"  onkeypress="check();" onkeydown="checkkey(<%=i%>,<%=u%>);" />
								</td>
							</tr>
								<% if(u==3) u=0; }
							 	
							%>
		
					<%  } %>
			<%  } %>
			
			<% if(dtcount%3 == 0){
				int u = 0;
				%>
		   		<% for (int i=dtcount;i<=dtsize;i++){ %>
		
		
							<% if(i%3 == 0){
							 	int j = (i-1) /3 ;
							    j= j+1;
							   
							%>
							 <tr id="pblock<%=j%>">
								<td class="tabletext" colspan="2" bgcolor="#e5e5e5" align="center"><font class="FireHeading">Permit Block   &nbsp;</td>
								<input type="hidden" id="pblk<%=i%>" name="pblk<%=i%>" value="<%=j%>">
							</tr>
							<%     } %>
							
							<% if(i%3 == 0){
								 u = u+1;
							%>
							 <tr >
								<td id="lncvdt<%=i%>"  colspan="2">
								<font class="panelVisited">LNCV Date <%=i%> &nbsp;
									<input type="text" id="lncvDate<%=i%>" name="lncvDate<%=i%>"  class="datepickerclass" onchange="nxtDates(this.value,<%=i%>);"  onkeypress="check();" onkeydown="checkkey(<%=i%>,<%=u%>);" />
								</td>
							</tr>
							<% }else {
								u = u+1;
								
							%>
							<tr >
								<td id="lncvdt<%=i%>"  colspan="2">
								<font class="panelVisited">LNCV Date <%=i%> &nbsp;
									<input type="text" id="lncvDate<%=i%>" name="lncvDate<%=i%>"  onkeypress="check();" onkeydown="checkkey(<%=i%>,<%=u%>);" />
								</td>
							</tr>
								<% if(u==3) u=0; }
							 	
							%>
		
					<%  } %>
			<%   } %>
		
   		 <% if(actId == 0){ %>
   				 <tr>
						<td class="tabletext" colspan="2"> <input type="checkbox" name="payfull"/>
						<font class="panelVisited"> Check here if you wish to pay now for all the LNCV permits this year and choose remaining dates later. &nbsp;

						</td>
					</tr>

   		<%  } %>
   		
    	 		<tr>
        	 		<td class="FireSelect">
	                    <div align="center">
	                   		 <html:reset  value="Back" styleClass="FireSelect" style="width: 100px" onclick="history.back();"/>
	                      	  <html:button  property="button" value="<%=btn %>" styleClass="FireSelect" style="width: 100px" onclick="goPay();"/>
	                      </div>
                 	 </td>
               	 </tr>
		<% } %>
	</table>

</fieldset>
</html:form>
</center>
</td>
</html:html>

