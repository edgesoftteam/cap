<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%
 	String contextRoot = request.getContextPath();
 	java.util.List newProjectDetails = new java.util.ArrayList();
 	newProjectDetails = (java.util.List)request.getAttribute("newProjectDetails");
 	String forcePC="";
		for(int i=0;i<newProjectDetails.size();i++)
        {
           elms.control.beans.online.ApplyOnlinePermitForm applyOnlinePermitForm = (elms.control.beans.online.ApplyOnlinePermitForm) newProjectDetails.get(i);
           forcePC = applyOnlinePermitForm.getForcePC();
         }
         String tempOnlineID = (String) request.getAttribute("tempOnlineID");

         int startsAfter=elms.util.StringUtils.s2i((String)request.getAttribute("startsAfter"));



%>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
<script language="javascript">
function inatilizeDate()
{

     //adding the days to current date Getting Current Date
  var today = new Date();
  //Adding Number Of Days To Date
  var addDays   =new Date().setDate(today.getDate());
  //Getting the Date in UTC Format
  addDays = new Date(addDays);

  var afterAddDt =(addDays.getMonth() < 9 ? "0" + (addDays.getMonth() + 1).toString() : (addDays.getMonth() + 1).toString() )+
                     "/" +((addDays.getDate() + <%=startsAfter%>).toString() < 10 ? "0" + (addDays.getDate() + <%=startsAfter%>).toString() : (addDays.getDate()  + <%=startsAfter%>).toString())+
                     "/"+(addDays.getFullYear().toString());
     document.forms[0].startDate.value=afterAddDt;
}
</script>
</head>
<body text="#000000" leftmargin="5" bottommargin="0"  scroll="yes" topmargin="0" marginwidth="0" marginheight="0" rightmargin="0" onLoad="inatilizeDate();">

<script language="JavaScript" src="../script/calendar.js"></script>
<script src="../script/yCodelib.js"> </script>
<script>checkBrowser();</script>
<script language="javascript">

function createQuestionaire()
{
   var tempOnlineID = document.forms[0].tempOnlineID.value;
   var startDate=document.forms[0].startDate.value;
   var startDt = startDate.substring(6,10)+"/"+startDate.substring(0,2)+"/"+startDate.substring(3,5);

  //adding the days to current date Getting Current Date
  var today = new Date();
  //Adding Number Of Days To Date
  var addDays   =new Date().setDate(today.getDate());
  //Getting the Date in UTC Format
  addDays = new Date(addDays);

       //Getting the Current Date in MM/DD/YYYY Format

  var currentDt = ((addDays.getFullYear().toString())+
                  "/"+(addDays.getMonth() < 9 ? "0" + (addDays.getMonth() + 1).toString() : (addDays.getMonth() + 1).toString() )+
                  "/" +(addDays.getDate() < 10 ? "0" + addDays.getDate().toString() : addDays.getDate().toString())) ;

    var currentDtAlert = ((addDays.getMonth() < 9 ? "0" + (addDays.getMonth() + 1).toString() : (addDays.getMonth() + 1).toString() )+
                  "/" + (addDays.getDate() < 10 ? "0" + addDays.getDate().toString() : addDays.getDate().toString())+
                  "/" + (addDays.getFullYear().toString()));

    var afterAddDt =(addDays.getFullYear().toString())+
                     "/" +(addDays.getMonth() < 9 ? "0" + (addDays.getMonth() + 1).toString() : (addDays.getMonth() + 1).toString() )+
                     "/" +((addDays.getDate() + <%=startsAfter%>).toString() < 10 ? "0" + (addDays.getDate() + <%=startsAfter%>).toString() : (addDays.getDate()  + <%=startsAfter%>).toString());

      var afterAddDtAlert =(addDays.getMonth() < 9 ? "0" + (addDays.getMonth() + 1).toString() : (addDays.getMonth() + 1).toString() )+
                     "/" +((addDays.getDate() + <%=startsAfter%>).toString() < 10 ? "0" + (addDays.getDate() + <%=startsAfter%>).toString() : (addDays.getDate()  + <%=startsAfter%>).toString())+
                     "/" + (addDays.getFullYear().toString());

    if(startDate=="")
    {
     alert("Please select a date after " +currentDtAlert);
	 document.forms[0].startDate.focus();
    }else if(startDt <  currentDt){
     alert("Please select a date after " +currentDtAlert);
	 document.forms[0].startDate.focus();
  }else if(afterAddDt > startDt)
  {
   alert("Please select a date after" +afterAddDtAlert);
   document.forms[0].startDate.focus();
  }else{


   document.forms[0].action='<%=contextRoot%>/planCheckQuestionaire.do?tempOnlineID='+tempOnlineID+'&startDate='+startDate;
   document.forms[0].submit();
  }
}

function forcePCYsubmit()
{
	 var tempOnlineID = document.forms[0].tempOnlineID.value;
	 var startDate=document.forms[0].startDate.value;

   var startDt = startDate.substring(6,10)+"/"+startDate.substring(0,2)+"/"+startDate.substring(3,5);

  //adding the days to current date Getting Current Date
  var today = new Date();
  //Adding Number Of Days To Date
  var addDays   =new Date().setDate(today.getDate());
  //Getting the Date in UTC Format
  addDays = new Date(addDays);

       //Getting the Current Date in MM/DD/YYYY Format
  var currentDt = ((addDays.getFullYear().toString())+
                  "/"+(addDays.getMonth() < 9 ? "0" + (addDays.getMonth() + 1).toString() : (addDays.getMonth() + 1).toString() )+
                  "/" +(addDays.getDate() < 10 ? "0" + addDays.getDate().toString() : addDays.getDate().toString())) ;

   var currentDtAlert = ((addDays.getMonth() < 9 ? "0" + (addDays.getMonth() + 1).toString() : (addDays.getMonth() + 1).toString() )+
                  "/" + (addDays.getDate() < 10 ? "0" + addDays.getDate().toString() : addDays.getDate().toString())+
                  "/" + (addDays.getFullYear().toString()));

    var afterAddDt =(addDays.getFullYear().toString())+
                     "/" +(addDays.getMonth() < 9 ? "0" + (addDays.getMonth() + 1).toString() : (addDays.getMonth() + 1).toString() )+
                     "/" +((addDays.getDate() + <%=startsAfter%>).toString() < 10 ? "0" + (addDays.getDate() + <%=startsAfter%>).toString() : (addDays.getDate()  + <%=startsAfter%>).toString());

     var afterAddDtAlert =(addDays.getMonth() < 9 ? "0" + (addDays.getMonth() + 1).toString() : (addDays.getMonth() + 1).toString() )+
                     "/" +((addDays.getDate() + <%=startsAfter%>).toString() < 10 ? "0" + (addDays.getDate() + <%=startsAfter%>).toString() : (addDays.getDate()  + <%=startsAfter%>).toString())+
                     "/" + (addDays.getFullYear().toString());




    if(startDate=="")
    {
     alert("Please select a date after "+currentDtAlert);
	 document.forms[0].startDate.focus();
    }else if(startDt <  currentDt){
     alert("Please select a date after " +currentDtAlert);
	 document.forms[0].startDate.focus();
  }else if(afterAddDt > startDt)
  {
   alert(" Please select a date after " +afterAddDtAlert);
   document.forms[0].startDate.focus();
  }else{
    var forcePC="Y";
	document.forms[0].action='<%=contextRoot%>/updateOnlinePermit.do?tempOnlineID='+tempOnlineID+'&forcePC='+forcePC+'&startDate='+startDate;
    document.forms[0].submit();
    }
}

</script>


<html:base/>
<%
    java.util.List tempOnlineDetails = new java.util.ArrayList();
    tempOnlineDetails= (java.util.List)request.getAttribute("tempOnlineDetails");
%>

<html:form action="/planCheckQuestionaire">
<html:hidden property="tempOnlineID" />
<table align="center" cellpadding="0" cellspacing="0" style="height:100%" width="100%">

	<td align="center" valign="top" width="99%" >
	<br><br> <br><br>
<fieldset style="width: 450px;height: 320px" >

<br><br>
<table cellpadding="0" cellspacing="0" style="width:100%">
 <html:errors />
   <tr height="15%">
        <td align="left">&nbsp;&nbsp; <font class="panelVisited" style="font-size:30px"> 8<font class="panelVisited" style="font-size:12px"><bean:message key="startDate"/>
        </td>
   </tr>


   <tr>
               <td align="center">
                 	 <table border="0" cellspacing="8" cellpadding="2" height="0" width="0">
              <tr>

				   			<td align="center"> <font class="panelVisited" style="font-size:12px">Start Date : </td>
							<td class="tabletext" valign="center">
							<html:text property="startDate" size="20" maxlength="20"  styleClass="textbox"  />
							<html:link href="javascript:show_calendar('forms[0].startDate');"
                        onmouseover="window.status='Calendar';return true;"
                        onmouseout="window.status='';return true;">
                        <img src="../images/calendar.gif" width="16" height="15" border=0/></html:link></nobr>  <font class="panelVisited" style="font-size:12px">(MM/DD/YYYY)</td>
							</tr>

				<tr>	<td>&nbsp; </td></tr>
            <tr>

                    <td class="FireSelect" colspan="2">
                    <div align="center">
                    <html:reset  value="Back" styleClass="FireSelect" style="width: 100px" onclick="history.back();"/>

                     <%
						  if(forcePC.equalsIgnoreCase("Y"))
						  {
						  %>
						    <html:button  property="button" value="Next" styleClass="FireSelect" style="width: 100px"  onclick="forcePCYsubmit()"/>


              		<%}else {%>
                      <html:button  property="button" value="Next" styleClass="FireSelect" style="width: 100px" onclick="createQuestionaire()"/>
                     <%}%>


                      </div>
                      </td>
                </tr>

	 </table>

                </td>
              </tr>



</table>
</fieldset>
</html:form>

	</td>


<jsp:include page="onlineSideBar.jsp" flush="true">
				<jsp:param name="page" value="8" />
			</jsp:include>
 </table>
</html:html>