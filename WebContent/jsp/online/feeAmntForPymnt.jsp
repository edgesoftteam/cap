<!-- <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"> -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd"> 
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="elms.agent.OnlineAgent"%>
<%@page import="elms.util.StringUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="elms.control.beans.online.CartDetailsForm"%>
<%@page import="elms.control.beans.online.MyPermitForm"%>
<%@page import="elms.app.finance.ActivityFeeEdit"%>
<%@page import="java.util.List"%>
<%@ page import="elms.control.beans.PaymentMgrForm"%>
<%@ page import="elms.security.*,elms.common.*"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>

<%-- <app:checkLogon/> --%>
<%
String contextRoot = request.getContextPath();


NumberFormat formatter = NumberFormat.getCurrencyInstance();
String formatValue = formatter.format(0.00);

PaymentMgrForm paymentMgrForm = new PaymentMgrForm();
paymentMgrForm = (PaymentMgrForm) request.getAttribute("paymentMgrForm");

CartDetailsForm cartDetailsForm = new CartDetailsForm();
cartDetailsForm = (CartDetailsForm) request.getAttribute("cartDetailsForm");
int noOfItems =0;
noOfItems = StringUtils.s2i(cartDetailsForm.getCount());
String totalAmnt = formatValue;
if(cartDetailsForm.getTotalFeeAmount() != null){
	totalAmnt = cartDetailsForm.getTotalFeeAmount();
}
System.out.println("totalAmnt.. "+totalAmnt);

String cartId = cartDetailsForm.getCartId();

System.out.println("cartId "+cartId);
System.out.println("cartId "+request.getAttribute("cartId"));
List<MyPermitForm> actList = (List)request.getAttribute("actList");
System.out.println("actList..."+actList.size());
if(request.getAttribute("cartId") != null){
	cartId = (String)request.getAttribute("cartId");	
}
request.setAttribute("cartId", cartId);

String emailAddr = (String)request.getAttribute("emailAddr");
System.out.println("emailAddr "+emailAddr);

List<CartDetailsForm> cartDetailsForms = new ArrayList<CartDetailsForm>();
cartDetailsForms = (List<CartDetailsForm>)request.getAttribute("cartDetailsForms");
String totalamnt="0.00";
if(request.getAttribute("totalamnt") != null){
	totalamnt = (String)request.getAttribute("totalamnt");	
}
String cartMsg=null;
cartMsg = (String)request.getAttribute("cartMsg");
String displayMsg=null;
displayMsg = (String)request.getAttribute("displayMsg");
%>

<html:html>
<app:checkLogon />
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">

.tooltip {
  position: relative;
  display: inline-block;
}

.tooltip .tooltiptext {
  visibility: hidden;
  width: 260px;
  background-color: black;
  color: #fff;
  text-align: center;
  border-radius: 6px;
  padding: 5px 0;
  position: absolute;
  z-index: 1;
  top: -5px;
  right: 110%;
}

.tooltip .tooltiptext::after {
  content: "";
  position: absolute;
  top: 50%;
  left: 100%;
  margin-top: -5px;
  border-width: 5px;
  border-style: solid;
  border-color: transparent transparent transparent black;
}
.tooltip:hover .tooltiptext {
  visibility: visible;
}

.textBox{  
  width: 100%;
  border: 1px solid #aaa;
  border-radius: 4px;
  margin: 8px 0px;
  outline: none;  
  padding: 7px 8px 7px 30px;
  box-sizing: border-box;
  transition: 0.3s;
  border-color: #003366;
  box-shadow: 0 0 8px 0 dodgerBlue;
  background-position:left;   
  background-repeat:no-repeat;   
  padding-left:10px;
  font-size:22px;
}

/*---------------------------------------------*/
input {
	outline: none;
/* 	border: none; */
}

input[type=text], select {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 2px solid rgba(81, 203, 238, 1);
  border-radius: 32px;
  box-sizing: border-box;
<%--   background-image:url(<%=contextRoot%>/jsp/online/images/add.jpg);  --%>
/*   background-position:right;    */
/*   background-repeat:no-repeat; */

}
input[type=text]:focus, textarea:focus {
  box-shadow: 0 0 5px rgba(81, 203, 238, 1);
  padding: 12px 20px;
  margin: 5px 1px 3px 0px;
  border: 1px solid rgba(81, 203, 238, 1);
}

i{
color:white;
 }
.container {
  position: relative;
  text-align: right;
  color: white;
}
.centered {
  text-align: center;
  color:black;
  position: absolute;
  top: 9.5%;
 -webkit-left: 100%;
 -moz-left: 100%;
 -o-left: 100%;
 -ms-left: 100%;
  left: 99.5%; 
  transform: translate(-80%, -20%); 
}
.abutton {
		font-family:  Arial, Helvetica, sans-serif;;
		text-transform: uppercase;
		padding: 10px;
		padding-left: 10px;
		padding-right: 10px;
		margin: 10px;
		font-size: 14px;
		font-weight: bold;
		color: #000000;
		cursor: pointer;
		text-decoration:none;
	}
	
	.abutton:hover {
		color: #ffffff;
	}
	a.disabled {
	  pointer-events: none;
	  cursor: default;
	}	
	table.csui, table.csui_title { width: 100%; padding: 0px; }
	table.csui { background-color: #cccccc; border-spacing: 1px; border-collapse: separate; }
	td.csui { padding: 6px; font-family:  Arial, Helvetica, sans-serif; font-size: 15px; background-color: #ffffff; }
	
	a.csui, a.csuisub { color: #000000; text-decoration: none; font-size: 2em; }
	td.csui_header, td.csui_label { text-align:center;color:white;padding: 6px; font-family:  Arial, Helvetica, sans-serif; font-size: 15px; background-color: #003366; }

.blur-filter {
    -webkit-filter: blur(2px);
    -moz-filter: blur(2px);
    -o-filter: blur(2px);
    -ms-filter: blur(2px);
    filter: blur(2px);
}

.button {
  background: #003366;
  border-radius: 100px;
  padding: 10px 30px;
  color: white;
  text-decoration: none;
  font-size: 1.45em;
  margin: 0 15px;
}

/* Hover state animation applied here */
.button:hover { 
  -webkit-animation: hover 1200ms linear 2 alternate;
  animation: hover 1200ms linear 2 alternate;
}

/* Active state animation applied here */
.button:active {
  -webkit-animation: active 1200ms ease 1 alternate;
  animation: active 1200ms ease 1 alternate; 
  background: #5F9BE0;
}

/* Active state animation keyframes below */

@-webkit-keyframes active { 
  0% {transform: scale(1,1);}
  90% {transform: scale(.9,.88);}
  100% {transform: scale(.92,.9);}
}

keyframes active { 
  0% {transform: scale(1,1);}
  90% {transform: scale(.9,.88);}
  100% {transform: scale(.92,.9);}
}
@-webkit-keyframes hover { 
  0% { -webkit-transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  1.8% { -webkit-transform: matrix3d(1.016, 0, 0, 0, 0, 1.037, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.016, 0, 0, 0, 0, 1.037, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  3.5% { -webkit-transform: matrix3d(1.033, 0, 0, 0, 0, 1.094, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.033, 0, 0, 0, 0, 1.094, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  4.7% { -webkit-transform: matrix3d(1.045, 0, 0, 0, 0, 1.129, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.045, 0, 0, 0, 0, 1.129, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  5.31% { -webkit-transform: matrix3d(1.051, 0, 0, 0, 0, 1.142, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.051, 0, 0, 0, 0, 1.142, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  7.01% { -webkit-transform: matrix3d(1.068, 0, 0, 0, 0, 1.158, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.068, 0, 0, 0, 0, 1.158, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  8.91% { -webkit-transform: matrix3d(1.084, 0, 0, 0, 0, 1.141, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.084, 0, 0, 0, 0, 1.141, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  9.41% { -webkit-transform: matrix3d(1.088, 0, 0, 0, 0, 1.132, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.088, 0, 0, 0, 0, 1.132, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  10.71% { -webkit-transform: matrix3d(1.097, 0, 0, 0, 0, 1.107, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.097, 0, 0, 0, 0, 1.107, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  12.61% { -webkit-transform: matrix3d(1.108, 0, 0, 0, 0, 1.077, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.108, 0, 0, 0, 0, 1.077, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  14.11% { -webkit-transform: matrix3d(1.114, 0, 0, 0, 0, 1.067, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.114, 0, 0, 0, 0, 1.067, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  14.41% { -webkit-transform: matrix3d(1.115, 0, 0, 0, 0, 1.067, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.115, 0, 0, 0, 0, 1.067, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  16.32% { -webkit-transform: matrix3d(1.119, 0, 0, 0, 0, 1.077, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.119, 0, 0, 0, 0, 1.077, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  18.12% { -webkit-transform: matrix3d(1.121, 0, 0, 0, 0, 1.096, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.121, 0, 0, 0, 0, 1.096, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  18.72% { -webkit-transform: matrix3d(1.121, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.121, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  20.02% { -webkit-transform: matrix3d(1.121, 0, 0, 0, 0, 1.113, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.121, 0, 0, 0, 0, 1.113, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  21.82% { -webkit-transform: matrix3d(1.119, 0, 0, 0, 0, 1.119, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.119, 0, 0, 0, 0, 1.119, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  24.32% { -webkit-transform: matrix3d(1.115, 0, 0, 0, 0, 1.11, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.115, 0, 0, 0, 0, 1.11, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  25.53% { -webkit-transform: matrix3d(1.113, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.113, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  29.23% { -webkit-transform: matrix3d(1.106, 0, 0, 0, 0, 1.089, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.106, 0, 0, 0, 0, 1.089, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  29.93% { -webkit-transform: matrix3d(1.105, 0, 0, 0, 0, 1.09, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.105, 0, 0, 0, 0, 1.09, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  35.54% { -webkit-transform: matrix3d(1.098, 0, 0, 0, 0, 1.105, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.098, 0, 0, 0, 0, 1.105, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  36.64% { -webkit-transform: matrix3d(1.097, 0, 0, 0, 0, 1.106, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.097, 0, 0, 0, 0, 1.106, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  41.04% { -webkit-transform: matrix3d(1.096, 0, 0, 0, 0, 1.099, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.096, 0, 0, 0, 0, 1.099, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  44.04% { -webkit-transform: matrix3d(1.096, 0, 0, 0, 0, 1.097, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.096, 0, 0, 0, 0, 1.097, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  51.45% { -webkit-transform: matrix3d(1.099, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.099, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  52.15% { -webkit-transform: matrix3d(1.099, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.099, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  58.86% { -webkit-transform: matrix3d(1.101, 0, 0, 0, 0, 1.099, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.101, 0, 0, 0, 0, 1.099, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  63.26% { -webkit-transform: matrix3d(1.101, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.101, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  66.27% { -webkit-transform: matrix3d(1.101, 0, 0, 0, 0, 1.101, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.101, 0, 0, 0, 0, 1.101, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  73.77% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  81.18% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  85.49% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  88.59% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  96% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  100% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); } 
}

@keyframes hover { 
  0% { -webkit-transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  1.8% { -webkit-transform: matrix3d(1.016, 0, 0, 0, 0, 1.037, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.016, 0, 0, 0, 0, 1.037, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  3.5% { -webkit-transform: matrix3d(1.033, 0, 0, 0, 0, 1.094, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.033, 0, 0, 0, 0, 1.094, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  4.7% { -webkit-transform: matrix3d(1.045, 0, 0, 0, 0, 1.129, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); tra6nsform: matrix3d(1.045, 0, 0, 0, 0, 1.129, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  5.31% { -webkit-transform: matrix3d(1.051, 0, 0, 0, 0, 1.142, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.051, 0, 0, 0, 0, 1.142, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  7.01% { -webkit-transform: matrix3d(1.068, 0, 0, 0, 0, 1.158, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.068, 0, 0, 0, 0, 1.158, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  8.91% { -webkit-transform: matrix3d(1.084, 0, 0, 0, 0, 1.141, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.084, 0, 0, 0, 0, 1.141, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  9.41% { -webkit-transform: matrix3d(1.088, 0, 0, 0, 0, 1.132, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.088, 0, 0, 0, 0, 1.132, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  10.71% { -webkit-transform: matrix3d(1.097, 0, 0, 0, 0, 1.107, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.097, 0, 0, 0, 0, 1.107, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  12.61% { -webkit-transform: matrix3d(1.108, 0, 0, 0, 0, 1.077, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.108, 0, 0, 0, 0, 1.077, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  14.11% { -webkit-transform: matrix3d(1.114, 0, 0, 0, 0, 1.067, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.114, 0, 0, 0, 0, 1.067, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  14.41% { -webkit-transform: matrix3d(1.115, 0, 0, 0, 0, 1.067, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.115, 0, 0, 0, 0, 1.067, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  16.32% { -webkit-transform: matrix3d(1.119, 0, 0, 0, 0, 1.077, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.119, 0, 0, 0, 0, 1.077, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  18.12% { -webkit-transform: matrix3d(1.121, 0, 0, 0, 0, 1.096, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.121, 0, 0, 0, 0, 1.096, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  18.72% { -webkit-transform: matrix3d(1.121, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.121, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  20.02% { -webkit-transform: matrix3d(1.121, 0, 0, 0, 0, 1.113, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.121, 0, 0, 0, 0, 1.113, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  21.82% { -webkit-transform: matrix3d(1.119, 0, 0, 0, 0, 1.119, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.119, 0, 0, 0, 0, 1.119, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  24.32% { -webkit-transform: matrix3d(1.115, 0, 0, 0, 0, 1.11, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.115, 0, 0, 0, 0, 1.11, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  25.53% { -webkit-transform: matrix3d(1.113, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.113, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  29.23% { -webkit-transform: matrix3d(1.106, 0, 0, 0, 0, 1.089, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.106, 0, 0, 0, 0, 1.089, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  29.93% { -webkit-transform: matrix3d(1.105, 0, 0, 0, 0, 1.09, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.105, 0, 0, 0, 0, 1.09, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  35.54% { -webkit-transform: matrix3d(1.098, 0, 0, 0, 0, 1.105, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.098, 0, 0, 0, 0, 1.105, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  36.64% { -webkit-transform: matrix3d(1.097, 0, 0, 0, 0, 1.106, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.097, 0, 0, 0, 0, 1.106, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  41.04% { -webkit-transform: matrix3d(1.096, 0, 0, 0, 0, 1.099, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.096, 0, 0, 0, 0, 1.099, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  44.04% { -webkit-transform: matrix3d(1.096, 0, 0, 0, 0, 1.097, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.096, 0, 0, 0, 0, 1.097, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  51.45% { -webkit-transform: matrix3d(1.099, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.099, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  52.15% { -webkit-transform: matrix3d(1.099, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.099, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  58.86% { -webkit-transform: matrix3d(1.101, 0, 0, 0, 0, 1.099, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.101, 0, 0, 0, 0, 1.099, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  63.26% { -webkit-transform: matrix3d(1.101, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.101, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  66.27% { -webkit-transform: matrix3d(1.101, 0, 0, 0, 0, 1.101, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.101, 0, 0, 0, 0, 1.101, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  73.77% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  81.18% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  85.49% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  88.59% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  96% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  100% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); } 
}

html, body {
   background: url(<%=contextRoot%>/jsp/online/images/burbank2.jpg) no-repeat center center fixed;
   -webkit-background-size: cover;
   -moz-background-size: cover; 
   -o-background-size: cover; 
   background-size: cover; 
  width: 100%;
  height: 100%;
  margin: 0;
  font-family:'Open Sans';
}

a { text-decoration: none; }

.page-wrapper {
  width: 100%;
  height: 100%;
  background-size: cover;
}

.blur-it {
  -webkit-filter: blur(5px);
  filter: blur(5px);
}
a.btn {
  width: 100px;
  padding: 18px 0;
  position: absolute;
  font-family: 'Open Sans', Arial, Helvetica, sans-serif;
  text-align: center;
  text-decoration: none;
  text-transform: uppercase;
  color: #fff;
  border-radius: 0;
/*   background: #e2525c; */
color: #000000; text-decoration: none; 
font-size: 17px;
}

.modal-wrapper {
  width: 150%;
  height: 100%;
  position: fixed; 
  top: 0;
  left: -30%;
  background: rgba(41, 171, 164, 0.2);
  visibility: hidden;
  opacity: 0;
  -webkit-transition: all 0.25s ease-in-out;
  transition: all 0.25s ease-in-out;
}

.modal-wrapper.open {
  opacity: 1;
  visibility: visible;
}

.modal {
  width: 720px;
  display: block;
  margin: 40% 0 0 -100px;
  left:40%;
  position: relative;
  background: #fff;
  opacity: 0;
  -webkit-transition: all 0.5s ease-in-out;
  transition: all 0.5s ease-in-out;
}

.modal-wrapper.open .modal {
  margin-top: 100px;
  opacity: 1;
}

.head {
  width: 92%;
  height: 30px;
  padding: 12px 30px;
  overflow: hidden;
  background: #336699;
  font-family: Arial, Helvetica, sans-serif;
}

.btn-close {
  font-size: 28px;
  display: block;
  float: right;
  color: white;
}

.content { padding: 6%; width:83%;}
h1 { margin:0; padding:0; text-align:center; color:#fff;}
</style>

<script src="../script/sweetalert.min.js"></script> 
<script src="../script/sweetalert.js"></script>
<link rel="stylesheet" href="/css/main.css" type="text/css">
<link rel="stylesheet" href="../css/sweetalert.min.css" type="text/css">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<script src="../script/jquery-3.2.1.min.js"></script>
<script src="../script/main.js"></script>

<script src="../script/sweetalert.min.js"></script> 
<script src="../script/sweetalert.js"></script>
<script src="../script/sweetalert.min2.js"></script>
<link rel="stylesheet" href="../css/sweetalert.min2.css">
<link rel="stylesheet" href="../css/sweetalert.min.css" type="text/css">

<script type="text/javascript">

(function ($) {
    "use strict";
    /*==================================================================
    [ Focus Contact2 ]*/
    $('.input100').each(function(){
        $(this).on('blur', function(){
            if($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        })    
    })    

})(jQuery);
</script>
<script>
/*All the scripts should be here in one place and not scattered all over the file*/
var xmlhttp = false;
var date = "";
   try {
     xmlhttp = new XMLHttpRequest();
   } catch (trymicrosoft) {
     try {
       xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
     } catch (othermicrosoft) {
       try {
         xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
       } catch (failed) {
         xmlhttp = false;
       }
     }
   }
   if (!xmlhttp)
	   swal("Error initializing XMLHttpRequest!");
</script>
<SCRIPT language="JavaScript">

function removeReg() {
<%if(displayMsg != "" && displayMsg != null){%>
swal("<%=displayMsg%>");       	  	
<%}%>              

<%if(cartMsg != "" && cartMsg != null){%>
swal("<%=cartMsg%>");
<%}%>
}

function goToPay(){
	document.getElementById("button").style.display="none";
	document.getElementById("button").style.display="none";
	
    window.open('<%=contextRoot%>/processPayment.do?levelId=<%=cartId%>&amount=<%=totalamnt%>&online=Y&from=OnlineDirect',"_self");
}

function addPermit(){
		var actNbr=document.forms[0].elements['actNum'].value;
		if(actNbr == "" || actNbr == null){
			swal("Please enter permit number");
			document.forms[0].elements['actNum'].focus;
			return false;
		}else{
			document.forms[0].action = "<%=contextRoot%>/simplePayment.do?action=addPermit";
			document.forms[0].submit();
		}
}

function removePermit(levelId){
	swal({
		  title: "Are you sure?",
		  text: "Do you want to untag this permit from your email address?",
// 		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes",
		  cancelButtonText: "No",
		  closeOnConfirm: false,
		  closeOnCancel: false
		},
		function(isConfirm) {
		  if (isConfirm) {			  
			  var params = {levelId:"'"+levelId+"'"};	
			  document.forms[0].method = 'post';
			  document.forms[0].action = "<%=contextRoot%>/removeActivity.do";	  
			  for (const key in params) {
			    if (params.hasOwnProperty(key)) {
			      const hiddenField = document.createElement('input');
			      hiddenField.type = 'hidden';
			      hiddenField.name = key;
			      hiddenField.value = params[key];

			      document.forms[0].appendChild(hiddenField);
			    }
			  }			 
			  
			  document.forms[0].submit();
		  }else{
			  swal("Permit is not removed.");
		  }
		});
		
}
function refresh(){	
	document.forms[0].action = "<%=contextRoot%>/simplePayment.do?action=initialOne";
	document.forms[0].submit();
}
function addToCart(levelId,amnt){	 
	var params = {action:'add', amount:"'"+amnt+"'", levelId:"'"+levelId+"'"};	
	  document.forms[0].method = 'post';
	  document.forms[0].action = "<%=contextRoot%>/addToCart.do";	  
	  for (const key in params) {
	    if (params.hasOwnProperty(key)) {
	      const hiddenField = document.createElement('input');
	      hiddenField.type = 'hidden';
	      hiddenField.name = key;
	      hiddenField.value = params[key];

	      document.forms[0].appendChild(hiddenField);
	    }
	  }	  
	 
	  document.forms[0].submit();
}
function removeCart(levelId){	
	var params = {action:'remove', levelId:"'"+levelId+"'"};		
	document.forms[0].method = 'post';
	document.forms[0].action = "<%=contextRoot%>/addToCart.do";	  
	  for (const key in params) {
	    if (params.hasOwnProperty(key)) {
	      const hiddenField = document.createElement('input');
	      hiddenField.type = 'hidden';
	      hiddenField.name = key;
	      hiddenField.value = params[key];

	      document.forms[0].appendChild(hiddenField);
	    }
	  }	  
	  
	  document.forms[0].submit(); 
}
function goBack(){
	document.forms[0].action = "<%=contextRoot%>/jsp/online/simplePayment.jsp";
	document.forms[0].submit();
}

</SCRIPT>

</head>
<center>
<body class="csui" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<br><br>
<html:form action="/simplePayment.do?action=addPermit" method="post">
<fieldset style="width: 85%;background-color: white;">
<!--   <legend class="FireResourceStatic"> -->
     <table cellpadding="0" cellspacing="0" >
            <tr>
              <td><img width="130%" src="<%=contextRoot%>/jsp/online/images/<%=elms.agent.LookupAgent.getKeyValue("ONLINE_LOGO") %>"><br/></td>
            </tr>
      </table>
<!--   </legend> -->
  <table cellpadding="50" cellspacing="0" width="65%" border="0" class="csui">
  <html:errors/>
  <tr class="csui">
        
	        <%if(displayMsg != "" && displayMsg != null){%>
			<td class="csui" style="font-size:26px;color:green;" align="center" colspan="7"><b><%=displayMsg%></b></td>	  	
			<%}%>              
			
			<%if(cartMsg != "" && cartMsg != null){%>
			<td class="csui" style="font-size:26px;color:green;" align="center" colspan="7"><b><%=cartMsg%></b></td>
			<%}%>
      		
        </tr>
        <tr class="csui"  >
		  	<td class="csui_header" colspan="7" style="text-align: right;">
			  	 Total Cart Value: <%=totalAmnt %>	&nbsp;&nbsp;&nbsp;
			</td>
        </tr>
        <tr class="csui">
        <td colspan="7"  class="csui">
        	<table  class="csui">
        		<tr class="csui">
						<td class="csui" style="color: #003265; font-size: 24px;font-weight: bold;text-align:center;">
<!-- 						</td> -->
<!-- 		              <td class="csui" style="color: #003265;align:right;font-weight: bold"> -->
		                <div class="container" >
						<div class="tooltip">
						  <span class="tooltiptext">Add a permit number</span>
<%-- 						  <html:text  name = "paymentMgrForm"  property="actNum" maxlength="9"  size="20" styleClass="textBox" />  --%>
						  <input style="font-size: 18px;" align="top" type="text" maxlength="9" placeholder="Add a Permit Number" name="actNum">
					  </div>
						<img title="Add a permit number" alt="" width="3.7%" align="top" onclick="addPermit();" src="<%=contextRoot%>/jsp/online/images/add.jpg">				
		                <a href="javascript:refresh();" title="Refresh"><img alt="" align="top" width="4%" onclick="refresh();" src="<%=contextRoot%>/jsp/images/refresh.jpg" ></a>
		               <%if(noOfItems > 0){ %><a class="trigger" id="updateCartLock" href="#"> <%} %><img align="top" title="Go to cart" width="5.5%" src="<%=contextRoot%>/jsp/images/cart.jpg" alt="Cart"> <%if(noOfItems > 0){ %></a><%} %>
				        <%if(noOfItems > 0){ %>
								  <div class="centered"><%=noOfItems %></div>
				        <%}else{%>
								  <div class="centered">0</div>
				        <%} %>
						</div>
						MY UNPAID PERMITS
						</td>	
        		</tr>
        	</table>
        </td>
        		
        </tr>                
        
    <tr>
      <td align="center" colspan="7" class="csui" >
        <table cellpadding="5" style="border: 1px solid #808080;" cellspacing="0" class="csui">  
        <tr>
			<td class="csui_header" width="6%">Untag</td>
			<td class="csui_header" width="15%">Permit Number</td>
			<td class="csui_header" width="27%">Permit Type</td>
			<td class="csui_header" width="20%">Status </td>
			<td class="csui_header" width="25%">Address </td>
			<td class="csui_header" width="11%">Amount </td>
			<td class="csui_header" width="25%">Add/Remove</td>
		</tr>
          		<html:hidden property="email" name="paymentMgrForm"></html:hidden>
          		<html:hidden property="cartId" name="paymentMgrForm"></html:hidden>
               <logic:iterate id="mypermitForm" name="actList" type="elms.control.beans.online.MyPermitForm" scope="request">
                 <logic:greaterThan  name="mypermitForm" property="amount" value="<%=formatValue %>">	
                   <tr valign="top" style="line-height:100%;">
                     <logic:greaterThan  name="mypermitForm" property="onlineActPeopleId" value="0">
	                  	<td class="csui" width="10%" style="color: #003265; font-size:16px; font-weight: bold" >
	                  		<div class="untagRecord">
	                  			<a class="abutton" href="javascript:removePermit(<bean:write name="mypermitForm" property="activityId"/>);" ><img alt="" width="36%" title="Untag" src="<%=contextRoot%>/jsp/images/delete1.jpg"></a>
	                  		</div>
	                  	</td>
	                  </logic:greaterThan>
	                  <logic:lessEqual  name="mypermitForm" property="onlineActPeopleId" value="0">
	                  	<td class="csui" width="10%">&nbsp;</td>
	                  </logic:lessEqual>
	       			 
       			       <td class="csui" valign="top" align="left">
                       <a href='<%=contextRoot%>/myPermits.do?action=exploreOnline&viewPermit=Yes&activityId=<bean:write name="mypermitForm" property="activityId" />'> 
                       <bean:write name="mypermitForm" property="activityNo" /> 
                       </a>
                       </td>
                       <td class="csui"> <bean:write name="mypermitForm" property="activityType"/></td>
                       <td class="csui"> <bean:write name="mypermitForm" property="activityStatus"/></td>
                       <td class="csui"> <bean:write name="mypermitForm" property="address"/></td>
                       <td class="csui" align="left"><bean:write name="mypermitForm" property="amount"/></td>

                       <logic:greaterThan  name="mypermitForm" property="amnt" value="0.00">	
	                       <td class="csui" align="left" >
	                       	<logic:equal  name="mypermitForm" property="addedToCart" value="false">
								<a class="abutton" href="javascript:addToCart('<bean:write name="mypermitForm" property="activityId"/>','<bean:write name="mypermitForm" property="amount"/>')"  ><img  title="Add to cart" alt="" width="53%" src="<%=contextRoot%>/jsp/images/addcart.jpg"></a>
						 </logic:equal>
						  <logic:equal name="mypermitForm" property="addedToCart" value="true">
			    			<div class="removing-from-cart-div">
			    				<a class="abutton" href="javascript:removeCart(<bean:write name="mypermitForm" property="activityId"/>)" ><img title="Remove from cart" alt="" width="53%" src="<%=contextRoot%>/jsp/images/removecart.jpg"></a>
					  	  	</div>
					  	  </logic:equal>
						  </td>
					   </logic:greaterThan>	
					   <logic:lessEqual   name="mypermitForm" property="amnt" value="0.00">
					    <td class="csui" align="left">&nbsp;</td>
					   </logic:lessEqual>
                   </tr>
                 </logic:greaterThan>
               </logic:iterate>     
          <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          </tr>
        </table>
        <table>
        <tr>
        <td colspan="2">&nbsp;</td>
        </tr>
          <tr>
            <td class="csui" align="center"> <a href="javascript:goBack();" class="button" style="color: white;text-decoration: none;font-size: 17px;">Exit</a></td>
	        <%if(noOfItems > 0){ %> 
    	    <td class="csui" align="center"><a href="#" class="button trigger" style="color: white;text-decoration: none;font-size: 17px;">Go To Cart</a></td>
        	<%} %>
          </tr>
       
       <tr>
          <td>


<!-- Modal -->
<div class="modal-wrapper">
  <div class="modal">
    <div class="head"  style="color:white;font-size:24px;">Cart Details Page<a class="btn-close trigger" id="updateCartLockFlag" style="color:white;" href="#"><i class="fa fa-times" aria-hidden="true"></i>X </a> </div>
    <div class="content">
    
	<table width="100%" cellpadding="0" cellspacing="0">
	    <tr class="csui">	      
		  <td width="100%" class="csui" style="color:red;"> Note: The name that will appear as the payee on the payment receipt is the credit card holder's name.</td>
		</tr>
	</table>
  <table cellpadding="50" cellspacing="0" width="80%" class="csui">
    <tr>
      <td align="center" class="csui" >
        <table width="100%" cellpadding="5" cellspacing="0">
        <tr class="csui"  >
              <td colspan="6" class="csui" style="color: #003265; font-size:22px; font-weight: bold">Cart Details</td>
        </tr>                
	    <tr>
			<td class="csui_header" style="font-size:18px;" width="10%">Permit Number</td>
			<td class="csui_header" style="font-size:18px;text-align:left;" width="10%">Amount </td>
			</tr>
          <tr bgcolor="FFFFFF">
            <logic:iterate id="cartDetailsForm1" name="cartDetailsForms" type="elms.control.beans.online.CartDetailsForm" scope="request">
                    <tr valign="top">
                        <td class="csui" align="center"> <bean:write name="cartDetailsForm1" property="activityNo" /> </td>
                   		<td class="csui" align="left"><bean:write name="cartDetailsForm1" property="amount"/></td>
                    </tr>
                </logic:iterate>  
                      
			</tr>
          <tr>
			<td class="csui_header" style="font-size:18px;">Total Cart Value : </td>
			<html:hidden name="cartDetailsForm" property="cartId"></html:hidden>
			<td class="csui" style=" background-color: #003366;color:white;"><bean:write name="cartDetailsForm" property="totalFeeAmount"/></td>
			</tr>			
          <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          </tr>
        </table>
        <table>
          <tr>
    	    <td class="csui" align="center"><a href="#" id="button" class="button trigger" style="color: white;text-decoration: none;font-size: 17px;" onclick="goToPay()">Proceed to Payment</a></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
    </div>
  </div>
</div>
<script src="../../../tools/jquery/jquery-3.1.0.slim.min.js"></script>   

<script>
$( document ).ready(function() {
  $('.trigger').on('click', function() {
     $('.modal-wrapper').toggleClass('open');
    $('.page-wrapper').toggleClass('blur-it');
     return false;
  });
});

$('#updateCartLock').on('click', function() {	  
	  $.ajax({
		  method: "POST",
		  url: "<%=contextRoot%>/updateCartLock.do",
		  data: { cartId:<%=cartId%>, action:"updateCartLock" }		  
		});
		  
});

$(document).on("click",".removing-from-cart-div a", function(event) { 
	  
	     var url_string = $(this).attr('href');		     
	     var levelId = url_string.substring(
				  url_string.lastIndexOf("(") + 1, 
				  url_string.lastIndexOf(")")
				);	  	 
		   event.preventDefault();	
	     $.ajax({
			  method: "POST",
			  url: "<%=contextRoot%>/updateCartLock.do",
			  data: { cartId:<%=cartId%>, action:"checkExistOrNot",levelId:levelId },
			  success: function(resultData) {				  
				  if(resultData.localeCompare("Y") == 0){			    	
				      swal("This permit can not be removed from cart, it is under process for payment.");					 	
				  }else{
					    removeCart(levelId);
				  }
			  } 
			});
});    


/* this function will update cart lock flag to N in cart and cart_details table. It will call on closing of payment modal window*/
$('#updateCartLockFlag').on('click', function() {	  

	  $.ajax({
		  method: "POST",
		  url: "<%=contextRoot%>/updateCartLock.do",
		  data: { cartId:<%=cartId%>, action:"updateCartLockFlag" }		 
		});
		  
});

$(document).on("click",".untagRecord a", function(event) {
	  var url_string = $(this).attr('href');	  
	  var levelId = url_string.substring(
			  url_string.lastIndexOf("(") + 1, 
			  url_string.lastIndexOf(")")
			);
	  
	  event.preventDefault();	
	     $.ajax({
			  method: "POST",
			  url: "<%=contextRoot%>/updateCartLock.do",
			  data: { cartId:<%=cartId%>, action:"checkExistOrNot",levelId:levelId },
			  success: function(resultData) {				  
				  if(resultData.localeCompare("Y") == 0){			    	
				      swal("The permit can not be untagged for this user, it is under processing for payment.");					 	
				  }else{
					    removePermit(levelId); 			  
				  }
			  } 
		});		 
});
 
</script>
            </td>  
          </tr>
        </table>
      </td>
    </tr>
  </table>
</fieldset>
</html:form>
</body>
</center>
</html:html>
