<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<app:checkLogon />

<html:html>
<head>
<html:base />
<title>City of Burbank :Permitting and Licensing System: People Manager :
add online People</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%
String contextRoot = request.getContextPath();
String peopleType = (String) request.getAttribute("peopleType") !=null ?(String) request.getAttribute("peopleType"):"";
String tempOnlineID= (String)request.getAttribute("tempOnlineID") !=null ?(String)request.getAttribute("tempOnlineID"):"";
%>
<script language="JavaScript" src="../script/calendar.js"></script>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<SCRIPT language="JavaScript">

var strValue;
function validateFunction()
{
	strValue=true;
	if (strValue==true) {
		if(document.forms['peopleForm'].elements['people.licenseNbr'])
          if (document.forms['peopleForm'].elements['people.licenseNbr'].value == '') {
    	     alert('License Number is a required field');
    	     document.forms['peopleForm'].elements['people.licenseNbr'].focus();
    	     strValue=false;
          }
    }
	if (strValue==true) {
          if (document.forms['peopleForm'].elements['people.name'].value == '') {
    	     alert('Name is a required field');
    	     document.forms['peopleForm'].elements['people.name'].focus();
    	     strValue=false;
          }
    }
   if (strValue==true){
        if (document.forms['peopleForm'].elements['people.address'].value == '') {
    	   alert('Address is a required field');
    	   document.forms['peopleForm'].elements['people.address'].focus();
    	   strValue=false;
        }
   }
   if (strValue==true){
        if (document.forms['peopleForm'].elements['people.city'].value == '') {
    	   alert('City is a required field');
    	   document.forms['peopleForm'].elements['people.city'].focus();
    	   strValue=false;
        }
   }
   if (strValue==true){
        if (document.forms['peopleForm'].elements['people.state'].value == '') {
    	   alert('State is a required field');
    	   document.forms['peopleForm'].elements['people.state'].focus();
    	   strValue=false;
        }
   }
   if (strValue==true){
   		if(isInteger(document.forms['peopleForm'].elements['people.zipCode'].value)){
	        if (document.forms['peopleForm'].elements['people.zipCode'].value == '') {
	    	   alert('Zip Code is a required field');
	    	   document.forms['peopleForm'].elements['people.zipCode'].focus();
	    	   strValue=false;
	        }
        }else{
	        alert("Enter a Integer");
	        document.forms['peopleForm'].elements['people.zipCode'].value='';
	        document.forms['peopleForm'].elements['people.zipCode'].focus();
	        strValue=false;
        }
   }
   if (strValue==true){
      if (document.forms['peopleForm'].elements['people.phoneNbr'].value == '') {
    	alert('Phone # is a required field');
    	document.forms['peopleForm'].elements['people.phoneNbr'].focus();
    	strValue=false;
      }
   }
   if (strValue==true){
      if (document.forms['peopleForm'].elements['people.emailAddress'].value == '') {
    	alert('E-Mail is a required field');
    	document.forms['peopleForm'].elements['people.emailAddress'].focus();
    	strValue=false;
      }
   }
   return strValue;
}

function DisplayHyphen(str)
{

	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46))
	{
		event.returnValue = false;
	}
	else
	{
		if ((document.peopleForm.elements[str].value.length == 3 ) || (document.peopleForm.elements[str].value.length == 7 ))
		{
			document.peopleForm.elements[str].value = document.peopleForm.elements[str].value +'-';
 		}
 		if (document.peopleForm.elements[str].value.length > 11 )  event.returnValue = false;
	}
}

function checkEmail(str){
	if(!validateEmail(str.value)){
		alert("please provide a valid E-mail");
		str.value='';
		str.focus();
		return false;
	}
	return true;
}

function savePeople(){
	document.forms[0].action='<%=contextRoot%>/addNewPeople.do?action=save&peopleType=<%=peopleType%>&tempOnlineID=<%=tempOnlineID%>';
}
</SCRIPT>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0"
	marginwidth="0" marginheight="0">
<html:errors />
<html:form action="/addNewPeople" onsubmit="return validateFunction();">
	<table width="100%" border="0" cellspacing="10" cellpadding="0">
		<tr valign="top">
			<td width="99%">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><font class="con_hdr_3b">People Manager</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
					<br></td>
				</tr>
				<tr>
					<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">

						<tr>
							<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="tabletitle" width="98%"> <%if (peopleType.equalsIgnoreCase("C")) {%>
									Contractor <%} else if (peopleType.equalsIgnoreCase("W")) {%>
									Owner <%} else if (peopleType.equalsIgnoreCase("E")) {%>
									Engineer <%} else if (peopleType.equalsIgnoreCase("R")) {%>
									Architect <%}%></td>
									<td width="1%" class="tablebutton"><nobr>
									<input type="button" value="Back"
										onclick="javascript:history.back();" class="button"> <html:submit
										value="Save" styleClass="button" onclick="savePeople();" />
									&nbsp;</nobr></td>
								</tr>
							</table>
							</td>
						</tr>

						<tr>
							<td background="../images/site_bg_B7C1CB.jpg">
							<table width="100%" border="0" cellspacing="1" cellpadding="2">
								<tr>
									<td class="tablelabel">Type</td>
									<td class="tabletext" > <%if (peopleType.equalsIgnoreCase("C")) {%>
									Contractor <%} else if (peopleType.equalsIgnoreCase("W")) {%>
									Owner <%} else if (peopleType.equalsIgnoreCase("E")) {%>
									Engineer <%} else if (peopleType.equalsIgnoreCase("R")) {%>
									Architect <%}%></td>
								</tr>
								<%if (!peopleType.equalsIgnoreCase("W")) {%>
								<tr>
									<td class="tablelabel">License No.</td>
									<td class="tablelabel"><html:text
										name="peopleForm" property="people.licenseNbr" size="10"
										maxlength="20" styleClass="textbox" /> <html:hidden
										name="peopleForm" property="people.peopleId" /> <html:hidden
										name="peopleForm" property="people.isExpired" /> <html:hidden
										name="peopleForm" property="isExpired" /> <html:hidden
										name="peopleForm" property="people.peopleType.code" /> <html:hidden
										name="peopleForm" property="psaId" /> <html:hidden
										name="peopleForm" property="psaType" /></td>
									<td class="tablelabel">License Expiration Date</td>
									<logic:equal name="peopleForm" property="isExpired" value="Y">
										<td bgcolor="#F48080">
									</logic:equal>
									<logic:notEqual name="peopleForm" property="isExpired"
										value="Y">
										<td class="tabletext">
									</logic:notEqual>
									<nobr><html:text name="peopleForm" property="strLicenseExpires"
										size="10" maxlength="10" styleClass="textbox" /> <html:link
										href="javascript:show_calendar('peopleForm.strLicenseExpires');"
										onmouseover="window.status='Calendar';return true;"
										onmouseout="window.status='';return true;">
										<img src="../images/calendar.gif" width="16" height="15"
											border=0 />
									</html:link></nobr>
									</td>
								</tr>
								<%}%>
								<%if (peopleType.equalsIgnoreCase("C")) {%>
								<tr>
									<td class="tablelabel">Business License No.</td>
									<td class="tabletext"><html:text name="peopleForm"
										property="people.businessLicenseNbr" size="15" maxlength="12"
										styleClass="textbox" />&nbsp;&nbsp;</td>
									<td class="tablelabel">Business License Expiration Date</td>
									<logic:equal name="peopleForm" property="BLExpirationDate"
										value="Y">
										<td bgcolor="#F48080">
									</logic:equal>
									<logic:notEqual name="peopleForm" property="BLExpirationDate"
										value="Y">
										<td class="tabletext">
									</logic:notEqual>
									<nobr><html:text name="peopleForm"
										property="strBusinessLicenseExpires" size="10" maxlength="10"
										styleClass="textbox" /> <html:link
										href="javascript:show_calendar('peopleForm.strBusinessLicenseExpires');"
										onmouseover="window.status='Calendar';return true;"
										onmouseout="window.status='';return true;">
										<img src="../images/calendar.gif" width="16" height="15"
											border=0 />
									</html:link></nobr>
									</td>
								</tr>
								<%}%>
								<tr>
									<td class="tablelabel">Name</td>
									<td class="tabletext"><html:text name="peopleForm"
										property="people.name" size="40" maxlength="45"
										styleClass="textbox" /></td>
									<td class="tablelabel">Agent Name</td>
									<td class="tabletext"><html:text name="peopleForm"
										property="people.agentName" size="25" maxlength="20"
										styleClass="textbox" />&nbsp;</td>
								</tr>
								<tr>
									<td class="tablelabel">Address</td>
									<td class="tabletext"><html:text name="peopleForm"
										property="people.address" size="45" maxlength="60"
										styleClass="textbox" /></td>
									<td class="tablelabel">City, State, Zip</td>
									<td class="tabletext"><html:text name="peopleForm"
										property="people.city" size="25" maxlength="20"
										styleClass="textbox" /> <html:text name="peopleForm"
										property="people.state" size="2" maxlength="2"
										styleClass="textbox" /> <html:text name="peopleForm"
										property="people.zipCode" size="10" maxlength="9"
										styleClass="textbox" /></td>
								</tr>
								<tr>
									<td class="tablelabel">Phone, Ext</td>
									<td class="tabletext"><html:text name="peopleForm"
										property="people.phoneNbr" size="25" maxlength="12"
										styleClass="textbox"
										onkeypress="return DisplayHyphen('people.phoneNbr');" />&nbsp;
									<html:text name="peopleForm" property="people.ext" size="4"
										maxlength="4" styleClass="textbox" /></td>
									<td class="tablelabel">Fax</td>
									<td class="tabletext"><html:text name="peopleForm"
										property="people.faxNbr" size="25" maxlength="12"
										styleClass="textbox"
										onkeypress="return DisplayHyphen('people.faxNbr');" /></td>
								</tr>
								<tr>
									<td class="tablelabel">Email</td>
									<td class="tabletext"><html:text name="peopleForm"
										property="people.emailAddress" size="25" maxlength="40"
										styleClass="textbox" onblur="checkEmail(this);"/></td>
								</tr>
								<logic:present name="actList" scope="request">
									<tr>
										<td class="tablelabel">select Activity Map</td>
										<td class="tabletext"><html:select property="selectedActvity"
											styleClass="textbox" size="5" multiple="multiple">
											<html:options collection="actList" property="actId"
												labelProperty="actName" />
										</html:select></td>
										<td class="tablelabel">Business Tax Required</td>
										<td class="tabletext"><html:checkbox
											property="reqBTax" /></td>
									</tr>
								</logic:present>
							</table>
							</td>
						</tr></table>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
			</td>
			<td width="1%">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td height="32">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
</html:form>
</body>

</html:html>
