<%@page import="elms.agent.OnlineAgent"%>
<%@page import="elms.security.User"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%
 	String contextRoot = request.getContextPath();
	java.util.List streetList = elms.agent.AddressAgent.getStreetArrayList();
	pageContext.setAttribute("streetList", streetList);
	
	User user = (elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY);
	int userId = user.getUserId();
	java.util.List previous = new OnlineAgent().getPrevious(userId);
	pageContext.setAttribute("previous", previous);
	
%>

<html:html>
<html:base/>

<head>
<title>Apply Permit</title>
	<link rel="stylesheet" href="../css/elms.css" type="text/css">
	 <meta http-equiv="X-UA-Compatible" content="IE=edge" >
	
	<script language="javascript" type="text/javascript" src="../../tools/jquery/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" media="all" href="../../tools/chosen/chosen.css"/>
	<script language="javascript" type="text/javascript" src="../../tools/chosen/chosen.jquery.js"></script>
	<script language="javascript" type="text/javascript" src="../../tools/chosen/cms.chosen.js"></script>
	<link rel="stylesheet" type="text/css" media="all" href="../../tools/toggleswitch/css/tinytools.toggleswitch.css"/>
	<script language="javascript" type="text/javascript" src="../../tools/toggleswitch/tinytools.toggleswitch.js"></script>
	<link rel="stylesheet" href="../css/online.css" type="text/css">
	
	<style>
	.abutton {
	    background-color: #eeeeee;
		border: 1px solid #cccccc;
		font-family: Oswald, Arial, Helvetica;
		text-transform: uppercase;
		padding: 10px;
		padding-left: 10px;
		padding-right: 10px;
		margin: 10px;
		font-size: 12px;
		font-weight: bold;
		border-radius: 5px;
		color: #000000;
		cursor: pointer;
		text-decoration:none;
	}
	
	.abutton:hover {
		background-color: #669966;
		color: #ffffff;
	}
	
	table.csui, table.csui_title { width: 100%; padding: 0px; }
	table.csui { background-color: #cccccc; border-spacing: 1px; border-collapse: separate; }
	td.csui { padding: 6px; font-family: Armata, Arial, Helvetica, sans-serif; font-size: 12px; background-color: #ffffff; }

	table.csui_title { border-spacing: 0px; background-color: #777777; }
	td.csui_title { padding: 5px; width: 100%; font-family: Oswald, Arial, Helvetica, sans-serif; font-size: 18px; font-weight: 700; color: #ffffff; vertical-align: top; text-transform: uppercase }
	
	
	a.csui, a.csuisub { color: #000000; text-decoration: none }
	a.csui:hover, a.csuisub:hover { color: #336699; }
	td.csui_header, td.csui_label { padding: 6px; font-family: Roboto Condensed, Arial, Helvetica, sans-serif; font-size: 10px; background-color: #eeeeee; text-transform: uppercase }
	td.csui_label { width: 10%; white-space: nowrap; vertical-align: top; padding: 8px; padding-top: 15px }
	td.csui_input { padding: 6px; font-family: Armata, Arial, Helvetica, sans-serif; font-size: 12px; background-color: #ffffff; width: 40% }
	div.csui_divider { height: 25px; }

	a.csui_title { color: #ffffff; text-decoration: none }
	a.csuisub_title { color: #555555; text-decoration: none }
	a.csuisub_header_title { color: #888888; text-decoration: none }
	</style>
	
	<script language="javascript">
	function mapAddress()
	{
		var checkAddress="yes";
		document.forms[0].action='<%=contextRoot%>/checkAddress.do?checkAddress='+checkAddress;
		document.forms[0].submit();
	
	}
	
	
	function proceedStep(url){
		document.location.href=url;
		
	}
	
	</script>
</head>
<body text="#000000" leftmargin="5" bottommargin="0"  scroll="yes" topmargin="0" marginwidth="0" marginheight="40" rightmargin="0">

<center>

<html:form action="/checkAddress">
<table align="center" cellpadding="0" cellspacing="0" style="height:100%"  width="100%">

	<td align="center" valign="top" width="99%">
	<br><br> <br><br>

		<fieldset style="width: 450px;height: 320px">
		<br><br>
		<table cellpadding="15" cellspacing="0" width="100%">
 		<html:errors locale="error.address.mismatch"/>

   <tr>
        <td align="left"> <font class="panelVisited" style="font-size:30px">1<font class="panelVisited" style="font-size:12px"><bean:message key="addressInfo"/>
        </td>
   </tr>


	    <tr>
	        <td align="center">
	            <table cellpadding="4" cellspacing="0">
	                <tr>
		          <td colspan="3">
		            <font class="panelVisited" style="font-size:12px">&nbsp;&nbsp;&nbsp;Street #

		          <td colspan="2">
		            <font class="panelVisited" style="font-size:12px">Street Name</td>

		          <td colspan="3">
		            <font class="panelVisited" style="font-size:12px">Unit</td>
	        </tr>
	        <tr>
	          <td>

	           <td bgcolor="#FFFFFF"><div class="textcss"><html:text size="5" property="streetNumber" />
	           </div>
	            </td>
	         	 <td bgcolor="#FFFFFF">		<html:select property="streetFraction" styleClass="combobox" style="width:80px;">  <html:option value=""></html:option> <html:option value="1/4">1/4</html:option> <html:option value="1/2">1/2</html:option> <html:option value="3/4">3/4</html:option> </html:select>
				
	            </td>

	         <td colspan=2>


			<html:select property="streetName" styleClass="combobox">
				 <html:option value="-1">Please Select</html:option>
				 <html:options collection="streetList" property="streetId" labelProperty="streetName"/>
			  </html:select>
			  </td>
			 <td ><div class="textcss">	<html:text size="10" property="unitNumber"  /></div>

			  </td>


	        </tr>


	            </table>
	        </td>

	    </tr>

	            <tr>

	                    <td class="FireSelect">
	                    <div align="center">
	                      <html:button  property="button" value="Next" styleClass="csui_button" style="width: 100px" onclick="mapAddress()"/>
	                      </div>
	                </tr>


	</table>
	</fieldset>
	</center>
	</html:form>
	
	<%if(previous.size()>0){ %>
		
		
		<table class="csui_title" style="width:50%" >
		</br>
		</br>
		</br>
		</br>
		<tr>
				<td class="csui_title">PREVIOUS</td>
				
			</tr>	
		</table>
	
		
		
		
		<table class="csui" style="width:50%" >
			
			<tr>
				<td  class="csui_header">Address</td>
				
				<td  class="csui_header">Created</td>
				
				<td  class="csui_header">Proceed</td>
			</tr>		
			<%for(int i=0;i<previous.size();i++){
				elms.control.beans.online.ApplyOnlinePermitForm a = (elms.control.beans.online.ApplyOnlinePermitForm) previous.get(i);
				
				
				%>
			<tr>
				<td class="csui"><%=a.getAddress() %></td>
				
				<td class="csui"><%=a.getCreated() %></td>
				
				<td class="csui" nowrap="nowrap"><input type="button" name="Proceed" value="Continue" onclick="proceedStep('<%=request.getContextPath() %><%=a.getUrl() %>');"> </td>
			</tr>		
	
	
			<%} %>
		</table>
	
	<%} %>
	
	
	
		</td>

<!--Display of current entered data for permits-->

<td valign="top" width="1%" nowrap align="right">

 	<table style="width:200px; height:100%" cellpadding="0" cellspacing="0" bgcolor="#9FADBA" background="images/panelback.png" align="right">


    <tr>
        <td valign="top">


            <table style="width:200px" cellpadding="0" cellspacing="0" >

                <tr>
                    <td width="8" class="panelActiveColor"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td class="panelActive" style="padding:8px">
                        <font style="font-size:18px">1</font> - ADDRESS<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="images/bullet.gif" height="9" width="9"></td>
                                <td class="panelVisited"></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="8" class="panelActiveColor"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td bgcolor="#FFFFFF"><img src="space.gif" width="0" height="1"></td>
                </tr>


                <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td class="panelActive" style="padding:8px">
                        <font style="font-size:18px">2</font> - STRUCTURE<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="images/bullet.gif" height="9" width="9"></td>
                                <td class="panelVisited"></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td bgcolor="#FFFFFF"><img src="space.gif" width="0" height="1"></td>
                </tr>



                <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td class="panelActive" style="padding:8px">
                        <font style="font-size:18px">3</font> - PERMIT TYPE<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="images/bullet.gif" height="9" width="9"></td>
                                <td class="panelActive"></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td bgcolor="#FFFFFF"><img src="space.gif" width="0" height="1"></td>
                </tr>

				 <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td class="panelActive" style="padding:8px">
                        <font style="font-size:18px">4</font> - VALUATION<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="images/bullet.gif" height="9" width="9"></td>
                                <td class="panelActive"></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td bgcolor="#FFFFFF"><img src="space.gif" width="0" height="1"></td>
                </tr>


				 <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td class="panelActive" style="padding:8px">
                        <font style="font-size:18px">5</font> - CONTRACTOR<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="images/bullet.gif" height="9" width="9"></td>
                                <td class="panelActive"></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td bgcolor="#FFFFFF"><img src="space.gif" width="0" height="1"></td>
                </tr>


				 <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td class="panelActive" style="padding:8px">
                        <font style="font-size:18px">6</font> - OWNER<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="images/bullet.gif" height="9" width="9"></td>
                                <td class="panelActive"></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td bgcolor="#FFFFFF"><img src="space.gif" width="0" height="1"></td>
                </tr>



                 <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td class="panelActive" style="padding:8px">
                        <font style="font-size:18px">7</font> - PLAN CHECK<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="images/bullet.gif" height="9" width="9"></td>
                                <td class="panelActive"></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td bgcolor="#FFFFFF"><img src="space.gif" width="0" height="1"></td>
                </tr>

                <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td class="panelActive" style="padding:8px">
                        <font style="font-size:18px">8</font> - COMPLETE / PAY<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="images/bullet.gif" height="9" width="9"></td>
                                <td class="panelActive"></td>
                            </tr>
                        </table>
                    </td>
                </tr>

 				 <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td bgcolor="#FFFFFF"><img src="space.gif" width="0" height="1"></td>
                </tr>

            </table>
        </td>
    </tr>

</table>
</body>
 </td>
 </table>
 	<!--End Display of current entered data for permits-->

</html:html>

