<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="elms.agent.ActivityAgent"%>
<%@page import="elms.util.StringUtils"%>

<%@page import="elms.app.people.People"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>


<html:html>
<head>
<html:base />
<title>City of Burbank : Online Business Center : Plan Check Manager</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link rel="stylesheet" href="../css/elms.css" type="text/css">
<link rel="stylesheet" href="../css/CalendarControl.css" type="text/css">
<script language="JavaScript" src="../script/CalendarControl.js"></script>
</head>
<%
String contextRoot = request.getContextPath();
String activityId =   (String)request.getAttribute("activityId");

String lncvList = "0";
lncvList = (String) (request.getAttribute("lncvList"));

List lncvCurrentList = new ArrayList();
lncvCurrentList = (List) (request.getAttribute("lncvCurrentList"));

List lncvPreviousList = new ArrayList();
lncvPreviousList = (List) (request.getAttribute("lncvPreviousList"));

int lncvUsed = 0;
lncvUsed = StringUtils.s2i(lncvList);
int lncvAvail = 0;
lncvAvail = 96 - lncvUsed;

String printUrl =  ActivityAgent.getLNCVPrintUrl();

%>
<script>
function editDt(dt){
	if(dt==undefined){
		//document.getElementById('edit').style.display= 'none';
	}else {
		document.getElementById('view').style.display= 'none';
		document.getElementById('edit').style.display= '';
		document.getElementById('lncvDate').value= dt;
		document.getElementById('oldlncvDate').value= dt;
	}

}
function updateDt(dt){
	document.forms[0].action="<%=contextRoot%>/listLncvActivity.do?action=update&activityId="+<%=activityId%>;
    document.forms[0].submit();

}

function printlncv(){
	
	var printval="";
	
	
	if(document.forms[0].printDt.length != undefined){
		for(var i=0;i<document.forms[0].printDt.length;i++){
			if(document.forms[0].printDt[i].checked == true){
				printval += document.forms[0].printDt[i].value +",";
			}
		}
	}else {
		if(document.forms[0].printDt.checked == true){
			printval += document.forms[0].printDt.value +",";
		}
	}	
		
	if(printval != ''){
		document.getElementById('printval').value= printval;
		window.open( "<%=printUrl%><%=activityId%>,"+printval+"",'_blank');


	}else {
		alert("Please select the blocks to be printed.");
		return false;
	}



}

</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="editDt();">
<html:form action="/listLncvActivity" >
<input type="hidden" name="oldlncvDate" />
<input type="hidden" name="printval" />
<%
	String error = (String)request.getAttribute("error");
	if(error==null) error="";
	String message = (String)request.getAttribute("message");
	if(message==null) message="";
%>
<font class="con_text_red1"><%=error%>
<font class="green2b"><%=message%>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <TBODY><tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <TBODY><tr>
            <td><font class="con_hdr_3b">LNCV &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <TBODY><tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" background="../images/site_bg_B7C1CB.jpg">LNCV
                         List -Used (<%=lncvUsed%>) &nbsp; Avail(<%=lncvAvail%>)</td>
                        <td width="1%" background="../images/site_bg_B7C1CB.jpg" nowrap><a href="javascript:history.back(0);" >Back</a> &nbsp;&nbsp; <a href="javascript:void(0);" onclick="printlncv();" >Print LNCV</a></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr id="view">
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2" border="0">
                     <tr>
                      <td class="tablelabel" colspan="2"> Select the dates to be printed.</td>
                   	 </tr>
                   	 
				   <% 
		           Set s = new HashSet(); 
		           for(int i=0 ;i< lncvCurrentList.size();i++ ) {
			           String pblock = (((People)lncvCurrentList.get(i)).getTitle());
			           String lncvdt = (((People)lncvCurrentList.get(i)).getDate());
			           String blockdt = (((People)lncvCurrentList.get(i)).getALDate());
			           if(!s.contains(pblock)){
		        	     
		           %>
		          
		          		 <tr>
			                <td style="background: #d1d1d1;font-family: Arial, Helvetica;font-size: 12px;color:#000000; text-decoration: none"  align="left">
			                  	 <input type="checkbox" name="printDt" id="printDt" value="<%=blockdt%>"> 
			                Permit Block 
			                </td>
			           	 </tr>
		          	   <% 	s.add(pblock);	  }  %>
		                <tr>
				            <td style="background: #FFFFFF;font-family: Arial, Helvetica;font-size: 11px;color:#000000; text-decoration: none; padding-left: 30px; " >
				             <%=lncvdt%>
				           </td>
				         </tr>
			         
		            <% }%>
		            
		             <tr>
                      <td class="tablelabel" colspan="2"> &nbsp;</td>
                   	 </tr>
		             <% 
		           Set sp = new HashSet(); 
		           for(int i=0 ;i< lncvPreviousList.size();i++ ) {
			           String pblock = (((People)lncvPreviousList.get(i)).getTitle());
			           String lncvdt = (((People)lncvPreviousList.get(i)).getDate());
			          // String blockdt = (((People)lncvPreviousList.get(i)).getALDate());
			           if(!sp.contains(pblock)){
		        	     
		           %>
		          
		          		 <tr>
			                <td style="background: #d1d1d1;font-family: Arial, Helvetica;font-size: 12px;color:#000000; text-decoration: none"  align="left">
			                  	
			                Permit Block 
			                </td>
			           	 </tr>
		          	   <% 	sp.add(pblock);	  }  %>
		                <tr>
				            <td style="background: #FFFFFF;font-family: Arial, Helvetica;font-size: 11px;color:#000000; text-decoration: none; padding-left: 30px; " >
				             <%=lncvdt%>
				           </td>
				         </tr>
			         
		            <% }%>
		          
                 
                </td>
              </tr>



            </TBODY></table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </TBODY></table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <TBODY><tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </TBODY></table>
    </td>
  </tr>
</TBODY></table>
</html:form>
</body>
</html:html>
