<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="javax.sql.*,sun.jdbc.rowset.*,java.util.*" %>
<%@ page import="elms.app.finance.*" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>

<%
 	String contextRoot = request.getContextPath();
    String contextHttpsRoot = (String)request.getAttribute("contextHttpsRoot");
	String comboNo= (String)request.getAttribute("comboNo");
    String comboName = (String)request.getAttribute("comboName");
    String levelId= (String)request.getAttribute("sprojId");



%>
<style type="text/css">
    .FireTitle          { FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  td.FireHeading        { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none; PADDING: 10px; BACKGROUND-COLOR:#D9D9D9 }
    .FireHeading        { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireGlance         { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  td.FireResource       { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none; PADDING: 6px }
    .FireResource       { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
  td.FireResourceStatic { FONT-SIZE: 12px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none; PADDING: 6px }
    .FireResourceStatic { FONT-SIZE: 12px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireSelect         { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireText           { FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireHighlight      { FONT-SIZE: 15px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #990000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireMessage        { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
  td.FireMenu           { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none; PADDING: 2px; BACKGROUND-COLOR:#000000 }
    .FireMenu           { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:link      { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:visited   { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:active    { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:hover     { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireMenu1          { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:link     { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:visited  { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:active   { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:hover    { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #FFFFFF; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  @page Section1 {
                   size:8.5in 11.0in;
                   margin:0.5in 0.5in 0.5in 0.5in;
                 }
 p.MsoNormal, li.MsoNormal, div.MsoNormal {
                margin:0in;
                margin-bottom:.0001pt;
                font-size:12.0pt;
                font-family:"Arial"
              }
 table.atable {
                border-collapse:collapse;
                border:1px solid #B3B3B3
              }
 tr           {
                page-break-inside:avoid
              }
 div.Section1 {
                page:Section1;
              }


.panelVisited      { font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #000000 }
.panelVisitedColor { background-color:#62A85F }
.panelActive       { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #FFFFFF }
.panelActiveColor  { background-color:#EAC563 }
.panel             { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #FFFFFF }
.panelColor        { background-color:#BB2F2F }

.panelVisited:link      { font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #003265;  TEXT-DECORATION: none }
.panelVisited:visited      { font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #003265;  TEXT-DECORATION: none }
.panelVisited:active      { font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #003265;  TEXT-DECORATION: none }
.panelVisited:hover      { font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #003265;  TEXT-DECORATION: none }

</style>
<html:html>
<html:base/>
<head>

<title>City of Burbank :Permitting and Licensing System: Fees</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
	<link rel="stylesheet" href="../css/online.css" type="text/css">

</head>

<script language="JavaScript" src="../script/formValidations.js"></script>
<script language="JavaScript">

function openTerms()
{
window.open("<%=contextRoot%>/jsp/online/termsAndConditions.jsp?contextHttpsRoot=<%=contextHttpsRoot%>",target="_new","toolbar=no,width=1000,height=600,screenX=100,left=100,screenY=100,top=1,location=no,status=no,menubar=no,scrollbars=no");
}
function openPrivacy()
{
window.open("<%=contextRoot%>/jsp/online/privacyPolicy.jsp?contextHttpsRoot=<%=contextHttpsRoot%>",target="_new","toolbar=no,width=1000,height=800,screenX=100,left=100,screenY=100,top=1,location=no,status=no,menubar=no,scrollbars=no");
}
function openRefund()
{
window.open("<%=contextRoot%>/jsp/online/refundPolicy.jsp?contextHttpsRoot=<%=contextHttpsRoot%>",target="_new","toolbar=no,width=1000,height=800,screenX=100,left=100,screenY=100,top=1,location=no,status=no,menubar=no,scrollbars=no");
}

function goHome(){
	alert("Your Permit is created successfully");
	document.forms[0].action='<%=contextRoot%>/jsp/online/welcomeOnline.jsp';
	document.forms[0].submit();

}

</SCRIPT>

<html:errors/>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form name="paymentMgrForm" type="elms.control.beans.PaymentMgrForm" action="/confirmPaymentDetails">


<table align="center" cellpadding="0" cellspacing="0" style="height:100%" width="100%">
  <tr>
    <td align="center">
      <fieldset style="width: 450px;height: 320px">
        <legend class="FireResourceStatic">
          <table cellpadding="5" cellspacing="0">
            <tr>
              <td><img src="<%=contextRoot%>/jsp/online/images/Shield.gif" width="100" height="100"></td>
              <td class="panelVisited" style="color: #003265; font-size:16px; font-weight: bold">PERMIT PROCESSED</td>
            </tr>
          </table>
        </legend>

        <center>
        <table cellpadding="5" cellspacing="0">
          <html:errors />

          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td nowrap><font class="panelVisited">Permit No:
           <font class="panelVisited"> <%=comboNo%></td>
          </tr>
          <tr>
            <td nowrap><font class="panelVisited">Permit Type:
            <font class="panelVisited"> <%=comboName%>
          </tr>

          <tr>
			<td ><font class="panelVisited"><font color="red" style="font-family: Arial; font-size:12px">
				   Your Permit # has been processed. Kindly Contact City of Burbank.
				    </td>
				</tr>




       	</table>
       	<table cellpadding="5" cellspacing="0">
            <tr>
              <td class="FireSelect" >
                <div align="center">

                   <html:button  property="button" value="Ok"  styleClass="FireSelect" style="width: 100px" onclick="goHome()"/>
                </div>
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>

          </table>

        </center>
      </fieldset>


      <table align="center" cellpadding="0" cellspacing="0" width="100%">
        <tr>
          <td align="center">
            <a class="panelVisited" style="font-size:11px" href="<%=contextRoot%>/jsp/online/welcomeOnline.jsp";>Home</a>
            <font class="panelVisited">&nbsp; | &nbsp;<a href="#" onclick="openTerms();" class="panelVisited">Terms & Conditions</a>
            <font class="panelVisited">&nbsp; | &nbsp;<a href="#" onclick="openRefund();" class="panelVisited">Refund Policy</a>
            <font class="panelVisited">&nbsp; | &nbsp;<a href="#" onclick="openPrivacy();" class="panelVisited">Privacy Policy</a>
            <font class="panelVisited">&nbsp;&nbsp;&nbsp; <a class="panelVisited" href="http://www.ci.burbank.ca.us/" style="color:#000000" target="_blank">� City of Burbank</a>
          </td>
        </tr>
      </table>

    </td>
  </tr>
</table>


</center>


</html:form>
</body>
</html:html>
