<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<app:checkLogon/>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%
   String contextRoot = request.getContextPath();
   String addEditFlag = (String) request.getAttribute("addEditFlag");
   String peopleId = (String) request.getAttribute("peopleId");

   	String lsoAddress = (String)session.getAttribute("lsoAddress");
	if (lsoAddress == null ) lsoAddress = "";

	String licenseNbr =(String) request.getAttribute("licenseNbr");
	if(licenseNbr==null) licenseNbr = "-1";

	String psaId = (String)request.getAttribute("psaId");
	if(psaId ==null) psaId ="";
	
	String vehicle =(String) request.getAttribute("vehicle");
	if(vehicle==null) vehicle = "N";

	String psaType = (String)request.getAttribute("psaType");
	if(psaType ==null) psaType = "";

	String  fromOnline =(String)session.getAttribute("fromOnline");
	if(fromOnline==null) fromOnline="";

    java.util.List peopleTypeList = (java.util.List) request.getAttribute("peopleTypeList");

     int[] alpaha = (int [])request.getAttribute("typeofpeople");

     String  peopleTypeName =(String)request.getAttribute("peopleTypeName");
     if(peopleTypeName==null) peopleTypeName="People";


	String message = (String)request.getAttribute("message");
	if(message==null) message="";

	String error = (String)request.getAttribute("error");
	if(error==null) error="";

	String isObc = ((elms.security.User)session.getAttribute(elms.common.Constants.USER_KEY)).getIsObc();
	String isDot = ((elms.security.User)session.getAttribute(elms.common.Constants.USER_KEY)).getIsDot();
	String  peopleTypes =(String)request.getAttribute("peopleTypes")!=null?(String)request.getAttribute("peopleTypes"):"";
%>
<script language="JavaScript" src="../script/calendar.js"></script>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<script language="JavaScript">
function validateEmail()
{
var email= document.forms['onlineRegisterFrom'].elements['emailAddress'].value;

//  a very simple email validation checking.
//  you can add more complex email checking if it helps
    var splitted = email.match("^(.+)@(.+)$");    if(splitted == null) return false;
    if(splitted[1] != null )
    {
      var regexp_user=/^\"?[\w-_\.]*\"?$/;
      if(splitted[1].match(regexp_user) == null) return false;
    }
    if(splitted[2] != null)
    {
      var regexp_domain=/^[\w-\.]*\.[A-Za-z]{2,4}$/;
      if(splitted[2].match(regexp_domain) == null)
      {
	    var regexp_ip =/^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
	    if(splitted[2].match(regexp_ip) == null) return false;
      }// if

      return true;
    }

return false;
}
function check()
{

var carCode= event.keyCode;
if ((carCode < 48) || (carCode > 57)){


event.returnValue = false;
}

}
function DisplayHyphen(str)
{

	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46))
	{
		event.returnValue = false;
	}
	else
	{
		if ((document.onlineRegisterFrom.elements[str].value.length == 3 ) || (document.onlineRegisterFrom.elements[str].value.length == 7 ))
		{
			document.onlineRegisterFrom.elements[str].value = document.onlineRegisterFrom.elements[str].value +'-';
 		}
 		if (document.onlineRegisterFrom.elements[str].value.length > 11 )  event.returnValue = false;
	}
}
var strValue;
function validateFunction()
{
	
	var strValue=true;
	
   if (strValue==true){
	   
	      if (document.forms['onlineRegisterFrom'].elements['emailAddress'].value == '') {
	    	alert('Email address is a required field');
	    	 //	alert(validateEmail(document.forms['onlineRegisterFrom'].elements['emailAddress'].value));
	    	document.forms['onlineRegisterFrom'].elements['emailAddress'].focus();
	    	strValue=false;
	    	return false;
	      }
	   }

	if (strValue==true){
		if(!validateEmail(document.forms['onlineRegisterFrom'].elements['emailAddress'].value)) {
	          strValue=false;
	        alert('Please enter valid Email address');
	        document.forms['onlineRegisterFrom'].elements['emailAddress'].focus();
	        return false;
	    }
	}
	
	
	   if (strValue==true){
		      if (document.forms['onlineRegisterFrom'].elements['firstName'].value == '') {
		    	alert('First Name  is a required field');
		    	 //	alert(validateEmail(document.forms['onlineRegisterFrom'].elements['emailAddress'].value));
		    	document.forms['onlineRegisterFrom'].elements['firstName'].focus();
		    	strValue=false;
		    	return false;
		      }
		   }
	   
	   if (strValue==true){
		   
		      if (document.forms['onlineRegisterFrom'].elements['lastName'].value == '') {
		    	alert('Last Name is a required field');
		    	 //	alert(validateEmail(document.forms['onlineRegisterFrom'].elements['emailAddress'].value));
		    	document.forms['onlineRegisterFrom'].elements['lastName'].focus();
		    	strValue=false;
		    	return false;
		      }
		   }
		
   if (strValue==true){
      if (document.forms['onlineRegisterFrom'].elements['phoneNbr'].value == '') {
    	alert('Phone # is a required field');
    	document.forms['onlineRegisterFrom'].elements['phoneNbr'].focus();
    	strValue=false;
    	return false;
      }
   }
   


	var peopleTyp = "";
/* 	if(document.forms[0].contractor.checked == true)
		peopleTyp= peopleTyp + "4,";
	if(document.forms[0].ownerBuilder.checked == true)
		peopleTyp= peopleTyp + "8,";
	
 		peopleTyp= peopleTyp + "2";


		document.forms[0].peopleType.value = peopleTyp; */

		document.forms[0].action="<%=contextRoot%>/editProfile.do?update=yes";
		//document.forms[0].submit();
		return true;
}

function fillDetails(){
var peopleTyp = '<%=peopleTypes%>';
document.getElementById("contTable").style.display = "none";
var people_array = new Array();
	people_array = peopleTyp.split(",");
	for(var i=0;i<people_array.length;i++){
		if(people_array[i]==4){
			document.forms[0].contractor.checked = true;
		}
		if(people_array[i]==8){
			document.forms[0].ownerBuilder.checked = true;
		}
	}

}


function showContTable() {
	if(document.forms[0].contractor.checked == true){
          document.getElementById("contTable").style.display = "none";
	}else{
          document.getElementById("contTable").style.display = "none";
	}
}
</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="fillDetails();showContTable();" ><!-- viewDetails();fillDetails(); showAcct()" -->
<html:form action="/editProfile" onsubmit="validateFunction();">
<html:hidden property="elms" value="<%=isObc%>"/>
<html:hidden property="dot" value="<%=isDot%>"/>
<html:hidden property="peopleType"/>
<%-- <html:hidden property="emailAddress"/> --%>
<html:hidden property="obc" value="<%=isObc%>"/>

<center>
        <table width="90%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
              <font class="green2b"><%=message%></font><br>
              <font class="red2b"><html:errors /><br><%=error%></font>
              <%   if(!error.equals("")) { %>
                <a  href=javascript:parent.location.href='<%=contextRoot%>/gateway.do?param=logout';><font class="red2b">Click here to Logout&nbsp;&nbsp;<br></a>
              <% }   %>

              <br>
            </td>
          </tr>
          <tr>
            <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td class="tabletitle" width="98%">
                        Edit Profile </td>

                        <td width="1%" class="tablebutton"><html:submit property="Update" value="  Update  Profile  " styleClass="button" /></td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td background="../images/site_bg_B7C1CB.jpg">
                    <table width="100%" border="0" cellspacing="1" cellpadding="2">
					  <tr>
						<!-- <td class="tablelabel">Contractor Type</td>
						<td class="tabletext" >-->
						 <!--  <input type="checkbox" name="applicant" disabled> Applicant  &nbsp;&nbsp;&nbsp; --> 
						 <!--   <input type="checkbox" name="contractor" onclick="showContTable()"> <!-- Contractor  &nbsp;&nbsp;&nbsp; -->  
						 <!-- <input type="checkbox" name="ownerBuilder"> Owner-Builder -->
						 <!-- </td> -->
						 <input type="hidden" name="contractor" value="true" />
                         <td class="tablelabel">Email<font color="red">*</font></td>
                          <td  class="tabletext" colspan="4">
                            <html:text readonly="true" name = "onlineRegisterFrom" property="emailAddress" size="45" maxlength="60" styleClass="textbox"/>
                        </td>

				      </tr>
                      <tr>
                        <td class="tablelabel">First Name<font color="red">*</font></td>
                        <td  class="tabletext">
                            <html:text  name = "onlineRegisterFrom"   property="firstName" size="45" maxlength="60" styleClass="textbox"/>
                        </td>
                        <td class="tablelabel">Last Name<font color="red">*</font></td>
                        <td  class="tabletext">
                            <html:text  name = "onlineRegisterFrom"   property="lastName" size="45" maxlength="60" styleClass="textbox"/>
                        </td>
                      </tr>
                      <tr>
                        <td class="tablelabel">Company</td>
                        <td class="tabletext" colspan="3">
                        	<html:text  name = "onlineRegisterFrom"   property="companyName" size="45" maxlength="60" styleClass="textbox"/>
                        </td>
                      </tr>
                       <tr>
                        <td class="tablelabel">Address</td>
                        <td class="tabletext">
                          <html:text  name = "onlineRegisterFrom"   property="address" size="45" maxlength="60" styleClass="textbox"/>
                        </td>
                         <td class="tablelabel">City, State, Zip</td>
                        <td class="tabletext">
                          <html:text  name ="onlineRegisterFrom" property="city" size="25"  styleClass="textbox" /><html:text  name ="onlineRegisterFrom" property="state" size="5"  maxlength="2" styleClass="textbox" /> &nbsp;&nbsp;
                            <html:text  name ="onlineRegisterFrom" property="zip" size="15" maxlength="9" styleClass="textbox"  onkeypress="check();"/>
                        </td>
                        </tr>

                      <tr>
                        <td class="tablelabel">Phone<font color="red">*</font></td>
                        <td class="tabletext">
                          <html:text  name = "onlineRegisterFrom"   property="phoneNbr" size="25" maxlength="12" styleClass="textbox"  onkeypress="return DisplayHyphen('phoneNbr');" />&nbsp;
                          Ext&nbsp;&nbsp;<html:text  name = "onlineRegisterFrom"  property="phoneExt" size="5" styleClass="textbox" maxlength="4"/>
                        </td>
                          <td class="tablelabel">Work Phone</td>
                        <td class="tabletext">
                          <html:text  name = "onlineRegisterFrom"   property="workPhone" size="25" maxlength="12" styleClass="textbox"  onkeypress="return DisplayHyphen('workPhone');" />&nbsp;
                          Ext&nbsp;&nbsp;<html:text  name = "onlineRegisterFrom"  property="workExt" size="5" styleClass="textbox" maxlength="4"/>
                        </td>
                      </tr>
                      
                       <tr>
                        <td class="tablelabel">Fax</td>
                        <td class="tabletext">
                           <html:text  name = "onlineRegisterFrom"   property="fax" size="25" maxlength="12" styleClass="textbox"  onkeypress="return DisplayHyphen('fax');" />&nbsp;
                        </td>
                         <td class="tabletext" colspan="4">
                        </td>
                        </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>


<br><br>

     <table border="0" cellspacing="0" cellpadding="0" width="100%">
      <tr>
	<td colspan="3">
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	    <tr>
	      <td background="../images/site_bg_B7C1CB.jpg" width="100%" class="tabletitle">
	      For LNCV Parking Only </td>

            </tr>
			<tr>
                  <td background="../images/site_bg_B7C1CB.jpg">
                    <table width="100%" border="0" cellspacing="1" cellpadding="2">
                       <tr>
	                        <td class="tablelabel">Driver's License Number</td>
	                        <td class="tabletext">
	                          <html:text  name = "onlineRegisterFrom"   property="dlNo" size="25"  styleClass="textbox"  />&nbsp;
	                        </td>
	                          <td class="tablelabel">Vehicle License Plate <br>(or last 4 digits of VIN)</td>
	                        <td class="tabletext">
	                      	<%if(vehicle.equals("N")){ %>	
	                         <bean:write property="vehicleNo" name = "onlineRegisterFrom" />
	                          <html:hidden property="vehicleNo" name = "onlineRegisterFrom"  />
	                      <% } else { %>
	                     	 <html:text  name = "onlineRegisterFrom"   property="vehicleNo" size="25"  styleClass="textbox"  style="text-transform: uppercase" />&nbsp;
	                      <% } %>
	                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
	  </table>
	</td>
      </tr>

     </table>


<span id="contTable">
     <table border="0" cellspacing="0" cellpadding="0" width="100%">
      <tr>
	<td colspan="3">
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	    <tr>
	      <td background="../images/site_bg_B7C1CB.jpg" width="100%" class="tabletitle">
	      Contractor Information </td>

            </tr>
			<tr>
                  <td background="../images/site_bg_B7C1CB.jpg">
                    <table width="100%" border="0" cellspacing="1" cellpadding="2">
					  <tr>
                        <td class="tablelabel">License No.</td>
                        <td  class="tabletext"><font class="con_text_1_ni">
                            <html:text  name = "onlineRegisterFrom"   property="licenseNbr" size="15" maxlength="12" styleClass="textbox"/>

                        </td>
                        <td class="tablelabel">License Expiration Date</td>
                        <td class="tabletext"><font class="con_text_1_ni">
							<nobr><html:text  name = "onlineRegisterFrom"   property="licExpDate" size="10" maxlength="10" styleClass="textbox" /> &nbsp;&nbsp;<html:link href="javascript:show_calendar('onlineRegisterFrom.licExpDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;">
	                        <img src="../images/calendar.gif" width="16" height="15" border=0/></html:link></nobr>
                        </td>
                      </tr>
                      <tr>
                        <td class="tablelabel">Workers Comp Expiration Date</td>
<!--                         <td  class="tabletext"><font class="con_text_1_ni"> -->
<%--                            <nobr><html:text  name = "onlineRegisterFrom"   property="strWorkersCompExpires" size="10" maxlength="10" styleClass="textbox" /> &nbsp;&nbsp;<html:link href="javascript:show_calendar('onlineRegisterFrom.strWorkersCompExpires');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;"> --%>
<%-- 	                       <img src="../images/calendar.gif" width="16" height="15" border=0/></html:link></nobr> --%>
<!--                         </td> -->
                        <td class="tabletext"><font class="con_text_1_ni">
							<nobr><html:text  name = "onlineRegisterFrom"   property="strWorkersCompExpires" size="10" maxlength="10" styleClass="textbox" /> &nbsp;&nbsp;<html:link href="javascript:show_calendar('onlineRegisterFrom.strWorkersCompExpires');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;">
	                        <img src="../images/calendar.gif" width="16" height="15" border=0/></html:link></nobr>
                        </td>
                        <td class="tablelabel">Workers Comp Waiver Flag</td>
                      	<td class="tabletext">Yes
                          <html:radio name = "onlineRegisterFrom" property="workersCompensationWaive" value="Y"  />
                        &nbsp;&nbsp;No
                          <html:radio name = "onlineRegisterFrom" property="workersCompensationWaive" value="N" />
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
	 	</table>
		</td>
      	</tr>
	</table>
</span>
</td>
</tr>
</table>
</center>
</html:form>
</body>
</html:html>