<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="org.owasp.encoder.Encode"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ page import="elms.security.*,elms.common.*"%>
<%
session.setAttribute("fromOnline",new String("fromOnline"));
String appId = request.getParameter("appId");
%>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
<%
    String contextRoot = request.getContextPath();

	java.util.List peopleTypeList = elms.agent.LookupAgent.getPeopleTypes();
  	pageContext.setAttribute("peopleTypeList", peopleTypeList);

	String error = (String)request.getAttribute("error");
	if(error==null) error="";

%>
<script language="JavaScript" src="../script/calendar.js"></script>
<script language="JavaScript" src="../script/formValidations.js"></script>
<SCRIPT language="JavaScript">

function validateFunction()
{
	var chked=0



	if(document.forms[0].contractor[0].checked == true || document.forms[0].contractor[1].checked == true){
	         chked++;
	}
	if(document.forms[0].owner[0].checked == true || document.forms[0].owner[1].checked == true){
		chked++;
     }
	 if(chked != 2){
		alert("Please choose Yes or No for both the options listed.");
		return false;
	}
	 //else if(document.forms[0].contractor[0].checked == true){
		//if(document.forms[0].licenseNbr.value==""){
		//	alert("Please enter a License Number");
			//document.forms[0].licenseNbr.focus();
			//return false;
		//}else if(document.forms[0].licExpDate.value==""){
			//alert("Please enter a License Expiration Date");
			//document.forms[0].licExpDate.focus();
			//return false;
		//}else if(document.forms[0].licenseNbr.value!="" && ((document.forms[0].licenseNbr.value.length < 6) || (document.forms[0].licenseNbr.value.length > 9))){
			//alert("Please enter your correct License Number.");
			//document.forms[0].licenseNbr.focus();
			//return false;
		//}
	//}
	document.forms[0].obc.value = "Y";

	if(document.forms[0].accountNo.value != ""){
		document.forms[0].dot.value = "Y";
	}else{
		document.forms[0].dot.value = "N";
	}

	var peopleTyp = "";
	if(document.forms[0].contractor[0].checked == true)
		peopleTyp= peopleTyp + "4,";
	if(document.forms[0].owner[0].checked == true)
		peopleTyp= peopleTyp + "8,";
		peopleTyp= peopleTyp + "2";

		document.forms[0].peopleType.value = peopleTyp;

	document.forms[0].action = "<%=contextRoot%>/onlineRegisterOtherDetails.do?appId=<%=appId%>";
	document.forms[0].submit();
}

function hideAccount(){
	document.getElementById("acctSub").style.display = "none";
	document.onlineRegisterFrom.accountNo.value = "";
}

function viewAccount(){
	if(document.forms[0].dotaccount[0].checked == true){
		document.getElementById("acctSub").style.display = "block";
	}else{
		document.onlineRegisterFrom.accountNo.value = "";
		document.getElementById("acctSub").style.display = "none";
	}
}

function viewDetails(){
	if(document.forms[0].contractor[0].checked == true){
		document.getElementById("menuSub").style.display = "none";
	}else{
		document.getElementById("menuSub").style.display = "none";
	}
}


function hideContractor(){
	document.getElementById("menuSub").style.display = "none";
	document.onlineRegisterFrom.accountNo.value = "";
}

function validateDate()
{
	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 47))
	event.returnValue = false;
}

function check(){

	var carCode= event.keyCode;
	 if ((carCode < 48) || (carCode > 57)){
	          event.returnValue = false;
	  }

	}

</SCRIPT>

</head>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="hideContractor(); hideAccount()">
<center>
<style type="text/css">
    .FireTitle          { FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  td.FireHeading        { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none; PADDING: 10px; BACKGROUND-COLOR:#D9D9D9 }
    .FireHeading        { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireGlance         { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  td.FireResource       { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none; PADDING: 6px }
    .FireResource       { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
  td.FireResourceStatic { FONT-SIZE: 12px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none; PADDING: 6px }
    .FireResourceStatic { FONT-SIZE: 12px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireSelect         { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireText           { FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireHighlight      { FONT-SIZE: 15px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #990000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireMessage        { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
  td.FireMenu           { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none; PADDING: 2px; BACKGROUND-COLOR:#000000 }
    .FireMenu           { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:link      { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:visited   { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:active    { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:hover     { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireMenu1          { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:link     { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:visited  { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:active   { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:hover    { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #FFFFFF; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  @page Section1 {
                   size:8.5in 11.0in;
                   margin:0.5in 0.5in 0.5in 0.5in;
                 }
 p.MsoNormal, li.MsoNormal, div.MsoNormal {
                margin:0in;
                margin-bottom:.0001pt;
                font-size:12.0pt;
                font-family:"Arial"
              }
 table.atable {
                border-collapse:collapse;
                border:1px solid #B3B3B3
              }
 tr           {
                page-break-inside:avoid
              }
 div.Section1 {
                page:Section1;
              }
.panelVisited      { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #003265 }
.panelLarge      { font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #003265 }
.panelVisitedColor { background-color:#62A85F }
.panelActive       { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #FFFFFF }
.panelActiveColor  { background-color:#EAC563 }
.panel             { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #FFFFFF }
.panelColor        { background-color:#BB2F2F }

</style>
<br><br>
<html:form action="/onlineRegisterOtherDetails">
<html:hidden property="firstName"/>
<html:hidden property="lastName"/>
<html:hidden property="address"/>
<html:hidden property="emailAddress"/>
<html:hidden property="pwd"/>
<html:hidden property="phoneNbr"/>
<html:hidden property="obc"/>
<html:hidden property="dot"/>
<html:hidden property="peopleType"/>
<html:hidden property="companyName"/>
<html:hidden property="phoneExt"/>
<html:hidden property="workPhone"/>
<html:hidden property="workExt"/>
<html:hidden property="fax"/>
<html:hidden property="city"/>
<html:hidden property="state"/>
<html:hidden property="zip"/>

<fieldset style="width: 400px">
<legend class="FireResourceStatic">
     <table cellpadding="0" cellspacing="0">
            <tr>
              <td><img src="<%=contextRoot%>/jsp/online/images/<%=elms.agent.LookupAgent.getKeyValue("ONLINE_LOGO") %>" width="100" height="100"></td>
              <td class="panelVisited" style="color: #003265; font-size:16px; font-weight: bold">&nbsp;&nbsp;Account Registration</td>
              <td class="panelVisited" align="right" style="color: #003265; font-size:12px; font-weight: bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Step 2 of 2</td>
            </tr>
          </table>
          </legend>
  <table cellpadding="10" cellspacing="0" width="550px">
    <html:errors /><%=error%>
    <tr>
      <td align="center">


        <table cellpadding="5" cellspacing="0">

       <tr>
            <td colspan="3">
	      <span id="acctSub">
                <table border="0" cellspacing="0" cellpadding="2" align="right">
                  <tr>
                    <td class="panelLarge">Enter your account number</td>
                    <td class="FireSelect"><html:text  name = "onlineRegisterFrom"  property="accountNo" style="width:100px" styleClass="textbox"/></td>
                  </tr>
                </table>
              </span>
            </td>
          </tr>


          <tr>
            <td class="tabletext"  valign="top">
              <font class="panelLarge">Do you own property in <%=elms.agent.LookupAgent.getKeyValue("CITY_NAME") %>?
            </td>
            <td class="tabletext"  valign="top" align="center">
              <input type="radio" name="owner"><br><font class="panelVisited" style="font-size:8px"> YES
            </td>
            <td class="tabletext" valign="top" align="center">
              <input type="radio" name="owner"><br><font class="panelVisited" style="font-size:8px"> NO
            </td>
          </tr>

        

          <tr>
            <td class="tabletext"  valign="top">
              <font class="panelLarge">Are you a contractor doing business in <%=elms.agent.LookupAgent.getKeyValue("CITY_NAME") %>?
            </td>
            <td class="tabletext"  valign="top" align="center">
              <input type="radio" name="contractor" onClick="viewDetails()"><br><font class="panelVisited" style="font-size:8px"> YES
            </td>
            <td class="tabletext" valign="top" align="center">
              <input type="radio" name="contractor" onClick="viewDetails()"><br><font class="panelVisited" style="font-size:8px"> NO
            </td>
          </tr>
          <tr>
            <td colspan="3">
	      <span id="menuSub">
                <table border="0" cellspacing="0" cellpadding="3" align="right">
                  <tr>
                    <td class="panelLarge">Contractors State License Board License Number </td>
                    <td class="FireSelect"><html:text  name = "onlineRegisterFrom"  property="licenseNbr" style="width:100px" styleClass="textbox" maxlength="10" /></td>
                  </tr>
                  <tr>
                    <td class="panelLarge">Contractors State License Board Expiration Date </td>
                    <td class="tabletext" valign="top"><html:text property="licExpDate"  maxlength="10" style="width:100px" styleClass="textboxd" onkeypress="return validateDate();" onblur="DateValidate(this);" /> <a href="javascript:show_calendar('onlineRegisterFrom.licExpDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;"> <img src="../images/calendar.gif" width="16" height="15" border="0"></a></td>
                  </tr>
                </table>
              </span>
            </td>
          </tr>


          <tr>
            <td class="FireSelect" colspan="3" align="center">&nbsp;</td>
          </tr>


          <tr>
            <td class="FireSelect" colspan="3" align="center">
              <html:reset  value="Back" styleClass="FireSelect" style="width: 100px" onclick="history.back();"/>&nbsp;&nbsp;&nbsp;&nbsp;
              <html:button  property="Submit" value="Finish" styleClass="FireSelect" style="width: 100px" onclick="validateFunction()"/>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </table>
</fieldset>

</html:form>

</center>
</body>
</html:html>
