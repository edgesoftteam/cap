<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%
 	String contextRoot = request.getContextPath();
	java.util.List streetList = new elms.agent.AddressAgent().getStreetArrayList();
  	pageContext.setAttribute("streetList", streetList);

%>
<html:html>
<html:base/>
<head>
<link rel="stylesheet" href="../css/elms.css" type="text/css">
<link rel="stylesheet" href="../css/online.css" type="text/css">
<script language="javascript">

function getCheckedValue(radioObj) {
	if(!radioObj)
		return "";
	var radioLength = radioObj.length;
	if(radioLength == undefined)
		if(radioObj.checked)
			return radioObj.value;
		else
			return "";
	for(var i = 0; i < radioLength; i++) {
		if(radioObj[i].checked) {
			return radioObj[i].value;
		}
	}
	return "";
}

function selectStructure()
{
	if((document.forms[0].stypeId[0].checked == false)&&(document.forms[0].stypeId[1].checked == false)&&(document.forms[0].stypeId[2].checked == false))
	{
		alert("Please select a structure");
	}else
	{
	    var tempOnlineID = document.forms[0].tempOnlineID.value;
	    var landLsoId = "";
	    var lsoType = "";
        lsoType = "S";

      var stypeId = getCheckedValue(document.forms[0].elements['stypeId']);
      var  sName=    stypeId;

	    document.forms[0].action='<%=contextRoot%>/structure.do?tempOnlineID='+tempOnlineID+"&landLsoId="+landLsoId+"&lsoType="+lsoType+"&sName="+sName;
		document.forms[0].submit();
	}
}

</script>
</head>
<body text="#000000" leftmargin="5" bottommargin="0"  scroll="yes" topmargin="0" marginwidth="0" marginheight="0" rightmargin="0">



<center>

<html:form action="/structure">
<html:hidden property="tempOnlineID" />
<table align="center" cellpadding="0" cellspacing="0" style="height:100%" width="100%">

	<td align="center" valign="top" width="99%">
	<br><br> <br><br>
<fieldset style="width: 450px;height: 320px" >
<br><br>


<table cellpadding="15" cellspacing="0" width="100%">
 <html:errors />
  <tr>

        <td align="left"> <font class="panelVisited" style="font-size:30px"> 2<font class="panelVisited" style="font-size:12px"><bean:message key="structureInfo"/>
        </td>
   </tr>

    <tr>
         <td align ="left" >
         	<table>
         		<tr>
         			<td class="FireResource"><html:radio property="stypeId" value="RONPER" >Single - Family Residential</html:radio></td>
         		</tr>
          		<tr>
         			<td class="FireResource"><html:radio property="stypeId" value="MONPER">Duplex, Apartment, Condominium, Townhouse</html:radio></td>
         		</tr>
          		<tr>
         			<td class="FireResource"><html:radio property="stypeId" value="CONPER">Commercial / Industrial</html:radio></td>
         		</tr>
         		
         	</table>
       </td>

    </tr>


      </td>
            <tr>
                    <td class="FireSelect">
                    <div align="center">
                    <html:reset   value="Back" styleClass="FireSelect" style="width: 100px" onclick="history.back();"/>
                      <html:button  property="button" value="Next" styleClass="FireSelect" style="width: 100px" onclick="selectStructure()"/>
                      </div>
                </tr>
</table>
</fieldset>
</html:form>
</center>
	</td> 

			<jsp:include page="onlineSideBar.jsp" flush="true">
				<jsp:param name="page" value="2" />
			</jsp:include>

 </table>
</body>
</html:html>

