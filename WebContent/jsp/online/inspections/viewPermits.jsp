<%@page import="elms.security.User"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="elms.common.Constants"%>
<%@page import="elms.util.StringUtils"%>
<%@page import="elms.control.beans.online.CartDetailsForm"%>
<%@page import="elms.util.db.Wrapper"%>
<%@page import="elms.agent.LookupAgent"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%


User user =new User();

user= (User)session.getAttribute("user");

session.setAttribute("user", user);
NumberFormat formatter = NumberFormat.getCurrencyInstance();
String formatValue = formatter.format(0.00);

int comboActSize =0;
if(request.getAttribute("comboSize") != null){
 comboActSize = elms.util.StringUtils.s2i((String) request.getAttribute("comboSize"));

session.setAttribute("comboSize", (String) request.getAttribute("comboSize")); 
}else{
	 comboActSize = elms.util.StringUtils.s2i((String) session.getAttribute("comboSize"));
}

if(request.getAttribute("comboActSize") != null){
	comboActSize = StringUtils.s2i((String) request.getAttribute("comboActSize"));   
	session.setAttribute("comboActSize", (String) request.getAttribute("comboActSize"));   
}else{
	comboActSize = StringUtils.s2i((String) session.getAttribute("comboActSize"));   
}
System.out.println("comboActSize.. in permits "+comboActSize);


java.util.List comboList = new java.util.ArrayList();
if(request.getAttribute("comboList") != null){
comboList = (java.util.List) request.getAttribute("comboList");
session.setAttribute("comboList", comboList);
}else{
	comboList = (java.util.List) session.getAttribute("comboList");
}


java.util.List cartDetailsForms = new java.util.ArrayList();
if(request.getAttribute("cartDetailsForms") != null){
comboList = (java.util.List) request.getAttribute("cartDetailsForms");
session.setAttribute("cartDetailsForms", cartDetailsForms);
}else{
	comboList = (java.util.List) session.getAttribute("cartDetailsForms");   
}

	
CartDetailsForm cartDetailsForm = new CartDetailsForm();
if(request.getAttribute("cartDetailsForm") != null){
cartDetailsForm = (CartDetailsForm) request.getAttribute("cartDetailsForm");

session.setAttribute("cartDetailsForm", cartDetailsForm);
}else{
	cartDetailsForm = (CartDetailsForm) session.getAttribute("cartDetailsForm");
}
int noOfItems = StringUtils.s2i((cartDetailsForm.getCount()));

String totalAmnt = formatter.format(StringUtils.s2d(StringUtils.nullReplaceWithZero(cartDetailsForm.getTotalFeeAmount())));
totalAmnt=cartDetailsForm.getTotalFeeAmount();
if(totalAmnt == null || totalAmnt == "0" || totalAmnt == ""){
	totalAmnt = "0.00";
}

String cartId="";
if(cartDetailsForm.getCartId()!=null){
 cartId=cartDetailsForm.getCartId();
 session.setAttribute("cartId", cartId);
}else{
	cartId=(String)session.getAttribute("cartId");	 
}   
String email = cartDetailsForm.getEmail();
System.out.println("cartId... "+cartId);
System.out.println("noOfItems... "+noOfItems);
System.out.println("totalAmnt... "+totalAmnt);
System.out.println("email... "+cartDetailsForm.getEmail());
session.setAttribute("email", email);

%>

<html:html>
<app:checkLogon />
<head>
<html:base />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="../../script/sweetalert.min.js"></script> 
<script src="../../script/sweetalert.js"></script>
<link rel="stylesheet" href="/css/main.css" type="text/css">
<link rel="stylesheet" href="../../css/sweetalert.min.css" type="text/css">
<link rel="stylesheet" href="../../css/elms.css" type="text/css">
<link rel="stylesheet" href="../../css/online.css" type="text/css">
<link rel="stylesheet" href="../../../tools/jquery/jquery-ui.css" type="text/css">
<link rel="stylesheet" href="../../script/jquery-ui-1.8.2.custom/development-bundle/themes/base/jquery.ui.all.css" type="text/css">

<script src="../script/sweetalert.min.js"></script> 
<script src="../script/sweetalert.js"></script>
<script src="../script/sweetalert.min2.js"></script>
<link rel="stylesheet" href="../css/sweetalert.min2.css">
<link rel="stylesheet" href="../css/sweetalert.min.css" type="text/css">        
   
<style type="text/css">

.tooltip {
  position: relative;
  display: inline-block;
}

.tooltip .tooltiptext {
  visibility: hidden;
  width: 260px;
  background-color: black;
  color: #fff;
  text-align: center;
  border-radius: 6px;
  padding: 5px 0;
  position: absolute;
  z-index: 1;
/*   top: -5px; */
  right: 110%;
}

.tooltip .tooltiptext::after {
  content: "";
  position: absolute;
/*   top: 50%; */
  left: 100%;
/*   margin-top: -5px; */
  border-width: 5px;
  border-style: solid;
  border-color: transparent transparent transparent black;
}
.tooltip:hover .tooltiptext {
  visibility: visible;
}

/*---------------------------------------------*/
input {
	outline: none;
/* 	border: none; */
}

.textBox {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 2px solid rgba(81, 203, 238, 1);
  border-radius: 32px;
  box-sizing: border-box;
}
.textBox:focus {
  box-shadow: 0 0 5px rgba(81, 203, 238, 1);
  padding: 12px 20px;
  margin: 5px 1px 3px 0px;
  border: 1px solid rgba(81, 203, 238, 1);
}

i{
color:white;
 }

.container {
  position: relative;
  text-align: right;
  color: black;
}
.centered {
  color:black;
  position: absolute;
  top: 1%;
  left: 95.8%;
  transform: translate(-80%, -20%);
}

.bottom-right {
  position: absolute;
  bottom: -10px;
  right: -9px;
  font-size: 15px;
}
	.abutton {
		font-family: Oswald, Arial, Helvetica;
		text-transform: uppercase;
		padding: 10px;
		padding-left: 10px;
		padding-right: 10px;
		margin: 10px;
		font-size: 12px;
		font-weight: bold;
		color: #000000;
		cursor: pointer;
		text-decoration:none;
	}

	.abutton:hover {
		color: #ffffff;
	}
	.csui_labell{font-family: Oswald, Arial, Helvetica, sans-serif; font-size: 19px; font-weight: 700; color: #ffffff; vertical-align: top; text-transform: uppercase}
	table.csui, table.csui_title { width: 100%; padding: 0px; }
	table.csui { background-color: #cccccc; border-spacing: 1px; border-collapse: separate; }
	td.csui { padding: 6px; font-family: Armata, Arial, Helvetica, sans-serif; font-size: 17px; background-color: #ffffff; }

	table.csui_title { border-spacing: 0px; background-color: #777777; }
	td.csui_title { padding: 5px; width: 100%; font-family: Oswald, Arial, Helvetica, sans-serif; font-size: 19px; font-weight: 700; color: #ffffff; vertical-align: top; text-transform: uppercase }
	
	
	a.csui, a.csuisub { color: #000000; text-decoration: none }
	a.csui:hover, a.csuisub:hover { color: #336699; }
	td.csui_header, td.csui_label { padding: 6px; font-family: Roboto Condensed, Arial, Helvetica, sans-serif; font-size: 15px; background-color: #eeeeee; }
	td.csui_header1 { text-align:center;color:white;padding: 6px; font-family:  Arial, Helvetica, sans-serif; font-size: 15px; background-color: #003366; }
	td.csui_label { width: 10%; white-space: nowrap; vertical-align: top; padding: 8px; padding-top: 15px }
	td.csui_input { padding: 6px; font-family: Armata, Arial, Helvetica, sans-serif; font-size: 12px; background-color: #ffffff; width: 40% }
	div.csui_divider { height: 25px; }

	a.csui_title { color: #ffffff; text-decoration: none }
	a.csuisub_title { color: #555555; text-decoration: none }
	a.csuisub_header_title { color: #888888; text-decoration: none }
	
   
.modal-wrapper {
  width: 140%;
  height: 100%;
  position: fixed; 
  top: 0;
  left: -33%;
  background: rgba(41, 171, 164, 0.2);
  visibility: hidden;
  opacity: 0;
  -webkit-transition: all 0.25s ease-in-out;
  transition: all 0.25s ease-in-out;
}

.modal-wrapper.open {
  opacity: 1;
  visibility: visible;
}

.modal {
  width: 750px;
  display: block;
  margin: 40% 0 0 -100px;
  left:40%;
  position: relative;
  background: #fff;
  opacity: 0;
  -webkit-transition: all 0.5s ease-in-out;
  transition: all 0.5s ease-in-out;
}

.modal-wrapper.open .modal {
  margin-top: 100px;
  opacity: 1;
}

.head {
  width: 91.9%;
  height: 30px;
  padding: 12px 30px;
  overflow: hidden;
  background: #336699;
  font-family: Armata, Arial, Helvetica, sans-serif;
}

.btn-close {
  font-size: 28px;
  display: block;
  float: right;
  color: white;
}


.button {
  background: #003366;
  border-radius: 100px;
  padding: 10px 30px;
  color: white;
  text-decoration: none;
  font-size: 19px;
  margin: 0 15px;
}

/* Hover state animation applied here */
.button:hover { 
  -webkit-animation: hover 1200ms linear 2 alternate;
  animation: hover 1200ms linear 2 alternate;
}

/* Active state animation applied here */
.button:active {
  -webkit-animation: active 1200ms ease 1 alternate;
  animation: active 1200ms ease 1 alternate; 
  background: #5F9BE0;
}

/* Active state animation keyframes below */

@-webkit-keyframes active { 
  0% {transform: scale(1,1);}
  90% {transform: scale(.9,.88);}
  100% {transform: scale(.92,.9);}
}

keyframes active { 
  0% {transform: scale(1,1);}
  90% {transform: scale(.9,.88);}
  100% {transform: scale(.92,.9);}
}
@-webkit-keyframes hover { 
  0% { -webkit-transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  1.8% { -webkit-transform: matrix3d(1.016, 0, 0, 0, 0, 1.037, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.016, 0, 0, 0, 0, 1.037, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  3.5% { -webkit-transform: matrix3d(1.033, 0, 0, 0, 0, 1.094, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.033, 0, 0, 0, 0, 1.094, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  4.7% { -webkit-transform: matrix3d(1.045, 0, 0, 0, 0, 1.129, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.045, 0, 0, 0, 0, 1.129, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  5.31% { -webkit-transform: matrix3d(1.051, 0, 0, 0, 0, 1.142, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.051, 0, 0, 0, 0, 1.142, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  7.01% { -webkit-transform: matrix3d(1.068, 0, 0, 0, 0, 1.158, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.068, 0, 0, 0, 0, 1.158, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  8.91% { -webkit-transform: matrix3d(1.084, 0, 0, 0, 0, 1.141, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.084, 0, 0, 0, 0, 1.141, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  9.41% { -webkit-transform: matrix3d(1.088, 0, 0, 0, 0, 1.132, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.088, 0, 0, 0, 0, 1.132, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  10.71% { -webkit-transform: matrix3d(1.097, 0, 0, 0, 0, 1.107, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.097, 0, 0, 0, 0, 1.107, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  12.61% { -webkit-transform: matrix3d(1.108, 0, 0, 0, 0, 1.077, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.108, 0, 0, 0, 0, 1.077, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  14.11% { -webkit-transform: matrix3d(1.114, 0, 0, 0, 0, 1.067, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.114, 0, 0, 0, 0, 1.067, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  14.41% { -webkit-transform: matrix3d(1.115, 0, 0, 0, 0, 1.067, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.115, 0, 0, 0, 0, 1.067, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  16.32% { -webkit-transform: matrix3d(1.119, 0, 0, 0, 0, 1.077, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.119, 0, 0, 0, 0, 1.077, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  18.12% { -webkit-transform: matrix3d(1.121, 0, 0, 0, 0, 1.096, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.121, 0, 0, 0, 0, 1.096, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  18.72% { -webkit-transform: matrix3d(1.121, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.121, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  20.02% { -webkit-transform: matrix3d(1.121, 0, 0, 0, 0, 1.113, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.121, 0, 0, 0, 0, 1.113, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  21.82% { -webkit-transform: matrix3d(1.119, 0, 0, 0, 0, 1.119, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.119, 0, 0, 0, 0, 1.119, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  24.32% { -webkit-transform: matrix3d(1.115, 0, 0, 0, 0, 1.11, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.115, 0, 0, 0, 0, 1.11, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  25.53% { -webkit-transform: matrix3d(1.113, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.113, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  29.23% { -webkit-transform: matrix3d(1.106, 0, 0, 0, 0, 1.089, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.106, 0, 0, 0, 0, 1.089, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  29.93% { -webkit-transform: matrix3d(1.105, 0, 0, 0, 0, 1.09, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.105, 0, 0, 0, 0, 1.09, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  35.54% { -webkit-transform: matrix3d(1.098, 0, 0, 0, 0, 1.105, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.098, 0, 0, 0, 0, 1.105, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  36.64% { -webkit-transform: matrix3d(1.097, 0, 0, 0, 0, 1.106, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.097, 0, 0, 0, 0, 1.106, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  41.04% { -webkit-transform: matrix3d(1.096, 0, 0, 0, 0, 1.099, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.096, 0, 0, 0, 0, 1.099, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  44.04% { -webkit-transform: matrix3d(1.096, 0, 0, 0, 0, 1.097, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.096, 0, 0, 0, 0, 1.097, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  51.45% { -webkit-transform: matrix3d(1.099, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.099, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  52.15% { -webkit-transform: matrix3d(1.099, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.099, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  58.86% { -webkit-transform: matrix3d(1.101, 0, 0, 0, 0, 1.099, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.101, 0, 0, 0, 0, 1.099, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  63.26% { -webkit-transform: matrix3d(1.101, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.101, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  66.27% { -webkit-transform: matrix3d(1.101, 0, 0, 0, 0, 1.101, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.101, 0, 0, 0, 0, 1.101, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  73.77% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  81.18% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  85.49% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  88.59% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  96% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  100% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); } 
}

@keyframes hover { 
  0% { -webkit-transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  1.8% { -webkit-transform: matrix3d(1.016, 0, 0, 0, 0, 1.037, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.016, 0, 0, 0, 0, 1.037, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  3.5% { -webkit-transform: matrix3d(1.033, 0, 0, 0, 0, 1.094, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.033, 0, 0, 0, 0, 1.094, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  4.7% { -webkit-transform: matrix3d(1.045, 0, 0, 0, 0, 1.129, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); tra6nsform: matrix3d(1.045, 0, 0, 0, 0, 1.129, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  5.31% { -webkit-transform: matrix3d(1.051, 0, 0, 0, 0, 1.142, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.051, 0, 0, 0, 0, 1.142, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  7.01% { -webkit-transform: matrix3d(1.068, 0, 0, 0, 0, 1.158, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.068, 0, 0, 0, 0, 1.158, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  8.91% { -webkit-transform: matrix3d(1.084, 0, 0, 0, 0, 1.141, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.084, 0, 0, 0, 0, 1.141, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  9.41% { -webkit-transform: matrix3d(1.088, 0, 0, 0, 0, 1.132, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.088, 0, 0, 0, 0, 1.132, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  10.71% { -webkit-transform: matrix3d(1.097, 0, 0, 0, 0, 1.107, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.097, 0, 0, 0, 0, 1.107, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  12.61% { -webkit-transform: matrix3d(1.108, 0, 0, 0, 0, 1.077, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.108, 0, 0, 0, 0, 1.077, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  14.11% { -webkit-transform: matrix3d(1.114, 0, 0, 0, 0, 1.067, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.114, 0, 0, 0, 0, 1.067, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  14.41% { -webkit-transform: matrix3d(1.115, 0, 0, 0, 0, 1.067, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.115, 0, 0, 0, 0, 1.067, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  16.32% { -webkit-transform: matrix3d(1.119, 0, 0, 0, 0, 1.077, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.119, 0, 0, 0, 0, 1.077, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  18.12% { -webkit-transform: matrix3d(1.121, 0, 0, 0, 0, 1.096, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.121, 0, 0, 0, 0, 1.096, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  18.72% { -webkit-transform: matrix3d(1.121, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.121, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  20.02% { -webkit-transform: matrix3d(1.121, 0, 0, 0, 0, 1.113, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.121, 0, 0, 0, 0, 1.113, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  21.82% { -webkit-transform: matrix3d(1.119, 0, 0, 0, 0, 1.119, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.119, 0, 0, 0, 0, 1.119, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  24.32% { -webkit-transform: matrix3d(1.115, 0, 0, 0, 0, 1.11, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.115, 0, 0, 0, 0, 1.11, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  25.53% { -webkit-transform: matrix3d(1.113, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.113, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  29.23% { -webkit-transform: matrix3d(1.106, 0, 0, 0, 0, 1.089, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.106, 0, 0, 0, 0, 1.089, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  29.93% { -webkit-transform: matrix3d(1.105, 0, 0, 0, 0, 1.09, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.105, 0, 0, 0, 0, 1.09, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  35.54% { -webkit-transform: matrix3d(1.098, 0, 0, 0, 0, 1.105, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.098, 0, 0, 0, 0, 1.105, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  36.64% { -webkit-transform: matrix3d(1.097, 0, 0, 0, 0, 1.106, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.097, 0, 0, 0, 0, 1.106, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  41.04% { -webkit-transform: matrix3d(1.096, 0, 0, 0, 0, 1.099, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.096, 0, 0, 0, 0, 1.099, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  44.04% { -webkit-transform: matrix3d(1.096, 0, 0, 0, 0, 1.097, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.096, 0, 0, 0, 0, 1.097, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  51.45% { -webkit-transform: matrix3d(1.099, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.099, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  52.15% { -webkit-transform: matrix3d(1.099, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.099, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  58.86% { -webkit-transform: matrix3d(1.101, 0, 0, 0, 0, 1.099, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.101, 0, 0, 0, 0, 1.099, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  63.26% { -webkit-transform: matrix3d(1.101, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.101, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  66.27% { -webkit-transform: matrix3d(1.101, 0, 0, 0, 0, 1.101, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.101, 0, 0, 0, 0, 1.101, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  73.77% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  81.18% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  85.49% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  88.59% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  96% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  100% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); } 
}

.content { padding: 6%;}	
</style>
</head>
<%
	String contextRoot = request.getContextPath();
String message ="";
	if(request.getAttribute("message")!=null){
			message = (String) request.getAttribute("message");
			session.setAttribute("message", message);
	}else{
		 message ="";
	}
		String inActive = "";
   if(request.getAttribute("inActive")!=null){			
		inActive = 	(String) request.getAttribute("inActive");
		session.setAttribute("inActive", inActive);
		
	}else{
		inActive ="";
	}       
 
%>
<SCRIPT language="JavaScript">
function switchMenu(menuSub)
{
	if(document.forms[0].menuSub.style.display == "none"){
		document.forms[0].menuSub.style.display = "block";   
	}else{
		document.forms[0].menuSub.style.display = "none";
	}
}

function openStatusDetails()
{
	window.open("<%=contextRoot%>/jsp/online/statusDetails.jsp?contextHttpsRoot=<%=contextRoot%>",target="_new","toolbar=no,width=600,height=400,screenX=100,left=100,screenY=100,top=1,location=no,status=no,menubar=no,scrollbars=yes");
}

function menuDisplay(){
	var menuSub="";
	for(var i=0;i<<%=comboActSize%>;i++){
		menuSub="menuSub"+i;
		document.forms[0].menuSub.style.display="none";  
		//document.getElementById("menuSub"+i).style.display="";
	}
}

function lncvPrint(actId){
	document.forms[0].action = "<%=contextRoot%>/onlineLncv.do?action=onlinePrint&activityId="+actId;
	document.forms[0].submit();
}

function changeActivityStatus(actId){
    window.open("www.google.com",target="_new","toolbar=no,screenX=100,left=100,screenY=100,top=1,location=no,status=no,menubar=no,scrollbars=yes");
}

function goToPay(){
	document.getElementById("button").style.display="none";
	document.getElementById("button").style.display="none";
	
	window.open("<%=contextRoot%>/processPayment.do?levelId=<%=cartId%>&amount=<%=totalAmnt%>&online=Y&from=CAP","_top");
 }

function removePermit(levelId){
	swal({
		  title: "Are you sure?",
		  text: "Do you want to untag this permit?",
// 		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes",
		  cancelButtonText: "No",
		  closeOnConfirm: false,
		  closeOnCancel: false
		},
		function(isConfirm) {
		  if (isConfirm) {
			  document.forms[0].action = "<%=contextRoot%>/removeActivity.do?action=view&email=<%=email%>&levelId="+levelId;
			  document.forms[0].submit();
		  }else{
			  swal("Permit is not removed.");
		  }
		});
		
}


function cancelPermit(levelId){
	swal({
		  title: "Are you sure?",
		  text: "Do you want to cancel this permit?",
// 		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes",
		  cancelButtonText: "No",
		  closeOnConfirm: false,
		  closeOnCancel: false
		},
		function(isConfirm) {
		  if (isConfirm) {
			  document.forms[0].action = "<%=contextRoot%>/cancelPermit.do?online=Y&levelId="+levelId;
			  document.forms[0].submit();
		  }else{
			  swal("Permit is not removed.");
		  }
		});
		
}

function addPermit(){
		var actNbr=document.forms[0].elements['activityNo'].value;
		if(actNbr == "" || actNbr == null){
			swal("Please enter permit number");
			document.forms[0].elements['activityNo'].focus;
			return false;
		}else{
			document.forms[0].action = "<%=contextRoot%>/addPermit.do?email=<%=email%>";
			document.forms[0].submit();
		}
}
function refresh(){	
	document.forms[0].action = "<%=contextRoot%>/myPermits.do?viewPermit=Yes&isObc=Y&isDot=N&inActive=Yes";
	document.forms[0].submit();
}

</script>

<body class="csui"  leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="menuDisplay()">

<html:form action="/addPermit.do">
<html:errors/>

	<table width="100%" border="0" cellspacing="10" cellpadding="0" >
		<tr valign="top">
			<td width="99%">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" >
				<tr>
					<td></td>
				</tr>
				<TR>
					<TD><font color='green' size='5'><b><%=message%></b></font></TD>
				</TR>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td width="100%">
					<table width="100%">
					<tr>
						<td style="padding: 5px; font-family: Oswald, Arial, Helvetica, sans-serif; font-size: 18px; font-weight: 600; color: black; vertical-align: top; text-transform: uppercase "  width="50%">My Permits</td>
					<%if (inActive.equalsIgnoreCase("yes")) {%>
						<td class="csui_title" width="50%">
							<div class="container">																	  	
							 <b>Total Cart Value: <%=totalAmnt %></b>
								<%if(noOfItems > 0){ %><a class="trigger" id="updateCartLock" style="font-color:blue;" href="#"><%} %><img id="mCart" align="bottom" border=0 style="border:0; text-decoration:none; outline:none" width="9.3%" title="Go to cart" src="<%=contextRoot%>/jsp/images/cart.jpg" alt="Cart"><%if(noOfItems > 0){ %></a> &nbsp;&nbsp;&nbsp;<%} %>
						        <%if(noOfItems > 0){ %>
							  		<div class="centered"><%=noOfItems %></div>
						        <%}else{%>
								  <div class="centered" style="left: 98.8%;">0</div>
						        <%} %>

<%-- 							 <%if(noOfItems > 0){ %><a href="#" class="button trigger" style="color: white;text-decoration: none;font-size:15px;border-radius:10px;">Proceed to pay</a><%} %> --%>
					        </div>						
						</td>
					<%} %>
					</tr>
					</table>
					</td>
				</tr>
				
				<tr>
					<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="2" class="csui">
						<tr>
							<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0" class="csui">
<!-- 								<tr> -->
<!-- 									<td width="100%" class="csui_title">My Permits</td> -->
<!-- 								</tr> -->
<!-- 								<tr> -->
<!-- 									<td colspan="3"><br> -->
<!-- 									</td> -->
<!-- 								</tr> -->

								<%
							
									if (inActive.equalsIgnoreCase("yes")) {
											
								%>
								<tr >
<!-- 									<td width="100%" class="csui">Below is a summary of your Active Permits.</td> -->
								</tr>
								<%
											} else {
								%>
								<tr >
									<td width="100%" class="csui">Below is a summary of your Closed Permits.</td>
								</tr>
								<%
									}
								%>
								
								<tr>
									<td background="../images/site_bg_B7C1CB.jpg" colspan="5"><!--Outer Combo Number Display--> 
									<table width="100%" border="0" cellspacing="0" cellpadding="0" class="csui" >										
										<tr>
											<%if (inActive.equalsIgnoreCase("yes")) {%>
								            <td class="csui" width="60%" >											        
											<div class="container csui">
												<div class="tooltip">
												  <span class="tooltip tooltiptext">Add a permit number</span>
												<input class="textbox" style="font-size: 18px;" type="text" maxlength="9" placeholder="Add a Permit Number" name="activityNo">
												 </div>
												<img title="Add a permit number" align="bottom" alt="" width="3.5%" onclick="addPermit();" src="<%=contextRoot%>/jsp/online/images/add.jpg">				
								                <img alt="Refresh" align="bottom" width="3.7%" onclick="refresh();" src="<%=contextRoot%>/jsp/images/refresh.jpg" >
									        </div>
											</td>
	    									<%}%>
    									</tr>
<!--     									<tr>											 -->
<!-- 											<td class="csui_labell" width="50%">&nbsp;</td> -->
<!-- 											<td class="csui_labell" width="50%">Address</td> -->
<!-- 										</tr> -->
									</table>
									
								<%if (comboActSize > 0) {
									int k = 0;
								%>

												
								<table width="100%" border="0" cellspacing="0" cellpadding="2" class="csui">
								
									<tr>
										<td class="csui_header" width="1%">&nbsp;</td>
										<td class="csui_header" width="12%">Permit Number</td>
										<td class="csui_header" width="26%">Permit Type</td>
										<td class="csui_header" width="18%">Status </td>
										<td class="csui_header" width="10%">Amount </td>
										<td class="csui_header" width="7%">Print</td>
										
										<%if (inActive.equalsIgnoreCase("yes")) {%>
										<td class="csui_header" width="10%" >Add to Cart</td>
										<td class="csui_header" width="8%">Untag</td>
										<%} %>
										
										<td class="csui_header" width="8%">Cancel</td>
									</tr>
									</table>
								
									<logic:iterate id="combo" name="comboList" type="elms.control.beans.online.ApplyOnlinePermitForm">

										<table width="100%" border="0" cellspacing="0" cellpadding="1" class="csui">
										
<%-- 										<logic:notMatch name="combo" property="streetName" value="PW CITYWIDE"> --%>
											<tr bgcolor="FFFFFF">

												<td valign="top" class="csui" style="cursor: pointer; cursor: hand" onclick="switchMenu('menuSub<%=k%>')"><span id="menu2"><b><bean:write name="combo" property="deptDesc" /></b></span></td>

											</tr>
<%-- 										</logic:notMatch> --%>
											<tr>
												<td colspan="7" ><span id="menuSub<%=k%>">
												<table width="100%" border="0" cellspacing="0" cellpadding="2"  bgcolor="#b7c1cb">
												<!--Inner Activity List Display--> 
												<%
												 	int j = 0;
												 %> 
                                                       <bean:define id="actid" name="combo" property="comboActList" type="java.util.Collection" /> 
                                                        <logic:iterate id="act" name="actid" type="elms.control.beans.online.MyPermitForm" indexId="i">
													<logic:greaterThan  name="act" property="amount" value="<%=formatValue%>">
        
														<logic:notEqual name="act" property="activityTypeCode" value="GARSL">			
														<tr bgcolor="FFFFFF" id="<%=j%>">

															<td valign="top" width="1%"></td>
															<td class="csui" valign="top" align="left" width="12%"><a href='<%=contextRoot%>/myPermits.do?action=explore&viewPermit=Yes&activityId=<bean:write name="act" property="activityId" />'> <bean:write name="act" property="activityNo" /> </a></td>
															<td class="csui" valign="top" align="left" width="26%"><bean:write name="act" property="activityType" /></td>
															<td class="csui" valign="top" align="left" width="18%"><bean:write name="act" property="activityStatus" /></td>
															<td class="csui" valign="top" align="left" width="10%"><bean:write name="act" property="amount" /></td>
															
<%-- 																<logic:notEqual name="act" property="activityTypeCode" value="PKNCOM"> --%>
																<%if(LookupAgent.getKeyValue(Constants.PROD_SERVER_FLAG).equalsIgnoreCase("N")) {%>
<%-- 																<td class="csui" align="left" width="7%"><a target="_blank" href="<%=LookupAgent.getKeyValue(Constants.ONLINE_PAYMENT_PRINT_NONPROD_URL)%><bean:write name="act" property="activityNo"/>"><img src="<%=contextRoot%>/jsp/online/images/print.png" border=0 width="25%" title="Print"></a></td> --%>
																<td class="csui" align="left" width="7%"><a target="_blank" href="<%=LookupAgent.getKeyValue(Constants.ONLINE_PAYMENT_PRINT_NONPROD_URL)%><bean:write name="act" property="activityId"/>"><img src="<%=contextRoot%>/jsp/online/images/print.png" border=0 width="25%" title="Print"></a></td>
			                                                       <%}else if(LookupAgent.getKeyValue(Constants.PROD_SERVER_FLAG).equalsIgnoreCase("Y")) { %>
<%-- 			                                                     	 <td class="csui" align="left" width="7%"><a target="_blank" href="<%=LookupAgent.getKeyValue(Constants.ONLINE_PAYMENT_PRINT_PROD_URL)%><bean:write name="act" property="activityNo"/>" class="tablebutton"><img src="<%=contextRoot%>/jsp/online/images/print.png" border=0 width="25%" title="Print"></a></td> --%>
			                                                     	 <td class="csui" align="left" width="7%"><a target="_blank" href="<%=LookupAgent.getKeyValue(Constants.ONLINE_PAYMENT_PRINT_PROD_URL)%><bean:write name="act" property="activityId"/>" class="tablebutton"><img src="<%=contextRoot%>/jsp/online/images/print.png" border=0 width="25%" title="Print"></a></td>
			                                                     <%} %>																	
<%-- 																</logic:notEqual> --%>
<%-- 																<logic:equal name="act" property="activityTypeCode" value="PKNCOM"> --%>
<!-- 																	<td class="csui" align="left" width="7%"> -->
<%-- 																	<a target="_blank" href="https://permit.burbankca.gov/Ripplestone/CRViewer.aspx?DocID=26&User=urluser&params=<bean:write name="act" property="activityId" />" class="tablebutton"><img src="<%=contextRoot%>/jsp/online/images/print.png" border=0 width="25%" title="Print"></a> --%>
<!-- 																	</td> -->
<%-- 																</logic:equal> --%>
																
																<%if (inActive.equalsIgnoreCase("yes")) {%>
																<logic:greaterThan  name="act" property="amnt" value="0.00">	
											                       <td class="csui" width="10%" align="left" >
											                       		<logic:equal  name="act" property="addedToCart" value="false">
																		<a class="abutton" href="<%=contextRoot%>/addToCart.do?from=vp_add&levelId=<bean:write name="act" property="activityId"/>&amount=<bean:write name="act" property="amount"/>&online=Y&email=<%=email %>"  ><img title="Add to cart" id="mCart" border=0 style="border:0; text-decoration:none; outline:none" alt="" width="35%" src="<%=contextRoot%>/jsp/images/addcart.jpg"></a>
																	  </logic:equal>
																	  <logic:equal name="act" property="addedToCart" value="true">
																	  <div class="removing-from-cart-div">
																	  	<a class="abutton" href="<%=contextRoot%>/addToCart.do?from=vp_remove&levelId=<bean:write name="act" property="activityId"/>&online=Y&email=<%=email %>"><img title="remove from cart"  border=0 style="border:0; text-decoration:none; outline:none" alt="" width="35%" src="<%=contextRoot%>/jsp/images/removecart.jpg"></a>
																  	  </div>
																  	  </logic:equal>
																  </td>
															   </logic:greaterThan>		
															   <logic:lessEqual   name="act" property="amnt" value="0.00">
															    <td class="csui" width="10%" align="left">&nbsp;</td>
															   </logic:lessEqual>
					   										  <logic:greaterThan  name="act" property="onlineActPeopleId" value="0">					   										  	
											                  		<td class="csui" width="8%" style="color: #003265; font-size:16px; font-weight: bold" >
												                  		<div class="untagRecord">
												                  			<a class="abutton" href="javascript:removePermit(<bean:write name="act" property="activityId"/>);" ><img alt="" width="43%"  border=0 style="border:0; text-decoration:none; outline:none" title="Untag" src="<%=contextRoot%>/jsp/images/delete1.jpg"></a>
												                  		</div>
											                  		</td>											                  	
											                  </logic:greaterThan>
											                  <logic:lessEqual  name="act" property="onlineActPeopleId" value="0">
											                  	<td class="csui" width="8%">&nbsp;</td>
											                  </logic:lessEqual>

					   										<%}%>
											                  <td class="csui" width="8%">&nbsp;</td>
											                  															
														</tr>
												</logic:notEqual>
												
												</logic:greaterThan>

        									<%if(LookupAgent.getKeyValue(Constants.GARAGE_SALE_FLAG).equalsIgnoreCase("Y")){ %>
												<logic:equal  name="act" property="activityTypeCode" value="GARSL">    
                                                
                                                <tr bgcolor="FFFFFF" id="<%=j%>">

                                                            <td valign="top" width="1%" ></td>
                                                            <td class="csui" valign="top" align="left" width="12%"><a href='<%=contextRoot%>/myPermits.do?action=explore&viewPermit=Yes&activityId=<bean:write name="act" property="activityId" />'> <bean:write name="act" property="activityNo" /> </a></td>
                                                            <td class="csui" valign="top" align="left" width="26%"><bean:write name="act" property="activityType" /></td>
                                                            <td class="csui" valign="top" align="left" width="18%"><bean:write name="act" property="activityStatus" /></td>
                                                            <td class="csui" valign="top" align="left" width="10%"></td>
                                                            <%
                                                            if (inActive.equalsIgnoreCase("yes")) {
                                                           	if(LookupAgent.getKeyValue(Constants.PROD_SERVER_FLAG).equalsIgnoreCase("N")) {%>
<%--                                                            	 <td class="csui" align="left" width="7%"><a target="_blank" href="<%=LookupAgent.getKeyValue(Constants.GARAGESALE_PRINT_NONPROD_URL)%><bean:write name="act" property="activityId"/>" class="tablebutton"><img src="<%=contextRoot%>/jsp/online/images/print.png" border=0 width="25%" title="Print"></a></td> --%>
                                                           	 <td class="csui" align="left" width="7%"><a target="_blank" href="<%=LookupAgent.getKeyValue(Constants.GARAGESALE_PRINT_NONPROD_URL)%><bean:write name="act" property="activityNo"/>" class="tablebutton"><img src="<%=contextRoot%>/jsp/online/images/print.png" border=0 width="25%" title="Print"></a></td>
                                                           <%}else if(LookupAgent.getKeyValue(Constants.PROD_SERVER_FLAG).equalsIgnoreCase("Y")) { %>
<%--                                                          	 <td class="csui" align="left" width="7%"><a target="_blank" href="<%=LookupAgent.getKeyValue(Constants.GARAGESALE_PRINT_PROD_URL)%><bean:write name="act" property="activityId"/>" class="tablebutton"><img src="<%=contextRoot%>/jsp/online/images/print.png" border=0 width="25%" title="Print"></a></td> --%>
                                                         	 <td class="csui" align="left" width="7%"><a target="_blank" href="<%=LookupAgent.getKeyValue(Constants.GARAGESALE_PRINT_PROD_URL)%><bean:write name="act" property="activityNo"/>" class="tablebutton"><img src="<%=contextRoot%>/jsp/online/images/print.png" border=0 width="25%" title="Print"></a></td>
                                                         <%} %>
                                                        <%if (inActive.equalsIgnoreCase("yes")) {%>
                                                            <td class="csui" align="left" width="10%"></td>
                                                            <td class="csui" align="left" width="8%"></td>
                                                            <%} %>
                                                            <td class="csui" align="left" width="8%">
                                                            <logic:equal  name="act" property="activityStatus" value="Permit Issued">
                                                            <a class="abutton" href="javascript:cancelPermit(<bean:write name="act" property="activityId"/>)" ><img src="../images/delete.gif" alt="Delete" border="0" width="22%"></a>
                                                            </logic:equal>
                                                            </td>
                                                            <%} else {%>
                                                            <td class="csui" align="left" width="7%"></td>
                                                            <%if (inActive.equalsIgnoreCase("yes")) {%>
                                                            
                                                            <td class="csui" align="left" width="10%"></td>
                                                            <td class="csui" align="left" width="8%"></td>
                                                            <%} %>
                                                            <td class="csui" align="left" width="8%"></td>
                                                            <%} %>
                                                </tr>
                                                
                                                </logic:equal>
                                                <%} %>
												</logic:iterate> </span></td>
											</tr>
											
											</table>
											<%k++;%>
										</table>
									</logic:iterate> 
										<%} %>
									</td>
								</tr>
								
								<tr>
 <td>


<!-- Modal -->
<div class="modal-wrapper">
  <div class="modal">
    <div class="head"  style="color:white;font-size:24px;">Cart Details Page<a class="btn-close trigger" id="updateCartLockFlag" style="color:white;" href="#"><i class="fa fa-times" aria-hidden="true"></i> X</a> </div>
    <div class="content">

	<table width="100%" cellpadding="0" cellspacing="0">
	    <tr class="csui">	      
		  <td width="100%" class="csui" style="color:red;"> Note: The name that will appear as the payee on the payment receipt is the credit card holder's name.</td>
		</tr>
	</table>
  <table cellpadding="50" cellspacing="0" width="80%" class="csui">
    <tr>
      <td align="center" class="csui" >
        <table cellpadding="5" cellspacing="0">
        <tr class="csui"  >
              <td colspan="6" class="csui" style="color: #003265; font-size:21px; font-weight: bold">&nbsp;&nbsp;Cart Details
              </td>              
        </tr>                        
	    <tr>
			<td class="csui_header1" style="font-size:18px;" width="10%">Permit Number</td>
			<td class="csui_header1" style="font-size:18px;text-align:left;" width="10%">Amount </td>
			</tr>
          <tr bgcolor="FFFFFF">

            <logic:iterate id="cartDetailsForm1" name="cartDetailsForms" type="elms.control.beans.online.CartDetailsForm" scope="request">   
                    <tr >
                       <td class="csui" style="text-align:center;" width="10%"> <bean:write name="cartDetailsForm1" property="activityNo" /> </td>
                       <td class="csui" align="left"><bean:write name="cartDetailsForm1" property="amount"/></td>
                    </tr>
                </logic:iterate>  
                      
			</tr>
			               
          <tr>
			<td class="csui_header1" align="left" style="font-size:18px;" width="10%">Total Cart Value : </td>
			<html:hidden name="cartDetailsForm" property="cartId"></html:hidden>
			<td class="csui_header1" style="font-size:18px;text-align:left;" width="10%"><bean:write name="cartDetailsForm" property="totalFeeAmount"/></td>
			</tr>			
          <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          </tr>
        </table>
        <table>
          <tr>
            <td class="csui" align="center">
<!--             onclick="goToPay();" -->
<%-- <%=contextRoot%>/processPayment.do?levelId=<%=cartId%>&amount=<%=totalAmnt%>&online=Y&from=CAP --%>
            <a href="javascript:goToPay();" id="button" class="button" style="color: white;text-decoration: none;font-size:18px;" >Proceed to Payment</a>
            </td>  
          </tr>
        </table>
      </td>
    </tr>
  </table>
    </div>
  </div>
</div>
<script src="../../../tools/jquery/jquery-3.1.0.slim.min.js"></script>   
<script src="../../../tools/jquery/jquery.min.js"></script>  
<script src="../../../tools/jquery/jquery-ui.js"></script>          
<script>

  $('.trigger').on('click', function() {	  
     $('.modal-wrapper').toggleClass('open');
    $('.page-wrapper').toggleClass('blur-it');
     return false;
  });
  
  $('#updateCartLock').on('click', function() {	  
	  $.ajax({
		  method: "POST",
		  url: "<%=contextRoot%>/updateCartLock.do",
		  data: { cartId:<%=cartId%>, action:"updateCartLock" }		  
	  });
		  
  });
  
  $(document).on("click",".removing-from-cart-div a", function(event) {	  
	     var url_string = $(this).attr('href');	  
	     var levelId = getUrlParameter('levelId',url_string);	     
	     event.preventDefault();	
	     $.ajax({
			  method: "POST",
			  url: "<%=contextRoot%>/updateCartLock.do",
			  data: { cartId:<%=cartId%>, action:"checkExistOrNot",levelId:levelId },
			  success: function(resultData) {				  
				  if(resultData.localeCompare("Y") == 0){			    	
				      swal("This permit can not be removed from cart, it is under process for payment.");					 	
				  }else{
					  window.location = "<%=contextRoot%>/addToCart.do?from=vp_remove&levelId="+levelId+"&online=Y&email=<%=email %>";	  			  
				  }
			  } 
			});	     
		
});    
  
   
  /* this function will update cart lock flag to N in cart and cart_details table. It will call on closing of payment modal window*/
  $('#updateCartLockFlag').on('click', function() {	  
  
	  $.ajax({
		  method: "POST",
		  url: "<%=contextRoot%>/updateCartLock.do",
		  data: { cartId:<%=cartId%>, action:"updateCartLockFlag" }		  
		});
		  
  });
  
  $(document).on("click",".untagRecord a", function(event) {
	  var url_string = $(this).attr('href');	  
	  var levelId = url_string.substring(
			  url_string.lastIndexOf("(") + 1, 
			  url_string.lastIndexOf(")")
			);
	  
	  event.preventDefault();	
	     $.ajax({
			  method: "POST",
			  url: "<%=contextRoot%>/updateCartLock.do",
			  data: { cartId:<%=cartId%>, action:"checkExistOrNot",levelId:levelId },
			  success: function(resultData) {				  
				  if(resultData.localeCompare("Y") == 0){			    	
				      swal("The permit can not be untagged for this user, it is under processing for payment.");					 	
				  }else{
					  removePermit(levelId); 			  
				  }
			  } 
		});		 
});  
  
  
  var getUrlParameter = function getUrlParameter(name, url) {
	    var urlString = url,
	        urlVariables = urlString.split('&'),
	        parameterName,
	        i;

	    for (i = 0; i < urlVariables.length; i++) {
	        parameterName = urlVariables[i].split('=');

	        if (parameterName[0] === name) {
	            return parameterName[1] === undefined ? true : decodeURIComponent(parameterName[1]);
	        }
	    }
	};
   
</script>
            </td>  
          </tr>
								
							</table>
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
</html:form>
</body>
</html:html>
