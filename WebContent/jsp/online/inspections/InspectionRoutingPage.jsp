<%@page import="elms.app.inspection.Inspection"%>
<%@page import="java.util.ArrayList,elms.util.*"%>
<%@page import="elms.control.beans.online.HolidayEditorForm"%>
<%@page import="java.util.List"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<html:html>
<%
String contextRoot = request.getContextPath();
String message = (String)request.getAttribute("message");
if(message==null) message="";

String error = (String)request.getAttribute("error");
if(error==null) error="";

String inspectionDate =(String)request.getAttribute("inspectionDate");
if(inspectionDate==null) inspectionDate="";

String inspectionDateCancel=(String)request.getAttribute("inspectionDateCancel");
if(inspectionDateCancel==null) inspectionDateCancel="";

String address = (String) request.getAttribute("address");


List<Inspection> list = (List) request.getAttribute("inspectionList");
System.out.println("inspectionList  :"+list);
java.util.List inspectionItemCodeList = (java.util.List) request.getAttribute("inspectionItemCodeList");
String  decisionButton = (String)request.getAttribute("decisionButton");
if(decisionButton == null) decisionButton= "";
%>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link href="<%=contextRoot%>/jsp/css/jquery-ui.css" rel="Stylesheet" type="text/css" />
<link rel="stylesheet" href="<%=contextRoot%>/jsp/css/elms.css" type="text/css">
<app:checkLogon/>

<script type="text/javascript" src="<%=contextRoot%>/jsp/script/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="<%=contextRoot%>/jsp/script/jquery-ui.js"></script>

<script language="JavaScript" src="<%=contextRoot%>/jsp/script/calendar.js"></script>
<SCRIPT language="JavaScript" src="<%=contextRoot%>/jsp/script/formValidations.js"></SCRIPT>
<script language="JavaScript" type="text/javascript">


var ids = '';
function update(control, id) {
	if(control.checked) {
		ids = id+","+ids;
	} else {
		ids = ids.replace(id+",", '');
	}
}		
function cancelInspection() {
	if(ids.length == 0) {
		alert("please select above inpsection to cancel");
		return false;
	} else {
		var action = document.forms['inspectionForm'].action;
		var temp = document.forms['inspectionForm'].elements['actid'].value;
		action = action + '?ids='+ids+'&queryString='+temp;
		document.forms['inspectionForm'].action = action;
		document.forms['inspectionForm'].submit();
	}
}

function submitResults() {
  document.forms[0].action='<%=contextRoot%>/inspectionResults.do?action=view';
 document.forms[0].submit();
}


function  submitRequest(){
	
	var strValue=true;
	
	   if (strValue==true){
		   
		      if (document.forms[0].elements['contactName'].value == '') {
		    	alert('Please enter contact name');
		    	 //	alert(validateEmail(document.forms['onlineRegisterFrom'].elements['emailAddress'].value));
		    	document.forms[0].elements['contactName'].focus();
		    	strValue=false;
		    	return false;
		      }
		   }
	   
	   if (strValue==true){
		   
		      if (document.forms[0].elements['contactPhone'].value == '') {
		    	alert('Please enter contact phone');
		    	 //	alert(validateEmail(document.forms['onlineRegisterFrom'].elements['emailAddress'].value));
		    	document.forms[0].elements['contactPhone'].focus();
		    	strValue=false;
		    	return false;
		      }
		   }

	 document.forms[0].action="<%=contextRoot%>/inspectionRequest.do?action=schedule";
	 document.forms[0].scheduleInspection.disabled=true;
	 document.forms[0].submit();
}

function  submitCancel(){
	
	if(ids.length == 0) {
		alert("please select above inpsection to cancel");
		return false;
	}
 document.forms[0].action='<%=contextRoot%>/inspectionResults.do?action=viewCancel&ids='+ids;
 document.forms[0].submit();
}

function  cancelInspectionsList(){
	 document.forms[0].action="<%=contextRoot%>/inspectionResults.do?action=displayCancel";
	 document.forms[0].submit();
	}


function  submitGetCode(){
 document.forms[0].action="<%=contextRoot%>/inspectionRequest.do?action=getCode";
 document.forms[0].submit();
}

function  submitResultsAddress(){
	 document.forms[0].action="<%=contextRoot%>/inspectionRequest.do?action=resaddress";
	 document.forms[0].submit();
	}

function  submitCancelAddress(){
	 document.forms[0].action="<%=contextRoot%>/inspectionRequest.do?action=cancelAddress";
	 document.forms[0].submit();
	}

if(document.addEventListener){ //code for Moz
document.addEventListener("keypress",keyCapt,false);
}else{
document.attachEvent("onkeyup",keyCapt); //code for IE
}
function keyCapt(e){
	if(typeof window.event!="undefined"){
		e=window.event;//code for IE
	}
	if(e.keyCode == 13){
		if("<%=decisionButton%>" == "fromRequest")
			document.forms[0].action="<%=contextRoot%>/inspectionRequest.do?action=schedule";
		else if("<%=decisionButton%>" == "fromResult")
			document.forms[0].action='<%=contextRoot%>/inspectionResults.do?action=view';
		else
			document.forms[0].action="<%=contextRoot%>/inspectionResults.do?action=viewCancel";
	}

}

function DisplayHyphenForPhone(str)
{
	if (( event.keyCode<48 || event.keyCode>57 ))
	{
		event.returnValue = false;
	}
	else
	{
		if ((document.forms[0].elements[str].value.length == 3 ) || (document.forms[0].elements[str].value.length == 7 ))
		{
			document.forms[0].elements[str].value = document.forms[0].elements[str].value +'-';
 		}
 		if (document.forms[0].elements[str].value.length > 11 )  event.returnValue = false;
	}
	return true;
}

function fieldFocus()
{
	var address ='<%=address%>';
	
	var message ='<%=message%>';
	if(address.length >0 && message !='Inspection schedule has been sucessfully cancelled')
	{
	document.forms[0].inspectionDate.focus();
	}		
}


</script>
</head>


<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="fieldFocus()" >

<html:form action="/inspectionRequest.do">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td> <% String buff = ""+
     "          To cancel an inspection after 7:00 am on the day of inspection, please contact Building & Safety Division staff at 818-238-5220 or your Inspector. ";%>
			          <font class="red2b"><%
			          if(message!=null && error!=null  && message.equals("") && error.equals("") && (inspectionDateCancel!=null && (!inspectionDateCancel.equalsIgnoreCase("")))) out.println(buff);
			          %>
          </td>
        </tr>
        <TR>
        <TD>
        <font class="green2b"><b><%=message%></b>
        </TD>
        </TR>
        <TR>
        <TD>
        <font class="red2b" ><b><%=error%></b>
        </TD>
        </TR>
  <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%"></td>
                     </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td>
                </td>
              </tr>
            </table>
          </td>
        </tr>
		<tr>
			<td>
			&nbsp;
			</td>
		</tr>
		<tr>
          <td>
            <table align="center" width="80%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="80%" border="0" cellspacing="0" cellpadding="0">
                    <tr align="left">
                       <%
          if (decisionButton.equalsIgnoreCase("fromResult")) {
      %>
                      <td width="100%" align="left" background="../../images/site_bg_B7C1CB.jpg" class="tabletitle">Inspections Scheduled</td>
                 <% } else  if (decisionButton.equalsIgnoreCase("fromRequest")) {
      %>
                <td width="100%" align="left" background="../../images/site_bg_B7C1CB.jpg" class="tabletitle" >Inspection Request</td>

             <% } else { %>
 	    	         <td width="100%" align="left" background="../../images/site_bg_B7C1CB.jpg" class="tabletitle">Inspection Cancel</td>
                <%}%>

                      <td width="1%" background="../../images/site_bg_D7DCE2.jpg"><nobr>
					  </td>
                    </tr>
                    <tr>
			<td>
			&nbsp;
			</td>
		</tr>
                  </table>
                </td>
              </tr>
                      <tr>
                <td width="80%" class="tabletext">
                  <table width="80%" border="0" cellspacing="1" cellpadding="2"  background="../../images/site_bg_B7C1CB.jpg">

    	        	<tr>
    	        	    <% if (decisionButton.equalsIgnoreCase("fromRequest")) {
		                 %>
    	               		<td width="40%" background="../../images/site_bg_D7DCE2.jpg" class="tablelabel">Permit Number : (Example BS0701234)</td>
    	                     <td class="tabletext"><html:text   styleClass="textbox" size="15"  maxlength="10"  property="permitNumber" onkeypress="keyCapt(event)" onblur="submitGetCode();"/></td>
    	                <% }else if (decisionButton.equalsIgnoreCase("fromResult")) { %>
    	                 	<td width="40%" background="../../images/site_bg_D7DCE2.jpg" class="tablelabel">Permit Number : (Example BS0701234)</td>
    	                     <td class="tabletext"><html:text   styleClass="textbox" size="15"  maxlength="10"  property="permitNumber" onkeypress="keyCapt(event)" onblur="submitResultsAddress();"/></td>
    	                   <%} else {%>
    	                   <td width="40%" background="../../images/site_bg_D7DCE2.jpg" class="tablelabel">Permit Number : (Example BS0701234)</td>
    	                     <td class="tabletext"><html:text   styleClass="textbox" size="15"  maxlength="10"  property="permitNumber" onkeypress="keyCapt(event)" onblur="submitCancelAddress();"/></td>
    	                       <%}%>
    	               </tr>
    	               <tr>
   	               		<td width="40%" background="../../images/site_bg_D7DCE2.jpg" class="tablelabel">Address</td>
   	                    <td class="tabletext"><%=address %></td>
    	                  
    	               </tr>
    	               
    	                                     <tr>
                   <% if (decisionButton.equalsIgnoreCase("fromRequest")) {
                   %>
                          <td width="40%"  background="../../images/site_bg_D7DCE2.jpg" class="tablelabel">Inspection Date :</td>
                           <%-- <td style="padding:6px" class="tabletext"><%=inspectionDate%></td> --%>
                             <td class="tabletext">
                             <html:text size="10" styleClass="textbox"  property="inspectionDate" readonly='true' styleId="inspectionDate" onblur="document.forms[0].inspectionCodes.focus()" /> 
                             <!-- <input id="datepicker" > -->
                             <!-- <a href="javascript:show_calendar('inspectionRoutingForm.inspectionDate');"
                          onmouseover="window.status='Calendar';return true;"
                          onmouseout="window.status='';return true;">
                          <img src="../../images/calendar.gif" width="16" height="15" border="0"/></a> --></td>
                           

                    <% } else if(inspectionDateCancel!=null && (!inspectionDateCancel.equalsIgnoreCase(""))) {
                    %>
                           <td  width="40%" background="../../images/site_bg_D7DCE2.jpg" class="tablelabel">Inspection Date :</td>
                           <%-- <td style="padding:6px" class="tabletext"><%=inspectionDateCancel%></td> --%>
                            <td class="tabletext">
                             <html:text size="10" styleClass="textbox"  property="inspectionDate" styleId="inspectionDate" readonly='true' onblur="document.forms[0].inspectionCodes.focus()" /> 
                             </td>


                    <% } else  {
                    	if (!decisionButton.equalsIgnoreCase("fromResult")) {
                    %>
                          <td width="40%" background="../../images/site_bg_D7DCE2.jpg" class="tablelabel"> Inspection Date :</td>
                          <td class="tabletext">
                             <html:text size="10" styleClass="textbox"  property="inspectionDate" readonly='true' onblur="document.forms[0].inspectionCodes.focus()"  /> 
                             </td>
                             <%}else{ %>
                             <html:hidden property="inspectionDate" /> 
                             <%} %>
                          <%-- <td class="tabletext"><html:text readonly="true" size="10" styleClass="textbox" onkeypress="return validateDate();"  property="inspectionDate"/> <a href="javascript:show_calendar('inspectionRoutingForm.inspectionDate');"
                          onmouseover="window.status='Calendar';return true;"
                          onmouseout="window.status='';return true;">
                          <img src="../../images/calendar.gif" width="16" height="15" border="0"/></a></td> --%>

                            <%}%>
                          </tr>
    	               
    	               
                      <% if (decisionButton.equalsIgnoreCase("fromRequest")) {%>

                       <tr>

		                <td width="40%" background="../../images/site_bg_D7DCE2.jpg" class="tablelabel">Inspection Type :</td>
                         <td  width="70%" background="../../images/site_bg_D7DCE2.jpg" class="tablelabel"><html:select property="inspectionCodes" styleClass="textbox" multiple="true" size="5">
				   <% if (inspectionItemCodeList!=null) {
		                 %>
                            <html:options collection="inspectionItemCodeList" property="code" labelProperty="description" />
                      <%}%>
                        	</html:select>
                        	 </td>

                       </tr>
                         <% }%>

             
      <%
        if (decisionButton.equalsIgnoreCase("fromRequest")) {
      %>
	   <tr>
        <td background="../../images/site_bg_D7DCE2.jpg" class="tablelabel" >Contact Information: Job Site Contact <font color="red">*</font></td>
        <td><input type="text" name="contactName"  size="15" placeholder="Name"/>
        <span background="../../images/site_bg_D7DCE2.jpg" class="tablelabel">Cell Phone  <font color="red">*</font> &nbsp;
        <input type="text" name="contactPhone"  size="15" placeholder="Cell Phone" onkeypress="DisplayHyphenForPhone('contactPhone');" />
        </span>  
        </td>
     
       </tr>
       <%-- <tr>
        <td width="40%" background="../../images/site_bg_D7DCE2.jpg" class="tablelabel">Contact Phone:</td>
        <td class="tabletext"><html:text property="contactPhone"  size="15"  onkeypress="DisplayHyphenForPhone('contactPhone');" /></td>
       </tr> --%>
       <tr>
        <td width="40%" background="../../images/site_bg_D7DCE2.jpg" class="tablelabel">Comments to the Inspector:</td>
        <td class="tabletext"><html:textarea property="comments" cols="40" rows="5"/></td>
       </tr>
      <%
        }
      %>

      <%
        if (decisionButton.equalsIgnoreCase("fromResult")) {
      %>
                      <tr>
                          <td align="right" style="padding:8px" colspan="2"  width="90%" background="../../images/site_bg_D7DCE2.jpg"  class="tablebutton"><html:button  styleClass="button" value="View Inspections" property="viewResults"  onclick="submitResults();"/></td>

                          </tr>

<%} else if (decisionButton.equalsIgnoreCase("fromRequest")){%>

            <tr>
                          <td  colspan="2" style="padding:6px" align="right" width="90%" background="../../images/site_bg_D7DCE2.jpg" class="tablebutton"><html:button   styleClass="button" value="Schedule Inspection Request" property="scheduleInspection" onclick="submitRequest();"/></td>

                          </tr>

                           <% } else {%>
                         <%--   <%if(list==null || list.size()==0){ %> --%>
                      <tr>
                          <td colspan="2" style="padding:6px" align="right" width="90%" background="../../images/site_bg_D7DCE2.jpg" class="tablebutton"><html:button  styleClass="button" value="Inspections List" property="cancel"  onclick="cancelInspectionsList();"/></td>
							
                          </tr>
                           <%}%>
						  
                  </table>
                </td>
              </tr>
              <%if(list!=null && list.size()>0){ %>
               <tr>
                <td>
                  <table width="80%" border="0" cellspacing="0" cellpadding="0">
                  <tr><td>&nbsp; </td> </tr>
              </table></td></tr>
               <tr>
                <td width="80%" class="tabletext">
                  <table width="80%" border="0" cellspacing="1" cellpadding="1"  background="../../images/site_bg_B7C1CB.jpg">
               <tr><td width="100%" align="left" background="../../images/site_bg_B7C1CB.jpg" class="tabletitle">Inspections </td></tr>
              </table></td></tr>
               <tr>
                <td width="80%" class="tabletext">
                  <table width="80%" border="0" cellspacing="1" cellpadding="1"  background="../../images/site_bg_B7C1CB.jpg">
              
               <%-- <%if () {%>   --%>  
                
          <tr >
          <td width="5%"  background="../../images/site_bg_D7DCE2.jpg" class="tablelabel"></td>
        <td width="20%"  background="../../images/site_bg_D7DCE2.jpg" class="tablelabel">Inspection Date</td>
         <td width="35%" background="../../images/site_bg_D7DCE2.jpg" class="tablelabel">Inspection Type</td>
        <td width="40%"  background="../../images/site_bg_D7DCE2.jpg" class="tablelabel">Inspection Status</td>
       </tr>
       <%Inspection insp = null;
       for(int i=0; i<list.size(); i++ ){
    	   insp = list.get(i);
    	   %>
        <tr >
        <td  class="tabletext"><input onchange="update(this, '<%=insp.getInspectionId()%>')" type="checkbox"/></td>
        <td class="tabletext"><%=insp.getDate() %></td>
        <td class="tabletext"><%=insp.getInspectionType() %></td>
        <td class="tabletext"><%=insp.getActionCodeDescription() %></td>
       </tr>  
       <%} %>
       </table></td></tr>
       <tr>
                <td width="80%" class="tabletext">
                  <table width="80%" border="0" cellspacing="1" cellpadding="1"  background="../../images/site_bg_B7C1CB.jpg">
                <tr>
                <td colspan="2" style="padding:6px" align="right" width="90%" background="../../images/site_bg_D7DCE2.jpg" class="tablebutton"><html:button  styleClass="button" value="Cancel Inspection" property="cancel"  onclick="submitCancel();"/></td>
							
               </tr>
              </table>
              </td>
        </tr>
             <%} %>       
         <tr>
          <td> 
              <font class="red2b">&nbsp;</font>  
          </td>
        </tr>
        
        <tr>
          <td> 
              <font class="red2b">If you have any questions, please call the Inspection Staff at 818-238-5220 between 7:00 and 8:00 am.</font>
          </td>
        </tr>
        
          </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>



      </table>

</html:form>

<%
List<HolidayEditorForm> holidaysList = (List<HolidayEditorForm>) request.getAttribute("holidaysList");
int countHoliday =0;
if(holidaysList!=null)
{
 countHoliday = holidaysList.size();
}
int countBusinessDay= countHoliday + StringUtils.s2i(StringUtils.nullReplaceWithZero(StringUtils.getKeyValue("CAP_INSPECTIONS_DAYS")));
%>

<script lang="javascript" type="text/javascript">
var dateObj = new Date();
var month = dateObj.getUTCMonth() + 1; //months from 1-12
var day = dateObj.getUTCDate();
var year = dateObj.getUTCFullYear();

dateObj.setDate(dateObj.getDate()+ <%=countBusinessDay%>);  

var month1 = dateObj.getUTCMonth() + 1; //months from 1-12
var day1 = dateObj.getUTCDate();
var year1 = dateObj.getUTCFullYear();
var d ='';
var holiday_dates = [];

<%


//String[] obj = holidaysList.toString().split(",");     	
    	List<String> holidayList = new ArrayList<String>();   
    	if(holidaysList!=null){
    	for(int i=0; i<=holidaysList.size()-1;i++){    		
    		String val = holidaysList.get(i).getDate().replaceAll( "[^a-zA-Z0-9 -]" , "" ).replaceAll(" ", "");    		
    		holidayList.add("'"+val+"'");     		
    	}
    	}

StringUtils stringUtilsInspDt = new StringUtils();
%>
<%-- 

$("#inspDate").kendoDatePicker({
<% if(inspectionDate != null && !inspectionDate.equalsIgnoreCase("")){%>
  value: new Date(<%=inspectionDate.substring(6, 10)%>, <%=inspectionDate.substring(0, 2)%> -1, <%=inspectionDate.substring(3, 5)%>),
  <%}%>
  min: new Date(year, month-1, day),
  max: new Date(year1, month1-1, day1),
  
  disableDates: holiday_dates, 
      /* disableDates: [new Date(2019,03,2), new Date(2019,03,3)],  */
  

  month: {
	empty: '<span class="k-state-disabled">#= data.value #</span>'
  }
 /*  dataSource: {
	    type: 'json'
	} */
});
 --%>    

 var holidaysList = <%=holidayList%>; 
function disableDates(date) { 
	
    var dt = $.datepicker.formatDate('mm-dd-yy', date);
    var noWeekend = jQuery.datepicker.noWeekends(date);
    
    return noWeekend[0] ? (($.inArray(dt, holidaysList) < 0) ? [true] : [false]) : noWeekend;
}
    $(document).ready(function () {
        $("#inspectionDate").datepicker({
        	minDate: 1,
            beforeShowDay: disableDates,   
            maxDate: <%=countBusinessDay%>   
        });  
        $("#inspectionDate").val(minDate);
      
      
    });
    
    


</script>


</body>
</html:html>
