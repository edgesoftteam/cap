<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="javax.sql.*,sun.jdbc.rowset.*" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>

<%
String contextRoot = request.getContextPath();
CachedRowSet crset = (CachedRowSet) request.getAttribute("holidayList");
if(crset !=null) crset.beforeFirst();
%>
<html:html>
<HEAD>
<html:base/>
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<LINK rel="stylesheet" href="../../css/elms.css" type="text/css">
<SCRIPT>
function deleteHoliday(holidayId) {
   userInput = confirm("Are you sure you want to delete this entry?");
   if (userInput==true) {
       document.location.href='<%=contextRoot%>/holidayEditor.do?action=delete&holidayId='+holidayId;
   }
}
</SCRIPT>
</HEAD>

<BODY >

<TABLE width="100%" border="0" cellspacing="10" cellpadding="0">
  <TBODY><TR valign="top">
    <TD width="99%">
      <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
        <TBODY><TR>
          <TD><FONT class="con_hdr_3b">Holiday List</FONT><BR>
            <BR>
            </TD>
        </TR>
        <TR>
          <TD>
            <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
              <TBODY><TR>
                <TD>
                  <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
                    <TBODY><TR>
                      <TD width="99%" class="tabletitle">Holidays</TD>
                      <TD width="1%" class="tablebutton"><NOBR>
                           <A href="<%=contextRoot%>/holidayEditor.do?action=add">Add<IMG src="../../images/site_blt_plus_000000.gif" width="21" height="9" border="0"></A></NOBR></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR>
                <TD background="../../images/site_bg_B7C1CB.jpg">
                  <TABLE width="100%" border="0" cellspacing="1" cellpadding="2">
                    <TBODY><TR>
                      <TD class="tablelabel">Date</TD>
                      <TD class="tablelabel">Description</TD>
                      <TD class="tablelabel">Delete</TD>
                    </TR>
 <%
   					while (crset.next()) {
   						int id = crset.getInt("insp_cal_id");
   						String date = elms.util.StringUtils.date2str(crset.getDate("insp_cal_date"));
   						String description = crset.getString("description");
 %>
            	    <TR valign="top">
	 					<TD class="tabletext">
             			  <%=date%>

             			</TD>

 	 					<TD class="tabletext">
             			<%= description%>

             			</TD>

                        <td class="tabletext" align="center">
                        	<a href="javascript:deleteHoliday(<%=id%>);"><img src="../../images/delete.gif" alt="Delete" border="0"></a>
                        </td>


					</TR>
 <%
 					}
 					if(crset!=null) crset.close();
 %>
                  </TBODY></TABLE>
                </TD>
              </TR>
            </TBODY></TABLE>
          </TD>
        </TR>
        <TR>
          <TD>&nbsp;</TD>
        </TR>
      </TBODY></TABLE>
    </TD>
    <TD width="1%">
      <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
        <TBODY><TR>
          <TD height="32">&nbsp;</TD>
        </TR>
        <TR>
          <TD>&nbsp;</TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>
</TBODY></TABLE>

</BODY>
</html:html>
