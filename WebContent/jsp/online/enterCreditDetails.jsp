<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@page import="elms.common.Constants"%>
<%@ page import="javax.sql.*,sun.jdbc.rowset.*,java.util.*" %>
<%@ page import="elms.app.finance.*" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>

<%
 	String contextRoot = request.getContextPath();
	String contextHttpsRoot = (String)request.getAttribute("contextHttpsRoot");

    String ssl_amount= (String)request.getAttribute("ssl_amount");
    String comboNo= (String)request.getAttribute("comboNo");
    String comboName = (String)request.getAttribute("comboName");
    String levelId= (String)request.getAttribute("sprojId");

    String tempOnlineID= (String)request.getAttribute("tempOnlineID");

    java.util.List YY = new java.util.ArrayList();
    YY= (java.util.List)request.getAttribute("YY");
    String isObc = ((elms.security.User)session.getAttribute(elms.common.Constants.USER_KEY)).getIsObc();
	String isDot = ((elms.security.User)session.getAttribute(elms.common.Constants.USER_KEY)).getIsDot();

%>
<style type="text/css">
    .FireTitle          { FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  td.FireHeading        { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none; PADDING: 10px; BACKGROUND-COLOR:#D9D9D9 }
    .FireHeading        { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireGlance         { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  td.FireResource       { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none; PADDING: 6px }
    .FireResource       { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
  td.FireResourceStatic { FONT-SIZE: 12px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none; PADDING: 6px }
    .FireResourceStatic { FONT-SIZE: 12px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireSelect         { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireText           { FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireHighlight      { FONT-SIZE: 15px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #990000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireMessage        { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
  td.FireMenu           { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none; PADDING: 2px; BACKGROUND-COLOR:#000000 }
    .FireMenu           { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:link      { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:visited   { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:active    { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:hover     { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireMenu1          { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:link     { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:visited  { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:active   { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:hover    { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #FFFFFF; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  @page Section1 {
                   size:8.5in 11.0in;
                   margin:0.5in 0.5in 0.5in 0.5in;
                 }
 p.MsoNormal, li.MsoNormal, div.MsoNormal {
                margin:0in;
                margin-bottom:.0001pt;
                font-size:12.0pt;
                font-family:"Arial"
              }
 table.atable {
                border-collapse:collapse;
                border:1px solid #B3B3B3
              }
 tr           {
                page-break-inside:avoid
              }
 div.Section1 {
                page:Section1;
              }


.panelVisited      { font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #000000 }
.panelVisitedColor { background-color:#62A85F }
.panelActive       { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #FFFFFF }
.panelActiveColor  { background-color:#EAC563 }
.panel             { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #FFFFFF }
.panelColor        { background-color:#BB2F2F }

.panelVisited:link      { font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #003265;  TEXT-DECORATION: none }
.panelVisited:visited      { font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #003265;  TEXT-DECORATION: none }
.panelVisited:active      { font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #003265;  TEXT-DECORATION: none }
.panelVisited:hover      { font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #003265;  TEXT-DECORATION: none }

</style>
<html:html>
<head>

<title>City of Burbank :Permitting and Licensing System: Fees</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>

<script language="JavaScript" src="../script/formValidations.js"></script>
<script language="JavaScript">

function openTerms()
{
window.open("<%=contextRoot%>/jsp/online/termsAndConditions.jsp?contextHttpsRoot=<%=contextHttpsRoot%>",target="_new","toolbar=no,width=1000,height=600,screenX=100,left=100,screenY=100,top=1,location=no,status=no,menubar=no,scrollbars=no");
}
function openPrivacy()
{
window.open("<%=contextRoot%>/jsp/online/privacyPolicy.jsp?contextHttpsRoot=<%=contextHttpsRoot%>",target="_new","toolbar=no,width=1000,height=800,screenX=100,left=100,screenY=100,top=1,location=no,status=no,menubar=no,scrollbars=no");
}
function openRefund()
{
window.open("<%=contextRoot%>/jsp/online/refundPolicy.jsp?contextHttpsRoot=<%=contextHttpsRoot%>",target="_new","toolbar=no,width=1000,height=800,screenX=100,left=100,screenY=100,top=1,location=no,status=no,menubar=no,scrollbars=no");
}





function confirmPayment()
{

  var month= document.forms['paymentMgrForm'].month.value;

  var year=  document.forms['paymentMgrForm'].year.value;

  var MMYY= month+year;
  var YYYYMM =document.forms['paymentMgrForm'].year.value + month;

    //adding the days to current date Getting Current Date
  var today = new Date();
  //Adding Number Of Days To Date
  var addDays   =new Date().setDate(today.getDate());
  //Getting the Date in UTC Format
  addDays = new Date(addDays);

       //Getting the Current Date in MM/DD/YYYY Format

  var currentDt = ((addDays.getFullYear().toString())+(addDays.getMonth() < 9 ? "0" + (addDays.getMonth() + 1).toString() : (addDays.getMonth() + 1).toString()));


  document.forms['paymentMgrForm'].ssl_exp_date.value=MMYY;
  document.forms['paymentMgrForm'].ssl_exp_date_year.value=year;
  document.forms['paymentMgrForm'].ssl_exp_date_month.value=month;
      if(isNaN(document.forms['paymentMgrForm'].ssl_card_number.value))
   {
   alert("Please Enter proper Credit Card Number");
   document.forms['paymentMgrForm'].ssl_card_number.focus();
   }
   else if(document.forms['paymentMgrForm'].ssl_card_number.value=="")
   {
   alert("Please Enter Credit Card Number");
   document.forms['paymentMgrForm'].ssl_card_number.focus();
   }
   else if(document.forms['paymentMgrForm'].ssl_cvv2cvc2.value=="")
   {
   alert("Please Enter CVV Number");
   document.forms['paymentMgrForm'].ssl_cvv2cvc2.focus();
   }
   else if(month=="" || year=="")
   {
   alert("Please select Expiration Date");
   document.forms['paymentMgrForm'].month.focus();
   }else if(YYYYMM < currentDt)
   {
   alert("Please enter valid date");
   document.forms['paymentMgrForm'].month.focus();

   }else if(document.forms['paymentMgrForm'].ssl_card_number.value.length<16)
   {
   alert("Credit Card No should be 16 digits");
   document.forms['paymentMgrForm'].ssl_card_number.focus();
   }
   else if(document.forms['paymentMgrForm'].ssl_cvv2cvc2.value.length<3)
   {
   alert("CVV/CV2 No should be 3 or 4 digits");
   document.forms['paymentMgrForm'].ssl_cvv2cvc2.focus();
   }
   else if(document.forms['paymentMgrForm'].ssl_ownerFName.value=="")
   {
   alert("Please enter First Name");
   document.forms['paymentMgrForm'].ssl_ownerFName.focus();
   }

   else if(document.forms['paymentMgrForm'].ssl_ownerLName.value=="")
   {
   alert("Please enter Last Name");
   document.forms['paymentMgrForm'].ssl_ownerLName.focus();
   }
      
   else if(document.forms['paymentMgrForm'].ssl_avs_address.value=="")
   {
   alert("Please Enter your Billing Address");
   document.forms['paymentMgrForm'].ssl_avs_address.focus();
   }

   else if(document.forms['paymentMgrForm'].ssl_city.value=="")
   {
   alert("Please enter City");
   document.forms['paymentMgrForm'].ssl_city.focus();
   }
      
   else if(document.forms['paymentMgrForm'].ssl_state.value=="")
   {
   alert("Please enter State");
   document.forms['paymentMgrForm'].ssl_state.focus();
   }

   else if(document.forms['paymentMgrForm'].ssl_country.value=="")
   {
   alert("Please enter Country");
   document.forms['paymentMgrForm'].ssl_country.focus();
   }
   
    else if(document.forms['paymentMgrForm'].ssl_avs_zip.value=="")
   {
   alert("Please Enter your Zip Code");
   document.forms['paymentMgrForm'].ssl_avs_zip.focus();
   }

   else{
	   
	   
	   document.forms['paymentMgrForm'].button.disabled=true;
  document.forms['paymentMgrForm'].action="<%=contextRoot%>/confirmPaymentDetails.do";
	 document.forms['paymentMgrForm'].submit();
	 }

}

function disableSubmit()
{

	document.forms[0].button[1].disabled = true;
	document.forms[0].termsCondition.checked=false;

   //document.forms[0].button.disabled=true;

}
function enableSubmit()
{
    if(document.forms[0].termsCondition.checked==true)
    {
	document.forms[0].button[1].disabled=false;
	}else
	{
	document.forms[0].button[1].disabled=true;
	}
}

function check()
{

var carCode= event.keyCode;
if ((carCode < 48) || (carCode > 57)){


event.returnValue = false;
}

}
function openCvv()
{
	window.open("<%=contextRoot%>/jsp/online/cvvDetails.jsp?contextHttpsRoot=<%=contextRoot%>",target="_new","toolbar=no,width=700,height=600,screenX=100,left=100,screenY=100,top=1,location=no,status=no,menubar=no,scrollbars=yes");
}
function cancelPermit(){
	document.forms[0].action='<%=contextRoot%>/jsp/online/welcomeOnline.jsp';
	document.forms[0].submit();
}
</SCRIPT>

<html:errors/>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="disableSubmit();">
<html:form name="paymentMgrForm" type="elms.control.beans.PaymentMgrForm" action="/confirmPaymentDetails">

<input type="hidden" name="ssl_amount" value="<%=ssl_amount%>">
<html:hidden property="tempOnlineID" value="<%=tempOnlineID%>" />
<input type="hidden" name="comboNo" value="<%=comboNo%>">
<input type="hidden" name="levelId" value="<%=levelId%>">
<input type="hidden" name="comboName" value="<%=comboName%>">
<input type="hidden" name="ssl_exp_date_year"/>
<input type="hidden" name="ssl_exp_date_month"/>

<input type="hidden" name="ssl_exp_date" value="ssl_exp_date">

<table align="center" cellpadding="0" cellspacing="0" style="height:100%" width="100%">
  <tr>
    <td align="center">
      <fieldset style="width: 450px;">
        <legend class="FireResourceStatic">
          <table cellpadding="5" cellspacing="0">
            <tr>
              <td><img src="<%=contextRoot%>/jsp/online/images/Shield.gif" width="100" height="100"></td>
              <td class="panelVisited" style="color: #003265; font-size:16px; font-weight: bold">PAYMENT INFORMATION</td>
            </tr>
          </table>
        </legend>

        <center>
        <table cellpadding="5" cellspacing="0">
          <html:errors />

          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <% if(!comboName.equals(Constants.ONLINE_LNCV_PERMIT)){ %>
          <tr>
            <td nowrap><font class="panelVisited">Permit No: </td>
            <td valign ="top" colspan="1" class="con_hdr_1" bgcolor="FFFFFF"><font class="panelVisited"> <%=comboNo%></td>
          </tr>
          <% } %>
          <tr>
            <td nowrap><font class="panelVisited">Permit Type: </td>
            <td colspan="2" valign ="top" colspan="1" class="con_hdr_1" bgcolor="FFFFFF"><font class="panelVisited"> <%=comboName%></td>
          </tr>
          <tr>
            <td nowrap><font class="panelVisited">Amount:</td>
            <td valign="top" colspan="1" class="con_hdr_1" bgcolor="FFFFFF"><font class="panelVisited"> <%=ssl_amount%></td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td nowrap><font class="panelVisited">Credit Card Number:</td>
            <td valign ="top" colspan="1" class="con_hdr_1" bgcolor="FFFFFF"> <html:text property="ssl_card_number" maxlength="16" onkeypress="check();" styleClass="panelVisited"></html:text></td>
          </tr>
          <tr>
            <td nowrap><font class="panelVisited">Security Code <img  width="12" height="12"  src="<%=contextRoot%>/jsp/online/images/help.png" style="cursor:pointer;cursor:hand" title="What is this ?" onclick="openCvv();"> : </td>
            <td valign ="top" colspan="1" class="con_hdr_1" bgcolor="FFFFFF">  <html:password property="ssl_cvv2cvc2" maxlength="4" onkeypress="check();" styleClass="panelVisited"></html:password></td>
          </tr>
          <tr>
            <td nowrap><font class="panelVisited">Expiration Date:</td>
            <td valign="top">
              <select name="month" class="panelVisited">
                <option>Month</option>
                <option value=01>Jan</option>
                <option value=02>Feb</option>
                <option value=03>Mar</option>
                <option value=04>Apr</option>
                <option value=05>May</option>
                <option value=06>Jun</option>
                <option value=07>Jul</option>
                <option value=08>Aug</option>
                <option value=09>Sep</option>
                <option value=10>Oct</option>
                <option value=11>Nov</option>
                <option value=12>Dec</option>
              </select>
              <select name="year" class="panelVisited">
                <option>Year</option>
		<%
		  for(int i=0;i<YY.size();i++){
		%>
                <option value="<%=YY.get(i)%>"><%=YY.get(i)%></option>
                <%
                  }
                %>
              </td>
            </tr>

             <tr>
            <td  class="panelVisited" style="color: #003265; font-size:11px; font-weight: bold" align="left" nowrap>Billing Address</td>
             </tr>
            <tr>
              <td nowrap><font class="panelVisited">First Name:</td>
              <td valign ="top" colspan="1" class="con_hdr_1" bgcolor="FFFFFF"> <html:text property="ssl_ownerFName" maxlength="30" styleClass="panelVisited"></html:text></td>
              <td nowrap><font class="panelVisited">Last Name:</td>
              <td valign ="top" colspan="1" class="con_hdr_1" bgcolor="FFFFFF"> <html:text property="ssl_ownerLName" maxlength="30" styleClass="panelVisited"></html:text></td>
            </tr>
            <tr>
              <td nowrap><font class="panelVisited">Street:</td>
              <td valign ="top" colspan="1" class="con_hdr_1" bgcolor="FFFFFF"> <html:text property="ssl_avs_address" maxlength="30" styleClass="panelVisited"></html:text></td>
              <td nowrap><font class="panelVisited">City:</td>
              <td valign ="top" colspan="1" class="con_hdr_1" bgcolor="FFFFFF"> <html:text property="ssl_city" maxlength="15" styleClass="panelVisited"></html:text></td>
            </tr>
            <tr>
              <td nowrap><font class="panelVisited">State:</td>
              <td valign ="top" colspan="1" class="con_hdr_1" bgcolor="FFFFFF"> 
              <select name="ssl_state" class="panelVisited">
             		 <option value="">Please Select</option>
					<option value="AL">Alabama</option>
					<option value="AK">Alaska</option>
					<option value="AZ">Arizona</option>
					<option value="AR">Arkansas</option>
					<option value="CA">California</option>
					<option value="CO">Colorado</option>
					<option value="CT">Connecticut</option>
					<option value="DE">Delaware</option>
					<option value="DC">District of Columbia</option>
					<option value="FL">Florida</option>
					<option value="GA">Georgia</option>
					<option value="HI">Hawaii</option>
					<option value="ID">Idaho</option>
					<option value="IL">Illinois</option>
					<option value="IN">Indiana</option>
					<option value="IA">Iowa</option>
					<option value="KS">Kansas</option>
					<option value="KY">Kentucky</option>
					<option value="LA">Louisiana</option>
					<option value="ME">Maine</option>
					<option value="MD">Maryland</option>
					<option value="MA">Massachusetts</option>
					<option value="MI">Michigan</option>
					<option value="MN">Minnesota</option>
					<option value="MS">Mississippi</option>
					<option value="MO">Missouri</option>
					<option value="MT">Montana</option>
					<option value="NE">Nebraska</option>
					<option value="NV">Nevada</option>
					<option value="NH">New Hampshire</option>
					<option value="NJ">New Jersey</option>
					<option value="NM">New Mexico</option>
					<option value="NY">New York</option>
					<option value="NC">North Carolina</option>
					<option value="ND">North Dakota</option>
					<option value="OH">Ohio</option>
					<option value="OK">Oklahoma</option>
					<option value="OR">Oregon</option>
					<option value="PA">Pennsylvania</option>
					<option value="RI">Rhode Island</option>
					<option value="SC">South Carolina</option>
					<option value="SD">South Dakota</option>
					<option value="TN">Tennessee</option>
					<option value="TX">Texas</option>
					<option value="UT">Utah</option>
					<option value="VT">Vermont</option>
					<option value="VA">Virginia</option>
					<option value="WA">Washington</option>
					<option value="WV">West Virginia</option>
					<option value="WI">Wisconsin</option>
					<option value="WY">Wyoming</option>
				</select>
			 </td>
			  <td nowrap><font class="panelVisited">&nbsp;</td>
              <td valign ="top" colspan="1" class="con_hdr_1" bgcolor="FFFFFF"> <html:hidden property="ssl_country"  value="US"></html:hidden></td>
            </tr>
            <tr>
			  <td nowrap> <font class="panelVisited">Zip Code:</td>
              <td valign ="top" colspan="1" class="con_hdr_1" bgcolor="FFFFFF"> <html:text property="ssl_avs_zip" maxlength="8" onkeypress="check();" styleClass="panelVisited"></html:text></td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
            <tr valign="top" >
              <td class="tabletext" colspan="2" align="center">
                <html:checkbox property="termsCondition" onclick="enableSubmit();"/>
                <font class="panelVisited">I hereby Agree to the <a href="<%=contextRoot%>/jsp/online/termsAndConditions.jsp?contextHttpsRoot=<%=contextHttpsRoot%>&scrollbars='No',resizable='No',statusBar='No',ToolBar='No'" target="_blank" >Terms and Conditions </a>
              </td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
       	</table>
       	<table cellpadding="5" cellspacing="0">
            <tr>
              <td class="FireSelect" >
                <div align="center">
                  <% if(!comboName.equals(Constants.ONLINE_LNCV_PERMIT)){ %>
                  	<html:reset  value="Back" styleClass="FireSelect" style="width: 100px" onclick="history.back();"/>
                  <% } %>
                  <html:button  property="button" value="Cancel" styleClass="FireSelect" style="width: 100px" onclick="cancelPermit()"/>
                  <html:button  property="button" value="Submit"  styleClass="FireSelect" style="width: 100px" onclick="confirmPayment()"/>
                </div>
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>

          </table>

        </center>
      </fieldset>


      <table align="center" cellpadding="0" cellspacing="0" width="100%">
        <tr>
          <td align="center">
            <a class="panelVisited" style="font-size:11px" href="<%=contextRoot%>/jsp/online/welcomeOnline.jsp";>Home</a>
            <font class="panelVisited">&nbsp; | &nbsp;<a href="#" onclick="openTerms();" class="panelVisited">Terms & Conditions</a>
            <font class="panelVisited">&nbsp; | &nbsp;<a href="#" onclick="openRefund();" class="panelVisited">Refund Policy</a>
            <font class="panelVisited">&nbsp; | &nbsp;<a href="#" onclick="openPrivacy();" class="panelVisited">Privacy Policy</a>
            <font class="panelVisited">&nbsp;&nbsp;&nbsp; <a  href="http://www.ci.burbank.ca.us/"  class="panelVisited"  target="_blank">� City of Burbank</a>
          </td>
        </tr>
      </table>

    </td>
  </tr>
</table>


</center>


</html:form>
</body>
</html:html>
