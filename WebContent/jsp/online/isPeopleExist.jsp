<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%
 	String contextRoot = request.getContextPath();
 	int peopleTypeId= elms.util.StringUtils.s2i((String)request.getAttribute("peopleTypeId"));
 	int peopleTypeIdCont= elms.util.StringUtils.s2i((String)request.getAttribute("peopleTypeIdCont"));
    java.util.List streetList = new elms.agent.AddressAgent().getStreetArrayList();
  	pageContext.setAttribute("streetList", streetList);


%>
<html:html>
<head>
<html:base/>
<link rel="stylesheet" href="../css/elms.css" type="text/css">
<%if(peopleTypeIdCont==4 && peopleTypeId==8){%>
<script language="javascript">

function getSubmit()
{

	 if(((document.forms[0].chkExistPeopleFlag[0].checked)==false) && ((document.forms[0].chkExistPeopleFlag[1].checked)==false)) {
		          alert("Please select the correct option to proceed.");
	 }else {
		       var tempOnlineID = document.forms[0].tempOnlineID.value;
		       var peopleType="C";
		       if(("document.forms[0].chkExistPeopleFlag[0].checked")==true){
			        document.forms[0].action='<%=contextRoot%>/jsp/online/ownerBuilder.jsp?tempOnlineID='+tempOnlineID+'&peopleType='+peopleType;
		        }else{
			        document.forms[0].action='<%=contextRoot%>/checkExistingPeople.do?tempOnlineID='+tempOnlineID+'&peopleType='+peopleType;
		        }
		       document.forms[0].submit();
		}
}
</script>
<%}else if(peopleTypeId==8){%>
<script language="javascript">

function getSubmit(){

	 if((("document.forms[0].chkExistPeopleFlag.checked")==false) && (("document.forms[0].chkExistPeopleFlag.checked")==false)){
		         alert("Please select atleast one option to proceed");
	   }else {
		       var tempOnlineID = document.forms[0].tempOnlineID.value;
		       var peopleType="C";
            	if(("document.forms[0].chkExistPeopleFlag.checked")==true){
			               document.forms[0].action='<%=contextRoot%>/jsp/online/ownerBuilder.jsp?tempOnlineID='+tempOnlineID+'&peopleType='+peopleType;
		       }else{
			               document.forms[0].action='<%=contextRoot%>/checkExistingPeople.do?tempOnlineID='+tempOnlineID+'&peopleType='+peopleType;
		            }
		        document.forms[0].submit();
		}
}
</script>
<%}else{%>
<script language="javascript">
function getSubmit(){
	
	 if((document.forms[0].chkExistPeopleFlag[0] != undefined)){
		 if((eval("document.forms[0].chkExistPeopleFlag[0].checked")==false) && (eval("document.forms[0].chkExistPeopleFlag[1].checked")==false)){
		        alert("Please select atleast one option to proceed");
	     }else {
		    var tempOnlineID = document.forms[0].tempOnlineID.value;
		    var peopleType="C";

		   document.forms[0].action='<%=contextRoot%>/checkExistingPeople.do?tempOnlineID='+tempOnlineID+'&peopleType='+peopleType;
		   document.forms[0].submit();
		}
	}else{
		   if((eval("document.forms[0].chkExistPeopleFlag.checked")==false)) {
		     alert("Please select the correct option to proceed.");
	       }else {
		     var tempOnlineID = document.forms[0].tempOnlineID.value;
		     var peopleType="C";

		     document.forms[0].action='<%=contextRoot%>/checkExistingPeople.do?tempOnlineID='+tempOnlineID+'&peopleType='+peopleType;
		     document.forms[0].submit();
		   }
	 }
}

</script>
<%}%>
<script language="javascript">
function disableSubmit()
{
   	document.forms[0].button.disabled=true;
   	document.forms[0].termsCondition.checked=false;
	document.forms[0].print.checked=false;
	if(document.forms[0].chkExistPeopleFlag[0] != undefined){
		document.forms[0].chkExistPeopleFlag[0].checked = false;
	}
	if(document.forms[0].chkExistPeopleFlag[1] != undefined){
		document.forms[0].chkExistPeopleFlag[1].checked = false;
	}
	if(document.forms[0].chkExistPeopleFlag[2] != undefined){
		document.forms[0].chkExistPeopleFlag[2].checked = false;
	}
}
function enableSubmit()
{
    if(document.forms[0].termsCondition.checked==true && document.forms[0].print.checked==true)
    {
	document.forms[0].button.disabled=false;
	}else
	{
	document.forms[0].button.disabled=true;
	}
}
function disableText(){
	document.getElementById("acctSub").style.display = "none";
	document.forms[0].termsCondition.checked=true;
	document.forms[0].print.checked=true;
	document.forms[0].button.disabled=false;
}
function enableText(){
	document.getElementById("acctSub").style.display = "block";
	document.forms[0].termsCondition.checked=false;
	document.forms[0].print.checked=false;
	document.forms[0].button.disabled=true;
}

</script>


</head>
<body text="#000000" leftmargin="5" bottommargin="0"  scroll="yes" topmargin="0" marginwidth="0" marginheight="40" rightmargin="0" onload="disableSubmit();">


<center>


<html:form action="checkExistingPeople">
<html:hidden property="tempOnlineID" />
<table align="center" cellpadding="0" cellspacing="0" style="height:100%" width="100%">

<td align="center" valign="top" width="99%">
	<br><br> <br><br>

<fieldset style="width: 450px;height: 320px" >

<br> <br>

<table cellpadding="15" cellspacing="0" width="100%" >
 <html:errors />

  <tr>
        <td align="left"> <font class="panelVisited" style="font-size:30px"> 5<font class="panelVisited" style="font-size:12px"><bean:message key="isOwnerBuilder"/>
        </td>
   </tr>
	<%if(peopleTypeId==8){%>
	<tr align="left">
    <td bgcolor="#FFFFFF" valign="top" colspan="3"><font class="panelVisited" style="font-size:12px">&nbsp;&nbsp;&nbsp;&nbsp;
    <html:radio property="chkExistPeopleFlag" value="O" onclick="enableText();"/> Owner Builder</td>
	</tr>
	<%}if(peopleTypeIdCont==4){%>

	 <tr align="left">
    <td bgcolor="#FFFFFF" valign="top" colspan="3"><font class="panelVisited" style="font-size:12px">&nbsp;&nbsp;&nbsp;&nbsp;
     <html:radio property="chkExistPeopleFlag" value="S" onclick="enableText();"/> Self  ( I am Contractor )</td>
	</tr>
	<%}%>


<tr>
      <td colspan="3">
  		<span id="acctSub">
		    <table border="0" cellspacing="0" cellpadding="2" align="right">
				<tr>
				    <td bgcolor="#FFFFFF" valign="top" colspan="3"><html:checkbox property="termsCondition" onclick="enableSubmit();"/><font color="red" style="font-family: Arial; font-size:11px">&nbsp;&nbsp;
				    This permit is to be issued in either the name of a California State Licensed Contractor or the Property Owner (Owner-Builder) as the permit holder of record.  The permit holder will be responsible and liable for the construction.
				    </td>
				</tr>
				<tr>
				    <td bgcolor="#FFFFFF" valign="top" colspan="3"><html:checkbox property="print" onclick="enableSubmit();"/><font color="red" style="font-family: Arial; font-size:11px">&nbsp;&nbsp;
				    A permit issued to the property owner will be considered an owner-builder permit.  An owner/builder owns the property and acts as their own general contractor on the job and either does the work themselves or has employees (or subcontractors) working on the project.  When you sign a building permit application as an owner/builder, you assume full responsibility for all phases of your project and its integrity. To understand the responsibilities of an owner-builder visit the California State Contractors License Board website.
				    </td>
				</tr>
				<tr>
			</table>
	      </span>
	    </td>
	  </tr>
	            <tr>
                    <td class="FireSelect">
                    <div align="center">
                    <html:reset  value="Back" styleClass="FireSelect" style="width: 100px" onclick="history.back();"/>
                      <html:button  property="button" value="Next" styleClass="FireSelect" style="width: 100px" onclick="getSubmit()"/>
                      </div>
                </tr>




</table>
</fieldset>
</html:form>
</center>

	</td>

			<jsp:include page="onlineSideBar.jsp" flush="true">
				<jsp:param name="page" value="5" />
			</jsp:include>
 </table>
</html:html>

