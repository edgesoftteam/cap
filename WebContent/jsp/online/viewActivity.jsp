<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="elms.common.Constants"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>


<%@ page import="javax.sql.*,sun.jdbc.rowset.*,java.util.*, elms.util.*, elms.agent.OnlineAgent" %>
<app:checkLogon/>
<html:html>

<head>
	<html:base/>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<bean:define id="user" name="user" type="elms.security.User"/>
<bean:define id="lsoAddress" name="lsoAddress" type="java.lang.String" scope="request"/>
<%
		String contextRoot = request.getContextPath();
		String onloadAction = "";
			onloadAction = (String)session.getAttribute("onloadAction");

		String activityId =   (String)request.getAttribute("activityId");
		String activityType= (String)request.getAttribute("activityType");
		if (activityType == null) activityType = "";

		String level = "A";

		String activityTypeDesc= (String)request.getAttribute("activityTypeDesc");
		if (activityTypeDesc == null) activityTypeDesc = "";
		List activitySubTypes=(ArrayList)request.getAttribute("activitySubTypes");
		if (activitySubTypes == null) activitySubTypes = new ArrayList();
		String activityNumber = (String)request.getAttribute("activityNumber");
		if (activityNumber == null) activityNumber = "";
		String description = (String)request.getAttribute("description");
		if (description == null) description = "";
		String address = (String)request.getAttribute("address");
		if (address == null) address = "";
		String status = (String)request.getAttribute("status");
		if (status == null) status = "";
		String valuation = (String)request.getAttribute("valuation");
		if (valuation == null) valuation = "";
		String issueDate = (String)request.getAttribute("issueDate");
		if (issueDate == null) issueDate = "";
		String startDate = (String)request.getAttribute("startDate");
		if (startDate == null) startDate = "";
		String completionDate = (String)request.getAttribute("completionDate");
		if (completionDate == null) completionDate = "";
		String expirationDate = (String)request.getAttribute("expirationDate");
		if (expirationDate == null) expirationDate = "";
		String createdBy = (String)request.getAttribute("createdBy");
		if (createdBy == null) createdBy = "";
		String completionDateLabel = (String)request.getAttribute("completionDateLabel");
		if (completionDateLabel == null) completionDateLabel = "";
		String expirationDateLabel = (String)request.getAttribute("expirationDateLabel");
		if (expirationDateLabel == null) expirationDateLabel = "";
		String microfilm = (String)request.getAttribute("microfilm");
		

	String error= request.getAttribute("error")!=null?(String)request.getAttribute("error"):"";

	java.util.List questionList = new java.util.ArrayList();
	questionList = OnlineAgent.getQuestionAnswers(activityNumber);
	pageContext.setAttribute("questionList", questionList);
%>

<html:form method="post" action="/printFIR.do">

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td colspan="3"><font class="red2b"><%=error%><br><br><font class="con_hdr_3b">View Permit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b">
				<% if(!(lsoAddress.trim().equalsIgnoreCase("1   Pw Citywide"))){ %>
                <bean:write name="lsoAddress"/>
                <%} %>
                <br>
                <br>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="2">
                            <tr>
                               <td width="99%" class="tabletitle">Permit Detail
								</td>
								 <td width="1%" class="tabletitle">
<!-- 								 <a href="javascript:history.back(0);" >Back</a> &nbsp;&nbsp; -->
								 <html:reset  value="Back" style="width: 100px;font-size: 15px;" onclick="javascript:history.back(0);"/>
								 </td>
                            </tr>

                            

                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr valign="top">
                                <td class="tablelabel">Permit #</td>
                                <td class="tabletext"><%=activityNumber%></td>
                                <td class="tablelabel">Permit Status</td>
                                <td class="tabletext"><%=status%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Permit Type</td>
                                <td class="tabletext"><%=activityTypeDesc%></td>
                                <td class="tablelabel">Sub Type</td>
                                <td class="tabletext">

                                <%
                                	Iterator it =activitySubTypes.iterator();
	                                while(it.hasNext()){
    		                            out.println(""+it.next()+"<br/>"+"<dt>");
            	                    }
                                 %>
                                </td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Description</td>
                                <td class="tabletext" colspan="3"><%=description%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Applied Date</td>
                                <td class="tabletext"><%=startDate%></td>
                                <td class="tablelabel">Issue Date</td>
                                <td class="tabletext"><%=issueDate%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel"><%=completionDateLabel%></td>
                                <td class="tabletext"><%=completionDate%></td>
                                <td class="tablelabel"><%=expirationDateLabel%></td>
                                <td class="tabletext"><%=expirationDate%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Address</td>
                                <% if(!(lsoAddress.trim().equalsIgnoreCase("1   Pw Citywide"))){ %>
                                      <td class="tabletext"><%=address%></td>
                                <%} else {%>
                                      <td class="tabletext">&nbsp;</td>
                                <%} %>
                                <td class="tablelabel">Valuation</td>
                                <td class="tabletext"><%=valuation%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Microfilm</td>
                                <td class="tabletext"><%=microfilm%></td>
                                <td class="tablelabel">Created By</td>
                                <td class="tabletext"><%=createdBy%></td>
                            </tr>

                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
           <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
        <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <%if(!(activityTypeDesc.equalsIgnoreCase(Constants.GARAGE_SALE_DESC))){ %>
            <tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="98%" class="tabletitle">Finance Manager</td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                                <td class="tablelabel"><img src="../images/spacer.gif" width="1" height="1"></td>
                                <td class="tablelabel">
                                <div align="right">Activity</div>
                                </td>
                                <td class="tablelabel">
                                <div align="right">Plan Check</div>
                                </td>
                                <td class="tablelabel">
                                <div align="right">Business Tax</div>
                                </td>
                                <td class="tablelabel">
                                <div align="right">Total</div>
                                </td>
                            </tr>
                            <tr>
                                <td class="tablelabel">
									Fees
                                </td>

                                <td class="tabletext">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="permitFeeAmt"/></div>
                                </td>
                                <td class="tabletext">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="planCheckFeeAmt"/></div>
                                </td>
                                <td class="tabletext">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="businessTaxFeeAmt"/></div>
                                </td>
                                <td class="tabletext">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="totalFeeAmt"/></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="tablelabel">

								     Payment</td>

                                <td class="tabletext">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="permitPaidAmt"/></div>
                                </td>
                                <td class="tabletext">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="planCheckPaidAmt"/></div>
                                </td>
                                <td class="tabletext">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="businessTaxPaidAmt"/></div>
                                </td>
                                <td class="tabletext">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="totalPaidAmt"/></div>
                                </td>
                            </tr>
                        
                            <tr>
                                <td class="tablelabel">Balance Due </td>
                                <td class="tabletext">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="permitAmountDue"/></div>
                                </td>
                                <td class="tabletext">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="planCheckAmountDue"/></div>
                                </td>
                                <td class="tabletext">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="businessTaxAmountDue"/></div>
                                </td>
                                <td class="tabletext">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="totalAmountDue"/></div>
                                </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <%} %>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            
            <!-- Questionaire Block Start -->
             <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                 
                 
            		<%if(questionList.size()>0){ %>
            		   <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
								<td class="tabletitle"><nobr>Online Questionaire</nobr></td>
                                
                            </tr>
                        </table>
                        </td>
                    </tr>                    
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr valign="top">
                                <td class="tablelabel">ID</td>
                                <td class="tablelabel" >Question</td>
                                <td class="tablelabel">Answer</td>
                               
                            </tr>                            
						<%
							for(int q=0;q<questionList.size();q++){
							elms.control.beans.online.ApplyOnlinePermitForm a = (elms.control.beans.online.ApplyOnlinePermitForm) questionList.get(q);
							String imgsrc ="../images/unchecked.png";
							if(a.getTempQuestionId().equals("Y")){
								imgsrc ="../images/check.png";
							}
							
						 %>
                            <tr valign="top" >
                                <td class="tabletext"> <%=a.getTempOnlineID()%> </td>
                                <td class="tabletext"> <%=a.getQuestionaireDescription()%> </td>
                                <td class="tabletext"> <img src="<%=imgsrc%>"> </td>
                               
                            </tr>
						<%} %>
                           
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
             	<%}%>
            <!-- Questionaire Block End -->

	 

		
                            </tr>
                        </table>
                        </td>
                    </tr>
                  
                   
                </td>
            </tr>
                            <tr>
<td><br></td>
<td><br></td>
                            </tr>

            </table>
</html:form>
</body>
</html:html>
