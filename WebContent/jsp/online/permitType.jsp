<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="org.owasp.encoder.Encode"%>
<%@ page import="elms.app.admin.*, java.util.*,elms.Operator"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%

String contextRoot = request.getContextPath();
List<ActivitySubType> comboNames = (List<ActivitySubType>)request.getAttribute("comboNames");

java.util.List streetList = new elms.agent.AddressAgent().getStreetArrayList();
pageContext.setAttribute("streetList", streetList);

int tempOnlineID = Operator.toInt((String) request.getParameter("tempOnlineID") != null ? (String) request.getParameter("tempOnlineID") : "0");

int useMapId = Operator.toInt((String) request.getParameter("useMapId") != null ? (String) request.getParameter("useMapId") : "0");


%>

<%@page import="elms.Operator"%>
<%@page import="elms.util.StringUtils"%><html:html>
<html:base/>
<head>
<link rel="stylesheet" href="../css/elms.css" type="text/css">
<link rel="stylesheet" href="../css/online.css" type="text/css">
<link rel="stylesheet" href="<%=contextRoot%>/jsp/script/jquery-ui-1.8.2.custom/development-bundle/themes/base/jquery.ui.all.css">
<script src="<%=contextRoot%>/jsp/script/jquery-ui-1.8.2.custom/js/jquery-1.6.4.min.js" type="text/javascript"></script>	
<script language="javascript">

function getCheckedValue(radioObj) {
	if(!radioObj)
		return "";
	var radioLength = radioObj.length;
	if(radioLength == undefined)
		if(radioObj.checked)
			return radioObj.value;
		else
			return "";
	for(var i = 0; i < radioLength; i++) {
		if(radioObj[i].checked) {
			return radioObj[i].value;
		}
	}
	return "";
}


function selectPermit()
{
	var feeIds ="";
	$("input[name='fee_name']:checked").each(function(){
	        feeIds +=$(this).val() +"|";
	});
	
    var useMapId = getCheckedValue(document.forms[0].elements['useMapId']);
	if(
			(document.forms[0].useMapId[0].checked == false)
			&&
			(document.forms[0].useMapId[1].checked == false)
			&&
			(document.forms[0].useMapId[2].checked == false)
			&&
			(document.forms[0].useMapId[3].checked == false)
			&&
			(document.forms[0].useMapId[4].checked == false)
			&&
			(document.forms[0].useMapId[5].checked == false)
			&&
			(document.forms[0].useMapId[6].checked == false)
			&&
			(document.forms[0].useMapId[7].checked == false)
			&&
			(document.forms[0].useMapId[8].checked == false)
			&&
			(document.forms[0].useMapId[9].checked == false)
		)
	{
		alert("Please select a Permit");
	} else if(feeIds==''){
		alert("Please select a Permit type");
		return false;
	}else
	{
	   var tempOnlineID = document.forms[0].tempOnlineID.value;
	   document.forms[0].action='<%=contextRoot%>/permitType.do?tempOnlineID='+tempOnlineID+'&useMapId='+useMapId+'&feeIds='+feeIds;
	   document.forms[0].submit();
	}
}



$(function(){
	$(document).ready(function() {
		if($('#usemp').val() >0){
			showfee($('#usemp').val(),0);
		}
		
	 });
	
});


function showfee(useId,fb){
	$.post("actions.jsp?EXECUTE=LOOKUPFEE&ID="+$('#tempid').val()+"&usemapid="+useId+"&fb="+fb+"", function(data){
		if(data.length>0) {
			$('.usemapid').empty();
			$('.usemapid').hide();
			$('#usemapid'+useId).append(data);
			$('#usemapid'+useId).show();
			$('#usemp').val(useId);
		}
	});
	
}

</script>
</head>
<body text="#000000" leftmargin="5" bottommargin="0"  scroll="yes" topmargin="0" marginwidth="0" marginheight="40" rightmargin="0" >


<center>

<html:form action="/permitType" >
<html:hidden property="tempOnlineID" /> 
<input type="hidden" id="tempid" name="tempid" value="<%=tempOnlineID %>">
<input type="hidden" id="usemp" name="usemp" value="<%=useMapId %>">

<table align="center" cellpadding="0" cellspacing="0" style="height:100%" width="100%" >

	<td align="center" valign="top" width="99%">
	<br><br> <br><br>
<fieldset style="width: 450px;height: 830px" >
<br><br>



<table cellpadding="15" cellspacing="0" width="100%">
 <html:errors />
  <tr>
        <td align="left"> <font class="panelVisited" style="font-size:30px"> 3<font class="panelVisited" style="font-size:12px"><bean:message key="permitInfo"/>
        </td>
   </tr>


	<tr>
		<td align ="left">
			<table>
				<%
				for(int j=0;j<comboNames.size();j++){
					ActivitySubType activitySubType = comboNames.get(j);
					String id = ""+activitySubType.getId();
					String description = activitySubType.getDescription();
					String label = StringUtils.nullReplaceWithEmpty(activitySubType.getLabel());
				%>
		       		<tr>
		       			<td class="FireResource">
		       			<html:radio property="useMapId" value="<%=id %>" onclick="showfee(this.value,1);" ><font class="FireResourceBold"><%=description %></font></html:radio>
		       			<br><%=label %>
		       			 <div style="padding-left:50px; " id="usemapid<%=id%>" class="usemapid"><br>
		       		
		       			</div>
		       			</td>
		       		</tr>
		       		
				<%	
				}
				%>
			</table>
        </td>
    </tr>


            <tr>
                    <td class="FireSelect">
                    
                    <div align="center">
                    <html:reset  value="Back" styleClass="FireSelect" style="width: 100px" onclick="history.back();"/>
                    <html:button  property="button" value="Next" styleClass="FireSelect" style="width: 100px" onclick="selectPermit()"/>
                      </div>
                </tr>
</table>
</fieldset>
</html:form>
</center>
	</td>

			<jsp:include page="onlineSideBar.jsp" flush="true">
				<jsp:param name="page" value="3" />
			</jsp:include>

 </table>

</html:html>

