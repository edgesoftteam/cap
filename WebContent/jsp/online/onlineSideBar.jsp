<!--Display of current entered data for permits-->

<%@page import="org.owasp.encoder.Encode"%>
<%
    java.util.List tempOnlineDetails = new java.util.ArrayList();
    tempOnlineDetails= (java.util.List)request.getAttribute("tempOnlineDetails");
    int intPage = Integer.parseInt(request.getParameter("page"));
%>

<td valign="top" width="1%" nowrap align="right" >

 	<table style="width:200px; height:100%" cellpadding="0" cellspacing="0" bgcolor="#9FADBA" background="../online/images/panelback.png" align="right">

   <%
	 for(int i=0;i<tempOnlineDetails.size();i++)
	 {
	 elms.control.beans.online.ApplyOnlinePermitForm applyOnlinePermitForm = (elms.control.beans.online.ApplyOnlinePermitForm)tempOnlineDetails.get(i);

	 %>

    <tr>
        <td valign="top">


            <table style="width:200px" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="8" class="<%if(intPage >1) {out.print("panelVisitedColor");} else if(intPage==1){out.print("panelActiveColor");}else {out.print("panelColor");}%>"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td class="<%if(intPage >1) {out.print("panelVisited");} else if(intPage==1){out.print("panelActive");}else {out.print("panel");}%>" style="padding:8px">
                        <font style="font-size:18px">1</font> - ADDRESS<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="images/bullet.gif" height="9" width="9"></td>
                                <td class="<%if(intPage >1) {out.print("panelVisited");} else if(intPage==1){out.print("panelActive");}else {out.print("panel");}%>"><%=elms.util.StringUtils.nullReplaceWithEmpty(applyOnlinePermitForm.getAddress())%></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="8" class="<%if(intPage >1) {out.print("panelVisitedColor");} else if(intPage==1){out.print("panelActiveColor");}else {out.print("panelColor");}%>"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td bgcolor="#FFFFFF"><img src="space.gif" width="0" height="1"></td>
                </tr>


                <tr>
                    <td width="8" class="<%if(intPage >2) {out.print("panelVisitedColor");} else if(intPage==2){out.print("panelActiveColor");}else {out.print("panelColor");}%>"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td class="<%if(intPage >2) {out.print("panelVisited");} else if(intPage==2){out.print("panelActive");}else {out.print("panel");}%>" style="padding:8px">
                        <font style="font-size:18px">2</font> - STRUCTURE<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="images/bullet.gif" height="9" width="9"></td>
                                <td class="<%if(intPage >2) {out.print("panelVisited");} else if(intPage==2){out.print("panelActive");}else {out.print("panel");}%>"><%=elms.util.StringUtils.nullReplaceWithEmpty(applyOnlinePermitForm.getStructureName())%></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="8" class="<%if(intPage >2) {out.print("panelVisitedColor");} else if(intPage==2){out.print("panelActiveColor");}else {out.print("panelColor");}%>"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td bgcolor="#FFFFFF"><img src="space.gif" width="0" height="1"></td>
                </tr>



                <tr>
                    <td width="8" class="<%if(intPage >3) {out.print("panelVisitedColor");} else if(intPage==3){out.print("panelActiveColor");}else {out.print("panelColor");}%>"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td class="<%if(intPage >3) {out.print("panelVisited");} else if(intPage==3){out.print("panelActive");}else {out.print("panel");}%>" style="padding:8px">
                        <font style="font-size:18px">3</font> - PERMIT TYPE<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="images/bullet.gif" height="9" width="9"></td>
                                <td class="<%if(intPage >3) {out.print("panelVisited");} else if(intPage==3){out.print("panelActive");}else {out.print("panel");}%>"><%=elms.util.StringUtils.nullReplaceWithEmpty(applyOnlinePermitForm.getSubProjectName())%></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="8" class="<%if(intPage >3) {out.print("panelVisitedColor");} else if(intPage==3){out.print("panelActiveColor");}else {out.print("panelColor");}%>"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td bgcolor="#FFFFFF"><img src="space.gif" width="0" height="1"></td>
                </tr>

				 <tr>
                    <td width="8" class="<%if(intPage >4) {out.print("panelVisitedColor");} else if(intPage==4){out.print("panelActiveColor");}else {out.print("panelColor");}%>"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td class="<%if(intPage >4) {out.print("panelVisited");} else if(intPage==4){out.print("panelActive");}else {out.print("panel");}%>" style="padding:8px">
                        <font style="font-size:18px">4</font> - VALUATION<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="images/bullet.gif" height="9" width="9"></td>
                                <td class="<%if(intPage >4) {out.print("panelVisited");} else if(intPage==4){out.print("panelActive");}else {out.print("panel");}%>"><%=elms.util.StringUtils.nullReplaceWithEmpty(applyOnlinePermitForm.getValuation())%></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="8" class="<%if(intPage >4) {out.print("panelVisitedColor");} else if(intPage==4){out.print("panelActiveColor");}else {out.print("panelColor");}%>"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td bgcolor="#FFFFFF"><img src="space.gif" width="0" height="1"></td>
                </tr>


				 <tr>
                    <td width="8" class="<%if(intPage >5) {out.print("panelVisitedColor");} else if(intPage==5){out.print("panelActiveColor");}else {out.print("panelColor");}%>"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td class="<%if(intPage >5) {out.print("panelVisited");} else if(intPage==5){out.print("panelActive");}else {out.print("panel");}%>" style="padding:8px">
                        <font style="font-size:18px">5</font> - CONTRACTOR<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="images/bullet.gif" height="9" width="9"></td>
                                <td class="<%if(intPage >5) {out.print("panelVisited");} else if(intPage==5){out.print("panelActive");}else {out.print("panel");}%>"><%=elms.util.StringUtils.nullReplaceWithEmpty(applyOnlinePermitForm.getContractorName())%></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="8" class="<%if(intPage >5) {out.print("panelVisitedColor");} else if(intPage==5){out.print("panelActiveColor");}else {out.print("panelColor");}%>"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td bgcolor="#FFFFFF"><img src="space.gif" width="0" height="1"></td>
                </tr>


				 <tr>
                    <td width="8" class="<%if(intPage >6) {out.print("panelVisitedColor");} else if(intPage==6){out.print("panelActiveColor");}else {out.print("panelColor");}%>"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td class="<%if(intPage >6) {out.print("panelVisited");} else if(intPage==6){out.print("panelActive");}else {out.print("panel");}%>" style="padding:8px">
                        <font style="font-size:18px">6</font> - OWNER<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="images/bullet.gif" height="9" width="9"></td>
                                <td class="<%if(intPage >6) {out.print("panelVisited");} else if(intPage==6){out.print("panelActive");}else {out.print("panel");}%>"><%=elms.util.StringUtils.nullReplaceWithEmpty(applyOnlinePermitForm.getOwnerName())%></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="8" class="<%if(intPage >6) {out.print("panelVisitedColor");} else if(intPage==6){out.print("panelActiveColor");}else {out.print("panelColor");}%>"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td bgcolor="#FFFFFF"><img src="space.gif" width="0" height="1"></td>
                </tr>
                
                

                <tr>
                    <td width="8" class="<%if(intPage >7) {out.print("panelVisitedColor");} else if(intPage==7){out.print("panelActiveColor");}else {out.print("panelColor");}%>"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td class="<%if(intPage >7) {out.print("panelVisited");} else if(intPage==7){out.print("panelActive");}else {out.print("panel");}%>" style="padding:8px">
                        <font style="font-size:18px">7</font> - QUESTIONAIRE<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="images/bullet.gif" height="9" width="9"></td>

                            </tr>
                        </table>
                    </td>
                </tr>
                 <tr>
                    <td width="8" class="<%if(intPage >8) {out.print("panelVisitedColor");} else if(intPage==8){out.print("panelActiveColor");}else {out.print("panelColor");}%>"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td bgcolor="#FFFFFF"><img src="space.gif" width="0" height="1"></td>
                </tr>

                  <tr>
                    <td width="8" class="<%if(intPage >8) {out.print("panelVisitedColor");} else if(intPage==8){out.print("panelActiveColor");}else {out.print("panelColor");}%>"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td class="<%if(intPage >8) {out.print("panelVisited");} else if(intPage==8){out.print("panelActive");}else {out.print("panel");}%>" style="padding:8px">
                        <font style="font-size:18px">8</font> - COMPLETE / PAY<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="images/bullet.gif" height="9" width="9"></td>
                                <td class="<%if(intPage >8) {out.print("panelVisited");} else if(intPage==8){out.print("panelActive");}else {out.print("panel");}%>"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                
                
  

 				 <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" bgcolor="#FFFFFF"><img src="space.gif" width="1" height="0"></td>
                    <td bgcolor="#FFFFFF"><img src="space.gif" width="0" height="1"></td>
                </tr>






            </table>
        </td>
    </tr>
<%}%>
</table>





 </td>

<!--End Display of current entered data for permits-->