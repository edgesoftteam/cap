<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%
 	String contextRoot = request.getContextPath();

 	 java.util.List ownerList = new java.util.ArrayList();
     ownerList= (java.util.List)request.getAttribute("ownerList");
     int size = ownerList.size();

  	//left side details bar display
    java.util.List tempOnlineDetails = new java.util.ArrayList();
    tempOnlineDetails= (java.util.List)request.getAttribute("tempOnlineDetails");
	int peopleTypeId= elms.util.StringUtils.s2i((String)request.getAttribute("peopleTypeId"));
 	String ownerBuilder= (String)request.getAttribute("ownerBuilder")!=null?(String)request.getAttribute("ownerBuilder"):"";
%>
<html:html>
<head>
<%if(ownerBuilder.equals("true")){%>
<script language="javascript">

function getSubmit()
{
	 if(document.forms[0].chkExistPeopleFlag.checked==false)
	 {
	 alert("Please select the correct option to proceed.");

	 }else
	 {
	var peopleType= "W";
	 var tempOnlineID = document.forms[0].tempOnlineID.value;

	document.forms[0].action='<%=contextRoot%>/checkExistingOwner.do?tempOnlineID='+tempOnlineID+'&peopleType='+peopleType;
	document.forms[0].submit();
	}
}
</script>
<%}else if(peopleTypeId==8){%>
<script language="javascript">

function getSubmit()
{
var flag = false;
<%int i=0;
for(i=0;i<size;i++){%>
if(eval("document.forms[0].chkExistPeopleFlag["+<%=i%>+"].checked")==true){
	flag = true;
}
<%}%>
if(eval("document.forms[0].chkExistPeopleFlag["+<%=i%>+"].checked")==true){
	flag = true;
}
<%if(peopleTypeId==8){%>
if(eval("document.forms[0].chkExistPeopleFlag["+<%=i+1%>+"].checked")==true){
	flag = true;
}
<%}%>
	 if(flag == false)
	 {
	 alert("Please select the correct option to proceed.");

	 }else
	 {
	var peopleType= "W";
	 var tempOnlineID = document.forms[0].tempOnlineID.value;

	document.forms[0].action='<%=contextRoot%>/checkExistingOwner.do?tempOnlineID='+tempOnlineID+'&peopleType='+peopleType;
	document.forms[0].submit();
	}
}
</script>

<%}else{%>
<script language="javascript">

function getSubmit()
{

	 if(document.forms[0].chkExistPeopleFlag.checked==false)
	 {
      alert("Please select the correct option to proceed.");
      return false;
	 }else

	 {
	var peopleType= "W";
	 var tempOnlineID = document.forms[0].tempOnlineID.value;

	document.forms[0].action='<%=contextRoot%>/checkExistingOwner.do?tempOnlineID='+tempOnlineID+'&peopleType='+peopleType;
	document.forms[0].submit();
	}
}

</script>
<%}%>
<%if(ownerBuilder.equals("true")){%>
<script language="javascript">

function enableButton()
{
	 if(document.forms[0].termsCondition.checked == true && document.forms[0].print.checked == true){
	 	document.forms[0].button.disabled = false;
	 }else{
	 	document.forms[0].button.disabled = true;
	 }
}

function disableButton(){
	document.forms[0].button.disabled = true;
	document.forms[0].termsCondition.checked = false;
	document.forms[0].print.checked = false;
}
</script>
</head>
<body text="#000000" leftmargin="5" bottommargin="0"  scroll="yes" topmargin="0" marginwidth="0" marginheight="40" rightmargin="0" onLoad="disableButton()">
<%}else{%>
</head>
<body text="#000000" leftmargin="5" bottommargin="0"  scroll="yes" topmargin="0" marginwidth="0" marginheight="40" rightmargin="0">
<%}%>
<%--
//Scheme used to identify  Http , https or FTP

 //   if (request.getScheme().equalsIgnoreCase("HTTP") ) {
{      String  srvname = request.getServerName();
      String    scrname = request.getQueryString();
        response.sendRedirect("https://"+srvname);
  --%>

<html:base/>
<%

    java.util.List streetList = new elms.agent.AddressAgent().getStreetArrayList();
  	pageContext.setAttribute("streetList", streetList);
%>

<center>
<style type="text/css">
    .FireTitle          { FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  td.FireHeading        { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none; PADDING: 10px; BACKGROUND-COLOR:#D9D9D9 }
    .FireHeading        { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireGlance         { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  td.FireResource       { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none; PADDING: 6px }
    .FireResource       { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
  td.FireResourceStatic { FONT-SIZE: 12px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none; PADDING: 6px }
    .FireResourceStatic { FONT-SIZE: 12px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireSelect         { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireText           { FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireHighlight      { FONT-SIZE: 15px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #990000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireMessage        { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
  td.FireMenu           { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none; PADDING: 2px; BACKGROUND-COLOR:#000000 }
    .FireMenu           { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:link      { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:visited   { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:active    { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:hover     { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireMenu1          { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:link     { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:visited  { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:active   { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:hover    { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #FFFFFF; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  @page Section1 {
                   size:8.5in 11.0in;
                   margin:0.5in 0.5in 0.5in 0.5in;
                 }
 p.MsoNormal, li.MsoNormal, div.MsoNormal {
                margin:0in;
                margin-bottom:.0001pt;
                font-size:12.0pt;
                font-family:"Arial"
              }
 table.atable {
                border-collapse:collapse;
                border:1px solid #B3B3B3
              }
 tr           {
                page-break-inside:avoid
              }
 div.Section1 {
                page:Section1;
              }


.panelVisited      { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #003265 }
.panelVisitedColor { background-color:#62A85F }
.panelActive       { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #FFFFFF }
.panelActiveColor  { background-color:#EAC563 }
.panel             { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #FFFFFF }
.panelColor        { background-color:#BB2F2F }

</style>


<html:form action="checkExistingOwner">
<html:hidden property="tempOnlineID" />
<table align="center" cellpadding="0" cellspacing="0" style="height:100%" width="100%">

	<td align="center" valign="top" width="99%">
	<br><br> <br><br>
<fieldset style="width: 450px;height: 320px" >

<br> <br>

<table cellpadding="15" cellspacing="0" width="100%">
 <html:errors />

   <tr>
        <td align="left"> <font class="panelVisited" style="font-size:30px"> 6<font class="panelVisited" style="font-size:12px"><bean:message key="isOwner"/>
        </td>
   </tr>

 <!-- adding now here -->
 <%if(!ownerBuilder.equals("true")){%>
 		 <%
 		for(int i=0;i<ownerList.size();i++)
						{
					 		elms.control.beans.online.ApplyOnlinePermitForm applyOnlinePermitForm = (elms.control.beans.online.ApplyOnlinePermitForm) ownerList.get(i);
                  %>
                            <tr> <td class="tabletext" valign="top" colspan="3"><font class="panelVisited" style="font-size:12px">&nbsp;&nbsp;&nbsp;&nbsp;
                           <%
                           String  checkOwnerOrPeople = applyOnlinePermitForm.getCheckOwnerOrPeople();
                           %>
                           <input type="radio" name="chkExistPeopleFlag" value="<%=elms.util.StringUtils.i2s(applyOnlinePermitForm.getPeopleId())%>">  <%=applyOnlinePermitForm.getAgentName()%>&nbsp; ( is the Owner )

                           </td>
	               			<html:hidden property="checkOwnerOrPeople" value="<%=applyOnlinePermitForm.getCheckOwnerOrPeople()%>"/>
	                        <%
                  			  }
                    		%>
                    		 <tr>
    <tr>

    	<td class="tabletext" valign="top" colspan="3"><font class="panelVisited" style="font-size:12px">&nbsp;&nbsp;&nbsp;&nbsp;
    	 <html:radio property="chkExistPeopleFlag" value="Y" /> Other </td>

    </tr>
<%}%>
		<%if(peopleTypeId==8){%>
	 <tr>
    <td class="tabletext" valign="top" colspan="3"><font class="panelVisited" style="font-size:12px">&nbsp;&nbsp;&nbsp;&nbsp;
     <html:radio property="chkExistPeopleFlag" value="S" /> Self  ( I am Owner )</td>
	</tr>
	<%}%>



<%if(ownerBuilder.equals("true")){%>
	<tr>
    <td class="tabletext" valign="top" colspan="3"><html:checkbox property="termsCondition" onclick="enableButton();"/><font color="red" style="font-family: Arial; font-size:11px">&nbsp;&nbsp;
    I hereby affirm under penalty of perjury that I am exempt from the Contractors License Law and that A) I, as owner of the property, or my employees with wages as their sole compensation, will do the work, and the structure is not intended or offered for sale; or B) I, as owner of the property, am exclusively contracting with licensed contractors to construct the project.
    </td>
	</tr>
	<tr>
    <td class="tabletext" valign="top" colspan="3"><html:checkbox property="print" onclick="enableButton();"/><font color="red" style="font-family: Arial; font-size:11px">&nbsp;&nbsp;
    I hereby acknowledge that as 'owner-builder' I am the responsible party of record for all construction approved under this permit.  By acknowledging this as an owner-builder, I have stated that I will be personally performing all work, or if my work is being performed by someone other than myself, those individuals must be licensed contractors or subcontractors with a current and valid license from the State of California, City of Burbank business license, and workers' compensation insurance.  If I employ unlicensed individuals I may be considered an employer and I may be responsible for workers' compensation insurance and other financial obligations specified by the state and federal government.
    </td>
	</tr>
<%}%>

	<tr>

                    <td class="FireSelect">
                    <div align="center">
                    <html:reset  value="Back" styleClass="FireSelect" style="width: 100px" onclick="history.back();"/>
                      <html:button  property="button" value="Next" styleClass="FireSelect" style="width: 100px" onclick="getSubmit()"/>
                      </div>
                </tr>
</table>
</fieldset>
</html:form>
</center>

	</td>

<!--Display of current entered data for permits-->
<td valign="top" width="1%" nowrap align="right">

 	<table style="width:200px; height:100%" cellpadding="0" cellspacing="0" bgcolor="#9FADBA" background="/elms/jsp/online/images/panelback.png" align="right">
   <%
	 for(int i=0;i<tempOnlineDetails.size();i++)
	 {
	 elms.control.beans.online.ApplyOnlinePermitForm applyOnlinePermitForm = (elms.control.beans.online.ApplyOnlinePermitForm)tempOnlineDetails.get(i);

	 %>
    <tr>
        <td valign="top">


            <table style="width:200px" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="8" class="panelVisitedColor"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="panelVisited" style="padding:8px">
                        <font style="font-size:18px">1 - ADDRESS<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="/elms/jsp/online/images/bullet.gif" height="9" width="9"></td>
                                <td class="panelVisited"><%=elms.util.StringUtils.nullReplaceWithEmpty(applyOnlinePermitForm.getAddress())%></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="8" class="panelVisitedColor"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="tabletext"><img src="space.gif" width="0" height="1"></td>
                </tr>


                <tr>
                    <td width="8" class="panelVisitedColor"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="panelVisited" style="padding:8px">
                        <font style="font-size:18px">2 - STRUCTURE<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="/elms/jsp/online/images/bullet.gif" height="9" width="9"></td>
                                <td class="panelVisited"><%=elms.util.StringUtils.nullReplaceWithEmpty(applyOnlinePermitForm.getStructureName())%></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="8" class="panelVisitedColor"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="tabletext"><img src="space.gif" width="0" height="1"></td>
                </tr>



                <tr>
                    <td width="8" class="panelVisitedColor"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="panelVisited" style="padding:8px">
                        <font style="font-size:18px">3 - PERMIT TYPE<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="/elms/jsp/online/images/bullet.gif" height="9" width="9"></td>
                                <td class="panelVisited"><%=elms.util.StringUtils.nullReplaceWithEmpty(applyOnlinePermitForm.getSubProjectName())%></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="8" class="panelVisitedColor"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="tabletext"><img src="space.gif" width="0" height="1"></td>
                </tr>

				 <tr>
                    <td width="8" class="panelVisitedColor"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="panelVisited" style="padding:8px">
                        <font style="font-size:18px">4 - VALUATION<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="/elms/jsp/online/images/bullet.gif" height="9" width="9"></td>
                                <td class="panelVisited"><%=elms.util.StringUtils.nullReplaceWithEmpty(applyOnlinePermitForm.getValuation())%></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="8" class="panelVisitedColor"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="tabletext"><img src="space.gif" width="0" height="1"></td>
                </tr>


				 <tr>
                    <td width="8" class="panelVisitedColor"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="panelVisited" style="padding:8px">
                        <font style="font-size:18px">5 - CONTRACTOR<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="/elms/jsp/online/images/bullet.gif" height="9" width="9"></td>
                                <td class="panelVisited"><%=elms.util.StringUtils.nullReplaceWithEmpty(applyOnlinePermitForm.getContractorName())%></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="8" class="panelVisitedColor"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="tabletext"><img src="space.gif" width="0" height="1"></td>
                </tr>


				 <tr>
                    <td width="8" class="panelActiveColor"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="panelActive" style="padding:8px">
                        <font style="font-size:18px">6 - OWNER<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="bullet.gif" height="9" width="9"></td>
                                <td class="panelActive"><%=elms.util.StringUtils.nullReplaceWithEmpty(applyOnlinePermitForm.getOwnerName())%></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="8" class="panelActiveColor"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="tabletext"><img src="space.gif" width="0" height="1"></td>
                </tr>

				 <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="panelActive" style="padding:8px">
                        <font style="font-size:18px">7 - PROJECT NAME<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="bullet.gif" height="9" width="9"></td>
                                <td class="panelActive"><%=elms.util.StringUtils.nullReplaceWithEmpty(applyOnlinePermitForm.getProjectName())%></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="tabletext"><img src="space.gif" width="0" height="1"></td>
                </tr>


                 <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="panelActive" style="padding:8px">
                        <font style="font-size:18px">8 - START DATE<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="bullet.gif" height="9" width="9"></td>

                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="tabletext"><img src="space.gif" width="0" height="1"></td>
                </tr>

                 <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="panelActive" style="padding:8px">
                        <font style="font-size:18px">9 - PLAN CHECK<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="bullet.gif" height="9" width="9"></td>

                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="tabletext"><img src="space.gif" width="0" height="1"></td>
                </tr>

                  <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="panelActive" style="padding:8px">
                        <font style="font-size:18px">10 - COMPLETE / PAY<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="bullet.gif" height="9" width="9"></td>
                                <td class="panelActive"></td>
                            </tr>
                        </table>
                    </td>
                </tr>

 				 <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="tabletext"><img src="space.gif" width="0" height="1"></td>
                </tr>





            </table>
        </td>
    </tr>
<%}%>
</table>
</body>
 </td>
 </table>
 	<!--End Display of current entered data for permits-->

</html:html>

