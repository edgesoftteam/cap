<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>

<html:html>
<head>
<html:base/>
<title>City of Burbank :Permitting and Licensing System: Search Results</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<% String contextRoot = request.getContextPath();%>
<script language="JavaScript" src="../script/calendar.js"></script>
<script>
function addPeople()
{
    document.location='<%=contextRoot%>/addPeople.do'
}

</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table width="100%" border="0" cellspacing="0" cellpadding="0">


  <tr>
    <td valign="top" colspan="2">
	<html:form name ="peopleSearchForm" type="elms.control.beans.PeopleSearchForm" action="">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><bean:write name="peopleSearchForm" property="lsoAddress" /><bean:write name="peopleSearchForm" property="psaInfo" /><br><br>
          </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="98%" background="../images/site_bg_B7C1CB.jpg">Search
                        Results</td>

                      <td width="1%"background="../images/site_bg_B7C1CB.jpg"><nobr>
                      	<html:button  property = "new" value="New" onclick="addPeople()" styleClass="button"/>
                        &nbsp;&nbsp;
						<html:reset  value="Cancel" styleClass="button" onclick="history.back();"/>
		    			</nobr>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr valign="top">
                      <td class="tablelabel"></td>
                      <td class="tablelabel">Name</td>
                      <td class="tablelabel">License</td>
                     <td class="tablelabel">Phone</td>
                      <td class="tablelabel">Type</td>
                    </tr>
                    <% int peopleCount = 0; %>
		    <logic:iterate id="people" name="peopleSearchForm" property="peopleList">
                    <% peopleCount++; %>
                    <tr class="tabletext">
                      <td>
                        <%= peopleCount %>
                      </td>
                      <td>
                      	<a href='<%=contextRoot%>/editPeople.do?peopleId=<bean:write name="people" property="peopleId" />&psaId=<bean:write name="peopleSearchForm" property="psaId" />&psaType=<bean:write name="peopleSearchForm" property="psaType" />'>
                      	  <logic:equal name="people" property="onHold" value="Y"><font class="red1"></logic:equal><bean:write name="people" property="name" />
                      	</a>
                      </td>
                      <td>
                        <bean:write name="people" property="licenseNbr" />
                      </td>
                      <td>
                        <bean:write name="people" property="phoneNbr" />
                      </td>
                      <td>
                        <bean:write name="people" property="peopleType.type" />
                      </td>
                    </tr>
                    </logic:iterate>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<html:hidden name = "peopleForm" property="people.peopleType.code" />
</html:form>
</body>
</html:html>

