<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="org.owasp.encoder.Encode"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ page import="elms.security.*,elms.common.*"%>
<%
session.setAttribute("fromOnline",new String("fromOnline"));
String contextRoot = request.getContextPath();
String appId = request.getParameter("appId");
%>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<SCRIPT language="JavaScript">

function validateEmail()
{
var email= document.forms['onlineRegisterFrom'].elements['emailAddress'].value;

//  a very simple email validation checking.
//  you can add more complex email checking if it helps
    var splitted = email.match("^(.+)@(.+)$");    if(splitted == null) return false;
    if(splitted[1] != null )
    {
      var regexp_user=/^\"?[\w-_\.]*\"?$/;
      if(splitted[1].match(regexp_user) == null) return false;
    }
    if(splitted[2] != null)
    {
      var regexp_domain=/^[\w-\.]*\.[A-Za-z]{2,4}$/;
      if(splitted[2].match(regexp_domain) == null)
      {
	    var regexp_ip =/^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
	    if(splitted[2].match(regexp_ip) == null) return false;
      }// if

      return true;
    }

return false;
}

function getMoreDetails()
{
	if(document.forms[0].firstName.value==""){
		alert("Please enter your First Name");
		document.forms[0].firstName.focus();
		return false;
	}else if(document.forms[0].lastName.value==""){
		alert("Please enter your Last Name");
		document.forms[0].lastName.focus();
		return false;
	}else if(document.forms[0].address.value==""){
		alert("Please enter an Address");
		document.forms[0].address.focus();
		return false;
	}else if(document.forms[0].city.value==""){
		alert("Please enter a City");
		document.forms[0].city.focus();
		return false;
	}else if(document.forms[0].state.value==""){
		alert("Please enter a State");
		document.forms[0].state.focus();
		return false;
	}else if(document.forms[0].zip.value==""){
		alert("Please enter a Zip Code");
		document.forms[0].zip.focus();
		return false;
	}else if(document.forms[0].emailAddress.value==""){
		alert("Please enter an E-mail Address ");
		document.forms[0].emailAddress.focus();
		return false;
	}else if(document.forms[0].pwd.value==""){
		alert("Please enter a Password");
		document.forms[0].pwd.focus();
		return false;
	}else if(document.forms[0].confirmPwd.value==""){
		alert("Please reenter the Password");
		document.forms[0].confirmPwd.focus();
		return false;
	}else if(document.forms[0].phoneNbr.value==""){
		alert("Please enter a Phone Number");
		document.forms[0].phoneNbr.focus();
		return false;
	}else if(document.forms[0].pwd.value.length < 6){
		alert("Passwords must be at least 6 alphanumeric characters long.");
		document.forms[0].pwd.focus();
		return false;
	}

	if(document.forms[0].emailAddress.value != ""){
		if(!validateEmail(document.forms[0].emailAddress.value)) {
	          strValue=false;
	        alert('Please enter a valid E-mail Address');
	        document.forms[0].emailAddress.focus();
	        return false;
	    }
	}

	if(document.forms[0].pwd.value != document.forms[0].confirmPwd.value){
		alert("Confirm Password does not match your Password");
		document.forms[0].pwd.focus();
		document.forms[0].pwd.value="";
		document.forms[0].confirmPwd.value="";
		return false;
	}
	document.forms[0].action = "<%=contextRoot%>/onlineRegisterDetails.do?appId=<%=appId%>";
	document.forms[0].submit();
}

function DisplayHyphen(str)
{

	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46))
	{
		event.returnValue = false;
	}
	else
	{
		if ((document.onlineRegisterFrom.elements[str].value.length == 3 ) || (document.onlineRegisterFrom.elements[str].value.length == 7 ))
		{
			document.onlineRegisterFrom.elements[str].value = document.onlineRegisterFrom.elements[str].value +'-';
 		}
 		if (document.onlineRegisterFrom.elements[str].value.length > 11 )  event.returnValue = false;
	}
}
function check(){

var carCode= event.keyCode;
 if ((carCode < 48) || (carCode > 57)){
          event.returnValue = false;
  }

}
</SCRIPT>

</head>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<%
	String error = (String)request.getAttribute("error");
	if(error==null) error="";
%>

<center>
<style type="text/css">
    .FireTitle          { FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  td.FireHeading        { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none; PADDING: 10px; BACKGROUND-COLOR:#D9D9D9 }
    .FireHeading        { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireGlance         { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  td.FireResource       { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none; PADDING: 6px }
    .FireResource       { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
  td.FireResourceStatic { FONT-SIZE: 12px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none; PADDING: 6px }
    .FireResourceStatic { FONT-SIZE: 12px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireSelect         { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireText           { FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireHighlight      { FONT-SIZE: 15px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #990000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireMessage        { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
  td.FireMenu           { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none; PADDING: 2px; BACKGROUND-COLOR:#000000 }
    .FireMenu           { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:link      { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:visited   { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:active    { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:hover     { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireMenu1          { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:link     { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:visited  { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:active   { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:hover    { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #FFFFFF; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  @page Section1 {
                   size:8.5in 11.0in;
                   margin:0.5in 0.5in 0.5in 0.5in;
                 }
 p.MsoNormal, li.MsoNormal, div.MsoNormal {
                margin:0in;
                margin-bottom:.0001pt;
                font-size:12.0pt;
                font-family:"Arial"
              }
 table.atable {
                border-collapse:collapse;
                border:1px solid #B3B3B3
              }
 tr           {
                page-break-inside:avoid
              }
 div.Section1 {
                page:Section1;
              }
</style>
<br><br>
<html:form action="/onlineRegisterDetails">

<fieldset style="width: 400px">
  <legend class="FireResourceStatic">
     <table cellpadding="0" cellspacing="0">
            <tr>
              <td><img src="<%=contextRoot%>/jsp/online/images/<%=elms.agent.LookupAgent.getKeyValue("ONLINE_LOGO") %>" width="100" height="100"></td>
              <td class="panelVisited" style="color: #003265; font-size:16px; font-weight: bold">&nbsp;&nbsp;Account Registration</td>
               <td class="panelVisited"  align="right" style="color: #003265; font-size:12px; font-weight: bold">&nbsp;&nbsp;&nbsp;&nbsp;Step 1 of 2</td>
            </tr>
          </table>
          </legend>
  <table cellpadding="50" cellspacing="0" width="400px">
    <html:errors /><%=error%>
    <tr>
      <td align="center">


        <table cellpadding="5" cellspacing="0">
          <tr>
            <td class="FireResource">First Name <font color="red">*</td>
            <td class="FireSelect"><html:text  name = "onlineRegisterFrom"  property="firstName" size="35" styleClass="textbox"/></td>
          </tr>
          <tr>
            <td class="FireResource">Last Name <font color="red">*</td>
            <td class="FireSelect"><html:text  name = "onlineRegisterFrom"  property="lastName" size="35" styleClass="textbox"/></td>
          </tr>
          <tr>
            <td class="FireResource" nowrap>Company Name</td>
            <td class="FireSelect"><html:text  name = "onlineRegisterFrom"  property="companyName" size="35" styleClass="textbox"/></td>
          </tr>
          <tr>
            <td class="FireResource" nowrap>Address <font color="red">*</td>
            <td class="FireSelect"><html:text  name = "onlineRegisterFrom"  property="address" size="25" styleClass="textbox"/></td>
          </tr>
          <tr>
            <td class="FireResource">City <font color="red">*</td>
            <td class="FireSelect"><html:text  name = "onlineRegisterFrom"  property="city" size="25" styleClass="textbox" /></td>
          </tr>
			<tr>
            <td class="FireResource">State <font color="red">*</td>
            <td class="FireSelect"><html:text  name = "onlineRegisterFrom"  property="state" size="5" styleClass="textbox" maxlength="2"/>
			&nbsp;Zip <font color="red">*&nbsp;&nbsp;&nbsp;<html:text  name = "onlineRegisterFrom"  property="zip" size="15" styleClass="textbox" maxlength="9" onkeypress="check();"/></td>
          </tr>
          <tr>
            <td class="FireResource" nowrap>E-mail Address <font color="red">*</td>
            <td class="FireSelect"><html:text  name = "onlineRegisterFrom"  property="emailAddress" size="35" styleClass="textbox"/></td>
          </tr>
          <tr>
            <td class="FireResource">Password <font color="red">*</td>
            <td class="FireSelect"><html:password  name = "onlineRegisterFrom"  property="pwd" size="35" styleClass="textbox"/></td>
          </tr>
          <tr>
            <td class="FireResource" nowrap>Confirm Password <font color="red">*</td>
            <td class="FireSelect"><html:password  name = "onlineRegisterFrom"  property="confirmPwd" size="35" styleClass="textbox"/></td>
          </tr>
          <tr>
            <td class="FireResource">Phone Number <font color="red">*</td>
            <td class="FireSelect" nowrap><html:text  name = "onlineRegisterFrom"  property="phoneNbr" size="20" styleClass="textbox" onkeypress="return DisplayHyphen('phoneNbr');" maxlength="12"/>
            &nbsp;Ext&nbsp;&nbsp;<html:text  name = "onlineRegisterFrom"  property="phoneExt" size="5" styleClass="textbox" maxlength="4"/></td>
          </tr>
          <tr>
            <td class="FireResource">Work Phone</td>
            <td class="FireSelect" nowrap><html:text  name = "onlineRegisterFrom"  property="workPhone" size="20" styleClass="textbox" onkeypress="return DisplayHyphen('workPhone');" maxlength="12"/>
            &nbsp;Ext&nbsp;&nbsp;<html:text  name = "onlineRegisterFrom"  property="workExt" size="5" styleClass="textbox" maxlength="4"/></td>
          </tr>
          <tr>
            <td class="FireResource" nowrap>Fax Number</td>
            <td class="FireSelect"><html:text  name = "onlineRegisterFrom"  property="fax" size="20" styleClass="textbox" onkeypress="return DisplayHyphen('fax');" maxlength="12"/></td>
          </tr>

          <tr>
            <td class="FireResource" colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;</td>
          </tr>
            <tr>
            <td class="FireResource" colspan="2"><font color="red">Required fields are marked with an asterisk *</td>

          </tr>


        </table>
        <br><br>
        <table>

          <tr>
            <td class="FireSelect" align="center">
              <html:reset  value="Back" styleClass="FireSelect" style="width: 100px" onclick="history.back();"/>&nbsp;&nbsp;&nbsp;&nbsp;
              <html:button  property="Submit" value="Next" styleClass="FireSelect" style="width: 100px" onclick="getMoreDetails()"/>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</fieldset>



</html:form>
</center>
</html:html>
