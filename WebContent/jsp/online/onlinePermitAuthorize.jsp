<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%
 	String contextRoot = request.getContextPath();
 	//left side details bar display
    java.util.List tempOnlineDetails = new java.util.ArrayList();
    tempOnlineDetails= (java.util.List)request.getAttribute("tempOnlineDetails");
    int peopleTypeId= elms.util.StringUtils.s2i((String)request.getAttribute("peopleTypeId"));
 	String forcePC=(String)request.getAttribute("forcePC");
%>
<html:html>

<head>
<html:base/>
<link rel="stylesheet" href="../css/elms.css" type="text/css">
	<link rel="stylesheet" href="../css/online.css" type="text/css">

<%if(peopleTypeId==4){%>
<script language="javascript">
function finishProcess()
{

    var tempOnlineID = document.forms[0].tempOnlineID.value;
	document.forms[0].button.disabled=false;
	 document.forms[0].reset.disabled=true;
	document.forms[0].action='<%=contextRoot%>/onlinePermitAuthorize.do?viewPermit=Yes&tempOnlineID='+tempOnlineID+'&forcePC=<%=forcePC%>';
	document.forms[0].submit();
}

// function disableSubmit()
// {
//    document.forms[0].button[1].disabled=true;
//    document.forms[0].termsCondition.checked=false;
//    document.forms[0].comboNumber.checked=false;
//    document.forms[0].comboName.checked=false;
// }
// function enableSubmit()
// {
//     if(document.forms[0].comboNumber.checked==true )
//     {
// 	document.forms[0].button[1].disabled=false;
// 	}else
// 	{
// 	document.forms[0].button[1].disabled=true;
// 	}
// }

function cancelPermit(){
	document.forms[0].action='<%=contextRoot%>/jsp/online/welcomeOnline.jsp';
	document.forms[0].submit();
}

</script>
<%}else{%>
<script language="javascript">


function finishProcess()
{

    var tempOnlineID = document.forms[0].tempOnlineID.value;
     document.forms[0].reset.disabled=true;
	 document.forms[0].button.disabled=false;
	document.forms[0].action='<%=contextRoot%>/onlinePermitAuthorize.do?viewPermit=Yes&tempOnlineID='+tempOnlineID+'&forcePC=<%=forcePC%>';
	document.forms[0].submit();
}

// function disableSubmit()
// {
//    document.forms[0].button[1].disabled=true;
//    document.forms[0].termsCondition.checked=false;
//    document.forms[0].comboNumber.checked=false;
//    document.forms[0].comboName.checked=false;
// }


// function enableSubmit()
// {
//     if(document.forms[0].termsCondition.checked==true && document.forms[0].comboNumber.checked==true && document.forms[0].comboName.checked==true)
//     {
// 	document.forms[0].button[1].disabled=false;
// 	}else
// 	{
// 	document.forms[0].button[1].disabled=true;
// 	}
// }

function cancelPermit(){
	document.forms[0].action='<%=contextRoot%>/jsp/online/welcomeOnline.jsp';
	document.forms[0].submit();
}
</script>
<%}%>

<script>

function apply()
{
	document.forms[0].button[1].disabled=true;
	if(document.forms[0].comboNumber.checked==true)
	{
		document.forms[0].button[1].disabled=false;
	}
	if(document.forms[0].comboNumber.checked==false)
	{
		document.forms[0].button[1].enabled=false;
	}
}
</script>
</head>
<body text="#000000" leftmargin="5" bottommargin="0"  scroll="yes" topmargin="0" marginwidth="0" marginheight="40" rightmargin="0"  onload="disableSubmit();">

<html:base/>
<%

    java.util.List streetList = new elms.agent.AddressAgent().getStreetArrayList();
  	pageContext.setAttribute("streetList", streetList);
%>

<center>

<html:form action="checkExistingArchitect">
<html:hidden property="tempOnlineID" />
<table align="center" cellpadding="0" cellspacing="0" style="height:100%" width="100%">

	<td align="center" valign="top" width="99%" >
	<br><br> <br><br>
<fieldset style="width: 450px;height: 100%" >

<br><br>




<table cellpadding="15" cellspacing="0" width="100%">
 <html:errors />

 <tr>
        <td align="left"> <font class="panelVisited" style="font-size:30px"> 8<font class="panelVisited" style="font-size:12px"><bean:message key="completePay"/>
        </td>
   </tr>

	   <tr>
	        <td> <font class="panelVisited" style="font-size:12px"><bean:message key="onlineAuthorize"/></td>
	   </tr>
	    <tr>
	    <td class="tabletext" valign="top" colspan="3"><font color="red" style="font-family: Arial; font-size:11px"><font color="red" style="font-family: Arial; font-size:11px">&nbsp;&nbsp;
	    <bean:message key="onlineAuthorizeAck1"/>
	    </td>
		</tr>
		<tr>
	    <td class="tabletext" valign="top" colspan="3"><font color="red" style="font-family: Arial; font-size:11px"><font color="red" style="font-family: Arial; font-size:11px">&nbsp;&nbsp;
	    <bean:message key="onlineAuthorizeAck2"/>
	    </td>
		</tr>
		<tr>
	    <td class="tabletext" valign="top" colspan="3"><font color="red" style="font-family: Arial; font-size:11px"><font color="red" style="font-family: Arial; font-size:11px">&nbsp;&nbsp;
	    <bean:message key="onlineAuthorizeAck3"/>
	    </td>
		</tr>
		<tr>
	    <td class="tabletext" valign="top" colspan="3"><font color="red" style="font-family: Arial; font-size:11px">
		<center><u><b>NOTICE</b></u></center><b>
		The contractor and/or property owner will be notified by mail that this permit has been issued.  The notice will identify the individual to whom the permit was issued and the scope of work of the project.</b>
	    </td>
		</tr>
		
		<tr>
	    <td class="tabletext" valign="top" colspan="3"><font color="red" style="font-family: Arial; font-size:11px"><html:checkbox property="comboNumber" onclick="apply()" />
	    I have Read and Agree to the Terms and Conditions listed above
	    </td>
		</tr>

 		<tr>
	        <td align="left"> <font class="panelVisited" style="font-size:11px"><bean:message key="selectFinish"/>
	        </td>
    	</tr>
			<tr>
                    <td class="FireSelect">
                    <div align="center">
                    	<html:reset  value="Back" styleClass="FireSelect" style="width: 100px" onclick="history.back();"/>
                    	<html:button  property="button" value="Cancel" styleClass="FireSelect" style="width: 100px" onclick="cancelPermit()"/>
                      	<html:button  property="button" value="Submit" styleClass="FireSelect" style="width: 100px" onclick="finishProcess()" disabled="true"/>

					</div>
                </tr>


</table>
</fieldset>
</html:form>
</center>


	</td>
			<jsp:include page="onlineSideBar.jsp" flush="true">
				<jsp:param name="page" value="7" />
			</jsp:include>

 </table>
</body>
</html:html>

