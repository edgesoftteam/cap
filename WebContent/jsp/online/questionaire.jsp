<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%
 	String contextRoot = request.getContextPath();

 	java.util.List questionList = new java.util.ArrayList();

 	questionList = (java.util.List)request.getAttribute("questionList");
 	
 	ArrayList ackList = new ArrayList();

 	ackList = (ArrayList)request.getAttribute("ackList");

	String size = elms.util.StringUtils.i2s(questionList.size());

    String tempOnlineID = (String) request.getAttribute("tempOnlineID");
    //left side details bar display
    java.util.List tempOnlineDetails = new java.util.ArrayList();

    tempOnlineDetails= (java.util.List)request.getAttribute("tempOnlineDetails");
  //  System.out.println("LIST CCCC"+tempOnlineDetails.size());
    boolean odd = true;
    String bgc = "#E5E5E5";
    


%>

<%@page import="java.util.ArrayList"%>
<%@page import="elms.util.Operator"%><html:html>
<head>
<html:base/>
<title>City of Burbank :Permitting and Licensing System: online Permit Admin</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
	<link rel="stylesheet" href="../css/online.css" type="text/css">

<script src="<%=contextRoot%>/jsp/script/jquery-ui-1.8.2.custom/js/jquery-1.6.4.min.js" type="text/javascript"></script>

</head>

<center>
<script language="javascript">

function enableSubmit(){
//alert(document.forms[0].comboName.checked);
if(document.forms[0].comboName.checked==true)    {
	document.forms[0].button.disabled=false;
	}else
	{
	document.forms[0].button.disabled=true;
	}
}

function disableSubmit(){
//alert(document.forms[0].comboName.checked);
	//document.forms[0].button.disabled=true;
	//document.forms[0].comboName.checked=false;
}
function forcePCNsubmit() {

   // var comboName =document.forms[0].comboName.value;
//alert(comboName);
    var tempOnlineID = document.forms[0].tempOnlineID.value;
    var yes = 0;
    var no = 0;
    var size =document.forms[0].checkArrayQuestionSize.value;
if(size!=0){
    for (var i=0;i<parseInt(size);i++) {
    //checking for questions checked with "Yes" Added By Manjuprasad
        if (eval("document.forms[0].checkQuestionFlagY" + i + "[0].checked") == true)
            yes++;
     //checking for questions checked with "No" Added By Manjuprasad
        if (eval("document.forms[0].checkQuestionFlagY" + i + "[1].checked") == true)
            no++;
    }
    var val = yes + no;
}
    if( size >0 &&(yes == 0) && (no == 0) )
    {
    	alert("Please answer all questions");
    }else
    {

 	if(size >0 && (parseInt(size) != val))
    {
    alert("Please answer all the Questions");
    }else
    {

		if(parseInt(yes) > 0)
		{
			 var forcePC="Y";



			document.forms['applyOnlinePermitForm'].action='<%=contextRoot%>/updateOnlinePermit.do?tempOnlineID='+tempOnlineID+'&updatePermit=yes&forcePC='+forcePC;
		    document.forms['applyOnlinePermitForm'].submit();

		}else{
			 var forcePC="N";

			document.forms['applyOnlinePermitForm'].action='<%=contextRoot%>/updateOnlinePermit.do?tempOnlineID='+tempOnlineID+'&updatePermit=yes&forcePC='+forcePC;
		    document.forms['applyOnlinePermitForm'].submit();
		}
    }
	}

}



$(function() {

	$(document).ready(function () {
		 if(<%=ackList.size()%> > 0){
	 		$(":checkbox").click(countChecked);
			countChecked();
		 }
	
	});
	
	function countChecked() {
		  var n = 0;
		  $("input[name='comboName']:checked").each(function(){
			n= n+1;
		  });
		  
		  if(<%=ackList.size()%> == n){
			  $('#accept').removeAttr('disabled'); 
		  }else {
			  $('#accept').attr('disabled', 'disabled');
		  }    
	}

});

</script>

<body text="#000000" leftmargin="5" bottommargin="0"  scroll="yes" topmargin="0" marginwidth="0" marginheight="40" rightmargin="0" onload="disableSubmit();">

<html:form name="applyOnlinePermitForm" type="elms.control.beans.online.ApplyOnlinePermitForm" action="/updateOnlinePermit">

<html:hidden property="checkArrayQuestionSize" value='<%=size%>' />


<html:hidden property="tempOnlineID" value="<%=tempOnlineID%>" />
<table align="center" cellpadding="0" cellspacing="0" style="height:100%" width="100%">

<tr>
  <td align="center" valign="top" width="99%" >
	<br><br> <br><br>
<fieldset style="width: 600px" >

  <table cellpadding="15" cellspacing="0" width="100%" >
   <tr>
      <td align="left"> <font class="panelVisited" style="font-size:30px"> 7<font class="panelVisited" style="font-size:12px"><bean:message key="question"/></td>
   </tr>

   <tr valign="top" align="left">
     <td>
       <table width="100%" border="0" cellspacing="0" cellpadding="10" style="border-top: 1px solid #003366; border-left: 1px solid #003366; border-right: 1px solid #003366">

         <%
           int j=1;
           //Display of questions dynamically from the database with "Yes" or "No" Option - Added By Manjuprasad
           for(int i=0;i<questionList.size();i++) {
             elms.control.beans.online.ApplyOnlinePermitForm applyOnlinePermitForm = (elms.control.beans.online.ApplyOnlinePermitForm) questionList.get(i);
             
             if (odd) {
                 bgc = "#E5E5E5";
                 odd = false;
             }
             else {
                 bgc = "#FFFFFF";
                 odd = true;
             }
             %>


             <tr>
               <td class="tabletext" width="15" valign="top" style="border-bottom: 1px solid #003366; background-color: <%out.print(bgc);%>">
                  <%=j%>.
                 
               </td>

               <td class="tabletext"  valign="top" style="border-bottom: 1px solid #003366; background-color: <%out.print(bgc);%>">
                 <%=applyOnlinePermitForm.getQuestionaireDescription()%>
               </td>
               <td class="tabletext"  valign="top" style="border-bottom: 1px solid #003366; border-left: 1px solid #003366; background-color: <%out.print(bgc);%>" align="center">
                 <input type="radio" name="checkQuestionFlagY<%=i%>" value="Y_<%=applyOnlinePermitForm.getTempQuestionId()%>"><br><font class="panelVisited" style="font-size:8px"> YES
               </td>
               <td class="tabletext" valign="top" style="border-bottom: 1px solid #003366; background-color: <%out.print(bgc);%>" align="center">
                 <input type="radio" name="checkQuestionFlagY<%=i%>" value="N_<%=applyOnlinePermitForm.getTempQuestionId()%>"><br><font class="panelVisited" style="font-size:8px"> NO
               </td>
             </tr>
             <%
             j++;
           }
         %>
       </table>
       <br>
 <table cellpadding="15" cellspacing="0" width="100%">
 <html:errors />

	<%
           for(int i=0;i<ackList.size();i++) {
             elms.control.beans.online.ApplyOnlinePermitForm applyOnlinePermitForm = (elms.control.beans.online.ApplyOnlinePermitForm) ackList.get(i);
     %>
	   <tr>
       <td class="tabletext" valign="top" colspan="3"><font color="red" style="font-family: Arial; font-size:11px">
       		<input type="checkbox" name="comboName"  value="<%=applyOnlinePermitForm.getTempAcknowledgementId()%>" />
       		<font color="red" style="font-family: Arial; font-size:11px">&nbsp;&nbsp;
		  <%=applyOnlinePermitForm.getAcknowledgementDescription()%>
       </td><tr>
       
       <%
           }
     %>
   
   
       </table>
<br>


       <table width="100%" border="0" cellspacing="0" cellpadding="10">
         <tr>
           <td class="FireSelect" colspan="4">
             <div align="center">
     <!-- On Click of Button it will check the list of questions checked with "Yes" or "No" -->
             <html:reset  value="Back" styleClass="FireSelect" style="width: 100px" onclick="history.back();"/>
             <input type="button" name="accept" id="accept" value="Continue"   class="FireSelect" onclick="forcePCNsubmit()"/>
             </div>
           </td>
         </tr>
       </table>
       <br>
       <br>
       <br>
      </td>
    </tr>
   </table>

</fieldset>
       <br><br><br><br>
</td>

</fieldset>
</html:form>
</center>
	</td> 

			<jsp:include page="onlineSideBar.jsp" flush="true">
				<jsp:param name="page" value="7" />
			</jsp:include>

 </table>
</body>
</html:html>