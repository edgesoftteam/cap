<%@page import="org.owasp.encoder.Encode"%>
<%@ page import="elms.agent.*" %>

<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>
<app:checkLogon />

<html:html>
<head>
<html:base />
<title>City of Burbank : Online Business Center : Fees</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<bean:define id="user" name="user" type="elms.security.User"/>

<%!String activityId = ""; %>

<%
String contextRoot = request.getContextPath();
activityId = request.getParameter("id");
			if (!(request.getParameter("id") == null)) {
                activityId = request.getParameter("id");
            }else {
                activityId = (String) request.getAttribute("id");
            }

String actNbr = (String) session.getAttribute("actNumber");
if (actNbr == null)	actNbr = "";
String lsoAddress = (String) session.getAttribute("lsoAddress");
if (lsoAddress == null)	lsoAddress = "";
String psaInfo = (String) session.getAttribute("psaInfo");
if (psaInfo == null) psaInfo = "";
String onloadScript = (String) request.getAttribute("onloadScript");
if (onloadScript == null) onloadScript = "frmLoad()";

String projectName = LookupAgent.getProjectNameForActivityNumber(actNbr);
%>
<script language="JavaScript" src="../script/calendar.js"></script>
<script language="JavaScript" src="../script/formValidations.js"></script>

<script language="JavaScript">
function getBusinessTaxReport(actId) {
   window.open('<%=contextRoot%>/printBusTaxReceipt.do?actNbr=<%=actNbr%>&report=yes');
}
function validateUnits() {
	ii = parseInt(document.forms[1].elements['planCheckFeeCount'].value);
	for(i=0;i<ii;i++) {
		if ((document.forms[1].elements['planCheckFeeList[' + i + '].input'].value == 'I')&&  (document.forms[1].elements['planCheckFeeList[' + i + '].checked'].checked == true)&&  ((document.forms[1].elements['planCheckFeeList[' + i + '].feeUnits'].value == '') || (document.forms[1].elements['planCheckFeeList[' + i + '].feeUnits'].value == '0'))) {
			alert ("Please Enter # Units");
			document.forms[1].elements['planCheckFeeList[' + i + '].feeUnits'].focus();
			return false;
			}
	}

	//code added for development fees by Gayathri on 15thDec'08
    ii = parseInt(document.forms[1].elements['developmentFeeCount'].value);
	for(i=0;i<ii;i++) {
		if ((document.forms[1].elements['developmentFeeList[' + i + '].input'].value == 'I')&&  (document.forms[1].elements['developmentFeeList[' + i + '].checked'].checked == true)&&  ((document.forms[1].elements['developmentFeeList[' + i + '].feeUnits'].value == '') || (document.forms[1].elements['developmentFeeList[' + i + '].feeUnits'].value == '0'))) {
			alert ("Please Enter # Units");
			document.forms[1].elements['developmentFeeList[' + i + '].feeUnits'].focus();
			return false;
			}
	}

	ii = parseInt(document.forms[1].elements['activityFeeCount'].value);
	for(i=0;i<ii;i++) {
		if ((document.forms[1].elements['activityFeeList[' + i + '].input'].value == 'I')		&&  (document.forms[1].elements['activityFeeList[' + i + '].checked'].checked == true)		&&  ((document.forms[1].elements['activityFeeList[' + i + '].feeUnits'].value == '') || (document.forms[1].elements['activityFeeList[' + i + '].feeUnits'].value == '0'))) {
			alert ("Please Enter # Units");
			document.forms[1].elements['activityFeeList[' + i + '].feeUnits'].focus();
			return false;
			}
	}

	return true;
}

function calculateBusTax(name)
{
    BUSINESS_TAX_MIN = 10;
    BUSINESS_TAX_MAX = 460;

	startIndex = name.indexOf('[')+1;
	endIndex = name.indexOf(']');
	index  = name.substring(startIndex,endIndex);
	// alert(name + '...' + startIndex + '...' + endIndex + '...' + index);
	if (document.forms[2].elements['businessTaxList[' + index + '].feeValuation'].value != "") {
		valuation = document.forms[2].elements['businessTaxList[' + index + '].feeValuation'].value;
		valuation = valuation.replace('$','');
		valuation = parseFloat(valuation.replace(',',''));
		tax = valuation * 0.00080;//changed by AB, was 77 cents before july 1,2008
		if (tax < BUSINESS_TAX_MIN) {
            tax = BUSINESS_TAX_MIN;
        }
        else if (tax > BUSINESS_TAX_MAX) {
            tax = BUSINESS_TAX_MAX;
        }
		document.forms[2].elements['businessTaxList[' + index + '].feeAmount'].value = dollarFormatted(tax.toFixed(2));
	}
}

function resetForm() {
 		document.forms[1].action='<html:rewrite forward="viewFeesMgr"/>';
 		document.forms[1].submit();
}
// Add a new row to the business tax table. Make sure the last row has a peopleid in it
function addBusinessTaxRow() {
	i = parseInt(document.forms[2].elements['busTaxCount'].value);
	if (document.forms[2].elements['businessTaxList[' + i + '].peopleId'].value != "") {
		document.forms[2].action='<html:rewrite forward="addBusTaxFees" />';
		document.forms[2].submit();
		return true;
	}
}
function frmLoad () {

	ctr=parseInt(document.forms[2].elements['busTaxCount'].value);

	for(i=1;i<=ctr;i++)
		{
			b=(8*(i-1))+3;
			{
				document.forms[2].elements[b+3].disabled=true;
			}
		}

}
function copyValidate(indexValue){

	BUSINESS_TAX_MIN = 10;
	BUSINESS_TAX_MAX = 460;

     indexValue=indexValue.substring(16,17);
     if(document.forms[2].elements['businessTaxList[' + indexValue + '].checked'].checked == true)
     {
            document.forms[2].elements['businessTaxList[' + indexValue + '].feeValuation'].value  = document.forms[0].elements['valuation'].value;
            document.forms[2].elements['businessTaxList[' + indexValue + '].feeValuation'].readonly  = true;

			valuation = document.forms[2].elements['businessTaxList[' + indexValue + '].feeValuation'].value;
			valuation = valuation.replace('$','');
			re = /,/g;
			valuation = valuation.replace(re,'');
			valuation = parseFloat(valuation);

			tax = valuation * 0.00080;//changed by Ab, was 77 cents before july 1, 2008
			if (tax < BUSINESS_TAX_MIN) {
	            tax = BUSINESS_TAX_MIN;
	        }
	        else if (tax > BUSINESS_TAX_MAX) {
	            tax = BUSINESS_TAX_MAX;
	        }


			document.forms[2].elements['businessTaxList[' + indexValue + '].feeAmount'].value =   dollarFormatted(tax.toFixed(2));
	} else {
            document.forms[2].elements['businessTaxList[' + indexValue + '].feeValuation'].value  = "";
            document.forms[2].elements['businessTaxList[' + indexValue + '].feeAmount'].value =   "";
     }

}

function checkUnit(indexValue,feeType){

		if (feeType == 1) {
			indexValue=indexValue.substring(17,19);
			indexValue=indexValue.replace(']','');
			//alert(indexValue);
			if(parseInt(document.forms[1].elements['planCheckFeeList[' + indexValue + '].feeUnits'].value) > 0) {
				document.forms[1].elements['planCheckFeeList[' + indexValue + '].checked'].checked =true;
			}
			else{
				document.forms[1].elements['planCheckFeeList[' + indexValue + '].checked'].checked =false;
			}

		}
		else if (feeType == 2) {

			indexValue=indexValue.substring(16,18);
			indexValue=indexValue.replace(']','');
			if(parseInt(document.forms[1].elements['activityFeeList[' + indexValue + '].feeUnits'].value) > 0) {
				document.forms[1].elements['activityFeeList[' + indexValue + '].checked'].checked =true;
			}
			else{
				document.forms[1].elements['activityFeeList[' + indexValue + '].checked'].checked =false;
			}
		}
	//code added for development fees by Gayathri on 15thDec'08
		else if (feeType == 3) {
			indexValue=indexValue.substring(19,21);
			indexValue=indexValue.replace(']','');
			if(parseInt(document.forms[1].elements['developmentFeeList[' + indexValue + '].feeUnits'].value) > 0) {
				document.forms[1].elements['developmentFeeList[' + indexValue + '].checked'].checked =true;
			}
			else{
				document.forms[1].elements['developmentFeeList[' + indexValue + '].checked'].checked =false;
			}
		}

}

function checkFee(indexValue,feeType){
		if (feeType == 1) {
			indexValue=indexValue.substring(17,19);
			indexValue=indexValue.replace(']','');
			//alert(indexValue);
			if(document.forms[1].elements['planCheckFeeList[' + indexValue + '].feeAmount'].value == '$0.00') {
				document.forms[1].elements['planCheckFeeList[' + indexValue + '].checked'].checked = false;
			}
			else{
				document.forms[1].elements['planCheckFeeList[' + indexValue + '].checked'].checked =true;
			}

		}
		else if (feeType == 2) {

			indexValue=indexValue.substring(16,18);
			indexValue=indexValue.replace(']','');
			if(document.forms[1].elements['activityFeeList[' + indexValue + '].feeAmount'].value == '$0.00') {
				document.forms[1].elements['activityFeeList[' + indexValue + '].checked'].checked = false;
			}
			else{
				document.forms[1].elements['activityFeeList[' + indexValue + '].checked'].checked = true;
			}
		}
		//code added for development fees by Gayathri on 15thDec'08
		else if (feeType == 3) {

			indexValue=indexValue.substring(19,21);
			indexValue=indexValue.replace(']','');
			if(document.forms[1].elements['developmentFeeList[' + indexValue + '].feeAmount'].value == '$0.00') {
				document.forms[1].elements['developmentFeeList[' + indexValue + '].checked'].checked = false;
			}
			else{
				document.forms[1].elements['developmentFeeList[' + indexValue + '].checked'].checked = true;
			}
		}

}

function prefillUnit(indexValue,feeType){
}

function newWindow(window) {

	 busTaxCount = document.forms[2].elements['busTaxCount'].value;

	 strValue=validateData('req',document.forms[2].elements['businessTaxList[' + busTaxCount + '].licenseNbr'],'License Number is a required field');

	if (strValue ==true)
	{
	msgWindow=open('',window,'resizable=yes,width=450,height=500,screenX=600,screenY=200,top=200,left=475,scrollbars');
     msgWindow.location.href = '<html:rewrite forward="listContractor" />' + '?licenseNumber=' + document.forms[2].elements['businessTaxList[' + busTaxCount + '].licenseNbr'].value + '&peopleId=' + document.forms[2].elements['businessTaxList[' + busTaxCount + '].peopleId'].value + '&name=' + document.forms[2].elements['businessTaxList[' + busTaxCount + '].name'].value + '&strFlag=fresh';
     if (msgWindow.opener == null) msgWindow.opener = self;
     }
}

function validateValuationFunction() {
	//strValue=false;
    strValue=validateData('req',document.forms[0].elements['planCheckFeeDate'],'Plan Check Fee Date is a required field');
    if (strValue == true)
    {
		if (document.forms[0].elements['planCheckFeeDate'].value != '')
    	{
    		strValue=validateData('date',document.forms[0].elements['planCheckFeeDate'],'Invalid date format');
    	}
    }
    if (strValue == true)
    {
    	strValue=validateData('req',document.forms[0].elements['permitFeeDate'],'Permit Fee Date is a required field');
    }
    if (strValue == true)
    {
		if (document.forms[0].elements['permitFeeDate'].value != '')
    	{
    		strValue=validateData('date',document.forms[0].elements['permitFeeDate'],'Invalid date format');
    	}
    }
    if (strValue == true)
    {
    	//alert("test");
 		document.forms[0].action='<%=contextRoot%>/saveValuationMgr.do';
 		document.forms[0].submit();
 		//return true;
    }
}

function validateBusinessTaxFunction() {
	ctr=parseInt(document.forms[2].elements['busTaxCount'].value)+1;
	for(i=1;i<=ctr;i++)	{
	    b=(8*(i-1))+3;
		if((eval(document.forms[2].elements[b+2].value.length) != 0) || (eval(document.forms[2].elements[b+6].value.length) != 0)) {
			strValue=validateData('req',document.forms[2].elements[b+2],'License Number is a required field');
			if (strValue == true) {
				strValue=validateData('req',document.forms[2].elements[b+4],'Name is a required field. Please click on Find Contractor button');
			}
			if (strValue == true) {
				strValue=validateData('req',document.forms[2].elements[b+5],'Valuation is a required field');
			}
			if ((document.forms[2].elements[b+1].checked != true) && (strValue == true)) {
				strValue=validateData('req',document.forms[2].elements[b+7],'Trade Description is a required field');
			}
			if (strValue == false) {
				return strValue;
			}
		}
	}

	document.forms[2].action='<%=contextRoot%>/saveBusTaxFees.do';
	document.forms[2].submit();
}
function checkPlanRequired() {
	// checking the plan check fees
	count =  parseInt(document.forms[1].elements['planCheckFeeCount'].value);
	for (i=0;i<=count;i++) {
		if ((document.forms[1].elements['planCheckFeeList[' + i + '].required'].value != "0") &&
			(document.forms[1].elements['planCheckFeeList[' + i + '].required'].value != "")) {
				document.forms[1].elements['planCheckFeeList[' + i + '].checked'].checked = true;
		}
	}

}

//code added for development fees by Gayathri on 15thDec'08
function checkDevelopmentFeesRequired() {
	// checking the development fees
	count =  parseInt(document.forms[1].elements['developmentFeeCount'].value);

	for (i=0;i<=count;i++) {
		if ((document.forms[1].elements['developmentFeeList[' + i + '].required'].value != "0") &&
			(document.forms[1].elements['developmentFeeList[' + i + '].required'].value != "")) {
				document.forms[1].elements['developmentFeeList[' + i + '].checked'].checked = true;
		}
	}
}

function checkPermitRequired() {
	// checking the activity fees
	count =  parseInt(document.forms[1].elements['activityFeeCount'].value);
	for (i=0;i<=count;i++) {
	//alert(i);
	//alert(document.forms[1].elements['activityFeeList[' + i + '].required'].value);
	if 	((document.forms[1].elements['activityFeeList[' + i + '].feePc'].value != "X") &&
			(document.forms[1].elements['activityFeeList[' + i + '].input'].value != "X") ) {

		if ((document.forms[1].elements['activityFeeList[' + i + '].required'].value != "0") &&
			(document.forms[1].elements['activityFeeList[' + i + '].required'].value != ""))
			{
				document.forms[1].elements['activityFeeList[' + i + '].checked'].checked = true;
		}
	}
	}
}

function previewFees() {
	if (validateUnits() == true) {
			document.forms[1].action='<%=contextRoot%>/previewFees.do';
			document.forms[1].submit();
 	}
}
function saveFees() {
	if (validateUnits() == true) {
	document.forms[1].action='<%=contextRoot%>/saveFeesMgr.do';
 	document.forms[1].submit();
	}
}
function cancelPage() {
	document.forms[1].action='<%=contextRoot%>/viewActivity.do?activityId=<%=activityId%>';
 	document.forms[1].submit();
}

//-----------------Added for penalty---------------
function checkPenaltyFeesRequired(){
count =  parseInt(document.forms[1].elements['penaltyFeeCount'].value);
for (i=0;i<=count;i++) {
if ((document.forms[1].elements['penaltyFeeList[' + i + '].required'].value != "0") &&
			(document.forms[1].elements['penaltyFeeList[' + i + '].required'].value != ""))
			{
				document.forms[1].elements['penaltyFeeList[' + i + '].checked'].checked = true;
		}
	}
}

</SCRIPT>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0"	marginwidth="0" marginheight="0" onload="<%=onloadScript%>">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
	<tr valign="top">
		<td width="99%">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<font class="con_hdr_3b">Fee Manager&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%>
						<br><br>
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="98%" background="../images/site_bg_B7C1CB.jpg"><font
												class="con_hdr_2b">Fees</td>
											<td width="1%"><img src="../images/site_hdr_split_1_tall.jpg"
												width="26" height="25"></td>
		<!-- 									<td width="1%" class="tablelabel">
												<nobr>
													<html:button property="Back" value="Back" styleClass="button" onclick="cancelPage();"></html:button> &nbsp;
												</nobr>
											</td>
					 -->					</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td background="../images/site_bg_B7C1CB.jpg">
									<table width="100%" border="0" cellspacing="1" cellpadding="2">
										<tr>
											<td class="tablelabel">
												<img src="../images/spacer.gif" width="1" height="1">
											</td>
											<td class="tablelabel">
												<div align="right">
													Activity
												</div>
											</td>
											<td class="tablelabel">
												<div align="right">
													Plan Check
												</div>
											</td>

											<td class="tablelabel">
												<div align="right">
													Development Fee
												</div>
											</td>
											<td class="tablelabel">
												<div align="right">
													Business Tax
												</div>
											</td>
											<td class="tablelabel">
												<div align="right">
													Total
												</div>
											</td>
										</tr>
										<tr>
											<td class="tablelabel">
												Fees
											</td>
											<td class="tabletext">
												<div align="right">
													<bean:write scope="session" name="financeSummary" property="permitFeeAmt" />
												</div>
											</td>
											<td class="tabletext">
												<div align="right">
													<bean:write scope="session" name="financeSummary"	property="planCheckFeeAmt" />
												</div>
											</td>
											<td class="tabletext">
												<div align="right">
													<bean:write scope="session" name="financeSummary"	property="developmentFeeAmt" />
												</div>
											</td>
											<td class="tabletext">
												<div align="right">
													<bean:write	scope="session" name="financeSummary"	property="businessTaxFeeAmt" />
												</div>
											</td>
											<td class="tabletext">
												<div align="right">
													<bean:write scope="session" name="financeSummary" property="totalFeeAmt" />
												</div>
											</td>
										</tr>
										<tr>
											<td class="tablelabel"><font
												class="con_hdr_1"><a
												href='<%=contextRoot%>/processPayment.do?levelId=<%=activityId%>&amount=<bean:write scope="session" name="financeSummary" property="totalAmountDue" />'
												class="hdrs">Payment</a></td>
											<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary" property="permitPaidAmt" /></div>
											</td>
											<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary"
												property="planCheckPaidAmt" /></div>
											</td>
											<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary"
												property="developmentFeePaidAmt" /></div>
											</td>
											<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary"
												property="businessTaxPaidAmt" /></div>
											</td>
											<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary" property="totalPaidAmt" /></div>
											</td>
										</tr>
										<tr>
											<td class="tablelabel"><font
												class="con_hdr_1">Extended Credit</td>
											<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary"
												property="permitCreditAmt" /></div>
											</td>
											<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary"
												property="planCheckCreditAmt" /></div>
											</td>
											<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary"
													property="developmentFeeCreditAmt" /></div>
											</td>
											<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary"
												property="businessTaxCreditAmt" /></div>
											</td>
											<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary" property="totalCreditAmt" /></div>
											</td>
										</tr>
										<tr>
											<td class="tablelabel"><font
												class="con_hdr_1">Bounced Amount</td>
											<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary"
												property="permitBouncedAmt" /></div>
											</td>
											<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary"
												property="planCheckBouncedAmt" /></div>
											</td>
											<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary"
													property="developmentFeeBouncedAmt" /></div>
											</td>
											<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary"
												property="businessTaxBouncedAmt" /></div>
											</td>
											<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary"
												property="totalBouncedAmt" /></div>
											</td>
										</tr>
										<tr>
											<td class="tablelabel"><font
												class="con_hdr_1">Balance Due </td>
											<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary"
												property="permitAmountDue" /></div>
											</td>
											<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary"
												property="planCheckAmountDue" /></div>
											</td>
											<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary"
													property="developmentFeeAmountDue" /></div>
											</td>
											<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary"
												property="businessTaxAmountDue" /></div>
											</td>
											<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary" property="totalAmountDue" /></div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			<html:form name="feesMgrForm" type="elms.control.beans.FeesMgrForm"	action="">

				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="98%" background="../images/site_bg_B7C1CB.jpg"><font
												class="con_hdr_2b">Valuation</td>
											<td width="1%"><img src="../images/site_hdr_split_1_tall.jpg"
												width="26" height="25"></td>
									<!-- 		<td width="1%" class="tablelabel"><nobr>
											<security:editable levelId="<%--=activityId--%>" levelType="A" editProperty="checkUser">
											<html:button property="Save" value="Save Valuation" styleClass="button" onclick="validateValuationFunction();"/>
											</security:editable>
											&nbsp;</nobr></td>
									-->	</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td background="../images/site_bg_B7C1CB.jpg">
									<table width="100%" border="0" cellspacing="1" cellpadding="2">
										<tr valign="top">
											<td class="tablelabel" width="1%"><font
												class="con_hdr_1">Valuation</td>
											<td class="tabletext" width="99%"><html:text
												property="valuation" size="18" maxlength="14"
												styleClass="textboxm" onblur="formatCurrency(this);"
												onkeypress="return displayPeriod();" /> <font
												class="con_text_1"></td>
										</tr>
									<!-- 	<tr valign="top">
											<td class="tablelabel" width="1%"><nobr><font
												class="con_hdr_1">PLAN Check Fee Date</nobr></td>
											<td class="tabletext" width="99%"><nobr> <html:text
												property="planCheckFeeDate" maxlength="10"
												styleClass="textboxd" onkeypress="return validateDate();" />
											<A
												href="javascript:show_calendar('forms[0].planCheckFeeDate');"
												onmouseover="window.status='Calendar';return true;"
												onmouseout="window.status='';return true;"> <img
												src="../images/calendar.gif" width="16" height="15" border=0 /></a></nobr>
										</tr>

											<tr valign="top">
											<td class="tablelabel" width="1%"><nobr><font
												class="con_hdr_1">DEVELOPMENT Fee Date</nobr></td>
											<td class="tabletext" width="99%"><nobr> <html:text
												property="developmentFeeDate" maxlength="10"
												styleClass="textboxd" onkeypress="return validateDate();" />
											<A
												href="javascript:show_calendar('forms[0].developmentFeeDate');"
												onmouseover="window.status='Calendar';return true;"
												onmouseout="window.status='';return true;"> <img
												src="../images/calendar.gif" width="16" height="15" border=0 /></a></nobr>
										</tr>

										<tr valign="top">
											<td class="tablelabel" width="1%"><nobr><font
												class="con_hdr_1">PERMIT Fee Date</nobr></td>
											<td class="tabletext" width="99%"><nobr> <html:text
												property="permitFeeDate" maxlength="10" styleClass="textboxd"
												onkeypress="return validateDate();" /> <A
												href="javascript:show_calendar('forms[0].permitFeeDate');"
												onmouseover="window.status='Calendar';return true;"
												onmouseout="window.status='';return true;"> <img
												src="../images/calendar.gif" width="16" height="15" border=0 /></a></nobr>
										</tr>
									--> </table>
								</td>
								<html:hidden property="id" value="<%=activityId%>" />
							</tr>
						</table>
					</td>
				</tr>

				<tr>
					<td>&nbsp;</td>
				</tr>
			</html:form>


			<html:form name="feesMgrForm" type="elms.control.beans.FeesMgrForm" action="">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="98%" background="../images/site_bg_B7C1CB.jpg"><font
												class="con_hdr_2b">Finance Manager</td>
											<td width="1%"><img src="../images/site_hdr_split_1_tall.jpg"
												width="26" height="25"></td>
											<td width="1%" class="tablelabel"><nobr>
											<security:editable levelId="<%=activityId%>" levelType="A" editProperty="checkUser">
											 <html:button property="Preview"
												value="Calculate" styleClass="button" onclick="previewFees();"></html:button>
												</security:editable>

											<security:editable levelId="<%=activityId%>" levelType="A" editProperty="checkUser">
											<html:button property="Save" value="Save Fees" styleClass="button" onclick="saveFees()" />
											</security:editable>
											&nbsp;</nobr></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td><html:errors /></td>
							</tr>

							<%
								boolean rowFlag = false;
								int i = -1;
								String feeCount = "";
							%>

							<logic:equal name="feesMgrForm" property="planCheckFeesRequired" value="Y">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="98%" background="../images/site_bg_B7C1CB.jpg"><font
												class="con_hdr_2b">Plan Check Fees</td>
											<td width="1%"><img src="../images/site_hdr_split_1_tall.jpg"
												width="26" height="25"></td>
											<td width="1%" class="tablelabel"><nobr>
											</td>

										</tr>
									</table>
								</td>
							</tr>

							<tr>
								<td background="../images/site_bg_B7C1CB.jpg">
									<table width="100%" border="0" cellspacing="1" cellpadding="2">
										<tr>
											<td class="tablelabel">
											<div align="left"><img src="../images/spacer.gif" width="1"
												height="1"></div>
											</td>
											<td class="tablelabel">
											<div align="left">Fee Description</div>
											</td>
											<td class="tablelabel">
											<div align="left">Units</div>
											</td>
											<td class="tablelabel">
											<div align="left">Amount</div>
											</td>
											<td class="tabletext">
											<div align="left"></div>
											</td>
											<td class="tablelabel">
											<div align="left"></div>
											</td>
											<td class="tablelabel">
											<div align="left">Fee Description</div>
											</td>
											<td class="tablelabel">
											<div align="left">Units</div>
											</td>
											<td class="tablelabel">
											<div align="left">Amount</div>
											</td>
										</tr>

										<nested:iterate property="planCheckFeeList">

										<%
											try{
													i++;
													if (rowFlag) {
														out.println("<TD></TD>");
														rowFlag = false;
													}
													else {
														if (i > 0) {
															out.println("</tr>");
															out.println("<tr>");
														}
														rowFlag = true;
													}
												}catch(Exception e){}

										%>
											<td class="tabletext">
												<div align="left">
													<nested:hidden property="required" />
													<nested:checkbox property="checked" onclick="prefillUnit(this.name,1)"></nested:checkbox>
												</div>
											</td>
											<td class="tabletext">
												<div align="left">

														<nested:write property="feeDescription" />

												</div>
											</td>
											<td class="tabletext">
												<div align="left">
													<nested:equal value="I" property="input">
														<nested:text property="feeUnits" styleClass="textboxm" size="5" onkeypress="validateInteger();"	onblur="checkUnit(this.name,1)" />
													</nested:equal>
													<nested:equal value="D" property="input">
														<nested:text property="feeUnits" styleClass="textboxm" size="5" onkeypress="validateInteger();" />
													</nested:equal>
												</div>
											</td>
											<td class="tabletext">

												<div align="right">

														<nested:equal value="M" property="input">
															<nested:text property="feeAmount" styleClass="textboxm"	size="12" onblur="formatCurrency(this);checkFee(this.name,1)" />
														</nested:equal>
														<nested:notEqual property="input" value="M">
															<div align="right">
																<nested:write property="feeAmount" />
															</div>
														</nested:notEqual>

												</div>
											</td>
											<nested:hidden property="input" />

										</nested:iterate>
										<%
										feeCount = "" + i;
										%>
									</table>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
								<html:hidden property="planCheckFeeCount" value="<%=feeCount%>" />
							</logic:equal>

							<logic:notEqual name="feesMgrForm" property="planCheckFeesRequired"	value="Y">
								<html:hidden property="planCheckFeeCount" value="0" />
							</logic:notEqual>

							<!-- Development of Fee ----- Code add by Gayathri on 15thDec'08 -->
							<!-- Code for development Fee -->

						<logic:equal name="feesMgrForm" property="developmentFeesRequired" value="Y">

							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="98%" background="../images/site_bg_B7C1CB.jpg">Development Fees</td>

											<td width="1%" class="tablelabel"><nobr>
											</td>
										</tr>
									</table>
								</td>
							</tr>

							<tr>
								<td background="../images/site_bg_B7C1CB.jpg">
									<table width="100%" border="0" cellspacing="1" cellpadding="2">
										<tr>
											<td class="tablelabel">
											<div align="left"><img src="../images/spacer.gif" ></div>
											</td>
											<td class="tablelabel">
											<div align="left">Fee Description</div>
											</td>
											<td class="tablelabel">
											<div align="left">Units</div>
											</td>
											<td class="tablelabel">
											<div align="left">Amount</div>
											</td>
											<td class="tabletext">
											<div align="left"></div>
											</td>
											<td class="tablelabel">
											<div align="left"></div>
											</td>
											<td class="tablelabel">
											<div align="left">Fee Description</div>
											</td>
											<td class="tablelabel">
											<div align="left">Units</div>
											</td>
											<td class="tablelabel">
											<div align="left">Amount</div>
											</td>
										</tr>
										<%
											rowFlag = false;
											i = -1;
										%>
										<nested:iterate property="developmentFeeList">

										<%
											try{

													i++;
													if (rowFlag) {
														out.println("</TD><TD>");
														rowFlag = false;
													}
													else {
														if (i > 0) {
															out.println("</tr>");
															out.println("<tr>");
														}
														rowFlag = true;
													}
												}catch(Exception e){}
										%>

											<td class="tabletext">
												<div align="left">
													<nested:hidden property="required" />
													<nested:checkbox property="checked" onclick="prefillUnit(this.name,3)"></nested:checkbox>
												</div>
											</td>
											<td class="tabletext">
												<div align="left">

														<nested:write property="feeDescription" />

												</div>
											</td>
											<td class="tabletext">
												<div align="left">
													<nested:equal value="I" property="input">
														<nested:text property="feeUnits" styleClass="textboxm" size="5" onkeypress="validateInteger();"	onblur="checkUnit(this.name,3)" />
													</nested:equal>
													<nested:equal value="D" property="input">
														<nested:text property="feeUnits" styleClass="textboxm" size="5" onkeypress="validateInteger();" />
													</nested:equal>
												</div>
											</td>
											<td class="tabletext">

												<div align="right">

														<nested:equal value="M" property="input">
															<nested:text property="feeAmount" styleClass="textboxm"	size="12" onblur="formatCurrency(this);checkFee(this.name,3)" />
														</nested:equal>
														<nested:notEqual property="input" value="M">
															<div align="right">
																<nested:write property="feeAmount" />
															</div>
														</nested:notEqual>

												</div>
											</td>
											<nested:hidden property="input" />

										</nested:iterate>
										<%
										feeCount = "" + i;
										%>
									</table>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
								<html:hidden property="developmentFeeCount" value="<%=feeCount%>" />
							</logic:equal>

							<logic:notEqual name="feesMgrForm" property="developmentFeesRequired" value="Y">
								<html:hidden property="developmentFeeCount" value="0" />
							</logic:notEqual>

						<!-- Development of Fee -->
						<!-- End of the Code for development Fee -->


						<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="98%" background="../images/site_bg_B7C1CB.jpg"><font
												class="con_hdr_2b">Permit / License Fees</td>
											<td width="1%"><img src="../images/site_hdr_split_1_tall.jpg"
												width="26" height="25"></td>
											<td width="1%" class="tablelabel"><nobr>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td background="../images/site_bg_B7C1CB.jpg">
									<table width="100%" border="0" cellspacing="1" cellpadding="2">
										<tr>
											<td class="tablelabel">
											<div align="left"><img src="../images/spacer.gif" width="1" height="1"></div>
											</td>
											<td class="tablelabel">
											<div align="left">Fee Description</div>
											</td>
											<td class="tablelabel">
											<div align="left">Units</div>
											</td>
											<td class="tablelabel">
											<div align="left">Amount</div>
											</td>
											<td class="tabletext">
											<div align="left"></div>
											</td>
											<td class="tablelabel">
											<div align="left"></div>
											</td>
											<td class="tablelabel">
											<div align="left">Fee Description</div>
											</td>
											<td class="tablelabel">
											<div align="left">Units</div>
											</td>
											<td class="tablelabel">
											<div align="left">Amount</div>
											</td>
										</tr>
										<%
											rowFlag = false;
											i = -1;
										%>
										<nested:iterate property="activityFeeList">
											<nested:hidden property="required" />
											<nested:hidden property="feePc" />
											<nested:hidden property="input" />
											<nested:notEqual value="X" property="feePc">
											<nested:notEqual value="X" property="input">

												<%
												try{
													i++;
													if (rowFlag) {
														out.println("<TD></TD>");
														rowFlag = false;
													} else {
														if (i > 0) {
															out.println("</tr>");
															out.println("<tr>");
														}
														rowFlag = true;
													}
												}catch(Exception e){}
										%>
											<td class="tabletext">
												<div align="left">
													<nested:checkbox property="checked"	onclick="prefillUnit(this.name,2)"></nested:checkbox>
												</div>
											</td>
											<td class="tabletext">
												<div align="left">

														<nested:write property="feeDescription" />

												</div>
											</td>
											<td class="tabletext">
												<div align="left">
													<nested:equal value="I" property="input">
														<nested:text property="feeUnits" styleClass="textboxm"	size="5" onkeypress="validateInteger();" onblur="checkUnit(this.name,2)" />
													</nested:equal>
													<nested:equal value="D" property="input">
														<nested:text property="feeUnits" styleClass="textboxm"	size="5" />
													</nested:equal>
												</div>
											</td>
											<td class="tabletext">
												<div align="left">

														<nested:equal property="input" value="M">
															<nested:text property="feeAmount" styleClass="textboxm"	size="12" onblur="formatCurrency(this);checkFee(this.name,2)" />
														</nested:equal>
														<nested:notEqual property="input" value="M">
															<div align="right">
																<nested:write property="feeAmount" />
															</div>
														</nested:notEqual>

												</div>
											</td>
										</nested:notEqual>
									</nested:notEqual>
										</nested:iterate>
										<%
										feeCount = "" + i;
										%>
										<html:hidden property="activityFeeCount" value="<%=feeCount%>" />
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>

				<html:hidden property="id" value="<%=activityId%>" />

				<!-- -------------------------Processing of penalty fees------------------------------ -->
						<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="98%" background="../images/site_bg_B7C1CB.jpg"><font
												class="con_hdr_2b">Penalty Fees</td>
											<td width="1%"><img src="../images/site_hdr_split_1_tall.jpg"
												width="26" height="25"></td>
											<td width="1%" class="tablelabel"><nobr>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						<tr>
								<td background="../images/site_bg_B7C1CB.jpg">
									<table width="100%" border="0" cellspacing="1" cellpadding="2">
										<tr>
											<td class="tablelabel">
												<div align="left">
													<img src="../images/spacer.gif" width="1" height="1">
												</div>
											</td>
											<td class="tablelabel">
												<div align="left">
													Fee Description
												</div>
											</td>
											<td class="tablelabel">
												<div align="left">
													Amount
												</div>
											</td>
											<td class="tabletext">
												<div align="left"></div>
											</td>
											<td class="tablelabel">
												<div align="left"></div>
											</td>
											<td class="tablelabel">
												<div align="left">
													Fee Description
												</div>
											</td>
											<td class="tablelabel">
												<div align="left">
													Amount
												</div>
											</td>
										</tr>
										<%
										rowFlag = false;
										i= -1;

										%>
										<nested:iterate property="penaltyFeeList">

												<%
												try{
													i++;
													if (rowFlag) {
														out.println("<TD></TD>");
														rowFlag = false;
													} else {
														if (i > 0) {
															out.println("</tr>");
															out.println("<tr>");
														}
														rowFlag = true;
													}

												}catch(Exception e){}

												%>

										<td class="tabletext">
														<div align="left">
															<nested:hidden property="required" />
															<nested:checkbox property="checked"	onclick="prefillUnit(this.name,4)"></nested:checkbox>
														</div>
													</td>
													<td class="tabletext">
														<div align="left">

																<nested:write property="feeDescription" />

														</div>
													</td>
										<td class="tabletext">
														<div align="left">

																<nested:equal property="input" value="M">
																	<nested:text property="feeAmount" styleClass="textboxm"	size="12" onblur="formatCurrency(this);checkFee(this.name,4)" />
																</nested:equal>
																<nested:notEqual property="input" value="M">
																	<div align="right">
																		<nested:write property="feeAmount" />
																	</div>
																</nested:notEqual>

														</div>
													</td>

										</nested:iterate>
										<%
										feeCount = "" + i;

										%>
										<html:hidden property="penaltyFeeCount" value="<%=feeCount%>" />

</table> </td> </tr><tr>
					<td>&nbsp;</td>
				</tr>
					<!-- -----------------------End of penalty fees-------------------------- -->

			</html:form>

			<html:form name="feesMgrForm" type="elms.control.beans.FeesMgrForm"	action="">
				<tr>
					<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="98%" background="../images/site_bg_B7C1CB.jpg"><font
										class="con_hdr_2b">Business Tax Fees</td>
									<td width="1%"><img src="../images/site_hdr_split_1_tall.jpg"
										width="26" height="25"></td>
									<td width="1%" class="tablelabel"><nobr>
										<security:editable levelId="<%=activityId%>" levelType="A" editProperty="checkUser">
											<html:button property="printTax" value="Print Receipt" styleClass="button" onclick="getBusinessTaxReport()" />
											<html:button property="addTax" value="Add" styleClass="button" onclick="addBusinessTaxRow();"></html:button>
										</security:editable>
										<security:editable levelId="<%=activityId%>" levelType="A" editProperty="checkUser">
											<html:button property="save" value="Save Business Tax" styleClass="button" onclick="validateBusinessTaxFunction();"/>
										</security:editable>
									</nobr></td>
								</tr>
							</table>
							</td>
						</tr>
						<tr>
							<td background="../images/site_bg_B7C1CB.jpg">
							<table width="100%" border="0" cellspacing="1" cellpadding="2">


								<tr>
									<td class="tablelabel">
									<div align="left"><img src="../images/spacer.gif" width="1"
										height="1">Primary</div>
									</td>
									<td class="tablelabel">
									<div align="left">License #</div>
									</td>
									<td class="tablelabel">
									<div align="left">Name</div>
									</td>
									<td class="tablelabel">
									<div align="left">Valuation</div>
									</td>
									<td class="tablelabel"><font
										class="con_text_1">Amount</td>
									<td class="tablelabel"><font
										class="con_text_1">Trade Description</td>
									<td class="tablelabel"><a
										target="_blank"
										href="<%=contextRoot%>/printBusTaxReceipt.do?actNbr=<%=actNbr%>&pId=all">
									<img src="../images/print-all.gif" border="0"> </a></td>
								</tr>
								<%int index = -1;%>
								<nested:iterate property="businessTaxList">
									<%index++;%>
									<tr>
										<nested:hidden property="peopleId" />
										<td class="tabletext">
										<div align="left"><nested:checkbox property="checked"
											styleId="checkbox2" onclick="copyValidate(this.name);" /></div>
										</td>
										<td class="tabletext">
										<div align="left"><nested:text property="licenseNbr" size="10"
											styleClass="textbox" /> &nbsp; <html:button
											property="findContractor" value="Find" styleClass="button"
											onclick="newWindow('window2')" /></div>
										</td>
										<td class="tabletext">
										<div align="left"><nested:text property="name" readonly="true"
											size="25" styleClass="textbox" /></div>
										</td>
										<td class="tabletext">
										<div align="left"><nested:text property="feeValuation"
											size="12" styleClass="textboxm"
											onkeypress="return validateCurrency();"
											onblur="calculateBusTax(this.name);formatCurrency(this);" /></div>
										</td>
										<td class="tabletext">
										<div align="left"><nested:text property="feeAmount"
											readonly="true" size="8" styleClass="textboxm" /></div>
										</td>
										<td class="tabletext"><nested:textarea rows="2" cols="20"
											property="comments" /></td>
										<td class="tabletext"><a target="_blank"
											href="<%=contextRoot%>/printBusTaxReceipt.do?actNbr=<%=actNbr%>&pId=<nested:write property="peopleId"/>">
										<img src="../images/print.gif" border="0"> </a></td>
										<td class="tabletext">
             							<a href="<%=contextRoot%>/deleteBusTaxFees.do?activityId=<%=activityId%>&pId=<nested:write property="peopleId"/>"><img src="../images/delete.gif" alt="Delete" border="0"></a>
             							</td>
									</tr>

								</nested:iterate>
								<%String sCount = "" + index;%>
								<html:hidden property="busTaxCount" value="<%=sCount%>" />
							</table>
							</td>
							<td width="1%">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td><img src="../images/spacer.gif" width="1" height="32"></td>
								</tr>
							</table>
							</td>
						</tr>
					</table>
					<html:hidden property="id" value="<%=activityId%>" />
					</html:form>
</body>
</html:html>
