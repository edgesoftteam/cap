<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%
 	String contextRoot = request.getContextPath();
	String  val  = (String)request.getAttribute("valuation");

%>
<html:html>
<head>
<html:base/>
<link rel="stylesheet" href="../css/elms.css" type="text/css">
<link rel="stylesheet" href="../css/online.css" type="text/css">

<script language="javascript">

function validation(){
	
	strValue=true;

	 if(document.forms[0].elements['valuation'].value == 0.0){
	    	alert('Enter valuation');
	    	document.forms[0].elements['valuation'].focus();
	    	strValue=false;
	    	 return false;
	   }
	 if(document.forms[0].elements['numberOfFloors'].value == 0){
	    	alert('Enter numberOfFloors');
	    	document.forms[0].elements['numberOfFloors'].focus();
	    	strValue=false;
	    	 return false;
	   }

	 if (strValue == true)
	    {
			 var tempOnlineID = document.forms[0].tempOnlineID.value;
		   document.forms[0].action='<%=contextRoot%>/valuation.do?tempOnlineID='+tempOnlineID;
		   document.forms[0].submit();
	 		document.forms[0].submit();
	    }else{
          return false;
    }
	
}
function checkNum(data)
{
   var validNum = "0123456789.";
   var i = count = 0;
   var dec = ".";
   var space = " ";
   var val = "";

   for (i = 0; i < data.length; i++)
      if (validNum.indexOf(data.substring(i, i+1)) != "-1")
         val += data.substring(i, i+1);

   return val;
}

function dollarFormatted(amount) {
	return '$' + commaFormatted(amount);
}

function commaFormatted(amount) {
		var delimiter = ","; // replace comma if desired
		var a = amount.split('.',2)
		var d = a[1];
		var i = parseInt(a[0]);
		if(isNaN(i)) { return ''; }
		var minus = '';
		if(i < 0) { minus = '-'; }
		i = Math.abs(i);
		var n = new String(i);
		var a = [];
		while(n.length > 3)
		{
		var nn = n.substr(n.length-3);
		a.unshift(nn);
		n = n.substr(0,n.length-3);
		}
		if(n.length > 0) { a.unshift(n); }
		n = a.join(delimiter);
		if(d.length < 1) { amount = n; }
		else { amount = n + '.' + d; }
		amount = minus + amount;
		return amount;
}

function formatCurrency(field) {
		var i = parseFloat(checkNum(field.value));
		if(isNaN(i)) { i = 0.00; }
		var minus = '';
		if(i < 0) { minus = '-'; }
		i = Math.abs(i);
		i = parseInt((i + .005) * 100);
		i = i / 100;
		s = new String(i);
		if(s.indexOf('.') < 0) { s += '.00'; }
		if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
		s = minus + s;
		field.value = dollarFormatted(s);
		return true;
}

function getValuation(){

	   var tempOnlineID = document.forms[0].tempOnlineID.value;
	   document.forms[0].action='<%=contextRoot%>/valuation.do?tempOnlineID='+tempOnlineID;
	   document.forms[0].submit();
}



</script>
</head>
<body text="#000000" leftmargin="5" bottommargin="0"  scroll="yes" topmargin="0" marginwidth="0" marginheight="40" rightmargin="0">

<center>


<html:form action="/valuation">
<html:hidden property="tempOnlineID" />
<table align="center" cellpadding="0" cellspacing="0" style="height:100%" width="100%">

<td align="center" valign="top" width="99%">
	<br><br> <br><br>
<fieldset style="width: 450px;height: 420px" >
<br><br>


<table cellpadding="15" cellspacing="0" width="100%">
 <html:errors />

 <tr>
        <td align="left" nowrap="nowrap"> <font class="panelVisited" style="font-size:30px"> 4<font class="panelVisited" style="font-size:12px"><bean:message key="valuationAmt"/>
        </td>
   </tr>


    <tr>
   		<td align="right" nowrap="nowrap"> <font class="panelVisited" style="font-size:12px">Construction Valuation : <span style="color:red">*</span> </td>
			<td align="left" colspan="1" class="con_hdr_1" bgcolor="FFFFFF"><html:text property="valuation" size="20" maxlength="20"  value="<%=val%>" styleClass="textbox" onblur="return formatCurrency(this);"  />
		</td>
    </tr>
    <tr>
   		<td align="right" nowrap="nowrap"> <font class="panelVisited" style="font-size:12px">Square Footage of Project :  </td>
			<td valign ="top" colspan="1" class="con_hdr_1" bgcolor="FFFFFF"><html:text property="squareFootage" size="20" maxlength="20"  styleClass="textbox" value="0"  /><br>For additions and remodels, include only the project area.</br>
		</td>
    </tr>
    <tr>
   		<td align="right" nowrap="nowrap"> <font class="panelVisited" style="font-size:12px">Number of dwelling units : </td>
			<td valign ="top" colspan="1" class="con_hdr_1" bgcolor="FFFFFF"><html:text property="numberOfDwellingUnits" size="20" maxlength="20"   styleClass="textbox" value="0" /> 
		</td>
    </tr>
    <tr>
   		<td align="right" nowrap="nowrap"> <font class="panelVisited" style="font-size:12px">Number of floors : <span style="color:red">*</span>  </td>
			<td valign ="top" colspan="1" class="con_hdr_1" bgcolor="FFFFFF"><html:text property="numberOfFloors" size="20" maxlength="20"   styleClass="textbox" value="0"  />
		</td>
    </tr>


<tr>
   		<td align="right" nowrap="nowrap">  <html:reset  value="Back" onclick="history.back();"/>  </td>
			<td align="left" colspan="1" class="con_hdr_1" bgcolor="FFFFFF"> <html:button  property="button" value="Next"  onclick="validation()"/>
		</td>
    </tr>
    

</table>
</fieldset>

</center>

	</td>

			<jsp:include page="onlineSideBar.jsp" flush="true">
				<jsp:param name="page" value="4" />
			</jsp:include>

 </table>
</html:form>
</html:html>

