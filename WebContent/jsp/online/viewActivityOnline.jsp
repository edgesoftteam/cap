<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ page import="javax.sql.*,sun.jdbc.rowset.*,java.util.*, elms.util.*, elms.agent.OnlineAgent" %>
<%
	String contextRoot = request.getContextPath();
%>
<app:checkLogon/>
<html:html>

<head>
	<html:base/>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
	table.csui, table.csui_title { width: 100%; padding: 0px; color:white;}
	table.csui { background-color: #cccccc; border-spacing: 1px; border-collapse: separate; color:white;}
	td.csui { padding: 6px; font-family: Armata, Arial, Helvetica, sans-serif; font-size: 1.1em; background-color: #003366; color:white; }
	td.csui_header, td.csui_label { color:white;padding: 6px; font-family: Roboto Condensed, Arial, Helvetica, sans-serif; color:white; font-size: 1.2em; background-color: #003366; text-transform: uppercase }
	
	td.tablelabell { padding: 6px; font-family: Armata, Arial, Helvetica, sans-serif; font-size: 1.1em; background-color: #e5e5e5; color:black; }

	td.tabletextt { padding: 6px; font-family: Armata, Arial, Helvetica, sans-serif; font-size: 1.1em; background-color: #ffffff; color:black; }

html, body {
   background: url(<%=contextRoot%>/jsp/online/images/burbank2.jpg) no-repeat center center fixed;
   -webkit-background-size: cover;
   -moz-background-size: cover; 
   -o-background-size: cover; 
   background-size: cover; 
  width: 100%;
  height: 100%;
  margin: 0;
  font-family:'Open Sans';
}
.head {
  width: 93%;
  height: 32px;
  padding: 12px 30px;
  overflow: hidden;
  background: #336699;
}

.content { padding: 10%; }

.button {
  background: #003366;
  border-radius: 100px;
  padding: 10px 30px;
  color: white;
  text-decoration: none;
  font-size: 1.45em;
  margin: 0 15px;
}

/* Hover state animation applied here */
.button:hover { 
  -webkit-animation: hover 1200ms linear 2 alternate;
  animation: hover 1200ms linear 2 alternate;
}

/* Active state animation applied here */
.button:active {
  -webkit-animation: active 1200ms ease 1 alternate;
  animation: active 1200ms ease 1 alternate; 
  background: #5F9BE0;
}

/* Active state animation keyframes below */

@-webkit-keyframes active { 
  0% {transform: scale(1,1);}
  90% {transform: scale(.9,.88);}
  100% {transform: scale(.92,.9);}
}

keyframes active { 
  0% {transform: scale(1,1);}
  90% {transform: scale(.9,.88);}
  100% {transform: scale(.92,.9);}
}
@-webkit-keyframes hover { 
  0% { -webkit-transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  1.8% { -webkit-transform: matrix3d(1.016, 0, 0, 0, 0, 1.037, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.016, 0, 0, 0, 0, 1.037, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  3.5% { -webkit-transform: matrix3d(1.033, 0, 0, 0, 0, 1.094, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.033, 0, 0, 0, 0, 1.094, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  4.7% { -webkit-transform: matrix3d(1.045, 0, 0, 0, 0, 1.129, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.045, 0, 0, 0, 0, 1.129, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  5.31% { -webkit-transform: matrix3d(1.051, 0, 0, 0, 0, 1.142, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.051, 0, 0, 0, 0, 1.142, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  7.01% { -webkit-transform: matrix3d(1.068, 0, 0, 0, 0, 1.158, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.068, 0, 0, 0, 0, 1.158, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  8.91% { -webkit-transform: matrix3d(1.084, 0, 0, 0, 0, 1.141, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.084, 0, 0, 0, 0, 1.141, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  9.41% { -webkit-transform: matrix3d(1.088, 0, 0, 0, 0, 1.132, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.088, 0, 0, 0, 0, 1.132, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  10.71% { -webkit-transform: matrix3d(1.097, 0, 0, 0, 0, 1.107, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.097, 0, 0, 0, 0, 1.107, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  12.61% { -webkit-transform: matrix3d(1.108, 0, 0, 0, 0, 1.077, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.108, 0, 0, 0, 0, 1.077, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  14.11% { -webkit-transform: matrix3d(1.114, 0, 0, 0, 0, 1.067, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.114, 0, 0, 0, 0, 1.067, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  14.41% { -webkit-transform: matrix3d(1.115, 0, 0, 0, 0, 1.067, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.115, 0, 0, 0, 0, 1.067, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  16.32% { -webkit-transform: matrix3d(1.119, 0, 0, 0, 0, 1.077, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.119, 0, 0, 0, 0, 1.077, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  18.12% { -webkit-transform: matrix3d(1.121, 0, 0, 0, 0, 1.096, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.121, 0, 0, 0, 0, 1.096, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  18.72% { -webkit-transform: matrix3d(1.121, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.121, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  20.02% { -webkit-transform: matrix3d(1.121, 0, 0, 0, 0, 1.113, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.121, 0, 0, 0, 0, 1.113, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  21.82% { -webkit-transform: matrix3d(1.119, 0, 0, 0, 0, 1.119, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.119, 0, 0, 0, 0, 1.119, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  24.32% { -webkit-transform: matrix3d(1.115, 0, 0, 0, 0, 1.11, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.115, 0, 0, 0, 0, 1.11, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  25.53% { -webkit-transform: matrix3d(1.113, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.113, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  29.23% { -webkit-transform: matrix3d(1.106, 0, 0, 0, 0, 1.089, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.106, 0, 0, 0, 0, 1.089, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  29.93% { -webkit-transform: matrix3d(1.105, 0, 0, 0, 0, 1.09, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.105, 0, 0, 0, 0, 1.09, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  35.54% { -webkit-transform: matrix3d(1.098, 0, 0, 0, 0, 1.105, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.098, 0, 0, 0, 0, 1.105, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  36.64% { -webkit-transform: matrix3d(1.097, 0, 0, 0, 0, 1.106, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.097, 0, 0, 0, 0, 1.106, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  41.04% { -webkit-transform: matrix3d(1.096, 0, 0, 0, 0, 1.099, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.096, 0, 0, 0, 0, 1.099, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  44.04% { -webkit-transform: matrix3d(1.096, 0, 0, 0, 0, 1.097, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.096, 0, 0, 0, 0, 1.097, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  51.45% { -webkit-transform: matrix3d(1.099, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.099, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  52.15% { -webkit-transform: matrix3d(1.099, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.099, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  58.86% { -webkit-transform: matrix3d(1.101, 0, 0, 0, 0, 1.099, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.101, 0, 0, 0, 0, 1.099, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  63.26% { -webkit-transform: matrix3d(1.101, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.101, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  66.27% { -webkit-transform: matrix3d(1.101, 0, 0, 0, 0, 1.101, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.101, 0, 0, 0, 0, 1.101, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  73.77% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  81.18% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  85.49% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  88.59% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  96% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  100% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); } 
}

@keyframes hover { 
  0% { -webkit-transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  1.8% { -webkit-transform: matrix3d(1.016, 0, 0, 0, 0, 1.037, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.016, 0, 0, 0, 0, 1.037, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  3.5% { -webkit-transform: matrix3d(1.033, 0, 0, 0, 0, 1.094, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.033, 0, 0, 0, 0, 1.094, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  4.7% { -webkit-transform: matrix3d(1.045, 0, 0, 0, 0, 1.129, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); tra6nsform: matrix3d(1.045, 0, 0, 0, 0, 1.129, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  5.31% { -webkit-transform: matrix3d(1.051, 0, 0, 0, 0, 1.142, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.051, 0, 0, 0, 0, 1.142, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  7.01% { -webkit-transform: matrix3d(1.068, 0, 0, 0, 0, 1.158, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.068, 0, 0, 0, 0, 1.158, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  8.91% { -webkit-transform: matrix3d(1.084, 0, 0, 0, 0, 1.141, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.084, 0, 0, 0, 0, 1.141, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  9.41% { -webkit-transform: matrix3d(1.088, 0, 0, 0, 0, 1.132, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.088, 0, 0, 0, 0, 1.132, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  10.71% { -webkit-transform: matrix3d(1.097, 0, 0, 0, 0, 1.107, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.097, 0, 0, 0, 0, 1.107, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  12.61% { -webkit-transform: matrix3d(1.108, 0, 0, 0, 0, 1.077, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.108, 0, 0, 0, 0, 1.077, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  14.11% { -webkit-transform: matrix3d(1.114, 0, 0, 0, 0, 1.067, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.114, 0, 0, 0, 0, 1.067, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  14.41% { -webkit-transform: matrix3d(1.115, 0, 0, 0, 0, 1.067, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.115, 0, 0, 0, 0, 1.067, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  16.32% { -webkit-transform: matrix3d(1.119, 0, 0, 0, 0, 1.077, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.119, 0, 0, 0, 0, 1.077, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  18.12% { -webkit-transform: matrix3d(1.121, 0, 0, 0, 0, 1.096, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.121, 0, 0, 0, 0, 1.096, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  18.72% { -webkit-transform: matrix3d(1.121, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.121, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  20.02% { -webkit-transform: matrix3d(1.121, 0, 0, 0, 0, 1.113, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.121, 0, 0, 0, 0, 1.113, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  21.82% { -webkit-transform: matrix3d(1.119, 0, 0, 0, 0, 1.119, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.119, 0, 0, 0, 0, 1.119, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  24.32% { -webkit-transform: matrix3d(1.115, 0, 0, 0, 0, 1.11, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.115, 0, 0, 0, 0, 1.11, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  25.53% { -webkit-transform: matrix3d(1.113, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.113, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  29.23% { -webkit-transform: matrix3d(1.106, 0, 0, 0, 0, 1.089, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.106, 0, 0, 0, 0, 1.089, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  29.93% { -webkit-transform: matrix3d(1.105, 0, 0, 0, 0, 1.09, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.105, 0, 0, 0, 0, 1.09, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  35.54% { -webkit-transform: matrix3d(1.098, 0, 0, 0, 0, 1.105, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.098, 0, 0, 0, 0, 1.105, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  36.64% { -webkit-transform: matrix3d(1.097, 0, 0, 0, 0, 1.106, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.097, 0, 0, 0, 0, 1.106, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  41.04% { -webkit-transform: matrix3d(1.096, 0, 0, 0, 0, 1.099, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.096, 0, 0, 0, 0, 1.099, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  44.04% { -webkit-transform: matrix3d(1.096, 0, 0, 0, 0, 1.097, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.096, 0, 0, 0, 0, 1.097, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  51.45% { -webkit-transform: matrix3d(1.099, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.099, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  52.15% { -webkit-transform: matrix3d(1.099, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.099, 0, 0, 0, 0, 1.102, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  58.86% { -webkit-transform: matrix3d(1.101, 0, 0, 0, 0, 1.099, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.101, 0, 0, 0, 0, 1.099, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  63.26% { -webkit-transform: matrix3d(1.101, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.101, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  66.27% { -webkit-transform: matrix3d(1.101, 0, 0, 0, 0, 1.101, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.101, 0, 0, 0, 0, 1.101, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  73.77% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  81.18% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  85.49% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  88.59% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  96% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); }
  100% { -webkit-transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform: matrix3d(1.1, 0, 0, 0, 0, 1.1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); } 
}
</style>
	
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<bean:define id="user" name="user" type="elms.security.User"/>
<bean:define id="lsoAddress" name="lsoAddress" type="java.lang.String" scope="request"/>
<%
// 		String contextRoot = request.getContextPath();
		String onloadAction = "";
			onloadAction = (String)session.getAttribute("onloadAction");

		String activityId =   (String)request.getAttribute("activityId");
		String activityType= (String)request.getAttribute("activityType");
		if (activityType == null) activityType = "";

		String level = "A";

		String activityTypeDesc= (String)request.getAttribute("activityTypeDesc");
		if (activityTypeDesc == null) activityTypeDesc = "";
		List activitySubTypes=(ArrayList)request.getAttribute("activitySubTypes");
		if (activitySubTypes == null) activitySubTypes = new ArrayList();
		String activityNumber = (String)request.getAttribute("activityNumber");
		if (activityNumber == null) activityNumber = "";
		String description = (String)request.getAttribute("description");
		if (description == null) description = "";
		String address = (String)request.getAttribute("address");
		if (address == null) address = "";
		String status = (String)request.getAttribute("status");
		if (status == null) status = "";
		String valuation = (String)request.getAttribute("valuation");
		if (valuation == null) valuation = "";
		String issueDate = (String)request.getAttribute("issueDate");
		if (issueDate == null) issueDate = "";
		String startDate = (String)request.getAttribute("startDate");
		if (startDate == null) startDate = "";
		String completionDate = (String)request.getAttribute("completionDate");
		if (completionDate == null) completionDate = "";
		String expirationDate = (String)request.getAttribute("expirationDate");
		if (expirationDate == null) expirationDate = "";
		String createdBy = (String)request.getAttribute("createdBy");
		if (createdBy == null) createdBy = "";
		String completionDateLabel = (String)request.getAttribute("completionDateLabel");
		if (completionDateLabel == null) completionDateLabel = "";
		String expirationDateLabel = (String)request.getAttribute("expirationDateLabel");
		if (expirationDateLabel == null) expirationDateLabel = "";
		String microfilm = (String)request.getAttribute("microfilm");
		

	String error= request.getAttribute("error")!=null?(String)request.getAttribute("error"):"";

	java.util.List questionList = new java.util.ArrayList();
	questionList = OnlineAgent.getQuestionAnswers(activityNumber);
	pageContext.setAttribute("questionList", questionList);
%>


<center>
<body class="csui" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<br><br>
<html:form method="post" action="/printFIR.do">
<fieldset style="width: 85%;background-color: white;">
<!--   <legend class="FireResourceStatic"> -->
     <table cellpadding="0" cellspacing="0" >
            <tr>
              <td><img width="130%" src="<%=contextRoot%>/jsp/online/images/<%=elms.agent.LookupAgent.getKeyValue("ONLINE_LOGO") %>"><br/></td>
            </tr>
      </table>
<!--   </legend> -->
  <table cellpadding="50" cellspacing="0" width="100%">
    <tr>
      <td align="center" ><table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="1" cellspacing="0" cellpadding="0">
            <tr>
                <td class="csui" style="background:white;color:black;font-size:20px;" colspan="3">
                <font class="red2b"><%=error%></font>
                <br><b>View Permit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<% if(!(lsoAddress.trim().equalsIgnoreCase("1   Pw Citywide"))){ %>
                <bean:write name="lsoAddress"/>
                <%} %>
                <br>
                <br></b>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table class="csui" width="100%" border="0" cellspacing="0" cellpadding="2">
                            <tr>
                               <td width="99%" colspan="2" class="csui">Permit Detail</td>
                            </tr>

                            

                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr valign="top">
                                <td class="tablelabell">Permit #</td>
                                <td class="tabletextt"><%=activityNumber%></td>
                                <td class="tablelabell">Permit Status</td>
                                <td class="tabletextt"><%=status%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabell">Permit Type</td>
                                <td class="tabletextt"><%=activityTypeDesc%></td>
                                <td class="tablelabell">Sub Type</td>
                                <td class="tabletextt">

                                <%
                                	Iterator it =activitySubTypes.iterator();
	                                while(it.hasNext()){
    		                            out.println(""+it.next()+"<br/>"+"<dt>");
            	                    }
                                 %>
                                </td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabell">Description</td>
                                <td class="tabletextt" colspan="3"><%=description%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabell">Applied Date</td>
                                <td class="tabletextt"><%=startDate%></td>
                                <td class="tablelabell">Issue Date</td>
                                <td class="tabletextt"><%=issueDate%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabell"><%=completionDateLabel%></td>
                                <td class="tabletextt"><%=completionDate%></td>
                                <td class="tablelabell"><%=expirationDateLabel%></td>
                                <td class="tabletextt"><%=expirationDate%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabell">Address</td>
                                <% if(!(lsoAddress.trim().equalsIgnoreCase("1   Pw Citywide"))){ %>
                                      <td class="tabletextt"><%=address%></td>
                                <%} else {%>
                                      <td class="tabletextt">&nbsp;</td>
                                <%} %>
                                <td class="tablelabell">Valuation</td>
                                <td class="tabletextt"><%=valuation%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabell">Microfilm</td>
                                <td class="tabletextt"><%=microfilm%></td>
                                <td class="tablelabell">Created By</td>
                                <td class="tabletextt"><%=createdBy%></td>
                            </tr>

                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
           <tr>
                <td colspan="3">&nbsp;<br><br></td>

            </tr>
            <tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="98%" class="csui">Finance Manager</td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                                <td class="tablelabell"><img src="../images/spacer.gif" width="1" height="1"></td>
                                <td class="tablelabell">
                                <div align="right">Activity</div>
                                </td>
                                <td class="tablelabell">
                                <div align="right">Plan Check</div>
                                </td>
                                <td class="tablelabell">
                                <div align="right">Business Tax</div>
                                </td>
                                <td class="tablelabell">
                                <div align="right">Total</div>
                                </td>
                            </tr>
                            <tr>
                                <td class="tablelabell">
									Fees
                                </td>

                                <td class="tabletextt">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="permitFeeAmt"/></div>
                                </td>
                                <td class="tabletextt">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="planCheckFeeAmt"/></div>
                                </td>
                                <td class="tabletextt">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="businessTaxFeeAmt"/></div>
                                </td>
                                <td class="tabletextt">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="totalFeeAmt"/></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="tablelabell">

								     Payment</td>

                                <td class="tabletextt">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="permitPaidAmt"/></div>
                                </td>
                                <td class="tabletextt">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="planCheckPaidAmt"/></div>
                                </td>
                                <td class="tabletextt">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="businessTaxPaidAmt"/></div>
                                </td>
                                <td class="tabletextt">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="totalPaidAmt"/></div>
                                </td>
                            </tr>
                        
                            <tr>
                                <td class="tablelabell">Balance Due </td>
                                <td class="tabletextt">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="permitAmountDue"/></div>
                                </td>
                                <td class="tabletextt">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="planCheckAmountDue"/></div>
                                </td>
                                <td class="tabletextt">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="businessTaxAmountDue"/></div>
                                </td>
                                <td class="tabletextt">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="totalAmountDue"/></div>
                                </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;<br><br></td>
            </tr>
            
            <!-- Questionaire Block Start -->
           
         		<%if(questionList.size()>0){ %>
         		 <tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
								<td class="csui"><nobr>Online Questionnaire</nobr></td>
                                
                            </tr>
                        </table>
                        </td>
                    </tr>                    
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr valign="top">
                                <td class="tablelabell">ID</td>
                                <td class="tablelabell" >Question</td>
                                <td class="tablelabell">Answer</td>
                               
                            </tr>                            
						<%
							for(int q=0;q<questionList.size();q++){
							elms.control.beans.online.ApplyOnlinePermitForm a = (elms.control.beans.online.ApplyOnlinePermitForm) questionList.get(q);
							String imgsrc ="../images/unchecked.png";
							if(a.getTempQuestionId().equals("Y")){
								imgsrc ="../images/check.png";
							}
							
						 %>
                            <tr valign="top" >
                                <td class="tabletextt"> <%=a.getTempOnlineID()%> </td>
                                <td class="tabletextt"> <%=a.getQuestionaireDescription()%> </td>
                                <td class="tabletextt"> <img src="<%=imgsrc%>"> </td>
                               
                            </tr>
						<%} %>
                           
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            
             	<%}%>
            <!-- Questionnaire Block End -->
                        </table>
                     
                        </td>
                        
                    </tr>
               <tr>
               <td>&nbsp;
               </td>
               </tr>
             <tr>                    
<%-- 				<td align="center"><html:reset  value="Back" styleClass="FireSelect" style="width: 100px;font-size: 18px;" onclick="javascript:history.back(0);"/></td> --%>
    	    <td align="center"><a href="javascript:history.back(0);" class="button" style="color: white;text-decoration: none;font-size: 17px;">Back</a></td>
			</tr>         
            </table>   
            </td></tr>
             
        </table>
</fieldset>
</html:form>
</body>
</center>
</html:html>