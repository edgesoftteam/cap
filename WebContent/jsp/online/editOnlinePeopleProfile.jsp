<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<app:checkLogon/>

<html:html>
<head>
<html:base/>
<title>City of Burbank :Permitting and Licensing System: People Manager : Edit Contractor</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%
System.out.println("inside Jsp***");
   String contextRoot = request.getContextPath();
   String addEditFlag = (String) request.getAttribute("addEditFlag");
   String peopleId = (String) request.getAttribute("peopleId");

   	String lsoAddress = (String)session.getAttribute("lsoAddress");
	if (lsoAddress == null ) lsoAddress = "";

//	String psaInfo = (String)session.getAttribute("psaInfo");
//	if (psaInfo == null ) psaInfo = "";

	String licenseNbr =(String) request.getAttribute("licenseNbr");
	if(licenseNbr==null) licenseNbr = "-1";

	String psaId = (String)request.getAttribute("psaId");
	if(psaId ==null) psaId ="";

	String psaType = (String)request.getAttribute("psaType");
	if(psaType ==null) psaType = "";

	String  fromOnline =(String)session.getAttribute("fromOnline");
	if(fromOnline==null) fromOnline="";

    java.util.List peopleTypeList = (java.util.List) request.getAttribute("peopleTypeList");

     int[] alpaha = (int [])request.getAttribute("typeofpeople");

     String  peopleTypeName =(String)request.getAttribute("peopleTypeName");
     if(peopleTypeName==null) peopleTypeName="People";


	String message = (String)request.getAttribute("message");
	if(message==null) message="";

	String error = (String)request.getAttribute("error");
	if(error==null) error="";


%>
<script language="JavaScript" src="../script/calendar.js"></script>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<script language="JavaScript">
function validateEmail()
{
var email= document.forms['peopleForm'].elements['people.emailAddress'].value;

//  a very simple email validation checking.
//  you can add more complex email checking if it helps
    var splitted = email.match("^(.+)@(.+)$");    if(splitted == null) return false;
    if(splitted[1] != null )
    {
      var regexp_user=/^\"?[\w-_\.]*\"?$/;
      if(splitted[1].match(regexp_user) == null) return false;
    }
    if(splitted[2] != null)
    {
      var regexp_domain=/^[\w-\.]*\.[A-Za-z]{2,4}$/;
      if(splitted[2].match(regexp_domain) == null)
      {
	    var regexp_ip =/^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
	    if(splitted[2].match(regexp_ip) == null) return false;
      }// if

      return true;
    }

return false;
}
</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form action="/savePeople" onsubmit="return validateFunction();">

<table width="100%" border="0" cellspacing="10" cellpadding="0" >
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
          <td><font class="green2b"><%=message%><br>
          <font class="red2b"><%=error%>
       <%   if(!error.equals("")) { %>
          <a  href=javascript:parent.location.href='<%=contextRoot%>/gateway.do?param=logout';><font class="red2b">Click here to Logout&nbsp;&nbsp;<br></a>
         <% }   %>
          <br>
          </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td background="../images/site_bg_B7C1CB.jpg" width="98%">
                      Edit Profile </td>

                      <td width="1%" class="tablelabel"><nobr>
 			           </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <!--<tr>
                     <td class="tablelabel">Type</td>
                      <td class="tabletext" ><%--=peopleTypeName--%></td>
                       <td class="tablelabel"></td>
                       <td class="tabletext">
						  <%--if(peopleTypeList!=null) {%>
					  	<html:select property="peopleType" styleClass="textbox" onchange="return bevy(this);">
	                     <html:options collection="peopleTypeList" property="typeId" labelProperty="type"/>
                        </html:select>
                    	<%}--%>
    			        </td>-->
                    <tr>
                      <td class="tablelabel">License No</td>
                      <td  class="tabletext"  style="font:  8pt/9pt verdana,arial,helvetica;width: 200 px; border : 0px;">
                          <bean:write  name = "peopleForm"  property="people.licenseNbr" />
                          <html:hidden  name = "peopleForm"  property="people.licenseNbr" />
                     	  <html:hidden name = "peopleForm"  property="people.peopleId" />
                      	  <html:hidden name = "peopleForm"  property="people.isExpired" />
                      	  <html:hidden name = "peopleForm"  property="isExpired" />
                      	  <html:hidden name = "peopleForm"  property="people.peopleType.code" />
                     	  <html:hidden name = "peopleForm"  property="psaId" />
                     	  <html:hidden name = "peopleForm"  property="psaType" />
                      </td>
                        <td class="tablelabel"></td>
                      <td class="tabletext">
                      </td>
	                </tr>
                    <tr>
                      <td class="tablelabel">Name</td>
                      <td  class="tabletext" class="textbox">
                          <bean:write   name = "peopleForm"   property="people.name"/>
                          <html:hidden   name = "peopleForm"   property="people.name"/>
                      </td>
                        <td class="tablelabel"></td>
                      <td class="tabletext">
                      </td>
                    </tr>
                      <tr>
                      <td class="tablelabel">Address</td>
                      <td class="tabletext">
                          <html:text  name = "peopleForm"   property="people.address" size="45" maxlength="60" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel">Agent Name</td>
                      <td class="tabletext">
                          <html:text  name = "peopleForm"  property="people.agentName" size="25" maxlength="20" styleClass="textbox"/>&nbsp;
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel">City,
                        State, Zip</td>
                      <td class="tabletext" colspan="1">
                          <html:text  name = "peopleForm"   property="people.city" size="25" maxlength="20" styleClass="textbox" />
                          <html:text  name = "peopleForm"   property="people.state" size="2" maxlength="2" styleClass="textbox"  />
                          <html:text  name = "peopleForm"   property="people.zipCode" size="10" maxlength="9" styleClass="textbox" />
                      </td>
                      <td class="tablelabel"></td>
                       <td class="tabletext" colspan="1"></td>
                    </tr>
                    <tr>
                      <td class="tablelabel">Phone, Ext</td>
                      <td class="tabletext">
                          <html:text  name = "peopleForm"   property="people.phoneNbr" size="25" maxlength="12" styleClass="textbox"  onkeypress="return DisplayHyphen('people.phoneNbr');" />&nbsp;
                          <html:text  name = "peopleForm"   property="people.ext" size="4" maxlength="4" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel">Fax</td>
                      <td class="tabletext">
                          <html:text  name = "peopleForm"   property="people.faxNbr" size="25" maxlength="12" styleClass="textbox" onkeypress="return DisplayHyphen('people.faxNbr');"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel">Email</td>
                      <td class="tabletext">
                          <html:text  name ="peopleForm" property="people.emailAddress" size="25" maxlength="40" styleClass="textbox" onblur="return validateEmail();" />
                      </td>
                        <td class="tablelabel">Receive Notification</td>
                      <td class="tabletext"> <html:select   name="peopleForm" property="people.enotification" styleClass="textbox">
                       <html:option value="Y">Yes</html:option>
	             <html:option value="N">No</html:option>
                      </html:select>
                      <input type="hidden" name="abc" value=""/>
                      </td>
                      </tr>
					 <tr>
                     <td colspan="4"  align="right" style="padding:6px"  background="../images/site_bg_B7C1CB.jpg">
                     <html:submit property="Update" value="  Update  Profile  " styleClass="button"  />
                      </td>
                      </tr>


                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>

</html:html>
