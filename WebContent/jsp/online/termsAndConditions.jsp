<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="org.owasp.encoder.Encode"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%
 	String contextRoot = request.getContextPath();
 	//left side details bar display
    java.util.List tempOnlineDetails = new java.util.ArrayList();
    tempOnlineDetails= (java.util.List)request.getAttribute("tempOnlineDetails");
    int peopleTypeId= elms.util.StringUtils.s2i((String)request.getAttribute("peopleTypeId"));
 	String forcePC=(String)request.getAttribute("forcePC");
 	String contextHttpsRoot =(String) request.getParameter("contextHttpsRoot");
%>
<html:html>
<head>
<%if(peopleTypeId==4){%>
<script language="javascript">

</script>
<%}else{%>
<script language="javascript">


</script>
<%}%>
<title>
City of Burbank :Permitting and Licensing System: - Terms & Conditions
</title>
</head>
<body text="#000000" leftmargin="5" bottommargin="0"  statusBar="no"  scroll="yes" topmargin="0" marginwidth="0" marginheight="40" rightmargin="0">

<%--
//Scheme used to identify  Http , https or FTP

 //   if (request.getScheme().equalsIgnoreCase("HTTP") ) {
{      String  srvname = request.getServerName();
      String    scrname = request.getQueryString();
        response.sendRedirect("https://"+srvname);
  --%>

<html:base/>
<%

    java.util.List streetList = new elms.agent.AddressAgent().getStreetArrayList();
  	pageContext.setAttribute("streetList", streetList);
%>

<center>
<style type="text/css">
    .FireTitle          { FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  td.FireHeading        { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none; PADDING: 10px; BACKGROUND-COLOR:#D9D9D9 }
    .FireHeading        { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireGlance         { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  td.FireResource       { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none; PADDING: 6px }
    .FireResource       { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
  td.FireResourceStatic { FONT-SIZE: 12px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none; PADDING: 6px }
    .FireResourceStatic { FONT-SIZE: 12px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireSelect         { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireText           { FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireHighlight      { FONT-SIZE: 15px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #990000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireMessage        { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
  td.FireMenu           { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none; PADDING: 2px; BACKGROUND-COLOR:#000000 }
    .FireMenu           { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:link      { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:visited   { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:active    { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:hover     { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireMenu1          { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:link     { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:visited  { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:active   { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:hover    { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #FFFFFF; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  @page Section1 {
                   size:8.5in 11.0in;
                   margin:0.5in 0.5in 0.5in 0.5in;
                 }
 p.MsoNormal, li.MsoNormal, div.MsoNormal {
                margin:0in;
                margin-bottom:.0001pt;
                font-size:12.0pt;
                font-family:"Arial"
              }
 table.atable {
                border-collapse:collapse;
                border:1px solid #B3B3B3
              }
 tr           {
                page-break-inside:avoid
              }
 div.Section1 {
                page:Section1;
              }

.panelVisited      { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #003265 }
.panelVisitedColor { background-color:#62A85F }
.panelActive       { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #FFFFFF }
.panelActiveColor  { background-color:#EAC563 }
.panel             { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #FFFFFF }
.panelColor        { background-color:#BB2F2F }

</style>


<html:form action="checkExistingArchitect">
<html:hidden property="tempOnlineID" />
<table cellpadding="15" cellspacing="0" width="100%">
 <html:errors />


    <tr>
        <td valign=top align="center"><img src="<%=contextRoot%>/jsp/online/images/Shield.gif" width="100" height="100"></td>
    </tr>

       <tr>
	        <td> <font class="panelVisited" style="font-size:18px">Terms and Conditions</td>
	   </tr>
	   <tr>
	        <td> <font class="panelVisited" style="font-size:12px">By submitting an electronic application, you are certifying, under penalty of perjury, that all information provided by you is true and correct.  If the City of Burbank determines that the information submitted is incorrect, the City reserves the right to revoke the permit, issue a stop work order, cancel the LNCV permit or reevaluate the scope of the project.  If necessary, the City will assess additional fees.  You are also agreeing to comply with all City of Burbank and County of Los Angeles ordinances and California State laws relating to construction, and authorizing representatives of the City of Burbank to enter upon the property for which the permit is issued for inspection purposes.</td>
	   </tr>


       <tr><td>&nbsp;</td></tr>

			<tr>
                    <td class="FireSelect">
                    <div align="center">
                      <html:button  property="button" value="Close" styleClass="FireSelect" style="width: 100px" onclick="javascript:window.close();"/>

                      </div>
                </tr>


</table>

</html:form>


</body>

</html:html>

