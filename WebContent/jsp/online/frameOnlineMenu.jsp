<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@page import="elms.agent.LookupAgent"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %> 
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>

<%@ page import="elms.common.*,java.util.*,elms.security.*"%>
<%@page import="elms.util.StringUtils"%>
<% 
   String contextRoot = request.getContextPath(); 
%>

<html:html>
<style type="text/css">
#menu1 {cursor: hand}
#menu1a {display: none}
#menu1a1 {display: none}
#menu1a11 {display: none}
#menu1a11a {display: none}
#menu1a12 {display: none}
#menu1a2 {display: none}
#menu1a3 {display: none}
#menu1a4 {display: none}
#menu1b {cursor: hand}
#menu1b1 {display: none}
#menu1b2 {display: none}
#menu1b3 {display: none}
#menu1c {display: none}
#menu1c1 {display: none}
#menu1c2 {display: none}
#menu1c3 {display: none}
#menu2 {cursor: hand}
#menu2a {display: none}
#menu2a1 {display: none}
#menu2a2 {display: none}
#menu1d1 {display: none}
#menu1e1 {display: none}
#menu1f1 {display: none}
#menu1g1 {display: none}
#menu1h1 {display: none}
#menu1i1 {display: none}
#menu1j1 {display: none}
#menu1k1 {display: none}
</style>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
<script>
window.onload = function() {
    if(!window.location.hash) {
        window.location = window.location + '#loaded';
        window.location.reload();
    }
}
</script>

</head>

<% 
   
   User user = (elms.security.User)session.getAttribute(elms.common.Constants.USER_KEY);
   String username = user.getUsername()!=null ? user.getUsername():"";
   System.out.println("username... "+username);
%>

<script>
function dohref(strHref)
{
	   if (strHref == 'ireq')      parent.f_content.location.href="<%=contextRoot%>/inspectionRequest.do?action=req";
	   else if (strHref == 'ires') parent.f_content.location.href="<%=contextRoot%>/inspectionResults.do?action=res";
	   else if (strHref == 'cancel') parent.f_content.location.href="<%=contextRoot%>/inspectionResults.do?action=cancel";
	   else if (strHref == 'profile') parent.f_content.location.href="<%=contextRoot%>/editProfile.do";
	   else if (strHref == 'cpassword') parent.f_content.location.href="<%=contextRoot%>/changePassword.do";    
	   else if (strHref == 'permits') parent.f_content.location.href="<%=contextRoot%>/myPermits.do?viewPermit=Yes&isObc=Y&isDot=N&inActive=Yes";
	   else if (strHref == 'inactive') parent.f_content.location.href="<%=contextRoot%>/myPermits.do?action=view&viewPermit=Yes&isObc=Y&isDot=N&inActive=No";
	   else if (strHref == 'apply')	parent.f_content.location.href="<%=contextRoot%>/applyPermits.do";
	   else if (strHref == 'pay') parent.f_content.location.href="<%=contextRoot%>/onlinePayment.do";
	   else if (strHref == 'dExmp') parent.f_content.location.href="<%=contextRoot%>/onlinePayment.do";   
	   else if (strHref == 'lncv') parent.f_content.location.href="<%=contextRoot%>/onlineLncv.do";
	   else if (strHref == 'garagesale') parent.f_content.location.href="<%=contextRoot%>/onlineAddress.do";
	}

function viewPermits(){
	if(document.all.menu1a1.style.display =='block'){
		document.all.menu1a1.style.display='none';		
	} else{
		document.all.menu1a1.style.display='block';
	}
}

function displayProfile()
{
	if((document.all.menu1a11.style.display =='block') && (document.all.menu1a11a.style.display =='block'))
	{
		document.all.menu1a11.style.display='none';
		document.all.menu1a11a.style.display='none'; 
		document['plusminus11'].src='../images/site_blt_plus_003366.gif';
	} else {
		document.all.menu1a11.style.display='block'; 
		document.all.menu1a11a.style.display='block'; 
		document['plusminus11'].src='../images/site_blt_minus_003366.gif';
		}
}

</script>

<body bgcolor="#e5e5e5" text="#000000" background="../images/site_bg_e5e5e5.jpg" style="margin:0; overflow-x:auto; overflow-y:auto;">

<table width="108%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="tabletitle">Online Services</td>
  </tr>
  <tr>
    <td background="../images/site_bg_B7C1CB.jpg"><img src="../images/spacer.gif" width="201" height="1"></td>
  </tr>
  <tr>
    <td><img src="../images/spacer.gif" width="1" height="10"></td>
  </tr>
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0"> 
        <tr>
			<td class="treetext">
				<div class="adminMenuContainer">
					
					<!-- My Profile -->
					<div class="sub_menu" onclick="displayProfile()"><img name="plusminus11" src="../images/site_blt_plus_003366.gif">&nbsp;<html:link href="#" styleClass="tree1" title="My Profile">My Profile</html:link></div>
						<ul id="menu1a11">
							<li><img src="../images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('profile')"  styleClass="tree1" title="Modify">Modify</html:link></li>
						</ul>
						<ul id="menu1a11a">
							<li><img src="../images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('cpassword')"  styleClass="tree1" title="Modify">Change Password</html:link></li>
						</ul>

					 
					<!-- My Permits -->
					<div class="sub_menu" onclick="if(document.all.menu1a1.style.display =='block'){document.all.menu1a1.style.display='none'; document['plusminus1'].src='../images/site_blt_plus_003366.gif';} else {document.all.menu1a1.style.display='block'; document['plusminus1'].src='../images/site_blt_minus_003366.gif'}"><img name="plusminus1" src="../images/site_blt_plus_003366.gif">&nbsp;<html:link href="#" styleClass="tree1" title="My Permits">My Permits</html:link></div>
						<ul id="menu1a1">
							<li><img src="../images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('permits')"  styleClass="tree1" title="Active Permits" >Active Permits</html:link></li>
							<li><img src="../images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('inactive')"  styleClass="tree1" title="Inactive Permits" >Inactive Permits</html:link></li>
						</ul>
					<!-- Building Permits -->
					<% 
					StringUtils stringUtils = new StringUtils();
					if(stringUtils.getKeyValue("CAP_APPLY_PERMIT").equalsIgnoreCase("true")){ %>
					<div class="sub_menu" onclick="if(document.all.menu1a12.style.display =='block'){document.all.menu1a12.style.display='none'; document['plusminus12'].src='../images/site_blt_plus_003366.gif';} else {document.all.menu1a12.style.display='block'; document['plusminus12'].src='../images/site_blt_minus_003366.gif'}"><img name="plusminus12" src="../images/site_blt_plus_003366.gif">&nbsp;<html:link href="#" styleClass="tree1" title="Building Permits">Building Permits</html:link></div>
						<ul id="menu1a12">
							<li><img src="../images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('apply')"  styleClass="tree1" title="Apply for a Permit">Apply for a Permit</html:link></li>
							<li><img src="../images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('apply')"  styleClass="tree1" title="Submit for Plan Check">Submit for Plan Check</html:link></li>
						</ul>
					<%}%>
					<!-- Building Inspections --> 
										
					<% 
					StringUtils stringUtilsInsp = new StringUtils();
					if(stringUtilsInsp.getKeyValue("CAP_INSPECTIONS").equalsIgnoreCase("true")){ %>
					<div class="sub_menu" onclick="if(document.all.menu1b1.style.display =='block'){document.all.menu1b1.style.display='none'; document['plusminus2'].src='../images/site_blt_plus_003366.gif';} else {document.all.menu1b1.style.display='block'; document['plusminus2'].src='../images/site_blt_minus_003366.gif'}"><img name="plusminus2" src="../images/site_blt_plus_003366.gif">&nbsp;<html:link href="#" styleClass="tree1" title="Inspections">Building Inspections</html:link></div>
						<ul id="menu1b1">
							<li><img src="../images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('ireq')"  styleClass="tree1" title="Schedule Inspection">Request an Inspection</html:link></li>
							<li><img src="../images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('ires')"  styleClass="tree1" title="View Results">Review Scheduled Inspections</html:link></li>
							<li><img src="../images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('cancel')"  styleClass="tree1" title="Cancel Inspection">Cancel a Scheduled Inspection</html:link></li>
						</ul>
					<%} %>
					
					<!-- Parking permits -->
					<div class="sub_menu" onclick="if(document.all.menu1c1.style.display =='block'){document.all.menu1c1.style.display='none'; document['plusminus3'].src='../images/site_blt_plus_003366.gif';} else {document.all.menu1c1.style.display='block'; document['plusminus3'].src='../images/site_blt_minus_003366.gif'}"><img name="plusminus3" src="../images/site_blt_plus_003366.gif">&nbsp;<html:link href="#"  styleClass="tree1" title="Parking Permits">Parking Permits</html:link></div>
						<ul id="menu1c1">
							<li><img src="../images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('lncv')"  styleClass="tree1" title="Apply for a License">Large Non-Commercial Vehicle</html:link></li>
						</ul>
					
						
					
        <%if(LookupAgent.getKeyValue(Constants.GARAGE_SALE_FLAG).equalsIgnoreCase("Y")){ %>
					<!-- Garage sale permits -->
					<div class="sub_menu" onclick="if(document.all.menu1k1.style.display =='block'){document.all.menu1k1.style.display='none'; document['plusminus4'].src='../images/site_blt_plus_003366.gif';} else {document.all.menu1k1.style.display='block'; document['plusminus4'].src='../images/site_blt_minus_003366.gif'}"><img name="plusminus4" src="../images/site_blt_plus_003366.gif">&nbsp;<html:link href="#"  styleClass="tree1" title="Garage Sale Permits">Garage Sale Permits</html:link></div>
					
						<ul id="menu1k1">
							<li><img src="../images/right_arrow.gif"><html:link href="javascript:dohref('garagesale')"  styleClass="tree1" title="Apply for Garage Sale Permit">New Application for Garage sale</html:link></li>
						</ul>
						<%} %>
					</div>	
				</td>
			</tr>
		</table>
	</td>
</tr>
</table>
</body>
</html:html>