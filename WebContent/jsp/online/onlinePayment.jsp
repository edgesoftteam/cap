<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="elms.util.Operator"%>
<%@page import="elms.agent.ActivityAgent, elms.agent.OnlineAgent"%>

<%@ page import="javax.sql.*,sun.jdbc.rowset.*,java.util.*" %>
<%@ page import="elms.app.finance.*" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>

<%
	String contextRoot = request.getContextPath();
	String contextHttpsRoot = (String)request.getAttribute("contextHttpsRoot");
	String comboNo = (String)request.getAttribute("comboNo");
	 String pcfees = (String)session.getAttribute("pcfees")!=null?(String)session.getAttribute("pcfees"):"N";


	String levelId = (java.lang.String)session.getAttribute("levelId");
	
	
	
//	String level = (java.lang.String)session.getAttribute("level");
String level = "A";
	String label = "";
	double pcCredit = 0;
	double pmtCredit = 0;
    String forcePC=(String)request.getAttribute("forcePC");
//          ArrayList comboActList =(ArrayList) session.getAttribute("comboActList");

	String activityNumber = new ActivityAgent().getActivtiyNumber(Operator.toInt(levelId));

 if (activityNumber == null ) activityNumber = "";
 if (comboNo == null ) comboNo = activityNumber;
		elms.agent.PeopleAgent peopleListAgent = new elms.agent.PeopleAgent();
		elms.agent.FinanceAgent financeAgent = new elms.agent.FinanceAgent();
	    java.util.List peopleList = peopleListAgent.getPeople(levelId,level);
	    elms.util.StringUtils stringUtils = new elms.util.StringUtils();

	  	pageContext.setAttribute("peopleList", peopleList);

		if ((level == null) || level.equals("A"))
			{
			label = "Activity ";
			level = "A";
			}
	//else
	//	   { label = "Sub Project ";
	//		  level = "Q";
			  request.setAttribute("level","A");
	  //}
      String lsoAddress = (String)session.getAttribute("lsoAddress");
      if (lsoAddress == null ) lsoAddress = "";
      String psaInfo = (String)session.getAttribute("psaInfo");
      if (psaInfo == null ) psaInfo = "";

	  pcCredit =elms.util.StringUtils.s2bd(financeAgent.pcpCreditForActivity('1', elms.util.StringUtils.s2i(levelId))).doubleValue();

      pmtCredit =elms.util.StringUtils.s2bd(financeAgent.pcpCreditForActivity('2', elms.util.StringUtils.s2i(levelId))).doubleValue();

      String ledgerURL=elms.agent.LookupAgent.getReportsURL()+activityNumber;

		String tempOnlineID= (String)request.getAttribute("tempOnlineID");
		
		java.util.List questionList = new java.util.ArrayList();
		questionList = OnlineAgent.getQuestionAnswers(activityNumber);
		pageContext.setAttribute("questionList", questionList);

%>
<html:html>
<head>
<html:base/>
<title>City of Burbank :Permitting and Licensing System : Fees</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>

<script language="JavaScript" src="../script/calendar.js"></script> <script language="JavaScript" src="../script/formValidations.js"></script>


</SCRIPT>

<html:errors/>
<body  class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form name="paymentMgrForm" type="elms.control.beans.PaymentMgrForm" action="/processPayment" target="_top">
<input type="hidden" name="ssl_merchant_id" value="my_virtualmerchant_ID">
<input type="hidden" name="ssl_pin" value="my_PIN">
<input type="hidden" name="ssl_amount" value="14.95">
<input type="hidden" name="method" value="">
<html:hidden property="tempOnlineID" value="<%=tempOnlineID%>" />

<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><font class="con_hdr_3b">ONLINE PAYMENT MANAGER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br>
                <br>
                </td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="95%" background="../images/site_bg_B7C1CB.jpg" style="font-family: Arial; color: #000000; font-size: 12px; font-weight: bold; padding-left: 10px">FEE DETAILS *</td>

                                <td width="4%" class="tablelabel" style="padding-right: 15px" nowrap style="font-family: Arial; color: #013567; font-size: 12px; font-weight: bold">Reference Number: <%=comboNo%></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                                <td class="tablelabel"><img src="../images/spacer.gif" width="1" height="1"></td>
                                <td class="tablelabel">
                                <div align="right">Activity</div>
                                </td>
                                <td class="tablelabel">
                                <div align="right">Plan Check</div>
                                </td>
                                <td class="tablelabel">
                                <div align="right">Business Tax</div>
                                </td>
                                <td class="tablelabel">
                                <div align="right">Total</div>
                                </td>
                            </tr>
                            <tr>
                                <td class="tablelabel">Fees *</td>
                                <td class="tabletext">
                                  <div align="right"><bean:write name="financeSummary" property="permitFeeAmt"/> *</div>
                                </td>
                                <td class="tabletext">
                                  <div align="right"><bean:write name="financeSummary" property="planCheckFeeAmt"/> *</div>
                                </td>
                                <td class="tabletext">
                                  <div align="right"><bean:write name="financeSummary" property="businessTaxFeeAmt"/> *</div>
                                </td>
                                <td class="tabletext">
                                  <div align="right"><bean:write name="financeSummary" property="totalFeeAmt"/> *</div>
                                </td>
                            </tr>
                            <tr>
                                <td class="tablelabel">Payment</td>
                                <td class="tabletext">
                                <div align="right"><bean:write name="financeSummary" property="permitPaidAmt"/></div>
                                </td>
                                <td class="tabletext">
                                <div align="right"><bean:write name="financeSummary" property="planCheckPaidAmt"/></div>
                                </td>
                                <td class="tabletext">
                                <div align="right"><bean:write name="financeSummary" property="businessTaxPaidAmt"/></div>
                                </td>
                                <td class="tabletext">
                                <div align="right"><bean:write name="financeSummary" property="totalPaidAmt"/></div>
                                </td>
                            </tr>
                           <tr>
                                <td class="tablelabel">Extended Credit</td>
                                <td class="tabletext">
                                <div align="right"><bean:write name="financeSummary" property="permitCreditAmt"/></div>
                                </td>
                                <td class="tabletext">
                                <div align="right"><bean:write name="financeSummary" property="planCheckCreditAmt"/></div>
                                </td>
                                <td class="tabletext">
                                <div align="right"><bean:write name="financeSummary" property="businessTaxCreditAmt"/></div>
                                </td>
                                <td class="tabletext">
                                <div align="right"><bean:write name="financeSummary" property="totalCreditAmt"/></div>
                                </td>
                            </tr>
                             <tr>
                                <td class="tablelabel">Bounced Amount</td>
                                <td class="tabletext">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="permitBouncedAmt"/></div>
                                </td>
                                <td class="tabletext">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="planCheckBouncedAmt"/></div>
                                </td>
                                <td class="tabletext">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="businessTaxBouncedAmt"/></div>
                                </td>
                                <td class="tabletext">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="totalBouncedAmt"/></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="tablelabel">Balance Due *</td>
                                <td class="tabletext">
                                <div align="right"><bean:write name="financeSummary" property="permitAmountDue"/> *</div>
                                </td>
                                <td class="tabletext">
                                <div align="right"><bean:write name="financeSummary" property="planCheckAmountDue"/> *</div>
                                </td>
                                <td class="tabletext">
                                <div align="right"><bean:write name="financeSummary" property="businessTaxAmountDue"/> *</div>
                                </td>
                                <td class="tabletext">
                                <div align="right"><bean:write name="financeSummary" property="totalAmountDue"/> *</div>
                                </td>

                                <input type="hidden" name="planCheckFeeAmt" value="<bean:write name="financeSummary" property="planCheckFeeAmt"/>">
                                <input type="hidden" name="permitFeeAmt" value="<bean:write name="financeSummary" property="permitFeeAmt"/>">
                                <input type="hidden" name="businessTaxFeeAmt" value="<bean:write name="financeSummary" property="businessTaxFeeAmt"/>">

                                <input type="hidden" name="pmtPermit" value="<bean:write name="financeSummary" property="permitPaidAmt"/>">
                                <input type="hidden" name="pmtPlanCheck" value="<bean:write name="financeSummary" property="planCheckPaidAmt"/>">
                                <input type="hidden" name="pmtBusTax" value="<bean:write name="financeSummary" property="businessTaxPaidAmt"/>">
                                <input type="hidden" name="permitAmt" value="<bean:write name="financeSummary" property="permitAmountDue"/>">
                                <input type="hidden" name="planCheckAmt" value="<bean:write name="financeSummary" property="planCheckAmountDue"/>">
                                <input type="hidden" name="businessTaxAmt" value="<bean:write name="financeSummary" property="businessTaxAmountDue"/>">
                                <input type="hidden" name="permitBouncedAmt" value="<bean:write name="financeSummary" property="permitBouncedAmt"/>">
                                <input type="hidden" name="planCheckBouncedAmt" value="<bean:write name="financeSummary" property="planCheckBouncedAmt"/>">
                                <input type="hidden" name="businessTaxBouncedAmt" value="<bean:write name="financeSummary" property="businessTaxBouncedAmt"/>">
                                <input type="hidden" name="totalBouncedAmt" value="<bean:write name="financeSummary" property="totalBouncedAmt"/>">
                                <input type="hidden" name="totalAmt" value="<bean:write name="financeSummary" property="totalAmountDue"/>">
                                <input type="hidden" name="pcCreditAmt" value="<%=pcCredit%>">
                                <input type="hidden" name="pmtCreditAmt" value="<%=pmtCredit%>">
                                <input type="hidden" name="permitECAmt" value="<bean:write name="financeSummary" property="permitCreditAmt"/>">
                                <input type="hidden" name="pcECAmt" value="<bean:write name="financeSummary" property="planCheckCreditAmt"/>">
                                <input type="hidden" name="busECAmt" value="<bean:write name="financeSummary" property="businessTaxCreditAmt"/>">
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td align="right">
                        <font style="font-family:Arial; font-size: 11px; color: red">* The fees displayed represent the minimum applicable fees for the activity. The activity may be subject to increased and/or additional fees.
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td>






                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td>
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="97%" background="../images/site_bg_B7C1CB.jpg" style="font-family: Arial; color: #000000; font-size: 12px; font-weight: bold; padding-left: 10px">TRANSACTION MANAGER</td>

                          <td width="2%" nowrap class="tablelabel" style="padding-right: 15px" nowrap style="font-family: Arial; color: #013567; font-size: 12px; font-weight: bold">Reference Number: <%=comboNo%></td>
                        </tr>
                      </table>
                    </td>
                </tr>
                <tr>
                  <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <logic:equal name="paymentMgrForm" property="levelType" value="A">
                        
                      </logic:equal>



                      <tr valign="top">


                        <td width="75%" style="font-family: Arial; font-size: 11px">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td background="../images/site_bg_B7C1CB.jpg" style="font-family: Arial; font-size: 11px">

                                <table width="100%" border="0" cellspacing="1" cellpadding="2">
                                  <tr>
                                    <td bgcolor="#D7DCE2" width="100%" colspan="3" style="font-family: Arial; font-size: 11px; padding: 8px">
                                      Please reference the following unique activity identification number assigned to your activity: <b><%=comboNo%></b>
                                    </td>
                                  </tr>
                                  <tr valign="top">
                                    <td class="tabletext" width="100%" colspan="3" style="padding: 10px">
                                      <html:hidden property="transactionType" value="1"/>

<%
  if(forcePC.equalsIgnoreCase("N")) {
%>

<font class="red2">Click Pay to continue.

<%
  }
  else if(forcePC.equalsIgnoreCase("Y")) {
%>

  <font class="red2" style="font-family: Arial, font-size: 11px">
    Your application has been processed and saved.  However, your activity scope requires further review prior to completion and issuance.
    <br><br>
    Please contact the  City of Burbank at (818) 238-5220 to schedule a review appointment.
    <br><br>
    


<%
  }
%>

                                    </td>
                                  </tr>

 <!--  
                                  <tr valign="top">
                                    <td class="tablelabel" width="99%" style="padding: 10px" align="right">Amount</td>
                                    <td class="tabletext" width="1%" style="padding: 10px" nowrap><logic:equal name="paymentMgrForm" property="levelType" value="Q">
                                     <html:text property="amount" readonly="true" size="10" styleClass="textboxm" onkeypress="return valCurrency();" onblur="showMethod();"/></logic:equal>
                                     <logic:equal name="paymentMgrForm" property="levelType" value="Q"><html:hidden property="amount"/>
                                      <bean:write name="financeSummary" property="totalAmountDue"/></logic:equal>
                                   </td>



                          <logic:notEqual name="paymentMgrForm" property="amount" value="$0.00">
                          <%
                            if(pcfees.equalsIgnoreCase("Y"))
                            {
                          %>
                            &nbsp;
                          <%} else {%>
                            <td class="tablelabel" style="padding: 10px" width="1%" nowrap><html:button property="pay" value="Pay" styleClass="button" onclick="return payTotalFees()"/></td>
                          <%}%>
                          </logic:notEqual>



                                 </tr>
                            -->
                               </table>
                             </td>
                           </tr>
                         </table>
                       </td>







                        <td  width="50%" valign ="top" bgcolor="FFFFFF" style="font-family: Arial; font-size: 11px; padding: 10px" nowrap>
                          <logic:present scope="session"  name="comboActList">
                            <input type ="checkbox" name= "chk" /><font class="red2b">Select/Deselect<br>
                            <logic:iterate id ="combo" name ="comboActList" scope="session" indexId="i">
                              <html:multibox property="selectedActivity"  onclick="paramapatham();">
                                <bean:write name="combo" property="actId"/>
                              </html:multibox>
                              &nbsp;<bean:write name="combo" property="actName"/> - <bean:write name="combo" property="actNbr"/> ($<bean:write name="combo" property="amountDue"/>)<br>
                              <input type ="hidden"   name='<%="store"+i%>' value='<bean:write name="combo" property="amountDue"/>'/>
                            </logic:iterate>
                          </logic:present>
                        </td>
                      </tr>




                    </table>

                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>








            <tr>
                <td>
                <table id="paymentTable" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">


                            <tr class="tablelabel">
                               <td background="../images/site_bg_B7C1CB.jpg" style="font-family: Arial; color: #000000; font-size: 12px; font-weight: bold; padding-left: 10px">FEE</td>
                            </tr>

							 <tr>
                                <td class="tablelabel">
                                <div align="left">&nbsp;</div>
                                </td>
                                <td class="tablelabel">
                                <div align="left">Name</div>
                                </td>
                                <td class="tablelabel">
                                <div align="left">Amount Due</div>
                                </td>
                                 <td class="tablelabel">
                                <div align="left">&nbsp;</div>
                                </td>
                                  <td class="tablelabel">
                                <div align="left">&nbsp;</div>
                                </td>
                                <td class="tabletext">
                                <div align="left"&nbsp;></div>
                                </td>
                                <td class="tablelabel">
                                <div align="left">Name</div>
                                </td>
                                <td class="tablelabel">
                                <div align="left">Amount Due</div>
                                </td>
                              
                            </tr>



                            <tr>
                                <logic:equal name="paymentMgrForm" property="levelType" value="A"> <%
						boolean rowFlag = false;
						int i = 0;
%> <nested:iterate property="activityFeeList"> <%
						if (rowFlag) {
							out.println("<TD></TD>");
							rowFlag = false;
							} else {
							if (i>0) {
								out.println("</tr><tr>");
								}
							rowFlag = true;
						}
						i++;

%>
								<td class="tabletext">
                                <div align="left">&nbsp;</div>
                                </td>
                                <td class="tabletext">
                                <div align="left"><nested:write property="feeDescription"/></div>
                                </td>
                                <td class="tabletext">
                                <div align="left"><nested:write property="feeAmount"/></div>
                                </td>
                                <td class="tabletext">&nbsp;
                                </td>
                                <nested:hidden property="feePaid" /><nested:hidden property="feeAccount" /><nested:hidden property="bouncedAmount" /><nested:hidden property="feeFlagFour" />
                                </nested:iterate> <input type="hidden" name="feeindex" value="<%=i%>">
                            </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">

                            <tr class="tablelabel">
                                <td class="tabletext"><br>
                                </td>
                            </tr>
                        </table>

                        <!-- bussinessTax

                        <table width="100%" border="0" cellspacing="1" cellpadding="2">

                            <tr class="tablelabel">
                                Business Tax Fees
                            </tr>

                            <tr>
                                <td class="tablelabel">
                                <div align="left">&nbsp;Name</div>
                                </td>
                                <td class="tablelabel">
                                <div align="left">Amount Due</div>
                                </td>
                                <td class="tablelabel">
                                <div align="left">Payment</div>
                                </td>
                                <td class="tabletext">
                                <div align="left"></div>
                                </td>
                                <td class="tablelabel">
                                <div align="left">Name</div>
                                </td>
                                <td class="tablelabel">
                                <div align="left">Amount Due</div>
                                </td>
                                <td class="tablelabel">
                                <div align="left">Payment</div>
                                </td>
                            </tr>
                            <tr>

                                <%
						rowFlag = false;
						i = 0;
%>
                                <nested:iterate property="businessTaxList"> <%
						if (rowFlag) {
							out.println("<TD></TD>");
							rowFlag = false;
							} else {
							if (i>0) {
								out.println("</tr><tr>");
								}
							rowFlag = true;
						}
						i++;

%> <nested:hidden property="peopleId"/>
                                <td class="tabletext">
                                <div align="left"><nested:write property="name"/></div>
                                </td>
                                <td class="tabletext"> <nested:write property="feeAmount"/></td>
                                <td class="tabletext"> <nested:text property="paymentAmount" styleClass="textboxm" size="12" onkeypress="return validateCurrency();"/></td>
                                <nested:hidden property="feePaid" />
                                </nested:iterate>
                                <input type="hidden" name="busindex" value="<%=i%>">
                                </logic:equal>

                            </tr>
                        </table>
                        
                         -->
                </table>
                </td>
            </tr>
            
           <!-- Questionaire Block Start -->
             <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
								<td class="tabletitle"><nobr>Online Questionaire</nobr></td>
                                
                            </tr>
                        </table>
                        </td>
                    </tr>                    
            		<%if(questionList.size()>0){ %>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr valign="top">
                                <td class="tablelabel">ID</td>
                                <td class="tablelabel" >Question</td>
                                <td class="tablelabel">Answer</td>
                               
                            </tr>                            
						<%
							for(int q=0;q<questionList.size();q++){
							elms.control.beans.online.ApplyOnlinePermitForm a = (elms.control.beans.online.ApplyOnlinePermitForm) questionList.get(q);
							String imgsrc ="../images/unchecked.png";
							if(a.getTempQuestionId().equals("Y")){
								imgsrc ="../images/check.png";
							}
							
						 %>
                            <tr valign="top" >
                                <td class="tabletext"> <%=a.getTempOnlineID()%> </td>
                                <td class="tabletext"> <%=a.getQuestionaireDescription()%> </td>
                                <td class="tabletext"> <img src="<%=imgsrc%>"> </td>
                               
                            </tr>
						<%} %>
                           
                        </table>
                        </td>
                    </tr>
             	<%}%>
                </table>
                </td>
            </tr>
            <!-- Questionaire Block End -->
           

        </table>
        <html:hidden property="levelId" value="<%=levelId%>"/> <html:hidden property="level" value="<%=level%>"/></td>

    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
</table>

<td width="1%">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td height="32">&nbsp;</td>
    </tr>
</table>
</td>
</html:form>
</body>
</html:html>
