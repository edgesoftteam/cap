<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="org.owasp.encoder.Encode"%>
<%@page import="elms.common.Constants"%>
<%@ page import="javax.sql.*,sun.jdbc.rowset.*,java.util.*" %>
<%@ page import="elms.app.finance.*, elms.control.beans.*, elms.util.*,elms.app.finance.Payment,elms.agent.*,elms.control.actions.ApplicationScope" %>
<%@ page import="elms.control.beans.PaymentMgrForm" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>

<%

   String contextRoot = (String)request.getAttribute("contextRoot")!=null?(String)request.getAttribute("contextRoot"):request.getContextPath();
   String contextHttpsRoot = (String)request.getAttribute("contextHttpsRoot")!=null?(String)request.getAttribute("contextHttpsRoot"):"";//ApplicationScope.contextHttpsRoot;
   String contextLinkHttpRoot = (String)request.getAttribute("contextLinkHttpRoot")!=null?(String)request.getAttribute("contextLinkHttpRoot"):"";//ApplicationScope.contextLinkHttpRoot;

    String ssl_amount= ((String)request.getAttribute("ssl_amount")!=null)?(String)request.getAttribute("ssl_amount"):"";
    String comboNo= ((String)request.getAttribute("comboNo")!=null)?(String)request.getAttribute("comboNo"):"";
	String comboName= ((String)request.getAttribute("comboName")!=null)?(String)request.getAttribute("comboName"):"";
    String sprojId= ((String)request.getAttribute("sprojId")!=null)?(String)request.getAttribute("sprojId"):"";
    String ssl_txn_id = ((String)request.getAttribute("ssl_txn_id")!=null)?(String)request.getAttribute("ssl_txn_id"):"";



    String resultCode=((String)request.getAttribute("ssl_result")!=null)?(String)request.getAttribute("ssl_result"):"";
    String resultMessage=((String)request.getAttribute("ssl_result_message")!=null)?(String)request.getAttribute("ssl_result_message"):"";
    elms.agent.OnlineAgent onlinePermitAgent = new elms.agent.OnlineAgent();

    boolean checkApproval = false;
    String username="";
    String lastname="";
    String fname="";
    String isObc = "";
    String isDot = "";
    try{
    
	    elms.security.User user = ((elms.security.User)session.getAttribute(elms.common.Constants.USER_KEY));
	    username= user.getFirstName();
	    username = username.toLowerCase();
	    isObc = ((elms.security.User)session.getAttribute(elms.common.Constants.USER_KEY)).getIsObc();
		isDot = ((elms.security.User)session.getAttribute(elms.common.Constants.USER_KEY)).getIsDot();
		
		lastname = user.getLastName();
		
		fname =username+" "+lastname;
    }catch(Exception e){
    	
    	System.err.println(e.getMessage());
    }

	Date dt =new Date();
	String tran_date = new java.text.SimpleDateFormat("MM/dd/yyyy").format(dt);
	

	//lncv print values
	String printVal = (String)  session.getAttribute("printVal");
	int actId = Integer.parseInt((String) (session.getAttribute("activityId")!=null? session.getAttribute("activityId"):"0"));
	String cardNo = request.getParameter("ssl_card_number");
	cardNo =  "XXXX-XXXX-XXXX-" +cardNo.substring(12,cardNo.length());
	String actNbr = new ActivityAgent().getActivtiyNumber(actId);
	String printUrl =  ActivityAgent.getLNCVPrintUrl();
	
	
%>
<style type="text/css">
    .FireTitle          { FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  td.FireHeading        { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none; PADDING: 10px; BACKGROUND-COLOR:#D9D9D9 }
    .FireHeading        { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireGlance         { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  td.FireResource       { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none; PADDING: 6px }
    .FireResource       { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
  td.FireResourceStatic { FONT-SIZE: 12px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none; PADDING: 6px }
    .FireResourceStatic { FONT-SIZE: 12px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireSelect         { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireText           { FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireHighlight      { FONT-SIZE: 15px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #990000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireMessage        { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
  td.FireMenu           { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none; PADDING: 2px; BACKGROUND-COLOR:#000000 }
    .FireMenu           { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:link      { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:visited   { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:active    { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:hover     { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireMenu1          { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:link     { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:visited  { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:active   { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:hover    { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #FFFFFF; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  @page Section1 {
                   size:8.5in 11.0in;
                   margin:0.5in 0.5in 0.5in 0.5in;
                 }
 p.MsoNormal, li.MsoNormal, div.MsoNormal {
                margin:0in;
                margin-bottom:.0001pt;
                font-size:12.0pt;
                font-family:"Arial"
              }
 table.atable {
                border-collapse:collapse;
                border:1px solid #B3B3B3
              }
 tr           {
                page-break-inside:avoid
              }
 div.Section1 {
                page:Section1;
              }


.panelVisited            { font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #003265; text-decoration: none }
a.panelVisited:link      { font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #003265; text-decoration: none }
a.panelVisited:visited   { font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #003265; text-decoration: none }
a.panelVisited:active    { font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #003265; text-decoration: none }
a.panelVisited:hover     { font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #003265; text-decoration: none }
.panelVisitedColor { background-color:#62A85F }
.panelActive       { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #FFFFFF }
.panelActiveColor  { background-color:#EAC563 }
.panel             { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #FFFFFF }
.panelColor        { background-color:#BB2F2F }

</style>
<html:html>
<head>
<title>City of Burbank :Permitting and Licensing System: Fees</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="/elms/jsp/css/elms.css" type="text/css">
</head>

<script language="JavaScript" src="/elms/jsp/script/formValidations.js"></script>
<script language="JavaScript">
/*function printPermit()
{
		window.open("<%=contextRoot%>/printSubProjectFIR.do?subProjectId=<%=sprojId%>&showStatus=True&status=0","_blank");

}*/
function printReceipt()
{

		window.open("<%=contextRoot%>/jsp/online/printReceipt.jsp?username=<%=fname%>&permitNo=<%=actNbr%>&amount=<%=ssl_amount%>&tran_date=<%=tran_date%>&cardNo=<%=cardNo%>&comboName=<%=comboName%>&ssl_txn_id=<%=ssl_txn_id%>","", "dialogWidth:5cm; dialogHeight:10cm");


}
function openTerms()
{
window.open("<%=contextRoot%>/jsp/online/termsAndConditions.jsp?contextHttpsRoot=<%=contextHttpsRoot%>",target="_new","toolbar=no,width=1000,height=600,screenX=100,left=100,screenY=100,top=1,location=no,status=no,menubar=no,scrollbars=no");
}
function openPrivacy()
{
window.open("<%=contextRoot%>/jsp/online/privacyPolicy.jsp?contextHttpsRoot=<%=contextHttpsRoot%>",target="_new","toolbar=no,width=1000,height=800,screenX=100,left=100,screenY=100,top=1,location=no,status=no,menubar=no,scrollbars=no");
}
function openRefund()
{
window.open("<%=contextRoot%>/jsp/online/refundPolicy.jsp?contextHttpsRoot=<%=contextHttpsRoot%>",target="_new","toolbar=no,width=1000,height=800,screenX=100,left=100,screenY=100,top=1,location=no,status=no,menubar=no,scrollbars=no");
}

function goPrintLncv(){
	window.open( "<%=printUrl%><%=actNbr%>",'_blank');
	
}

</SCRIPT>

<html:errors/>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form name="paymentMgrForm" type="elms.control.beans.PaymentMgrForm" action="https://www.myvirtualmerchant.com/VirtualMerchant/process.do" method="POST" >

<input type="hidden" name="ssl_amount" value="<%=ssl_amount%>">

<table align="center" cellpadding="0" cellspacing="0" style="height:100%">
  <td align="center">
    <fieldset style="width: 450px" >
      <legend class="FireResourceStatic">

        <table cellpadding="5" cellspacing="0">
          <tr>
            <td><img src="<%=contextRoot%>/jsp/online/images/Shield.gif" width="100" height="100"></td>
          </tr>
        </table>
      </legend>


      <br><br>

      <table cellpadding="15" cellspacing="0" width="100%">
      <html:errors />

      <tr>
        <td align="center"><font class="panelVisited" style="font-size:30px"><font color="green" style="font-size:20px">Payment Successful</td>
      </tr>

	<% if(!comboName.equals(Constants.ONLINE_LNCV_PERMIT)){ %>
      <tr>
        <td align="center"><font class="panelVisited" style="font-size:12px">Permit No : <%=comboNo%></td>
      </tr>
	<%} %>
	 <tr>
        <td align="center"><font class="panelVisited" style="font-size:12px">Permit Type : <%=comboName%></td>
      </tr>
      <tr>
        <td align="center"><font class="panelVisited" style="font-size:12px">&nbsp;&nbsp;&nbsp;&nbsp;Amount  :  <%=ssl_amount%></td>
      </tr>

      <tr>
        <td align="center"> <font class="panelVisited" style="font-size:12px">Transaction Id : <%=ssl_txn_id%> </td>
      </tr>

      <tr>
        <td>&nbsp;</td>
      </tr>

      <tr>
        <td class="FireSelect">
          <div align="center">
          <% if(comboName.equals(Constants.ONLINE_LNCV_PERMIT)){ %>
         	 <html:button  property="button" value="Print Lncv" styleClass="FireSelect" style="width: 100px" onclick="goPrintLncv();"/>
          <%} %>
            <html:button  property="button" value="Print Receipt" styleClass="FireSelect" style="width: 100px" onclick="printReceipt();"/>
            <!-- <html:button  property="button" value="Print Permit" styleClass="FireSelect" style="width: 100px" onclick="printPermit();"/> -->
          </div>
        </td>
      </tr>

      <tr>
        <td>&nbsp;</td>
      </tr>
    </table>


  </fieldset>


  <br><br><br>

  <table align="center" cellpadding="0" cellspacing="0" width="100%">

    <tr>
      <td>
        <div align="center">
              <a class="panelVisited" style="font-size:11px" href="<%=contextRoot%>/jsp/online/welcomeOnline.jsp";>Home</a>
              &nbsp; | &nbsp;<a class="panelVisited" style="font-size:11px" href="#" onclick="openTerms();">Terms & Conditions</a>
              &nbsp; | &nbsp;<a class="panelVisited" style="font-size:11px" href="#" onclick="openRefund();">Refund Policy</a>
              &nbsp; | &nbsp;<a class="panelVisited" style="font-size:11px" href="#" onclick="openPrivacy();">Privacy Policy</a>
              <font class="panelVisited" style="font-size:11px; color: #000000">&nbsp;&nbsp;&nbsp; � City of Burbank
        </div>
      </td>
    </tr>
  </table>


</td>
</tr>
</table>
</center>


</html:form>
</body>
</html:html>
