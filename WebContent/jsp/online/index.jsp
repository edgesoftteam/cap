<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="org.owasp.encoder.Encode"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@page import="elms.common.*"%>
<%
    String contextRoot = request.getContextPath();


%>
<html:html>
<head>
	<script language="javascript">
	
	function checkEnter(e){ //e is event object passed from function invocation
	
	if((e.keyCode) == 13){ //if generated character code is equal to ascii 13 (if enter key)
	
	document.forms[0].action="<%=request.getContextPath()%>/onlineLogon.do";
	document.forms[0].submit(); //submit the form
	return true;
	}
	
	
	}
	</script>



	<html:base/>

<center>
<style type="text/css">
    .FireTitle          { FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  td.FireHeading        { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none; PADDING: 10px; BACKGROUND-COLOR:#D9D9D9 }
    .FireHeading        { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireGlance         { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  td.FireResource       { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none; PADDING: 6px }
    .FireResource       { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
  td.FireResourceStatic { FONT-SIZE: 12px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none; PADDING: 6px }
    .FireResourceStatic { FONT-SIZE: 12px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireSelect         { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireText           { FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireHighlight      { FONT-SIZE: 15px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #990000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireMessage        { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
  td.FireMenu           { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none; PADDING: 2px; BACKGROUND-COLOR:#000000 }
    .FireMenu           { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:link      { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:visited   { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:active    { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:hover     { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireMenu1          { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:link     { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:visited  { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:active   { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:hover    { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #FFFFFF; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  @page Section1 {
                   size:8.5in 11.0in;
                   margin:0.5in 0.5in 0.5in 0.5in;
                 }
 p.MsoNormal, li.MsoNormal, div.MsoNormal {
                margin:0in;
                margin-bottom:.0001pt;
                font-size:12.0pt;
                font-family:"Arial"
              }
 table.atable {
                border-collapse:collapse;
                border:1px solid #B3B3B3
              }
 tr           {
                page-break-inside:avoid
              }
 div.Section1 {
                page:Section1;
              }
</style>
<br><br>
<%
	String error = (String)request.getAttribute("error");
	if(error==null) error="";

%>
<html:form focus="username"  action="/onlineLogon">
<fieldset style="width: 400px">
<legend class="FireResourceStatic"><img src="<%=contextRoot%>/jsp/online/images/<%=elms.agent.LookupAgent.getKeyValue("ONLINE_LOGO") %>" width="100" height="100"></legend><table cellpadding="50" cellspacing="0" width="400px">
   <br> <html:errors /><%=error%>
<center><b><font color='green'>
<%
    String message = (String)request.getParameter("message")!=null ? (String)request.getParameter("message") : (String)request.getAttribute("message");
    if(message==null) message="";
	out.print(message);
%></b>
</center>
<h2>CITIZEN ACCESS PORTAL</h2>
<tr>
        <td align="center">
            <table cellpadding="5" cellspacing="0">
                <tr>
                    <td class="FireResource">E-mail Address</td>
                    <td class="FireSelect">
                    <html:text  property="username" size="15" styleClass="FireSelect" style="width: 200px" onkeyup="return checkEnter(event);" />
                </tr>
                <tr>
                    <td class="FireResource">Password</td>
                    <td class="FireSelect">
                    <html:password  property="password" size="15" styleClass="FireSelect" redisplay="false" style="width: 200px" onkeyup="return checkEnter(event);" /></td>
                </tr>
               <tr>
                
                <td class="FireResource"></td>
                </tr>
                <tr>
                    <td class="FireResource">&nbsp;</td>
                    <td class="FireSelect">
                      <html:button  property="Submit" value="Login" styleClass="FireSelect" style="width: 200px" onclick="submit()"/>
                </tr>

                <tr>
                     <td class="FireResource">&nbsp;</td>
                    <td class="FireSelect">
                       <a href="<%=request.getContextPath()%>/jsp/online/onlineForgot.jsp" class="FireResource"  style="width: 200px"> &nbsp; <u>Forgot Password?</u></a>
                      </td>
                  </tr>
                <tr>
                     <td class="FireResource">&nbsp;</td>
                     <td class="FireResource">&nbsp;</td>

                  </tr>
                <tr>
                     <td class="FireResource">&nbsp;</td>
                    <td class="FireSelect">
                        <a href="<%=request.getContextPath()%>/jsp/online/onlineRegister.jsp?appId=elms" class="FireResourceStatic" style="width: 200px">&nbsp; <u>New User? Register Here</u></a><br>
                      </td>
                  </tr>
                  
                <tr>
                     <td class="FireResource">&nbsp;</td>
                    <td class="FireSelect">
                        <a href="<%=request.getContextPath()%>/jsp/online/simplePayment.jsp" class="FireResourceStatic" style="width: 200px">&nbsp; <u>Direct online payment</u></a><br>
                      </td>
                  </tr>
                  
                   <!-- <tr>
                    <td class="FireResource">&nbsp;</td>
                    <td width="250" align="right"><font class="white1" ><strong>EPALS CAP v <%--Version.getNumber() --%></font> | <font class="white1"> <%--Version.getDate()--%>&nbsp;&nbsp;</strong></font>&nbsp;</td>
                  </tr> -->
                  
            </table>
        </td>
    </tr>    
</table>
</fieldset>
</html:form>
</center>
</html:html>

