<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ page import="elms.security.*,elms.common.*"%>
<%
   String contextRoot = request.getContextPath();

%>
<html:html>
<head>
<script>
function openTerms()
{
window.open("termsAndConditions.jsp?contextHttpsRoot=<%=contextRoot%>","Oelms","toolbar=no,width=800,height=400,screenX=100,left=100,screenY=100,top=1,location=no,status=no,menubar=no,scrollbars=no,resizable=yes");
}
function openPrivacy()
{
window.open("privacyPolicy.jsp?contextHttpsRoot=<%=contextRoot%>","Oelms","toolbar=no,width=800,height=400,screenX=100,left=100,screenY=100,top=1,location=no,status=no,menubar=no,scrollbars=no,resizable=yes");
}
function openRefund()
{
window.open("refundPolicy.jsp?contextHttpsRoot=<%=contextRoot%>","Oelms","toolbar=no,width=800,height=400,screenX=100,left=100,screenY=100,top=1,location=no,status=no,menubar=no,scrollbars=no,resizable=yes");
}


</script>
</head>
<html:base/>

<title>City of Burbank :Permitting and Licensing System</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
<body bgcolor="#013567" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" background="images/CopyrightBack.gif">

<table width="100%" cellpadding="2" cellspacing="0" border="0">
  <tr>
    <td width="1%" style="padding-top: 12px; padding-left: 10px" nowrap><a href="javascript:void(0)" onclick="openTerms();"><img src="images/Terms.gif" height="12" width="98" border="0"></a></td>
    <td width="1%" style="padding-top: 12px" nowrap><img src="images/CopyrightDivider.gif" height="12" width="3"></td>
    <td width="1%" style="padding-top: 12px" nowrap><a href="javascript:void(0)" onclick="openRefund();"><img src="images/Refund.gif" height="12" width="60" border="0"></a></td>
    <td width="1%" style="padding-top: 12px" nowrap><img src="images/CopyrightDivider.gif" height="12" width="3"></td>
    <td width="1%" style="padding-top: 12px" nowrap><a href="javascript:void(0)" onclick="openPrivacy();"><img src="images/Privacy.gif" height="12" width="59" border="0"></a></td>

    <td width="95%" style="padding-top: 12px; padding-right: 10px" align="right"><img src="images/Copyright.gif" height="12" width="152" border="0"></td>
  </tr>
</table>

</body>
</html:html>


