<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="org.owasp.encoder.Encode"%>
<%@page import="elms.util.StringUtils"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>

<html:html>

<html:base/>
<head>
<link rel="stylesheet" href="../css/elms.css" type="text/css">
<link rel="stylesheet" href="../css/online.css" type="text/css">

<%
 	String contextRoot = request.getContextPath();
 	String peopleId = request.getParameter("peopleId");
 	String tempOnlineID = request.getParameter("tempOnlineID");
 	elms.agent.PeopleAgent peopleAgent = new elms.agent.PeopleAgent();
	String contName = peopleAgent.getPeopleName(elms.util.StringUtils.s2i(peopleId));

  	//left side details bar display
  	elms.agent.OnlineAgent onlinePermitAgent = new elms.agent.OnlineAgent();

	onlinePermitAgent.updatePeopleId(StringUtils.s2i(tempOnlineID), "C", StringUtils.s2i(peopleId));
    java.util.List tempOnlineDetails = onlinePermitAgent.getTempOnlineDetails(elms.util.StringUtils.s2i(tempOnlineID));
%>
<script language="javascript">
function disableSubmit()
{
   	document.forms[0].button.disabled=true;
   	document.forms[0].termsCondition.checked=false;
	document.forms[0].print.checked=false;
	document.forms[0].print1.checked=false;
}
function enableSubmit()
{
    if(document.forms[0].termsCondition.checked==true && document.forms[0].print.checked==true && document.forms[0].print1.checked==true)
    {
	document.forms[0].button.disabled=false;
	}else
	{
	document.forms[0].button.disabled=true;
	}
}
function getSubmit()
{
	var tempOnlineID = '<%= tempOnlineID %>';
	document.forms[0].action='<%=contextRoot%>/checkOwner.do?tempOnlineID='+tempOnlineID;
	document.forms[0].submit();
}
</script>
</head>
<body text="#000000" leftmargin="5" bottommargin="0"  scroll="yes" topmargin="0" marginwidth="0" marginheight="40" rightmargin="0" onload="disableSubmit();">
<html:base/>
<center>
<style type="text/css">
    .FireTitle          { FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  td.FireHeading        { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none; PADDING: 10px; BACKGROUND-COLOR:#D9D9D9 }
    .FireHeading        { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireGlance         { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  td.FireResource       { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none; PADDING: 6px }
    .FireResource       { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
  td.FireResourceStatic { FONT-SIZE: 12px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none; PADDING: 6px }
    .FireResourceStatic { FONT-SIZE: 12px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireSelect         { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireText           { FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireHighlight      { FONT-SIZE: 15px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #990000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireMessage        { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
  td.FireMenu           { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none; PADDING: 2px; BACKGROUND-COLOR:#000000 }
    .FireMenu           { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:link      { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:visited   { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:active    { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:hover     { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireMenu1          { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:link     { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:visited  { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:active   { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:hover    { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #FFFFFF; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  @page Section1 {
                   size:8.5in 11.0in;
                   margin:0.5in 0.5in 0.5in 0.5in;
                 }
 p.MsoNormal, li.MsoNormal, div.MsoNormal {
                margin:0in;
                margin-bottom:.0001pt;
                font-size:12.0pt;
                font-family:"Arial"
              }
 table.atable {
                border-collapse:collapse;
                border:1px solid #B3B3B3
              }
 tr           {
                page-break-inside:avoid
              }
 div.Section1 {
                page:Section1;
              }

.panelVisited      { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #003265 }
.panelVisitedColor { background-color:#62A85F }
.panelActive       { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #FFFFFF }
.panelActiveColor  { background-color:#EAC563 }
.panel             { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #FFFFFF }
.panelColor        { background-color:#BB2F2F }

</style>

<html:form action="/checkOwner">
<html:hidden property="tempOnlineID" />
<table align="center" cellpadding="0" cellspacing="0" style="height:100%" width="100%">

<td align="center" valign="top" width="99%">
	<br><br> <br><br>

<fieldset style="width: 450px;height: 100%" >

<br> <br>

<table cellpadding="15" cellspacing="0" width="100%" >
 <html:errors />

  <tr>
        <td align="left"> <font class="panelVisited" style="font-size:30px">5. <font class="panelVisited" style="font-size:12px"><bean:message key="confContractor"/>
        </td>
   </tr>
	<tr align="left">
    <td class="tabletext" valign="top" colspan="3"><font class="panelVisited" style="font-size:12px">&nbsp;&nbsp;&nbsp;&nbsp;<%=contName%></td>
	</tr>

	<tr>
	<td class="tabletext" valign="top" colspan="3"><font color="red" style="font-family: Arial; font-size:12px">
	WARNING: FAILURE TO SECURE WORKERS? COMPENSATION COVERAGE IS UNLAWFUL, AND SHALL SUBJECT AN EMPLOYER TO CRIMINAL PENALTIES AND CIVIL FINES UP TO ONE HUNDRED THOUSAND DOLLARS ($100,000), IN ADDITION TO THE COST OF COMPENSATION, DAMAGES AS PROVIDED FOR IN SECTION 3706 OF THE LABOR CODE, INTEREST, AND ATTORNEY'S FEES.
	</td>
	</tr>

	<tr>
      <td colspan="3">
  		<span id="acctSub">
		    <table border="0" cellspacing="0" cellpadding="2" align="right">
				<tr>
				    <td class="tabletext" valign="top" colspan="3"><html:checkbox property="termsCondition" onclick="enableSubmit();"/><font color="red" style="font-family: Arial; font-size:11px">&nbsp;&nbsp;
				    Section 2-4-804 of the Burbank Municipal Code requires payment of business tax on all work being performed by a licensed contractor.  If a contractor is listed as being actively involved in this construction project, the tax must be paid prior to issuing the permit.  If a contractor is not listed, but is actively involved after issuance of the permit, the tax must be paid prior to scheduling inspections.
				    </td>
				</tr>
				<tr>
				    <td class="tabletext" valign="top" colspan="3"><html:checkbox property="print" onclick="enableSubmit();"/><font color="red" style="font-family: Arial; font-size:11px">&nbsp;&nbsp;
				    I hereby affirm under penalty of perjury that I am the contractor is licensed under provisions of Chapter 9, (commencing with Section 7000, ) of Division 3 of the Business and Professions Code, and that my license is in full force and effect.
				    </td>
				</tr>
				<tr>
				    <td class="tabletext" valign="top" colspan="3"><html:checkbox property="print1" onclick="enableSubmit();"/><font color="red" style="font-family: Arial; font-size:11px">&nbsp;&nbsp;
				    I hereby affirm under penalty of perjury one of the following declarations: I have and will maintain a certificate of consent to self-insure for workers' compensation, issued by the Director of Industrial Relations as provided for by Section 3700 of the Labor Code, for the performance of the work for which this permit is issued; or I have and will maintain workers' compensation insurance, as required by Section 3700 of the Labor Code, for the performance of the work for which this permit is issued; or I certify that, in the performance of the work for which this permit is issued, I shall not employ any person in any manner so as to become subject to the workers' compensation laws of California, and agree that, if I should become subject to the workers' compensation provision of Section 3700 of the Labor Code, I shall forthwith comply with those provisions.
				    </td>
				</tr>

				<tr>
			</table>
	      </span>
	    </td>
	  </tr>
               <tr>
                    <td class="FireSelect">
                    <div align="center">
                    <html:reset  value="Back" styleClass="FireSelect" style="width: 100px" onclick="history.back();"/>
                      <html:button  property="button" value="Next" styleClass="FireSelect" style="width: 100px" onclick="getSubmit()"/>
                      </div>
                </tr>

</table>
</fieldset>
</html:form>
</center>
	</td> 

			<jsp:include page="onlineSideBar.jsp" flush="true">
				<jsp:param name="page" value="5" />	
			</jsp:include>

 </table>
</body>
</html:html>
