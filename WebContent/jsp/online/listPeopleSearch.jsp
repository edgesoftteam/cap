<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<%
		java.util.List peopleSearchList = new java.util.ArrayList();
		peopleSearchList= (java.util.List)request.getAttribute("peopleSearchList");
		int size = peopleSearchList.size();
		String tempOnlineID= (String)request.getAttribute("tempOnlineID");
		String peopleType = (String)request.getAttribute("peopleType");

%>
<html:html>
<head>
<html:base/>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
	<link rel="stylesheet" href="../css/online.css" type="text/css">

</head>
<% String contextRoot = request.getContextPath();%>
<script language="JavaScript" src="../script/calendar.js"></script>
<script language="javascript" type="text/javascript">
//Getting PeopleId when checked
function getPeopleIdChecked(val)
{
document.forms[0].selectedId.value= val.value;

}

//Submitting the people record to be added based on peopleId
function moveNext()
{
    var type = '<%=peopleType%>';
    var peopleIs ="";
	if(type== 'C'){
		peopleIs =	"Contractor";
   	}else if(type == 'W'){
  		peopleIs = "Owner";
  	}else if(type == 'E'){
  		peopleIs = "Engineer";
  	}else if(type == 'R'){
  		peopleIs = "Architect";
  	}

  		if(document.forms[0].selectedId.value =="")
	{
	 alert("Please select the correct "+peopleIs+"");
	}else
	{
		 var tempOnlineID = document.forms[0].tempOnlineID.value;
		 var peopleId =  (document.forms[0].selectedId.value);
		 if('<%=peopleType%>' == 'C'){
		 	document.forms[0].action='<%=contextRoot%>/selectContractor.do?tempOnlineID='+tempOnlineID+'&peopleId='+peopleId+'&peopleType='+'<%=peopleType%>';
		 }else{
 		 	document.forms[0].action='<%=contextRoot%>/planCheckQuestionaire.do?tempOnlineID='+tempOnlineID+'&peopleId='+peopleId;
 		 }
		 document.forms[0].submit();
 	}
}

function addPeople(){
	var tempOnlineID = document.forms[0].tempOnlineID.value;
	document.forms[0].action='<%=contextRoot%>/addNewPeople.do?peopleType=<%=peopleType%>&tempOnlineID='+tempOnlineID;
	document.forms[0].submit();
}

function checkPeople(){
	if(<%=size%>==1){
	     document.forms[0].peopleIdval.checked = true;
		 getPeopleIdChecked(document.forms[0].peopleIdval);
	}
}
</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="checkPeople();">
<html:form name ="applyOnlinePermitForm" type="elms.control.beans.online.ApplyOnlinePermitForm" action="largerOngoingProject">
<html:hidden property="tempOnlineID" value="<%=tempOnlineID%>" />
<html:hidden property="selectedId" />

<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td class="tabletitle">
						            	<%
						  	if(peopleType.equalsIgnoreCase("C"))
						  	{
						  	%>
						  	Contractor
						  	<%
						  	}else if(peopleType.equalsIgnoreCase("W")){
						  	%>
						  	Owner
						  	<%}else if(peopleType.equalsIgnoreCase("E")){%>
						  	Engineer
						  	<%}else if(peopleType.equalsIgnoreCase("R")){%>
						  	Architect
						  	<%}%>
				           Search Results
				          </td>

                      <td width="1%" class="tablebutton"><nobr>
                             	<%
                             	 	if(!peopleType.equalsIgnoreCase("C")){
							  	%>
                    	 <html:button  property="new" value="New" style="width:100px" styleClass="FireSelect" onclick="addPeople();"/> 

			                   <%
			                   		}
			                   %>

						<html:reset  value="Back" style="width:100px" styleClass="FireSelect" onclick="history.back();"/>
						<%if(size != 0){%>
						<html:button  property = "next" value="Next" style="width:100px" styleClass="FireSelect" onclick="moveNext()"/>
						<%}%>
                      	</nobr>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr valign="top">
                      <td class="tablelabel"></td>
                       <%if(!peopleType.equalsIgnoreCase("W")){%>
                        <td class="tablelabel">License</td>
                         <%}%>
                      <td class="tablelabel">Name</td>
                       <%if(!peopleType.equalsIgnoreCase("W")){%>
                      <td class="tablelabel">Agent Name</td>
                      <%}%>

                     <td class="tablelabel">Address</td>
                    </tr>
 					<%
 					//Display of Search result Type of people contractor with their details
					for(int i=0;i<peopleSearchList.size();i++)
						{
					 		elms.control.beans.online.ApplyOnlinePermitForm applyOnlinePermitForm = (elms.control.beans.online.ApplyOnlinePermitForm) peopleSearchList.get(i);
                  %>
                    <tr class="tabletext">


	                        <td class="tabletext"><input type="radio" name="peopleIdval" value="<%=elms.util.StringUtils.i2s(applyOnlinePermitForm.getPeopleId())%>" onclick="getPeopleIdChecked(this)"></td>
	                        <%if(!peopleType.equalsIgnoreCase("W")){%>
	                       <td class="tabletext">
	                      	<%=applyOnlinePermitForm.getLicNo()%>
	                      </td>
	                      <%}%>
	                      <td class="tabletext">
	                      	<%=applyOnlinePermitForm.getName()%>
	                      </td>
	                        <%if(!peopleType.equalsIgnoreCase("W")){%>
	                      <td class="tabletext">
	                      	 <%=applyOnlinePermitForm.getAgentName()%>
	                      </td>
	                       <%}%>


	                      <td class="tabletext">
	                       	<%=applyOnlinePermitForm.getAddress()%>
	                      </td>

			        </tr>
                    <%
                    }
                    %>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>


                      <td width="1%" class="tablebutton" align="right"><nobr>
                             	<%
                             	 	if(!peopleType.equalsIgnoreCase("C")){
							  	%>
                    	 <html:button  property="new" value="New" style="width:100px" styleClass="FireSelect" onclick="addPeople();"/> 

			                   <%
			                   		}
			                   %>
                      	<html:reset  value="Back" styleClass="FireSelect" style="width:100px" onclick="history.back();"/></nobr>
                      	<%if(size != 0){%>
                      	<html:button  property = "next" value="Next" styleClass="FireSelect" style="width:100px" onclick="moveNext()"/>
                        &nbsp;&nbsp;
                        <%}%>
						</td>
                </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</html:form>
</body>
</html:html>

