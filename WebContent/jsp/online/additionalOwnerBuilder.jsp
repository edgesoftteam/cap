<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="org.owasp.encoder.Encode"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>

<html:html>
<head>
<%
 	String contextRoot = request.getContextPath();
 	String tempOnlineID = request.getParameter("tempOnlineID");

  	//left side details bar display
  	elms.agent.OnlineAgent onlinePermitAgent = new elms.agent.OnlineAgent();
    java.util.List tempOnlineDetails = onlinePermitAgent.getTempOnlineDetails(elms.util.StringUtils.s2i(tempOnlineID));
%>
<script language="javascript">
function disableSubmit()
{
   	document.forms[0].button.disabled=true;
   	for(var i=0;i<document.forms[0].elements.length;i++){
		if(document.forms[0].elements[i].type=='checkbox')
			document.forms[0].elements[i].checked=false
	}
}
function enableSubmit()
{
	for(var i=0;i<document.forms[0].elements.length;i++){
		if(document.forms[0].elements[i].type=='checkbox')
			if(document.forms[0].elements[i].checked==true){
				flag=true;
			}else{
				flag=false;
				break;
			}
	}

	if(flag){
		document.forms[0].button.disabled=false;
	}else{
		document.forms[0].button.disabled=true;
	}
}
function getSubmit()
{
	document.forms[0].action='<%=contextRoot%>/jsp/online/additionalOwnerBuilderCont.jsp?tempOnlineID=<%=tempOnlineID%>&peopleType=C';
	document.forms[0].submit();
}
</script>
</head>
<body text="#000000" leftmargin="5" bottommargin="0"  scroll="yes" topmargin="0" marginwidth="0" marginheight="40" rightmargin="0" onload="disableSubmit();">
<html:base/>
<center>
<style type="text/css">
    .FireTitle          { FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  td.FireHeading        { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none; PADDING: 10px; BACKGROUND-COLOR:#D9D9D9 }
    .FireHeading        { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireGlance         { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  td.FireResource       { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none; PADDING: 6px }
    .FireResource       { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
  td.FireResourceStatic { FONT-SIZE: 12px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none; PADDING: 6px }
    .FireResourceStatic { FONT-SIZE: 12px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireSelect         { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireText           { FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireHighlight      { FONT-SIZE: 15px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #990000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireMessage        { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
  td.FireMenu           { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none; PADDING: 2px; BACKGROUND-COLOR:#000000 }
    .FireMenu           { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:link      { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:visited   { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:active    { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:hover     { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireMenu1          { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:link     { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:visited  { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:active   { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:hover    { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #FFFFFF; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  @page Section1 {
                   size:8.5in 11.0in;
                   margin:0.5in 0.5in 0.5in 0.5in;
                 }
 p.MsoNormal, li.MsoNormal, div.MsoNormal {
                margin:0in;
                margin-bottom:.0001pt;
                font-size:12.0pt;
                font-family:"Arial"
              }
 table.atable {
                border-collapse:collapse;
                border:1px solid #B3B3B3
              }
 tr           {
                page-break-inside:avoid
              }
 div.Section1 {
                page:Section1;
              }

.panelVisited      { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #003265 }
.panelVisitedColor { background-color:#62A85F }
.panelActive       { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #FFFFFF }
.panelActiveColor  { background-color:#EAC563 }
.panel             { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #FFFFFF }
.panelColor        { background-color:#BB2F2F }

</style>

<html:form action="checkExistingPeople">
<html:hidden property="tempOnlineID" />
<html:hidden property="chkExistPeopleFlag" value="O" />
<table align="center" cellpadding="0" cellspacing="0" style="height:100%" width="100%">

<td align="center" valign="top" width="99%">
	<br><br> <br><br>

<fieldset style="width: 450px;height: 320px" >

<br> <br>

<table cellpadding="15" cellspacing="0" width="100%" >
 <html:errors />

  <tr>
        <td align="left"> <font class="panelVisited" style="font-size:30px">5. <font class="panelVisited" style="font-size:12px"><bean:message key="additionalOwnerBuilder"/>
        </td>
   </tr>
	<tr align="left">
    <td class="tabletext" valign="top" colspan="3"><font class="panelVisited" style="font-size:12px">
		<bean:message key="statOwnerBuilder"/>
	</td>
	</tr>
	<tr>
      <td colspan="3">
  		<span id="acctSub">
		    <table border="0" cellspacing="0" cellpadding="2" align="right">
				<tr>
				    <td class="tabletext" valign="top" colspan="3"><input type="checkbox" name="check1" onclick="enableSubmit()"><font color="red" style="font-family: Arial; font-size:11px">&nbsp;&nbsp;
				    	<bean:message key="checkMsg1"/>
				    </td>
				</tr>
				<tr>
				    <td class="tabletext" valign="top" colspan="3"><input type="checkbox" name="check2" onclick="enableSubmit()"><font color="red" style="font-family: Arial; font-size:11px">&nbsp;&nbsp;
				    	<bean:message key="checkMsg2"/>
				    </td>
				</tr>
				<tr>
				    <td class="tabletext" valign="top" colspan="3"><input type="checkbox" name="check3" onclick="enableSubmit()"><font color="red" style="font-family: Arial; font-size:11px">&nbsp;&nbsp;
				    	<bean:message key="checkMsg3"/>
				    </td>
				</tr>
				<tr>
				    <td class="tabletext" valign="top" colspan="3"><input type="checkbox" name="check4" onclick="enableSubmit()"><font color="red" style="font-family: Arial; font-size:11px">&nbsp;&nbsp;
				    	<bean:message key="checkMsg4"/>
				    </td>
				</tr>
				<tr>
				    <td class="tabletext" valign="top" colspan="3"><input type="checkbox" name="check5" onclick="enableSubmit()"><font color="red" style="font-family: Arial; font-size:11px">&nbsp;&nbsp;
				    	<bean:message key="checkMsg5"/>
				    </td>
				</tr>
				<tr>
				    <td class="tabletext" valign="top" colspan="3"><input type="checkbox" name="check6" onclick="enableSubmit()"><font color="red" style="font-family: Arial; font-size:11px">&nbsp;&nbsp;
				    	<bean:message key="checkMsg6"/>
				    </td>
				</tr>
			</table>
	      </span>
	    </td>
	  </tr>
               <tr>
                    <td class="FireSelect">
                    <div align="center">
                    <html:reset  value="Back" styleClass="FireSelect" style="width: 100px" onclick="history.back();"/>
                      <html:button  property="button" value="Next" styleClass="FireSelect" style="width: 100px" onclick="getSubmit()"/>
                      </div>
                </tr>




</table>
</fieldset>
</html:form>
</center>

	</td>

<!--Display of current entered data for permits-->
<td valign="top" width="1%" nowrap align="right">

 	<table style="width:200px; height:100%" cellpadding="0" cellspacing="0" bgcolor="#9FADBA" background="/elms/jsp/online/images/panelback.png" align="right">
   <%
	 for(int i=0;i<tempOnlineDetails.size();i++)
	 {
	 elms.control.beans.online.ApplyOnlinePermitForm applyOnlinePermitForm = (elms.control.beans.online.ApplyOnlinePermitForm)tempOnlineDetails.get(i);

	 %>
    <tr>
        <td valign="top">


            <table style="width:200px" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="8" class="panelVisitedColor"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="panelVisited" style="padding:8px">
                        <font style="font-size:18px">1 - ADDRESS<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="/elms/jsp/online/images/bullet.gif" height="9" width="9"></td>
                                <td class="panelVisited"><%=elms.util.StringUtils.nullReplaceWithEmpty(applyOnlinePermitForm.getAddress())%></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="8" class="panelVisitedColor"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="tabletext"><img src="space.gif" width="0" height="1"></td>
                </tr>


                <tr>
                    <td width="8" class="panelVisitedColor"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="panelVisited" style="padding:8px">
                        <font style="font-size:18px">2 - STRUCTURE<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="/elms/jsp/online/images/bullet.gif" height="9" width="9"></td>
                                <td class="panelVisited"><%=elms.util.StringUtils.nullReplaceWithEmpty(applyOnlinePermitForm.getStructureName())%></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="8" class="panelVisitedColor"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="tabletext"><img src="space.gif" width="0" height="1"></td>
                </tr>



                <tr>
                    <td width="8" class="panelVisitedColor"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="panelVisited" style="padding:8px">
                        <font style="font-size:18px">3 - PERMIT TYPE<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="/elms/jsp/online/images/bullet.gif" height="9" width="9"></td>
                                <td class="panelVisited"><%=elms.util.StringUtils.nullReplaceWithEmpty(applyOnlinePermitForm.getSubProjectName())%></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="8" class="panelVisitedColor"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="tabletext"><img src="space.gif" width="0" height="1"></td>
                </tr>

				 <tr>
                    <td width="8" class="panelVisitedColor"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="panelVisited" style="padding:8px">
                        <font style="font-size:18px">4 - VALUATION<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="/elms/jsp/online/images/bullet.gif" height="9" width="9"></td>
                                <td class="panelVisited"><%=elms.util.StringUtils.nullReplaceWithEmpty(applyOnlinePermitForm.getValuation())%></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="8" class="panelVisitedColor"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="tabletext"><img src="space.gif" width="0" height="1"></td>
                </tr>


				 <tr>
                    <td width="8" class="panelActiveColor"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="panelActive" style="padding:8px">
                        <font style="font-size:18px">5 - CONTRACTOR<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="bullet.gif" height="9" width="9"></td>
                                <td class="panelActive"><%=elms.util.StringUtils.nullReplaceWithEmpty(applyOnlinePermitForm.getContractorName())%></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="8" class="panelActiveColor"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="tabletext"><img src="space.gif" width="0" height="1"></td>
                </tr>


				 <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="panelActive" style="padding:8px">
                        <font style="font-size:18px">6 - OWNER<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="bullet.gif" height="9" width="9"></td>
                                <td class="panelActive"><%=elms.util.StringUtils.nullReplaceWithEmpty(applyOnlinePermitForm.getOwnerName())%></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="tabletext"><img src="space.gif" width="0" height="1"></td>
                </tr>

				 <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="panelActive" style="padding:8px">
                        <font style="font-size:18px">7 - PROJECT NAME<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="bullet.gif" height="9" width="9"></td>
                                <td class="panelActive"><%=elms.util.StringUtils.nullReplaceWithEmpty(applyOnlinePermitForm.getProjectName())%></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="tabletext"><img src="space.gif" width="0" height="1"></td>
                </tr>


                 <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="panelActive" style="padding:8px">
                        <font style="font-size:18px">8 - START DATE<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="bullet.gif" height="9" width="9"></td>

                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="tabletext"><img src="space.gif" width="0" height="1"></td>
                </tr>

                                 <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="panelActive" style="padding:8px">
                        <font style="font-size:18px">9 - PLAN CHECK<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="bullet.gif" height="9" width="9"></td>

                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="tabletext"><img src="space.gif" width="0" height="1"></td>
                </tr>

                  <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="0"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="panelActive" style="padding:8px">
                        <font style="font-size:18px">10 - COMPLETE / PAY<br>
                        <table cellpadding="4" cellspacing="0" border="0">
                            <tr>
                                <td><img src="bullet.gif" height="9" width="9"></td>
                                <td class="panelActive"></td>
                            </tr>
                        </table>
                    </td>
                </tr>

 				 <tr>
                    <td width="8" class="panelColor"><img src="space.gif" width="8" height="1"></td>
                    <td width="1" class="tabletext"><img src="space.gif" width="1" height="0"></td>
                    <td class="tabletext"><img src="space.gif" width="0" height="1"></td>
                </tr>


            </table>
        </td>
    </tr>
<%}%>
</table>
</body>
 </td>
 </table>
 	<!--End Display of current entered data for permits-->
</html:html>