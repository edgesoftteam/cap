<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ page import="elms.util.StringUtils,java.util.*,elms.common.*,java.text.SimpleDateFormat"%>
<% 
String contextRoot = request.getContextPath();

%>
<app:checkLogon/>
<html:html>
<head>
<script>
window.onload = function() {
    if(!window.location.hash) {
        window.location = window.location + '#loaded';
        window.location.reload();
    }
}
<%-- window.open("contextRoot/contacts.do","Oelms","toolbar=no,width=800,height=400,screenX=150,left=250,screenY=150,top=1,location=no,status=no,menubar=no,scrollbars=no,resizable=yes");
--%>
function dohref(strHref)
{
   if (strHref == 'f_content') parent.f_content.location.href="welcomeOnline.jsp";
   else if (strHref == 'contacts') 
	   parent.f_content.location.href="<%=contextRoot%>/contacts.do";
   	}
</script>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
<style type="text/css">
	table.csui, table.csui_title { width: 100%; padding: 0px; }
	table.csui { border-spacing: 1px; border-collapse: separate; background-color: #003366;}
	td.csui { padding: 6px; font-family:  Arial, Helvetica, sans-serif; font-size: 2px; background-color: #ffffff; }
	
	a.csui, a.csuisub { font-family:  Arial, Helvetica, sans-serif; color: white; text-decoration: none; font-size: 15px; }
	td.csui_header, td.csui_label { text-align:center;color:white;padding: 6px; font-family:  Arial, Helvetica, sans-serif; font-size: 17.5px; background-color: #003366; }

</style>
</head>
<body text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" background="../images/frames/top/background.gif" onload="refreshPage()">
<%
    //get user from session.
    elms.security.User user = (elms.security.User)session.getAttribute(elms.common.Constants.USER_KEY);
    //get full name of the user
   	String fullName = user.getFullName()!=null?user.getFullName():"";
	//get the menu highlight parameter

%>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="top" colspan="2" background="../images/frames/top/ShadowBottom.png"><img src="../images/spacer.gif" width="1" height="5"></td>
    </tr>
    <tr>
        <td valign="top">
        <table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><img src="../images/spacer.gif" width="10" height="1"></td>
                <td><img src="../images/site_logo_shield.png" width="33" height="35"></td>
                <td><img src="../images/spacer.gif" width="10" height="1"></td>
                <td><img src="../images/citylogo.png" width="413" height="35"></td>
            </tr>
        </table>
        </td>
        <td align="right">
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td width="250" align="right"><font class="white1" ><%=fullName.toUpperCase() %></font> | <font class="white1">v <%=Version.getNumber()%>&nbsp;&nbsp;</font>&nbsp;</td>
                </tr>
            </table>
        </td>

    </tr>
    <tr>
        <td valign="top" colspan="2" background="../images/frames/top/ShadowTop.png"><img src="../images/spacer.gif" width="1" height="5"></td>
    </tr>
    <tr>
        <td valign="top" colspan="2" bgcolor="#e5e5e5"  style="border-bottom: 1px solid #c9c9c9">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="csui">
            <tr>
                <td>
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr valign="middle"> 
                        
                        <td width="1"><img src="../images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="58" align="center">
                        	    <a href="javascript:dohref('f_content')" class="csui">Home</a>
                        </td>
                        
                        <td width="1" align="center"><img src="../images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="84" align="center">
                        	    <a href="javascript:dohref('contacts')" class="csui">Contact Us</a>
                        </td>

                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="70" align="center">
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/logout.do?f=o'"  class="csui">Logout</a>
                        </td>
                        <td width="1" align="center"><img src="../images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        
                    </tr>
                </table>
                </td>
            </tr>
        </table>
        </td>
    </tr>
    <tr>
        <td valign="top" colspan="2" bgcolor="#e5e5e5"><br><br><br><br><br><br></td>
    </tr>
</table>
</body>
</html:html>
