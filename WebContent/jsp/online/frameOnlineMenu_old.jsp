<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ page import="java.util.*,java.text.SimpleDateFormat" %>

<html:html>
<head>
<title>City of Burbank :Permitting and Licensing System: Online Explorer Frame</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%
    String contextRoot = request.getContextPath();
%>
<script>
function dohref(strHref)
{
   if (strHref == 'ireq')      parent.f_content.location.href="<%=contextRoot%>/inspectionRequest.do?action=req";
   else if (strHref == 'ires') parent.f_content.location.href="<%=contextRoot%>/inspectionResults.do?action=res";
   else if (strHref == 'cancel') parent.f_content.location.href="<%=contextRoot%>/inspectionResults.do?action=cancel";
   else if (strHref == 'profile') parent.f_content.location.href="<%=contextRoot%>/editProfile.do";
   else if (strHref == 'permits') parent.f_content.location.href="<%=contextRoot%>/myPermits.do?viewPermit=Yes";
   else if (strHref == 'inactive') parent.f_content.location.href="<%=contextRoot%>/myPermits.do?viewPermit=Yes&inActive=Yes";
   else if (strHref == 'apply'){
		parent.f_content.location.href="<%=contextRoot%>/applyPermits.do";
	}
    else if (strHref == 'pay') parent.f_content.location.href="<%=contextRoot%>/onlinePayment.do";
    else if (strHref == 'dExmp') parent.f_content.location.href="<%=contextRoot%>/onlinePayment.do";
}

function viewPermits(){
	if(document.all.menu1a1.style.display =='block'){
		document.all.menu1a1.style.display='none';
	} else{
		document.all.menu1a1.style.display='block';
	}
}

</script>
<body bgcolor="#e5e5e5" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" background="../images/site_bg_e5e5e5.jpg">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td background="../images/site_bg_B7C1CB.jpg"><img src="../images/spacer.gif" width="10" height="8"><font class="black2b">&nbsp;</td>
  </tr>
  <tr>
    <td background="../images/site_bg_B7C1CB.jpg"><img src="../images/spacer.gif" width="201" height="1"></td>
  </tr>
  <tr>
    <td><img src="../images/spacer.gif" width="1" height="10"></td>
  </tr>
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr >
            <td><font class="treetext">
            <img src="../images/spacer.gif" width="1" height="6"><br>

		 <!-- My Permits -->
			<span id="menu1" onclick="viewPermits();"> <font class="treetext"><img src="images/spacer.gif" width="9" height="1"><img src="images/site_blt_dot_003366.gif">&nbsp;<html:link href="#" styleClass="blue3b">&nbsp;MY PERMITS</html:link></span><br>
               <img src="../images/spacer.gif" width="1" height="6"><br>
               <span id="menu1a1">
		       <nobr><img src="../images/spacer.gif" width="19" height="1"><img src="images/site_blt_dot_003366.gif">&nbsp;<html:link href="javascript:dohref('permits')"  styleClass="blue2b2">ACTIVE PERMITS</html:link></nobr><br>
    		   <img src="../images/spacer.gif" width="1" height="6"><br>
		       <nobr><img src="../images/spacer.gif" width="19" height="1"><img src="images/site_blt_dot_003366.gif">&nbsp;<html:link href="javascript:dohref('inactive')"  styleClass="blue2b2">CLOSED PERMITS</html:link></nobr><br>
    		   <img src="../images/spacer.gif" width="1" height="6"><br>
             </span>




		 <!-- My Profile -->
			<nobr>
				<img src="../images/spacer.gif" width="5" height="1">
				<img src="../images/site_blt_dot_003366.gif">&nbsp;
				<html:link href="javascript:dohref('profile')"  styleClass="blue3b">MY PROFILE</html:link>
			</nobr><br>
            <img src="../images/spacer.gif" width="1" height="6"><br>



            <span id="menu2" onclick="if(document.all.menu2a1.style.display =='block'){document.all.menu2a1.style.display='none';} else{document.all.menu2a1.style.display='block';}"> <font class="treetext"><img src="images/spacer.gif" width="9" height="1"><img src="images/site_blt_dot_003366.gif">&nbsp;<html:link href="#" styleClass="blue3b">&nbsp;INSPECTIONS</html:link></span><br>
               <img src="../images/spacer.gif" width="1" height="6"><br>
               <span id="menu2a1">
		       <nobr><img src="../images/spacer.gif" width="19" height="1"><img src="images/site_blt_dot_003366.gif">&nbsp;<html:link href="javascript:dohref('ireq')"  styleClass="blue2b2">REQUEST INSPECTION</html:link></nobr><br>
    		   <img src="../images/spacer.gif" width="1" height="6"><br>
		       <nobr><img src="../images/spacer.gif" width="19" height="1"><img src="images/site_blt_dot_003366.gif">&nbsp;<html:link href="javascript:dohref('ires')"  styleClass="blue2b2">INSPECTION RESULTS</html:link></nobr><br>
    		   <img src="../images/spacer.gif" width="1" height="6"><br>
               <nobr><img src="../images/spacer.gif" width="19" height="1"><img src="images/site_blt_dot_003366.gif">&nbsp;<html:link href="javascript:dohref('cancel')"  styleClass="blue2b2">CANCEL INSPECTION</html:link></nobr><br>
               <img src="../images/spacer.gif" width="1" height="6"><br>
             </span>

          	<nobr>
				<img src="../images/spacer.gif" width="5" height="1">
				<img src="../images/site_blt_dot_003366.gif">&nbsp;
				<html:link href="javascript:dohref('apply')"  styleClass="blue3b">APPLY FOR A PERMIT</html:link>
			</nobr><br>
            <img src="../images/spacer.gif" width="1" height="6"><br>

            </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html:html>
