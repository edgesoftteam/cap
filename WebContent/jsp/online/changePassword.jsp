<%@page import="org.owasp.encoder.Encode"%>
<%@page import="elms.util.db.Wrapper"%>

<%@page import="elms.common.Constants"%>
<%@page import="java.util.*"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% 
String userName = request.getParameter("username");
String userEmailId = (String) session.getAttribute("usernameemailId");
if(userEmailId == null || userEmailId.length() == 0) {
	userEmailId = "";
}
String contextRoot = request.getContextPath();
String error = (String) request.getAttribute("error");
%>

<html>
<head>
	<title>Online Services</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="<%=contextRoot%>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<%=contextRoot%>/css/font-awesome.min.css">
	<link rel="stylesheet" href="<%=contextRoot%>/css/style.css">
	<script src="<%=contextRoot%>/jsp/js/jquery.min.js"></script>
	<script src="<%=contextRoot%>/jsp/js/bootstrap.min.js"></script>
	<script src="<%=contextRoot%>/jsp/js/default.js"></script>
	<script src="<%=contextRoot%>/jsp/js/formValidation.js"></script>
	<script>
/*
 *Initializing the form fields when body is loading 
 */
function init() {
	var userName = '<%=userName%>';
	if('null' == userName) {
		userName = '';
	}
	document.forms[0].elements['username'].value = userName;
	document.forms[0].elements['password'].value = '';
	document.forms[0].elements['newPassword'].value = '';
	document.forms[0].elements['confirmPassword'].value = '';	
}

function showMessage(errorMessage, color){
	document.getElementById("profile_status").innerHTML = errorMessage;
	document.getElementById('profile_status').style.color = color;	 
}
function hideMessage(){
	document.getElementById('profile_status').style.display = "none";
}
function validateAll() {
	var temp = '<%=userEmailId%>';
	if(temp != null && temp.length >0) {
		document.forms[0].elements['username'].value = temp;
	}
	
	
	if (document.forms[0].elements['newPassword'].value != ''){
		var pass = document.forms[0].elements['newPassword'].value;
		//alert(txt);
		 var regularExpression = /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/;
	      if(!regularExpression.test(pass) ) {
		       alert('New Password should be a combination of alphabets and numbers');
		       document.forms[0].elements['newPassword'].focus() ;
		       return false;
		    } 
		 }
	
	if(document.forms[0].elements['username'].value == "")  {
		alert('Please Enter Email Address' + document.forms[0].elements['username'].value);
		document.forms[0].elements['username'].focus();
		return false;
	}else
	if(document.forms[0].elements['password'].value == '') {
		alert('Please Enter current password');
		document.forms[0].elements['password'].focus();
		return false;
	}else
	if(document.forms[0].elements['newPassword'].value == '') {
		alert('Please Enter New Password');
		document.forms[0].elements['newPassword'].focus();
		return false;
	}else
	if(document.forms[0].elements['confirmPassword'].value == '') {
		alert('Please Enter Confirm New Password');
		document.forms[0].elements['confirmPassword'].focus();
		return false;
	} else if (document.forms[0].elements['newPassword'].value.length > 8){
		alert("Enter New Password of up to 8 characters");
		document.forms[0].elements['newPassword'].focus();
		return false;
	}else
	if(document.forms[0].elements['newPassword'].value.length< 6)
	{
		alert('New Password length should be > 6');
		document.forms[0].elements['newPassword'].focus();
		return false;
	}else
	if(document.forms[0].elements['newPassword'].value!=document.forms[0].elements['confirmPassword'].value)
	{
		alert('New Password and Confirm Password do not match');
		document.forms[0].elements['newPassword'].value = '';
		document.forms[0].elements['confirmPassword'].value = '';	
		document.forms[0].elements['newPassword'].focus();
		return false;	
	}
}

function cancel() {
	document.forms[0].action = "<%=contextRoot%>/changePassword.do";
	document.forms[0].submit();
}
</script>
</head>
<body>
	<div class="container-fluid">
	  
	    <div class="clear20"></div>
	    <div class="row">
	   		<div class="col-lg-12">
		    	<div class="heading">
		    		<%
					    error = (String) request.getAttribute("error");
						String success = (String) request.getAttribute("success");  
						if (error != null || success != null) {
					%>
					<div id="profile_status">
						<script type="text/javascript">
							var error = '<%=error%>';
							var success = '<%=success%>';
							var message = success;
							var color = 'Green';
							if(error != 'null') {
								message = error;
								color = 'red';
							}
							showMessage(message, color);
						</script>
					</div>
					<%
						}
					%>
				</div>
			</div>
		</div>
	    <div class="clear20"></div>
		<div class="container">
			<div class="row">
				<div class="col-lg-12 ">   
		        	<%-- <p>
					<span class="icon fa-phone-square"><a href="<%=contextRoot%>/getAccount.do" style="text-decoration: none;color:#36515E"> CONTACT</a></span> &nbsp;&nbsp;
					<span class="icon fa-credit-card"><a  href="<%=contextRoot%>/getReg.do"  style="text-decoration: none;color:#36515E"> ACCOUNT DETAILS</a></span>
					</p> --%>

					<html:form name="loginForm" type="elms.control.beans.online.LoginForm" styleClass="form-horizontal" onsubmit="return validateAll()" method="post" action="/changePassword.do">
						<div class="panel panel-default">
		                    <div class="panel-heading">
		                    	<div id="title"> CHANGE PASSWORD</div>
		                    </div>
							<div class="panel-body">
								<fieldset class="scheduler-border">					
									<div class="form-group">
										<label class="control-label col-sm-4" for="username">Email Address:</label>
										<div class="col-sm-4 icon-addon addon-l">				
											<%
												if(userEmailId != null && userEmailId.length() > 0) {							
											%>			
											<%=userEmailId %>
											<html:hidden property="username" />
											<%
												} else {
											%>
											<html:text value="" property="username" size="15" styleClass="form-control" onkeypress="hideMessage();" onblur="validateEmail()"/>
											<% } %>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-4" for="pwd">Current Password</label>
										<div class="col-sm-4">
											<html:password styleClass="form-control" property="password" onkeypress="hideMessage();" onblur="passwordLength(this,'password')"/>							
										</div>
									</div>
										<div class="form-group">
										<label class="control-label col-sm-4" for="pwd">New Password</label>
										<div class="col-sm-4">
											<html:password styleClass="form-control" property="newPassword" onkeypress="hideMessage();" onblur="passwordLength(this,'newPassword')"/>							
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-4" for="pwd">Confirm Password</label>
										<div class="col-sm-4">
											<html:password styleClass="form-control" property="confirmPassword" onkeypress="hideMessage();" onblur="passwordLength(this,'confirmPassword')"/>							
										</div>
									</div>
									<div class="form-group">
									<div class="col-sm-offset-4 col-sm-6">
											<button type="submit" styleClass="button"
												style="width: 150px">Change Password</button> &nbsp;&nbsp;
												<button onclick="cancel()" type="button" styleClass="button"
												style="width: 80px">Cancel</button>
									</div></div>
									<div class="form-group">
									<div class="col-sm-offset-4 col-sm-6">
											
									</div>
									</div>
								</fieldset>
							</div>
						</div>
					</html:form>
				</div>
			</div>
		</div>
	
	</div>
</body>
</html>