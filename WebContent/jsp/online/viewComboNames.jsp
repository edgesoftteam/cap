<%@page import="org.owasp.encoder.Encode"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%
 	String contextRoot = request.getParameter("contextRoot");
 	java.util.List comboNameList= new java.util.ArrayList();
 	comboNameList= (java.util.List)request.getAttribute("comboNameList");
%>
<html:html>
<head>

<script language="javascript">
function viewComboNames(){

window.open("<%=contextRoot%>/viewComboName.do?contextHttpsRoot=<%=contextRoot%>","Oelms","toolbar=no,width=900,height=600,screenX=100,left=100,screenY=100,top=1,location=no,status=no,menubar=no,scrollbars=no");
}

</script>
<style type="text/css">
<!--
td {
	font: 12px Arial, Helvetica, sans-serif;
}
body {
	font: 12px Arial, Helvetica, sans-serif;
}
-->
</style>
<title>
City of Burbank :Permitting and Licensing System - Combo Names
</title>
</head>
<body scroll="yes">
<center>
<!-- <p><img src="/elms/jsp/online/images/BHOBCLogo.gif" width="500" height="116" /><br> -->
</p>
<table width="500" border="1" cellpadding="5" cellspacing="0" bordercolor="#000000">
  <col width="155">
  <col width="628">
  <tr height="20">
    <td height="20" colspan="2" valign="top" bgcolor="#033568"><strong><font color="#FFFFFF" size="4">PERMIT NAMES LEGEND </strong></td>
    </tr>
<tr height="20">
    <td height="20" valign="top" bgcolor="#B7C1CB"><i>On-line permits are currently available for single-family residential projects only</i></td>

  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top" bgcolor="#B7C1CB"><strong>PERMIT NAMES</strong></td>

  </tr>
<logic:iterate id="combo" name="comboNameList"  type="elms.control.beans.online.ApplyOnlinePermitForm"  scope="request">
  <tr height="20">
    <td height="20" valign="top"nowrap="nowrap"><strong><bean:write name="combo" property="subProjectName"/></strong></td>
  </tr>
  </logic:iterate>

</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</center>
</body>

</html:html>

