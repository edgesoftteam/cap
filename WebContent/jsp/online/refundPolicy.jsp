<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="org.owasp.encoder.Encode"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%
 	String contextRoot = request.getContextPath();
 	//left side details bar display
    java.util.List tempOnlineDetails = new java.util.ArrayList();
    tempOnlineDetails= (java.util.List)request.getAttribute("tempOnlineDetails");
    int peopleTypeId= elms.util.StringUtils.s2i((String)request.getAttribute("peopleTypeId"));
 	String forcePC=(String)request.getAttribute("forcePC");
 	String contextHttpsRoot =(String) request.getParameter("contextHttpsRoot");
%>
<html:html>
<head>

<script language="javascript">


</script>

<title>
City of Burbank :Permitting and Licensing System: - Refund Policy
</title>
</head>
<body text="#000000" leftmargin="5" bottommargin="0"  scroll="yes" topmargin="0" marginwidth="0" marginheight="40" rightmargin="0">



<html:base/>
<%

    java.util.List streetList = new elms.agent.AddressAgent().getStreetArrayList();
  	pageContext.setAttribute("streetList", streetList);
%>

<center>
<style type="text/css">
    .FireTitle          { FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  td.FireHeading        { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none; PADDING: 10px; BACKGROUND-COLOR:#D9D9D9 }
    .FireHeading        { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireGlance         { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  td.FireResource       { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none; PADDING: 6px }
    .FireResource       { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
  td.FireResourceStatic { FONT-SIZE: 12px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none; PADDING: 6px }
    .FireResourceStatic { FONT-SIZE: 12px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireSelect         { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireText           { FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireHighlight      { FONT-SIZE: 15px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #990000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireMessage        { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
  td.FireMenu           { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none; PADDING: 2px; BACKGROUND-COLOR:#000000 }
    .FireMenu           { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:link      { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:visited   { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:active    { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:hover     { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireMenu1          { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:link     { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:visited  { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:active   { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:hover    { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #FFFFFF; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  @page Section1 {
                   size:8.5in 11.0in;
                   margin:0.5in 0.5in 0.5in 0.5in;
                 }
 p.MsoNormal, li.MsoNormal, div.MsoNormal {
                margin:0in;
                margin-bottom:.0001pt;
                font-size:12.0pt;
                font-family:"Arial"
              }
 table.atable {
                border-collapse:collapse;
                border:1px solid #B3B3B3
              }
 tr           {
                page-break-inside:avoid
              }
 div.Section1 {
                page:Section1;
              }

.panelVisited      { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #003265 }
.panelVisitedColor { background-color:#62A85F }
.panelActive       { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #FFFFFF }
.panelActiveColor  { background-color:#EAC563 }
.panel             { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #FFFFFF }
.panelColor        { background-color:#BB2F2F }

</style>


<html:form action="checkExistingArchitect">
<html:hidden property="tempOnlineID" />
<table cellpadding="15" cellspacing="0" width="100%">
 <html:errors />


    <tr>
        <td valign=top align="center"><img src="<%=contextRoot%>/jsp/online/images/Shield.gif" width="100" height="100"></td>
    </tr>
    		<tr>
	        <td> <font class="panelVisited" style="font-size:18px">Refund Policy </td>
	        </tr>

	        <tr>
	        <td> <font class="panelVisited" style="font-size:12px"> Fees shall be assessed in accordance with the provisions of the Burbank Fee Resolution. </td>
	        </tr>

	         <tr>
	        <td> <font class="panelVisited" style="font-size:12px"> Refunds shall be in accordance with the Refund Policy established by the Building Official as authorized under the provisions of the California Building Code.</td>
	        </tr>

	         <tr>
	        <td> <font class="panelVisited" style="font-size:12px"> PLAN CHECK:  When an application for a permit, for which a plan review fee has been paid, is withdrawn or canceled before any plan review services are performed, and a request for refund is submitted within 180 days of the cancellation or expiration of the application, not more than 80 percent of the plan review fee paid shall be refunded.  The City shall retain 20 percent for administrative costs already absorbed from the initial application.  Each request for refund must be in writing and submitted for approval by the Building Division.</td>
	        </tr>

	         <tr>
	        <td> <font class="panelVisited" style="font-size:12px"> PERMITS:  When a permit issued in accordance with the California Building Codes, and for which no work has been performed, is withdrawn or cancelled, and a request for refund is submitted within 180 days of the cancellation or expiration of the application, not more than 80 percent of the permit fee paid shall be refunded.  The City shall retain 20 percent for administrative costs already absorbed from the initial application.  Each request for refund must be in writing and submitted for approval by the Building Division.</td>
	        </tr>

	         <tr>
	        <td> <font class="panelVisited" style="font-size:12px"> WORK PERFORMED:  When plan check or inspection services have been performed no refund shall be authorized.  </td>
	        </tr>

	        <tr>
	        <td> <font class="panelVisited" style="font-size:12px"> PERMITS ISSUED IN ERROR:  Any fee which was erroneously paid or collected shall be refunded.</td>
	        </tr>

			<tr>
	        <td> <font class="panelVisited" style="font-size:12px"> The Building Official may grant modifications for individual cases upon written request by the applicant, provided the building official shall first find that special individual reason makes the strict letter of this policy impractical.</td>
	        </tr>


       <tr><td>&nbsp;</td></tr>

			<tr>
                    <td class="FireSelect">
                    <div align="center">
                      <html:button  property="button" value="Close" styleClass="FireSelect" style="width: 100px" onclick="javascript:window.close();"/>

                      </div>
                </tr>


</table>

</html:form>


</body>

</html:html>

