<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<html:html>
<app:checkLogon/>



<head>
<html:base/>
<title>City of Burbank :Permitting and Licensing System:  Email Templates </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%
String contextRoot = request.getContextPath();
String message = (String)request.getAttribute("message");
if(message==null) message="";
%>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<html:form action="/emailAdmin?action=save">


<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Email Templates Administration<br>
            <br>
            </td>
        </tr>
        <TR>
        <TD>
        <font color='green'><b><%=message%></b>
        </TD>
        </TR>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" background="../images/site_bg_B7C1CB.jpg">Available Templates
                        </td>

                      <td width="1%" class="tablelabel"><nobr>
					  <html:submit value="Update" styleClass="button"/>
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr valign="top">
                      <td class="tablelabel" >Inspection Request</td>
                      <td class="tabletext">
                        <html:textarea rows="5" cols="80" property="inspectionRequestedMessage"/>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel" >Inspection Scheduled</td>
                      <td class="tabletext">
                        <html:textarea rows="5" cols="80" property="inspectionScheduledMessage"/>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel" >Inspection Cancelled</td>
                      <td class="tabletext">
                        <html:textarea rows="5" cols="80" property="inspectionCancelledMessage"/>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel" >Activity Status Change</td>
                      <td class="tabletext">
                        <html:textarea rows="5" cols="80" property="permitStatusChangeMessage"/>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel" >Registration Successful</td>
                      <td class="tabletext">
                        <html:textarea rows="5" cols="80" property="registrationSuccessMessage"/>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel" >Forgot Password</td>
                      <td class="tabletext">
                        <html:textarea rows="5" cols="80" property="forgotPasswordMessage"/>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel" >Temporary Password</td>
                      <td class="tabletext">
                        <html:textarea rows="5" cols="80" property="tempPasswordChangeMessage"/>
                      </td>
                    </tr>
                     <tr valign="top">
                      <td class="tablelabel" >Payment Successful</td>
                      <td class="tabletext">
                        <html:textarea rows="5" cols="80" property="paymentChangeMessage"/>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</html:form>
</body>
</html:html>
