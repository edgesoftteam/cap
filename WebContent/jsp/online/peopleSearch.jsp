<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%
 	String contextRoot = request.getContextPath();
 	String peopleType = (String)request.getAttribute("peopleType");
 	String showText = (String)request.getAttribute("showContractorText") != null?(String)request.getAttribute("showContractorText") :"N";

%>
<html:html>
<head>
<html:base/>
<link rel="stylesheet" href="../css/elms.css" type="text/css">
	<link rel="stylesheet" href="../css/online.css" type="text/css">

<script language="javascript">

function peopleSearch()
{

	 var type = '<%=peopleType%>';
	    var peopleIs ="";
		if(type== 'C'){
			peopleIs =	"Please enter the name or license number to start your search";
	   	}else if(type == 'W'){
	  		peopleIs = "Please enter the name to start your search";
	  	}else if(type == 'E'){
	  		peopleIs = "Please enter the name or e-mail address of an engineer previously registered with the City of Burbank";
	  	}else if(type == 'R'){
	  		peopleIs = "Please enter the name or e-mail address of an architect previously registered with the City of Burbank";
	  	}else{
	  		peopleIs ="Please enter the name to start your search.";
	  	}


	if(document.forms[0].searchEntry.value == "")
	{
		alert(peopleIs);
		document.forms[0].searchEntry.focus();
	}else
	{
	 var tempOnlineID = document.forms[0].tempOnlineID.value;
	document.forms[0].action='<%=contextRoot%>/peopleSearchResult.do?tempOnlineID='+tempOnlineID+'&peopleType='+type;
	document.forms[0].submit();
	}
}


</script>
</head>
<body text="#000000" leftmargin="5" bottommargin="0"  scroll="yes" topmargin="0" marginwidth="0" marginheight="40" rightmargin="0">



<%

    java.util.List streetList = new elms.agent.AddressAgent().getStreetArrayList();
  	pageContext.setAttribute("streetList", streetList);
  	  //left side details bar display
    java.util.List tempOnlineDetails = new java.util.ArrayList();
    tempOnlineDetails= (java.util.List)request.getAttribute("tempOnlineDetails");
%>

<center>



<html:form action="peopleSearchResult">
<html:hidden property="tempOnlineID" />
<table align="center" cellpadding="0" cellspacing="0" style="height:100%" width="100%">

<td align="center" valign="top" width="99%" >
	<br><br> <br><br>
<fieldset style="width: 450px;height: 620px" >

<table cellpadding="15" cellspacing="0" width="100%">
 <html:errors />

 <tr>
        <td align="center"> <font class="panelVisited" style="font-size:30px">&nbsp; <font class="panelVisited" style="font-size:12px">&nbsp;
        </td>
   </tr>
  <tr>
  	<td align="left">
  	<%
  	if(peopleType.equalsIgnoreCase("C"))
  	{
  	%>
  	 <font class="panelVisited" style="font-size:30px">5<font class="panelVisited" style="font-size:14px"><bean:message key="contractor"/>
  	<%
  	}else if(peopleType.equalsIgnoreCase("W")){
  	%>
  	 <font class="panelVisited" style="font-size:30px">6<font class="panelVisited" style="font-size:14px"><bean:message key="owner"/>
  	<%}else if(peopleType.equalsIgnoreCase("E")){%>
  	 <font class="panelVisited" style="font-size:30px">9<font class="panelVisited" style="font-size:14px"><bean:message key="engineer"/>
  	<%}else if(peopleType.equalsIgnoreCase("R")){%>
   <font class="panelVisited" style="font-size:30px">9<font class="panelVisited" style="font-size:14px"><bean:message key="architect"/>
  	<%}%>
  	</td>
  </tr>

    <tr>

    	<td class="tabletext" align="center"><font class="panelVisited" style="font-size:14px"><bean:message key="search"/> :
    	 <html:text property="searchEntry"  />


    <html:select property="searchBased">
     <option value="FS">Full Search</option>
     <%if(!peopleType.equalsIgnoreCase("C")){%>
     <option value="NAME">Name</option>
     <%}%>
      <%if(peopleType.equalsIgnoreCase("C")){%>
	 <option value="LIC_NO">License Number</option>
	 <%}%>
	 </html:select>
	</td>


	</tr>
	
	<tr>
                    <div align="right">
                    <td align="center" class="FireSelect">

                    <html:reset  value="Back" styleClass="FireSelect" style="width: 100px" onclick="history.back();"/>

	<html:button property="pSearch" value="Search" styleClass="FireSelect" style="width: 100px" onclick="peopleSearch()"/> </td>
	</div>
                </tr>

	<%if(showText.equalsIgnoreCase("Y")){ %>
	<tr>
		<td align ="left" class="con_text_red1"><bean:message key="iContractorSearchText"/> </td>
	</tr>
	<%}%>

</table>

</center>
</html:form>
</fieldset>
	</td>

			<jsp:include page="onlineSideBar.jsp" flush="true">
				<jsp:param name="page" value="6" />
			</jsp:include>
 </table>
</html:html>

