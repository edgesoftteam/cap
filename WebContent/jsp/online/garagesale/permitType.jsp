<%@page import="elms.agent.AddressAgent"%>
<%@page import="elms.control.beans.online.OnlinePermitForm"%>
<%@page import="elms.util.Operator"%>
<%@page import="elms.util.StringUtils"%>
<%@page import="elms.app.common.DisplayItem"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<html>
<%String contextRoot = request.getContextPath();	
List streets = new ArrayList(); // for the Street list
System.out.println("Street Size :"+streets.size());
streets = (List) request.getAttribute("streetNameList");
request.setAttribute("streets",streets); 

OnlinePermitForm onlinePermitForm = (OnlinePermitForm) request.getAttribute("onlinePermitFrom");
String streetName = "";
String streetNo = "";
String unit ="";
if(onlinePermitForm != null){
	streetName = StringUtils.nullReplaceWithEmpty(onlinePermitForm.getStreetName()).equalsIgnoreCase("Non Locational")?"":onlinePermitForm.getStreetName();
	streetNo = StringUtils.nullReplaceWithEmpty(onlinePermitForm.getStreetNum()).equals("0")?"":onlinePermitForm.getStreetNum();
	unit = StringUtils.nullReplaceWithEmpty(onlinePermitForm.getStreetUnit()).equals("")?"":onlinePermitForm.getStreetUnit();
}
%>
<head>
<link rel="stylesheet" href="../css/elms.css" type="text/css">
<script src="<%=contextRoot%>/jsp/script/actb.js"></script>
<script language="javascript" type="text/javascript" src="<%=contextRoot%>/jsp/script/common.js"></script>
<script src="<%=contextRoot%>/jsp/js/formValidation.js"></script>
	
<script>
function validateAndSubmit(whichbutton) {
	
		var action = document.forms[0].action;
		action = action + '?action=pnext';
		document.forms[0].action = action;
		document.forms[0].submit();
	
}
</script>
	
<script>

</script>
</head>
<body  onpageshow="if (event.persisted) noBack();" onunload="" text="#000000" leftmargin="5" bottommargin="0"  scroll="yes" topmargin="0" marginwidth="0" marginheight="40" rightmargin="0" onLoad="dispMin();noBack();">
<html:base/>
<center>
<style type="text/css">
    .FireTitle          { FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  td.FireHeading        { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none; PADDING: 10px; BACKGROUND-COLOR:#D9D9D9 }
    .FireHeading        { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireGlance         { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  td.FireResource       { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none; PADDING: 6px }
    .FireResource       { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
  td.FireResourceStatic { FONT-SIZE: 12px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none; PADDING: 6px }
    .FireResourceStatic { FONT-SIZE: 12px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireSelect         { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireText           { FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireHighlight      { FONT-SIZE: 15px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #990000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireMessage        { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
  td.FireMenu           { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none; PADDING: 2px; BACKGROUND-COLOR:#000000 }
    .FireMenu           { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:link      { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:visited   { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:active    { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:hover     { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireMenu1          { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:link     { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:visited  { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:active   { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:hover    { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #FFFFFF; FONT-WEIGHT: bold; TEXT-DECORATION: none }
     td.FireHeading2     { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #FFFFFF; FONT-WEIGHT: bold; TEXT-DECORATION: none; PADDING: 10px; }
   
  @page Section1 {
                   size:8.5in 11.0in;
                   margin:0.5in 0.5in 0.5in 0.5in;
                 }
 p.MsoNormal, li.MsoNormal, div.MsoNormal {
                margin:0in;
                margin-bottom:.0001pt;
                font-size:12.0pt;
                font-family:"Arial"
              }
 table.atable {
                border-collapse:collapse;
                border:1px solid #B3B3B3
              }
 tr           {
                page-break-inside:avoid
              }
 div.Section1 {
                page:Section1;
              }


.panelVisited      { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #003265 }
.panelVisitedColor { background-color:#62A85F }
.panelActive       { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #FFFFFF }
.panelActiveColor  { background-color:#EAC563 }
.panel             { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #FFFFFF }
.panelColor        { background-color:#BB2F2F }

</style>


</center>
<table align="center" cellpadding="0" cellspacing="0" >
<td align="center" valign="top" width="99%">
	<br><br> <br><br>
<html:form action="/onlinePermitType">
<html:errors />
<fieldset style="width: 650px;" >
<br><br>
<table cellpadding="2" cellspacing="2" border="4" height="100%" width="100%" style="border-width: 1px; 
	border-spacing: ;
	border-style: outset;
	border-color: gray;
	border-collapse: separate;
	background-color: white;">
	
    						<tr>
								<td class="tabletext" colspan="2" bgcolor="#e5e5e5" align="center"><font class="FireHeading">Permit Type Selection   &nbsp;</td>
								
							</tr>
							 <tr>
        						<td align="left" colspan="2"> <font class="panelVisited" style="font-size:30px">
        						<font class="panelVisited" style="font-size:12px">Select Permit Type</font></font></td>
       						</tr>
       						 <tr >
								<td width="30%">
								<font class="panelVisited">Permit Type &nbsp;
								<font color="red">*</font>
								</font>
								</td>
								<td width="70%"> 
								<html:hidden property="activityType" value= "GARSL"></html:hidden>
								<html:text property="activityType" value="Garage sale" disabled="true">Garage sale</html:text>	</td>
							</tr>
       						<tr >
								<td>
								<font class="panelVisited">Select Type of Dwelling &nbsp;<font color="red">*</font>
								</font>
								</td>
								<td>
								<html:select property="typeOfDwelling" styleClass="textbox">
									<html:option value="">Please Select</html:option>
									<html:option value="Single Family Dwelling"><font class="con_hdr_1">Single Family Dwelling </font></html:option>
									<html:option value="Apartment Building"><font class="con_hdr_1">Apartment Building </font></html:option>
									<html:option value="Condominium"><font class="con_hdr_1">Condominium</font></html:option>
									<html:option value="Other"><font class="con_hdr_1">Other</font></html:option>
								</html:select>
								</td>
							</tr>	
							<tr >
							
								
							</tr>
							
							<tr>
        	 		<td class="FireSelect" colspan="2">
	                    <div align="center">
	                   		 <html:reset  value="Back" styleClass="FireSelect" style="width: 100px" onclick="history.back();"/>
	                      	 <html:button  property="button" value="Next" styleClass="FireSelect" style="width: 100px" onclick="validateAndSubmit('pnext');"/>
	                      	  
	                      </div>
                 	 </td>
               	 </tr>
							
    </table>
   </fieldset>
</html:form>
</td></table>
</body>
</html>