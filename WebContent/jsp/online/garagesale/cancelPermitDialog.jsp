<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" align="center">Are you sure you want to cancel this application?</h4>
      </div>
      <div class="modal-body">
        <p align="center">You will lose all information entered.</p>
        <br/>
       <center> <button class="next_btn btn btn-primary" onclick="cancelPermit('select')" >Yes</button>
        <a href="#" class="next_btn btn btn-primary"  data-dismiss="modal">No</a></center>
      </div>
      <div class="modal-footer">
      </div>
    </div>

  </div>
</div>
