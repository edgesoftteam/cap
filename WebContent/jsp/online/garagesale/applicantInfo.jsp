<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="elms.util.StringUtils"%>
<%@page import="elms.control.beans.online.OnlinePermitForm"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<html>
<head>
<html:base />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


</head>
<%
	String contextRoot = request.getContextPath();

	OnlinePermitForm addForm = (OnlinePermitForm) request.getAttribute("onlinePermitFrom");
	String isObc = ((elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY)).getIsObc();
	String isDot = ((elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY)).getIsDot();
	/* String  peopleTypes =(String)request.getAttribute("peopleTypes")!=null?(String)request.getAttribute("peopleTypes"):""; */
%>
<link rel="stylesheet" href="../css/elms.css" type="text/css">
	<script src="<%=contextRoot%>/tools/jquery/jquery.min.js"></script>
	<script src="<%=contextRoot%>/jsp/script/bootstrap.min.js"></script>
	<script src="<%=contextRoot%>/jsp/script/jquery.dropotron.min.js"></script>
	<script src="<%=contextRoot%>/jsp/script/skel.min.js"></script>
	<script src="<%=contextRoot%>/jsp/script/skel-layers.min.js"></script>
	<script src="<%=contextRoot%>/jsp/script/init.js"></script>	
	<script src="<%=contextRoot%>/jsp/script/formValidations.js"></script>
	<script language="JavaScript" src="<%=contextRoot%>/jsp/script/CalendarControl.js"></script>
	<noscript>
		<link rel="stylesheet" href="<%=contextRoot %>/css/style.css" />
		<link rel="stylesheet" href="<%=contextRoot %>/css/style-desktop.css" />
	</noscript>
	<link rel="stylesheet" href="<%=contextRoot%>/css/progress-wizard.css">
	<link rel="stylesheet" href="<%=contextRoot%>/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=contextRoot%>/css/bootstrap-switch.min.css">
	<link rel="stylesheet" href="<%=contextRoot%>/css/CalendarControl.css" type="text/css">
	
	<script src="<%=contextRoot%>/jsp/script/sweetalert.min.js"></script> 
<script src="<%=contextRoot%>/jsp/script/sweetalert.js"></script>
<script src="<%=contextRoot%>/jsp/script/sweetalert.min2.js"></script>
<link rel="stylesheet" href="<%=contextRoot%>/jsp/css/sweetalert.min2.css">
<link rel="stylesheet" href="<%=contextRoot%>/jsp/css/sweetalert.min.css" type="text/css">

<script language="JavaScript">
function validateEmail()
{
var email= document.forms[0].elements['emailAddress'].value;

//  a very simple email validation checking.
//  you can add more complex email checking if it helps
    var splitted = email.match("^(.+)@(.+)$");    if(splitted == null) return false;
    if(splitted[1] != null )
    {
      var regexp_user=/^\"?[\w-_\.]*\"?$/;
      if(splitted[1].match(regexp_user) == null) return false;
    }
    if(splitted[2] != null)
    {
      var regexp_domain=/^[\w-\.]*\.[A-Za-z]{2,4}$/;
      if(splitted[2].match(regexp_domain) == null)
      {
	    var regexp_ip =/^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
	    if(splitted[2].match(regexp_ip) == null) return false;
      }// if

      return true;
    }

return false;
}
function check()
{

var carCode= event.keyCode;
if ((carCode < 48) || (carCode > 57)){


event.returnValue = false;
}

}
function DisplayHyphen(str)
{

	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46))
	{
		event.returnValue = false;
	}
	else
	{
		if ((document.forms[0].elements[str].value.length == 3 ) || (document.forms[0].elements[str].value.length == 7 ))
		{
			document.forms[0].elements[str].value = document.forms[0].elements[str].value +'-';
 		}
 		if (document.forms[0].elements[str].value.length > 11 )  event.returnValue = false;
	}
}

function validateAndSubmit(whichbutton) {
	var strValue=true;
	 if (strValue==true){
	      if (document.forms[0].elements['firstName'].value == '') {
	    	swal("","First Name  is a required field");
	    	 //	alert(validateEmail(document.forms['onlinePermitForm'].elements['emailAddress'].value));
	    	document.forms[0].elements['firstName'].focus();
	    	strValue=false;
	    	return false;
	      }
	   }
  
  	if (strValue==true){
	   
	      if (document.forms[0].elements['lastName'].value == '') {
	    	  swal("","Last Name is a required field");
	    	 //	alert(validateEmail(document.forms['onlinePermitForm'].elements['emailAddress'].value));
	    	document.forms[0].elements['lastName'].focus();
	    	strValue=false;
	    	return false;
	      }
	   }
  	/* if (strValue==true){
		  if (document.forms['onlinePermitForm'].elements['address'].value == '') {
			alert('Address is a required field');
			document.forms['onlinePermitForm'].elements['address'].focus();
			strValue=false;
			return false;
			}
		} */
	
	if (strValue==true){
 		  if (document.forms[0].elements['phoneNbr'].value == '') {
 			 swal("","Phone Number is a required field");
			document.forms[0].elements['phoneNbr'].focus();
			strValue=false;
			return false;
 			}
		}
// 	if (strValue==true){
		   
// 	      if (document.forms[0].elements['emailAddress'].value == '') {
// 	    	  swal('Email Address  is a required field');
// 	    	 //	alert(validateEmail(document.forms['onlinePermitForm'].elements['emailAddress'].value));
// 	    	document.forms[0].elements['emailAddress'].focus();
// 	    	strValue=false;
// 	    	return false;
// 	      }
// 	   }

// 	if (strValue==true){
// 		if(!validateEmail(document.forms[0].elements['emailAddress'].value)) {
// 	          strValue=false;
// 	          swal('Please enter valid Email Address');
// 	        document.forms[0].elements['emailAddress'].focus();
// 	        return false;
// 	    }
// 	}
	
	
	var action = document.forms[0].action;
	if(whichbutton == "next"){
	action = action + '?action=apnext';
	document.forms[0].action = action;
	document.forms[0].submit();
	}else if(whichbutton == "update"){
		action = action + '?action=update';
		document.forms[0].action = action;
		document.forms[0].submit();
	}
	

}
function backbutton(whichbutton) {	
	var action = document.forms[0].action;
	action = action + '?action=prev';
	document.forms[0].action = action;
	document.forms[0].submit();
	}
function cancelPermit(whichbutton){
	var action = document.forms[0].action;
	action = action + '?action=reset';
	document.forms[0].action = action;
	document.forms[0].submit();
}
<%-- function backbutton(whichbutton) {	
	var action = document.forms[0].action;
	action = action + '?action=ptb&streetName=<%=addForm.getStreetName()%>&streetNum=<%=addForm.getStreetNum()%>&unit=<%=addForm.getUnit()%>';
	action = action + '?action=ptb&streetName=<%=addForm.getStreetName()%>&streetNum=<%=addForm.getStreetNum()%>&unit=<%=addForm.getStreetUnit()%>';
	document.forms[0].action = action;
	document.forms[0].submit();
	} --%>
</script>
<body>
<body class="homepage">
	<div class="wrapper style2">
		<!-- <div class="title">CITIZEN SERVICES ONLINE</div> -->
		<div id="main" class="container">
			<section id="features">
				<header class="style1">
				<h2><b>APPLICANT INFORMATION</b></h2>
					 <%
					 	OnlinePermitForm form = (OnlinePermitForm) request.getAttribute("onlinePermitFrom");
					 	String error = (String) request.getAttribute("error");
						String success = (String) request.getAttribute("success");
						if (error != null || success != null) {
					%>
					<div id="profile_status">
						<script type="text/javascript">
						var error = '<%=error%>';
							var success = '<%=success%>';
							var message = success;
							var color = 'Green';
							if(error != 'null') {
								message = error;
								color = 'red';
							}
							showMessage(message, color);
						</script>
					</div>
					<%
						}
					%>
					
				</header>
				<div class="feature-list">
					<div class="row">
											<div class="col-lg-12">
						<html:form action="applicantInfo.do">
							<div class="panel panel-default">
                          		<div class="panel-heading" style="background-color: #A9A9A9"><!--  #e5e5e5 -->
                          		<button type="button" class="next_btn btn btn-primary" data-toggle="modal" data-target="#myModal">Cancel Permit Application</button>
											<!-- Modal -->
											<jsp:include page="cancelPermitDialog.jsp" />
                          		<!-- <a class="next_btn btn btn-primary" style="height: 40px;" onclick="cancelPermit('select')">Cancel Permit Application</a> -->
			                    	<b></b>
			                    </div>
			                    <!--  <div class="panel-body">
								<div class="col-lg-12 col-md-6 col-sm-6 col-xs-12">
									  <div class="form-group">
									    2. Verify Applicant Mailing Information
									  </div>
								 </div>
								 </div> -->
								<div class="table-responsive" >
									<table class="table table-striped table-bordered table-hover" style="white-space: nowrap;">
									
									<tr><th colspan="2"> <b><font color="red">Applicant must be the resident of the garage sale permit address</font></b></th></tr>
									<tr><th colspan="2"> <b>2. Verify Applicant Mailing Information</b></th></tr>
										<tr>
									    	<td><b>Email Address</b></td>
									    	<td>									    	
									    		<html:hidden  property="emailAddress"/>
									    		<%=addForm.getEmailAddress()%>
									    	</td>
									    </tr>
									    <tr>
										<td><b>Applicant Name<font color="red">*</font></b></td>
										<td>
										<html:text  property="firstName" maxlength="60" />
										<html:text	 property="lastName" maxlength="60" />
										
										</td>
										</tr>
									    <tr>
									    	<td><b>Address</b></td>
									    	<td>
												<html:text  property="address"  maxlength="60" />
												<html:text  property="city" />
												<html:text  property="state" style="width:5%;" maxlength="2" />
												<html:text  property="zip" style="width:8%;" maxlength="9" onkeypress="check();"/> 									    	 
									    	 </td>
									    </tr>
									    <tr>
											<td><b>Phone Number <font color="red">*</font></b></td>
											<td>
											<html:text  property="phoneNbr" style="width:13%;" maxlength="12" onkeypress="return DisplayHyphen('phoneNbr');"/>&nbsp;
											<html:text  property="phoneExt" style="width:5%;" maxlength="4"/>
											</td>
											</tr>
											<tr>
										<td><b>Work Phone </b></td>
										<td>
										<html:text 	property="workPhone" style="width:13%;" maxlength="12" onkeypress="return DisplayHyphen('workPhone');"/>&nbsp; 
										<html:text	 property="workExt" style="width:5%;" maxlength="4"/>
										</td>
										    
										
									    </tr>
									     
									   
									</table>
								</div>							
							</div>
							
							<a class="next_btn btn btn-primary" style="margin-bottom:10px;margin-left:10px;valign:left;height: 35px;width: 60px" id="bckbtn" onclick="backbutton('select')">Back</a>
							<a class="next_btn btn btn-primary" style="margin-bottom:10px;margin-left:588px;valign:right;height: 35px;width: 60px" id="nxtbtn" onclick="validateAndSubmit('next')">Next</a>
							
						</html:form>
					</div>
				</div>
			</section>
		</div>
	</div>
</body>
</html>