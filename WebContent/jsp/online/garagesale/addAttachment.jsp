<%@page import="elms.app.common.DisplayItem"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	String contextRoot = request.getContextPath();
	String description= (String)request.getAttribute("description");
	String error = (String) request.getAttribute("error");
	//System.out.println("description:"+description);
%>
<html>
<head>
<title>City of Burbank</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link rel="stylesheet" href="<%=contextRoot%>/jsp/css/elms.css" type="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="<%=contextRoot%>/jsp/script/jquery-1.7.2.min.js"></script>
	<script src="<%=contextRoot%>/jsp/script/bootstrap.min.js"></script>
	<script src="<%=contextRoot%>/jsp/script/jquery.dropotron.min.js"></script>
	<script src="<%=contextRoot%>/jsp/script/skel.min.js"></script>
	<script src="<%=contextRoot%>/jsp/script/skel-layers.min.js"></script>
	<script src="<%=contextRoot%>/jsp/script/init.js"></script>
	<link rel="stylesheet" href="<%=contextRoot%>/css/progress-wizard.css">
	<link rel="stylesheet" href="<%=contextRoot%>/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=contextRoot%>/css/bootstrap-switch.min.css">
	<script language="JavaScript" src="<%=contextRoot%>/jsp/script/CalendarControl.js"></script>
	<link rel="stylesheet" href="<%=contextRoot%>/jsp/css/CalendarControl.css" type="text/css">
	<script src="<%=contextRoot%>/jsp/script/formValidations.js"></script>
	
<script src="<%=contextRoot%>/jsp/script/sweetalert.min.js"></script> 
<script src="<%=contextRoot%>/jsp/script/sweetalert.js"></script>
<script src="<%=contextRoot%>/jsp/script/sweetalert.min2.js"></script>
<link rel="stylesheet" href="<%=contextRoot%>/jsp/css/sweetalert.min2.css">
<link rel="stylesheet" href="<%=contextRoot%>/jsp/css/sweetalert.min.css" type="text/css">

	<script type="text/javascript">
	function addAttachment(whichbutton){		
		if(document.forms['onlinePermitFrom'].elements['description'].value==""){
			swal('Please enter description.');
			return false;
		}
		if(document.forms['onlinePermitFrom'].elements['theFile'].value==""){
			swal('Please add Attachment.');
			return false;
		}
		var action = document.forms['onlinePermitFrom'].action;
		action = action + '?action=attachment';
		document.forms['onlinePermitFrom'].action = action;
		document.forms['onlinePermitFrom'].submit();
		$('#loading').show(); 
	}
	function cancelPermit(whichbutton){
		var action = document.forms[0].action;
		action = action + '?action=reset';
		document.forms[0].action = action;
		document.forms[0].submit();
	}	
	function backbutton(whichbutton) {
		var action = document.forms[0].action;
		action = action + '?action=oai';
		document.forms[0].action = action;
		document.forms[0].submit();
	}
	function nextPage(whichbutton) {
		
		var action = document.forms[0].action;
		action = action + '?action=nxt';
		document.forms[0].action = action;
		document.forms[0].submit();
	}
	
	function autoResize(){
			var el = document.getElementById("wrapperHeight");
		    var height = el.offsetHeight;
		    var newHeight = height + 100;
		    el.style.height = newHeight + 'px';
		    document.forms['onlinePermitFrom'].elements['description'].value="";
		    $('#loading').hide(); 
		    document.getElementById("loading").hide(); 
	    }
	function deleteAttachment(whichbutton){		
		var action = document.forms[0].action;
		action = action + '?attachId='+whichbutton;		
		document.forms[0].action = action;
		document.forms[0].submit();
	}
	$(window).load(function() {
		  $("html, body").animate({ scrollTop: $(document).height() }, 2000);
		}); 
	</script>
</head>

<style>
.vl {
  border-top: 1px white;
  height: 1px;
}
#loading {
   width: 100%;
   height: 100%;
   top: 0;
   left: 0;
   position: fixed;
   display: block;
   opacity: 15;
   /*  background-color: #DCDCDC; */ 
   z-index: 99;
   text-align: center;
}

#loading-image {
  position: absolute;
  top: 300px;
  left: 760px;
  z-index: 100;
}
.btn-primary{
 vertical-align: middle;
}
</style>


<!-- <script language="javascript" type="text/javascript">
     $(window).load(function() {
     $('#loading').hide();
  });
     
     
</script> -->


<body class="homepage" onload="autoResize();">
	
<div class="wrapper style2" id="wrapperHeight">
	<div class="title">CITIZEN SERVICES ONLINE</div>
		<div id="main" class="container">
			<header class="style1">
			
				<h2><b>Add Attachments</b></h2>
			</header>
			<% if(error != null){%>
				<div >
				  <div class="form-group" >
				   <label for="exampleInputName2" style="margin-left: 325px">
				 <font color="red" > <%= error%></font>     
				   </label>
					   </div>
						</div>
				<%} %>	
				<div class="feature-list">
					<div class="row">
						
					<div class="col-lg-9">
		<html:form action="/addAttachment.do" method="POST" enctype="multipart/form-data" >
		<html:errors/>
			<div class="panel panel-default">
				<div class="panel-heading" style="background-color: #A9A9A9">
				
				<div class=class="container">
				  <div class="row">
				       <div class="col-sm">
									<button type="button" class="next_btn btn btn-primary" data-toggle="modal" data-target="#myModal">Cancel Permit Application</button>
					  
					 						 <!-- Modal -->
											<jsp:include page="cancelPermitDialog.jsp" />
					   </div>
						<div class="col-sm" style="font-size: 14px">
							<strong>Use this upload feature to submit all supporting documents for the application in PDF format.
							  <br/><br/>
							  Multiple files may be uploaded one at a time. All attachments should be in PDF form with individual files being less than 20mb in size.
							   <br/><br/>
							  Attachment names and files should be numbers and letters only.  No symbols (i.e. #,@,*, etc.).
							</strong>
					  </div>
					  
				  </div>
		       </div>
		  </div>
								<div id="loading">
									<img id="loading-image"
										src="<%=contextRoot%>/jsp/images/loader.gif" alt="Loading..." />
								</div>
								<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover">
										<tr >
										<td style="width:40%;font-size: 14px;"><b>Permit Type<font color="red">*</font></b></td>
								
								<td> 
								<html:hidden property="activityType" value= "GARSL"></html:hidden>
								<html:text property="activityType" value="Garage sale" disabled="true" styleClass="form-control">Garage sale</html:text>	</td>
							</tr>
						                    <tr>
							                      <td style="width:40%;font-size: 14px;"><b>Description<font color="red">*</font></b></td>
							                    
												  <td>
							                        <html:textarea property="description" cols="20" rows="5" styleClass="form-control"/>
							                      </td>
						                      </tr>
						                      <tr>
							                      <td style="width:40%;font-size: 14px"><b>File Name<font color="red">*</font></b></td>
							                      <td>
							                       <table>
							                       <tr>
								                       <td>
															<html:file property="theFile" value="choose" styleClass="form-control" />
														</td>
														<td>&nbsp;</td>
														 <td>
													    	<a href="#" class="next_btn btn btn-primary"  style="height:35px;margin-bottom:10px"  onclick="return addAttachment('select')">Upload</a>
													    </td>
													    
												    </tr>
							                     </table>
							                      </td>
						                      </tr>
						                     
						                      
						                    <tr>
						                    	<td colspan="2">
						                    	    <a class="next_btn btn btn-primary" style="margin-bottom:10px;margin-left:10px;valign:left;height: 35px;width: 60px" onclick="backbutton('oai')">Back</a>
						                    	    <a class="next_btn btn btn-primary" style="margin-bottom:10px;margin-left:690px;valign:right;height: 35px;width: 60px" onclick="nextPage('select')" id="next">Next</a>
						                    	</td>
						                    </tr>
		                  			</table>
		                  			</div>
		                  			</div>
		                  			<div class="panel panel-default">
		                  			<div class="panel-heading" style="background-color: #A9A9A9;font-size:14px">
		                  			<b>Attachments</b>
		                  			</div>
									 <div class="table-responsive">
										<table class="table table-striped table-bordered">
												   <!--  <thead style="font-size:14px;background-color: #A9A9A9;font-size:14px;"> -->
													      <tr style="color:#000000;font-size:14px">
													        <th style="width:40%;"><b>File Name</b></th>
													        <th style="width:50%;"><b>Description</b></th>
													        <th style="width:10%;"><b>Delete</b></th>
													      </tr>
										    		<!-- </thead> -->
												     <tbody>
												     <%
												     List<DisplayItem> getAttachments = (List<DisplayItem>) session.getAttribute("getAttachmentsList");
											    			System.out.println(getAttachments);
											    		if (getAttachments != null && !getAttachments.isEmpty()) {
											    				
											    		for(int i = 0; i < getAttachments.size(); i++) {
													    	DisplayItem di = getAttachments.get(i);
												     %>
														     <tr>
														<td width="50%"><a href="<%=contextRoot%>/jsp/online/fopen.jsp?fileName=<%= di.getFieldTwo()%>" target="_new"><%= di.getFieldTwo()%></a></td>
											      	<td width="40%"><%= di.getFieldThree() != null ? di.getFieldThree() : "" %></td>
											      	<td width="10%"><a href="#" onclick="deleteAttachment(<%= di.getFieldFive() != null?di.getFieldFive() : ""%>)" style="text-align:center;" ><span class="glyphicon glyphicon-remove"></span></a></td>											      	
														     </tr>
											    	<% }} else{
											    		
											    	} %>
												     </tbody>
												     </table>
											  </div>
										</div>
							</html:form>
						</div>
					</div>
				</div>
			</div>
		</div>
<%-- <%
	String error = (String) request.getAttribute("error");
	if (error != null) {
%>
<script type="text/javascript">
	alert('<%=error%>');
</script>
			
<%}%> --%>
</body>
</html>
