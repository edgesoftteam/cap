<%@page import="elms.util.Operator"%>
<%@page import="elms.util.StringUtils"%>
<%@page import="elms.app.common.DisplayItem"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<html>
<%String contextRoot = request.getContextPath();	
%>
<% String message = (String)request.getAttribute("message");
/* if (message==null) message="";
if (message.equalsIgnoreCase("0")) { message = "No records found matching search criteria. Try again by changing criteria...";}
else message=""; */
%>
<head>
<link rel="stylesheet" href="../css/elms.css" type="text/css">

<script src="<%=contextRoot%>/jsp/js/formValidation.js"></script>
<script src="<%=contextRoot%>/tools/jquery/jquery.min.js"></script>

	<script src="<%=contextRoot%>/jsp/script/bootstrap.min.js"></script>
	<script src="<%=contextRoot%>/jsp/script/jquery.dropotron.min.js"></script>
	<script src="<%=contextRoot%>/jsp/script/skel.min.js"></script>
	<script src="<%=contextRoot%>/jsp/script/skel-layers.min.js"></script>
	<script src="<%=contextRoot%>/jsp/script/init.js"></script>	

<noscript>
		<link rel="stylesheet" href="<%=contextRoot %>/css/style.css" />
		<link rel="stylesheet" href="<%=contextRoot %>/css/style-desktop.css" />
	</noscript>
	<link rel="stylesheet" href="<%=contextRoot%>/css/progress-wizard.css">
	<link rel="stylesheet" href="<%=contextRoot%>/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=contextRoot%>/css/bootstrap-switch.min.css">
	<link rel="stylesheet" href="<%=contextRoot%>/css/CalendarControl.css" type="text/css">

</head>
<body class="homepage">  

<div class="wrapper style2">
			<div class="title">CITIZEN SERVICES ONLINE</div>
			<div id="main" class="container">
				<section id="features">
					
						<div class="feature-list">
						<div class="row">

							<div class="col-lg-9 col-xs-9">
							
								<div class="panel panel-default">
									<div class="panel-heading" style="background-color: #A9A9A9"><font color="white"><b> Garage Sale Cancelled</b> </font></div>				                    
					                     <div class="panel-body">
												<div class="col-lg-12 col-md-6 col-sm-6 col-xs-12">
													  <div class="form-group">
													    <label for="exampleInputName2"><%=message %></label><br/>
													    <a href="https://www.burbankca.gov/documents/173607/1012768/GarageSaleNonUseDeclaratio.pdf/d60a3132-4424-aa69-a12c-4261147bc64e?t=1624394845609" target="_new">Cancel Application Download</a>
													  </div>
													  <div>
													  <a href="<%=contextRoot%>/myPermits.do?action=view&viewPermit=Yes&isObc=Y&isDot=N&inActive=No" class="next_btn btn btn-primary"  style="height:35px;margin-bottom:10px">Inactive Permits</a>
													  </div>
												 </div>
									</div></div></div></div></div></section></div></div>
									
</body>
</html>