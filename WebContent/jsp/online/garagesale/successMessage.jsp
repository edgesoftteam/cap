<%@page import="elms.agent.ActivityAgent"%>
<%@page import="elms.common.Constants"%>
<%@page import="elms.agent.LookupAgent"%>
<%@page import="elms.util.Operator"%>
<%@page import="elms.util.StringUtils"%>
<%@page import="elms.app.common.DisplayItem"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<html>
<%String contextRoot = request.getContextPath();	
String levelId=(String)request.getAttribute("levelId");
String actNo = ActivityAgent.getActivtiyNumber(StringUtils.s2i(levelId));
System.out.println("levelId... "+request.getAttribute("levelId"));
// System.out.println("actNo... "+request.getAttribute("actNo"));
%>
<head>
<link rel="stylesheet" href="../css/elms.css" type="text/css">

<script src="<%=contextRoot%>/jsp/js/formValidation.js"></script>
<script src="<%=contextRoot%>/tools/jquery/jquery.min.js"></script>

	<script src="<%=contextRoot%>/jsp/script/bootstrap.min.js"></script>
	<script src="<%=contextRoot%>/jsp/script/jquery.dropotron.min.js"></script>
	<script src="<%=contextRoot%>/jsp/script/skel.min.js"></script>
	<script src="<%=contextRoot%>/jsp/script/skel-layers.min.js"></script>
	<script src="<%=contextRoot%>/jsp/script/init.js"></script>	

<noscript>
		<link rel="stylesheet" href="<%=contextRoot %>/css/style.css" />
		<link rel="stylesheet" href="<%=contextRoot %>/css/style-desktop.css" />
	</noscript>
	<link rel="stylesheet" href="<%=contextRoot%>/css/progress-wizard.css">
	<link rel="stylesheet" href="<%=contextRoot%>/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=contextRoot%>/css/bootstrap-switch.min.css">
	<link rel="stylesheet" href="<%=contextRoot%>/css/CalendarControl.css" type="text/css">

</head>
<body class="homepage">  

<div class="wrapper style2">
			<div class="title">CITIZEN SERVICES ONLINE</div>
			<div id="main" class="container">
				<section id="features">
					
						<div class="feature-list">
						<div class="row">

							<div class="col-lg-9 col-xs-9">
							
								<div class="panel panel-default">
									<div class="panel-heading" style="background-color: #A9A9A9"><font color="white"><b>Garage Sale Submitted </b></font></div>				                    
					                     <div class="panel-body">
												<div class="col-lg-12 col-md-6 col-sm-6 col-xs-12">
													  <div class="form-group">
													    <label for="exampleInputName2">Application submitted successfully</label>
													  </div>
													  <div>
													  <a href="<%=contextRoot%>/myPermits.do?viewPermit=Yes&isObc=Y&isDot=N&inActive=Yes" class="next_btn btn btn-primary"  style="height:35px;margin-bottom:10px">My Permits</a>
													  	   <%if(LookupAgent.getKeyValue(Constants.PROD_SERVER_FLAG).equalsIgnoreCase("N")) {%>
                                                      			<a target="_blank" href="<%=LookupAgent.getKeyValue(Constants.GARAGESALE_PRINT_NONPROD_URL)%><%=actNo %>" class="next_btn btn btn-primary"  style="height:35px;margin-bottom:10px">Print</a>
                                                           <%}else if(LookupAgent.getKeyValue(Constants.PROD_SERVER_FLAG).equalsIgnoreCase("Y")) { %>
                                                     			<a target="_blank" href="<%=LookupAgent.getKeyValue(Constants.GARAGESALE_PRINT_PROD_URL)%><%=actNo %>" class="next_btn btn btn-primary"  style="height:35px;margin-bottom:10px">Print</a>
                                                           <%} %>
                                                           
                                                           </div>
												 </div>
									</div></div></div></div></div></section></div></div>
									
</body>
</html>