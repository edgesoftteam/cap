<%@page import="elms.agent.LookupAgent"%>
<%@page import="elms.common.Constants"%>
<%@page import="elms.control.beans.online.HolidayEditorForm"%>
<%@page import="elms.agent.AddressAgent"%>
<%@page import="elms.control.beans.online.OnlinePermitForm"%>
<%@page import="elms.util.Operator"%>
<%@page import="elms.util.StringUtils"%>
<%@page import="elms.app.common.DisplayItem"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<html>
<%String contextRoot = request.getContextPath();	
%>
<head>
<link rel="stylesheet" href="../css/elms.css" type="text/css">
<script src="<%=contextRoot%>/jsp/script/actb.js"></script>
<script language="javascript" type="text/javascript" src="<%=contextRoot%>/jsp/script/common.js"></script>
<script src="<%=contextRoot%>/jsp/script/formValidations.js"></script>
<script src="<%=contextRoot%>/tools/jquery/jquery.min.js"></script>

	<script src="<%=contextRoot%>/jsp/script/bootstrap.min.js"></script>
	<script src="<%=contextRoot%>/jsp/script/jquery.dropotron.min.js"></script>
	<script src="<%=contextRoot%>/jsp/script/skel.min.js"></script>
	<script src="<%=contextRoot%>/jsp/script/skel-layers.min.js"></script>
	<script src="<%=contextRoot%>/jsp/script/init.js"></script>	

<link rel="stylesheet" href="<%=contextRoot%>/jsp/css/kendo.default-v2.min.css"/>
<noscript>
		<link rel="stylesheet" href="<%=contextRoot %>/css/style.css" />
		<link rel="stylesheet" href="<%=contextRoot %>/css/style-desktop.css" />
	</noscript>
	<link rel="stylesheet" href="<%=contextRoot%>/css/progress-wizard.css">
	<link rel="stylesheet" href="<%=contextRoot%>/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=contextRoot%>/css/bootstrap-switch.min.css">
	<link rel="stylesheet" href="<%=contextRoot%>/css/CalendarControl.css" type="text/css">
	
	<script src="<%=contextRoot%>/jsp/script/sweetalert.min.js"></script> 
<script src="<%=contextRoot%>/jsp/script/sweetalert.js"></script>
<script src="<%=contextRoot%>/jsp/script/sweetalert.min2.js"></script>
<link rel="stylesheet" href="<%=contextRoot%>/jsp/css/sweetalert.min2.css">
<link rel="stylesheet" href="<%=contextRoot%>/jsp/css/sweetalert.min.css" type="text/css">


<style type="text/css">
.disabledDay { 
    display: block;
    overflow: hidden;
    min-height: 22px;
    line-height: 22px;
    padding: 0 .45em 0 .1em;
    cursor:default;
    opacity: 0.5;
}
</style>
<script language="javascript" type="text/javascript" src="<%=contextRoot%>/jsp/script/kendo.all.min.js"></script>	
<script>
function cancelPermit(whichbutton){
	var action = document.forms[0].action;
	action = action + '?action=reset';
	document.forms[0].action = action;
	document.forms[0].submit();
}	
function backbutton(whichbutton) {
	var action = document.forms[0].action;
	action = action + '?action=bck';
	document.forms[0].action = action;
	document.forms[0].submit();
}
function validateAndSubmit() {
// 	if(document.forms[0].elements['actDescription'].value == ''){
// 		swal("Please Enter Description");
// 		return false;
// 	}
	if(document.forms[0].elements['issueDate'].value == ''){
		swal("","Please Enter Sale Date");
		return false;
	}
	if(document.forms[0].elements['endDate'].value == ''){
		swal("","Please Enter Expiration Date");
		return false;
	} 
		var action = document.forms[0].action;
		action = action + '?action=adnext';
		document.forms[0].action = action;
		document.forms[0].submit();
	
}

</script>
	
<script>

</script>
</head>
<body class="homepage">  
<html:form action="/onlineAdditionalInfo.do">
<div class="wrapper style2">
			<div class="title">CITIZEN SERVICES ONLINE</div>
			<div id="main" class="container">
				<section id="features">
					<header class="style1">
						<h2><b>Additional Information </b></h2>
						</header>
						<div class="feature-list">
						<div class="row">

							<div class="col-lg-9">
							
								<div class="panel panel-default">
									<div class="panel-heading" style="background-color: #A9A9A9">
									<button type="button" class="next_btn btn btn-primary"
										data-toggle="modal" data-target="#myModal">Cancel
										Permit Application</button>
						            <!-- Modal -->
									<jsp:include page="cancelPermitDialog.jsp" />

									<!-- <a class="next_btn btn btn-primary" style="height: 40px;" onclick="cancelPermit('select')">Cancel Permit Application</a> -->
                                    </div>				                    
					                     <div class="panel-body">
												<div class="col-lg-12 col-md-6 col-sm-6 col-xs-12">
													  <div class="form-group">
													    <label for="exampleInputName2">3. Please enter the information requested below.</label>
													  </div>
												 </div>
											
										<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
									 	 <div class="form-group" style="text-align:right">
<!-- 											<label for="exampleInputName2">Permit Description<font color="red">*</font><br> -->
<!-- 											</label> -->
											
										</div>
									</div>
									<div class="col-lg-5 col-md-6 col-sm-6 col-xs-12">
										<div class="form-group">
											<html:hidden property="actDescription" />
										</div>
									</div> 
									
							<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
									 	 <div class="form-group" style="text-align:right">
											<label for="exampleInputName2">Sale Date<font color="red">*</font><br>
											</label>
											
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										<div class="form-group">
											<html:text property="issueDate" size="10" styleId="issueDatepicker" style="color:black;font-size:12px;" styleClass="textboxd" onkeypress="return validateDate();" onblur ="DateValidate(this);" onchange="updateEndDate(this.value)"/>
										</div>
									</div> 
									<div style="width:30px">
											              <div class="form-group">
                                                           &nbsp;
                                                          </div>
                                                       </div>
							
								     <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
									 	 <div class="form-group" style="text-align:right">
											<label for="exampleInputName2">End Date<font color="red">*</font><br></label>
											<label for="exampleInputName2"><font color="red">(Sale may not be more than two consecutive days)</font><br></label>											
										</div>
									</div>
									<div class="col-lg-5 col-md-6 col-sm-6 col-xs-12">
										<div class="form-group">
											<html:text property="endDate" size="10" styleId="enddatepicker" style="color:black;font-size:12px;" styleClass="textboxd" onkeypress="return validateDate();" onblur="DateValidate(this);"/>
       						
										</div>
									</div> 
									<div style="width:30px">
											              <div class="form-group">
                                                           &nbsp;
                                                          </div>
                                                       </div>
							 			<!--  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
								  					 <div class="form-group">		
													  
												         <a class="next_btn btn btn-primary" style="margin-left:200px;height: 40px;valign:left;width: 70px" onclick="backbutton('back')">Back</a>
												         </div>
												         </div>
												<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6" >
								  					 <div class="form-group">      
												         <a class="next_btn btn btn-primary" style="margin-bottom:10px;margin-left:250px;valign:right;height: 40px;width: 70px" onclick="validateAndSubmit('next')">Next</a>
												      
												</div>
						                   </div> -->
						                   <a class="next_btn btn btn-primary" style="margin-bottom:10px;margin-left:10px;valign:left;height: 35px;width: 60px" id="bckbtn" onclick="backbutton('back')">Back</a>
										   <a class="next_btn btn btn-primary" style="margin-bottom:10px;margin-left:588px;valign:right;height: 35px;width: 60px" id="nxtbtn" onclick="validateAndSubmit('next')">Next</a>
						                   </div>
						                   
						                   <br>
<!-- 						                    <div class="panel-body"> -->
<!-- 						                      <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6"><font color="red">Note : Please do not use browser back button.</font></div> -->
<!-- 						                   </div>   -->
							</div>
							
							      
						          </div>								  
		
						</section>
						</div>
						</div>
						
</html:form>
<script type="text/javascript">
<% 

/* List<String> holidayList = new ArrayList();
holidayList.add("2020-10-04 00:00:00");
holidayList.add("2020-10-05 00:00:00");
holidayList.add("2020-10-06 00:00:00");
holidayList.add("2020-10-07 00:00:00");
holidayList.add("2020-10-08 00:00:00");
holidayList.add("2020-10-09 00:00:00");
holidayList.add("2020-10-10 00:00:00"); */
%>


var dateObj = new Date();
dateObj.setDate(dateObj.getDate()+1);
var month = dateObj.getUTCMonth() + 1; //months from 1-12
var day = dateObj.getUTCDate();
var year = dateObj.getUTCFullYear();

var holiday_dates = [];
<%
List <HolidayEditorForm> holidayList = (List<HolidayEditorForm>) request.getAttribute("holidaysList");
if(holidayList !=null){
	for(int i=0;i<  holidayList.size();i++){
	%>
	
     d = new Date(<%=holidayList.get(i).getDate().substring(0, 4)%>,<%=holidayList.get(i).getDate().substring(5, 7)%>-1,<%=holidayList.get(i).getDate().substring(8, 10)%>); 
     holiday_dates.push(d);
     

<%}}%>
var p=	 $("#issueDatepicker").kendoDatePicker({
		 min: new Date(year, month-1, day),
		 disableDates: holiday_dates,
	      month: {
	    		empty: '<span class="k-state-disabled">#= data.value #</span>'
	    	  }
	    

	});

function updateEndDate(stratDate){
	
	var daysToAdd = <%=LookupAgent.getKeyValue(Constants.GS_PERMIT_PERIOD_DAYS)%>;
	
	var trgDate = new Date(stratDate);
	if(trgDate == "Invalid Date"){
		document.forms[0].elements['endDate'].value = "";
		document.forms[0].elements['issueDate'].value = "";
		return false;
	}
	trgDate.setDate(trgDate.getDate() + daysToAdd);
	var dd = trgDate.getDate();
	var mm = trgDate.getMonth() + 1;
	var y = trgDate.getFullYear();
	var formattedDate = mm + '/'+ dd + '/'+ y;
	document.forms[0].elements['endDate'].value = formattedDate; 
	
	var edval = $('#issueDatepicker').data("kendoDatePicker");
    var sdval = $('#enddatepicker').data("kendoDatePicker");
    if (sdval && edval) {                        
    	sdval.min(edval.value());
    	sdval.max(new Date(y,mm-1,dd));
    }
}

$("#enddatepicker").kendoDatePicker({
	
});</script>
</body>
</html>