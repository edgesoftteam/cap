
<%@page import="org.owasp.encoder.Encode"%>
<%@page import="elms.util.StringUtils"%>
<%@page import="elms.control.beans.online.OnlinePermitForm"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	String contextRoot = request.getContextPath();
    OnlinePermitForm addForm= (OnlinePermitForm)request.getAttribute("addForm");
  // customFieldList= addForm.getCustomFieldList();
   //(List) request.getAttribute("customFieldsList");
  
%>
<html>
<head>
	<title>City of Burbank</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="../css/elms.css" type="text/css">
	<script src="<%=contextRoot%>/tools/jquery/jquery.min.js"></script>
	<script src="<%=contextRoot%>/jsp/script/bootstrap.min.js"></script>
	<script src="<%=contextRoot%>/jsp/script/jquery.dropotron.min.js"></script>
	<script src="<%=contextRoot%>/jsp/script/skel.min.js"></script>
	<script src="<%=contextRoot%>/jsp/script/skel-layers.min.js"></script>
	<script src="<%=contextRoot%>/jsp/script/init.js"></script>	
	<script src="<%=contextRoot%>/jsp/script/formValidations.js"></script>
	<script language="JavaScript" src="<%=contextRoot%>/jsp/script/CalendarControl.js"></script>
	<script src="<%=contextRoot%>/jsp/script/sweetalert.min.js"></script> 
	<script src="<%=contextRoot%>/jsp/script/sweetalert.js"></script>
	<script src="<%=contextRoot%>/jsp/script/sweetalert.min2.js"></script>
	<link rel="stylesheet" href="<%=contextRoot%>/jsp/css/sweetalert.min2.css">
	<link rel="stylesheet" href="<%=contextRoot%>/jsp/css/sweetalert.min.css" type="text/css">
	
	<noscript>
		<link rel="stylesheet" href="<%=contextRoot %>/css/style.css" />
		<link rel="stylesheet" href="<%=contextRoot %>/css/style-desktop.css" />
	</noscript>
	<link rel="stylesheet" href="<%=contextRoot%>/css/progress-wizard.css">
	<link rel="stylesheet" href="<%=contextRoot%>/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=contextRoot%>/css/bootstrap-switch.min.css">
	<link rel="stylesheet" href="<%=contextRoot%>/css/CalendarControl.css" type="text/css">
	<script>
	var currentdate;
		function validateAndSubmit(whichbutton) { 

			if (!$('#termsAndConditions').is(':checked')) {
			  swal("","Please indicate that you accept the Terms and Conditions");
			  return false;
			}else{
				document.getElementById("nxtbtn").style.display="none";
				document.getElementById("bckbtn").style.display="none";
				var action = document.forms[0].action;
				action = action + '?action=save';
				document.forms[0].action = action;
				document.forms[0].submit();
			}
		}
		function openTerms(){
			document.forms[0]["termsAndConditions"].checked=true;
		}
		function cancelPermit(whichbutton){
			var action = document.forms[0].action;
			action = action + '?action=reset';
			document.forms[0].action = action;
			document.forms[0].submit();
		}
		function backbutton(whichbutton){
			var action = document.forms[0].action;
			action = action + '?action=oai';
			document.forms[0].action = action;
			document.forms[0].submit();
		}

        function myFunction() {  
            window.open("<%=contextRoot%>/docs/GARAGE SALE CONDITIONS.pdf");
        } 
</script> 
</head>
<body class="homepage">
	<div class="wrapper style2">
		<div class="title">CITIZEN SERVICES ONLINE</div>
		<div id="main" class="container">
			<section id="features">
				<header class="style1">
				<h2 ><b> PERMIT PREVIEW DETAILS</b></h2>
					 <%
					 	OnlinePermitForm form = (OnlinePermitForm) request.getAttribute("onlinePermitFrom");
					 	String error = (String) request.getAttribute("error");
						String success = (String) request.getAttribute("success");
						if (error != null || success != null) {
					%>
					<div id="profile_status">
						<script type="text/javascript">
							var error = '<%=error%>';
							var success = '<%=success%>';
							var message = success;
							var color = 'Green';
							if(error != 'null') {
								message = error;
								color = 'red';
							}
							showMessage(message, color);
						</script>
					</div>
					<%
						}
					%>
					
				</header>
				<div class="feature-list">
					<div class="row">
											<div class="col-lg-9">
						<html:form action="permitPreview.do">
							<div class="panel panel-default">
                          		<div class="panel-heading" style="background-color: #A9A9A9">
                          		<button type="button" class="next_btn btn btn-primary" data-toggle="modal" data-target="#myModal">Cancel Permit Application</button>
											<!-- Modal -->
											<jsp:include page="cancelPermitDialog.jsp" />
                          		<!-- <a class="next_btn btn btn-primary" style="height: 40px;" onclick="cancelPermit('select')">Cancel Permit Application</a> -->
			                    	<b></b>
			                    </div>
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover">
									    <tr>
									    	<td><b>Address</b></td><td colspan="3"> <%=StringUtils.nullReplaceWithEmpty(form.getStreetNum())%>&nbsp;<%=StringUtils.nullReplaceWithEmpty(form.getStreetName())%>&nbsp;<%=StringUtils.nullReplaceWithEmpty(form.getStreetUnit()) %></td>
									    </tr>
									    <tr>
											<td><b>Permit Type  </b></td><td> <%=StringUtils.nullReplaceWithEmpty(form.getActivityTypeDesc()) %></td>
											<%-- <td><b>Sub Type</b></td><td>
											<%
											if(form.getSubActivityDesc()!=null){
																Iterator it = form.getSubActivityDesc().iterator();
																		while (it.hasNext()) { 
																			out.println("" + it.next() + "<br/>" + ""); 
																		}
											}
															%>
											<%=StringUtils.nullReplaceWithEmpty(form.getSubActivityDesc()) %>
											</td> --%>
									    </tr>
									    
									    <%--<tr>
									     <%--<%if(form.getValuation()!=null && !(form.getValuation().equalsIgnoreCase(""))){ %>
											<td><b>Valuation </b></td><td><%=StringUtils.nullReplaceWithEmpty(form.getValuation()) %></td>
										<%} %>
									         --%>
									         <%-- <td><b>Applied Date </b></td><td><script type="text/javascript">
									        
									        var today = new Date();
										    var dd = today.getDate();
										    var mm = today.getMonth()+1; //January is 0!

										    var yyyy = today.getFullYear();
										    if(dd<10){
										        dd='0'+dd
										    } 
										    if(mm<10){
										        mm='0'+mm
										    } 
										    var today = mm+'/'+dd+'/'+yyyy;
										    document.write(today);</script></td>
										     --%>
										   <%--  <%if( (StringUtils.nullReplaceWithEmpty(form.getValuation()).equalsIgnoreCase(""))){ %>
									        <td><b>&nbsp; </b></td><td>&nbsp;</td>
									        <%} %> --%>
									 <%--    </tr> --%>
									     <tr>
									    	<td><b>Description</b></td>
									    	<td>									    	
									    		<%= StringUtils.nullReplaceWithEmpty(form.getActDescription())%>
									    	</td>
									    </tr>
									    <tr>
									    <td><b>Sale Date</b></td>
									    <td><%= form.getIssueDate()%></td>
									    </tr>
									    <tr>
									    <td><b>End Date</b></td>
									    <td><%= form.getEndDate()%></td>
									    </tr>
									    <tr><td colspan="2"><center><b>Applicant Information</b></center></td></tr>
									    <tr>
									    <td><b>Applicant Name</b></td>
									    <td><%= StringUtils.nullReplaceWithEmpty(form.getFirstName())%> &nbsp; <%= StringUtils.nullReplaceWithEmpty(form.getLastName())%></td>
									    </tr>
									    <tr>
									    	<td><b>Address</b></td>
									    	<td>
												<%-- <html:text name="onlinePermitForm" property="address" />
												<html:text name="onlinePermitForm" property="city" />
												<html:text name="onlinePermitForm" property="state" />
												<html:text name="onlinePermitForm" property="zip" />  --%>
												<%if(form.getAddress()!=null && !form.getAddress().equals("")){%> <%= StringUtils.nullReplaceWithEmpty(form.getAddress())+"," %>&nbsp;<%}%>
												<%if(form.getCity()!=null && !form.getCity().equals("")){%> <%= StringUtils.nullReplaceWithEmpty(form.getCity())+"," %>&nbsp;<%}%>
												<%if(form.getState()!=null && !form.getState().equals("")){%> <%= StringUtils.nullReplaceWithEmpty(form.getState())+"," %>&nbsp;<%}%>
												<%if(form.getZip()!=null && !form.getZip().equals("")){%> <%= StringUtils.nullReplaceWithEmpty(form.getZip())%><%}%>
									    	 </td>
									    </tr>
									    <tr>
											<td><b>Phone Number </b></td>
											<td>
											<%-- <html:text name="onlinePermitForm" property="phoneNbr" />
											<html:text name="onlinePermitForm" property="phoneExt" /> --%>
											<%= StringUtils.nullReplaceWithEmpty(form.getPhoneNbr())%>&nbsp; 
											<%= StringUtils.nullReplaceWithEmpty(form.getPhoneExt()) %>
											</td>
											</tr>
											<tr>
										<td><b>Work Phone </b></td>
										<td>
										<%-- <html:text name="onlinePermitForm"	property="workPhone" />&nbsp; 
										<html:text	name="onlinePermitForm" property="workExt" /> --%>
										<%= StringUtils.nullReplaceWithEmpty(form.getWorkPhone()) %>&nbsp;
										<%= StringUtils.nullReplaceWithEmpty(form.getWorkExt()) %>
										</td>
										    
										
									    </tr>
									     <tr>
									    	<td><b>Email</b></td>
									    	<td>									    	
									    		<%-- <html:hidden name="onlinePermitForm"	property="emailAddress"></html:hidden> 
									    		<html:text	name="onlinePermitForm" property="emailAddress" disabled="true" /> --%>
									    		<!-- <bean:write name="onlinePermitForm" property="emailAddress"/> -->
									    		<%= StringUtils.nullReplaceWithEmpty(form.getEmailAddress()) %>
									    	</td>
									    </tr>
									</table>
								</div>							
							</div>
					
					 <div id="mytext" class="wrap-input100 validate-input" > 
			             <input type="checkbox" id="termsAndConditions" placeholder="Check Box">						
						<a href="javascript:openTerms();" class="text-primary" onclick = "myFunction()"><u>I have read and agree to the conditions for this permit</u></a> 
			        </div> 
							<%
							
								String action = request.getParameter("action");
							System.out.println("action   "+action);
							System.out.println("form.getActivityType()   "+form.getActivityType());
// 								if((form.getActivityType() != null && (action == null || !action.equals("save")))) {
							%>
							<a class="next_btn btn btn-primary" style="margin-bottom:10px;margin-left:10px;valign:left;height:35px;width: 60px" id="bckbtn" onclick="backbutton('select')">Back</a>
							<a class="next_btn btn btn-primary justify-content-center" style="margin-bottom:10px;margin-left:570px;valign:left;height: 35px;width: 195px" id="nxtbtn" onclick="validateAndSubmit('save')">Submit Permit Application</a>
							
<%-- 							<% } %> --%>
						</html:form>
					</div>
				</div>
			</section>
		</div>
	</div>	
</body>
</html>