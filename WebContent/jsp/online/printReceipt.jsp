<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"> 
<%@page import="org.owasp.encoder.Encode"%>
<%@ page import="javax.sql.*,sun.jdbc.rowset.*,java.util.*" %>
<%@ page import="elms.agent.OnlineAgent,elms.control.actions.ApplicationScope" %>
<%@ page import="elms.control.beans.PaymentMgrForm" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>

<%
String contextRoot = request.getContextPath();
String permitNo = (String) request.getParameter("permitNo")!=null?(String) request.getParameter("permitNo"):"";
String amount = (String) request.getParameter("amount")!=null?(String) request.getParameter("amount"):"";
String tran_date = (String) request.getParameter("tran_date")!=null?(String) request.getParameter("tran_date"):"";
String cardNo = (String) request.getParameter("cardNo")!=null?(String) request.getParameter("cardNo"):"";
String comboName = (String) request.getParameter("comboName")!=null?(String) request.getParameter("comboName"):"";
String txn_id = (String) request.getParameter("ssl_txn_id")!=null?(String) request.getParameter("ssl_txn_id"):"";

elms.security.User user = (elms.security.User)session.getAttribute(elms.common.Constants.USER_KEY);
String username= new OnlineAgent().getUName(user.userId);


%>



<script>


function printWindow(){
	document.getElementById("prnt").style.display = 'none';
	window.print();
}


</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>City of Burbank - Payment Receipt</title>
<style type="text/css">
<!--
.RNote {
	font: bold 25px Verdana, Arial, Helvetica, sans-serif;
	color:#FF0000
}
.RHeader {
	font: bold 10px Verdana, Arial, Helvetica, sans-serif;
}
.RContent {
	font: normal 10px Verdana, Arial, Helvetica, sans-serif;
}
.RAddress {
	font: bold 12px Verdana, Arial, Helvetica, sans-serif;
}
-->
</style>

</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
  <tr>
    <td class="RAddress"><img src="<%=contextRoot%>/jsp/online/images/Shield.gif" width="100" height="100"><br />
      <br />
      City of Burbank<br />
     
    </td>
    <td align="right" valign="bottom" class="RNote">PAYMENT RECEIVED</td>
  </tr>
</table>
<table width="100%" border="1" cellpadding="5" cellspacing="0" bordercolor="#000000">
  <tr>
    <td colspan="2" nowrap="nowrap" bgcolor="9FADBA" class="RHeader">TRANSACTION ID : <%=txn_id%> </td>
    <td width="1%" nowrap="nowrap" bgcolor="9FADBA" class="RHeader">DATE: <%=tran_date%> </td>
  </tr>
  <tr>
    <td width="1%" nowrap="nowrap" class="RHeader">APPLICANT NAME: </td>
    <td colspan="2" class="RContent"><%=username%></td>
  </tr>

  <tr>
  <%if(!comboName.equals("")){%>
    <td width="1%" nowrap="nowrap" bgcolor="9FADBA" class="RHeader">PERMIT NO </td>
    <td bgcolor="9FADBA" class="RHeader">DESCRIPTION</td>
  <%}else{%>
    <td width="1%" nowrap="nowrap" bgcolor="9FADBA" class="RHeader">ACCOUNT NO </td>
  <%}%>
    <td nowrap="nowrap" bgcolor="9FADBA" class="RHeader">AMOUNT PAID </td>
  </tr>
  <tr>
  <%if(!comboName.equals("")){%>
    <td width="1%" nowrap="nowrap" class="RContent"><%=permitNo%></td>
    <td class="RContent"><%=comboName%></td>
  <%}else{%>
  	<td width="1%" nowrap="nowrap" class="RContent"><%=permitNo%></td>
  <%}%>
    <td nowrap="nowrap" class="RContent"><%=amount%></td>
  </tr>
  <tr>
    <td width="1%" nowrap="nowrap" class="RHeader">CARD NUMBER  </td>
    <td colspan="2" class="RContent"><%=cardNo%></td>
  </tr>
</table>
<br />
<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td class="RContent"><p>Fees are assessed in accordance with the City of Burbank Citywide Fee Schedule adopted by the Burbank City Council. </p>
      <p>In the event that the applicant elects not to perform the permitted work and has not commenced construction, the fees paid may be refunded to the applicant.  A portion of the fees may be retained by the City for administrative costs incurred.  Each request must be in writing and submitted for approval to the Building Official. </p>
      <p>If a transaction was charged to your account in error or without your permission, the City of Burbank will cancel the original transaction and refund the payment.  Each request must be in writing and submitted for approval to the Building Official. </p>      
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr id="prnt">
    <td>
      <div align="center">
        <html:button  property="button" value="Print" style="width: 100px" onclick="printWindow()"/>            
      </div>
    </td>
  </tr>
</table>
<p>&nbsp; </p>
</body>
</html>