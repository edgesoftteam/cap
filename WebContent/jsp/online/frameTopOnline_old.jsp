<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ page import="elms.security.*,elms.common.*"%>
<%
   String contextRoot = request.getContextPath();
%>
<html:html>
<head>
<script>
function dohref(strHref)
{
   if (strHref == 'f_content') parent.f_content.location.href="welcomeOnline.jsp";
   else if (strHref == 'contacts')
   	window.open("<%=contextRoot%>/contacts.do","Oelms","toolbar=no,width=800,height=400,screenX=150,left=250,screenY=150,top=1,location=no,status=no,menubar=no,scrollbars=no,resizable=yes");
}
</script>
</head>
<html:base/>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" background="../images/site_bg_e5e5e5.jpg">


<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="top" width="1%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td valign="top"><img src="../images/site_logo_shield.jpg" width="68" height="61"></td>
                <td valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td colspan="2"><img src="../images/site_logo_text.jpg" width="133" height="41"></td>
                    </tr>
                    <tr>
                        <td bgcolor="#003366" width="99%"><font class="white2b">


                  <script>

					function makeArray() {
					  var args = makeArray.arguments;
					  for (var i = 0; i < args.length; i++) {
						this[i] = args[i];
					  }
					  this.length = args.length;
					}

					function fixDate(date) {
					  var base = new Date(0);
					  var skew = base.getTime();
					  if (skew > 0)
						date.setTime(date.getTime() - skew);
					}

					function getString(date) {
					  var months = new makeArray("January", "February", "March",
												 "April",   "May",      "June",
												 "July",    "August",   "September",
												 "October", "November", "December");
					  return months[date.getMonth()] + " " +
							 date.getDate() + ", " + ( date.getYear());
					}

					var cur = new Date();
					fixDate(cur);
					var str = getString(cur);
					document.write(str);
					</script> </td>
                        <td bgcolor="#003366" width="1%"><img src="../images/spacer.gif" width="1" height="20"></td>
                    </tr>
                </table>
                </td>
            </tr>
        </table>
        </td>
        <td valign="top" width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td background="../images/site_top_bg.jpg"><img src="../images/site_top_img.jpg" width="297" height="41"></td>
                <td background="../images/site_top_bg.jpg" align="right" valign="bottom">
        			<font class="white2b"> <%= ((elms.security.User)session.getAttribute(elms.common.Constants.USER_KEY)).getUsername()%>&nbsp;&nbsp; &nbsp;<br>

              	</td>
             </tr>
            <tr>
                <td background="../images/site_nav_bg.jpg" colspan="2">
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">

                        <td width="1" nowrap><img src="../images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td nowrap style="padding-left: 8px; padding-right: 8px" align="center" background="../images/site_nav_bg_on.jpg" valign="top">
                        <a class="blue2b" href="javascript:dohref('f_content')">HOME</a>
                        </td>

                        <td width="1" nowrap><img src="../images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td nowrap style="padding-left: 8px; padding-right: 8px" align="center" background="../images/site_nav_bg_on.jpg" nowrap>
                        <a class="blue2b" href="javascript:dohref('contacts')">CONTACT US</a>
                        </td>

                        <td width="1" nowrap ><img src="../images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td nowrap style="padding-left: 8px; padding-right: 8px" align="center" background="../images/site_nav_bg_on.jpg" nowrap>
                        <a class="blue2b"  href=javascript:parent.location.href='<%=contextRoot%>/onlineLogOut.do';>LOGOUT</a>
                        </td>

                    </tr>
                </table>
                </td>
            </tr>
        </table>
        </td>
    </tr>
    <tr>
        <td valign="top" colspan="2">&nbsp;</td>
    </tr>
</table>
</body>
</html:html>
