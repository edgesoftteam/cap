<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="elms.common.Constants"%>
<%@ page import="javax.sql.*,sun.jdbc.rowset.*,java.util.*,elms.control.actions.ApplicationScope" %>
<%@ page import="elms.app.finance.*" %>
<%@ page import="elms.control.beans.PaymentMgrForm" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>

<%
    	//left side details bar display
    	java.util.List tempOnlineDetails = new java.util.ArrayList();
    	tempOnlineDetails= (java.util.List)request.getAttribute("tempOnlineDetails");
    	PaymentMgrForm paymentMgrForm= (PaymentMgrForm)request.getAttribute("paymentMgrForm");

    	String tempOnlineID= (String)request.getAttribute("tempOnlineID");
     	String ssl_amount= (String)request.getAttribute("ssl_amount");
       	String ssl_card_number= (String)request.getAttribute("ssl_card_number");
        String ssl_exp_date= (String)request.getAttribute("ssl_exp_date");
        String ssl_exp_date_year= (String)request.getAttribute("ssl_exp_date_year");
        String ssl_exp_date_month= (String)request.getAttribute("ssl_exp_date_month");
        String ssl_cvv2cvc2= (String)request.getAttribute("ssl_cvv2cvc2");
        String comboNo= (String)request.getAttribute("comboNo");
        String comboName= paymentMgrForm.getComboName();
    	String contextHttpsRoot = (String)request.getAttribute("contextHttpsRoot")!=null?(String)request.getAttribute("contextHttpsRoot"):"";//ApplicationScope.contextHttpsRoot;
		String contextRoot = (String)request.getAttribute("contextRoot")!=null?(String)request.getAttribute("contextRoot"):request.getContextPath();
		String creditCardXX= ssl_card_number.substring(12,16);
		String ssl_avs_address= (String)request.getAttribute("ssl_avs_address");
		String ssl_avs_zip= (String)request.getAttribute("ssl_avs_zip");
		String ssl_ownerFName= (String)request.getAttribute("ssl_ownerFName");
       	String ssl_ownerLName= (String)request.getAttribute("ssl_ownerLName");
        String ssl_city= (String)request.getAttribute("ssl_city");
        String ssl_country= (String)request.getAttribute("ssl_country");
        String ssl_state= (String)request.getAttribute("ssl_state");
       	String isObc = ((elms.security.User)session.getAttribute(elms.common.Constants.USER_KEY)).getIsObc();
		String isDot = ((elms.security.User)session.getAttribute(elms.common.Constants.USER_KEY)).getIsDot();
%>
<style type="text/css">
    .FireTitle          { FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  td.FireHeading        { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none; PADDING: 10px; BACKGROUND-COLOR:#D9D9D9 }
    .FireHeading        { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireGlance         { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  td.FireResource       { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none; PADDING: 6px }
    .FireResource       { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
  td.FireResourceStatic { FONT-SIZE: 12px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none; PADDING: 6px }
    .FireResourceStatic { FONT-SIZE: 12px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireSelect         { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireText           { FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
    .FireHighlight      { FONT-SIZE: 15px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #990000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireMessage        { FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: normal; TEXT-DECORATION: none }
  td.FireMenu           { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none; PADDING: 2px; BACKGROUND-COLOR:#000000 }
    .FireMenu           { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:link      { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:visited   { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:active    { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu:hover     { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
    .FireMenu1          { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:link     { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:visited  { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:active   { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; FONT-WEIGHT: bold; TEXT-DECORATION: none }
   a.FireMenu1:hover    { FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #FFFFFF; FONT-WEIGHT: bold; TEXT-DECORATION: none }
  @page Section1 {
                   size:8.5in 11.0in;
                   margin:0.5in 0.5in 0.5in 0.5in;
                 }
 p.MsoNormal, li.MsoNormal, div.MsoNormal {
                margin:0in;
                margin-bottom:.0001pt;
                font-size:12.0pt;
                font-family:"Arial"
              }
 table.atable {
                border-collapse:collapse;
                border:1px solid #B3B3B3
              }
 tr           {
                page-break-inside:avoid
              }
 div.Section1 {
                page:Section1;
              }


.panelVisited            { font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #003265; text-decoration: none }
a.panelVisited:link      { font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #003265; text-decoration: none }
a.panelVisited:visited   { font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #003265; text-decoration: none }
a.panelVisited:active    { font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #003265; text-decoration: none }
a.panelVisited:hover     { font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #003265; text-decoration: none }
.panelVisitedColor { background-color:#62A85F }
.panelActive       { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #FFFFFF }
.panelActiveColor  { background-color:#EAC563 }
.panel             { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #FFFFFF }
.panelColor        { background-color:#BB2F2F }

</style>
<html:html>
<head>
<title>City of Burbank :Permitting and Licensing System: Fees</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="/elms/jsp/css/elms.css" type="text/css">
</head>

<script language="JavaScript" src="/elms/jsp/script/formValidations.js"></script>
<script language="JavaScript">

function confirmPayment()
{
		document.forms['paymentMgrForm'].button.disabled=true;
		document.forms['paymentMgrForm'].reset.disabled=true;
		document.forms['paymentMgrForm'].action="<%=contextRoot%>/payOnline.do";
		document.forms['paymentMgrForm'].submit();
}


function openTerms()
{
	window.open("<%=contextRoot%>/jsp/online/termsAndConditions.jsp?contextHttpsRoot=<%=contextHttpsRoot%>",target="_new","toolbar=no,width=1000,height=600,screenX=100,left=100,screenY=100,top=1,location=no,status=no,menubar=no,scrollbars=no");
}
function openPrivacy()
{
	window.open("<%=contextRoot%>/jsp/online/privacyPolicy.jsp?contextHttpsRoot=<%=contextHttpsRoot%>",target="_new","toolbar=no,width=1000,height=800,screenX=100,left=100,screenY=100,top=1,location=no,status=no,menubar=no,scrollbars=no");
}
function openRefund()
{
	window.open("<%=contextRoot%>/jsp/online/refundPolicy.jsp?contextHttpsRoot=<%=contextHttpsRoot%>",target="_new","toolbar=no,width=1000,height=800,screenX=100,left=100,screenY=100,top=1,location=no,status=no,menubar=no,scrollbars=no");
}


function disableFields()
{
 var ssl_amount = document.getElementById("details");
ssl_amount.style.display="block";
}



</SCRIPT>
<script language=JavaScript>


var message="Function Disabled!";


function clickIE4(){
if (event.button==2){

return false;
}
}

function clickNS4(e){
if (document.layers||document.getElementById&&!document.all){
if (e.which==2||e.which==3){

return false;
}
}
}

if (document.layers){
document.captureEvents(Event.MOUSEDOWN);
document.onmousedown=clickNS4;
}
else if (document.all&&!document.getElementById){
document.onmousedown=clickIE4;
}

document.oncontextmenu=new Function("return false")


</script>

<html:errors/>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form name="paymentMgrForm" type="elms.control.beans.PaymentMgrForm" action="/payOnline.do" >

<input type="hidden" name="ssl_amount" value="<%=ssl_amount%>">
<input type="hidden" name="comboName" value="<%=comboName%>">
<input type="hidden" name="tempOnlineID" value="<%=tempOnlineID%>">
<input type="hidden" name="ssl_card_number" value="<%=ssl_card_number%>" >
<input type="hidden" name="ssl_exp_date" value="<%=ssl_exp_date%>">
<input type="hidden" name="ssl_cvv2cvc2" value="<%=ssl_cvv2cvc2%>">
<input type="hidden" name="ssl_avs_address" value="<%=ssl_avs_address%>">
<input type="hidden" name="ssl_avs_zip" value="<%=ssl_avs_zip%>">
<input type="hidden" name="ssl_exp_date_year" value="<%=ssl_exp_date_year%>">
<input type="hidden" name="ssl_exp_date_month" value="<%=ssl_exp_date_month%>">
<input type="hidden" name="ssl_city" value="<%=ssl_city%>">
<input type="hidden" name="ssl_country" value="<%=ssl_country%>">
<input type="hidden" name="ssl_ownerFName" value="<%=ssl_ownerFName%>">
<input type="hidden" name="ssl_ownerLName" value="<%=ssl_ownerLName%>">
<input type="hidden" name="ssl_state" value="<%=ssl_state%>">



<table align="center" cellpadding="0" cellspacing="0" style="height:100%">
  <td align="center">

    <fieldset style="width: 450px;height: 320px" >
      <legend class="FireResourceStatic">

        <table cellpadding="5" cellspacing="0">
       <tr>
              <td><img src="<%=contextRoot%>/jsp/online/images/Shield.gif" width="100" height="100"></td>
              <td class="panelVisited" style="color: #003265; font-size:16px; font-weight: bold">CONFIRM PAYMENT DETAILS</td>
            </tr>
        </table>
      </legend>


      <br><br>


      <table cellpadding="15" cellspacing="0" width="100%">
        <html:errors />


        <% if(!comboName.equals(Constants.ONLINE_LNCV_PERMIT)){ %>
        <tr>
          <td align="center" class="panelVisited" style="font-size:12px">Permit No : <%=comboNo%></td>
        </tr>
           <% } %>
		<tr>
          <td align="center" class="panelVisited" style="font-size:12px">Permit Type : <%=comboName%></td>
        </tr>
        <tr>
          <td align="center" class="panelVisited" style="font-size:12px">&nbsp;&nbsp;&nbsp;&nbsp;Amount  :  $<%=ssl_amount%> </td>
        </tr>


        <tr>
          <td class="FireSelect">
            <div align="center">
              <html:reset  value="Back" styleClass="FireSelect" style="width: 100px" onclick="history.back();"/>
              <html:button  property="button" value="Confirm" styleClass="FireSelect" style="width: 100px" onclick="confirmPayment()"/>
            </div>
          </td>
        </tr>

      </table>
      <br><br>

    </fieldset>

    <br><br><br>

    <table align="center" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td>
          <div align="center">
              <a class="panelVisited" style="font-size:11px" href="<%=contextRoot%>/jsp/online/welcomeOnline.jsp";>Home</a>
              &nbsp; | &nbsp;<a class="panelVisited" style="font-size:11px" href="#" onclick="openTerms();">Terms & Conditions</a>
              &nbsp; | &nbsp;<a class="panelVisited" style="font-size:11px" href="#" onclick="openRefund();">Refund Policy</a>
              &nbsp; | &nbsp;<a class="panelVisited" style="font-size:11px" href="#" onclick="openPrivacy();">Privacy Policy</a>
              <font class="panelVisited" style="font-size:11px; color: #000000">&nbsp;&nbsp;&nbsp; � City of Burbank
          </div>
        </td>
      </tr>
    </table>
  </td>
</table>
</center>


</html:form>
</body>
</html:html>
