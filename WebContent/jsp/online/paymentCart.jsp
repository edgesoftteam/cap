<%@page import="elms.security.User"%>
<%@ page import="elms.control.beans.*,elms.common.Constants,elms.util.StringUtils,java.util.Map,java.util.HashMap" %>
<%@ page import="java.util.Random" %>
<%@ page import = "javax.servlet.RequestDispatcher" %>
<%@ page import="javax.crypto.Mac" %>
<%@ page import="javax.crypto.SecretKey" %>
<%@ page import="javax.crypto.spec.SecretKeySpec" %>

<%
Map<String, String> lkupSystemDataMap = new HashMap<String,String>();
lkupSystemDataMap = (Map<String,String>) request.getSession().getAttribute("lkupSystemDataMap");
String from = (String)request.getSession().getAttribute("from");


User user=(User)session.getAttribute("user");
String userName=user.getUsername();
String password=user.getPassword();

session.setAttribute("userName",userName);
session.setAttribute("password",password);

session.setAttribute("user", user);

String contextRoot = request.getContextPath();
String contextHttpsRoot = (String)request.getAttribute("contextHttpsRoot");

String ssl_amount= (String)request.getAttribute("ssl_amount");

String comboNo= (String)request.getAttribute("comboNo");
String comboName = (String)request.getAttribute("comboName");
String levelId= (String)request.getAttribute("sprojId");

String tempOnlineID= (String)request.getAttribute("tempOnlineID");


String prodServerFlag = lkupSystemDataMap.get(Constants.PROD_SERVER_FLAG);
String boaURL = "";
String x_login="";
String transactionKey ="";
String boaXFpHash="";
String formId ="";
String boaButtonCode ="";
if(prodServerFlag.equalsIgnoreCase("Y")){
    boaURL = lkupSystemDataMap.get(Constants.CAP_PROD_BOA_URL);
     x_login = lkupSystemDataMap.get(Constants.CAP_PROD_BOA_X_LOGIN);
     transactionKey = lkupSystemDataMap.get(Constants.CAP_PROD_BOA_TRANSACTIONKEY);
     boaXFpHash = lkupSystemDataMap.get(Constants.CAP_PROD_BOA_X_FP_HASH);
     formId = lkupSystemDataMap.get(Constants.CAP_PROD_BOA_FORM_ID);
     boaButtonCode = lkupSystemDataMap.get(Constants.CAP_PROD_BOA_BUTTON_CODE);   
}else{
     boaURL = lkupSystemDataMap.get(Constants.ONLINE_BOA_URL_FOR_TEST_ENV);
     x_login = lkupSystemDataMap.get(Constants.ONLINE_BOA_X_LOGIN_FOR_TEST_ENV);
     transactionKey = lkupSystemDataMap.get(Constants.ONLINE_BOA_TRANSACTIONKEY_FOR_TEST_ENV);
     boaXFpHash = lkupSystemDataMap.get(Constants.ONLINE_BOA_X_FP_HASH_FOR_TEST_ENV);
     formId = lkupSystemDataMap.get(Constants.ONLINE_BOA_FORM_ID_FOR_TEST_ENV);
     boaButtonCode = lkupSystemDataMap.get(Constants.ONLINE_BOA_BUTTON_CODE_FOR_TEST_ENV);   
}

String invoiceId=StringUtils.nullReplaceWithZero(request.getParameter("levelId"));
String feeAmount=StringUtils.nullReplaceWithZero(request.getParameter("amount"));
feeAmount = feeAmount.replace("Rs.", "");
feeAmount = feeAmount.replace("$", "");
feeAmount = feeAmount.replace(",", "");

if(feeAmount == "0.00" || feeAmount.equalsIgnoreCase("0.00")){
	feeAmount="0.50";
}

String x_amount = feeAmount;
String x_invoice_num = invoiceId;

// Generate a random sequence number
Random generator = new Random();
int x_fp_sequence = generator.nextInt(1000);

// Generate the timestamp
// Make sure this will be in UTC  
long x_fp_timestamp = System.currentTimeMillis()/1000;

// Use Java Cryptography functions to generate the x_fp_hash value
// generate secret key for HMAC-SHA1 using the transaction key
SecretKey key = new SecretKeySpec(transactionKey.getBytes(), boaXFpHash);

// Get instance of Mac object implementing HMAC-SHA1, and
// Initialize it with the above secret key
Mac mac = Mac.getInstance(boaXFpHash);
mac.init(key);

// process the input string
String inputstring = x_login + "^" + x_fp_sequence + "^" +x_fp_timestamp + "^" + x_amount + "^"+"USD";
byte[] result = mac.doFinal(inputstring.getBytes());
//convert the result from byte[] to hexadecimal format
StringBuffer strbuf = new StringBuffer(result.length * 2);
for(int i=0; i< result.length; i++)
{
    if(((int) result[i] & 0xff) < 0x10)
        strbuf.append("0");
    strbuf.append(Long.toString((int) result[i] & 0xff, 16));
}
String x_fp_hash = strbuf.toString();

%>
<html>
<head>
 <title>City of Burbank</title>
 <style type="text/css">
   label {
      display: block;
      margin: 5px 0px;
      color: #AAA;
   }
   input {
      display: block;
   }
   input[type=submit] {
      margin-top: 20px;
   }

 </style>  
 <script>

 function postPayment(){    
	 document.forms[0].action='<%=boaURL%>'; 
	 document.forms[0].submit();
	 
 }
 </script>
</head>
<body onload="postPayment();">
<%-- <form action="window.open(<%=boaURL%>, '_blank')" id="<%=formId%>" method="post"> --%>
<form action="<%=boaURL%>" id="<%=formId%>" method="post">
 <input type="hidden" name="x_login" value="<%= x_login %>" /> 
 <input type="hidden" name="x_fp_sequence" value="<%= x_fp_sequence %>" />
 <input type="hidden" name="x_fp_timestamp" value="<%= x_fp_timestamp %>" />
 <input type="hidden" name="x_currency_code" value="USD" /> 
 <input type="hidden" name="x_amount" value="<%=x_amount%>"  />
 <input type="hidden" name="x_invoice_num" value="<%=x_invoice_num%>"  />
 <input type="hidden" name="x_test_request" value="FALSE" />
 <input type="hidden" name="x_relay_response" value="" />
 <input type="hidden" name="donation_prompt" />
 <input type="hidden" name="button_code" value="<%=boaButtonCode%>" />
 <input type="hidden" name="x_fp_hash" value="<%=x_fp_hash %>" size="40"/>
 <input name="x_show_form" value="PAYMENT_FORM" type="hidden" />
 <input type="hidden" name="x_po_num" value="<%=x_invoice_num%>" /> 
 <input type="hidden" name="totalFee" value="<%=x_amount%>">

<input type="hidden" name="ssl_amount" value="<%=ssl_amount%>">
<html:hidden property="tempOnlineID" value="<%=tempOnlineID%>"/>
<input type="hidden" name="comboNo" value="<%=comboNo%>">
<input type="hidden" name="levelId" value="<%=levelId%>">
<input type="hidden" name="comboName" value="<%=comboName%>">
<input type="hidden" name="ssl_exp_date_year"/>
<input type="hidden" name="ssl_exp_date_month"/>
<input type="hidden" name="ssl_exp_date" value="ssl_exp_date">
<input type="hidden" name="from" value="<%=from%>">

</form>
</body>
</html>