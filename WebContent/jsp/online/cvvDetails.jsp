<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="org.owasp.encoder.Encode"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%
 	String contextRoot = request.getContextPath();
 	//left side details bar display
    java.util.List tempOnlineDetails = new java.util.ArrayList();
    tempOnlineDetails= (java.util.List)request.getAttribute("tempOnlineDetails");
    int peopleTypeId= elms.util.StringUtils.s2i((String)request.getAttribute("peopleTypeId"));
 	String forcePC=(String)request.getAttribute("forcePC");
 	String contextHttpsRoot =(String) request.getParameter("contextHttpsRoot");
%>
<html:html>
<head>
<style type="text/css">
<!--
td {
	font: 12px Arial, Helvetica, sans-serif;
}
body {
	font: 12px Arial, Helvetica, sans-serif;
}
-->
</style>
<title>

City of Burbank - CVV Details
</title>
</head>

<body>
<center>
<p><font face="Arial, Helvetica, sans-serif"><img src="<%=contextRoot%>/jsp/online/images/Shield.gif" width="100" height="100" /><br>
</p>
<table width="500" border="0" cellpadding="10" cellspacing="0">
  <col width="155">
  <col width="628">


  <tr height="20">
    <td height="20" colspan="2" valign="top" bgcolor="#B7C1CB"><font face="Arial, Helvetica, sans-serif"><strong>Security Code</strong></td>
    </tr>
  <tr height="20">
    <td height="20" colspan="2" valign="top">Card Verification Value (CVV) is an anti-fraud measure introduced by Card companies worldwide. It appears on the back of most Visa and Mastercards, and on the front of American Express cards. This code is a three or four digit number which provides a cryptographic check of the information embossed on the card. The code helps validate that the customer placing the order actually has the credit card in his/her possession, and that the credit/debit card account is legitimate. </td>
    </tr>
  <tr height="20">
    <td height="20" colspan="2" valign="top"><hr style="border: 1px dashed #033568"></td>
    </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><font face="Arial, Helvetica, sans-serif"><strong>Visa &amp; Mastercard </strong></td>
    <td width="325" valign="top">&nbsp;</td>
  </tr>
  <tr height="20">
    <td height="20" valign="top"><img src="<%=contextRoot%>/jsp/online/images/mc.png" width="173" height="108"></td>
    <td valign="top">Flip your card over and look at the signature box. You should see  the entire 16-digit credit card number (or just the last four digits) followed by a special 3 digit code. This 3 digit code is you CVV code. </td>
  </tr>
  <tr height="20">
    <td height="20" colspan="2" valign="top"><hr style="border: 1px dashed #033568"></td>
    </tr>
  <tr height="20">
    <td height="20" valign="top"><strong>American Express </strong></td>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr height="20">
    <td height="20" valign="top"><img src="<%=contextRoot%>/jsp/online/images/ae.png" width="173" height="108"></td>
    <td valign="top">Look for the 4 digit code printed on the front of your card just above and to the right of your main credit card number. </td>
  </tr>
  <tr height="20">
    <td height="20" valign="top"><img src="<%=contextRoot%>/jsp/online/images/ae2.png" width="173" height="108"></td>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr height="20">
    <td height="20" colspan="2" valign="top"><hr style="border: 1px dashed #033568"></td>
    </tr>
  <tr height="20">
    <td height="20" valign="top"><strong>Discover Card </strong></td>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr height="20">
    <td height="20" valign="top"><img src="<%=contextRoot%>/jsp/online/images/disc.png" width="173" height="108"></td>
    <td valign="top">The CVV code is the 3 digits that come after your credit card number on the back of your card. </td>
  </tr>

  <tr height="20">
    <td height="20" valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr height="20">
    <td height="20" colspan="2" valign="top" bgcolor="#B7C1CB">&nbsp;</td>
    </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</center>
</body>
</html:html>

