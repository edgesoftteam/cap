<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="elms.common.Constants"%>
<%@page import="elms.agent.LookupAgent"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%
 	String contextRoot = request.getContextPath();
	String isObc = ((elms.security.User)session.getAttribute(elms.common.Constants.USER_KEY)).getIsObc();
	String isDot = ((elms.security.User)session.getAttribute(elms.common.Constants.USER_KEY)).getIsDot(); 	
%> 
<html:html>
<head> 
<script language="javascript">
function openViewComboNames(){

window.open("<%=contextRoot%>/viewComboName.do?contextRoot=<%=contextRoot%>","blank","toolbar=no,width=900,height=600,screenX=100,left=100,screenY=100,top=1,location=no,status=no,menubar=no,scrollbars=no");
}

</script>
<style type="text/css">
<!--
td {
	font: 12px Arial, Helvetica, sans-serif;
}
body {
	font: 12px Arial, Helvetica, sans-serif;
}
.first {get ignored}

A:link {color:#003366; text-decoration:underline}
A:visited {color:#003366; text-decoration:underline}
A:active {color:#003366; text-decoration:underline}
A:hover {color:#003366; text-decoration:none}

.topnav:link {color:#003366; text-decoration:none}
.topnav:visited {color:#003366; text-decoration:none}
.topnav:active {color:#003366; text-decoration:none}
.topnav:hover {color:#003366; text-decoration:none}

.topnavhot:link {color:#FF0000; text-decoration:none}
.topnavhot:visited {color:#FF0000; text-decoration:none}
.topnavhot:active {color:#FF0000; text-decoration:none}
.topnavhot:hover {color:#FF0000; text-decoration:none}

.tree1:link {padding: 0px; border: #E5E5E5 1px solid; text-decoration:none}
.tree1:visited {padding: 0px; border: #E5E5E5 1px solid; text-decoration:none}
.tree1:active {padding: 0px; border: #9BABB9 1px solid; text-decoration:none}
.tree1:hover {padding: 0px; background-color: #C0C8CF; border: #9BABB9 1px solid; text-decoration:none}

.tree2:link {padding: 2px; border: #E5E5E5 1px solid; text-decoration:none}
.tree2:visited {padding: 2px; border: #E5E5E5 1px solid; text-decoration:none}
.tree2:active {padding: 2px; border: #9BABB9 1px solid; text-decoration:none}
.tree2:hover {padding: 2px; background-color: #C0C8CF; border: #9BABB9 1px solid; text-decoration:none}

.tree3:link {display:inline; padding: 2px; border: #E5E5E5 1px solid; text-decoration:none}
.tree3:visited {display:inline; padding: 2px; border: #E5E5E5 1px solid; text-decoration:none}
.tree3:active {display:inline; padding: 2px; border: #9BABB9 1px solid; text-decoration:none}
.tree3:hover {display:inline; padding: 2px; background-color: #C0C8CF; border: #9BABB9 1px solid; text-decoration:none}

.hdrs:link {color:#000000; text-decoration:underline}
.hdrs:visited {color:#000000; text-decoration:underline}
.hdrs:active {color:#000000; text-decoration:underline}
.hdrs:hover {color:#000000; text-decoration:none}

.hdrs_link:link {color:#000000; text-decoration:none}
.hdrs_link:visited {color:#000000; text-decoration:none}
.hdrs_link:active {color:#000000; text-decoration:none}
.hdrs_link:hover {color:#000000; text-decoration:underline}

.treetext {font: normal 8pt/9pt verdana,arial,helvetica; color:#003366}
.treetextdiff {font: normal 8pt/9pt verdana,arial,helvetica; color:#666666}

.black1 {font: normal 7pt/8pt verdana,arial,helvetica; color:#000000}
.black2b {font: bold 8pt/9pt verdana,arial,helvetica; color:#000000}

.blue2 {font: normal 8pt/9pt verdana,arial,helvetica; color:#003366}
.blue2b {font: bold 8pt/9pt verdana,arial,helvetica; color:#003366}
.blue2b:link {font: bold 8px verdana,arial,helvetica; color:#003366; text-decoration:none}
.blue2b:visited {font: bold 8px verdana,arial,helvetica; color:#003366; text-decoration:none}
.blue2b:active {font: bold 8px verdana,arial,helvetica; color:#003366; text-decoration:none}
.blue2b:hover {font: bold 8px verdana,arial,helvetica; color:#FFFFFF; text-decoration:none}

.blue2b2:link {font: bold 8px verdana,arial,helvetica; color:#003366; text-decoration:none}
.blue2b2:visited {font: bold 8px verdana,arial,helvetica; color:#003366; text-decoration:none}
.blue2b2:active {font: bold 8px verdana,arial,helvetica; color:#003366; text-decoration:none}
.blue2b2:hover {font: bold 8px verdana,arial,helvetica; color:#67819A; text-decoration:none}

.blue3b:link {font: bold 9px verdana,arial,helvetica; color:#003366; text-decoration:none}
.blue3b:visited {font: bold 9px verdana,arial,helvetica; color:#003366; text-decoration:none}
.blue3b:active {font: bold 9px verdana,arial,helvetica; color:#003366; text-decoration:none}
.blue3b:hover {font: bold 9px verdana,arial,helvetica; color:#67819A; text-decoration:none}

.con_text_1 {text-indent: 5px; font: normal 8pt/9pt verdana,arial,helvetica; color:#000000}
.con_td_1 { padding-left:5px; font: normal 8pt/9pt verdana,arial,helvetica; color:#000000}
.con_text_1_ni {font: normal 8pt/9pt verdana,arial,helvetica; color:#000000}
.con_text_1m {text-indent: 5px; font: normal 8pt/9pt verdana,arial,helvetica; color:#000000; text-align: right}
.con_text_red1 {text-indent: 5px; font: normal 8pt/9pt verdana,arial,helvetica; color:#FF0000}

.con_hdr_1 {text-indent: 5px; font: normal 8pt/9pt verdana,arial,helvetica; color:#000000}

.con_hdr_2b {text-indent: 7px; font: bold 10pt/11pt verdana,arial,helvetica; color:#000000}
.con_hdr_link {text-indent: 7px; font: normal 8pt/9pt verdana,arial,helvetica; color:#000000}
.con_hdr_3b {font: bold 11pt/12pt verdana,arial,helvetica; color:#000000}
.con_hdr_blue_3b {font: bold 10pt/12pt verdana,arial,helvetica; color:#003366}

.red1 {text-indent: 5px; font: normal 8pt/9pt verdana,arial,helvetica; color:#FF0000}
.red2 {text-indent: 5px; font: normal 9pt/10pt verdana,arial,helvetica; color:#FF0000}
.red3 {text-indent: 5px; font: bold 12pt/13pt verdana,arial,helvetica; color:#FF0000}
.red2b {font: bold 8pt/9pt verdana,arial,helvetica; color:#FF0000}

.white1 {font: normal 7pt/8pt verdana,arial,helvetica; color:#FFFFFF}
.white1b {font: bold 7pt/8pt verdana,arial,helvetica; color:#FFFFFF}
.white2 {font: normal 8pt/9pt verdana,arial,helvetica; color:#FFFFFF}
.white2b {font: bold 8pt/9pt verdana,arial,helvetica; color:#FFFFFF}
.white3 {font: normal 9pt/10pt verdana,arial,helvetica; color:#FFFFFF}
.white3b {font: bold 9pt/10pt verdana,arial,helvetica; color:#FFFFFF}
.white4 {font: normal 12pt/13pt verdana,arial,helvetica; color:#FFFFFF}
.white4b {font: bold 13pt/14pt verdana,arial,helvetica; color:#FFFFFF}

.green2b {font: bold 8pt/9pt verdana,arial,helvetica; color:#336633}

.button {font-size: 8pt}
.textbox {font-size: 8pt}
.listbox {font-size: 7pt}
.textboxm {font-size: 8pt; text-align: right}
.textboxg {font-size: 9pt}
.textboxd {font-size: 8pt; width=100px}

.label {font-size: 8pt;border-style:none;border:0px;border-width: 0px}

#menu1 {cursor: hand}
#menu1a {display: none}
#menu1a1 {display: none}
#menu1a2 {display: none}
#menu1a3 {display: none}
#menu1a4 {display: none}
#menu1b {cursor: hand}
#menu1b1 {display: none}
#menu1b2 {display: none}
#menu1b3 {display: none}
#menu1c {display: none}
#menu1c1 {display: none}
#menu1c2 {display: none}
#menu1c3 {display: none}
#menu2 {cursor: hand}
#menu2a {display: none}
#menu2a1 {display: none}
#menu2a2 {display: none}

#tree {
	font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;
	font-size: 11px;
}

}
#tree img {
	border: 0px;
	width: 19px;
	height: 16px;
}

-->
</style>
<title>
City of Burbank :- Welcome to Online Permitting System:
</title>
</head>
<body>
<br>

<table width="450" border="0" cellspacing="0" cellpadding="5" align="center">
  <tr>
    <td><p><font class="blue2b">Welcome to the <%=elms.agent.LookupAgent.getKeyValue("CITY_NAME") %> Online Portal ! </font></p>
      <p>Please refer to the navigation bar at the left of the screen to navigate. Some of the features include: </p>
      <table width="100%" border="0" cellspacing="0" cellpadding="10">
        <tr>
          <td width="1%" valign="top" nowrap="nowrap"><strong>My Profile </strong></td>
          <td width="99%" valign="top">       
          Review and update your profile to provide the City with your current contact information
          </td>
        </tr>
        <tr>
          <td width="1%" valign="top" nowrap="nowrap"><strong>My Permits </strong></td>
          <td width="99%" valign="top">My Active Permits<br />
             <a href="<%=contextRoot%>/myPermits.do?viewPermit=Yes&isObc=Y&isDot=N&inActive=Yes" >Click here for My Active Permits</a>
            </td>
        </tr>
        <tr>
          <td width="1%" valign="top" nowrap="nowrap"><strong>Inspections </strong></td>
          <td width="99%" valign="top">Request an Inspection, Review Scheduled Inspections, Cancel a Scheduled Inspection <br>
                                        <a href="<%=contextRoot%>/inspectionRequest.do?action=req" target="_self">Click here to Request an Inspection</a>.</td>
        </tr>
      
       <tr>
          <td width="1%" valign="top" nowrap="nowrap"><strong>Apply For LNCV Permit </strong></td>
          <td width="99%" valign="top">Parking permit issuance <br />
            Online credit card payment processing<br>
             <a href="<%=contextRoot%>/onlineLncv.do" >Click here to Apply for LNCV Permit Online</a>
            </td>
        </tr>
       
        <%if(LookupAgent.getKeyValue(Constants.GARAGE_SALE_FLAG).equalsIgnoreCase("Y")){ %>
	       <tr>
	          <td width="1%" valign="top" nowrap="nowrap"><strong>Garage Sale Application </strong></td>
	          <td width="99%" valign="top">Garage Sale Application<br />
	             <a href="<%=contextRoot%>/onlineAddress.do" >Click here for Garage Sale Application</a>
	            </td>
	        </tr>
        <%} %>
		<%-- 
        <tr>
          <td width="1%" valign="top" nowrap="nowrap"><strong>My Permits </strong></td>
          <td width="99%" valign="top">A summary of projects and activities with which you are affiliated. You may also link to other permits as an owner or contractor. </td>
        </tr>
       <tr>
          <td width="1%" valign="top" nowrap="nowrap"><strong>Apply For a Permit </strong></td>
          <td width="99%" valign="top">Project / permit queuing and issuance <br />
            Online credit card payment processing<br>
            <a href="#" onclick="openViewComboNames()">Click here to View Available Permits Online </a> 
            </td>
        </tr>
       <tr>
          <td width="1%" valign="top" nowrap="nowrap"><strong>Submit for Plan Check </strong></td>
          <td width="99%" valign="top">Online plan submittal and plan review for building permits
            </td>
        </tr>
        --%>

        
        <tr></tr>
      </table></td>
  </tr>
</table>
<!-- 
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="red2b">On-line project permits are currently available for single-family residential projects that do not require plan review.</td>
	</tr>
</table>
 -->
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><br />
</p>
</body>

</html:html>

