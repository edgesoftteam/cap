<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%
 	String contextRoot = request.getContextPath();
%>
<html:html>
<head>
<style type="text/css">
<!--
td {
	font: 12px Arial, Helvetica, sans-serif,border:1;
}
body {
	font: 12px Arial, Helvetica, sans-serif;
}


td {
	font: 12px Arial, Helvetica, sans-serif;
}
body {
	font: 12px Arial, Helvetica, sans-serif;
}
.first {get ignored}

A:link {color:#003366; text-decoration:underline}
A:visited {color:#003366; text-decoration:underline}
A:active {color:#003366; text-decoration:underline}
A:hover {color:#003366; text-decoration:none}

.topnav:link {color:#003366; text-decoration:none}
.topnav:visited {color:#003366; text-decoration:none}
.topnav:active {color:#003366; text-decoration:none}
.topnav:hover {color:#003366; text-decoration:none}

.topnavhot:link {color:#FF0000; text-decoration:none}
.topnavhot:visited {color:#FF0000; text-decoration:none}
.topnavhot:active {color:#FF0000; text-decoration:none}
.topnavhot:hover {color:#FF0000; text-decoration:none}

.tree1:link {padding: 0px; border: #E5E5E5 1px solid; text-decoration:none}
.tree1:visited {padding: 0px; border: #E5E5E5 1px solid; text-decoration:none}
.tree1:active {padding: 0px; border: #9BABB9 1px solid; text-decoration:none}
.tree1:hover {padding: 0px; background-color: #C0C8CF; border: #9BABB9 1px solid; text-decoration:none}

.tree2:link {padding: 2px; border: #E5E5E5 1px solid; text-decoration:none}
.tree2:visited {padding: 2px; border: #E5E5E5 1px solid; text-decoration:none}
.tree2:active {padding: 2px; border: #9BABB9 1px solid; text-decoration:none}
.tree2:hover {padding: 2px; background-color: #C0C8CF; border: #9BABB9 1px solid; text-decoration:none}

.tree3:link {display:inline; padding: 2px; border: #E5E5E5 1px solid; text-decoration:none}
.tree3:visited {display:inline; padding: 2px; border: #E5E5E5 1px solid; text-decoration:none}
.tree3:active {display:inline; padding: 2px; border: #9BABB9 1px solid; text-decoration:none}
.tree3:hover {display:inline; padding: 2px; background-color: #C0C8CF; border: #9BABB9 1px solid; text-decoration:none}

.hdrs:link {color:#000000; text-decoration:underline}
.hdrs:visited {color:#000000; text-decoration:underline}
.hdrs:active {color:#000000; text-decoration:underline}
.hdrs:hover {color:#000000; text-decoration:none}

.hdrs_link:link {color:#000000; text-decoration:none}
.hdrs_link:visited {color:#000000; text-decoration:none}
.hdrs_link:active {color:#000000; text-decoration:none}
.hdrs_link:hover {color:#000000; text-decoration:underline}

.treetext {font: normal 8pt/9pt verdana,arial,helvetica; color:#003366}
.treetextdiff {font: normal 8pt/9pt verdana,arial,helvetica; color:#666666}

.black1 {font: normal 7pt/8pt verdana,arial,helvetica; color:#000000}
.black2b {font: bold 8pt/9pt verdana,arial,helvetica; color:#000000}

.blue2 {font: normal 8pt/9pt verdana,arial,helvetica; color:#003366}
.blue2b {font: bold 8pt/9pt verdana,arial,helvetica; color:#003366}
.blue2b:link {font: bold 8px verdana,arial,helvetica; color:#003366; text-decoration:none}
.blue2b:visited {font: bold 8px verdana,arial,helvetica; color:#003366; text-decoration:none}
.blue2b:active {font: bold 8px verdana,arial,helvetica; color:#003366; text-decoration:none}
.blue2b:hover {font: bold 8px verdana,arial,helvetica; color:#FFFFFF; text-decoration:none}

.blue2b2:link {font: bold 8px verdana,arial,helvetica; color:#003366; text-decoration:none}
.blue2b2:visited {font: bold 8px verdana,arial,helvetica; color:#003366; text-decoration:none}
.blue2b2:active {font: bold 8px verdana,arial,helvetica; color:#003366; text-decoration:none}
.blue2b2:hover {font: bold 8px verdana,arial,helvetica; color:#67819A; text-decoration:none}

.blue3b:link {font: bold 9px verdana,arial,helvetica; color:#003366; text-decoration:none}
.blue3b:visited {font: bold 9px verdana,arial,helvetica; color:#003366; text-decoration:none}
.blue3b:active {font: bold 9px verdana,arial,helvetica; color:#003366; text-decoration:none}
.blue3b:hover {font: bold 9px verdana,arial,helvetica; color:#67819A; text-decoration:none}

.con_text_1 {text-indent: 5px; font: normal 8pt/9pt verdana,arial,helvetica; color:#000000}
.con_td_1 { padding-left:5px; font: normal 8pt/9pt verdana,arial,helvetica; color:#000000}
.con_text_1_ni {font: normal 8pt/9pt verdana,arial,helvetica; color:#000000}
.con_text_1m {text-indent: 5px; font: normal 8pt/9pt verdana,arial,helvetica; color:#000000; text-align: right}
.con_text_red1 {text-indent: 5px; font: normal 8pt/9pt verdana,arial,helvetica; color:#FF0000}

.con_hdr_1 {text-indent: 5px; font: normal 8pt/9pt verdana,arial,helvetica; color:#000000}

.con_hdr_2b {text-indent: 7px; font: bold 10pt/11pt verdana,arial,helvetica; color:#000000}
.con_hdr_link {text-indent: 7px; font: normal 8pt/9pt verdana,arial,helvetica; color:#000000}
.con_hdr_3b {font: bold 11pt/12pt verdana,arial,helvetica; color:#000000}
.con_hdr_blue_3b {font: bold 10pt/12pt verdana,arial,helvetica; color:#003366}

.red1 {text-indent: 5px; font: normal 8pt/9pt verdana,arial,helvetica; color:#FF0000}
.red2 {text-indent: 5px; font: normal 9pt/10pt verdana,arial,helvetica; color:#FF0000}
.red3 {text-indent: 5px; font: bold 12pt/13pt verdana,arial,helvetica; color:#FF0000}
.red2b {font: bold 8pt/9pt verdana,arial,helvetica; color:#FF0000}

.white1 {font: normal 7pt/8pt verdana,arial,helvetica; color:#FFFFFF}
.white1b {font: bold 7pt/8pt verdana,arial,helvetica; color:#FFFFFF}
.white2 {font: normal 8pt/9pt verdana,arial,helvetica; color:#FFFFFF}
.white2b {font: bold 8pt/9pt verdana,arial,helvetica; color:#FFFFFF}
.white3 {font: normal 9pt/10pt verdana,arial,helvetica; color:#FFFFFF}
.white3b {font: bold 9pt/10pt verdana,arial,helvetica; color:#FFFFFF}
.white4 {font: normal 12pt/13pt verdana,arial,helvetica; color:#FFFFFF}
.white4b {font: bold 13pt/14pt verdana,arial,helvetica; color:#FFFFFF}

.green2b {font: bold 8pt/9pt verdana,arial,helvetica; color:#336633}

.button {font-size: 8pt}
.textbox {font-size: 8pt}
.listbox {font-size: 7pt}
.textboxm {font-size: 8pt; text-align: right}
.textboxg {font-size: 9pt}
.textboxd {font-size: 8pt; width=100px}

.label {font-size: 8pt;border-style:none;border:0px;border-width: 0px}

#menu1 {cursor: hand}
#menu1a {display: none}
#menu1a1 {display: none}
#menu1a2 {display: none}
#menu1a3 {display: none}
#menu1a4 {display: none}
#menu1b {cursor: hand}
#menu1b1 {display: none}
#menu1b2 {display: none}
#menu1b3 {display: none}
#menu1c {display: none}
#menu1c1 {display: none}
#menu1c2 {display: none}
#menu1c3 {display: none}
#menu2 {cursor: hand}
#menu2a {display: none}
#menu2a1 {display: none}
#menu2a2 {display: none}

#tree {
	font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;
	font-size: 11px;
}

}
#tree img {
	border: 0px;
	width: 19px;
	height: 16px;
}

-->
</style>
<script language="javascript">


</script>

<title>
City of Burbank :Permitting and Licensing System: - contacts
</title>
<style>
td {
    border:none;
    border-collapse: collapse;
}

table  {
    border-top: 1px solid #000;
    border-left: 1px solid #000;
    border-right: 1px solid #000;
    border-bottom: 1px solid #000;
}

table td:first-child {
    border-left: none;
}

table td:last-child {
    border-right: none;
}
</style>
</head>
<body>
<center>
<!-- <p><font face="Arial, Helvetica, sans-serif"><img src="/elms/jsp/online/images/BHOBCLogo.gif" width="500" height="116" /> </p>--><br>

<table width="500" border="0" cellpadding="5" cellspacing="0" style="">
 
  <tr height="20" >
    <td height="20"  colspan="10" valign="top" bgcolor="#B7C1CB" align="center"><font face="Arial, Helvetica, sans-serif"><strong>CONTACT US</strong></font></td>
    </tr>

</table>
<br>
  
  <table width="500" border="0" cellpadding="5" cellspacing="0" style="">
  <tr>
    <td  colspan="2" valign="top" nowrap><font face="Arial, Helvetica, sans-serif"><strong>Building Inspection Requests</strong> </font></td>
    <td valign="top" colspan="8" ><font face="Arial, Helvetica, sans-serif"><strong>&nbsp;</strong></font> </td>
    
  </tr>
  
    <tr>
    <td  colspan="2" valign="top" nowrap><font face="Arial, Helvetica, sans-serif">&nbsp; </font></td>
    <td valign="top" colspan="8" ><font face="Arial, Helvetica, sans-serif"><strong>Building & Safety Division</strong></font> </td>
  </tr>

   <tr> 
    <td height="20"  colspan="2"  valign="top" nowrap><font face="Arial, Helvetica, sans-serif"><strong>&nbsp; </strong></font></td>
    <td valign="top" colspan="9" ><font face="Arial, Helvetica, sans-serif"><strong>By Phone :</strong> &nbsp;(818)238-5220 </font></td>
   
     
  </tr>
  
   <tr>
    <td width="170" colspan="2" ><font face="Arial, Helvetica, sans-serif"><strong>&nbsp;</strong></font></td>
    
    <td valign="top" colspan="8" nowrap><font face="Arial, Helvetica, sans-serif"><strong>Counter Hours:</strong>&nbsp;Monday - Friday   8:00 am - 11:45 am* and 1:00 pm to 3:00 pm
    <br>
    <span style="padding-right: 92px;"> </span>*Last ticket at 11:45 am. After 11:45 am <br> 
    <span style="padding-right: 92px;"></span>Plan Check Engineers available by appointment.
    </font></td>
   </tr>
   
   <tr>
    <td  width="170" valign="top" colspan="2"><font face="Arial, Helvetica, sans-serif">&nbsp; </font></td>
    <td valign="top" colspan="8" ><font face="Arial, Helvetica, sans-serif"><strong>&nbsp;</strong> </font></td>
  </tr>
   


  <tr>
    <td  width="153" valign="top" colspan="2"><font face="Arial, Helvetica, sans-serif">&nbsp; </font></td>
    <td valign="top" colspan="8" ><font face="Arial, Helvetica, sans-serif"><strong>&nbsp;</strong> </font></td>
  </tr>


  <tr>
    <td width="153"  colspan="2" valign="top" nowrap><font face="Arial, Helvetica, sans-serif"><strong>By appointment only</strong> </font></td>
    <td valign="top" colspan="8" ><font face="Arial, Helvetica, sans-serif"><strong>&nbsp;</strong></font> </td>
  </tr>
  
  <tr>
    <td  colspan="2" valign="top" ><font face="Arial, Helvetica, sans-serif">&nbsp; </font></td>
    <td valign="top" colspan="8" nowrap><font face="Arial, Helvetica, sans-serif"><strong>Inspector Hours:</strong> <span style="padding-right: 30px;"></span>Monday - Friday   7:00 am - 8:00 am and 3:00 pm to 3:30 pm</font> </td>
  </tr>

   <tr> 
    <td height="20"  colspan="2" valign="top" nowrap><font face="Arial, Helvetica, sans-serif"><strong>&nbsp; </strong></font></td>
    <td valign="top" colspan="9" nowrap><font face="Arial, Helvetica, sans-serif"><strong>General Office Hours:</strong> &nbsp;Monday - Friday   8:00 am - 12:00 pm and 1:00 pm - 5:00 pm</font></td>
  </tr>
  
   <tr>
    <td width="170" colspan="2" ><font face="Arial, Helvetica, sans-serif"><strong>&nbsp;</strong></font></td>
    
    <td valign="top" colspan="8" nowrap><font face="Arial, Helvetica, sans-serif"><font face="Arial, Helvetica, sans-serif"><strong>Address:</strong> &nbsp;First floor of the Community Services Building <br>
     <span style="padding-right: 56px;"></span>150 N. Third St., Burbank, CA 91502
     </font>
     </td>
   </tr>
   
   <tr>
    <td width="170" colspan="2" ><font face="Arial, Helvetica, sans-serif"><strong>&nbsp;</strong></font></td>
    <td valign="top" colspan="8" nowrap><font face="Arial, Helvetica, sans-serif"><strong>Email:</strong> &nbsp; Building@burbankca.gov</font>
     </td>
   </tr>   
   
   <tr>
    <td width="170" colspan="2" ><font face="Arial, Helvetica, sans-serif"><strong>&nbsp;</strong></font></td>
    <td valign="top" colspan="8" nowrap><font face="Arial, Helvetica, sans-serif"> &nbsp;</font>
     </td>
   </tr> 

</table>   
<br>
<table width="500" border="0" cellpadding="5" cellspacing="0" align="center"> 

  <tr>
    <td  width="170" colspan="2" valign="top" nowrap><font face="Arial, Helvetica, sans-serif"><strong>&nbsp;</strong> </font></td>
    <td valign="top" colspan="8" ><font face="Arial, Helvetica, sans-serif"><strong>&nbsp;</strong></font> </td>
    
  </tr>
  
    <tr>
    <td width="170" colspan="2">&nbsp; </td>
    <td colspan="8"  valign="top" nowrap ><font face="Arial, Helvetica, sans-serif"><strong>Public Works Department</strong></font> </td>
  </tr>
  
  

   <tr>
    <td width="170" colspan="2" ><font face="Arial, Helvetica, sans-serif"><strong>&nbsp;</strong></font></td>
    <td valign="top" colspan="8" nowrap><font face="Arial, Helvetica, sans-serif"><strong>By Phone :</strong> &nbsp;(818) 238-3915 </font> 
     </td>
   </tr>   
   
   <tr>
    <td width="170" colspan="2" ><font face="Arial, Helvetica, sans-serif"><strong>&nbsp;</strong></font></td>
    <td valign="top" colspan="8" nowrap><font face="Arial, Helvetica, sans-serif"> <strong>Counter Hours:</strong> Monday - Friday 8:00 am - 4:00 pm </font>
     </td>
   </tr>   
 

   <tr>
    <td width="170" colspan="2" ><font face="Arial, Helvetica, sans-serif"><strong>&nbsp;</strong></font></td>
    <td valign="top" colspan="8" nowrap><font face="Arial, Helvetica, sans-serif">For after hour emergencies, call (818) 238-3778 or (818) 238-3000   <span style="padding-right: 105px;"> </span>            </font>
     </td>
   </tr>   

   <tr>
    <td width="170" colspan="2" ><font face="Arial, Helvetica, sans-serif"><strong>&nbsp;</strong></font></td>
    <td valign="top" colspan="8" nowrap><font face="Arial, Helvetica, sans-serif"><strong>Address:</strong> &nbsp;150 North Third Street Burbank, CA 91502</font>
     </td>
   </tr>   
  <tr>
    <td  valign="top" colspan="2"><font face="Arial, Helvetica, sans-serif">&nbsp; </font></td>
    <td valign="top" colspan="8" ><font face="Arial, Helvetica, sans-serif"><strong>&nbsp;</strong> </font></td>
  </tr>

  </table></center>
 <!--  <tr height="20">
    <td height="20" colspan="2" valign="top"><hr style="border: 1px dashed #033568"></td>
    </tr>
  <tr height="20">
    <td width="175" height="20" valign="top" nowrap><font face="Arial, Helvetica, sans-serif"><strong>Office Hours </strong></td>
    <td width="325" valign="top"><font face="Arial, Helvetica, sans-serif">Monday - Friday: 8:00am - 12:00pm; 1:00pm - 4:00pm<br>
      Saturday &amp; Sunday: CLOSED </td>
  </tr>
  <tr height="20">
    <td height="20" colspan="2" valign="top"><hr style="border: 1px dashed #033568"></td>
    </tr>
  <tr height="20">
    <td width="175" height="20" valign="top" nowrap><font face="Arial, Helvetica, sans-serif"><strong>Address </strong></td>
    <td width="325" valign="top"><font face="Arial, Helvetica, sans-serif">150 N. Third St.<br>
      Burbank, CA  91502 </td>
  </tr>
  <tr height="20">
    <td height="20" valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr height="20">
    <td height="20" colspan="2" valign="top" bgcolor="#B7C1CB">&nbsp;</td>
    </tr>
</table>

<table width="500" border="0" cellpadding="10" cellspacing="0">
  <col width="155">
  <col width="628">



  <tr height="20">
    <td height="20" valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
  </tr>
  
  <tr height="20">
    <td><font face="Arial, Helvetica, sans-serif">&nbsp;</strong></td>
    <td width="325" valign="top"><font face="Arial, Helvetica, sans-serif"><strong>PUBLIC WORKS</strong> </td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top" nowrap><font face="Arial, Helvetica, sans-serif"><strong>General Information </strong></td>
    <td width="325" valign="top"><font face="Arial, Helvetica, sans-serif">(818)238-5220 </td>
  </tr>
  <tr height="20">
    <td height="20" colspan="2" valign="top"><hr style="border: 1px dashed #033568"></td>
    </tr>
  <tr height="20">
    <td width="175" height="20" valign="top" nowrap><font face="Arial, Helvetica, sans-serif"><strong>Office Hours </strong></td>
    <td width="325" valign="top"><font face="Arial, Helvetica, sans-serif">Monday - Friday: 8:00am - 12:00pm; 1:00pm - 4:00pm<br>
      Saturday &amp; Sunday: CLOSED </td>
  </tr>
  <tr height="20">
    <td height="20" colspan="2" valign="top"><hr style="border: 1px dashed #033568"></td>
    </tr>
  <tr height="20">
    <td width="175" height="20" valign="top" nowrap><font face="Arial, Helvetica, sans-serif"><strong>Address </strong></td>
    <td width="325" valign="top"><font face="Arial, Helvetica, sans-serif">150 N. Third St.<br>
      Burbank, CA  91502 </td>
  </tr>
  <tr height="20">
    <td height="20" valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr height="20">
    <td height="20" colspan="2" valign="top" bgcolor="#B7C1CB">&nbsp;</td>
    </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</center>  -->
</body>

</html:html>

