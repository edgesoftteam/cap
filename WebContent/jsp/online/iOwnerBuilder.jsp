<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%
 	String contextRoot = request.getContextPath();
	
	String fullName = (String)request.getAttribute("fullName")!=null?(String)request.getAttribute("fullName"):"";

%>
<html:html>
<html:base/>
<head>
<link rel="stylesheet" href="../css/elms.css" type="text/css">

	<link rel="stylesheet" href="../css/online.css" type="text/css">
	
<script language="javascript">

/* function disableNext(){
	document.getElementById('button').disabled=true;
}

function enableNext(){
	document.getElementById('button').enabled=true;
}
 */
function getCheckedValue(radioObj) {
	if(!radioObj)
		return "";
	var radioLength = radioObj.length;
	if(radioLength == undefined)
		if(radioObj.checked)
			return radioObj.value;
		else
			return "";
	for(var i = 0; i < radioLength; i++) {
		if(radioObj[i].checked) {
			return radioObj[i].value;
		}
	}
	return "";
}

function next()
{
	    var tempOnlineID = document.forms[0].tempOnlineID.value;
	    var landLsoId = "";
	    var lsoType = "";
        lsoType = "S";

      var selectPeopleType = getCheckedValue(document.forms[0].elements['selectPeopleType']);
      var  sName=    selectPeopleType;

	    document.forms[0].action='<%=contextRoot%>/checkOwner.do?tempOnlineID='+tempOnlineID+"&landLsoId="+landLsoId+"&lsoType="+lsoType+"&sName="+sName;
		document.forms[0].submit();
}

function apply()
{
	document.forms[0].button.disabled=true;
	if(document.forms[0].iAgree.checked==true)
	{
		document.forms[0].button.disabled=false;
	}
	if(document.forms[0].iAgree.checked==false)
	{
		document.forms[0].button.disabled=true;
	}
}

</script>
</head>
<body text="#000000" leftmargin="5" bottommargin="0"  scroll="yes" topmargin="0" marginwidth="0" marginheight="0" rightmargin="0" onload="apply()">



<center>

<html:form action="/checkOwner">
<html:hidden property="tempOnlineID" />
<table align="center" cellpadding="0" cellspacing="0" style="height:100%" width="100%">

	<td align="center" valign="top" width="99%">
	<br><br> <br><br>
<fieldset style="width: 450px;height: 100%" >
<br><br>


<table cellpadding="15" cellspacing="0" width="100%">
 <html:errors />
  
  	<tr>
        <td align="left"> <font class="panelVisited" style="font-size:30px"> 5<font class="panelVisited" style="font-size:12px"><bean:message key="iOwnerTitle"/>
        </td>
   </tr>

	<tr>
		<td align ="left" class="con_hdr_2b"><%=fullName %> </td>
	</tr>

	<tr>
		<td align ="left" class="con_text_red1"><bean:message key="iOwnerText"/> </td>
	</tr>

	<tr>
		<td align ="left" class="FireResource"><html:checkbox property="iAgree" styleClass="custom" onclick="apply()" >I have Read and Agree to the Terms and Conditions listed above </html:checkbox></td>
	</tr>

    <tr>
         <td class="FireSelect">
         <div align="center">
			<html:reset  value="Back" styleClass="FireSelect" style="width: 100px" onclick="history.back();"/>
			<html:button  property="button" value="Next" styleClass="FireSelect" style="width: 100px" onclick="next()"/>
           </div>
      </tr>
</table>
</fieldset>
</html:form>
</center>
	</td> 

			<jsp:include page="onlineSideBar.jsp" flush="true">
				<jsp:param name="page" value="5" />
			</jsp:include>

 </table>
</body>
</html:html>

