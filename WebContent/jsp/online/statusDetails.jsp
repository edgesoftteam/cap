<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="org.owasp.encoder.Encode"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%
 	String contextRoot = request.getContextPath();
 	//left side details bar display
    java.util.List tempOnlineDetails = new java.util.ArrayList();
    tempOnlineDetails= (java.util.List)request.getAttribute("tempOnlineDetails");
    int peopleTypeId= elms.util.StringUtils.s2i((String)request.getAttribute("peopleTypeId"));
 	String forcePC=(String)request.getAttribute("forcePC");
 	String contextHttpsRoot =(String) request.getParameter("contextHttpsRoot");
%>
<html:html>
<head>
<style type="text/css">
<!--
td {
	font: 12px Arial, Helvetica, sans-serif;
}
body {
	font: 12px Arial, Helvetica, sans-serif;
}
-->
</style>
<title>
City of Burbank  - Status Details
</title>
</head>
<body>
<center>
<p><img src="../images/site_logo_shield.jpg" /><br>
</p>
<table width="500" border="1" cellpadding="5" cellspacing="0" bordercolor="#000000">
  <col width="155">
  <col width="628">
  <tr height="20">
    <td height="20" colspan="2" valign="top" bgcolor="#033568"><strong><font color="#FFFFFF" size="4">STATUS LEGEND </strong></td>
    </tr>
  <tr height="20">
    <td width="175" height="20" valign="top" bgcolor="#B7C1CB"><strong>STATUS</strong></td>
    <td width="325" valign="top" bgcolor="#B7C1CB"><strong>DESCRIPTION</strong></td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>App complete</strong></td>
    <td width="325" valign="top">Application is complete</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>Appeal filed</strong></td>
    <td width="325" valign="top">Application has been filed</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>Approval</strong></td>
    <td width="325" valign="top">Approval has been granted</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>Balance Due&nbsp;</strong></td>
    <td width="325" valign="top">A balance is due; payment is required</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>Case file prprd</strong></td>
    <td width="325" valign="top">A case file has been prepared</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>Case logged in</strong></td>
    <td width="325" valign="top">A case has been logged in</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>CC hearing</strong></td>
    <td width="325" valign="top">Activity is subject to City Council Hearing</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>CC ord adpt</strong></td>
    <td width="325" valign="top">City Council has adopted as an ordinance</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>CC ord intro</strong></td>
    <td width="325" valign="top">Introduced as a City Council ordinance</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>Closed&nbsp;</strong></td>
    <td width="325" valign="top">Activity is closed</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>Cond coded</strong></td>
    <td width="325" valign="top">Condition has been codified</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>Contact appl</strong></td>
    <td width="325" valign="top">Contact with the activity applicant is required</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>Denial&nbsp;</strong></td>
    <td width="325" valign="top">Activity has been denied</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>Env Assess</strong></td>
    <td width="325" valign="top">Activity is in the environmental assessment stage</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>Expired&nbsp;</strong></td>
    <td width="325" valign="top">Acitivity has expired</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>Final&nbsp;</strong></td>
    <td width="325" valign="top">Activity has received final approval</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>Hold&nbsp;</strong></td>
    <td width="325" valign="top">Activity is on hold</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>Issued&nbsp;</strong></td>
    <td width="325" valign="top">Activity has been approved and issued</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>Mailed Pub Not</strong></td>
    <td width="325" valign="top">Public notices have been mailed</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>News pub not</strong></td>
    <td width="325" valign="top">Public notices has been posted in news publications</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>Not of decision</strong></td>
    <td width="325" valign="top">Notice of decision</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>Not of determn</strong></td>
    <td width="325" valign="top">Notice of determination</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>Notice of avail</strong></td>
    <td width="325" valign="top">Notice of availability</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>PC Appr by FD</strong></td>
    <td width="325" valign="top">Plan check completed and approved by Fire Department</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>PC Appr/Fee</strong></td>
    <td width="325" valign="top">Plan check completed and approved, with fees due</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>PC Approved</strong></td>
    <td width="325" valign="top">Plan check completed and approved</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>PC Assgn to FD</strong></td>
    <td width="325" valign="top">Plans assigned to Fire Department for review</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>PC Assgn to PRE</strong></td>
    <td width="325" valign="top">Plans assigned to project plan reviewer</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>PC Denied by FD&nbsp;</strong></td>
    <td width="325" valign="top">Plans reviewed and denied by Fire Department</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>PC Expired</strong></td>
    <td width="325" valign="top">Activity has been idle in plan check stage longer than six months, and    therefore has expired</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>PC Extended&nbsp;</strong></td>
    <td width="325" valign="top">Activity was previously expired, but has been granted a one-time    extension of six months</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>PC Final</strong></td>
    <td width="325" valign="top">We're not using this any more - do not include</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>PC Hold&nbsp;</strong></td>
    <td width="325" valign="top">Plan check is on hold; confer with plan reviewer for further details</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>PC in Progress</strong></td>
    <td width="325" valign="top">Plan review is currently in progress; any updates will be indicated by a    change in plan check status</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>PC Not Required&nbsp;</strong></td>
    <td width="325" valign="top">Plan check is not required for issuance of permit</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>PC Permit RTI</strong></td>
    <td width="325" valign="top">Plan check completed and approved; permit is ready to be issued</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>PC Picked Up&nbsp;</strong></td>
    <td width="325" valign="top">Plans picked up by applicant</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>PC Plan Incmplt</strong></td>
    <td width="325" valign="top">Plans submitted, but determined to be incomplete</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>PC Required</strong></td>
    <td width="325" valign="top">Plan check required prior to permit issuance</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>PC Resubmitted w/ FD</strong></td>
    <td width="325" valign="top">Plans resubmitted to Fire Department</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>PC Resubmitted w/ PRE</strong></td>
    <td width="325" valign="top">Plans resubmitted to project plan reviewer</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>PC Sub/Fee&nbsp;</strong></td>
    <td width="325" valign="top">Plans submitted, with fees due</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>PC Submitted</strong></td>
    <td width="325" valign="top">Plans submitted for review</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>PC Verification&nbsp;</strong></td>
    <td width="325" valign="top">Plans submitted for cursory verification review</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>PC Void</strong></td>
    <td width="325" valign="top">Plan check is void</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>PC w/ corr FD</strong></td>
    <td width="325" valign="top">Plans require corrections, as determined by Fire Department&nbsp;</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>PC w/Corr w/ PRE</strong></td>
    <td width="325" valign="top">Plans require corrections, as determined by project plan reviewer&nbsp;</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>PC w/Corr/Fee</strong></td>
    <td width="325" valign="top">Plans require corrections; additional fees due</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>PC Withdrawn</strong></td>
    <td width="325" valign="top">Plans withdrawn from plan check by applicant</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>Pending&nbsp;</strong></td>
    <td width="325" valign="top">Acitivity is pending further action (e.g. payment, approval)</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>Permit Ready&nbsp;</strong></td>
    <td width="325" valign="top">We're not using this any more - do not include</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>Permit Required&nbsp;</strong></td>
    <td width="325" valign="top">A permit is required prior to completion of proposed activity</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>Permit RTI&nbsp;</strong></td>
    <td width="325" valign="top">Plan check completed and approved; permit is ready to be issued</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>Refund Pending</strong></td>
    <td width="325" valign="top">Activity has a pending refund</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>Refunded</strong></td>
    <td width="325" valign="top">Activity fees have been refunded to applicant</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>Resln Adpt</strong></td>
    <td width="325" valign="top">Resolution has been adopted</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>Resltn coded</strong></td>
    <td width="325" valign="top">Resolution has been codified</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>Resltn to appl</strong></td>
    <td width="325" valign="top">Resolution has been given to applicant</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>Revised Plans</strong></td>
    <td width="325" valign="top">Plans have been revised</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>Revoked&nbsp;</strong></td>
    <td width="325" valign="top">Activity has been revoked</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>T.C.O.&nbsp;</strong></td>
    <td width="325" valign="top">Temporary Certificate of Occupancy</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>Void&nbsp;</strong></td>
    <td width="325" valign="top">Activity is void</td>
  </tr>
  <tr height="20">
    <td width="175" height="20" valign="top"><strong>Withdrawn&nbsp;</strong></td>
    <td width="325" valign="top">Activity has been withdrawn</td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</center>
</body>
</html:html>

