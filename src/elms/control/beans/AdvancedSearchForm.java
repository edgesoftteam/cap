package elms.control.beans;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class AdvancedSearchForm extends ActionForm {

	/**
	 * Member variable declaration
	 */

	protected String query;
	protected String table;
	protected String activityColumns;
	protected String activityOperators;
	protected String activityValue;
	protected String add;
	protected String projectColumns;
	protected String projectOperators;
	protected String projectValue;
	protected String inspectionColumns;
	protected String inspectionOperators;
	protected String inspectionValue;
	protected String and;
	protected String or;
	protected String not;
	protected String brackets;
	protected String search;
	protected String cancel;

	// activity array
	// project array
	// inspection array

	public AdvancedSearchForm() {
		query = "";
		table = "";
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		// query ="";

	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		// delcare validations for the form
		/*
		 * if ((firstName == null) || (firstName.length() < 1)) errors.add("firstName",new ActionError("error.firstname.required"));
		 * 
		 * if ((lastName == null) || (lastName.length() < 1)) errors.add("lastName",new ActionError("error.lastname.required"));
		 */
		return errors;
	}

	/**
	 * Gets the query
	 * 
	 * @return Returns a String
	 */
	public String getQuery() {
		return query;
	}

	/**
	 * Sets the query
	 * 
	 * @param query
	 *            The query to set
	 */
	public void setQuery(String query) {
		this.query = query;
	}

	/**
	 * Gets the table
	 * 
	 * @return Returns a String
	 */
	public String getTable() {
		return table;
	}

	/**
	 * Sets the table
	 * 
	 * @param table
	 *            The table to set
	 */
	public void setTable(String table) {
		this.table = table;
	}

	/**
	 * Gets the activityColumns
	 * 
	 * @return Returns a String
	 */
	public String getActivityColumns() {
		return activityColumns;
	}

	/**
	 * Sets the activityColumns
	 * 
	 * @param activityColumns
	 *            The activityColumns to set
	 */
	public void setActivityColumns(String activityColumns) {
		this.activityColumns = activityColumns;
	}

	/**
	 * Gets the activityOperators
	 * 
	 * @return Returns a String
	 */
	public String getActivityOperators() {
		return activityOperators;
	}

	/**
	 * Sets the activityOperators
	 * 
	 * @param activityOperators
	 *            The activityOperators to set
	 */
	public void setActivityOperators(String activityOperators) {
		this.activityOperators = activityOperators;
	}

	/**
	 * Gets the activityValue
	 * 
	 * @return Returns a String
	 */
	public String getActivityValue() {
		return activityValue;
	}

	/**
	 * Sets the activityValue
	 * 
	 * @param activityValue
	 *            The activityValue to set
	 */
	public void setActivityValue(String activityValue) {
		this.activityValue = activityValue;
	}

	/**
	 * Gets the add
	 * 
	 * @return Returns a String
	 */
	public String getAdd() {
		return add;
	}

	/**
	 * Sets the add
	 * 
	 * @param add
	 *            The add to set
	 */
	public void setAdd(String add) {
		this.add = add;
	}

	/**
	 * Gets the projectColumns
	 * 
	 * @return Returns a String
	 */
	public String getProjectColumns() {
		return projectColumns;
	}

	/**
	 * Sets the projectColumns
	 * 
	 * @param projectColumns
	 *            The projectColumns to set
	 */
	public void setProjectColumns(String projectColumns) {
		this.projectColumns = projectColumns;
	}

	/**
	 * Gets the projectOperators
	 * 
	 * @return Returns a String
	 */
	public String getProjectOperators() {
		return projectOperators;
	}

	/**
	 * Sets the projectOperators
	 * 
	 * @param projectOperators
	 *            The projectOperators to set
	 */
	public void setProjectOperators(String projectOperators) {
		this.projectOperators = projectOperators;
	}

	/**
	 * Gets the projectValue
	 * 
	 * @return Returns a String
	 */
	public String getProjectValue() {
		return projectValue;
	}

	/**
	 * Sets the projectValue
	 * 
	 * @param projectValue
	 *            The projectValue to set
	 */
	public void setProjectValue(String projectValue) {
		this.projectValue = projectValue;
	}

	/**
	 * Gets the inspectionColumns
	 * 
	 * @return Returns a String
	 */
	public String getInspectionColumns() {
		return inspectionColumns;
	}

	/**
	 * Sets the inspectionColumns
	 * 
	 * @param inspectionColumns
	 *            The inspectionColumns to set
	 */
	public void setInspectionColumns(String inspectionColumns) {
		this.inspectionColumns = inspectionColumns;
	}

	/**
	 * Gets the inspectionOperators
	 * 
	 * @return Returns a String
	 */
	public String getInspectionOperators() {
		return inspectionOperators;
	}

	/**
	 * Sets the inspectionOperators
	 * 
	 * @param inspectionOperators
	 *            The inspectionOperators to set
	 */
	public void setInspectionOperators(String inspectionOperators) {
		this.inspectionOperators = inspectionOperators;
	}

	/**
	 * Gets the inspectionValue
	 * 
	 * @return Returns a String
	 */
	public String getInspectionValue() {
		return inspectionValue;
	}

	/**
	 * Sets the inspectionValue
	 * 
	 * @param inspectionValue
	 *            The inspectionValue to set
	 */
	public void setInspectionValue(String inspectionValue) {
		this.inspectionValue = inspectionValue;
	}

	/**
	 * Gets the and
	 * 
	 * @return Returns a String
	 */
	public String getAnd() {
		return and;
	}

	/**
	 * Sets the and
	 * 
	 * @param and
	 *            The and to set
	 */
	public void setAnd(String and) {
		this.and = and;
	}

	/**
	 * Gets the or
	 * 
	 * @return Returns a String
	 */
	public String getOr() {
		return or;
	}

	/**
	 * Sets the or
	 * 
	 * @param or
	 *            The or to set
	 */
	public void setOr(String or) {
		this.or = or;
	}

	/**
	 * Gets the not
	 * 
	 * @return Returns a String
	 */
	public String getNot() {
		return not;
	}

	/**
	 * Sets the not
	 * 
	 * @param not
	 *            The not to set
	 */
	public void setNot(String not) {
		this.not = not;
	}

	/**
	 * Gets the brackets
	 * 
	 * @return Returns a String
	 */
	public String getBrackets() {
		return brackets;
	}

	/**
	 * Sets the brackets
	 * 
	 * @param brackets
	 *            The brackets to set
	 */
	public void setBrackets(String brackets) {
		this.brackets = brackets;
	}

	/**
	 * Gets the search
	 * 
	 * @return Returns a String
	 */
	public String getSearch() {
		return search;
	}

	/**
	 * Sets the search
	 * 
	 * @param search
	 *            The search to set
	 */
	public void setSearch(String search) {
		this.search = search;
	}

	/**
	 * Gets the cancel
	 * 
	 * @return Returns a String
	 */
	public String getCancel() {
		return cancel;
	}

	/**
	 * Sets the cancel
	 * 
	 * @param cancel
	 *            The cancel to set
	 */
	public void setCancel(String cancel) {
		this.cancel = cancel;
	}

} // End class
