package elms.control.beans;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import elms.app.inspection.Inspector;

public class ViewAllInspectionForm extends ActionForm {

	protected Inspector[] inspector;
	protected String inspectionDate;
	protected String reAssign;
	protected String routingId;
	protected String changed;
	protected String manager;

	public ViewAllInspectionForm() {
		this.inspectionDate = "";
		this.reAssign = "";
		this.routingId = "";
		this.changed = "no";
		manager = "N";
		this.inspector = new Inspector[0];
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		// inspector = null;

	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();
		/*
		 * Inspector insp; InspectorRecord[] recArray; boolean isnew = false; Set set = new HashSet(); for(int i=0; i < inspector.length; i++) { insp = inspector[i]; recArray = insp.getInspectorRecord(); set.clear(); for(int j=0; j < recArray.length; j++) { set.add(recArray[j].getRoute()); if(recArray[j].getRoute() == null || recArray[j].getRoute().equals("")) { isnew = true; } } if(isnew == false && set.size() != recArray.length) { errors.add("inspector", new ActionError("Route Sequence Numbers should not be same")); }
		 * 
		 * }
		 */
		// delcare validations for the form
		/*
		 * if ((firstName == null) || (firstName.length() < 1)) errors.add("firstName",new ActionError("error.firstname.required"));
		 * 
		 * if ((lastName == null) || (lastName.length() < 1)) errors.add("lastName",new ActionError("error.lastname.required"));
		 */
		return errors;
	}

	/**
	 * Gets the inspector
	 * 
	 * @return Returns a Inspector[]
	 */
	public Inspector[] getInspector() {
		// System.out.println("getInpector called");
		return inspector;
	}

	/**
	 * Sets the inspector
	 * 
	 * @param inspector
	 *            The inspector to set
	 */
	public void setInspector(Inspector[] inspector) {
		// System.out.println("setInspector called. set to : " + inspector);
		// System.out.println("first inspector has so many records. "+ (inspector[0].getInspectorRecord()).length);
		this.inspector = inspector;
	}

	/**
	 * Gets the inspectionDate
	 * 
	 * @return Returns a String
	 */
	public String getInspectionDate() {
		return inspectionDate;
	}

	/**
	 * Sets the inspectionDate
	 * 
	 * @param inspectionDate
	 *            The inspectionDate to set
	 */
	public void setInspectionDate(String inspectionDate) {
		this.inspectionDate = inspectionDate;
	}

	/**
	 * Gets the reAssign
	 * 
	 * @return Returns a String
	 */
	public String getReAssign() {
		return reAssign;
	}

	/**
	 * Sets the reAssign
	 * 
	 * @param reAssign
	 *            The reAssign to set
	 */
	public void setReAssign(String reAssign) {
		this.reAssign = reAssign;
	}

	/**
	 * Gets the routingId
	 * 
	 * @return Returns a String
	 */
	public String getRoutingId() {
		return routingId;
	}

	/**
	 * Sets the routingId
	 * 
	 * @param reAssign
	 *            The reAssign to set
	 */
	public void setRoutingId(String routingId) {
		this.routingId = routingId;
	}

	/**
	 * Gets the changed
	 * 
	 * @return Returns a String
	 */
	public String getChanged() {
		return changed;
	}

	/**
	 * Sets the changed
	 * 
	 * @param changed
	 *            The changed to set
	 */
	public void setChanged(String changed) {
		this.changed = changed;
	}

	/**
	 * @return
	 */
	public String getManager() {
		return manager;
	}

	/**
	 * @param string
	 */
	public void setManager(String string) {
		manager = string;
	}

}