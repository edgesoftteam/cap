package elms.control.beans;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;

public class FeeInitializeForm extends ActionForm {

	static Logger logger = Logger.getLogger(FeesForm.class.getName());

	protected String feeDate;
	protected String strMessage;

	public FeeInitializeForm() {
	}

	/**
	 * Gets the strMessage
	 * 
	 * @return Returns a String
	 */
	public String getStrMessage() {
		return strMessage;
	}

	/**
	 * Sets the strMessage
	 * 
	 * @param strMessage
	 *            The strMessage to set
	 */
	public void setStrMessage(String strMessage) {
		this.strMessage = strMessage;
	}

	/**
	 * Gets the feeDate
	 * 
	 * @return Returns a String
	 */
	public String getFeeDate() {
		return feeDate;
	}

	/**
	 * Sets the feeDate
	 * 
	 * @param feeDate
	 *            The feeDate to set
	 */
	public void setFeeDate(String feeDate) {
		this.feeDate = feeDate;
	}

}