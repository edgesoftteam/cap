/*
 * Created on Dec 27, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package elms.control.beans;

import java.util.List;

import org.apache.struts.action.ActionForm;

public class ConditionsInitializeForm extends ActionForm {

	private String conditionDate;
	private String strMessage;
	private boolean modules;
	private String moduleIdString;
	private List moduleList;

	/**
	 * @return Returns the moduleList.
	 */
	public List getModuleList() {
		return moduleList;
	}

	/**
	 * @param moduleList
	 *            The moduleList to set.
	 */
	public void setModuleList(List moduleList) {
		this.moduleList = moduleList;
	}

	/**
	 * @return Returns the conditionInitialize.
	 */

	public ConditionsInitializeForm() {

	}

	/**
	 * @return Returns the conditionDate.
	 */
	public String getconditionDate() {
		return conditionDate;
	}

	/**
	 * @param conditionDate
	 *            The conditionDate to set.
	 */
	public void setconditionDate(String conditionDate) {
		this.conditionDate = conditionDate;
	}

	/**
	 * @return Returns the strMessage.
	 */
	public String getStrMessage() {
		return strMessage;
	}

	/**
	 * @param strMessage
	 *            The strMessage to set.
	 */
	public void setStrMessage(String strMessage) {
		this.strMessage = strMessage;
	}

	/**
	 * @return Returns the modules.
	 */
	public boolean isModules() {
		return modules;
	}

	/**
	 * @param modules
	 *            The modules to set.
	 */
	public void setModules(boolean modules) {
		this.modules = modules;
	}

	/**
	 * @return Returns the moduleIdString.
	 */
	public String getModuleIdString() {
		return moduleIdString;
	}

	/**
	 * @param moduleIdString
	 *            The moduleIdString to set.
	 */
	public void setModuleIdString(String moduleIdString) {
		this.moduleIdString = moduleIdString;
	}

}
