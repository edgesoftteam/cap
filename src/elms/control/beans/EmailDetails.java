package elms.control.beans;

import java.util.Map;

public class EmailDetails {	
	
	private String emailId;
	private String permitNo;
	private String permitType;
	private String amount;
	private String bodyText;
	private String trans_id;
	private String userName;
	private String password;
	private String status;
	private String type;
	private String date;
	private String time;
	private int securityCode;
	private Map<String, String> lkupSystemDataMap;
	EmailTemplateAdminForm emailTemplateAdminForm;
	
	private String address;
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EmailDetails [emailId=" + emailId + ""
				+ ", securityCode=" + securityCode + "]";
	}
	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}
	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	/**
	 * @return the lkupSystemDataMap
	 */
	public Map<String, String> getLkupSystemDataMap() {
		return lkupSystemDataMap;
	}

	/**
	 * @param lkupSystemDataMap the lkupSystemDataMap to set
	 */
	public void setLkupSystemDataMap(Map<String, String> lkupSystemDataMap) {
		this.lkupSystemDataMap = lkupSystemDataMap;
	}

	/**
	 * @return the emailTemplateAdminForm
	 */
	public EmailTemplateAdminForm getEmailTemplateAdminForm() {
		return emailTemplateAdminForm;
	}

	/**
	 * @param emailTemplateAdminForm the emailTemplateAdminForm to set
	 */
	public void setEmailTemplateAdminForm(EmailTemplateAdminForm emailTemplateAdminForm) {
		this.emailTemplateAdminForm = emailTemplateAdminForm;
	}

	public int getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(int securityCode) {
		this.securityCode = securityCode;
	}
	public String getPermitNo() {
		return permitNo;
	}
	public void setPermitNo(String permitNo) {
		this.permitNo = permitNo;
	}
	public String getPermitType() {
		return permitType;
	}
	public void setPermitType(String permitType) {
		this.permitType = permitType;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getTrans_id() {
		return trans_id;
	}
	public void setTrans_id(String trans_id) {
		this.trans_id = trans_id;
	}
	public String getBodyText() {
		return bodyText;
	}
	public void setBodyText(String bodyText) {
		this.bodyText = bodyText;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}	
}