package elms.control.beans.planning;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import elms.app.planning.EnvReviewEdit;

public class EnvReviewForm extends ActionForm {

	static Logger logger = Logger.getLogger(ResolutionForm.class.getName());

	private String action;
	private int levelId;
	private String level;
	private String editable;
	private String address;

	private String submittedDate;
	private String determinationDate;
	private String requiredDeterminationDate;
	private String userId;
	private String enableCheckBox = "";

	private String pisActualDate;
	private String adcActualDate;
	private String pndActualDate;
	private String publicNoticeActualDate;
	private EnvReviewSummary envReviewSummary;
	private EnvReviewSummary decEarliestActualDate;
	private EnvReviewSummary decLastActualDate;

	private EnvReviewEdit[] envReviewList;

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		if (this.envReviewList != null)
			for (int i = 0; i < envReviewList.length; i++) {
				this.envReviewList[i].setUpdate("");
				this.envReviewList[i].setDeleteFlag("");
			}
	}

	public EnvReviewForm() {
		super();
		this.userId = "0";
	}

	/**
	 * Returns the envReviewList.
	 * 
	 * @return StatusEdit[]
	 */
	public EnvReviewEdit[] getEnvReviewList() {
		return envReviewList;
	}

	/**
	 * Sets the envReviewList.
	 * 
	 * @param envReviewList
	 *            The envReviewList to set
	 */
	public void setEnvReviewList(EnvReviewEdit[] envReviewList) {
		this.envReviewList = envReviewList;
	}

	public void setEnvReviewList(List envReviews) {
		if (envReviews == null)
			envReviews = new ArrayList();
		EnvReviewEdit[] envReviewEditArray = new EnvReviewEdit[envReviews.size()];
		Iterator iter = envReviews.iterator();
		int index = 0;
		while (iter.hasNext()) {
			envReviewEditArray[index] = (EnvReviewEdit) iter.next();
			envReviewEditArray[index].setIndex(index + 1);
			index++;
		}
		this.envReviewList = envReviewEditArray;
	}

	/**
	 * Returns the determinationDate.
	 * 
	 * @return String
	 */
	public String getDeterminationDate() {
		return determinationDate;
	}

	/**
	 * Returns the requiredDeterminationDate.
	 * 
	 * @return String
	 */
	public String getRequiredDeterminationDate() {
		return requiredDeterminationDate;
	}

	/**
	 * Returns the submittedDate.
	 * 
	 * @return String
	 */
	public String getSubmittedDate() {
		return submittedDate;
	}

	/**
	 * Sets the determinationDate.
	 * 
	 * @param determinationDate
	 *            The determinationDate to set
	 */
	public void setDeterminationDate(String determinationDate) {
		this.determinationDate = determinationDate;
	}

	/**
	 * Sets the requiredDeterminationDate.
	 * 
	 * @param requiredDeterminationDate
	 *            The requiredDeterminationDate to set
	 */
	public void setRequiredDeterminationDate(String requiredDeterminationDate) {
		this.requiredDeterminationDate = requiredDeterminationDate;
	}

	/**
	 * Sets the submittedDate.
	 * 
	 * @param submittedDate
	 *            The submittedDate to set
	 */
	public void setSubmittedDate(String submittedDate) {
		this.submittedDate = submittedDate;
	}

	/**
	 * @return
	 */
	public String getAction() {
		return action;
	}

	/**
	 * @param string
	 */
	public void setAction(String string) {
		action = string;
	}

	/**
	 * @return
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @return
	 */
	public String getEditable() {
		return editable;
	}

	/**
	 * @return
	 */
	public int getLevelId() {
		return levelId;
	}

	/**
	 * @param string
	 */
	public void setAddress(String string) {
		address = string;
	}

	/**
	 * @param string
	 */
	public void setEditable(String string) {
		editable = string;
	}

	/**
	 * @param i
	 */
	public void setLevelId(int i) {
		levelId = i;
	}

	/**
	 * @return
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param string
	 */
	public void setUserId(String string) {
		userId = string;
	}

	/**
	 * @return
	 */
	public EnvReviewSummary getEnvReviewSummary() {
		return envReviewSummary;
	}

	/**
	 * @param summary
	 */
	public void setEnvReviewSummary(EnvReviewSummary summary) {
		envReviewSummary = summary;
	}

	/**
	 * @return
	 */
	public String getLevel() {
		return level;
	}

	/**
	 * @param string
	 */
	public void setLevel(String string) {
		level = string;
	}

	/**
	 * @return
	 */
	public String getEnableCheckBox() {
		return enableCheckBox;
	}

	/**
	 * @param string
	 */
	public void setEnableCheckBox(String string) {
		enableCheckBox = string;
	}

} // End class