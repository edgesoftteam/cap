package elms.control.beans.planning;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import elms.app.planning.StatusEdit;

public class StatusForm extends ActionForm {

	static Logger logger = Logger.getLogger(StatusForm.class.getName());

	private String action;
	private int levelId;
	private String levelType;
	private String address;
	private String editable;
	private String userId;
	private String lastStatusId;

	private StatusEdit[] statusList;
	private StatusEdit[] displayList;

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		if (this.statusList != null)
			for (int i = 0; i < statusList.length; i++) {
				this.statusList[i].setUpdate("");
				this.statusList[i].setUpdateActivities("");
				this.statusList[i].setUpdateSubProject("");
				this.statusList[i].setDeleteFlag("");
			}
	}

	/**
	 * Returns the statusList.
	 * 
	 * @return StatusEdit[]
	 */
	public StatusEdit[] getStatusList() {
		return statusList;
	}

	/**
	 * Sets the statusList.
	 * 
	 * @param statusList
	 *            The statusList to set
	 */
	public void setStatusList(StatusEdit[] statusList) {
		this.statusList = statusList;
	}

	public void setStatusList(List statuses) {
		if (statuses == null)
			statuses = new ArrayList();
		StatusEdit[] statusEditArray = new StatusEdit[statuses.size()];
		Iterator iter = statuses.iterator();
		int index = 0;
		while (iter.hasNext()) {
			statusEditArray[index] = (StatusEdit) iter.next();
			index++;
		}
		this.statusList = statusEditArray;
	}

	/**
	 * @return
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @return
	 */
	public int getLevelId() {
		return levelId;
	}

	/**
	 * @return
	 */
	public String getLevelType() {
		return levelType;
	}

	/**
	 * @param string
	 */
	public void setAddress(String string) {
		address = string;
	}

	/**
	 * @param i
	 */
	public void setLevelId(int i) {
		levelId = i;
	}

	/**
	 * @param i
	 */
	public void setLevelType(String string) {
		levelType = string;
	}

	/**
	 * @return
	 */
	public String getEditable() {
		return editable;
	}

	/**
	 * @param string
	 */
	public void setEditable(String string) {
		editable = string;
	}

	/**
	 * @return
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param string
	 */
	public void setUserId(String string) {
		userId = string;
	}

	/**
	 * @return
	 */
	public StatusEdit[] getDisplayList() {
		return displayList;
	}

	/**
	 * @param edits
	 */
	public void setDisplayList(StatusEdit[] edits) {
		displayList = edits;
	}

	public void setDisplayList(List statuses) {
		if (statuses == null)
			statuses = new ArrayList();
		StatusEdit[] statusEditArray = new StatusEdit[statuses.size()];
		Iterator iter = statuses.iterator();
		int index = 0;
		while (iter.hasNext()) {
			statusEditArray[index] = (StatusEdit) iter.next();
			index++;
		}
		this.displayList = statusEditArray;
	}

	/**
	 * @return
	 */
	public String getLastStatusId() {
		return lastStatusId;
	}

	/**
	 * @param string
	 */
	public void setLastStatusId(String string) {
		lastStatusId = string;
	}

	/**
	 * @return
	 */
	public String getAction() {
		return action;
	}

	/**
	 * @param string
	 */
	public void setAction(String string) {
		action = string;
	}

} // End class