package elms.control.beans.planning;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import elms.app.planning.ResolutionEdit;

public class ResolutionForm extends ActionForm {

	static Logger logger = Logger.getLogger(ResolutionForm.class.getName());

	private int levelId;
	private String levelType;
	private String address;
	private int userId;
	private String enableCheckBox = "";

	private ResolutionEdit[] resolutionList;

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		if (this.resolutionList != null)
			for (int i = 0; i < resolutionList.length; i++) {
				this.resolutionList[i].setDelete("");
			}
	}

	/**
	 * Returns the address.
	 * 
	 * @return String
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Returns the resolutionList.
	 * 
	 * @return ResolutionEdit[]
	 */
	public ResolutionEdit[] getResolutionList() {
		return resolutionList;
	}

	/**
	 * Returns the userId.
	 * 
	 * @return int
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * Sets the address.
	 * 
	 * @param address
	 *            The address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Sets the resolutionList.
	 */
	public void setResolutionList(ResolutionEdit[] resolutionList) {
		this.resolutionList = resolutionList;
	}

	public void setResolutionList(List resolutions) {
		try {
			if (resolutions == null)
				resolutions = new ArrayList();
			ResolutionEdit[] resolutionEditArray = new ResolutionEdit[resolutions.size()];
			Iterator iter = resolutions.iterator();
			int index = 0;
			while (iter.hasNext()) {
				resolutionEditArray[index] = (ResolutionEdit) iter.next();
				index++;
			}
			this.resolutionList = resolutionEditArray;
		} catch (RuntimeException e) {
			logger.debug(e.getLocalizedMessage());
		}
	}

	/**
	 * Sets the userId.
	 * 
	 * @param userId
	 *            The userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * Returns the levelId.
	 * 
	 * @return int
	 */
	public int getLevelId() {
		return levelId;
	}

	/**
	 * Returns the levelType.
	 * 
	 * @return String
	 */
	public String getLevelType() {
		return levelType;
	}

	/**
	 * Sets the levelId.
	 * 
	 * @param levelId
	 *            The levelId to set
	 */
	public void setLevelId(int levelId) {
		this.levelId = levelId;
	}

	/**
	 * Sets the levelType.
	 * 
	 * @param levelType
	 *            The levelType to set
	 */
	public void setLevelType(String levelType) {
		this.levelType = levelType;
	}

	/**
	 * @return
	 */
	public String getEnableCheckBox() {
		return enableCheckBox;
	}

	/**
	 * @param string
	 */
	public void setEnableCheckBox(String string) {
		enableCheckBox = string;
	}

} // End class