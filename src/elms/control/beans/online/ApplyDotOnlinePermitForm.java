package elms.control.beans.online;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts.action.ActionForm;

import elms.app.admin.FeeEdit;

/**
 * @author Sunil
 */
public class ApplyDotOnlinePermitForm extends ActionForm {

	private int activityId;
	private String activityNbr;
	private String activityStatus;
	private String type;
	private String description;
	private String startDate;
	private String expiryDate;
	private String todaysDate;
	private String sixMonthsAfterDate;
	private String feePerUnit;
	private String totalFees;
	private FeeEdit feeEdit = null;
	private String nbrMonths;
	private String paymentAmount;
	private List activityList = new ArrayList();
	private String amount;
	private String[] selectedTypes;
	private String quantity;

	private List tempDotOnlineList = new ArrayList();
	private String tempDotOnlineIds;
	private String feeAmount;
	private String termsCondition;
	private int dotOnlineGrpId;

	// Virtual Merchant Payment Procces for Online Hidden Variable declaration
	protected String ssl_merchant_id;
	protected String ssl_pin;
	protected String ssl_amount;
	protected String ssl_card_number;
	protected String creditCardType;
	protected String ssl_exp_date;

	protected String ssl_show_form;
	protected String ssl_transaction_type;
	protected String ssl_cvv2_indicator;
	protected String ssl_cvv2cvc2;

	// result
	protected String ssl_result_format;
	protected String ssl_receipt_decl_method;
	protected String ssl_receipt_decl_post_url;
	protected String ssl_receipt_apprvl_method;
	protected String ssl_receipt_apprvl_get_url;
	protected String ssl_receipt_link_text;

	// we recieve
	protected String ssl_result;
	protected String ssl_result_message;
	protected String ssl_txn_id;
	protected String ssl_approval_code;

	protected String ssl_user_id;
	protected String ssl_test_mode;
	protected String ssl_invoice_number;

	protected String ssl_avs_address;
	protected String ssl_avs_zip;

	protected String feeId;

	protected String countDOTPPP;
	protected String countDOTON;

	private int count;
	private String actType;

	/**
	 * 
	 */
	public ApplyDotOnlinePermitForm() {

		// TODO Auto-generated constructor stub
	}

	public ApplyDotOnlinePermitForm(String type, String description) {
		this.type = type;
		this.description = description;
	}

	public ApplyDotOnlinePermitForm(int count, String actType) {
		this.count = count;
		this.actType = actType;
	}

	/**
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param string
	 */
	public void setDescription(String string) {
		description = string;
	}

	/**
	 * @param string
	 */
	public void setType(String string) {
		type = string;
	}

	/**
	 * @return
	 */
	public String getActType() {
		return actType;
	}

	/**
	 * @return
	 */
	public int getCount() {
		return count;
	}

	/**
	 * @param string
	 */
	public void setActType(String string) {
		actType = string;
	}

	/**
	 * @param i
	 */
	public void setCount(int i) {
		count = i;
	}

	/**
	 * @return
	 */
	public String getExpiryDate() {
		return expiryDate;
	}

	/**
	 * @return
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param string
	 */
	public void setExpiryDate(String string) {
		expiryDate = string;
	}

	/**
	 * @param string
	 */
	public void setStartDate(String string) {
		startDate = string;
	}

	/**
	 * @return
	 */
	public String getSixMonthsAfterDate() {
		return sixMonthsAfterDate;
	}

	/**
	 * @return
	 */
	public String getTodaysDate() {
		return todaysDate;
	}

	/**
	 * @param string
	 */
	public void setSixMonthsAfterDate(String string) {
		sixMonthsAfterDate = string;
	}

	/**
	 * @param string
	 */
	public void setTodaysDate(String string) {
		todaysDate = string;
	}

	/**
	 * @return
	 */
	public String getFeePerUnit() {
		return feePerUnit;
	}

	/**
	 * @param string
	 */
	public void setFeePerUnit(String string) {
		feePerUnit = string;
	}

	/**
	 * @return
	 */
	public String getTotalFees() {
		return totalFees;
	}

	/**
	 * @param string
	 */
	public void setTotalFees(String string) {
		totalFees = string;
	}

	/**
	 * @return
	 */
	public String getNbrMonths() {
		return nbrMonths;
	}

	/**
	 * @param string
	 */
	public void setNbrMonths(String string) {
		nbrMonths = string;
	}

	/**
	 * @return
	 */
	public String getPaymentAmount() {
		return paymentAmount;
	}

	/**
	 * @param string
	 */
	public void setPaymentAmount(String string) {
		paymentAmount = string;
	}

	/**
	 * @return
	 */
	public List getActivityList() {
		return activityList;
	}

	/**
	 * @param list
	 */
	public void setActivityList(List list) {
		activityList = list;
	}

	/**
	 * @return
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param string
	 */
	public void setAmount(String string) {
		amount = string;
	}

	/**
	 * @return
	 */
	public FeeEdit getFeeEdit() {
		return feeEdit;
	}

	/**
	 * @param edit
	 */
	public void setFeeEdit(FeeEdit edit) {
		feeEdit = edit;
	}

	/**
	 * @return
	 */
	public String[] getSelectedTypes() {
		return selectedTypes;
	}

	/**
	 * @param strings
	 */
	public void setSelectedTypes(String[] strings) {
		selectedTypes = strings;
	}

	/**
	 * @return
	 */
	public String getQuantity() {
		return quantity;
	}

	/**
	 * @param string
	 */
	public void setQuantity(String string) {
		quantity = string;
	}

	/**
	 * @return
	 */
	public List getTempDotOnlineList() {
		return tempDotOnlineList;
	}

	/**
	 * @param list
	 */
	public void setTempDotOnlineList(List list) {
		tempDotOnlineList = list;
	}

	/**
	 * @return
	 */
	public String getTempDotOnlineIds() {
		return tempDotOnlineIds;
	}

	/**
	 * @param string
	 */
	public void setTempDotOnlineIds(String string) {
		tempDotOnlineIds = string;
	}

	/**
	 * @return
	 */
	public String getFeeAmount() {
		return feeAmount;
	}

	/**
	 * @param string
	 */
	public void setFeeAmount(String string) {
		feeAmount = string;
	}

	/**
	 * @return
	 */
	public String getTermsCondition() {
		return termsCondition;
	}

	/**
	 * @param string
	 */
	public void setTermsCondition(String string) {
		termsCondition = string;
	}

	/**
	 * @return
	 */
	public int getDotOnlineGrpId() {
		return dotOnlineGrpId;
	}

	/**
	 * @param i
	 */
	public void setDotOnlineGrpId(int i) {
		dotOnlineGrpId = i;
	}

	/**
	 * @return
	 */
	public String getCreditCardType() {
		return creditCardType;
	}

	/**
	 * @return
	 */
	public String getSsl_amount() {
		return ssl_amount;
	}

	/**
	 * @return
	 */
	public String getSsl_approval_code() {
		return ssl_approval_code;
	}

	/**
	 * @return
	 */
	public String getSsl_avs_address() {
		return ssl_avs_address;
	}

	/**
	 * @return
	 */
	public String getSsl_avs_zip() {
		return ssl_avs_zip;
	}

	/**
	 * @return
	 */
	public String getSsl_card_number() {
		return ssl_card_number;
	}

	/**
	 * @return
	 */
	public String getSsl_cvv2_indicator() {
		return ssl_cvv2_indicator;
	}

	/**
	 * @return
	 */
	public String getSsl_cvv2cvc2() {
		return ssl_cvv2cvc2;
	}

	/**
	 * @return
	 */
	public String getSsl_exp_date() {
		return ssl_exp_date;
	}

	/**
	 * @return
	 */
	public String getSsl_invoice_number() {
		return ssl_invoice_number;
	}

	/**
	 * @return
	 */
	public String getSsl_merchant_id() {
		return ssl_merchant_id;
	}

	/**
	 * @return
	 */
	public String getSsl_pin() {
		return ssl_pin;
	}

	/**
	 * @return
	 */
	public String getSsl_receipt_apprvl_get_url() {
		return ssl_receipt_apprvl_get_url;
	}

	/**
	 * @return
	 */
	public String getSsl_receipt_apprvl_method() {
		return ssl_receipt_apprvl_method;
	}

	/**
	 * @return
	 */
	public String getSsl_receipt_decl_method() {
		return ssl_receipt_decl_method;
	}

	/**
	 * @return
	 */
	public String getSsl_receipt_decl_post_url() {
		return ssl_receipt_decl_post_url;
	}

	/**
	 * @return
	 */
	public String getSsl_receipt_link_text() {
		return ssl_receipt_link_text;
	}

	/**
	 * @return
	 */
	public String getSsl_result() {
		return ssl_result;
	}

	/**
	 * @return
	 */
	public String getSsl_result_format() {
		return ssl_result_format;
	}

	/**
	 * @return
	 */
	public String getSsl_result_message() {
		return ssl_result_message;
	}

	/**
	 * @return
	 */
	public String getSsl_show_form() {
		return ssl_show_form;
	}

	/**
	 * @return
	 */
	public String getSsl_test_mode() {
		return ssl_test_mode;
	}

	/**
	 * @return
	 */
	public String getSsl_transaction_type() {
		return ssl_transaction_type;
	}

	/**
	 * @return
	 */
	public String getSsl_txn_id() {
		return ssl_txn_id;
	}

	/**
	 * @return
	 */
	public String getSsl_user_id() {
		return ssl_user_id;
	}

	/**
	 * @param string
	 */
	public void setCreditCardType(String string) {
		creditCardType = string;
	}

	/**
	 * @param string
	 */
	public void setSsl_amount(String string) {
		ssl_amount = string;
	}

	/**
	 * @param string
	 */
	public void setSsl_approval_code(String string) {
		ssl_approval_code = string;
	}

	/**
	 * @param string
	 */
	public void setSsl_avs_address(String string) {
		ssl_avs_address = string;
	}

	/**
	 * @param string
	 */
	public void setSsl_avs_zip(String string) {
		ssl_avs_zip = string;
	}

	/**
	 * @param string
	 */
	public void setSsl_card_number(String string) {
		ssl_card_number = string;
	}

	/**
	 * @param string
	 */
	public void setSsl_cvv2_indicator(String string) {
		ssl_cvv2_indicator = string;
	}

	/**
	 * @param string
	 */
	public void setSsl_cvv2cvc2(String string) {
		ssl_cvv2cvc2 = string;
	}

	/**
	 * @param string
	 */
	public void setSsl_exp_date(String string) {
		ssl_exp_date = string;
	}

	/**
	 * @param string
	 */
	public void setSsl_invoice_number(String string) {
		ssl_invoice_number = string;
	}

	/**
	 * @param string
	 */
	public void setSsl_merchant_id(String string) {
		ssl_merchant_id = string;
	}

	/**
	 * @param string
	 */
	public void setSsl_pin(String string) {
		ssl_pin = string;
	}

	/**
	 * @param string
	 */
	public void setSsl_receipt_apprvl_get_url(String string) {
		ssl_receipt_apprvl_get_url = string;
	}

	/**
	 * @param string
	 */
	public void setSsl_receipt_apprvl_method(String string) {
		ssl_receipt_apprvl_method = string;
	}

	/**
	 * @param string
	 */
	public void setSsl_receipt_decl_method(String string) {
		ssl_receipt_decl_method = string;
	}

	/**
	 * @param string
	 */
	public void setSsl_receipt_decl_post_url(String string) {
		ssl_receipt_decl_post_url = string;
	}

	/**
	 * @param string
	 */
	public void setSsl_receipt_link_text(String string) {
		ssl_receipt_link_text = string;
	}

	/**
	 * @param string
	 */
	public void setSsl_result(String string) {
		ssl_result = string;
	}

	/**
	 * @param string
	 */
	public void setSsl_result_format(String string) {
		ssl_result_format = string;
	}

	/**
	 * @param string
	 */
	public void setSsl_result_message(String string) {
		ssl_result_message = string;
	}

	/**
	 * @param string
	 */
	public void setSsl_show_form(String string) {
		ssl_show_form = string;
	}

	/**
	 * @param string
	 */
	public void setSsl_test_mode(String string) {
		ssl_test_mode = string;
	}

	/**
	 * @param string
	 */
	public void setSsl_transaction_type(String string) {
		ssl_transaction_type = string;
	}

	/**
	 * @param string
	 */
	public void setSsl_txn_id(String string) {
		ssl_txn_id = string;
	}

	/**
	 * @param string
	 */
	public void setSsl_user_id(String string) {
		ssl_user_id = string;
	}

	/**
	 * @return
	 */
	public String getFeeId() {
		return feeId;
	}

	/**
	 * @param string
	 */
	public void setFeeId(String string) {
		feeId = string;
	}

	/**
	 * @return
	 */
	public int getActivityId() {
		return activityId;
	}

	/**
	 * @return
	 */
	public String getActivityNbr() {
		return activityNbr;
	}

	/**
	 * @return
	 */
	public String getActivityStatus() {
		return activityStatus;
	}

	/**
	 * @param i
	 */
	public void setActivityId(int i) {
		activityId = i;
	}

	/**
	 * @param string
	 */
	public void setActivityNbr(String string) {
		activityNbr = string;
	}

	/**
	 * @param string
	 */
	public void setActivityStatus(String string) {
		activityStatus = string;
	}

	/**
	 * @return
	 */
	public String getCountDOTON() {
		return countDOTON;
	}

	/**
	 * @return
	 */
	public String getCountDOTPPP() {
		return countDOTPPP;
	}

	/**
	 * @param string
	 */
	public void setCountDOTON(String string) {
		countDOTON = string;
	}

	/**
	 * @param string
	 */
	public void setCountDOTPPP(String string) {
		countDOTPPP = string;
	}

}
