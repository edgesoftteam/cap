package elms.control.beans.online;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author Anand Belaguly
 */
public class CutoffTimesForm extends ActionForm {

	protected String scheduleMondayStart;
	protected String scheduleMondayEnd;
	protected String scheduleTuesdayStart;
	protected String scheduleTuesdayEnd;
	protected String scheduleWednesdayStart;
	protected String scheduleWednesdayEnd;
	protected String scheduleThursdayStart;
	protected String scheduleThursdayEnd;
	protected String scheduleFridayStart;
	protected String scheduleFridayEnd;

	protected String cutoffMondaySchedule;
	protected String cutoffMondayCancel;
	protected String cutoffTuesdaySchedule;
	protected String cutoffTuesdayCancel;
	protected String cutoffWednesdaySchedule;
	protected String cutoffWednesdayCancel;
	protected String cutoffThursdaySchedule;
	protected String cutoffThursdayCancel;
	protected String cutoffFridaySchedule;
	protected String cutoffFridayCancel;

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		return errors;
	}

	/**
	 * @return
	 */
	public String getCutoffFridayCancel() {
		return cutoffFridayCancel;
	}

	/**
	 * @return
	 */
	public String getCutoffFridaySchedule() {
		return cutoffFridaySchedule;
	}

	/**
	 * @return
	 */
	public String getCutoffMondayCancel() {
		return cutoffMondayCancel;
	}

	/**
	 * @return
	 */
	public String getCutoffMondaySchedule() {
		return cutoffMondaySchedule;
	}

	/**
	 * @return
	 */
	public String getCutoffThursdayCancel() {
		return cutoffThursdayCancel;
	}

	/**
	 * @return
	 */
	public String getCutoffThursdaySchedule() {
		return cutoffThursdaySchedule;
	}

	/**
	 * @return
	 */
	public String getCutoffTuesdayCancel() {
		return cutoffTuesdayCancel;
	}

	/**
	 * @return
	 */
	public String getCutoffTuesdaySchedule() {
		return cutoffTuesdaySchedule;
	}

	/**
	 * @return
	 */
	public String getCutoffWednesdayCancel() {
		return cutoffWednesdayCancel;
	}

	/**
	 * @return
	 */
	public String getCutoffWednesdaySchedule() {
		return cutoffWednesdaySchedule;
	}

	/**
	 * @return
	 */
	public String getScheduleFridayEnd() {
		return scheduleFridayEnd;
	}

	/**
	 * @return
	 */
	public String getScheduleFridayStart() {
		return scheduleFridayStart;
	}

	/**
	 * @return
	 */
	public String getScheduleMondayEnd() {
		return scheduleMondayEnd;
	}

	/**
	 * @return
	 */
	public String getScheduleMondayStart() {
		return scheduleMondayStart;
	}

	/**
	 * @return
	 */
	public String getScheduleThursdayEnd() {
		return scheduleThursdayEnd;
	}

	/**
	 * @return
	 */
	public String getScheduleThursdayStart() {
		return scheduleThursdayStart;
	}

	/**
	 * @return
	 */
	public String getScheduleTuesdayEnd() {
		return scheduleTuesdayEnd;
	}

	/**
	 * @return
	 */
	public String getScheduleTuesdayStart() {
		return scheduleTuesdayStart;
	}

	/**
	 * @return
	 */
	public String getScheduleWednesdayEnd() {
		return scheduleWednesdayEnd;
	}

	/**
	 * @return
	 */
	public String getScheduleWednesdayStart() {
		return scheduleWednesdayStart;
	}

	/**
	 * @param string
	 */
	public void setCutoffFridayCancel(String string) {
		cutoffFridayCancel = string;
	}

	/**
	 * @param string
	 */
	public void setCutoffFridaySchedule(String string) {
		cutoffFridaySchedule = string;
	}

	/**
	 * @param string
	 */
	public void setCutoffMondayCancel(String string) {
		cutoffMondayCancel = string;
	}

	/**
	 * @param string
	 */
	public void setCutoffMondaySchedule(String string) {
		cutoffMondaySchedule = string;
	}

	/**
	 * @param string
	 */
	public void setCutoffThursdayCancel(String string) {
		cutoffThursdayCancel = string;
	}

	/**
	 * @param string
	 */
	public void setCutoffThursdaySchedule(String string) {
		cutoffThursdaySchedule = string;
	}

	/**
	 * @param string
	 */
	public void setCutoffTuesdayCancel(String string) {
		cutoffTuesdayCancel = string;
	}

	/**
	 * @param string
	 */
	public void setCutoffTuesdaySchedule(String string) {
		cutoffTuesdaySchedule = string;
	}

	/**
	 * @param string
	 */
	public void setCutoffWednesdayCancel(String string) {
		cutoffWednesdayCancel = string;
	}

	/**
	 * @param string
	 */
	public void setCutoffWednesdaySchedule(String string) {
		cutoffWednesdaySchedule = string;
	}

	/**
	 * @param string
	 */
	public void setScheduleFridayEnd(String string) {
		scheduleFridayEnd = string;
	}

	/**
	 * @param string
	 */
	public void setScheduleFridayStart(String string) {
		scheduleFridayStart = string;
	}

	/**
	 * @param string
	 */
	public void setScheduleMondayEnd(String string) {
		scheduleMondayEnd = string;
	}

	/**
	 * @param string
	 */
	public void setScheduleMondayStart(String string) {
		scheduleMondayStart = string;
	}

	/**
	 * @param string
	 */
	public void setScheduleThursdayEnd(String string) {
		scheduleThursdayEnd = string;
	}

	/**
	 * @param string
	 */
	public void setScheduleThursdayStart(String string) {
		scheduleThursdayStart = string;
	}

	/**
	 * @param string
	 */
	public void setScheduleTuesdayEnd(String string) {
		scheduleTuesdayEnd = string;
	}

	/**
	 * @param string
	 */
	public void setScheduleTuesdayStart(String string) {
		scheduleTuesdayStart = string;
	}

	/**
	 * @param string
	 */
	public void setScheduleWednesdayEnd(String string) {
		scheduleWednesdayEnd = string;
	}

	/**
	 * @param string
	 */
	public void setScheduleWednesdayStart(String string) {
		scheduleWednesdayStart = string;
	}

}