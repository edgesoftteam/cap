package elms.control.beans.online;

import java.util.Calendar;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

/**
 * @author admin
 *
 */
/**
 * @author admin
 *
 */
public class OnlinePermitForm extends ActionForm {
	
	private String tempId;
	private String streetNum;
	private String preDir;
	private String streetName;
	private String street;
	private String streetUnit;
	private String streetMod;
	//private String unit;
	private String lsoId;
	private String departmentType;
	
	private String activityType;
	private String activityTypeDesc;
	//private String typeOfDwelling;
	
	protected String userId;
	protected int accountNo;
	protected String firstName;
	protected String lastName;
	protected String emailAddress;
	protected String address;
	protected String phoneNbr;
	/*protected String streetNumber;
	protected String streetId;
	protected String streetFraction;
	protected String unitNumber;
	protected String streetName;
*/	protected String obc;
	protected String dot;
	protected String peopleType;
	protected String phoneExt;
	protected String workPhone;
	protected String workExt;
	protected String city;
	protected String state;
	protected String zip;
	
	protected String actDescription;
	protected String issueDate;
	protected String endDate;
	
	private String description;
	private FormFile theFile;
	private String actId;

	public void reset1() {
        this.tempId=null;
        this.preDir = null;
        this.streetName = null;
        this.streetNum = null;
        this.streetUnit = null;
        this.streetMod = null;
        this.street = null;
        this.lsoId = null;
        this.address = null;
        this.actDescription = null;
        this.issueDate = null;
        this.endDate = null;
        this.description = null;
        this.actId = null;
    }
	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	public String getStreetNum() {
		return streetNum;
	}
	public void setStreetNum(String streetNum) {
		this.streetNum = streetNum;
	}
	public String getPreDir() {
		return preDir;
	}
	public void setPreDir(String preDir) {
		this.preDir = preDir;
	}
	public String getStreetName() {
		return streetName;
	}
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	/*public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}*/
	public String getLsoId() {
		return lsoId;
	}
	public void setLsoId(String lsoId) {
		this.lsoId = lsoId;
	}

	public String getActivityType() {
		return activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	public String getDepartmentType() {
		return departmentType;
	}

	public void setDepartmentType(String departmentType) {
		this.departmentType = departmentType;
	}

	public String getStreetUnit() {
		return streetUnit;
	}

	public void setStreetUnit(String streetUnit) {
		this.streetUnit = streetUnit;
	}

	public String getStreetMod() {
		return streetMod;
	}

	public void setStreetMod(String streetMod) {
		this.streetMod = streetMod;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(int accountNo) {
		this.accountNo = accountNo;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNbr() {
		return phoneNbr;
	}

	public void setPhoneNbr(String phoneNbr) {
		this.phoneNbr = phoneNbr;
	}

	public String getObc() {
		return obc;
	}

	public void setObc(String obc) {
		this.obc = obc;
	}

	public String getDot() {
		return dot;
	}

	public void setDot(String dot) {
		this.dot = dot;
	}

	public String getPeopleType() {
		return peopleType;
	}

	public void setPeopleType(String peopleType) {
		this.peopleType = peopleType;
	}

	public String getPhoneExt() {
		return phoneExt;
	}

	public void setPhoneExt(String phoneExt) {
		this.phoneExt = phoneExt;
	}

	public String getWorkPhone() {
		return workPhone;
	}

	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}

	public String getWorkExt() {
		return workExt;
	}

	public void setWorkExt(String workExt) {
		this.workExt = workExt;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getActDescription() {
		return actDescription;
	}

	public void setActDescription(String actDescription) {
		this.actDescription = actDescription;
	}
/*
	public Calendar getStartDate() {
		return startDate;
	}

	public void setStartDate(Calendar startDate) {
		this.startDate = startDate;
	}

	public Calendar getEndDate() {
		return endDate;
	}

	public void setEndDate(Calendar endDate) {
		this.endDate = endDate;
	}*/

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public FormFile getTheFile() {
		return theFile;
	}

	public void setTheFile(FormFile theFile) {
		this.theFile = theFile;
	}

	public String getActId() {
		return actId;
	}

	public void setActId(String actId) {
		this.actId = actId;
	}

	public String getActivityTypeDesc() {
		return activityTypeDesc;
	}

	public void setActivityTypeDesc(String activityTypeDesc) {
		this.activityTypeDesc = activityTypeDesc;
	}
	public String getIssueDate() {
		return issueDate;
	}
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}
	
	
}
