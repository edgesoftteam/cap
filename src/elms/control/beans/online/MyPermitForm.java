package elms.control.beans.online;

import org.apache.struts.action.ActionForm;

/**
 * @author Sunil
 */
public class MyPermitForm extends ActionForm {

	protected String activityNo;
	protected String activityType;
	protected String activityStatus;
	protected String emailAddr;

	protected String activityDate;

	protected String address;
	protected String fullAddress;
	protected String activityId;
	protected String sprojId;
	protected String activityTypeCode;
	protected String print;
	protected String actSubType;
	protected String amount;
	// Use this for comparing amount and validate positive or negative to display add to cart button
	protected String amnt;
	protected boolean addedToCart;
	protected String egovOnline;
	protected String onlineFlag;
	protected String onlineActPeopleId;

	/**
	 * 
	 */
	public MyPermitForm() {

		// TODO Auto-generated constructor stub
	}

	/**
	 * @return
	 */
	public String getActivityDate() {
		return activityDate;
	}

	/**
	 * @return
	 */
	public String getActivityNo() {
		return activityNo;
	}

	/**
	 * @return
	 */
	public String getActivityStatus() {
		return activityStatus;
	}

	/**
	 * @return
	 */
	public String getActivityType() {
		return activityType;
	}

	/**
	 * @return
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param string
	 */
	public void setActivityDate(String string) {
		activityDate = string;
	}

	/**
	 * @param string
	 */
	public void setActivityNo(String string) {
		activityNo = string;
	}

	/**
	 * @param string
	 */
	public void setActivityStatus(String string) {
		activityStatus = string;
	}

	/**
	 * @param string
	 */
	public void setActivityType(String string) {
		activityType = string;
	}

	/**
	 * @param string
	 */
	public void setAddress(String string) {
		address = string;
	}

	/**
	 * @return
	 */
	public String getFullAddress() {
		return fullAddress;
	}

	/**
	 * @param string
	 */
	public void setFullAddress(String string) {
		fullAddress = string;
	}

	/**
	 * @return
	 */
	public String getActivityId() {
		return activityId;
	}

	/**
	 * @param string
	 */
	public void setActivityId(String string) {
		activityId = string;
	}

	/**
	 * @return
	 */
	public String getSprojId() {
		return sprojId;
	}

	/**
	 * @param string
	 */
	public void setSprojId(String string) {
		sprojId = string;
	}

	/**
	 * @return
	 */
	public String getActivityTypeCode() {
		return activityTypeCode;
	}

	/**
	 * @param string
	 */
	public void setActivityTypeCode(String string) {
		activityTypeCode = string;
	}

	/**
	 * @return
	 */
	public String getPrint() {
		return print;
	}

	/**
	 * @param string
	 */
	public void setPrint(String string) {
		print = string;
	}

	/**
	 * @return Returns the actSubType.
	 */
	public String getActSubType() {
		return actSubType;
	}

	/**
	 * @param actSubType
	 *            The actSubType to set.
	 */
	public void setActSubType(String actSubType) {
		this.actSubType = actSubType;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public boolean isAddedToCart() {
		return addedToCart;
	}

	public void setAddedToCart(boolean addedToCart) {
		this.addedToCart = addedToCart;
	}

	public String getEgovOnline() {
		return egovOnline;
	}

	public void setEgovOnline(String egovOnline) {
		this.egovOnline = egovOnline;
	}

	public String getOnlineFlag() {
		return onlineFlag;
	}

	public void setOnlineFlag(String onlineFlag) {
		this.onlineFlag = onlineFlag;
	}

	public String getOnlineActPeopleId() {
		return onlineActPeopleId;
	}

	public void setOnlineActPeopleId(String onlineActPeopleId) {
		this.onlineActPeopleId = onlineActPeopleId;
	}

	public String getEmailAddr() {
		return emailAddr;
	}

	public void setEmailAddr(String emailAddr) {
		this.emailAddr = emailAddr;
	}

	public String getAmnt() {
		return amnt;
	}

	public void setAmnt(String amnt) {
		this.amnt = amnt;
	}
}
