package elms.control.beans.online;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author Anand Belaguly
 */
public class LimitPerDayForm extends ActionForm {

	protected int limitPerDay;
	protected int currentLimit;

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		return errors;
	}

	/**
	 * @return
	 */
	public int getLimitPerDay() {
		return limitPerDay;
	}

	/**
	 * @param i
	 */
	public void setLimitPerDay(int i) {
		limitPerDay = i;
	}

	/**
	 * @return
	 */
	public int getCurrentLimit() {
		return currentLimit;
	}

	/**
	 * @param i
	 */
	public void setCurrentLimit(int i) {
		currentLimit = i;
	}

}