package elms.control.beans.online;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author Anand Belaguly
 */
public class EmailAdminForm extends ActionForm {

	protected String registrationSuccessTitle;
	protected String registrationSuccessSubject;
	protected String registrationSuccessMessage;

	protected String forgotPasswordTitle;
	protected String forgotPasswordSubject;
	protected String forgotPasswordMessage;

	protected String inspectionRequestedTitle;
	protected String inspectionRequestedSubject;
	protected String inspectionRequestedMessage;

	protected String inspectionScheduledTitle;
	protected String inspectionScheduledSubject;
	protected String inspectionScheduledMessage;

	protected String inspectionCancelledTitle;
	protected String inspectionCancelledSubject;
	protected String inspectionCancelledMessage;

	protected String permitStatusChangeTitle;
	protected String permitStatusChangeSubject;
	protected String permitStatusChangeMessage;

	protected String planCheckStatusChangeTitle;
	protected String planCheckStatusChangeSubject;
	protected String planCheckStatusChangeMessage;

	protected String tempPasswordChangeTitle;
	protected String tempPasswordChangeSubject;
	protected String tempPasswordChangeMessage;

	protected String paymentChangeTitle;
	protected String paymentChangeSubject;
	protected String paymentChangeMessage;

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		return errors;
	}

	/**
	 * @return
	 */
	public String getInspectionRequestedMessage() {
		return inspectionRequestedMessage;
	}

	/**
	 * @return
	 */
	public String getInspectionRequestedSubject() {
		return inspectionRequestedSubject;
	}

	/**
	 * @return
	 */
	public String getInspectionRequestedTitle() {
		return inspectionRequestedTitle;
	}

	/**
	 * @return
	 */
	public String getInspectionScheduledMessage() {
		return inspectionScheduledMessage;
	}

	/**
	 * @return
	 */
	public String getInspectionScheduledSubject() {
		return inspectionScheduledSubject;
	}

	/**
	 * @return
	 */
	public String getInspectionScheduledTitle() {
		return inspectionScheduledTitle;
	}

	/**
	 * @return
	 */
	public String getPermitStatusChangeMessage() {
		return permitStatusChangeMessage;
	}

	/**
	 * @return
	 */
	public String getPermitStatusChangeSubject() {
		return permitStatusChangeSubject;
	}

	/**
	 * @return
	 */
	public String getPermitStatusChangeTitle() {
		return permitStatusChangeTitle;
	}

	/**
	 * @return
	 */
	public String getPlanCheckStatusChangeMessage() {
		return planCheckStatusChangeMessage;
	}

	/**
	 * @return
	 */
	public String getPlanCheckStatusChangeSubject() {
		return planCheckStatusChangeSubject;
	}

	/**
	 * @return
	 */
	public String getPlanCheckStatusChangeTitle() {
		return planCheckStatusChangeTitle;
	}

	/**
	 * @return
	 */
	public String getRegistrationSuccessMessage() {
		return registrationSuccessMessage;
	}

	/**
	 * @return
	 */
	public String getRegistrationSuccessSubject() {
		return registrationSuccessSubject;
	}

	/**
	 * @return
	 */
	public String getRegistrationSuccessTitle() {
		return registrationSuccessTitle;
	}

	/**
	 * @param string
	 */
	public void setInspectionRequestedMessage(String string) {
		inspectionRequestedMessage = string;
	}

	/**
	 * @param string
	 */
	public void setInspectionRequestedSubject(String string) {
		inspectionRequestedSubject = string;
	}

	/**
	 * @param string
	 */
	public void setInspectionRequestedTitle(String string) {
		inspectionRequestedTitle = string;
	}

	/**
	 * @param string
	 */
	public void setInspectionScheduledMessage(String string) {
		inspectionScheduledMessage = string;
	}

	/**
	 * @param string
	 */
	public void setInspectionScheduledSubject(String string) {
		inspectionScheduledSubject = string;
	}

	/**
	 * @param string
	 */
	public void setInspectionScheduledTitle(String string) {
		inspectionScheduledTitle = string;
	}

	/**
	 * @param string
	 */
	public void setPermitStatusChangeMessage(String string) {
		permitStatusChangeMessage = string;
	}

	/**
	 * @param string
	 */
	public void setPermitStatusChangeSubject(String string) {
		permitStatusChangeSubject = string;
	}

	/**
	 * @param string
	 */
	public void setPermitStatusChangeTitle(String string) {
		permitStatusChangeTitle = string;
	}

	/**
	 * @param string
	 */
	public void setPlanCheckStatusChangeMessage(String string) {
		planCheckStatusChangeMessage = string;
	}

	/**
	 * @param string
	 */
	public void setPlanCheckStatusChangeSubject(String string) {
		planCheckStatusChangeSubject = string;
	}

	/**
	 * @param string
	 */
	public void setPlanCheckStatusChangeTitle(String string) {
		planCheckStatusChangeTitle = string;
	}

	/**
	 * @param string
	 */
	public void setRegistrationSuccessMessage(String string) {
		registrationSuccessMessage = string;
	}

	/**
	 * @param string
	 */
	public void setRegistrationSuccessSubject(String string) {
		registrationSuccessSubject = string;
	}

	/**
	 * @param string
	 */
	public void setRegistrationSuccessTitle(String string) {
		registrationSuccessTitle = string;
	}

	/**
	 * @return
	 */
	public String getInspectionCancelledMessage() {
		return inspectionCancelledMessage;
	}

	/**
	 * @return
	 */
	public String getInspectionCancelledSubject() {
		return inspectionCancelledSubject;
	}

	/**
	 * @return
	 */
	public String getInspectionCancelledTitle() {
		return inspectionCancelledTitle;
	}

	/**
	 * @param string
	 */
	public void setInspectionCancelledMessage(String string) {
		inspectionCancelledMessage = string;
	}

	/**
	 * @param string
	 */
	public void setInspectionCancelledSubject(String string) {
		inspectionCancelledSubject = string;
	}

	/**
	 * @param string
	 */
	public void setInspectionCancelledTitle(String string) {
		inspectionCancelledTitle = string;
	}

	/**
	 * @return
	 */
	public String getForgotPasswordMessage() {
		return forgotPasswordMessage;
	}

	/**
	 * @return
	 */
	public String getForgotPasswordSubject() {
		return forgotPasswordSubject;
	}

	/**
	 * @return
	 */
	public String getForgotPasswordTitle() {
		return forgotPasswordTitle;
	}

	/**
	 * @param string
	 */
	public void setForgotPasswordMessage(String string) {
		forgotPasswordMessage = string;
	}

	/**
	 * @param string
	 */
	public void setForgotPasswordSubject(String string) {
		forgotPasswordSubject = string;
	}

	/**
	 * @param string
	 */
	public void setForgotPasswordTitle(String string) {
		forgotPasswordTitle = string;
	}

	/**
	 * @return
	 */
	public String getTempPasswordChangeMessage() {
		return tempPasswordChangeMessage;
	}

	/**
	 * @return
	 */
	public String getTempPasswordChangeSubject() {
		return tempPasswordChangeSubject;
	}

	/**
	 * @return
	 */
	public String getTempPasswordChangeTitle() {
		return tempPasswordChangeTitle;
	}

	/**
	 * @param string
	 */
	public void setTempPasswordChangeMessage(String string) {
		tempPasswordChangeMessage = string;
	}

	/**
	 * @param string
	 */
	public void setTempPasswordChangeSubject(String string) {
		tempPasswordChangeSubject = string;
	}

	/**
	 * @param string
	 */
	public void setTempPasswordChangeTitle(String string) {
		tempPasswordChangeTitle = string;
	}

	public String getPaymentChangeTitle() {
		return paymentChangeTitle;
	}

	public void setPaymentChangeTitle(String paymentChangeTitle) {
		this.paymentChangeTitle = paymentChangeTitle;
	}

	public String getPaymentChangeSubject() {
		return paymentChangeSubject;
	}

	public void setPaymentChangeSubject(String paymentChangeSubject) {
		this.paymentChangeSubject = paymentChangeSubject;
	}

	public String getPaymentChangeMessage() {
		return paymentChangeMessage;
	}

	public void setPaymentChangeMessage(String paymentChangeMessage) {
		this.paymentChangeMessage = paymentChangeMessage;
	}

}