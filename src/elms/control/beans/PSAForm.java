package elms.control.beans;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author Shekhar Jain
 */
public class PSAForm extends ActionForm {

	// Declare the variable and object declarations used by the class here
	protected String levelId;
	protected String levelType;
	protected String lsoId;

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		// nothing to be done
	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		return errors;
	}

	/**
	 * @return
	 */
	public String getLevelId() {
		return levelId;
	}

	/**
	 * @return
	 */
	public String getLevelType() {
		return levelType;
	}

	/**
	 * @return
	 */
	public String getLsoId() {
		return lsoId;
	}

	/**
	 * @param string
	 */
	public void setLevelId(String string) {
		levelId = string;
	}

	/**
	 * @param string
	 */
	public void setLevelType(String string) {
		levelType = string;
	}

	/**
	 * @param string
	 */
	public void setLsoId(String string) {
		lsoId = string;
	}

}
