//Source file: C:\\Datafile\\OBCSource\\elms\\control\\beans\\EditHoldForm.java

package elms.control.beans;

import org.apache.struts.action.ActionForm;

/**
 * @author Shekhar Jain
 */
public class HoldForm extends ActionForm {
	protected String id;
	protected String status;
	protected String title;
	protected String comments;
	protected String statusDate;
	protected String type;
	protected int createdBy;
	protected String createdDate;
	protected int updatedBy;
	protected String updatedDate;

	/**
	 * Access method for the date property.
	 * 
	 * @return the current value of the date property
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the value of the date property.
	 * 
	 * @param aDate
	 *            the new value of the date property
	 */
	public void setId(String aId) {
		id = aId;
	}

	/**
	 * Access method for the status property.
	 * 
	 * @return the current value of the status property
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the value of the status property.
	 * 
	 * @param aStatus
	 *            the new value of the status property
	 */
	public void setStatus(String aStatus) {
		status = aStatus;
	}

	/**
	 * Access method for the comments property.
	 * 
	 * @return the current value of the comments property
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * Sets the value of the comments property.
	 * 
	 * @param aComments
	 *            the new value of the comments property
	 */
	public void setComments(String aComments) {
		comments = aComments;
	}

	/**
	 * Access method for the title property.
	 * 
	 * @return the current value of the title property
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the value of the title property.
	 * 
	 * @param aTitle
	 *            the new value of the title property
	 */
	public void setTitle(String aTitle) {
		title = aTitle;
	}

	/**
	 * Access method for the statusDate property.
	 * 
	 * @return the current value of the statusDate property
	 */
	public String getStatusDate() {
		return statusDate;
	}

	/**
	 * Sets the value of the statusDate property.
	 * 
	 * @param aStatusDate
	 *            the new value of the statusDate property
	 */
	public void setStatusDate(String aStatusDate) {
		statusDate = aStatusDate;
	}

	/**
	 * Access method for the type property.
	 * 
	 * @return the current value of the type property
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the value of the type property.
	 * 
	 * @param aType
	 *            the new value of the type property
	 */
	public void setType(String aType) {
		type = aType;
	}

	/**
	 * Gets the createdBy
	 * 
	 * @return Returns a int
	 */
	public int getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the createdBy
	 * 
	 * @param createdBy
	 *            The createdBy to set
	 */
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Gets the createdDate
	 * 
	 * @return Returns a String
	 */
	public String getCreatedDate() {
		return createdDate;
	}

	/**
	 * Sets the createdDate
	 * 
	 * @param createdDate
	 *            The createdDate to set
	 */
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * Gets the updatedBy
	 * 
	 * @return Returns a int
	 */
	public int getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Sets the updatedBy
	 * 
	 * @param updatedBy
	 *            The updatedBy to set
	 */
	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Gets the updatedDate
	 * 
	 * @return Returns a String
	 */
	public String getUpdatedDate() {
		return updatedDate;
	}

	/**
	 * Sets the updatedDate
	 * 
	 * @param updatedDate
	 *            The updatedDate to set
	 */
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

}
