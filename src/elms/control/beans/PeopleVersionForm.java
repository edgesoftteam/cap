/*
 * Created on Oct 19, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package elms.control.beans;

import java.util.Calendar;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;

import elms.app.people.People;
import elms.util.StringUtils;

/**
 * @author Manjuprasad
 * 
 *         TODO To change the template for this generated type comment go to Window - Preferences - Java - Code Style - Code Templates
 */

public class PeopleVersionForm extends ActionForm {

	static Logger logger = Logger.getLogger(PeopleForm.class.getName());

	/**
	 * Member variable declaration
	 */

	protected People people;
	protected String strLicenseExpires;
	protected String Name;
	protected String businessLicenseNumber;
	protected String title;
	protected String businessName;
	protected String comments;
	protected String strHoldDate;
	protected String strDlExpiryDate;
	protected String strGeneralLiabilityDate;
	protected String strAutoLiabilityDate;
	protected String strBusinessLicenseExpires;
	protected String strWorkersCompExpires;
	protected int peopleCount;
	protected int versionNumber;
	protected String currentVersionNumber;
	protected String isExpired = "Y";
	protected String BLExpirationDate = "Y";
	protected String GLDate = "Y";
	protected String ALDate = "Y";
	protected String WCEDate = "Y";
	protected String hasHold = "N";
	protected String date = StringUtils.cal2str(Calendar.getInstance());
	protected String firstName;
	protected String address;
	protected String city;
	protected String state;
	protected String zip;
	protected String updated;
	protected String phone;
	protected String psaId;
	protected String psaType;

	protected String[] selectedPeople = {};

	/**
	 * @roseuid 3CAA2F42019B
	 */
	public PeopleVersionForm() {
		people = new People();
		this.psaId = "";
		this.psaType = "";
		setIsExpired("Yahoo");
		setBLExpirationDate("Yahoo");
		setGLDate("Yahoo");
		setALDate("Yahoo");
		setWCEDate("Yahoo");
	}

	/**
	 * @return Returns the strDlExpiryDate.
	 */
	public String getStrDlExpiryDate() {
		return strDlExpiryDate;
	}

	/**
	 * @param strDlExpiryDate
	 *            The strDlExpiryDate to set.
	 */
	public void setStrDlExpiryDate(String strDlExpiryDate) {
		this.strDlExpiryDate = strDlExpiryDate;
	}

	public void setPeople(People people) {
		try {
			if (people != null) {
				PropertyUtils.copyProperties(this.people, people);
			}
		} catch (Exception e) {
			// logger.error("error while copying people " + e.getMessage());
		}
	}

	/**
	 * Gets the people
	 * 
	 * @return Returns a People
	 */
	public People getPeople() {
		return people;
	}

	/**
	 * Gets the strLicenseExpires
	 * 
	 * @return Returns a String
	 */
	public String getStrLicenseExpires() {
		strLicenseExpires = StringUtils.cal2str(people.licenseExpires);
		return strLicenseExpires;
	}

	/**
	 * Sets the strLicenseExpires
	 * 
	 * @param strLicenseExpires
	 *            The strLicenseExpires to set
	 */
	public void setStrLicenseExpires(String strLicenseExpires) {
		getPeople().setLicenseExpires(StringUtils.str2cal(strLicenseExpires));
		this.strLicenseExpires = strLicenseExpires;
		if (Calendar.getInstance().after(this.getPeople().getLicenseExpires())) {
			this.isExpired = "Y";
			getPeople().setIsExpired("Y");
		} else {
			this.isExpired = "N";
			getPeople().setIsExpired("N");
		}
	}

	/**
	 * Gets the strHoldDate
	 * 
	 * @return Returns a String
	 */
	public String getStrHoldDate() {
		strHoldDate = StringUtils.cal2str(people.holdDate);
		return strHoldDate;
	}

	/**
	 * Sets the strHoldDate
	 * 
	 * @param strHoldDate
	 *            The strHoldDate to set
	 */
	public void setStrHoldDate(String strHoldDate) {
		this.people.holdDate = StringUtils.str2cal(strHoldDate);
		this.strHoldDate = strHoldDate;
	}

	/**
	 * Gets the strGeneralLiabilityDate
	 * 
	 * @return Returns a String
	 */
	public String getStrGeneralLiabilityDate() {
		strGeneralLiabilityDate = StringUtils.cal2str(people.generalLiabilityDate);
		return strGeneralLiabilityDate;
	}

	/**
	 * Sets the strGeneralLiabilityDate
	 * 
	 * @param strGeneralLiabilityDate
	 *            The strGeneralLiabilityDate to set
	 */
	public void setStrGeneralLiabilityDate(String strGeneralLiabilityDate) {
		getPeople().setGeneralLiabilityDate(StringUtils.str2cal(strGeneralLiabilityDate));
		// this.people.generalLiabilityDate = StringUtils.str2cal(strGeneralLiabilityDate);
		this.strGeneralLiabilityDate = strGeneralLiabilityDate;
		if (Calendar.getInstance().after(this.getPeople().getGeneralLiabilityDate())) {
			this.GLDate = "Y";
			getPeople().setGLDate("Y");
		} else {
			this.GLDate = "N";
			getPeople().setGLDate("N");
		}

	}

	/**
	 * Gets the strAutoLiabilityDate
	 * 
	 * @return Returns a String
	 */
	public String getStrAutoLiabilityDate() {
		strAutoLiabilityDate = StringUtils.cal2str(people.autoLiabilityDate);
		return strAutoLiabilityDate;
	}

	/**
	 * Sets the strAutoLiabilityDate
	 * 
	 * @param strAutoLiabilityDate
	 *            The strAutoLiabilityDate to set
	 */
	public void setStrAutoLiabilityDate(String strAutoLiabilityDate) {
		// this.people.autoLiabilityDate = StringUtils.str2cal(strAutoLiabilityDate);
		getPeople().setAutoLiabilityDate(StringUtils.str2cal(strAutoLiabilityDate));
		this.strAutoLiabilityDate = strAutoLiabilityDate;
		if (Calendar.getInstance().after(this.getPeople().getAutoLiabilityDate())) {
			this.ALDate = "Y";
			getPeople().setALDate("Y");
		} else {
			this.ALDate = "N";
			getPeople().setALDate("N");
		}

	}

	/**
	 * Gets the strBusinessLicenseExpires
	 * 
	 * @return Returns a String
	 */
	public String getStrBusinessLicenseExpires() {
		strBusinessLicenseExpires = StringUtils.cal2str(people.businessLicenseExpires);
		return strBusinessLicenseExpires;
	}

	/**
	 * Sets the strBusinessLicenseExpires
	 * 
	 * @param strBusinessLicenseExpires
	 *            The strBusinessLicenseExpires to set
	 */
	public void setStrBusinessLicenseExpires(String strBusinessLicenseExpires) {
		// this.people.businessLicenseExpires = StringUtils.str2cal(strBusinessLicenseExpires);
		getPeople().setBusinessLicenseExpires(StringUtils.str2cal(strBusinessLicenseExpires));
		this.strBusinessLicenseExpires = strBusinessLicenseExpires;
		if (Calendar.getInstance().after(this.getPeople().getBusinessLicenseExpires())) {
			this.BLExpirationDate = "Y";
			getPeople().setBLExpirationDate("Y");
		} else {
			this.BLExpirationDate = "N";
			getPeople().setBLExpirationDate("N");
		}

	}

	/**
	 * Gets the strWorkersCompExpires
	 * 
	 * @return Returns a String
	 */
	public String getStrWorkersCompExpires() {
		strWorkersCompExpires = StringUtils.cal2str(people.workersCompExpires);
		return strWorkersCompExpires;
	}

	/**
	 * Sets the strWorkersCompExpires
	 * 
	 * @param strWorkersCompExpires
	 *            The strWorkersCompExpires to set
	 */
	public void setStrWorkersCompExpires(String strWorkersCompExpires) {
		// this.people.workersCompExpires = StringUtils.str2cal(strWorkersCompExpires);
		getPeople().setWorkersCompExpires(StringUtils.str2cal(strWorkersCompExpires));
		this.strWorkersCompExpires = strWorkersCompExpires;
		if (Calendar.getInstance().after(this.getPeople().getWorkersCompExpires())) {
			this.WCEDate = "Y";
			getPeople().setWCEDate("Y");
		} else {
			this.WCEDate = "N";
			getPeople().setWCEDate("N");
		}

	}

	/**
	 * @return Returns the businessLicenseNumber.
	 */
	public String getBusinessLicenseNumber() {
		return businessLicenseNumber;
	}

	/**
	 * @param businessLicenseNumber
	 *            The businessLicenseNumber to set.
	 */
	public void setBusinessLicenseNumber(String businessLicenseNumber) {
		this.businessLicenseNumber = businessLicenseNumber;
	}

	/**
	 * @return Returns the businessName.
	 */
	public String getBusinessName() {
		return businessName;
	}

	/**
	 * @param businessName
	 *            The businessName to set.
	 */
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	/**
	 * @return Returns the comments.
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments
	 *            The comments to set.
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return Returns the name.
	 */
	public String getName() {
		return Name;
	}

	/**
	 * @param name
	 *            The name to set.
	 */
	public void setName(String name) {
		Name = name;
	}

	/**
	 * @return Returns the title.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            The title to set.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the selectedPeople
	 * 
	 * @return Returns a String[]
	 */
	public String[] getSelectedPeople() {
		return selectedPeople;
	}

	/**
	 * Sets the selectedPeople
	 * 
	 * @param selectedPeople
	 *            The selectedPeople to set
	 */
	public void setSelectedPeople(String[] selectedPeople) {
		this.selectedPeople = selectedPeople;
	}

	/**
	 * Gets the peopleCount
	 * 
	 * @return Returns a int
	 */
	public int getPeopleCount() {
		return peopleCount;
	}

	/**
	 * Sets the peopleCount
	 * 
	 * @param peopleCount
	 *            The peopleCount to set
	 */
	public void setPeopleCount(int peopleCount) {
		this.peopleCount = peopleCount;
	}

	/**
	 * Returns the date.
	 * 
	 * @return String
	 */
	public String getDate() {
		return date;
	}

	/**
	 * Returns the isExpired.
	 * 
	 * @return String
	 */
	public String getIsExpired() {
		return isExpired;
	}

	/**
	 * Sets the date.
	 * 
	 * @param date
	 *            The date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * Sets the isExpired.
	 * 
	 * @param isExpired
	 *            The isExpired to set
	 */
	public void setIsExpired(String isExpired) {
		this.isExpired = isExpired;
	}

	/**
	 * Returns the logger.
	 * 
	 * @return Logger
	 */
	public static Logger getLogger() {
		return logger;
	}

	/**
	 * Returns the aLDate.
	 * 
	 * @return String
	 */
	public String getALDate() {
		return ALDate;
	}

	/**
	 * Returns the bLExpirationDate.
	 * 
	 * @return String
	 */
	public String getBLExpirationDate() {
		return BLExpirationDate;
	}

	/**
	 * Returns the gLDate.
	 * 
	 * @return String
	 */
	public String getGLDate() {
		return GLDate;
	}

	/**
	 * Returns the wCEDate.
	 * 
	 * @return String
	 */
	public String getWCEDate() {
		return WCEDate;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param logger
	 *            The logger to set
	 */
	public static void setLogger(Logger logger) {
		PeopleForm.logger = logger;
	}

	/**
	 * Sets the aLDate.
	 * 
	 * @param aLDate
	 *            The aLDate to set
	 */
	public void setALDate(String aLDate) {
		ALDate = aLDate;
	}

	/**
	 * Sets the bLExpirationDate.
	 * 
	 * @param bLExpirationDate
	 *            The bLExpirationDate to set
	 */
	public void setBLExpirationDate(String bLExpirationDate) {
		BLExpirationDate = bLExpirationDate;
	}

	/**
	 * Sets the gLDate.
	 * 
	 * @param gLDate
	 *            The gLDate to set
	 */
	public void setGLDate(String gLDate) {
		GLDate = gLDate;
	}

	/**
	 * Sets the wCEDate.
	 * 
	 * @param wCEDate
	 *            The wCEDate to set
	 */
	public void setWCEDate(String wCEDate) {
		WCEDate = wCEDate;
	}

	/**
	 * Returns the hasHold.
	 * 
	 * @return String
	 */
	public String getHasHold() {
		return hasHold;
	}

	/**
	 * Sets the hasHold.
	 * 
	 * @param hasHold
	 *            The hasHold to set
	 */
	public void setHasHold(String hasHold) {
		this.hasHold = hasHold;
	}

	/**
	 * @return
	 */
	public String getPsaId() {
		return psaId;
	}

	/**
	 * @return
	 */
	public String getPsaType() {
		return psaType;
	}

	/**
	 * @param string
	 */
	public void setPsaId(String string) {
		psaId = string;
	}

	/**
	 * @param string
	 */
	public void setPsaType(String string) {
		psaType = string;
	}

	/**
	 * @return Returns the versionNumber.
	 */
	public int getVersionNumber() {
		return versionNumber;
	}

	/**
	 * @param versionNumber
	 *            The versionNumber to set.
	 */
	public void setVersionNumber(int versionNumber) {
		this.versionNumber = versionNumber;
	}

	/**
	 * @return Returns the address.
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            The address to set.
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return Returns the city.
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            The city to set.
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return Returns the firstName.
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            The firstName to set.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return Returns the phone.
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone
	 *            The phone to set.
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return Returns the state.
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            The state to set.
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return Returns the updated.
	 */
	public String getUpdated() {
		return updated;
	}

	/**
	 * @param updated
	 *            The updated to set.
	 */
	public void setUpdated(String updated) {
		this.updated = updated;
	}

	/**
	 * @return Returns the zip.
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * @param zip
	 *            The zip to set.
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * @return Returns the currentVersionNumber.
	 */
	public String getCurrentVersionNumber() {
		return currentVersionNumber;
	}

	/**
	 * @param currentVersionNumber
	 *            The currentVersionNumber to set.
	 */
	public void setCurrentVersionNumber(String currentVersionNumber) {
		this.currentVersionNumber = currentVersionNumber;
	}
}
