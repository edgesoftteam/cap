package elms.control.beans;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class AddLookupFeesForm extends ActionForm {

	protected String name;
	protected String creationDate;

	protected LookupFeeEdit[] lkupFeeEditList;

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		// delcare validations for the form
		/*
		 * if ((firstName == null) || (firstName.length() < 1)) errors.add("firstName",new ActionError("error.firstname.required"));
		 * 
		 * if ((lastName == null) || (lastName.length() < 1)) errors.add("lastName",new ActionError("error.lastname.required"));
		 */
		return errors;
	}

	/**
	 * Gets the name
	 * 
	 * @return Returns a String
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name
	 * 
	 * @param name
	 *            The name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the creationDate
	 * 
	 * @return Returns a String
	 */
	public String getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the creationDate
	 * 
	 * @param creationDate
	 *            The creationDate to set
	 */
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Gets the lkupFeeEditList
	 * 
	 * @return Returns a LookupFeeEdit[]
	 */
	public LookupFeeEdit[] getLkupFeeEditList() {
		return lkupFeeEditList;
	}

	/**
	 * Sets the lkupFeeEditList
	 * 
	 * @param lkupFeeEditList
	 *            The lkupFeeEditList to set
	 */
	public void setLkupFeeEditList(LookupFeeEdit[] lkupFeeEditList) {
		this.lkupFeeEditList = lkupFeeEditList;
	}

} // End class