package elms.control.beans;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author shekhar
 * 
 *         To change this generated comment edit the template variable "typecomment": Window>Preferences>Java>Templates. To enable and disable the creation of type comments go to Window>Preferences>Java>Code Generation.
 */
public class ProjectViewerForm extends ActionForm {

	protected String lsoId;
	protected String address;
	protected String status = "";
	protected List projectList;

	public void reset(ActionMapping mapping, HttpServletRequest request) {

	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		return errors;
	}

	/**
	 * Returns the lsoId.
	 * 
	 * @return String
	 */
	public String getLsoId() {
		return lsoId;
	}

	/**
	 * Sets the lsoId.
	 * 
	 * @param lsoId
	 *            The lsoId to set
	 */
	public void setLsoId(String lsoId) {
		this.lsoId = lsoId;
	}

	/**
	 * Returns the address.
	 * 
	 * @return String
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 * 
	 * @param address
	 *            The address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Returns the projectList.
	 * 
	 * @return List
	 */
	public List getProjectList() {
		return projectList;
	}

	/**
	 * Sets the projectList.
	 * 
	 * @param projectList
	 *            The projectList to set
	 */
	public void setProjectList(List projectList) {
		this.projectList = projectList;
	}

	/**
	 * Returns the status.
	 * 
	 * @return String
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 * 
	 * @param status
	 *            The status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

}
