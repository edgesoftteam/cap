package elms.control.beans;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author Venkata Kastala
 */
public class PlanningActivitiesForm extends ActionForm {
	protected String activityId;

	protected String noOfTables;
	protected String noOfChairs;
	protected String squareFeet;
	protected String hoursOpen;
	protected String alcoholBev;
	protected String councilApproval;
	protected String noOfKids;
	protected String mondayThruFriday;
	protected String saturdayAndSunday;
	protected String noOfParkingSpacesReq;
	protected String noOfParkingSpacesProv;
	protected String noOfParkingSpacesUnderInlieu;
	protected String noOfBeds;
	protected String comments;

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
		ActionErrors errors = new ActionErrors();
		return errors;
	}

	/**
	 * Returns the activityId.
	 * 
	 * @return String
	 */
	public String getActivityId() {
		return activityId;
	}

	/**
	 * Returns the comments.
	 * 
	 * @return String
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * Returns the mondayThruFriday.
	 * 
	 * @return String
	 */
	public String getMondayThruFriday() {
		return mondayThruFriday;
	}

	/**
	 * Returns the noOfKids.
	 * 
	 * @return String
	 */
	public String getNoOfKids() {
		return noOfKids;
	}

	/**
	 * Returns the saturdayAndSunday.
	 * 
	 * @return String
	 */
	public String getSaturdayAndSunday() {
		return saturdayAndSunday;
	}

	/**
	 * Sets the activityId.
	 * 
	 * @param activityId
	 *            The activityId to set
	 */
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	/**
	 * Sets the comments.
	 * 
	 * @param comments
	 *            The comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * Sets the mondayThruFriday.
	 * 
	 * @param mondayThruFriday
	 *            The mondayThruFriday to set
	 */
	public void setMondayThruFriday(String mondayThruFriday) {
		this.mondayThruFriday = mondayThruFriday;
	}

	/**
	 * Sets the noOfKids.
	 * 
	 * @param noOfKids
	 *            The noOfKids to set
	 */
	public void setNoOfKids(String noOfKids) {
		this.noOfKids = noOfKids;
	}

	/**
	 * Sets the saturdayAndSunday.
	 * 
	 * @param saturdayAndSunday
	 *            The saturdayAndSunday to set
	 */
	public void setSaturdayAndSunday(String saturdayAndSunday) {
		this.saturdayAndSunday = saturdayAndSunday;
	}

	/**
	 * Returns the alcoholBev.
	 * 
	 * @return String
	 */
	public String getAlcoholBev() {
		return alcoholBev;
	}

	/**
	 * Returns the councilApproval.
	 * 
	 * @return String
	 */
	public String getCouncilApproval() {
		return councilApproval;
	}

	/**
	 * Returns the hoursOpen.
	 * 
	 * @return String
	 */
	public String getHoursOpen() {
		return hoursOpen;
	}

	/**
	 * Returns the noOfBeds.
	 * 
	 * @return String
	 */
	public String getNoOfBeds() {
		return noOfBeds;
	}

	/**
	 * Returns the noOfChairs.
	 * 
	 * @return String
	 */
	public String getNoOfChairs() {
		return noOfChairs;
	}

	/**
	 * Returns the noOfParkingSpacesProv.
	 * 
	 * @return String
	 */
	public String getNoOfParkingSpacesProv() {
		return noOfParkingSpacesProv;
	}

	/**
	 * Returns the noOfParkingSpacesReq.
	 * 
	 * @return String
	 */
	public String getNoOfParkingSpacesReq() {
		return noOfParkingSpacesReq;
	}

	/**
	 * Returns the noOfParkingSpacesUnderInlieu.
	 * 
	 * @return String
	 */
	public String getNoOfParkingSpacesUnderInlieu() {
		return noOfParkingSpacesUnderInlieu;
	}

	/**
	 * Returns the noOfTables.
	 * 
	 * @return String
	 */
	public String getNoOfTables() {
		return noOfTables;
	}

	/**
	 * Returns the squareFeet.
	 * 
	 * @return String
	 */
	public String getSquareFeet() {
		return squareFeet;
	}

	/**
	 * Sets the alcoholBev.
	 * 
	 * @param alcoholBev
	 *            The alcoholBev to set
	 */
	public void setAlcoholBev(String alcoholBev) {
		this.alcoholBev = alcoholBev;
	}

	/**
	 * Sets the councilApproval.
	 * 
	 * @param councilApproval
	 *            The councilApproval to set
	 */
	public void setCouncilApproval(String councilApproval) {
		this.councilApproval = councilApproval;
	}

	/**
	 * Sets the hoursOpen.
	 * 
	 * @param hoursOpen
	 *            The hoursOpen to set
	 */
	public void setHoursOpen(String hoursOpen) {
		this.hoursOpen = hoursOpen;
	}

	/**
	 * Sets the noOfBeds.
	 * 
	 * @param noOfBeds
	 *            The noOfBeds to set
	 */
	public void setNoOfBeds(String noOfBeds) {
		this.noOfBeds = noOfBeds;
	}

	/**
	 * Sets the noOfChairs.
	 * 
	 * @param noOfChairs
	 *            The noOfChairs to set
	 */
	public void setNoOfChairs(String noOfChairs) {
		this.noOfChairs = noOfChairs;
	}

	/**
	 * Sets the noOfParkingSpacesProv.
	 * 
	 * @param noOfParkingSpacesProv
	 *            The noOfParkingSpacesProv to set
	 */
	public void setNoOfParkingSpacesProv(String noOfParkingSpacesProv) {
		this.noOfParkingSpacesProv = noOfParkingSpacesProv;
	}

	/**
	 * Sets the noOfParkingSpacesReq.
	 * 
	 * @param noOfParkingSpacesReq
	 *            The noOfParkingSpacesReq to set
	 */
	public void setNoOfParkingSpacesReq(String noOfParkingSpacesReq) {
		this.noOfParkingSpacesReq = noOfParkingSpacesReq;
	}

	/**
	 * Sets the noOfParkingSpacesUnderInlieu.
	 * 
	 * @param noOfParkingSpacesUnderInlieu
	 *            The noOfParkingSpacesUnderInlieu to set
	 */
	public void setNoOfParkingSpacesUnderInlieu(String noOfParkingSpacesUnderInlieu) {
		this.noOfParkingSpacesUnderInlieu = noOfParkingSpacesUnderInlieu;
	}

	/**
	 * Sets the noOfTables.
	 * 
	 * @param noOfTables
	 *            The noOfTables to set
	 */
	public void setNoOfTables(String noOfTables) {
		this.noOfTables = noOfTables;
	}

	/**
	 * Sets the squareFeet.
	 * 
	 * @param squareFeet
	 *            The squareFeet to set
	 */
	public void setSquareFeet(String squareFeet) {
		this.squareFeet = squareFeet;
	}

}