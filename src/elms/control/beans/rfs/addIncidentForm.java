/*
 * Created on Mar 25, 2008
 * @author Akash
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package elms.control.beans.rfs;

import org.apache.struts.action.ActionForm;


public class addIncidentForm extends ActionForm{
	
	protected int incidentid;
	protected String poi="";
	protected String priority;
	protected String incidentstreetnumber="";
	protected String incidentstreetname;
	protected String incidentunit="";
	protected String crossstreet1;
	protected String crossstreet2;
	protected String problem="";
	protected String incidentdepartment;
	protected String description;
	protected String memo;
	protected String incidentcity="Glendale";
	protected String incidentstate="CA";
	protected String incidentzip="";
	protected String incidentaddress;
	protected int activityid;
	private String status;

	
	/**
	 * @return Returns the activityid.
	 */
	public int getActivityid() {
		return activityid;
	}
	/**
	 * @param activityid The activityid to set.
	 */
	public void setActivityid(int activityid) {
		this.activityid = activityid;
	}
	/**
	 * @return Returns the incidentaddress.
	 */
	public String getIncidentaddress() {
		return incidentaddress;
	}
	/**
	 * @param incidentaddress The incidentaddress to set.
	 */
	public void setIncidentaddress(String incidentaddress) {
		this.incidentaddress = incidentaddress;
	}
	/**
	 * @return Returns the crossstreet1.
	 */
	public String getCrossstreet1() {
		return crossstreet1;
	}
	/**
	 * @param crossstreet1 The crossstreet1 to set.
	 */
	public void setCrossstreet1(String crossstreet1) {
		this.crossstreet1 = crossstreet1;
	}
	/**
	 * @return Returns the crossstreet2.
	 */
	public String getCrossstreet2() {
		return crossstreet2;
	}
	/**
	 * @param crossstreet2 The crossstreet2 to set.
	 */
	public void setCrossstreet2(String crossstreet2) {
		this.crossstreet2 = crossstreet2;
	}
	/**
	 * @return Returns the description.
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return Returns the incidentdepartment.
	 */
	public String getIncidentdepartment() {
		return incidentdepartment;
	}
	/**
	 * @param incidentdepartment The incidentdepartment to set.
	 */
	public void setIncidentdepartment(String incidentdepartment) {
		this.incidentdepartment = incidentdepartment;
	}
	/**
	 * @return Returns the incidentid.
	 */
	public int getIncidentid() {
		return incidentid;
	}
	/**
	 * @param incidentid The incidentid to set.
	 */
	public void setIncidentid(int incidentid) {
		this.incidentid = incidentid;
	}
	/**
	 * @return Returns the incidentstreetname.
	 */
	public String getIncidentstreetname() {
		return incidentstreetname;
	}
	/**
	 * @param incidentstreetname The incidentstreetname to set.
	 */
	public void setIncidentstreetname(String incidentstreetname) {
		this.incidentstreetname = incidentstreetname;
	}
	
	/**
	 * @return Returns the incidentstreetnumber.
	 */
	public String getIncidentstreetnumber() {
		return incidentstreetnumber;
	}
	/**
	 * @param incidentstreetnumber The incidentstreetnumber to set.
	 */
	public void setIncidentstreetnumber(String incidentstreetnumber) {
		this.incidentstreetnumber = incidentstreetnumber;
	}
	
	/**
	 * @return Returns the incidentunit.
	 */
	public String getIncidentunit() {
		return incidentunit;
	}
	/**
	 * @param incidentunit The incidentunit to set.
	 */
	public void setIncidentunit(String incidentunit) {
		this.incidentunit = incidentunit;
	}
	/**
	 * @return Returns the memo.
	 */
	public String getMemo() {
		return memo;
	}
	/**
	 * @param memo The memo to set.
	 */
	public void setMemo(String memo) {
		this.memo = memo;
	}
	/**
	 * @return Returns the poi.
	 */
	public String getPoi() {
		return poi;
	}
	/**
	 * @param poi The poi to set.
	 */
	public void setPoi(String poi) {
		this.poi = poi;
	}
	/**
	 * @return Returns the priority.
	 */
	public String getPriority() {
		return priority;
	}
	/**
	 * @param priority The priority to set.
	 */
	public void setPriority(String priority) {
		this.priority = priority;
	}
	/**
	 * @return Returns the problem.
	 */
	public String getProblem() {
		return problem;
	}
	/**
	 * @param problem The problem to set.
	 */
	public void setProblem(String problem) {
		this.problem = problem;
	}
	/**
	 * @return Returns the incidentcity.
	 */
	public String getIncidentcity() {
		return incidentcity;
	}
	/**
	 * @param incidentcity The incidentcity to set.
	 */
	public void setIncidentcity(String incidentcity) {
		this.incidentcity = incidentcity;
	}
	/**
	 * @return Returns the incidentstate.
	 */
	public String getIncidentstate() {
		return incidentstate;
	}
	/**
	 * @param incidentstate The incidentstate to set.
	 */
	public void setIncidentstate(String incidentstate) {
		this.incidentstate = incidentstate;
	}
	/**
	 * @return Returns the incidentzip.
	 */
	public String getIncidentzip() {
		return incidentzip;
	}
	/**
	 * @param incidentzip The incidentzip to set.
	 */
	public void setIncidentzip(String incidentzip) {
		this.incidentzip = incidentzip;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
}
