/*
 * Created on Mar 20, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package elms.control.beans.rfs;


import java.util.List;

import org.apache.struts.action.ActionForm;

/**
 * @author Ashwini
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ListRequestServiceForm extends ActionForm {
	
	protected String rfsId;
	protected String incidentId;
	protected String activityId;
	protected String requestDate;
	protected String rfsDate;
	private String rfsDateFrom;
	protected String streetNumber;
	protected String streetName;
	protected String unit;
	protected String problem;
	protected String reRoute;
	protected String address;
	protected String[] selectedItems;
	protected List requestList;
	protected String deptId;
	protected String selectedActivity;
	protected String incistatus;
	protected List requestListOthers;
	private String rfsText;
	
   
	
	
	/**
	 * @return Returns the requestListOthers.
	 */
	public List getRequestListOthers() {
		return requestListOthers;
	}
	/**
	 * @param requestListOthers The requestListOthers to set.
	 */
	public void setRequestListOthers(List requestListOthers) {
		this.requestListOthers = requestListOthers;
	}
	/**
	 * @return Returns the incistatus.
	 */
	public String getIncistatus() {
		return incistatus;
	}
	/**
	 * @param incistatus The incistatus to set.
	 */
	public void setIncistatus(String incistatus) {
		this.incistatus = incistatus;
	}
	
	protected List activityList;

    /**
     * @return Returns the activityList.
     */
    public List getActivityList() {
        return activityList;
    }
    /**
     * @param activityList The activityList to set.
     */
    public void setActivityList(List activityList) {
        this.activityList = activityList;
    }
    	
	
	/**
	 * @return Returns the rfsDate.
	 */
	public String getRfsDate() {
		return rfsDate;
	}
	/**
	 * @param rfsDate The rfsDate to set.
	 */
	public void setRfsDate(String rfsDate) {
		this.rfsDate = rfsDate;
	}
	
	/**
	 * @return Returns the address.
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address The address to set.
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * @return Returns the reRoute.
	 */
	public String getReRoute() {
		return reRoute;
	}
	/**
	 * @param reRoute The reRoute to set.
	 */
	public void setReRoute(String reRoute) {
		this.reRoute = reRoute;
	}
		
    /**
     * @return Returns the requestList.
     */
    public List getRequestList() {
        return requestList;
    }
    /**
     * @param requestList The requestList to set.
     */
    public void setRequestList(List requestList) {
        this.requestList = requestList;
    }
    /*public void setRequestList(ActivityListForm[] requestList) {
        if (requestList == null) requestList = new ArrayList();
        ActivityListForm[] activityListForm = new ActivityListForm[requestList.size()];
        Iterator iter = requestList.iterator();
        int index = 0;
        while (iter.hasNext()) {
            activityListForm[index] = (ActivityListForm) iter.next();
            index++;
        }
        this.requestList = activityListForm;
    }*/
	/**
	 * @return Returns the incidentId.
	 */
	/**
	 * @return Returns the problem.
	 */
	public String getProblem() {
		return problem;
	}
	/**
	 * @param problem The problem to set.
	 */
	public void setProblem(String problem) {
		this.problem = problem;
	}
	/**
	 * @return Returns the requestDate.
	 */
	public String getRequestDate() {
		return requestDate;
	}
	/**
	 * @param requestDate The requestDate to set.
	 */
	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}
	/**
	 * @return Returns the streetName.
	 */
	public String getStreetName() {
		return streetName;
	}
	/**
	 * @param streetName The streetName to set.
	 */
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}
	/**
	 * @return Returns the streetNumber.
	 */
	public String getStreetNumber() {
		return streetNumber;
	}
	/**
	 * @param streetNumber The streetNumber to set.
	 */
	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}
	/**
	 * @return Returns the unit.
	 */
	public String getUnit() {
		return unit;
	}
	/**
	 * @param unit The unit to set.
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}
	/**
	 * @return Returns the selectedItems.
	 */
	public String[] getSelectedItems() {
		return selectedItems;
	}
	/**
	 * @param selectedItems The selectedItems to set.
	 */
	public void setSelectedItems(String[] selectedItems) {
		this.selectedItems = selectedItems;
	}
    /**
     * @return Returns the activityId.
     */
    public String getActivityId() {
        return activityId;
    }
    /**
     * @param activityId The activityId to set.
     */
    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }
    /**
     * @return Returns the deptId.
     */
    public String getDeptId() {
        return deptId;
    }
    /**
     * @param deptId The deptId to set.
     */
    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }
    /**
     * @return Returns the incidentId.
     */
    public String getIncidentId() {
        return incidentId;
    }
    /**
     * @param incidentId The incidentId to set.
     */
    public void setIncidentId(String incidentId) {
        this.incidentId = incidentId;
    }
    /**
     * @return Returns the rfsId.
     */
    public String getRfsId() {
        return rfsId;
    }
    /**
     * @param rfsId The rfsId to set.
     */
    public void setRfsId(String rfsId) {
        this.rfsId = rfsId;
    }
   
    /**
     * @return Returns the selectedActivity.
     */
    public String getSelectedActivity() {
        return selectedActivity;
    }
    /**
     * @param selectedActivity The selectedActivity to set.
     */
    public void setSelectedActivity(String selectedActivity) {
        this.selectedActivity = selectedActivity;
    }
	public String getRfsDateFrom() {
		return rfsDateFrom;
	}
	public void setRfsDateFrom(String rfsDateFrom) {
		this.rfsDateFrom = rfsDateFrom;
	}
	/**
	 * @return the rfsText
	 */
	public String getRfsText() {
		return rfsText;
	}
	/**
	 * @param rfsText the rfsText to set
	 */
	public void setRfsText(String rfsText) {
		this.rfsText = rfsText;
	}
}


