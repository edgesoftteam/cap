/*
 * Created on Apr 4, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package elms.control.beans.rfs;


/**
 * @author Ashwini
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ActivityListDetail implements java.io.Serializable{
    protected String activityId;
    protected String activityNumber;
    

    /**
     * @return Returns the activityId.
     */
    public String getActivityId() {
        return activityId;
    }
    /**
     * @param activityId The activityId to set.
     */
    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }
    /**
     * @return Returns the activityNumber.
     */
    public String getActivityNumber() {
        return activityNumber;
    }
    /**
     * @param activityNumber The activityNumber to set.
     */
    public void setActivityNumber(String activityNumber) {
        this.activityNumber = activityNumber;
    }
}
