/**
 * 
 */
package elms.control.beans.rfs;

import java.io.Serializable;

/**
 * @author Gaurav Garg
 *
 */
public class RFSIncident implements Serializable{

	private String rfsNo;
	private String incidentNo;
	private String type;
	private String status;
	private String description;
	
	/**
	 * @return the rfsNo
	 */
	public String getRfsNo() {
		return rfsNo;
	}
	/**
	 * @param rfsNo the rfsNo to set
	 */
	public void setRfsNo(String rfsNo) {
		this.rfsNo = rfsNo;
	}
	/**
	 * @return the incidentNo
	 */
	public String getIncidentNo() {
		return incidentNo;
	}
	/**
	 * @param incidentNo the incidentNo to set
	 */
	public void setIncidentNo(String incidentNo) {
		this.incidentNo = incidentNo;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	private String address;
}
