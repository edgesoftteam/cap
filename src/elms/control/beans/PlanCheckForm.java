package elms.control.beans;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import elms.app.project.PlanCheck;

/**
 * @author Shekhar Jain
 */
public class PlanCheckForm extends ActionForm {

	// Declare the variable and object declarations used by the class here
	protected String planCheckStatus;
	protected String planCheckId;
	protected String planCheckDate;
	protected String enginner;
	protected String engineerId;
	protected String comments;
	protected String activityId;
	protected PlanCheck[] planChecks;
	protected String[] selectedPlanCheck = {};

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		planCheckStatus = null;
		planCheckDate = null;
		engineerId = null;
		comments = null;
		planChecks = null;
	}

	public PlanCheckForm() {
		planCheckStatus = null;
		planCheckDate = null;
		engineerId = null;
		comments = null;
		planChecks = new PlanCheck[0];
	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
		ActionErrors errors = new ActionErrors();

		// delcare validations for the form
		/*
		 * if ((firstName == null) || (firstName.length() < 1)) errors.add("firstName",new ActionError("error.firstname.required"));
		 * 
		 * if ((lastName == null) || (lastName.length() < 1)) errors.add("lastName",new ActionError("error.lastname.required"));
		 */
		return errors;
	}

	/**
	 * Gets the planCheckStatus
	 * 
	 * @return Returns a String
	 */
	public String getPlanCheckStatus() {
		return planCheckStatus;
	}

	/**
	 * Sets the planCheckStatus
	 * 
	 * @param planCheckStatus
	 *            The planCheckStatus to set
	 */
	public void setPlanCheckStatus(String planCheckStatus) {
		this.planCheckStatus = planCheckStatus;
	}

	/**
	 * Gets the date
	 * 
	 * @return Returns a String
	 */
	public String getPlanCheckDate() {
		return planCheckDate;
	}

	/**
	 * Sets the date
	 * 
	 * @param date
	 *            The date to set
	 */
	public void setPlanCheckDate(String planCheckdate) {
		this.planCheckDate = planCheckdate;
	}

	/**
	 * Gets the comments
	 * 
	 * @return Returns a String
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * Sets the comments
	 * 
	 * @param comments
	 *            The comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * Gets the activityId
	 * 
	 * @return Returns a String
	 */
	public String getActivityId() {
		return activityId;
	}

	/**
	 * Sets the activityId
	 * 
	 * @param activityId
	 *            The activityId to set
	 */
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	/**
	 * Gets the planChecks
	 * 
	 * @return Returns an Array
	 */
	public PlanCheck[] getPlanChecks() {
		return planChecks;
	}

	/**
	 * Sets the planChecks
	 * 
	 * @param planChecks
	 *            The planChecks to set
	 */
	public void setPlanChecks(PlanCheck[] planChecks) {
		this.planChecks = planChecks;
	}

	/**
	 * Returns the engineerId.
	 * 
	 * @return String
	 */
	public String getEngineerId() {
		return engineerId;
	}

	/**
	 * Sets the engineerId.
	 * 
	 * @param engineerId
	 *            The engineerId to set
	 */
	public void setEngineerId(String engineerId) {
		this.engineerId = engineerId;
	}

	/**
	 * Returns the enginner.
	 * 
	 * @return String
	 */
	public String getEnginner() {
		return enginner;
	}

	/**
	 * Sets the enginner.
	 * 
	 * @param enginner
	 *            The enginner to set
	 */
	public void setEnginner(String enginner) {
		this.enginner = enginner;
	}

	/**
	 * Returns the selectedPlanCheck.
	 * 
	 * @return String[]
	 */
	public String[] getSelectedPlanCheck() {
		return selectedPlanCheck;
	}

	/**
	 * Sets the selectedPlanCheck.
	 * 
	 * @param selectedPlanCheck
	 *            The selectedPlanCheck to set
	 */
	public void setSelectedPlanCheck(String[] selectedPlanCheck) {
		this.selectedPlanCheck = selectedPlanCheck;
	}

	/**
	 * Returns the planCheckId.
	 * 
	 * @return String
	 */
	public String getPlanCheckId() {
		return planCheckId;
	}

	/**
	 * Sets the planCheckId.
	 * 
	 * @param planCheckId
	 *            The planCheckId to set
	 */
	public void setPlanCheckId(String planCheckId) {
		this.planCheckId = planCheckId;
	}

}