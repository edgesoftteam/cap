package elms.control.beans;

import org.apache.struts.action.ActionForm;

public class BusinessLicenseApprovalForm extends ActionForm {

	protected int approvalId;
	protected String activityId;
	protected int departmentId;
	protected int userId;
	protected int approvalStatusId;
	protected String approvalStatus;
	protected String approvalDate;
	protected String referredDate;
	protected String comments;
	protected String departmentName;
	protected String userName;
	protected String applicationDateString;
	protected int updatedBy;
	protected String duplicate;

	/**
	 * @return Returns the updatedBy.
	 */
	public int getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy
	 *            The updatedBy to set.
	 */
	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * @return Returns the approvalStatus.
	 */
	public String getApprovalStatus() {
		return approvalStatus;
	}

	/**
	 * @param approvalStatus
	 *            The approvalStatus to set.
	 */
	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	/**
	 * @return Returns the approvalId.
	 */
	public int getApprovalId() {
		return approvalId;
	}

	/**
	 * @param approvalId
	 *            The approvalId to set.
	 */
	public void setApprovalId(int approvalId) {
		this.approvalId = approvalId;
	}

	/**
	 * @return Returns the departmentName.
	 */
	public String getDepartmentName() {
		return departmentName;
	}

	/**
	 * @param departmentName
	 *            The departmentName to set.
	 */
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	/**
	 * @return Returns the userName.
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            The userName to set.
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return Returns the activityId.
	 */
	public String getActivityId() {
		return activityId;
	}

	/**
	 * @param activityId
	 *            The activityId to set.
	 */
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	/**
	 * @return Returns the approvalDate.
	 */
	public String getApprovalDate() {
		return approvalDate;
	}

	/**
	 * @param approvalDate
	 *            The approvalDate to set.
	 */
	public void setApprovalDate(String approvalDate) {
		this.approvalDate = approvalDate;
	}

	/**
	 * @return Returns the approvalStatusId.
	 */
	public int getApprovalStatusId() {
		return approvalStatusId;
	}

	/**
	 * @param approvalStatusId
	 *            The approvalStatusId to set.
	 */
	public void setApprovalStatusId(int approvalStatusId) {
		this.approvalStatusId = approvalStatusId;
	}

	/**
	 * @return Returns the comments.
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments
	 *            The comments to set.
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return Returns the departmentId.
	 */
	public int getDepartmentId() {
		return departmentId;
	}

	/**
	 * @param departmentId
	 *            The departmentId to set.
	 */
	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * @return Returns the referredDate.
	 */
	public String getReferredDate() {
		return referredDate;
	}

	/**
	 * @param referredDate
	 *            The referredDate to set.
	 */
	public void setReferredDate(String referredDate) {
		this.referredDate = referredDate;
	}

	/**
	 * @return Returns the userId.
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            The userId to set.
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @return Returns the applicationDateString.
	 */
	public String getApplicationDateString() {
		return applicationDateString;
	}

	/**
	 * @param applicationDateString
	 *            The applicationDateString to set.
	 */
	public void setApplicationDateString(String applicationDateString) {
		this.applicationDateString = applicationDateString;
	}

	/**
	 * @return Returns the duplicate.
	 */
	public String getDuplicate() {
		return duplicate;
	}

	/**
	 * @param duplicate
	 *            The duplicate to set.
	 */
	public void setDuplicate(String duplicate) {
		this.duplicate = duplicate;
	}
}