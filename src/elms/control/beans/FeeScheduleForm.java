package elms.control.beans;

import org.apache.struts.action.ActionForm;

import elms.app.admin.FeeEdit;

/**
 * @author Shekhar Jain
 */
public class FeeScheduleForm extends ActionForm {

	protected String action;
	protected String activityType;
	protected String percent;
	protected FeeEdit[] feeEditList;
	protected String rowCount;

	/**
	 * Gets the change
	 * 
	 * @return Returns a String
	 */
	public String getPercent() {
		return percent;
	}

	/**
	 * Sets the change
	 * 
	 * @param change
	 *            The change to set
	 */
	public void setPercent(String percent) {
		this.percent = percent;
	}

	/**
	 * Gets the feeEditList
	 * 
	 * @return Returns a FeeEdit[]
	 */
	public FeeEdit[] getFeeEditList() {
		return feeEditList;
	}

	/**
	 * Sets the feeEditList
	 * 
	 * @param feeEditList
	 *            The feeEditList to set
	 */
	public void setFeeEditList(FeeEdit[] feeEditList) {
		this.feeEditList = feeEditList;
	}

	/**
	 * Gets the activityType
	 * 
	 * @return Returns a String
	 */
	public String getActivityType() {
		return activityType;
	}

	/**
	 * Sets the activityType
	 * 
	 * @param activityType
	 *            The activityType to set
	 */
	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	/**
	 * Gets the action
	 * 
	 * @return Returns a String
	 */
	public String getAction() {
		return action;
	}

	/**
	 * Sets the action
	 * 
	 * @param action
	 *            The action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * Gets the rowCount
	 * 
	 * @return Returns a String
	 */
	public String getRowCount() {
		return rowCount;
	}

	/**
	 * Sets the rowCount
	 * 
	 * @param rowCount
	 *            The rowCount to set
	 */
	public void setRowCount(String rowCount) {
		this.rowCount = rowCount;
	}
}