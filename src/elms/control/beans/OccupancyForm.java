package elms.control.beans;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import elms.app.lso.Street;

public class OccupancyForm extends ActionForm {
	// occupancy Details
	protected String lsoId;
	protected String alias;
	protected String description;
	protected String unitNo;
	protected String beginningFloor;
	protected String endingFloor;
	protected boolean active;
	protected String label;
	protected String householdSize;
	protected String bedroomSize;
	protected String grossRent;
	protected String utilityAllowance;
	protected String propertyManager;
	protected String restrictedUnits;
	// occupancy address
	protected int addressId;
	protected String streetNumber;
	protected String streetMod;
	protected String streetName;
	protected Street street = new Street();
	protected String unit;
	protected String city;
	protected String state;
	protected String zip;
	protected String zip4;
	protected String addrDescription;
	protected boolean primary;
	protected boolean activeCheck;
	// occupancy apn
	protected String apn;
	protected String ownerName;
	protected String ownerId;
	protected String apnStreetNumber;
	protected String apnPreDir;
	protected String apnStreetMod;
	protected String apnStreetName;
	protected String apnCity;
	protected String apnState;
	protected String apnZip;
	protected String foreignAddress = "N";
	protected String line1;
	protected String line4;
	protected String line3;
	protected String line2;
	protected String country;
	protected String email;
	protected String phone;
	protected String fax;
	protected String parkingZone;
	protected String resZone;

	protected String[] selectedUse = {};

	/**
	 * Gets the selectedUse
	 * 
	 * @return Returns a String[]
	 */
	public String[] getSelectedUse() {
		return selectedUse;
	}

	/**
	 * Sets the selectedUse
	 * 
	 * @param selectedUse
	 *            The selectedUse to set
	 */
	public void setSelectedUse(String[] selectedUse) {
		this.selectedUse = selectedUse;
	}

	/**
	 * Gets the lsoId
	 * 
	 * @return Returns a String
	 */
	public String getLsoId() {
		return lsoId;
	}

	/**
	 * Sets the lsoId
	 * 
	 * @param lsoId
	 *            The lsoId to set
	 */
	public void setLsoId(String lsoId) {
		this.lsoId = lsoId;
	}

	/**
	 * Gets the streetName
	 * 
	 * @return Returns a String
	 */
	public String getStreetName() {
		return streetName;
	}

	/**
	 * Sets the streetName
	 * 
	 * @param streetName
	 *            The streetName to set
	 */
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	/**
	 * Gets the street
	 * 
	 * @return Returns a String
	 */
	public Street getStreet() {
		return street;
	}

	/**
	 * Sets the street
	 * 
	 * @param streetName
	 *            The streetName to set
	 */
	public void setStreet(Street street) {
		this.street = street;
	}

	/**
	 * Gets the apn
	 * 
	 * @return Returns a String
	 */
	public String getApn() {
		return apn;
	}

	/**
	 * Sets the apn
	 * 
	 * @param apn
	 *            The apn to set
	 */
	public void setApn(String apn) {
		this.apn = apn;
	}

	/**
	 * Gets the addressId
	 * 
	 * @return Returns a String
	 */
	public int getAddressId() {
		return addressId;
	}

	/**
	 * Sets the addressId
	 * 
	 * @param addressId
	 *            The addressId to set
	 */
	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}

	/**
	 * Gets the endingFloor
	 * 
	 * @return Returns a String
	 */
	public String getEndingFloor() {
		return endingFloor;
	}

	/**
	 * Sets the endingFloor
	 * 
	 * @param endingFloor
	 *            The endingFloor to set
	 */
	public void setEndingFloor(String endingFloor) {
		this.endingFloor = endingFloor;
	}

	/**
	 * Gets the apnCity
	 * 
	 * @return Returns a String
	 */
	public String getApnCity() {
		return apnCity;
	}

	/**
	 * Sets the apnCity
	 * 
	 * @param apnCity
	 *            The apnCity to set
	 */
	public void setApnCity(String apnCity) {
		this.apnCity = apnCity;
	}

	/**
	 * Gets the foreignAddress
	 * 
	 * @return Returns a String
	 */
	public String getForeignAddress() {
		return foreignAddress;
	}

	/**
	 * Sets the foreignAddress
	 * 
	 * @param foreignAddress
	 *            The foreignAddress to set
	 */
	public void setForeignAddress(String foreignAddress) {
		this.foreignAddress = foreignAddress;
	}

	/**
	 * Gets the phone
	 * 
	 * @return Returns a String
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * Sets the phone
	 * 
	 * @param phone
	 *            The phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * Gets the streetMod
	 * 
	 * @return Returns a String
	 */
	public String getStreetMod() {
		return streetMod;
	}

	/**
	 * Sets the streetMod
	 * 
	 * @param streetMod
	 *            The streetMod to set
	 */
	public void setStreetMod(String streetMod) {
		this.streetMod = streetMod;
	}

	/**
	 * Gets the fax
	 * 
	 * @return Returns a String
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * Sets the fax
	 * 
	 * @param fax
	 *            The fax to set
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the line4
	 * 
	 * @return Returns a String
	 */
	public String getLine4() {
		return line4;
	}

	/**
	 * Sets the line4
	 * 
	 * @param line4
	 *            The line4 to set
	 */
	public void setLine4(String line4) {
		this.line4 = line4;
	}

	/**
	 * Gets the line3
	 * 
	 * @return Returns a String
	 */
	public String getLine3() {
		return line3;
	}

	/**
	 * Sets the line3
	 * 
	 * @param line3
	 *            The line3 to set
	 */
	public void setLine3(String line3) {
		this.line3 = line3;
	}

	/**
	 * Gets the line2
	 * 
	 * @return Returns a String
	 */
	public String getLine2() {
		return line2;
	}

	/**
	 * Sets the line2
	 * 
	 * @param line2
	 *            The line2 to set
	 */
	public void setLine2(String line2) {
		this.line2 = line2;
	}

	/**
	 * Gets the unitNo
	 * 
	 * @return Returns a String
	 */
	public String getUnitNo() {
		return unitNo;
	}

	/**
	 * Sets the unitNo
	 * 
	 * @param unitNo
	 *            The unitNo to set
	 */
	public void setUnitNo(String unitNo) {
		this.unitNo = unitNo;
	}

	/**
	 * Gets the line1
	 * 
	 * @return Returns a String
	 */
	public String getLine1() {
		return line1;
	}

	/**
	 * Sets the line1
	 * 
	 * @param line1
	 *            The line1 to set
	 */
	public void setLine1(String line1) {
		this.line1 = line1;
	}

	/**
	 * Gets the city
	 * 
	 * @return Returns a String
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the city
	 * 
	 * @param city
	 *            The city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Gets the apnState
	 * 
	 * @return Returns a String
	 */
	public String getApnState() {
		return apnState;
	}

	/**
	 * Sets the apnState
	 * 
	 * @param apnState
	 *            The apnState to set
	 */
	public void setApnState(String apnState) {
		this.apnState = apnState;
	}

	/**
	 * Gets the zip
	 * 
	 * @return Returns a String
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * Sets the zip
	 * 
	 * @param zip
	 *            The zip to set
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * Gets the zip4
	 * 
	 * @return Returns a String
	 */
	public String getZip4() {
		return zip4;
	}

	/**
	 * Sets the zip4
	 * 
	 * @param zip4
	 *            The zip4 to set
	 */
	public void setZip4(String zip4) {
		this.zip4 = zip4;
	}

	/**
	 * Gets the email
	 * 
	 * @return Returns a String
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email
	 * 
	 * @param email
	 *            The email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the apnStreetMod
	 * 
	 * @return Returns a String
	 */
	public String getApnStreetMod() {
		return apnStreetMod;
	}

	/**
	 * Sets the apnStreetMod
	 * 
	 * @param apnStreetMod
	 *            The apnStreetMod to set
	 */
	public void setApnStreetMod(String apnStreetMod) {
		this.apnStreetMod = apnStreetMod;
	}

	/**
	 * Gets the apnZip
	 * 
	 * @return Returns a String
	 */
	public String getApnZip() {
		return apnZip;
	}

	/**
	 * Sets the apnZip
	 * 
	 * @param apnZip
	 *            The apnZip to set
	 */
	public void setApnZip(String apnZip) {
		this.apnZip = apnZip;
	}

	/**
	 * Gets the apnPreDir
	 * 
	 * @return Returns a String
	 */
	public String getApnPreDir() {
		return apnPreDir;
	}

	/**
	 * Sets the apnPreDir
	 * 
	 * @param apnPreDir
	 *            The apnPreDir to set
	 */
	public void setApnPreDir(String apnPreDir) {
		this.apnPreDir = apnPreDir;
	}

	/**
	 * Gets the apnStreetName
	 * 
	 * @return Returns a String
	 */
	public String getApnStreetName() {
		return apnStreetName;
	}

	/**
	 * Sets the apnStreetName
	 * 
	 * @param apnStreetName
	 *            The apnStreetName to set
	 */
	public void setApnStreetName(String apnStreetName) {
		this.apnStreetName = apnStreetName;
	}

	/**
	 * Gets the country
	 * 
	 * @return Returns a String
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Sets the country
	 * 
	 * @param country
	 *            The country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Gets the alias
	 * 
	 * @return Returns a String
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * Sets the alias
	 * 
	 * @param alias
	 *            The alias to set
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}

	/**
	 * Gets the unit
	 * 
	 * @return Returns a String
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * Sets the unit
	 * 
	 * @param unit
	 *            The unit to set
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * Gets the addrDescription
	 * 
	 * @return Returns a String
	 */
	public String getAddrDescription() {
		return addrDescription;
	}

	/**
	 * Sets the addrDescription
	 * 
	 * @param addrDescription
	 *            The addrDescription to set
	 */
	public void setAddrDescription(String addrDescription) {
		this.addrDescription = addrDescription;
	}

	/**
	 * Gets the beginningFloor
	 * 
	 * @return Returns a String
	 */
	public String getBeginningFloor() {
		return beginningFloor;
	}

	/**
	 * Sets the beginningFloor
	 * 
	 * @param beginningFloor
	 *            The beginningFloor to set
	 */
	public void setBeginningFloor(String beginningFloor) {
		this.beginningFloor = beginningFloor;
	}

	/**
	 * Gets the apnStreetNumber
	 * 
	 * @return Returns a String
	 */
	public String getApnStreetNumber() {
		return apnStreetNumber;
	}

	/**
	 * Sets the apnStreetNumber
	 * 
	 * @param apnStreetNumber
	 *            The apnStreetNumber to set
	 */
	public void setApnStreetNumber(String apnStreetNumber) {
		this.apnStreetNumber = apnStreetNumber;
	}

	/**
	 * Gets the primaryCheck
	 * 
	 * @return Returns a String / public String getPrimaryCheck() { return primaryCheck; } /** Sets the primaryCheck
	 * @param primaryCheck
	 *            The primaryCheck to set / public void setPrimaryCheck(String primaryCheck) { this.primaryCheck = primaryCheck; }
	 */

	/**
	 * Gets the ownerName
	 * 
	 * @return Returns a String
	 */
	public String getOwnerName() {
		return ownerName;
	}

	/**
	 * Sets the ownerName
	 * 
	 * @param ownerName
	 *            The ownerName to set
	 */
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	/**
	 * Gets the ownerId
	 * 
	 * @return Returns a String
	 */
	public String getOwnerId() {
		return ownerId;
	}

	/**
	 * Sets the ownerId
	 * 
	 * @param ownerId
	 *            The ownerId to set
	 */
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	/**
	 * Gets the streetNumber
	 * 
	 * @return Returns a String
	 */
	public String getStreetNumber() {
		return streetNumber;
	}

	/**
	 * Sets the streetNumber
	 * 
	 * @param streetNumber
	 *            The streetNumber to set
	 */
	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	/**
	 * Gets the state
	 * 
	 * @return Returns a String
	 */
	public String getState() {
		return state;
	}

	/**
	 * Sets the state
	 * 
	 * @param state
	 *            The state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		// reset the declaration values...or rather set it to something default.
		// this.firstName = null;
		// this.lastName = null;

	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		// delcare validations for the form
		/*
		 * if ((firstName == null) || (firstName.length() < 1)) errors.add("firstName",new ActionError("error.firstname.required"));
		 * 
		 * if ((lastName == null) || (lastName.length() < 1)) errors.add("lastName",new ActionError("error.lastname.required"));
		 */
		return errors;
	}

	/**
	 * Gets the active
	 * 
	 * @return Returns a boolean
	 */
	public boolean getActive() {
		return active;
	}

	/**
	 * Sets the active
	 * 
	 * @param active
	 *            The active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * Gets the activeCheck
	 * 
	 * @return Returns a boolean
	 */
	public boolean getActiveCheck() {
		return activeCheck;
	}

	/**
	 * Sets the activeCheck
	 * 
	 * @param activeCheck
	 *            The activeCheck to set
	 */
	public void setActiveCheck(boolean activeCheck) {
		this.activeCheck = activeCheck;
	}

	/**
	 * Gets the primary
	 * 
	 * @return Returns a boolean
	 */
	public boolean getPrimary() {
		return primary;
	}

	/**
	 * Sets the primary
	 * 
	 * @param primary
	 *            The primary to set
	 */
	public void setPrimary(boolean primary) {
		this.primary = primary;
	}

	/**
	 * Returns the parkingZone.
	 * 
	 * @return String
	 */
	public String getParkingZone() {
		return parkingZone;
	}

	/**
	 * Returns the resZone.
	 * 
	 * @return String
	 */
	public String getResZone() {
		return resZone;
	}

	/**
	 * Sets the parkingZone.
	 * 
	 * @param parkingZone
	 *            The parkingZone to set
	 */
	public void setParkingZone(String parkingZone) {
		this.parkingZone = parkingZone;
	}

	/**
	 * Sets the resZone.
	 * 
	 * @param resZone
	 *            The resZone to set
	 */
	public void setResZone(String resZone) {
		this.resZone = resZone;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return the householdSize
	 */
	public String getHouseholdSize() {
		return householdSize;
	}

	/**
	 * @param householdSize the householdSize to set
	 */
	public void setHouseholdSize(String householdSize) {
		this.householdSize = householdSize;
	}

	/**
	 * @return the bedroomSize
	 */
	public String getBedroomSize() {
		return bedroomSize;
	}

	/**
	 * @param bedroomSize the bedroomSize to set
	 */
	public void setBedroomSize(String bedroomSize) {
		this.bedroomSize = bedroomSize;
	}

	/**
	 * @return the grossRent
	 */
	public String getGrossRent() {
		return grossRent;
	}

	/**
	 * @param grossRent the grossRent to set
	 */
	public void setGrossRent(String grossRent) {
		this.grossRent = grossRent;
	}

	/**
	 * @return the utilityAllowance
	 */
	public String getUtilityAllowance() {
		return utilityAllowance;
	}

	/**
	 * @param utilityAllowance the utilityAllowance to set
	 */
	public void setUtilityAllowance(String utilityAllowance) {
		this.utilityAllowance = utilityAllowance;
	}

	/**
	 * @return the propertyManager
	 */
	public String getPropertyManager() {
		return propertyManager;
	}

	/**
	 * @param propertyManager the propertyManager to set
	 */
	public void setPropertyManager(String propertyManager) {
		this.propertyManager = propertyManager;
	}

	/**
	 * @return the restrictedUnits
	 */
	public String getRestrictedUnits() {
		return restrictedUnits;
	}

	/**
	 * @param restrictedUnits the restrictedUnits to set
	 */
	public void setRestrictedUnits(String restrictedUnits) {
		this.restrictedUnits = restrictedUnits;
	}

} // End class