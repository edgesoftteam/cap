package elms.control.beans;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author Shekhar Jain
 */
public class AssessorForm extends ActionForm {

	// Declare the variable and object declarations used by the class here

	protected String apn;
	protected String streetName;
	protected String streetId;
	protected String streetNumber;
	protected String streetFraction;
	protected String unitNbr;

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		// reset the declaration values...or rather set it to something default.
		this.apn = null;

	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		return errors;
	}

	/**
	 * Gets the apn
	 * 
	 * @return Returns a String
	 */
	public String getApn() {
		return apn;
	}

	/**
	 * Sets the apn
	 * 
	 * @param apn
	 *            The apn to set
	 */
	public void setApn(String apn) {
		this.apn = apn;
	}

	/**
	 * Returns the streetFraction.
	 * 
	 * @return String
	 */
	public String getStreetFraction() {
		return streetFraction;
	}

	/**
	 * Returns the streetName.
	 * 
	 * @return String
	 */
	public String getStreetName() {
		return streetName;
	}

	/**
	 * Returns the streetNumber.
	 * 
	 * @return String
	 */
	public String getStreetNumber() {
		return streetNumber;
	}

	/**
	 * Returns the unitNbr.
	 * 
	 * @return String
	 */
	public String getUnitNbr() {
		return unitNbr;
	}

	/**
	 * Sets the streetFraction.
	 * 
	 * @param streetFraction
	 *            The streetFraction to set
	 */
	public void setStreetFraction(String streetFraction) {
		this.streetFraction = streetFraction;
	}

	/**
	 * Sets the streetName.
	 * 
	 * @param streetName
	 *            The streetName to set
	 */
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	/**
	 * Sets the streetNumber.
	 * 
	 * @param streetNumber
	 *            The streetNumber to set
	 */
	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	/**
	 * Sets the unitNbr.
	 * 
	 * @param unitNbr
	 *            The unitNbr to set
	 */
	public void setUnitNbr(String unitNbr) {
		this.unitNbr = unitNbr;
	}

	/**
	 * Returns the streetId.
	 * 
	 * @return String
	 */
	public String getStreetId() {
		return streetId;
	}

	/**
	 * Sets the streetId.
	 * 
	 * @param streetId
	 *            The streetId to set
	 */
	public void setStreetId(String streetId) {
		this.streetId = streetId;
	}

}
