package elms.control.beans.combo;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author Shekhar Jain
 */
public class ComboWizardForm extends ActionForm {

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		return errors;
	}

	protected String normal;
	protected String sproj;
	protected String project;
	protected String projectDesc;
	protected String activity;
	protected String valuation;
	protected String lsoid;
	protected String lsoLevel;
	protected String newProject;
	protected String[] selectedActivity;
	protected String[] selectedActivitypc;
	protected String actDesc;
	protected String sprojDesc;
	protected String psaProjectDesc;

	/**
	 * @return
	 */
	public String getNormal() {
		return normal;
	}

	/**
	 * @param string
	 */
	public void setNormal(String string) {
		normal = string;
	}

	/**
	 * @return
	 */
	public String getProject() {
		return project;
	}

	/**
	 * @return
	 */
	public String getProjectDesc() {
		return projectDesc;
	}

	/**
	 * @return
	 * 
	 *         public String getValuation() { return valuation; }
	 * 
	 *         /**
	 * @param list
	 */
	public void setProject(String string) {
		project = string;
	}

	/**
	 * @param string
	 */
	public void setProjectDesc(String string) {
		projectDesc = string;
	}

	/**
	 * @param list
	 */

	public void setValuation(String string) {
		valuation = string;
	}

	/**
	 * @return
	 */
	public String getLsoid() {
		return lsoid;
	}

	/**
	 * @return
	 */
	public String getLsoLevel() {
		return lsoLevel;
	}

	/**
	 * @param string
	 */
	public void setLsoid(String string) {
		lsoid = string;
	}

	/**
	 * @param string
	 */
	public void setLsoLevel(String string) {
		lsoLevel = string;
	}

	/**
	 * @param string
	 */
	public void setActivity(String string) {
		activity = string;
	}

	/**
	 * @param string
	 */
	public void setSproj(String string) {
		sproj = string;
	}

	/**
	 * @return
	 */
	public String getActivity() {
		return activity;
	}

	/**
	 * @return
	 */
	public String getSproj() {
		return sproj;
	}

	/**
	 * @return
	 * 
	 *         public String getValuation() { return valuation; }
	 * 
	 *         /**
	 * @param list
	 * 
	 *            /**
	 * @return
	 */
	public String[] getSelectedActivity() {
		return selectedActivity;
	}

	/**
	 * @param strings
	 */
	public void setSelectedActivity(String[] strings) {
		selectedActivity = strings;
	}

	/**
	 * @return
	 */
	public String getNewProject() {
		return newProject;
	}

	/**
	 * @param string
	 */
	public void setNewProject(String string) {
		newProject = string;
	}

	/**
	 * @return
	 */
	public String getValuation() {
		return valuation;
	}

	/**
	 * @return
	 */
	public String getActDesc() {
		return actDesc;
	}

	/**
	 * @return
	 */
	public String getPsaProjectDesc() {
		return psaProjectDesc;
	}

	/**
	 * @return
	 */
	public String getSprojDesc() {
		return sprojDesc;
	}

	/**
	 * @param string
	 */
	public void setActDesc(String string) {
		actDesc = string;
	}

	/**
	 * @param string
	 */
	public void setPsaProjectDesc(String string) {
		psaProjectDesc = string;
	}

	/**
	 * @param string
	 */
	public void setSprojDesc(String string) {
		sprojDesc = string;
	}

	/**
	 * @return
	 */
	/**
	 * @return Returns the selectedActivitypc.
	 */
	public String[] getSelectedActivitypc() {
		return selectedActivitypc;
	}

	/**
	 * @param selectedActivitypc
	 *            The selectedActivitypc to set.
	 */
	public void setSelectedActivitypc(String[] selectedActivitypc) {
		this.selectedActivitypc = selectedActivitypc;
	}
}