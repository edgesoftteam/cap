//Source file: D:\\OBC\\war\\src\\main\\classes\\elms\\control\\beans\\ActivityForm.java

package elms.control.beans.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author Shekhar Jain
 */
public class ActivityTypeForm extends ActionForm {

	// Declare the variable and object declarations used by the class here
	private String activityTypeId;
	private String activityType;
	private String description;
	private String department;
	private String subProjectType;
	private String moduleId;
	private String subProjectSubType;
	private String subProjectSubSubType;
	private String muncipalCode;
	private String sicCode;
	private String classCode;
	protected String[] selectedMaping = {};

	protected List activityTypes;
	protected String displayActTypes;

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		this.activityType = null;
		this.description = null;
		this.department = null;
		this.subProjectType = null;
		this.subProjectSubType = null;
		this.subProjectSubSubType = null;
		this.activityTypes = null;
		this.displayActTypes = null;
		this.muncipalCode = null;
		this.sicCode = null;
		this.classCode = null;
	}

	/**
	 * @return Returns the moduleId.
	 */
	public String getModuleId() {
		return moduleId;
	}

	/**
	 * @param moduleId
	 *            The moduleId to set.
	 */
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}

	/**
	 * Returns the activityType.
	 * 
	 * @return String
	 */
	public String getActivityType() {
		return activityType;
	}

	/**
	 * Returns the department.
	 * 
	 * @return String
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * Returns the description.
	 * 
	 * @return String
	 */
	public String getDescription() {
		return description;
	}



	/**
	 * Returns the subProjectSubSubType.
	 * 
	 * @return String
	 */
	public String getSubProjectSubSubType() {
		return subProjectSubSubType;
	}

	/**
	 * Returns the subProjectSubType.
	 * 
	 * @return String
	 */
	public String getSubProjectSubType() {
		return subProjectSubType;
	}

	/**
	 * Returns the subProjectType.
	 * 
	 * @return String
	 */
	public String getSubProjectType() {
		return subProjectType;
	}

	/**
	 * Sets the activityType.
	 * 
	 * @param activityType
	 *            The activityType to set
	 */
	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	/**
	 * Sets the department.
	 * 
	 * @param department
	 *            The department to set
	 */
	public void setDepartment(String department) {
		this.department = department;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}



	/**
	 * Sets the subProjectSubSubType.
	 * 
	 * @param subProjectSubSubType
	 *            The subProjectSubSubType to set
	 */
	public void setSubProjectSubSubType(String subProjectSubSubType) {
		this.subProjectSubSubType = subProjectSubSubType;
	}

	/**
	 * Sets the subProjectSubType.
	 * 
	 * @param subProjectSubType
	 *            The subProjectSubType to set
	 */
	public void setSubProjectSubType(String subProjectSubType) {
		this.subProjectSubType = subProjectSubType;
	}

	/**
	 * Sets the subProjectType.
	 * 
	 * @param subProjectType
	 *            The subProjectType to set
	 */
	public void setSubProjectType(String subProjectType) {
		this.subProjectType = subProjectType;
	}

	/**
	 * Returns the selectedMaping.
	 * 
	 * @return String[]
	 */
	public String[] getSelectedMaping() {
		return selectedMaping;
	}

	/**
	 * Sets the selectedMaping.
	 * 
	 * @param selectedMaping
	 *            The selectedMaping to set
	 */
	public void setSelectedMaping(String[] selectedMaping) {
		this.selectedMaping = selectedMaping;
	}

	/**
	 * Returns the activityTypeId.
	 * 
	 * @return String
	 */
	public String getActivityTypeId() {
		return activityTypeId;
	}

	/**
	 * Returns the activityTypes.
	 * 
	 * @return List
	 */
	public List getActivityTypes() {
		return activityTypes;
	}

	/**
	 * Returns the displayActTypes.
	 * 
	 * @return String
	 */
	public String getDisplayActTypes() {
		return displayActTypes;
	}

	/**
	 * Sets the activityTypeId.
	 * 
	 * @param activityTypeId
	 *            The activityTypeId to set
	 */
	public void setActivityTypeId(String activityTypeId) {
		this.activityTypeId = activityTypeId;
	}

	/**
	 * Sets the activityTypes.
	 * 
	 * @param activityTypes
	 *            The activityTypes to set
	 */
	public void setActivityTypes(List activityTypes) {
		this.activityTypes = activityTypes;
	}

	/**
	 * Sets the displayActTypes.
	 * 
	 * @param displayActTypes
	 *            The displayActTypes to set
	 */
	public void setDisplayActTypes(String displayActTypes) {
		this.displayActTypes = displayActTypes;
	}

	/**
	 * @return Returns the classCode.
	 */
	public String getClassCode() {
		return classCode;
	}

	/**
	 * @param classCode
	 *            The classCode to set.
	 */
	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	/**
	 * @return Returns the muncipalCode.
	 */
	public String getMuncipalCode() {
		return muncipalCode;
	}

	/**
	 * @param muncipalCode
	 *            The muncipalCode to set.
	 */
	public void setMuncipalCode(String muncipalCode) {
		this.muncipalCode = muncipalCode;
	}

	/**
	 * @return Returns the sicCode.
	 */
	public String getSicCode() {
		return sicCode;
	}

	/**
	 * @param sicCode
	 *            The sicCode to set.
	 * 
	 */
	public void setSicCode(String sicCode) {
		this.sicCode = sicCode;
	}
}
