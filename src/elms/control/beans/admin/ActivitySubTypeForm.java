//Source file: D:\\OBC\\war\\src\\main\\classes\\elms\\control\\beans\\ActivitySubTypeForm.java

package elms.control.beans.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author Gai3
 */
public class ActivitySubTypeForm extends ActionForm {

	// Declare the variable and object declarations used by the class here
	private int activitySubTypeId;
	private String activityType;
	private String activitySubType;
	protected String[] selectedMaping = {};

	protected List activitySubTypes;
	protected String displayActSubTypes;

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		this.activityType = null;
		this.activitySubType = null;
	}

	/**
	 * Returns the activityType.
	 * 
	 * @return String
	 */
	public String getActivityType() {
		return activityType;
	}

	/**
	 * Sets the activityType.
	 * 
	 * @param activityType
	 *            The activityType to set
	 */
	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	/**
	 * @return Returns the activitySubType.
	 */
	public String getActivitySubType() {
		return activitySubType;
	}

	/**
	 * @param activitySubType
	 *            The activitySubType to set.
	 */
	public void setActivitySubType(String activitySubType) {
		this.activitySubType = activitySubType;
	}

	/**
	 * @return Returns the activitySubTypeId.
	 */
	public int getActivitySubTypeId() {
		return activitySubTypeId;
	}

	/**
	 * @param activitySubTypeId
	 *            The activitySubTypeId to set.
	 */
	public void setActivitySubTypeId(int activitySubTypeId) {
		this.activitySubTypeId = activitySubTypeId;
	}

	/**
	 * @return Returns the activitySubTypes.
	 */
	public List getActivitySubTypes() {
		return activitySubTypes;
	}

	/**
	 * @param activitySubTypes
	 *            The activitySubTypes to set.
	 */
	public void setActivitySubTypes(List activitySubTypes) {
		this.activitySubTypes = activitySubTypes;
	}

	/**
	 * @return Returns the displayActSubTypes.
	 */
	public String getDisplayActSubTypes() {
		return displayActSubTypes;
	}

	/**
	 * @param displayActSubTypes
	 *            The displayActSubTypes to set.
	 */
	public void setDisplayActSubTypes(String displayActSubTypes) {
		this.displayActSubTypes = displayActSubTypes;
	}

	/**
	 * @return Returns the selectedMaping.
	 */
	public String[] getSelectedMaping() {
		return selectedMaping;
	}

	/**
	 * @param selectedMaping
	 *            The selectedMaping to set.
	 */
	public void setSelectedMaping(String[] selectedMaping) {
		this.selectedMaping = selectedMaping;
	}
}
