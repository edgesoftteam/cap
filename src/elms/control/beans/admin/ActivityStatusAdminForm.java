package elms.control.beans.admin;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;

import elms.control.actions.admin.ActivityStatusAdminAction;

public class ActivityStatusAdminForm extends ActionForm {
	static Logger logger = Logger.getLogger(ActivityStatusAdminAction.class.getName());

	private String activityStatusId;
	private String statusCode;
	private String descriptionType;
	private boolean active;
	private boolean psaActive;
	private String projectNameId;
	private String moduleId;
	protected String[] selectedMaping = {};
	protected List activityStatus;
	protected String displayActStatus;

	/**
	 * @return Returns the activityStatusId.
	 */
	public String getActivityStatusId() {
		return activityStatusId;
	}

	/**
	 * @param activityStatusId
	 *            The activityStatusId to set.
	 */
	public void setActivityStatusId(String activityStatusId) {
		this.activityStatusId = activityStatusId;
	}

	/**
	 * @return Returns the displayActStatus.
	 */
	public String getDisplayActStatus() {
		return displayActStatus;
	}

	/**
	 * @param displayActStatus
	 *            The displayActStatus to set.
	 */
	public void setDisplayActStatus(String displayActStatus) {
		this.displayActStatus = displayActStatus;
	}

	/**
	 * @return Returns the descriptionType.
	 */
	public String getDescriptionType() {
		return descriptionType;
	}

	/**
	 * @param descriptionType
	 *            The descriptionType to set.
	 */
	public void setDescriptionType(String descriptionType) {
		this.descriptionType = descriptionType;
	}

	/**
	 * @return Returns the moduleId.
	 */
	public String getModuleId() {
		return moduleId;
	}

	/**
	 * @param moduleId
	 *            The moduleId to set.
	 */
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}

	/**
	 * @return Returns the projectNameId.
	 */
	public String getProjectNameId() {
		return projectNameId;
	}

	/**
	 * @param projectNameId
	 *            The projectNameId to set.
	 */
	public void setProjectNameId(String projectNameId) {
		this.projectNameId = projectNameId;
	}

	/**
	 * @return Returns the psaActive.
	 */
	public void setPsaActive(boolean psaActive) {
		this.psaActive = psaActive;
	}

	/**
	 * @return Returns the statusCode.
	 */
	public String getStatusCode() {
		return statusCode;
	}

	/**
	 * @param statusCode
	 *            The statusCode to set.
	 */
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * @return Returns the active.
	 */

	/**
	 * @return Returns the activityStatus.
	 */
	public List getActivityStatus() {
		return activityStatus;
	}

	/**
	 * @param activityStatus
	 *            The activityStatus to set.
	 */
	public void setActivityStatus(List activityStatus) {
		this.activityStatus = activityStatus;
	}

	/**
	 * @return Returns the selectedMaping.
	 */
	public String[] getSelectedMaping() {
		return selectedMaping;
	}

	/**
	 * @param selectedMaping
	 *            The selectedMaping to set.
	 */
	public void setSelectedMaping(String[] selectedMaping) {
		this.selectedMaping = selectedMaping;
	}

	/**
	 * @return Returns the active.
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * @param active
	 *            The active to set.
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * @return Returns the psaActive.
	 */
	public boolean isPsaActive() {
		return psaActive;
	}
	/**
	 * @param psaActive
	 *            The psaActive to set.
	 */
}
