package elms.control.beans.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author Shekhar Jain
 */
public class ConditionsLibraryForm extends ActionForm {
	// Declare the variable and object declarations used by the class here
	private String conditionId;
	private String levelType;
	private String conditionType;
	private String conditionCode;
	private String conditionDesc;
	private String shortText;
	private String creationDate;
	private String inspectability;
	private String warning;
	private String required;
	protected String[] selectedItems = {};
	private String checkDate;
	private List conditions;
	private String displayConditions;
	private String conditionText;

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		this.conditionId = null;
		this.levelType = null;
		this.conditionType = null;
		this.conditionCode = null;
		this.conditionDesc = null;
		this.shortText = null;
		this.creationDate = null;
		this.inspectability = null;
		this.warning = null;
		this.required = null;
	}

	/**
	 * @return Returns the conditionText.
	 */
	public String getConditionText() {
		return conditionText;
	}

	/**
	 * @param conditionText
	 *            The conditionText to set.
	 */
	public void setConditionText(String conditionText) {
		this.conditionText = conditionText;
	}

	/**
	 * Returns the conditionCode.
	 * 
	 * @return String
	 */
	public String getConditionCode() {
		return conditionCode;
	}

	/**
	 * Returns the conditionDesc.
	 * 
	 * @return String
	 */
	public String getConditionDesc() {
		return conditionDesc;
	}

	/**
	 * Returns the conditionId.
	 * 
	 * @return String
	 */
	public String getConditionId() {
		return conditionId;
	}

	/**
	 * Returns the conditionType.
	 * 
	 * @return String
	 */
	public String getConditionType() {
		return conditionType;
	}

	/**
	 * Returns the creationDate.
	 * 
	 * @return String
	 */
	public String getCreationDate() {
		return creationDate;
	}

	/**
	 * Returns the levelType.
	 * 
	 * @return String
	 */
	public String getLevelType() {
		return levelType;
	}

	/**
	 * Sets the conditionCode.
	 * 
	 * @param conditionCode
	 *            The conditionCode to set
	 */
	public void setConditionCode(String conditionCode) {
		this.conditionCode = conditionCode;
	}

	/**
	 * Sets the conditionDesc.
	 * 
	 * @param conditionDesc
	 *            The conditionDesc to set
	 */
	public void setConditionDesc(String conditionDesc) {
		this.conditionDesc = conditionDesc;
	}

	/**
	 * Sets the conditionId.
	 * 
	 * @param conditionId
	 *            The conditionId to set
	 */
	public void setConditionId(String conditionId) {
		this.conditionId = conditionId;
	}

	/**
	 * Sets the conditionType.
	 * 
	 * @param conditionType
	 *            The conditionType to set
	 */
	public void setConditionType(String conditionType) {
		this.conditionType = conditionType;
	}

	/**
	 * Sets the creationDate.
	 * 
	 * @param creationDate
	 *            The creationDate to set
	 */
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Sets the levelType.
	 * 
	 * @param levelType
	 *            The levelType to set
	 */
	public void setLevelType(String levelType) {
		this.levelType = levelType;
	}

	/**
	 * Returns the shortText.
	 * 
	 * @return String
	 */
	public String getShortText() {
		return shortText;
	}

	/**
	 * Sets the shortText.
	 * 
	 * @param shortText
	 *            The shortText to set
	 */
	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	/**
	 * Returns the conditions.
	 * 
	 * @return List
	 */
	public List getConditions() {
		return conditions;
	}

	/**
	 * Returns the displayConditions.
	 * 
	 * @return String
	 */
	public String getDisplayConditions() {
		return displayConditions;
	}

	/**
	 * Sets the conditions.
	 * 
	 * @param conditions
	 *            The conditions to set
	 */
	public void setConditions(List conditions) {
		this.conditions = conditions;
	}

	/**
	 * Sets the displayConditions.
	 * 
	 * @param displayConditions
	 *            The displayConditions to set
	 */
	public void setDisplayConditions(String displayConditions) {
		this.displayConditions = displayConditions;
	}

	/**
	 * Returns the inspectability.
	 * 
	 * @return String
	 */
	public String getInspectability() {
		return inspectability;
	}

	/**
	 * Returns the required.
	 * 
	 * @return String
	 */
	public String getRequired() {
		return required;
	}

	/**
	 * Returns the warning.
	 * 
	 * @return String
	 */
	public String getWarning() {
		return warning;
	}

	/**
	 * Sets the inspectability.
	 * 
	 * @param inspectability
	 *            The inspectability to set
	 */
	public void setInspectability(String inspectability) {
		this.inspectability = inspectability;
	}

	/**
	 * Sets the required.
	 * 
	 * @param required
	 *            The required to set
	 */
	public void setRequired(String required) {
		this.required = required;
	}

	/**
	 * Sets the warning.
	 * 
	 * @param warning
	 *            The warning to set
	 */
	public void setWarning(String warning) {
		this.warning = warning;
	}

	/**
	 * Returns the selectedItems.
	 * 
	 * @return String[]
	 */
	public String[] getSelectedItems() {
		return selectedItems;
	}

	/**
	 * Sets the selectedItems.
	 * 
	 * @param selectedItems
	 *            The selectedItems to set
	 */
	public void setSelectedItems(String[] selectedItems) {
		this.selectedItems = selectedItems;
	}

	/**
	 * @return Returns the checkDate.
	 */
	public String getCheckDate() {
		return checkDate;
	}

	/**
	 * @param checkDate
	 *            The checkDate to set.
	 */
	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate;
	}
}
