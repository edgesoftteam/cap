package elms.control.beans.admin;

import java.util.List;

import org.apache.struts.action.ActionForm;

import elms.app.lso.LsoUse;

/**
 * @author Akshay
 */

public class AdminComboMappingForm extends ActionForm {

	// Declare the variable and object declarations used by the class here
	private int mapId;
	private String subProjName;
	private String actType;
	private String actName;
	private String deptCode;
	private String displayComboList;
	private List comboList;
	private String actCode;
	private String[] selectedId;
	private String isActive;
	private int stypeId;
	private int stypeSubId;
	private boolean isPcReq = false;

	private String isMapped;

	// added for admin combo mapping online use

	private String forcePC = "N";
	private String forcePCTemp = "N";
	private String online = "N";
	private String approval = "N";

	// getting list of LsoUse
	private LsoUse lsoUse;
	private String lsoUseMap;
	private String useIdArray;

	private String startsAfter = "0";

	// To add Plan check to individual activity in Admin Combo Permiting -added by Manjuprasad
	private String pcReq;

	/**
	 * @return Returns the selectedId.
	 */
	public String[] getSelectedId() {
		return selectedId;
	}

	/**
	 * @param selectedId
	 *            The selectedId to set.
	 */
	public void setSelectedId(String[] selectedId) {
		this.selectedId = selectedId;
	}

	/**
	 * @return Returns the actName.
	 */
	public String getActName() {
		return actName;
	}

	/**
	 * @param actName
	 *            The actName to set.
	 */
	public void setActName(String actName) {
		this.actName = actName;
	}

	/**
	 * @return Returns the actType.
	 */
	public String getActType() {
		return actType;
	}

	/**
	 * @param actType
	 *            The actType to set.
	 */
	public void setActType(String actType) {
		this.actType = actType;
	}

	/**
	 * @return Returns the deptCode.
	 */
	public String getDeptCode() {
		return deptCode;
	}

	/**
	 * @param deptCode
	 *            The deptCode to set.
	 */
	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}

	/**
	 * @return Returns the displayComboList.
	 */
	public String getDisplayComboList() {
		return displayComboList;
	}

	/**
	 * @param displayComboList
	 *            The displayComboList to set.
	 */
	public void setDisplayComboList(String displayComboList) {
		this.displayComboList = displayComboList;
	}

	/**
	 * @return Returns the mapId.
	 */
	public int getMapId() {
		return mapId;
	}

	/**
	 * @param mapId
	 *            The mapId to set.
	 */
	public void setMapId(int mapId) {
		this.mapId = mapId;
	}

	/**
	 * @return Returns the subProjName.
	 */
	public String getSubProjName() {
		return subProjName;
	}

	/**
	 * @param subProjName
	 *            The subProjName to set.
	 */
	public void setSubProjName(String subProjName) {
		this.subProjName = subProjName;
	}

	/**
	 * Returns the comboList.
	 * 
	 * @return List
	 */
	public List getComboList() {

		return comboList;
	}

	/**
	 * Sets the comboList.
	 * 
	 * @param comboList
	 *            The comboList to set
	 */
	public void setComboList(List comboList) {

		this.comboList = comboList;
	}

	/**
	 * @return Returns the actCode.
	 */
	public String getActCode() {
		return actCode;
	}

	/**
	 * @param actCode
	 *            The actCode to set.
	 */
	public void setActCode(String actCode) {
		this.actCode = actCode;
	}

	/**
	 * @return Returns the isActive.
	 */
	public String getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive
	 *            The isActive to set.
	 */
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return Returns the stypeId.
	 */
	public int getStypeId() {
		return stypeId;
	}

	/**
	 * @param stypeId
	 *            The stypeId to set.
	 */
	public void setStypeId(int stypeId) {
		this.stypeId = stypeId;
	}

	/**
	 * @return Returns the stypeSubId.
	 */
	public int getStypeSubId() {
		return stypeSubId;
	}

	/**
	 * @param stypeSubId
	 *            The stypeSubId to set.
	 */
	public void setStypeSubId(int stypeSubId) {
		this.stypeSubId = stypeSubId;
	}

	/**
	 * @return Returns the isPcReq.
	 */
	public boolean getIsPcReq() {
		return isPcReq;
	}

	/**
	 * @param isPcReq
	 *            The isPcReq to set.
	 */
	public void setIsPcReq(boolean isPcReq) {
		this.isPcReq = isPcReq;
	}

	/**
	 * @return Returns the isMapped.
	 */
	public String getIsMapped() {
		return isMapped;
	}

	/**
	 * @param isMapped
	 *            The isMapped to set.
	 */
	public void setIsMapped(String isMapped) {
		this.isMapped = isMapped;
	}

	/**
	 * @return
	 */
	public String getPcReq() {
		return pcReq;
	}

	/**
	 * @param string
	 */
	public void setPcReq(String string) {
		pcReq = string;
	}

	/**
	 * @return
	 */
	public LsoUse getLsoUse() {
		return lsoUse;
	}

	/**
	 * @param use
	 */
	public void setLsoUse(LsoUse use) {
		lsoUse = use;
	}

	/**
	 * @return
	 */
	public String getLsoUseMap() {
		return lsoUseMap;
	}

	/**
	 * @param string
	 */
	public void setLsoUseMap(String string) {
		lsoUseMap = string;
	}

	/**
	 * @return
	 */
	public String getUseIdArray() {
		return useIdArray;
	}

	/**
	 * @param string
	 */
	public void setUseIdArray(String string) {
		useIdArray = string;
	}

	/**
	 * @return
	 */
	public String getForcePC() {
		return forcePC;
	}

	/**
	 * @param string
	 */
	public void setForcePC(String string) {
		forcePC = string;
	}

	/**
	 * @return
	 */
	public String getStartsAfter() {
		return startsAfter;
	}

	/**
	 * @param string
	 */
	public void setStartsAfter(String string) {
		startsAfter = string;
	}

	/**
	 * @return
	 */
	public String getApproval() {
		return approval;
	}

	/**
	 * @return
	 */
	public String getForcePCTemp() {
		return forcePCTemp;
	}

	/**
	 * @return
	 */
	public String getOnline() {
		return online;
	}

	/**
	 * @param string
	 */
	public void setApproval(String string) {
		approval = string;
	}

	/**
	 * @param string
	 */
	public void setForcePCTemp(String string) {
		forcePCTemp = string;
	}

	/**
	 * @param string
	 */
	public void setOnline(String string) {
		online = string;
	}

}
