package elms.control.beans.admin;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author Manju
 */
public class NoticeTemplateAdminForm extends ActionForm {
	
	
	protected int id;
	
	protected String noticeTemplateName;
	protected String name;
	
	protected int actionId;
	protected String action;
	
	protected int noticeTemplateId;
	protected int noticeTypeId;
	protected String noticeTemplate;
	
	
	protected String[] selectedItems = {};
	protected String peopleManager;
	protected String activityTeam;
	protected String condtion;
	
	protected String message;   
	
	protected int fieldId;
	protected String fieldName;
	protected String fieldDescription;

	public String getNoticeTemplate() {
		return noticeTemplate;
	}

	public void setNoticeTemplate(String noticeTemplate) {
		this.noticeTemplate = noticeTemplate;
	}

	public String getCondtion() {
		return condtion;
	}

	public void setCondtion(String condtion) {
		this.condtion = condtion;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	

	public String getNoticeTemplateName() {
		return noticeTemplateName;
	}

	public void setNoticeTemplateName(String noticeTemplateName) {
		this.noticeTemplateName = noticeTemplateName;
	}

	public String getPeopleManager() {
		return peopleManager;
	}

	public void setPeopleManager(String peopleManager) {
		this.peopleManager = peopleManager;
	}

	public String getActivityTeam() {
		return activityTeam;
	}

	public void setActivityTeam(String activityTeam) {
		this.activityTeam = activityTeam;
	}

	public String[] getSelectedItems() {
		return selectedItems;
	}

	public void setSelectedItems(String[] selectedItems) {
		this.selectedItems = selectedItems;
	}
	
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		return errors;
	}

	public int getActionId() {
		return actionId;
	}

	public void setActionId(int actionId) {
		this.actionId = actionId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public int getNoticeTemplateId() {
		return noticeTemplateId;
	}

	public void setNoticeTemplateId(int noticeTemplateId) {
		this.noticeTemplateId = noticeTemplateId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNoticeTypeId() {
		return noticeTypeId;
	}

	public void setNoticeTypeId(int noticeTypeId) {
		this.noticeTypeId = noticeTypeId;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFieldDescription() {
		return fieldDescription;
	}

	public void setFieldDescription(String fieldDescription) {
		this.fieldDescription = fieldDescription;
	}

	public int getFieldId() {
		return fieldId;
	}

	public void setFieldId(int fieldId) {
		this.fieldId = fieldId;
	}
	
	
			
}