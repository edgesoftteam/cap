package elms.control.beans.admin;

import java.util.List;

import org.apache.struts.action.ActionForm;

/**
 * @author Sunil
 */
public class CustomLabelsForm extends ActionForm {

	private static long serialVersionUID = 1L;

	private int fieldId;
	private String fieldLabel;
	private String fieldDesc;
	private String fieldType;
	private int levelId;
	private String levelType;
	private int levelTypeId;

	private String fieldRequired;
	private String levelTypeDesc;

	private List comboList;
	private String displayComboList;
	protected String[] selectedActItem;

	private String levelStat;
	private String levelCode;
	private String levelName;
	private String strFieldIds;
	private String message;
	private String deleteDisplay;
	private String sequence;
	private String dropDownOption;
	private int levelSubTypeId; 

	public int getLevelSubTypeId() {
		return levelSubTypeId;
	}

	public void setLevelSubTypeId(int levelSubTypeId) {
		this.levelSubTypeId = levelSubTypeId;
	}


	public String getDropDownOption() {
		return dropDownOption;
	}

	public void setDropDownOption(String dropDownOption) {
		this.dropDownOption = dropDownOption;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public static void setSerialVersionUID(long serialVersionUID) {
		CustomLabelsForm.serialVersionUID = serialVersionUID;
	}

	public int getFieldId() {
		return fieldId;
	}

	public void setFieldId(int fieldId) {
		this.fieldId = fieldId;
	}

	public String getFieldLabel() {
		return fieldLabel;
	}

	public void setFieldLabel(String fieldLabel) {
		this.fieldLabel = fieldLabel;
	}

	public String getFieldDesc() {
		return fieldDesc;
	}

	public void setFieldDesc(String fieldDesc) {
		this.fieldDesc = fieldDesc;
	}

	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	public int getLevelId() {
		return levelId;
	}

	public void setLevelId(int levelId) {
		this.levelId = levelId;
	}

	public String getLevelType() {
		return levelType;
	}

	public void setLevelType(String levelType) {
		this.levelType = levelType;
	}

	public List getComboList() {
		return comboList;
	}

	public void setComboList(List comboList) {
		this.comboList = comboList;
	}

	public String getDisplayComboList() {
		return displayComboList;
	}

	public void setDisplayComboList(String displayComboList) {
		this.displayComboList = displayComboList;
	}

	public String[] getSelectedActItem() {
		return selectedActItem;
	}

	public void setSelectedActItem(String[] selectedActItem) {
		this.selectedActItem = selectedActItem;
	}

	public int getLevelTypeId() {
		return levelTypeId;
	}

	public void setLevelTypeId(int levelTypeId) {
		this.levelTypeId = levelTypeId;
	}

	public String getLevelTypeDesc() {
		return levelTypeDesc;
	}

	public void setLevelTypeDesc(String levelTypeDesc) {
		this.levelTypeDesc = levelTypeDesc;
	}

	public String getFieldRequired() {
		return fieldRequired;
	}

	public void setFieldRequired(String fieldRequired) {
		this.fieldRequired = fieldRequired;
	}

	public String getLevelStat() {
		return levelStat;
	}

	public void setLevelStat(String levelStat) {
		this.levelStat = levelStat;
	}

	public String getLevelCode() {
		return levelCode;
	}

	public void setLevelCode(String levelCode) {
		this.levelCode = levelCode;
	}

	public String getLevelName() {
		return levelName;
	}

	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	public String getStrFieldIds() {
		return strFieldIds;
	}

	public void setStrFieldIds(String strFieldIds) {
		this.strFieldIds = strFieldIds;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDeleteDisplay() {
		return deleteDisplay;
	}

	public void setDeleteDisplay(String deleteDisplay) {
		this.deleteDisplay = deleteDisplay;
	}
	
	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

}