package elms.control.beans;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class PeopleContractorForm extends ActionForm {

	/**
	 * Member variable declaration
	 */

	protected String workersCompWaiverFlag;
	protected String businessLicenseExpirationDate;
	protected String businessLicenseNo;
	protected String type;
	protected String address;
	protected String state;
	protected String generalLiabilityDate;
	protected String phone;
	protected String licenseNo;
	protected String copyApplicant;
	protected String licenseExpirationDate;
	protected String autoLiabilityDate;
	protected String workersCompExpirationDate;
	protected String email;
	protected String city;
	protected String additionalComments;
	protected String zip;
	protected String fax;
	protected String name;
	protected String placeOnHold;
	protected String dateOfHold;
	protected String reasonForHold;

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		workersCompWaiverFlag = null;
		businessLicenseExpirationDate = null;
		businessLicenseNo = null;
		type = null;
		address = null;
		state = null;
		generalLiabilityDate = null;
		phone = null;
		licenseNo = null;
		copyApplicant = null;
		licenseExpirationDate = null;
		autoLiabilityDate = null;
		workersCompExpirationDate = null;
		email = null;
		city = null;
		additionalComments = null;
		zip = null;
		fax = null;
		name = null;
		placeOnHold = null;
		dateOfHold = null;
		reasonForHold = null;

	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		// delcare validations for the form
		/*
		 * if ((firstName == null) || (firstName.length() < 1)) errors.add("firstName",new ActionError("error.firstname.required"));
		 * 
		 * if ((lastName == null) || (lastName.length() < 1)) errors.add("lastName",new ActionError("error.lastname.required"));
		 */
		return errors;
	}

	/**
	 * Gets the workersCompWaiverFlag
	 * 
	 * @return Returns a String
	 */
	public String getWorkersCompWaiverFlag() {
		return workersCompWaiverFlag;
	}

	/**
	 * Sets the workersCompWaiverFlag
	 * 
	 * @param workersCompWaiverFlag
	 *            The workersCompWaiverFlag to set
	 */
	public void setWorkersCompWaiverFlag(String workersCompWaiverFlag) {
		this.workersCompWaiverFlag = workersCompWaiverFlag;
	}

	/**
	 * Gets the businessLicenseExpirationDate
	 * 
	 * @return Returns a String
	 */
	public String getBusinessLicenseExpirationDate() {
		return businessLicenseExpirationDate;
	}

	/**
	 * Sets the businessLicenseExpirationDate
	 * 
	 * @param businessLicenseExpirationDate
	 *            The businessLicenseExpirationDate to set
	 */
	public void setBusinessLicenseExpirationDate(String businessLicenseExpirationDate) {
		this.businessLicenseExpirationDate = businessLicenseExpirationDate;
	}

	/**
	 * Gets the businessLicenseNo
	 * 
	 * @return Returns a String
	 */
	public String getBusinessLicenseNo() {
		return businessLicenseNo;
	}

	/**
	 * Sets the businessLicenseNo
	 * 
	 * @param businessLicenseNo
	 *            The businessLicenseNo to set
	 */
	public void setBusinessLicenseNo(String businessLicenseNo) {
		this.businessLicenseNo = businessLicenseNo;
	}

	/**
	 * Gets the type
	 * 
	 * @return Returns a String
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type
	 * 
	 * @param type
	 *            The type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the address
	 * 
	 * @return Returns a String
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the address
	 * 
	 * @param address
	 *            The address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Gets the state
	 * 
	 * @return Returns a String
	 */
	public String getState() {
		return state;
	}

	/**
	 * Sets the state
	 * 
	 * @param state
	 *            The state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * Gets the generalLiabilityDate
	 * 
	 * @return Returns a String
	 */
	public String getGeneralLiabilityDate() {
		return generalLiabilityDate;
	}

	/**
	 * Sets the generalLiabilityDate
	 * 
	 * @param generalLiabilityDate
	 *            The generalLiabilityDate to set
	 */
	public void setGeneralLiabilityDate(String generalLiabilityDate) {
		this.generalLiabilityDate = generalLiabilityDate;
	}

	/**
	 * Gets the phone
	 * 
	 * @return Returns a String
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * Sets the phone
	 * 
	 * @param phone
	 *            The phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * Gets the licenseNo
	 * 
	 * @return Returns a String
	 */
	public String getLicenseNo() {
		return licenseNo;
	}

	/**
	 * Sets the licenseNo
	 * 
	 * @param licenseNo
	 *            The licenseNo to set
	 */
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}

	/**
	 * Gets the copyApplicant
	 * 
	 * @return Returns a String
	 */
	public String getCopyApplicant() {
		return copyApplicant;
	}

	/**
	 * Sets the copyApplicant
	 * 
	 * @param copyApplicant
	 *            The copyApplicant to set
	 */
	public void setCopyApplicant(String copyApplicant) {
		this.copyApplicant = copyApplicant;
	}

	/**
	 * Gets the licenseExpirationDate
	 * 
	 * @return Returns a String
	 */
	public String getLicenseExpirationDate() {
		return licenseExpirationDate;
	}

	/**
	 * Sets the licenseExpirationDate
	 * 
	 * @param licenseExpirationDate
	 *            The licenseExpirationDate to set
	 */
	public void setLicenseExpirationDate(String licenseExpirationDate) {
		this.licenseExpirationDate = licenseExpirationDate;
	}

	/**
	 * Gets the autoLiabilityDate
	 * 
	 * @return Returns a String
	 */
	public String getAutoLiabilityDate() {
		return autoLiabilityDate;
	}

	/**
	 * Sets the autoLiabilityDate
	 * 
	 * @param autoLiabilityDate
	 *            The autoLiabilityDate to set
	 */
	public void setAutoLiabilityDate(String autoLiabilityDate) {
		this.autoLiabilityDate = autoLiabilityDate;
	}

	/**
	 * Gets the workersCompExpirationDate
	 * 
	 * @return Returns a String
	 */
	public String getWorkersCompExpirationDate() {
		return workersCompExpirationDate;
	}

	/**
	 * Sets the workersCompExpirationDate
	 * 
	 * @param workersCompExpirationDate
	 *            The workersCompExpirationDate to set
	 */
	public void setWorkersCompExpirationDate(String workersCompExpirationDate) {
		this.workersCompExpirationDate = workersCompExpirationDate;
	}

	/**
	 * Gets the email
	 * 
	 * @return Returns a String
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email
	 * 
	 * @param email
	 *            The email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the city
	 * 
	 * @return Returns a String
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the city
	 * 
	 * @param city
	 *            The city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Gets the additionalComments
	 * 
	 * @return Returns a String
	 */
	public String getAdditionalComments() {
		return additionalComments;
	}

	/**
	 * Sets the additionalComments
	 * 
	 * @param additionalComments
	 *            The additionalComments to set
	 */
	public void setAdditionalComments(String additionalComments) {
		this.additionalComments = additionalComments;
	}

	/**
	 * Gets the zip
	 * 
	 * @return Returns a String
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * Sets the zip
	 * 
	 * @param zip
	 *            The zip to set
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * Gets the fax
	 * 
	 * @return Returns a String
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * Sets the fax
	 * 
	 * @param fax
	 *            The fax to set
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * Gets the name
	 * 
	 * @return Returns a String
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name
	 * 
	 * @param name
	 *            The name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the placeOnHold
	 * 
	 * @return Returns a String
	 */
	public String getPlaceOnHold() {
		return placeOnHold;
	}

	/**
	 * Sets the placeOnHold
	 * 
	 * @param placeOnHold
	 *            The placeOnHold to set
	 */
	public void setPlaceOnHold(String placeOnHold) {
		this.placeOnHold = placeOnHold;
	}

	/**
	 * Gets the dateOfHold
	 * 
	 * @return Returns a String
	 */
	public String getDateOfHold() {
		return dateOfHold;
	}

	/**
	 * Sets the dateOfHold
	 * 
	 * @param dateOfHold
	 *            The dateOfHold to set
	 */
	public void setDateOfHold(String dateOfHold) {
		this.dateOfHold = dateOfHold;
	}

	/**
	 * Gets the reasonForHold
	 * 
	 * @return Returns a String
	 */
	public String getReasonForHold() {
		return reasonForHold;
	}

	/**
	 * Sets the reasonForHold
	 * 
	 * @param reasonForHold
	 *            The reasonForHold to set
	 */
	public void setReasonForHold(String reasonForHold) {
		this.reasonForHold = reasonForHold;
	}

} // End class
