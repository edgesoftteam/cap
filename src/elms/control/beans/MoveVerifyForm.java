package elms.control.beans;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author Shekhar Jain
 */
public class MoveVerifyForm extends ActionForm {

	static Logger logger = Logger.getLogger(MoveVerifyForm.class.getName());
	// Declare the variable and object declarations used by the class here

	// For Land
	protected String lsoNbr;
	protected String lsoAlias;
	protected String lsoDescription;
	protected String lsoAddressOne;
	protected String lsoAddressTwo;

	// For Structure- there may be multple structures.
	protected List moveStructureDetails;
	protected String moveId;

	// For Occupancy
	protected String toId;
	protected String toAlias;
	protected String toDescription;
	protected String toAddressOne;
	protected String toAddressTwo;

	// For Project
	protected String projectNbr;
	protected String projectName;
	protected String projectDescription;
	protected String addressOne;
	protected String addressTwo;

	// For Sub Project
	protected String subProjectId;
	protected String subProjectNbr;
	protected String subProjectName;
	protected String subProjectDescription;
	protected String subProjectType;
	protected String subProjectSubType;

	// For Activity
	protected String activityId;
	protected String activityNbr;
	protected String activityType;
	protected String activityDescription;
	protected String amtDue;
	protected String amtPaid;

	protected String message;
	protected String moveType;

	public void reset(ActionMapping mapping, HttpServletRequest request) {

	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		return errors;
	}

	/**
	 * @return
	 */
	public String getActivityDescription() {
		return activityDescription;
	}

	/**
	 * @return
	 */
	public String getActivityId() {
		return activityId;
	}

	/**
	 * @return
	 */
	public String getActivityNbr() {
		return activityNbr;
	}

	/**
	 * @return
	 */
	public String getActivityType() {
		return activityType;
	}

	/**
	 * @return
	 */
	public String getAddressOne() {
		return addressOne;
	}

	/**
	 * @return
	 */
	public String getAddressTwo() {
		return addressTwo;
	}

	/**
	 * @return
	 */
	public String getAmtDue() {
		return amtDue;
	}

	/**
	 * @return
	 */
	public String getAmtPaid() {
		return amtPaid;
	}

	/**
	 * @return
	 */
	public String getLsoAddressOne() {
		return lsoAddressOne;
	}

	/**
	 * @return
	 */
	public String getLsoAddressTwo() {
		return lsoAddressTwo;
	}

	/**
	 * @return
	 */
	public String getLsoAlias() {
		return lsoAlias;
	}

	/**
	 * @return
	 */
	public String getLsoDescription() {
		return lsoDescription;
	}

	/**
	 * @return
	 */
	public String getLsoNbr() {
		return lsoNbr;
	}

	/**
	 * @return
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @return
	 */
	public String getProjectDescription() {
		return projectDescription;
	}

	/**
	 * @return
	 */
	public String getProjectName() {
		return projectName;
	}

	/**
	 * @return
	 */
	public String getProjectNbr() {
		return projectNbr;
	}

	/**
	 * @return
	 */
	public String getSubProjectDescription() {
		return subProjectDescription;
	}

	/**
	 * @return
	 */
	public String getSubProjectId() {
		return subProjectId;
	}

	/**
	 * @return
	 */
	public String getSubProjectName() {
		return subProjectName;
	}

	/**
	 * @return
	 */
	public String getSubProjectNbr() {
		return subProjectNbr;
	}

	/**
	 * @return
	 */
	public String getSubProjectSubType() {
		return subProjectSubType;
	}

	/**
	 * @return
	 */
	public String getSubProjectType() {
		return subProjectType;
	}

	/**
	 * @return
	 */
	public String getToAddressOne() {
		return toAddressOne;
	}

	/**
	 * @return
	 */
	public String getToAddressTwo() {
		return toAddressTwo;
	}

	/**
	 * @return
	 */
	public String getToAlias() {
		return toAlias;
	}

	/**
	 * @return
	 */
	public String getToDescription() {
		return toDescription;
	}

	/**
	 * @return
	 */
	public String getToId() {
		return toId;
	}

	/**
	 * @param string
	 */
	public void setActivityDescription(String string) {
		activityDescription = string;
	}

	/**
	 * @param string
	 */
	public void setActivityId(String string) {
		activityId = string;
	}

	/**
	 * @param string
	 */
	public void setActivityNbr(String string) {
		activityNbr = string;
	}

	/**
	 * @param string
	 */
	public void setActivityType(String string) {
		activityType = string;
	}

	/**
	 * @param string
	 */
	public void setAddressOne(String string) {
		addressOne = string;
	}

	/**
	 * @param string
	 */
	public void setAddressTwo(String string) {
		addressTwo = string;
	}

	/**
	 * @param string
	 */
	public void setAmtDue(String string) {
		amtDue = string;
	}

	/**
	 * @param string
	 */
	public void setAmtPaid(String string) {
		amtPaid = string;
	}

	/**
	 * @param string
	 */
	public void setLsoAddressOne(String string) {
		lsoAddressOne = string;
	}

	/**
	 * @param string
	 */
	public void setLsoAddressTwo(String string) {
		lsoAddressTwo = string;
	}

	/**
	 * @param string
	 */
	public void setLsoAlias(String string) {
		lsoAlias = string;
	}

	/**
	 * @param string
	 */
	public void setLsoDescription(String string) {
		lsoDescription = string;
	}

	/**
	 * @param string
	 */
	public void setLsoNbr(String string) {
		lsoNbr = string;
	}

	/**
	 * @param string
	 */
	public void setMessage(String string) {
		message = string;
	}

	/**
	 * @param string
	 */
	public void setProjectDescription(String string) {
		projectDescription = string;
	}

	/**
	 * @param string
	 */
	public void setProjectName(String string) {
		projectName = string;
	}

	/**
	 * @param string
	 */
	public void setProjectNbr(String string) {
		projectNbr = string;
	}

	/**
	 * @param string
	 */
	public void setSubProjectDescription(String string) {
		subProjectDescription = string;
	}

	/**
	 * @param string
	 */
	public void setSubProjectId(String string) {
		subProjectId = string;
	}

	/**
	 * @param string
	 */
	public void setSubProjectName(String string) {
		subProjectName = string;
	}

	/**
	 * @param string
	 */
	public void setSubProjectNbr(String string) {
		subProjectNbr = string;
	}

	/**
	 * @param string
	 */
	public void setSubProjectSubType(String string) {
		subProjectSubType = string;
	}

	/**
	 * @param string
	 */
	public void setSubProjectType(String string) {
		subProjectType = string;
	}

	/**
	 * @param string
	 */
	public void setToAddressOne(String string) {
		toAddressOne = string;
	}

	/**
	 * @param string
	 */
	public void setToAddressTwo(String string) {
		toAddressTwo = string;
	}

	/**
	 * @param string
	 */
	public void setToAlias(String string) {
		toAlias = string;
	}

	/**
	 * @param string
	 */
	public void setToDescription(String string) {
		toDescription = string;
	}

	/**
	 * @param string
	 */
	public void setToId(String string) {
		toId = string;
	}

	/**
	 * @return
	 */
	public String getMoveType() {
		return moveType;
	}

	/**
	 * @param string
	 */
	public void setMoveType(String string) {
		moveType = string;
	}

	/**
	 * @return
	 */
	public List getMoveStructureDetails() {
		return moveStructureDetails;
	}

	/**
	 * @param list
	 */
	public void setMoveStructureDetails(List list) {
		moveStructureDetails = list;
	}

	/**
	 * @return
	 */
	public String getMoveId() {
		return moveId;
	}

	/**
	 * @param string
	 */
	public void setMoveId(String string) {
		moveId = string;
	}

}
