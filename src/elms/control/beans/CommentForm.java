package elms.control.beans;

import java.util.List;

import org.apache.struts.action.ActionForm;

/**
 * @author Shekhar Jain
 */
public class CommentForm extends ActionForm {

	protected int commentId;
	protected int levelId;
	protected String commentLevel;
	protected String comments;
	protected String internal;
	protected int createdBy;
	protected String createdUserName;
	protected String creationDate;
	protected int updatedBy;
	protected String updatedUserName;
	protected String updatedDate;
	protected String[] selectedComments;
	protected List commentsList;
	protected String uptSubProject;
	protected String uptActivities;

	/**
	 * Gets the commentLevel
	 * 
	 * @return Returns a String
	 */
	public String getCommentLevel() {
		return commentLevel;
	}

	/**
	 * Sets the commentLevel
	 * 
	 * @param commentLevel
	 *            The commentLevel to set
	 */
	public void setCommentLevel(String commentLevel) {
		this.commentLevel = commentLevel;
	}

	/**
	 * Gets the comments
	 * 
	 * @return Returns a String
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * Sets the comments
	 * 
	 * @param comments
	 *            The comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * Gets the internal
	 * 
	 * @return Returns a String
	 */
	public String getInternal() {
		return internal;
	}

	/**
	 * Sets the internal
	 * 
	 * @param internal
	 *            The internal to set
	 */
	public void setInternal(String internal) {
		this.internal = internal;
	}

	/**
	 * Gets the createdBy
	 * 
	 * @return Returns a int
	 */
	public int getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the createdBy
	 * 
	 * @param createdBy
	 *            The createdBy to set
	 */
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Gets the updatedBy
	 * 
	 * @return Returns a int
	 */
	public int getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Sets the updatedBy
	 * 
	 * @param updatedBy
	 *            The updatedBy to set
	 */
	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Gets the commentId
	 * 
	 * @return Returns a int
	 */
	public int getCommentId() {
		return commentId;
	}

	/**
	 * Sets the commentId
	 * 
	 * @param commentId
	 *            The commentId to set
	 */
	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}

	/**
	 * Gets the levelId
	 * 
	 * @return Returns a int
	 */
	public int getLevelId() {
		return levelId;
	}

	/**
	 * Sets the levelId
	 * 
	 * @param levelId
	 *            The levelId to set
	 */
	public void setLevelId(int levelId) {
		this.levelId = levelId;
	}

	/**
	 * Gets the creationDate
	 * 
	 * @return Returns a String
	 */
	public String getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the creationDate
	 * 
	 * @param creationDate
	 *            The creationDate to set
	 */
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Gets the updatedDate
	 * 
	 * @return Returns a String
	 */
	public String getUpdatedDate() {
		return updatedDate;
	}

	/**
	 * Sets the updatedDate
	 * 
	 * @param updatedDate
	 *            The updatedDate to set
	 */
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	/**
	 * @return
	 */
	public String[] getSelectedComments() {
		return selectedComments;
	}

	/**
	 * @param strings
	 */
	public void setSelectedComments(String[] strings) {
		selectedComments = strings;
	}

	/**
	 * @return
	 */
	public List getCommentsList() {
		return commentsList;
	}

	/**
	 * @param list
	 */
	public void setCommentsList(List list) {
		commentsList = list;
	}

	/**
	 * @return
	 */
	public String getCreatedUserName() {
		return createdUserName;
	}

	/**
	 * @param string
	 */
	public void setCreatedUserName(String string) {
		createdUserName = string;
	}

	/**
	 * @return
	 */
	public String getUpdatedUserName() {
		return updatedUserName;
	}

	/**
	 * @param string
	 */
	public void setUpdatedUserName(String string) {
		updatedUserName = string;
	}

	/**
	 * @return
	 */
	public String getUptActivities() {
		return uptActivities;
	}

	/**
	 * @return
	 */
	public String getUptSubProject() {
		return uptSubProject;
	}

	/**
	 * @param string
	 */
	public void setUptActivities(String string) {
		uptActivities = string;
	}

	/**
	 * @param string
	 */
	public void setUptSubProject(String string) {
		uptSubProject = string;
	}

}
