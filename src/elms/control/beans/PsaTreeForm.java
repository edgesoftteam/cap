package elms.control.beans;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class PsaTreeForm extends ActionForm {

	protected String radiobutton;
	protected String lsoId;

	public PsaTreeForm() {
		radiobutton = "";
		lsoId = "";
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		// nothing to be done
	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		return errors;
	}

	/**
	 * Gets the radiobutton
	 * 
	 * @return Returns a String
	 */
	public String getRadiobutton() {
		return radiobutton;
	}

	/**
	 * Sets the radiobutton
	 * 
	 * @param radiobutton
	 *            The radiobutton to set
	 */
	public void setRadiobutton(String radiobutton) {
		this.radiobutton = radiobutton;
	}

	/**
	 * Gets the lsoId
	 * 
	 * @return Returns a String
	 */
	public String getLsoId() {
		return lsoId;
	}

	/**
	 * Sets the lsoId
	 * 
	 * @param lsoId
	 *            The lsoId to set
	 */
	public void setLsoId(String lsoId) {
		this.lsoId = lsoId;
	}

}
