//Source file: C:\\Datafile\\OBCSource\\elms\\control\\beans\\EditHoldForm.java

package elms.control.beans;

import org.apache.struts.action.ActionForm;

/**
 * @author Shekhar Jain
 */
public class EditHoldForm extends ActionForm {
	protected String date;
	protected String status;
	protected String comments;
	protected String statusDate;
	protected String issuer;
	protected String type;

	/**
	 * Access method for the date property.
	 * 
	 * @return the current value of the date property
	 */
	public String getDate() {
		return date;
	}

	/**
	 * Sets the value of the date property.
	 * 
	 * @param aDate
	 *            the new value of the date property
	 */
	public void setDate(String aDate) {
		date = aDate;
	}

	/**
	 * Access method for the status property.
	 * 
	 * @return the current value of the status property
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the value of the status property.
	 * 
	 * @param aStatus
	 *            the new value of the status property
	 */
	public void setStatus(String aStatus) {
		status = aStatus;
	}

	/**
	 * Access method for the comments property.
	 * 
	 * @return the current value of the comments property
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * Sets the value of the comments property.
	 * 
	 * @param aComments
	 *            the new value of the comments property
	 */
	public void setComments(String aComments) {
		comments = aComments;
	}

	/**
	 * Access method for the statusDate property.
	 * 
	 * @return the current value of the statusDate property
	 */
	public String getStatusDate() {
		return statusDate;
	}

	/**
	 * Sets the value of the statusDate property.
	 * 
	 * @param aStatusDate
	 *            the new value of the statusDate property
	 */
	public void setStatusDate(String aStatusDate) {
		statusDate = aStatusDate;
	}

	/**
	 * Access method for the issuer property.
	 * 
	 * @return the current value of the issuer property
	 */
	public String getIssuer() {
		return issuer;
	}

	/**
	 * Sets the value of the issuer property.
	 * 
	 * @param aIssuer
	 *            the new value of the issuer property
	 */
	public void setIssuer(String aIssuer) {
		issuer = aIssuer;
	}

	/**
	 * Access method for the type property.
	 * 
	 * @return the current value of the type property
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the value of the type property.
	 * 
	 * @param aType
	 *            the new value of the type property
	 */
	public void setType(String aType) {
		type = aType;
	}
}
