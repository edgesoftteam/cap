/*
 * Created on Sep 29, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package elms.control.beans.enforcement;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import elms.app.enforcement.CodeEnforcement;
import elms.util.StringUtils;

/**
 * @author Shekhar
 * 
 *         To change the template for this generated type comment go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CodeEnforcementForm extends ActionForm {

	static Logger logger = Logger.getLogger(CodeEnforcementForm.class.getName());

	protected String action;
	protected long activityId;
	protected String scrollTo;

	protected List noticeList;
	protected List feeList;
	protected List resolutionList;
	protected List inspectionList;
	protected List peopleList;
	protected List complaintList;
	protected List complianceList;

	protected CodeEnforcement[] noticeArray;
	protected CodeEnforcement[] feeArray;
	protected CodeEnforcement[] resolutionArray;
	protected CodeEnforcement[] inspectionArray;
	protected CodeEnforcement[] peopleArray;
	protected CodeEnforcement[] complaintArray;
	protected CodeEnforcement[] complianceArray;

	protected List noticeTypeList;
	protected List feeTypeList;
	protected List resolutionTypeList;
	protected List inspectionTypeList;
	protected List peopleTypeList;
	protected List complaintTypeList;
	protected List complianceTypeList;

	protected List userList;
	protected List subTypeList;

	public CodeEnforcementForm() {
		noticeList = new ArrayList();
		feeList = new ArrayList();
		resolutionList = new ArrayList();
		inspectionList = new ArrayList();
		peopleList = new ArrayList();
		complaintList = new ArrayList();
		complianceList = new ArrayList();

		noticeTypeList = new ArrayList();
		feeTypeList = new ArrayList();
		resolutionTypeList = new ArrayList();
		inspectionTypeList = new ArrayList();
		peopleTypeList = new ArrayList();
		complaintTypeList = new ArrayList();
		complianceTypeList = new ArrayList();

	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
	}

	public void reset() {
		scrollTo = "";
		noticeList.clear();
		feeList.clear();
		resolutionList.clear();
		inspectionList.clear();
		peopleList.clear();
		complaintList.clear();
		complianceList.clear();

		noticeTypeList.clear();
		feeTypeList.clear();
		resolutionTypeList.clear();
		inspectionTypeList.clear();
		peopleTypeList.clear();
		complaintTypeList.clear();
		complianceTypeList.clear();

		noticeArray = (CodeEnforcement[]) noticeList.toArray(new CodeEnforcement[noticeList.size()]);
		feeArray = (CodeEnforcement[]) feeList.toArray(new CodeEnforcement[noticeList.size()]);
		resolutionArray = (CodeEnforcement[]) resolutionList.toArray(new CodeEnforcement[noticeList.size()]);
		inspectionArray = (CodeEnforcement[]) inspectionList.toArray(new CodeEnforcement[noticeList.size()]);
		peopleArray = (CodeEnforcement[]) peopleList.toArray(new CodeEnforcement[noticeList.size()]);
		complaintArray = (CodeEnforcement[]) complaintList.toArray(new CodeEnforcement[noticeList.size()]);
		complianceArray = (CodeEnforcement[]) complianceList.toArray(new CodeEnforcement[noticeList.size()]);
	}

	/**
	 * @return
	 */
	public String getAction() {
		return action;
	}

	/**
	 * @return
	 */
	public long getActivityId() {
		return activityId;
	}

	/**
	 * @return
	 */
	public List getComplaintList() {
		return complaintList;
	}

	/**
	 * @return
	 */
	public List getFeeList() {
		return feeList;
	}

	/**
	 * @return
	 */
	public List getInspectionList() {
		return inspectionList;
	}

	/**
	 * @return
	 */
	public List getNoticeList() {
		return noticeList;
	}

	/**
	 * @return
	 */
	public List getPeopleList() {
		return peopleList;
	}

	/**
	 * @return
	 */
	public List getResolutionList() {
		return resolutionList;
	}

	/**
	 * @return
	 */
	public List getComplianceList() {
		return complianceList;
	}

	/**
	 * @return
	 */
	public CodeEnforcement[] getComplaintArray() {
		return complaintArray;
	}

	/**
	 * @return
	 */
	public CodeEnforcement[] getFeeArray() {
		return feeArray;
	}

	/**
	 * @return
	 */
	public CodeEnforcement[] getInspectionArray() {
		return inspectionArray;
	}

	/**
	 * @return
	 */
	public CodeEnforcement[] getNoticeArray() {
		return noticeArray;
	}

	/**
	 * @return
	 */
	public CodeEnforcement[] getPeopleArray() {
		return peopleArray;
	}

	/**
	 * @return
	 */
	public CodeEnforcement[] getResolutionArray() {
		return resolutionArray;
	}

	/**
	 * @return
	 */
	public CodeEnforcement[] getComplianceArray() {
		return complianceArray;
	}

	/**
	 * @param string
	 */
	public void setAction(String string) {
		action = string;
	}

	/**
	 * @param l
	 */
	public void setActivityId(long l) {
		activityId = l;
	}

	/**
	 * @param list
	 */
	public void setComplaintList(List list) {
		complaintList = list;
		complaintArray = (CodeEnforcement[]) list.toArray();
	}

	/**
	 * @param list
	 */
	public void setFeeList(List list) {
		feeList = list;
		feeArray = (CodeEnforcement[]) list.toArray();
	}

	/**
	 * @param list
	 */
	public void setInspectionList(List list) {
		inspectionList = list;
		inspectionArray = (CodeEnforcement[]) list.toArray();
	}

	/**
	 * @param list
	 */
	public void setNoticeList(List list) {
		noticeList = list;
		noticeArray = (CodeEnforcement[]) list.toArray();
	}

	/**
	 * @param list
	 */
	public void setPeopleList(List list) {
		peopleList = list;
		peopleArray = (CodeEnforcement[]) list.toArray();
	}

	/**
	 * @param list
	 */
	public void setResolutionList(List list) {
		resolutionList = list;
		resolutionArray = (CodeEnforcement[]) list.toArray();
	}

	/**
	 * @param list
	 */
	public void setComplianceList(List list) {
		complianceList = list;
		complianceArray = (CodeEnforcement[]) list.toArray();
	}

	/**
	 * @param enforcements
	 */
	public void setComplaintArray(CodeEnforcement[] enforcements) {
		complaintArray = enforcements;
		complaintList = StringUtils.arrayToList(enforcements);
	}

	/**
	 * @param enforcements
	 */
	public void setFeeArray(CodeEnforcement[] enforcements) {
		feeArray = enforcements;
		feeList = StringUtils.arrayToList(enforcements);
	}

	/**
	 * @param enforcements
	 */
	public void setInspectionArray(CodeEnforcement[] enforcements) {
		inspectionArray = enforcements;
		inspectionList = StringUtils.arrayToList(enforcements);
	}

	/**
	 * @param enforcements
	 */
	public void setNoticeArray(CodeEnforcement[] enforcements) {
		noticeArray = enforcements;
		noticeList = StringUtils.arrayToList(noticeArray);
	}

	/**
	 * @param enforcements
	 */
	public void setPeopleArray(CodeEnforcement[] enforcements) {
		peopleArray = enforcements;
		peopleList = StringUtils.arrayToList(enforcements);
	}

	/**
	 * @param enforcements
	 */
	public void setResolutionArray(CodeEnforcement[] enforcements) {
		resolutionArray = enforcements;
		resolutionList = StringUtils.arrayToList(enforcements);
	}

	/**
	 * @param enforcements
	 */
	public void setComplianceArray(CodeEnforcement[] compliances) {
		complianceArray = compliances;
		complianceList = StringUtils.arrayToList(compliances);
	}

	/**
	 * @param list
	 */
	public void addComplaintList(Object o) {
		complaintList.add(o);
		complaintArray = (CodeEnforcement[]) complaintList.toArray(new CodeEnforcement[complaintList.size()]);
	}

	/**
	 * @param list
	 */
	public void addComplianceList(Object o) {
		complianceList.add(o);
		complianceArray = (CodeEnforcement[]) complianceList.toArray(new CodeEnforcement[complianceList.size()]);

	}

	/**
	 * @param list
	 */
	public void addFeeList(Object o) {
		feeList.add(o);
		feeArray = (CodeEnforcement[]) feeList.toArray(new CodeEnforcement[feeList.size()]);
	}

	/**
	 * @param list
	 */
	public void addInspectionList(Object o) {
		inspectionList.add(o);
		inspectionArray = (CodeEnforcement[]) inspectionList.toArray(new CodeEnforcement[inspectionList.size()]);
	}

	/**
	 * @param list
	 */
	public void addNoticeList(Object o) {
		noticeList.add(o);
		noticeArray = (CodeEnforcement[]) noticeList.toArray(new CodeEnforcement[noticeList.size()]);
	}

	/**
	 * @param list
	 */
	public void addPeopleList(Object o) {
		peopleList.add(o);
		peopleArray = (CodeEnforcement[]) peopleList.toArray(new CodeEnforcement[peopleList.size()]);
	}

	/**
	 * @param list
	 */
	public void addResolutionList(Object o) {
		resolutionList.add(o);
		resolutionArray = (CodeEnforcement[]) resolutionList.toArray(new CodeEnforcement[resolutionList.size()]);
	}

	public void addList(Object o) {
		CodeEnforcement ceItem = (CodeEnforcement) o;
		if (ceItem.getCategory().startsWith("Complaint"))
			addComplaintList(ceItem);
		else if (ceItem.getCategory().startsWith("Fees"))
			addFeeList(ceItem);
		else if (ceItem.getCategory().startsWith("Inspection"))
			addInspectionList(ceItem);
		else if (ceItem.getCategory().startsWith("Notices"))
			addNoticeList(ceItem);
		else if (ceItem.getCategory().startsWith("Persons"))
			addPeopleList(ceItem);
		else if (ceItem.getCategory().startsWith("Resolution"))
			addResolutionList(ceItem);
		else if (ceItem.getCategory().startsWith("Compliance"))
			addComplianceList(ceItem);
	}

	public void addTypes(Object o) {
		CodeEnforcement ceItem = (CodeEnforcement) o;
		if (ceItem.getCategory().startsWith("Complaint"))
			complaintTypeList.add(ceItem);
		else if (ceItem.getCategory().startsWith("Fees"))
			feeTypeList.add(ceItem);
		else if (ceItem.getCategory().startsWith("Inspection"))
			inspectionTypeList.add(ceItem);
		else if (ceItem.getCategory().startsWith("Notices"))
			noticeTypeList.add(ceItem);
		else if (ceItem.getCategory().startsWith("Persons"))
			peopleTypeList.add(ceItem);
		else if (ceItem.getCategory().startsWith("Resolution"))
			resolutionTypeList.add(ceItem);
		else if (ceItem.getCategory().startsWith("Compliance"))
			complianceTypeList.add(ceItem);
	}

	/**
	 * @return
	 */
	public List getComplaintTypeList() {
		return complaintTypeList;
	}

	/**
	 * @return
	 */
	public List getFeeTypeList() {
		return feeTypeList;
	}

	/**
	 * @return
	 */
	public List getInspectionTypeList() {
		return inspectionTypeList;
	}

	/**
	 * @return
	 */
	public List getNoticeTypeList() {
		return noticeTypeList;
	}

	/**
	 * @return
	 */
	public List getPeopleTypeList() {
		return peopleTypeList;
	}

	/**
	 * @return
	 */
	public List getResolutionTypeList() {
		return resolutionTypeList;
	}

	/**
	 * @param list
	 */
	public void setComplaintTypeList(List list) {
		complaintTypeList = list;
	}

	/**
	 * @param list
	 */
	public void setFeeTypeList(List list) {
		feeTypeList = list;
	}

	/**
	 * @param list
	 */
	public void setInspectionTypeList(List list) {
		inspectionTypeList = list;
	}

	/**
	 * @param list
	 */
	public void setNoticeTypeList(List list) {
		noticeTypeList = list;
	}

	/**
	 * @param list
	 */
	public void setPeopleTypeList(List list) {
		peopleTypeList = list;
	}

	/**
	 * @param list
	 */
	public void setResolutionTypeList(List list) {
		resolutionTypeList = list;
	}

	/**
	 * @return
	 */
	public List getSubTypeList() {
		return subTypeList;
	}

	/**
	 * @return
	 */
	public List getUserList() {
		return userList;
	}

	/**
	 * @param list
	 */
	public void setSubTypeList(List list) {
		subTypeList = list;
	}

	/**
	 * @param list
	 */
	public void setUserList(List list) {
		userList = list;
	}

	/**
	 * @return
	 */
	public String getScrollTo() {
		return scrollTo;
	}

	/**
	 * @param string
	 */
	public void setScrollTo(String string) {
		scrollTo = string;
	}

	/**
	 * @return Returns the complianceTypeList.
	 */
	public List getComplianceTypeList() {
		return complianceTypeList;
	}

	/**
	 * @param complianceTypeList
	 *            The complianceTypeList to set.
	 */
	public void setComplianceTypeList(List list) {
		complianceTypeList = list;
	}
}