package elms.control.beans;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.app.common.ProcessTeamNameRecord;
import elms.app.planning.Planner;
import elms.app.planning.PlannerUpdateRecord;

public class PlannerUpdateForm extends ActionForm {
	private String reAssign;
	private boolean reassignSelect;
	private List plannerUpdateList;
	Logger logger;
	public Planner[] plannerArray;

	public PlannerUpdateForm() {
		plannerUpdateList = new ArrayList();
		logger = Logger.getLogger(elms.control.beans.PlannerUpdateForm.class.getName());
		plannerArray = new Planner[0];
	}

	public void reset(ActionMapping actionmapping, HttpServletRequest httpservletrequest) {
	}

	public String getReAssign() {
		return reAssign;
	}

	public void setReAssign(String string) {
		reAssign = string;
	}

	public boolean isReassignSelect() {
		return reassignSelect;
	}

	public void setReassignSelect(boolean b) {
		reassignSelect = b;
	}

	public List getPlannerUpdateList() {
		return plannerUpdateList;
	}

	public void setPlannerUpdateList(List list) {
		plannerUpdateList = list;
	}

	/**
	 * Get Planner activities.
	 * 
	 * @return
	 * @throws Exception
	 */
	public void getPlannersActivities() throws Exception {
		logger.info("getPlannersActivities()");

		try {
			ActivityAgent planningAgent = new ActivityAgent();
			List planners = ActivityAgent.getPlanners();
			List activitylist = new ArrayList();

			for (int i = 0; i < planners.size(); i++) {
				ProcessTeamNameRecord processTeamRecord = (ProcessTeamNameRecord) planners.get(i);
				String userId = processTeamRecord.getNameValue();
				String userName = processTeamRecord.getNameLabel();
				activitylist.add(planningAgent.getPlannerUpdate(userId, userName));
			}

			logger.debug(" activitylist: " + activitylist);

			plannerUpdateList = activitylist;
			Planner[] tmpArray = new Planner[activitylist.size()];
			activitylist.toArray(tmpArray);
			plannerArray = tmpArray;
		} catch (Exception e) {
			logger.error("Exception occured in getPlannersActivities method of PlannerUpdateForm. Message-" + e.getMessage());
			throw e;
		}

	}

	public String assignActivitiesToPlanners() throws Exception {
		logger.info("assignActivitiesToPlanners()");

		Planner[] tmpPlanner = new Planner[0];
		PlannerUpdateRecord[] tmpPlannerUpdateRecord = new PlannerUpdateRecord[0];
		tmpPlanner = plannerArray;

		try {
			for (int i = 0; i < tmpPlanner.length; i++) {
				tmpPlannerUpdateRecord = tmpPlanner[i].plannerUpdateRecord;

				for (int j = 0; j < tmpPlannerUpdateRecord.length; j++) {
					logger.debug("Print Check : " + tmpPlannerUpdateRecord[j].check);

					if ("true".equals(tmpPlannerUpdateRecord[j].check)) {
						logger.debug("Print Activity No: " + tmpPlannerUpdateRecord[j].activityNumber);
						logger.debug("Print USer ID: " + tmpPlannerUpdateRecord[j].getFromUserId());
						(new ActivityAgent()).reAssignPlanningActivity(reAssign, tmpPlannerUpdateRecord[j].activityNumber, tmpPlannerUpdateRecord[j].getFromUserId());
					}
				}
			}
		} catch (Exception e) {
			logger.error("Exception " + e.toString());
		}

		return "assign";
	}

	public void activityDetailsByPlanner(String userId, String userName) throws Exception {
		try {
			ActivityAgent planningAgent = new ActivityAgent();
			List planners = ActivityAgent.getPlanners();
			List activitylist = new ArrayList();
			activitylist.add(planningAgent.getPlannerUpdate(userId, userName));
			plannerUpdateList = activitylist;

			Planner[] tmpArray = new Planner[activitylist.size()];
			activitylist.toArray(tmpArray);
			plannerArray = tmpArray;
		} catch (Exception e) {
			logger.error("Exception occured in getPlannersActivities method of PlannerUpdateForm. Message-" + e.getMessage());
			throw e;
		}
	}

	public Planner[] getPlannerArray() {
		return plannerArray;
	}

	public void setPlannerArray(Planner[] planners) {
		plannerArray = planners;
	}
}
