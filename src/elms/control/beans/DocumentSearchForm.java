package elms.control.beans;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * The class acts as a presentation support class for the document search presentation interface.
 * 
 * @author abelaguly updated - Dec 21, 2003,10:53:41 AM - Anand Belaguly
 */
public class DocumentSearchForm extends ActionForm {

	/**
	 * Member variable declaration
	 */
	protected String streetNumber;
	protected String streetFraction;
	protected String streetName;
	protected String unit;
	protected String projectNumber;
	protected String subProjectNumber;
	protected String activityNumber;
	protected String appliedDate;
	protected String issuedDate;
	protected String finaledDate;
	protected String activityDescription;
	protected String comment;
	protected String keyword1;
	protected String keyword2;
	protected String keyword3;
	protected String keyword4;

	protected String search;
	protected String cancel;

	protected boolean addressSearch;
	protected String lsoList;

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		activityNumber = null;
		streetNumber = null;
		streetFraction = null;
		streetName = null;
		projectNumber = null;
		appliedDate = null;
		issuedDate = null;
		finaledDate = null;
		activityDescription = null;
		comment = null;
		keyword1 = null;
		keyword2 = null;
		keyword3 = null;
		keyword4 = null;
		addressSearch = false;
		lsoList = "";
	}

	/**
	 * The validate method
	 */
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		// delcare validations for the form
		/*
		 * if ((firstName == null) || (firstName.length() < 1)) errors.add("firstName",new ActionError("error.firstname.required"));
		 * 
		 * if ((lastName == null) || (lastName.length() < 1)) errors.add("lastName",new ActionError("error.lastname.required"));
		 */
		return errors;
	}

	public DocumentSearchForm() {
		addressSearch = false;
	}

	/**
	 * @return activityDescription
	 */
	public String getActivityDescription() {
		return activityDescription;
	}

	/**
	 * @return activityNumber
	 */
	public String getActivityNumber() {
		return activityNumber;
	}

	/**
	 * @return appliedDate
	 */
	public String getAppliedDate() {
		return appliedDate;
	}

	/**
	 * @return
	 */
	public String getCancel() {
		return cancel;
	}

	/**
	 * @return
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @return
	 */
	public String getFinaledDate() {
		return finaledDate;
	}

	/**
	 * @return
	 */
	public String getIssuedDate() {
		return issuedDate;
	}

	/**
	 * @return
	 */
	public String getKeyword1() {
		return keyword1;
	}

	/**
	 * @return
	 */
	public String getKeyword2() {
		return keyword2;
	}

	/**
	 * @return
	 */
	public String getKeyword3() {
		return keyword3;
	}

	/**
	 * @return
	 */
	public String getKeyword4() {
		return keyword4;
	}

	/**
	 * @return
	 */
	public String getProjectNumber() {
		return projectNumber;
	}

	/**
	 * @return
	 */
	public String getSearch() {
		return search;
	}

	/**
	 * @return
	 */
	public String getStreetFraction() {
		return streetFraction;
	}

	/**
	 * @return
	 */
	public String getStreetName() {
		return streetName;
	}

	/**
	 * @return
	 */
	public String getStreetNumber() {
		return streetNumber;
	}

	/**
	 * @param string
	 */
	public void setActivityDescription(String string) {
		activityDescription = string;
	}

	/**
	 * @param string
	 */
	public void setActivityNumber(String string) {
		activityNumber = string;
	}

	/**
	 * @param string
	 */
	public void setAppliedDate(String string) {
		appliedDate = string;
	}

	/**
	 * @param string
	 */
	public void setCancel(String string) {
		cancel = string;
	}

	/**
	 * @param string
	 */
	public void setComment(String string) {
		comment = string;
	}

	/**
	 * @param string
	 */
	public void setFinaledDate(String string) {
		finaledDate = string;
	}

	/**
	 * @param string
	 */
	public void setIssuedDate(String string) {
		issuedDate = string;
	}

	/**
	 * @param string
	 */
	public void setKeyword1(String string) {
		keyword1 = string;
	}

	/**
	 * @param string
	 */
	public void setKeyword2(String string) {
		keyword2 = string;
	}

	/**
	 * @param string
	 */
	public void setKeyword3(String string) {
		keyword3 = string;
	}

	/**
	 * @param string
	 */
	public void setKeyword4(String string) {
		keyword4 = string;
	}

	/**
	 * @param string
	 */
	public void setProjectNumber(String string) {
		projectNumber = string;
	}

	/**
	 * @param string
	 */
	public void setSearch(String string) {
		search = string;
	}

	/**
	 * @param string
	 */
	public void setStreetFraction(String string) {
		streetFraction = string;
	}

	/**
	 * @param string
	 */
	public void setStreetName(String string) {
		streetName = string;
	}

	/**
	 * @param string
	 */
	public void setStreetNumber(String string) {
		streetNumber = string;
	}

	/**
	 * @return
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * @param string
	 */
	public void setUnit(String string) {
		unit = string;
	}

	/**
	 * @return
	 */
	public String getSubProjectNumber() {
		return subProjectNumber;
	}

	/**
	 * @param string
	 */
	public void setSubProjectNumber(String string) {
		subProjectNumber = string;
	}

	/**
	 * @return
	 */
	public boolean isAddressSearch() {
		return addressSearch;
	}

	/**
	 * @return
	 */
	public String getLsoList() {
		return lsoList;
	}

	/**
	 * @param b
	 */
	public void setAddressSearch(boolean b) {
		addressSearch = b;
	}

	/**
	 * @param string
	 */
	public void setLsoList(String string) {
		lsoList = string;
	}

} // End class