package elms.control.beans;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author Shekhar Jain
 */
public class PeopleSearchForm extends ActionForm {

	String level;
	String psaId;
	String psaType;
	String lsoAddress;
	String psaInfo;
	List peopleList;

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		// reset the declaration values...or rather set it to something default.
		// this.firstName = null;
		// this.lastName = null;

	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		// delcare validations for the form
		/*
		 * if ((firstName == null) || (firstName.length() < 1)) errors.add("firstName",new ActionError("error.firstname.required"));
		 * 
		 * if ((lastName == null) || (lastName.length() < 1)) errors.add("lastName",new ActionError("error.lastname.required"));
		 */
		return errors;
	}

	public PeopleSearchForm() {
		level = psaId = psaType = lsoAddress = psaInfo = "";
	}

	/**
	 * @return
	 */
	public String getLsoAddress() {
		return lsoAddress;
	}

	/**
	 * @return
	 */
	public String getPsaId() {
		return psaId;
	}

	/**
	 * @return
	 */
	public String getPsaInfo() {
		return psaInfo;
	}

	/**
	 * @return
	 */
	public String getPsaType() {
		return psaType;
	}

	/**
	 * @param string
	 */
	public void setLsoAddress(String string) {
		lsoAddress = string;
	}

	/**
	 * @param string
	 */
	public void setPsaId(String string) {
		psaId = string;
	}

	/**
	 * @param string
	 */
	public void setPsaInfo(String string) {
		psaInfo = string;
	}

	/**
	 * @param string
	 */
	public void setPsaType(String string) {
		psaType = string;
	}

	/**
	 * @return
	 */
	public List getPeopleList() {
		return peopleList;
	}

	/**
	 * @param list
	 */
	public void setPeopleList(List list) {
		peopleList = list;
	}

	/**
	 * @return
	 */
	public String getLevel() {
		return level;
	}

	/**
	 * @param string
	 */
	public void setLevel(String string) {
		level = string;
	}

}
