package elms.control.beans.finance;

import org.apache.struts.action.ActionForm;

import elms.app.finance.RevenueEdit;

/**
 * @author Shekhar Jain
 */
public class PostRevenueForm extends ActionForm {
	private String inputDate;
	private String inputDepartment;
	private String action;

	private String deptName;
	private String deptCode;
	private String transactionDate;
	private String batchNbr;
	private String batchDate;
	private RevenueEdit[] accountList;
	private String totalRevenue;
	private String totalRevenuePosted;
	private String userName;

	/**
	 * Returns the action.
	 * 
	 * @return String
	 */
	public String getAction() {
		return action;
	}

	/**
	 * Returns the batchDate.
	 * 
	 * @return String
	 */
	public String getBatchDate() {
		return batchDate;
	}

	/**
	 * Returns the batchNbr.
	 * 
	 * @return String
	 */
	public String getBatchNbr() {
		return batchNbr;
	}

	/**
	 * Returns the deptName.
	 * 
	 * @return String
	 */
	public String getDeptName() {
		return deptName;
	}

	/**
	 * Returns the inputDate.
	 * 
	 * @return String
	 */
	public String getInputDate() {
		return inputDate;
	}

	/**
	 * Returns the inputDepartment.
	 * 
	 * @return String
	 */
	public String getInputDepartment() {
		return inputDepartment;
	}

	/**
	 * Returns the totalRevenue.
	 * 
	 * @return String
	 */
	public String getTotalRevenue() {
		return totalRevenue;
	}

	/**
	 * Returns the totalRevenuePosted.
	 * 
	 * @return String
	 */
	public String getTotalRevenuePosted() {
		return totalRevenuePosted;
	}

	/**
	 * Returns the transactionDate.
	 * 
	 * @return String
	 */
	public String getTransactionDate() {
		return transactionDate;
	}

	/**
	 * Sets the action.
	 * 
	 * @param action
	 *            The action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * Sets the batchDate.
	 * 
	 * @param batchDate
	 *            The batchDate to set
	 */
	public void setBatchDate(String batchDate) {
		this.batchDate = batchDate;
	}

	/**
	 * Sets the batchNbr.
	 * 
	 * @param batchNbr
	 *            The batchNbr to set
	 */
	public void setBatchNbr(String batchNbr) {
		this.batchNbr = batchNbr;
	}

	/**
	 * Sets the deptName.
	 * 
	 * @param deptName
	 *            The deptName to set
	 */
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	/**
	 * Sets the inputDate.
	 * 
	 * @param inputDate
	 *            The inputDate to set
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	/**
	 * Sets the inputDepartment.
	 * 
	 * @param inputDepartment
	 *            The inputDepartment to set
	 */
	public void setInputDepartment(String inputDepartment) {
		this.inputDepartment = inputDepartment;
	}

	/**
	 * Sets the totalRevenue.
	 * 
	 * @param totalRevenue
	 *            The totalRevenue to set
	 */
	public void setTotalRevenue(String totalRevenue) {
		this.totalRevenue = totalRevenue;
	}

	/**
	 * Sets the totalRevenuePosted.
	 * 
	 * @param totalRevenuePosted
	 *            The totalRevenuePosted to set
	 */
	public void setTotalRevenuePosted(String totalRevenuePosted) {
		this.totalRevenuePosted = totalRevenuePosted;
	}

	/**
	 * Sets the transactionDate.
	 * 
	 * @param transactionDate
	 *            The transactionDate to set
	 */
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	/**
	 * Returns the accountList.
	 * 
	 * @return RevenueEdit[]
	 */
	public RevenueEdit[] getAccountList() {
		return accountList;
	}

	/**
	 * Sets the accountList.
	 * 
	 * @param accountList
	 *            The accountList to set
	 */
	public void setAccountList(RevenueEdit[] accountList) {
		this.accountList = accountList;
	}

	/**
	 * Returns the deptCode.
	 * 
	 * @return String
	 */
	public String getDeptCode() {
		return deptCode;
	}

	/**
	 * Sets the deptCode.
	 * 
	 * @param deptCode
	 *            The deptCode to set
	 */
	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}

	/**
	 * Returns the userName.
	 * 
	 * @return String
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the userName.
	 * 
	 * @param userName
	 *            The userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

}