package elms.control.beans;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class SearchForm extends ActionForm {

	/**
	 * Member variable declaration
	 */

	protected String activityNumber;
	protected String projectTeam;
	protected String streetNumber;
	protected String parcleC;
	protected String parcleA;
	protected String parcelB;
	protected String addressStreetName;

	protected String projectNumber;
	protected String streetFraction;
	protected String subProjectSubType;
	protected String subProjectType;
	protected String personInfoType;
	protected String activityType;
	protected String Submit;
	protected String subProject;
	protected String projectName;
	protected String streetnameID;

	/**
	 * Gets the activityNumber
	 * 
	 * @return Returns a String
	 */
	public String getActivityNumber() {
		return activityNumber;
	}

	/**
	 * Sets the activityNumber
	 * 
	 * @param activityNumber
	 *            The activityNumber to set
	 */
	public void setActivityNumber(String activityNumber) {
		this.activityNumber = activityNumber;
	}

	/**
	 * Gets the projectTeam
	 * 
	 * @return Returns a String
	 */
	public String getProjectTeam() {
		return projectTeam;
	}

	/**
	 * Sets the projectTeam
	 * 
	 * @param projectTeam
	 *            The projectTeam to set
	 */
	public void setProjectTeam(String projectTeam) {
		this.projectTeam = projectTeam;
	}

	/**
	 * Gets the streeNumber
	 * 
	 * @return Returns a String
	 */
	public String getStreetNumber() {
		return streetNumber;
	}

	/**
	 * Sets the streeNumber
	 * 
	 * @param streeNumber
	 *            The streeNumber to set
	 */
	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	/**
	 * Gets the parcleC
	 * 
	 * @return Returns a String
	 */
	public String getParcleC() {
		return parcleC;
	}

	/**
	 * Sets the parcleC
	 * 
	 * @param parcleC
	 *            The parcleC to set
	 */
	public void setParcleC(String parcleC) {
		this.parcleC = parcleC;
	}

	/**
	 * Gets the parcleA
	 * 
	 * @return Returns a String
	 */
	public String getParcleA() {
		return parcleA;
	}

	/**
	 * Sets the parcleA
	 * 
	 * @param parcleA
	 *            The parcleA to set
	 */
	public void setParcleA(String parcleA) {
		this.parcleA = parcleA;
	}

	/**
	 * Gets the parcelB
	 * 
	 * @return Returns a String
	 */
	public String getParcelB() {
		return parcelB;
	}

	/**
	 * Sets the parcelB
	 * 
	 * @param parcelB
	 *            The parcelB to set
	 */
	public void setParcelB(String parcelB) {
		this.parcelB = parcelB;
	}

	/**
	 * Gets the projectNumber
	 * 
	 * @return Returns a String
	 */
	public String getProjectNumber() {
		return projectNumber;
	}

	/**
	 * Sets the projectNumber
	 * 
	 * @param projectNumber
	 *            The projectNumber to set
	 */
	public void setProjectNumber(String projectNumber) {
		this.projectNumber = projectNumber;
	}

	/**
	 * Gets the streetFraction
	 * 
	 * @return Returns a String
	 */
	public String getStreetFraction() {
		return streetFraction;
	}

	/**
	 * Sets the streetFraction
	 * 
	 * @param streetFraction
	 *            The streetFraction to set
	 */
	public void setStreetFraction(String streetFraction) {
		this.streetFraction = streetFraction;
	}

	/**
	 * Gets the subProjectSubType
	 * 
	 * @return Returns a String
	 */
	public String getSubProjectSubType() {
		return subProjectSubType;
	}

	/**
	 * Sets the subProjectSubType
	 * 
	 * @param subProjectSubType
	 *            The subProjectSubType to set
	 */
	public void setSubProjectSubType(String subProjectSubType) {
		this.subProjectSubType = subProjectSubType;
	}

	/**
	 * Gets the subProjectType
	 * 
	 * @return Returns a String
	 */
	public String getSubProjectType() {
		return subProjectType;
	}

	/**
	 * Sets the subProjectType
	 * 
	 * @param subProjectType
	 *            The subProjectType to set
	 */
	public void setSubProjectType(String subProjectType) {
		this.subProjectType = subProjectType;
	}

	/**
	 * Gets the personInfoType
	 * 
	 * @return Returns a String
	 */
	public String getPersonInfoType() {
		return personInfoType;
	}

	/**
	 * Sets the personInfoType
	 * 
	 * @param personInfoType
	 *            The personInfoType to set
	 */
	public void setPersonInfoType(String personInfoType) {
		this.personInfoType = personInfoType;
	}

	/**
	 * Gets the activityType
	 * 
	 * @return Returns a String
	 */
	public String getActivityType() {
		return activityType;
	}

	/**
	 * Sets the activityType
	 * 
	 * @param activityType
	 *            The activityType to set
	 */
	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	/**
	 * Gets the submit
	 * 
	 * @return Returns a String
	 */
	public String getSubmit() {
		return Submit;
	}

	/**
	 * Sets the submit
	 * 
	 * @param submit
	 *            The submit to set
	 */
	public void setSubmit(String submit) {
		Submit = submit;
	}

	/**
	 * Gets the subProject
	 * 
	 * @return Returns a String
	 */
	public String getSubProject() {
		return subProject;
	}

	/**
	 * Sets the subProject
	 * 
	 * @param subProject
	 *            The subProject to set
	 */
	public void setSubProject(String subProject) {
		this.subProject = subProject;
	}

	/**
	 * Gets the projectName
	 * 
	 * @return Returns a String
	 */
	public String getProjectName() {
		return projectName;
	}

	/**
	 * Sets the projectName
	 * 
	 * @param projectName
	 *            The projectName to set
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/**
	 * Gets the streetnameID
	 * 
	 * @return Returns a String
	 */
	public String getStreetnameID() {
		return streetnameID;
	}

	/**
	 * Sets the streetnameID
	 * 
	 * @param streetnameID
	 *            The streetnameID to set
	 */
	public void setStreetnameID(String streetnameID) {
		this.streetnameID = streetnameID;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		activityNumber = null;
		projectTeam = null;
		streetNumber = null;
		parcleC = null;
		parcleA = null;
		parcelB = null;
		projectNumber = null;
		streetFraction = null;
		subProjectSubType = null;
		subProjectType = null;
		personInfoType = null;
		activityType = null;
		Submit = null;
		subProject = null;
		projectName = null;
		streetnameID = null;
	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		// delcare validations for the form
		/*
		 * if ((firstName == null) || (firstName.length() < 1)) errors.add("firstName",new ActionError("error.firstname.required"));
		 * 
		 * if ((lastName == null) || (lastName.length() < 1)) errors.add("lastName",new ActionError("error.lastname.required"));
		 */
		return errors;
	}

	/**
	 * Gets the addressStreetName
	 * 
	 * @return Returns a String
	 */
	public String getAddressStreetName() {
		return addressStreetName;
	}

	/**
	 * Sets the addressStreetName
	 * 
	 * @param addressStreetName
	 *            The addressStreetName to set
	 */
	public void setAddressStreetName(String addressStreetName) {
		this.addressStreetName = addressStreetName;
	}

} // End class