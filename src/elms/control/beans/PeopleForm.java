package elms.control.beans;

import java.util.Calendar;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;

import elms.app.people.People;
import elms.util.StringUtils;

public class PeopleForm extends ActionForm {

	static Logger logger = Logger.getLogger(PeopleForm.class.getName());

	/**
	 * Member variable declaration
	 */
	protected People people;
	protected String strLicenseExpires;
	protected String strHoldDate;
	protected String strDlExpiryDate;
	protected String strGeneralLiabilityDate;
	protected String strAutoLiabilityDate;
	protected String strInsuranceExpirationDate;
	protected String strBusinessLicenseExpires;
	protected String strWorkersCompExpires;
	protected int peopleCount;
	protected String isExpired = "Y";
	protected String BLExpirationDate = "Y";

	protected String GLDate = "Y";
	protected String ALDate = "Y";
	protected String WCEDate = "Y";
	protected String hasHold = "N";
	protected String versionDifference;
	protected String psaId;
	protected String psaType;
	protected String checkboxArray;
	protected int versionNumber;
	public String date = StringUtils.cal2str(Calendar.getInstance());

	protected String anyDateExpired = "N";

	public String dateOfBirthString;
	public String confirmStrDlExpiryDate;

	protected String[] selectedPeople = {};

	protected String insuranceAmount;
	

	protected String strMoveInDate;
	protected String strMoveOutDate;
	

	/**
	 * @roseuid 3CAA2F42019B
	 */
	public PeopleForm() {
		people = new People();
		this.psaId = "";
		this.psaType = "";
		setIsExpired("Yahoo");
		setBLExpirationDate("Yahoo");
		setGLDate("Yahoo");
		setALDate("Yahoo");
		setWCEDate("Yahoo");

	}

	public String getInsuranceAmount() {
		insuranceAmount = StringUtils.dbl2$(people.getInsuranceAmount());
		return insuranceAmount;
	}

	public void setInsuranceAmount(String insuranceAmount) {
		this.insuranceAmount = insuranceAmount;
		this.people.insuranceAmount = StringUtils.$2dbl(insuranceAmount);
	}

	public void setPeople(People people) {
		try {
			if (people != null) {
				this.people = (People) people.clone();
			}
		} catch (Exception e) {
			// logger.error("error while copying people " + e.getMessage());
		}
	}

	/**
	 * Gets the people
	 * 
	 * @return Returns a People
	 */
	public People getPeople() {
		return people;
	}

	public String getStrInsuranceExpirationDate() {
		strInsuranceExpirationDate = StringUtils.cal2str(people.insuranceExpirationDate);
		return strInsuranceExpirationDate;
	}

	/**
	 * Sets the setStrInsuranceExpirationDate
	 * 
	 * @param setStrInsuranceExpirationDate
	 */
	public void setStrInsuranceExpirationDate(String strInsuranceExpirationDate) {
		getPeople().setInsuranceExpirationDate(StringUtils.str2cal(strInsuranceExpirationDate));
		this.strInsuranceExpirationDate = strInsuranceExpirationDate;

	}

	/**
	 * Gets the strLicenseExpires
	 * 
	 * @return Returns a String
	 */
	public String getStrLicenseExpires() {
		strLicenseExpires = StringUtils.cal2str(people.licenseExpires);
		return strLicenseExpires;
	}

	/**
	 * Sets the strLicenseExpires
	 * 
	 * @param strLicenseExpires
	 *            The strLicenseExpires to set
	 */
	public void setStrLicenseExpires(String strLicenseExpires) {
		getPeople().setLicenseExpires(StringUtils.str2cal(strLicenseExpires));
		this.strLicenseExpires = strLicenseExpires;
		if (Calendar.getInstance().after(this.getPeople().getLicenseExpires())) {
			this.isExpired = "Y";
			getPeople().setIsExpired("Y");
		} else {
			this.isExpired = "N";
			getPeople().setIsExpired("N");
		}
	}

	/**
	 * Gets the strHoldDate
	 * 
	 * @return Returns a String
	 */
	public String getStrHoldDate() {
		strHoldDate = StringUtils.cal2str(people.holdDate);
		return strHoldDate;
	}

	/**
	 * Sets the strHoldDate
	 * 
	 * @param strHoldDate
	 *            The strHoldDate to set
	 */
	public void setStrHoldDate(String strHoldDate) {
		this.people.holdDate = StringUtils.str2cal(strHoldDate);
		this.strHoldDate = strHoldDate;
	}

	/**
	 * Gets the strGeneralLiabilityDate
	 * 
	 * @return Returns a String
	 */
	public String getStrGeneralLiabilityDate() {
		strGeneralLiabilityDate = StringUtils.cal2str(people.generalLiabilityDate);
		return strGeneralLiabilityDate;
	}

	/**
	 * Sets the strGeneralLiabilityDate
	 * 
	 * @param strGeneralLiabilityDate
	 *            The strGeneralLiabilityDate to set
	 */
	public void setStrGeneralLiabilityDate(String strGeneralLiabilityDate) {
		getPeople().setGeneralLiabilityDate(StringUtils.str2cal(strGeneralLiabilityDate));
		this.strGeneralLiabilityDate = strGeneralLiabilityDate;
		if (Calendar.getInstance().after(this.getPeople().getGeneralLiabilityDate())) {
			this.GLDate = "Y";
			getPeople().setGLDate("Y");
		} else {
			this.GLDate = "N";
			getPeople().setGLDate("N");
		}

	}

	/**
	 * Gets the strAutoLiabilityDate
	 * 
	 * @return Returns a String
	 */
	public String getStrAutoLiabilityDate() {
		strAutoLiabilityDate = StringUtils.cal2str(people.autoLiabilityDate);
		return strAutoLiabilityDate;
	}

	/**
	 * Sets the strAutoLiabilityDate
	 * 
	 * @param strAutoLiabilityDate
	 *            The strAutoLiabilityDate to set
	 */
	public void setStrAutoLiabilityDate(String strAutoLiabilityDate) {
		getPeople().setAutoLiabilityDate(StringUtils.str2cal(strAutoLiabilityDate));
		this.strAutoLiabilityDate = strAutoLiabilityDate;
		if (Calendar.getInstance().after(this.getPeople().getAutoLiabilityDate())) {
			this.ALDate = "Y";
			getPeople().setALDate("Y");
		} else {
			this.ALDate = "N";
			getPeople().setALDate("N");
		}

	}

	/**
	 * Gets the strAutoLiabilityDate
	 * 
	 * @return Returns a String
	 */
	public String getDateOfBirthString() {
		dateOfBirthString = StringUtils.cal2str(people.dateOfBirth);
		return dateOfBirthString;
	}

	/**
	 * Sets the strAutoLiabilityDate
	 * 
	 * @param strAutoLiabilityDate
	 *            The strAutoLiabilityDate to set
	 */
	public void setDateOfBirthString(String dateOfBirthString) {

		getPeople().setDateOfBirth(StringUtils.str2cal(dateOfBirthString));
		this.dateOfBirthString = dateOfBirthString;
	}

	/**
	 * Gets the strBusinessLicenseExpires
	 * 
	 * @return Returns a String
	 */
	public String getStrBusinessLicenseExpires() {
		strBusinessLicenseExpires = StringUtils.cal2str(people.businessLicenseExpires);
		return strBusinessLicenseExpires;
	}

	/**
	 * Sets the strBusinessLicenseExpires
	 * 
	 * @param strBusinessLicenseExpires
	 *            The strBusinessLicenseExpires to set
	 */
	public void setStrBusinessLicenseExpires(String strBusinessLicenseExpires) {

		getPeople().setBusinessLicenseExpires(StringUtils.str2cal(strBusinessLicenseExpires));
		this.strBusinessLicenseExpires = strBusinessLicenseExpires;
		if (Calendar.getInstance().after(this.getPeople().getBusinessLicenseExpires())) {
			this.BLExpirationDate = "Y";
			getPeople().setBLExpirationDate("Y");
		} else {
			this.BLExpirationDate = "N";
			getPeople().setBLExpirationDate("N");
		}

	}

	/**
	 * Gets the strDlExpiryDate
	 * 
	 * @return Returns a String
	 */
	public String getStrDlExpiryDate() {
		strDlExpiryDate = StringUtils.cal2str(people.dlExpiryDate);
		return strDlExpiryDate;
	}

	/**
	 * Sets the strDlExpiryDate
	 * 
	 * @param strDlExpiryDate
	 *            The strDlExpiryDate to set
	 */
	public void setStrDlExpiryDate(String strDlExpiryDate) {

		getPeople().setDlExpiryDate(StringUtils.str2cal(strDlExpiryDate));
		this.strDlExpiryDate = strDlExpiryDate;

	}

	/**
	 * Gets the strWorkersCompExpires
	 * 
	 * @return Returns a String
	 */
	public String getStrWorkersCompExpires() {
		strWorkersCompExpires = StringUtils.cal2str(people.workersCompExpires);
		return strWorkersCompExpires;
	}

	/**
	 * Sets the strWorkersCompExpires
	 * 
	 * @param strWorkersCompExpires
	 *            The strWorkersCompExpires to set
	 */
	public void setStrWorkersCompExpires(String strWorkersCompExpires) {
		getPeople().setWorkersCompExpires(StringUtils.str2cal(strWorkersCompExpires));
		this.strWorkersCompExpires = strWorkersCompExpires;
		if (Calendar.getInstance().after(this.getPeople().getWorkersCompExpires())) {
			this.WCEDate = "Y";
			getPeople().setWCEDate("Y");
		} else {
			this.WCEDate = "N";
			getPeople().setWCEDate("N");
		}

	}

	/**
	 * Gets the selectedPeople
	 * 
	 * @return Returns a String[]
	 */
	public String[] getSelectedPeople() {
		return selectedPeople;
	}

	/**
	 * Sets the selectedPeople
	 * 
	 * @param selectedPeople
	 *            The selectedPeople to set
	 */
	public void setSelectedPeople(String[] selectedPeople) {
		this.selectedPeople = selectedPeople;
	}

	/**
	 * Gets the peopleCount
	 * 
	 * @return Returns a int
	 */
	public int getPeopleCount() {
		return peopleCount;
	}

	/**
	 * Sets the peopleCount
	 * 
	 * @param peopleCount
	 *            The peopleCount to set
	 */
	public void setPeopleCount(int peopleCount) {
		this.peopleCount = peopleCount;
	}

	/**
	 * Returns the date.
	 * 
	 * @return String
	 */
	public String getDate() {
		return date;
	}

	/**
	 * Returns the isExpired.
	 * 
	 * @return String
	 */
	public String getIsExpired() {
		return isExpired;
	}

	/**
	 * Sets the date.
	 * 
	 * @param date
	 *            The date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * Sets the isExpired.
	 * 
	 * @param isExpired
	 *            The isExpired to set
	 */
	public void setIsExpired(String isExpired) {
		this.isExpired = isExpired;
	}

	/**
	 * Returns the logger.
	 * 
	 * @return Logger
	 */
	public static Logger getLogger() {
		return logger;
	}

	/**
	 * Returns the aLDate.
	 * 
	 * @return String
	 */
	public String getALDate() {
		return ALDate;
	}

	/**
	 * Returns the bLExpirationDate.
	 * 
	 * @return String
	 */
	public String getBLExpirationDate() {
		return BLExpirationDate;
	}

	/**
	 * Returns the gLDate.
	 * 
	 * @return String
	 */
	public String getGLDate() {
		return GLDate;
	}

	/**
	 * Returns the wCEDate.
	 * 
	 * @return String
	 */
	public String getWCEDate() {
		return WCEDate;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param logger
	 *            The logger to set
	 */
	public static void setLogger(Logger logger) {
		PeopleForm.logger = logger;
	}

	/**
	 * Sets the aLDate.
	 * 
	 * @param aLDate
	 *            The aLDate to set
	 */
	public void setALDate(String aLDate) {
		ALDate = aLDate;
	}

	/**
	 * Sets the bLExpirationDate.
	 * 
	 * @param bLExpirationDate
	 *            The bLExpirationDate to set
	 */
	public void setBLExpirationDate(String bLExpirationDate) {
		BLExpirationDate = bLExpirationDate;
	}

	/**
	 * Sets the gLDate.
	 * 
	 * @param gLDate
	 *            The gLDate to set
	 */
	public void setGLDate(String gLDate) {
		GLDate = gLDate;
	}

	/**
	 * Sets the wCEDate.
	 * 
	 * @param wCEDate
	 *            The wCEDate to set
	 */
	public void setWCEDate(String wCEDate) {
		WCEDate = wCEDate;
	}

	/**
	 * Returns the hasHold.
	 * 
	 * @return String
	 */
	public String getHasHold() {
		return hasHold;
	}

	/**
	 * Sets the hasHold.
	 * 
	 * @param hasHold
	 *            The hasHold to set
	 */
	public void setHasHold(String hasHold) {
		this.hasHold = hasHold;
	}

	/**
	 * @return
	 */
	public String getPsaId() {
		return psaId;
	}

	/**
	 * @return
	 */
	public String getPsaType() {
		return psaType;
	}

	/**
	 * @param string
	 */
	public void setPsaId(String string) {
		psaId = string;
	}

	/**
	 * @param string
	 */
	public void setPsaType(String string) {
		psaType = string;
	}

	/**
	 * @return Returns the versionDifference.
	 */
	public String getVersionDifference() {
		return versionDifference;
	}

	/**
	 * @param versionDifference
	 *            The versionDifference to set.
	 */
	public void setVersionDifference(String versionDifference) {
		this.versionDifference = versionDifference;
	}

	/**
	 * @return Returns the checkboxArray.
	 */
	public String getCheckboxArray() {
		return checkboxArray;
	}

	/**
	 * @param checkboxArray
	 *            The checkboxArray to set.
	 */
	public void setCheckboxArray(String checkboxArray) {
		this.checkboxArray = checkboxArray;
	}

	/**
	 * @return Returns the versionNumber.
	 */
	public int getVersionNumber() {
		return versionNumber;
	}

	/**
	 * @param versionNumber
	 *            The versionNumber to set.
	 */
	public void setVersionNumber(int versionNumber) {
		this.versionNumber = versionNumber;
	}

	/**
	 * @return Returns the anyDateExpired.
	 */
	public String getAnyDateExpired() {
		return anyDateExpired;
	}

	/**
	 * @param anyDateExpired
	 *            The anyDateExpired to set.
	 */
	public void setAnyDateExpired(String anyDateExpired) {
		this.anyDateExpired = anyDateExpired;
	}

	/**
	 * @return Returns the confirmStrDlExpiryDate.
	 */
	public String getConfirmStrDlExpiryDate() {
		return confirmStrDlExpiryDate;
	}

	/**
	 * @param confirmStrDlExpiryDate
	 *            The confirmStrDlExpiryDate to set.
	 */
	public void setConfirmStrDlExpiryDate(String confirmStrDlExpiryDate) {

		this.confirmStrDlExpiryDate = confirmStrDlExpiryDate;
	}
	
	/**
	 * @return the strMoveInDate
	 */
	public String getStrMoveInDate() {
		strMoveInDate = (people.moveInDate);
		return strMoveInDate;
	}

	/**
	 * @param strMoveInDate the strMoveInDate to set
	 */
	public void setStrMoveInDate(String strMoveInDate) {
		getPeople().setMoveInDate((strMoveInDate));
		this.strMoveInDate = strMoveInDate;
	}

	/**
	 * @return the strMoveOutDate
	 */
	public String getStrMoveOutDate() {
		strMoveOutDate = (people.moveOutDate);
		return strMoveOutDate;
	}

	/**
	 * @param strMoveOutDate the strMoveOutDate to set
	 */
	public void setStrMoveOutDate(String strMoveOutDate) {
		getPeople().setMoveOutDate((strMoveOutDate));
		this.strMoveOutDate = strMoveOutDate;
	}

}