package elms.control.actions.inspection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.InspectionAgent;
import elms.control.beans.SearchLibForm;
import elms.util.StringUtils;

public class SearchLibraryAction extends Action {
	static Logger logger = Logger.getLogger(SearchLibraryAction.class.getName());
	String nextPage = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.info("Entering SearchLibraryAction ");

		MessageResources messages = getResources();
		HttpSession session = request.getSession();

		try {
			SearchLibForm frm = (SearchLibForm) form;
			InspectionAgent inspectionAgent = new InspectionAgent();
			List inspectionSubCategoryList = new ArrayList();

			String flag = request.getParameter("flag");

			if (flag == null) {
				flag = "";
			}

			logger.debug("the flag got form the request as" + flag);

			if (flag.equals("onChange")) {
				nextPage = "search";

				String strCategoryId = frm.getCategory();
				logger.debug("category Id from the form is " + strCategoryId);
				inspectionSubCategoryList = InspectionAgent.getInspectionSubCategories(StringUtils.s2i(strCategoryId));
				logger.debug("The sub category list size is" + inspectionSubCategoryList.size());
				request.setAttribute("inspectionSubCategoryList", inspectionSubCategoryList);
			} else {
				nextPage = "success";

				SearchLibForm libSet = inspectionAgent.getLibraryItems((SearchLibForm) form);
				form = libSet;
				request.setAttribute("libSet", libSet);
				session.setAttribute("searchLibForm", libSet);
			}

			logger.info("Exiting SearchLibraryAction");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward(nextPage));
	}
}
