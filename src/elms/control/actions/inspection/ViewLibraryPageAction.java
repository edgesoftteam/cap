package elms.control.actions.inspection;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.InspectionAgent;
import elms.control.beans.InspectionForm;
import elms.control.beans.SearchLibForm;
import elms.util.StringUtils;

public class ViewLibraryPageAction extends Action {

	static Logger logger = Logger.getLogger(ViewLibraryPageAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		MessageResources messages = getResources();
		HttpSession session = request.getSession();

		try {
			logger.info("Entering ViewLibraryPageAction ");
			InspectionAgent inspectionAgent = new InspectionAgent();
			InspectionForm frm = (InspectionForm) session.getAttribute("inspectionForm");
			String activityType = frm.getActivityType();
			if (activityType == null)
				activityType = "";
			String strCategoryId = "";
			if (activityType.equals("BLDG"))
				strCategoryId = "5";
			else if (activityType.equals("ELEC"))
				strCategoryId = "3";
			else if (activityType.equals("MECH"))
				strCategoryId = "2";
			else if (activityType.equals("PLUM"))
				strCategoryId = "4";
			logger.debug("category Id is " + strCategoryId);
			SearchLibForm searchLibForm = new SearchLibForm();
			List inspectionSubCategoryList = new java.util.ArrayList();
			if (strCategoryId == null) {
				searchLibForm.setCategory("-1");
			} else {
				searchLibForm.setCategory(strCategoryId);
				inspectionSubCategoryList = InspectionAgent.getInspectionSubCategories(StringUtils.s2i(strCategoryId));
			}
			request.setAttribute("inspectionSubCategoryList", inspectionSubCategoryList);
			session.setAttribute("searchLibForm", searchLibForm);

			/*
			 * if (mapping.getAttribute() != null) { if ("request".equals(mapping.getScope())) request.removeAttribute(mapping.getAttribute()); else session.removeAttribute(mapping.getAttribute()); } request.setAttribute("strCategoryId", strCategoryId); logger.info("Exiting ViewLibraryPageAction");
			 */
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));
	}

}