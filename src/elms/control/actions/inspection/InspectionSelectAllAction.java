package elms.control.actions.inspection;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.app.inspection.Inspector;
import elms.app.inspection.InspectorRecord;
import elms.common.Constants;
import elms.control.beans.ViewAllInspectionForm;
import elms.security.Group;
import elms.security.User;

public class InspectionSelectAllAction extends Action {

	static Logger logger = Logger.getLogger(InspectionSelectAllAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		logger.info("Entering InspectionSelectAllAction");

		try {
			ViewAllInspectionForm frm = (ViewAllInspectionForm) form;
			ViewAllInspectionForm sessionFrm = (ViewAllInspectionForm) session.getAttribute("viewAllInspectionForm");

			User user = (User) session.getAttribute(Constants.USER_KEY);
			List groups = user.getGroups();
			Group group;
			for (int i = 0; i < groups.size(); i++) {
				group = (Group) groups.get(i);
				logger.debug("Group Name : " + group.getDescription().toUpperCase() + ":");
				if (group.getDescription().toUpperCase().equals("INSPECTION MANAGER")) {
					frm.setManager("Y");
					break;
				}
			}

			Inspector[] inspectors;
			InspectorRecord[] records;

			inspectors = frm.getInspector();

			for (int i = 0; i < inspectors.length; i++) {

				if (inspectors[i].getSelectValue().equals("all")) {
					records = inspectors[i].getInspectorRecord();

					for (int j = 0; j < records.length; j++) {
						records[j].setCheck("on");
					}
				} // end if "all"
				inspectors[i].setSelectValue("off");

			}// end for inspectors.length

			frm.setInspector(inspectors);

			session.setAttribute("viewAllInspectionForm", frm);

			logger.info("Exiting InspectionSelectAllAction");

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return (mapping.findForward("success"));
	}

}