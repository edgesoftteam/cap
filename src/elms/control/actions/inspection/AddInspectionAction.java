package elms.control.actions.inspection;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.InspectionAgent;
import elms.agent.LookupAgent;
import elms.agent.ProjectAgent;
import elms.common.Constants;
import elms.control.beans.InspectionForm;
import elms.util.StringUtils;

public class AddInspectionAction extends Action {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(AddInspectionAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();

		try {
			String activityId = request.getParameter("activityId");
			String activityType = request.getParameter("type");
			logger.info("Entering AddInspectionAction with activityId of " + activityId);

			String subProjectId = new ProjectAgent().getSubProjectId(activityId);
			String apn = "";
			apn = LookupAgent.getApnForActivityId(activityId);

			InspectionAgent inspectionAgent = new InspectionAgent();
			String subProjectTypeId = LookupAgent.getSubProjectTypeId(subProjectId);

			String inspector = inspectionAgent.getInspectorFromTerritoryMapping(subProjectTypeId, activityType, apn);
			logger.debug("inspector id : " + inspector);

			List inspectionItemCodeList = InspectionAgent.getInspectionItemCodes(activityType);
			int inspectionItemCode = inspectionAgent.getInspectionItemCode(activityId); // to set in drop-down
			request.setAttribute("inspectionItemCodeList", inspectionItemCodeList);
			logger.debug("Inspection Item List is set to request with size  " + inspectionItemCodeList.size());

			InspectionForm inspectionForm = new InspectionForm();
			inspectionForm.setActivityId(activityId);
			inspectionForm.setActivityType(activityType);
			inspectionForm.setAddOrEdit("add");
			inspectionForm.setInspectorId(inspector);

			String date = StringUtils.cal2str(Calendar.getInstance());
			inspectionForm.setInspectionDate(date);

			inspectionForm.setInspectionItem(StringUtils.i2s(inspectionItemCode));
			logger.debug("Inspection Item Code :" + inspectionItemCode);

			inspectionForm.setActionCode(StringUtils.i2s(Constants.INSPECTION_CODES_REQUEST_FOR_INSPECTION));
			request.setAttribute("inspectionForm", inspectionForm);
			session.setAttribute("inspectionForm", inspectionForm);
			request.setAttribute("activityId", activityId);

			logger.info("Exiting AddInspectionAction");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));
	}
}
