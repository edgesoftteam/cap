package elms.control.actions.common;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.CommonAgent;

public class DeleteAttachmentsAction extends Action {

	static Logger logger = Logger.getLogger(DeleteAttachmentsAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		try {

			String attachmentId = (request.getParameter("attachmentId") != null) ? request.getParameter("attachmentId") : (String) request.getAttribute("attachmentId");
			logger.debug("attachmentId " + attachmentId);
			String levelId = (request.getParameter("levelId") != null) ? request.getParameter("levelId") : (String) request.getAttribute("levelId");
			logger.debug("levelId " + levelId);
			String level = (request.getParameter("level") != null) ? request.getParameter("level") : (String) request.getAttribute("level");
			logger.debug("level " + level);

			CommonAgent attachmentAgent = new CommonAgent();
			attachmentAgent.deleteAttachment(Integer.parseInt(attachmentId), level);
			logger.debug("Attachment deleted successfully");
			List attachments = attachmentAgent.getAttachments(Integer.parseInt(levelId), level);
			logger.debug("obtained attachments list of size " + attachments.size());

			request.setAttribute("attachments", attachments);
			request.setAttribute("levelId", levelId);
			request.setAttribute("level", level);

			logger.debug("forwareded to success");
			return (mapping.findForward("success"));
		} catch (Exception e) {

			logger.error("Exception occured when deleting attachment " + e.getMessage());
			return (mapping.findForward("error"));
		}

	}
}
