package elms.control.actions.common;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.agent.LookupAgent;
import elms.control.actions.people.AddPeopleAction;
import elms.control.beans.CommentForm;
import elms.security.User;
import elms.util.StringUtils;

public class AddCommentAction extends Action {

	static Logger logger = Logger.getLogger(AddPeopleAction.class.getName());
	protected CommentForm commentForm = null;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		AdminAgent userAgent = new AdminAgent();

		try {
			CommentForm commentForm = new CommentForm();

			String levelId = request.getParameter("levelId"); // 105
			String commentLevel = request.getParameter("commentLevel");
			logger.debug("Entered into AddCommentAction with LevelId : " + levelId + " And CommentLevel : " + commentLevel);

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}

			request.setAttribute("commentForm", commentForm);

			commentForm.setLevelId(StringUtils.s2i(levelId));
			commentForm.setCommentLevel(commentLevel);
			User user = (User) session.getAttribute("user");
			commentForm.setCreatedBy(user.getUserId());
			logger.debug("setting default user as current logged in user " + user.getUsername());

			List users = userAgent.getNameList();
			request.setAttribute("users", users);
			logger.debug("setting users object list in the request " + users.size());
			String url = request.getContextPath();
			if (commentLevel.equals("L"))
				url = "/viewLand.do?lsoId=";
			else if (commentLevel.equals("S"))
				url = "/viewStructure.do?lsoId=";
			else if (commentLevel.equals("O"))
				url = "/viewOccupancy.do?lsoId=";
			else if (commentLevel.equals("P"))
				url = "/viewProject.do?projectId=";
			else if (commentLevel.equals("Q"))
				url = "/viewSubProject.do?subProjectId=";
			else if (commentLevel.equals("A")) {
				String activityType = LookupAgent.getActivityTypeForActId(levelId);
				if (activityType == null)
					activityType = "";
				if (activityType.equals("PWFAC"))
					url = "/pwViewFacilities.do?activityId=";
				else if (activityType.equals("PWINFO"))
					url = "/pwViewFacilities.do?activityId=";
				else if (activityType.equals("PWSAN"))
					url = "/pwViewSanitation.do.do?activityId=";
				else if (activityType.equals("PWSEW"))
					url = "/pwViewSewer.do?activityId=";
				else if (activityType.equals("PWSTR"))
					url = "/pwViewStreets.do?activityId=";
				else if (activityType.equals("PWWAT"))
					url = "/pwViewWater.do?activityId=";
				else if (activityType.equals("PWSANC"))
					url = "/pwViewSanitation.do?activityId=";
				else if (activityType.equals("PWTSSL"))
					url = "/pwViewTrafficSignals.do?activityId=";
				else if (activityType.equals("PWELEC"))
					url = "/pwViewElectrical.do?activityId=";
				else
					url = "/viewActivity.do?activityId=";
			} else if (commentLevel.equals("Z"))
				url = "/editPeople.do?id=";
			url = request.getContextPath() + url + levelId;
			logger.debug("Url to Back is : " + url);
			request.setAttribute("goBackUrl", url);
		} catch (Exception e) {
			logger.error("Error in AddCommentAction");
		}
		return (mapping.findForward("success"));
	}
}