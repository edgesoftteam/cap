package elms.control.actions.common;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.CommonAgent;
import elms.control.beans.ConditionForm;
import elms.util.StringUtils;

public class ListConditionAction extends Action {
	static Logger logger = Logger.getLogger(ListConditionAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		try {
			int levelId = 0;
			String conditionLevel = "";

			if (request.getParameter("levelId") == null) {
				levelId = Integer.parseInt((String) request.getAttribute("levelId"));
				conditionLevel = (String) request.getAttribute("conditionLevel");
			} else {
				levelId = Integer.parseInt(request.getParameter("levelId"));
				conditionLevel = request.getParameter("conditionLevel");
			}

			if (conditionLevel == null) {
				conditionLevel = "";
			}

			logger.debug("In ListConditionAction With LevelId : " + levelId + " and ConditionLevel : " + conditionLevel);

			ConditionForm frm = (ConditionForm) form;
			frm.setLevelId(levelId);
			frm.setConditionLevel(conditionLevel);

			CommonAgent conditionAgent = new CommonAgent();

			// when Conditions selected for delete
			String[] deletedConditionId = frm.getSelectedConditions();

			if (deletedConditionId != null) {
				for (int i = 0; i < deletedConditionId.length; i++) {
					logger.debug("Deleted Comment Id  : " + deletedConditionId[i]);
					conditionAgent.deleteCondition(StringUtils.s2i(deletedConditionId[i]));
				}
			}

			String action = (String) request.getParameter("action") != null ? request.getParameter("action") : "";
			if (action.equalsIgnoreCase("saveOrder")) {
				String order = (String) request.getParameter("order") != null ? request.getParameter("order") : "";
				logger.debug("Saving sort order with List  : " + order);
				if (!order.equalsIgnoreCase("")) {
					String[] orderList = StringUtils.stringtoArray(order, ",");
					for (int i = 0; i < orderList.length; i++) {
						String conditonOrder = orderList[i].replace("list", "");
						conditionAgent.updateConditonOrder(conditonOrder, i);
					}

				}

			}

			List conditionList = conditionAgent.getConditionList(conditionLevel, levelId);
			frm.setConditions(conditionList);
			request.setAttribute("conditionForm", frm);

			String url = request.getContextPath();

			if (conditionLevel.equals("L")) {
				url = "/viewLand.do?lsoId=";
			} else if (conditionLevel.equals("S")) {
				url = "/viewStructure.do?lsoId=";
			} else if (conditionLevel.equals("O")) {
				url = "/viewOccupancy.do?lsoId=";
			} else if (conditionLevel.equals("P")) {
				url = "/viewProject.do?projectId=";
			} else if (conditionLevel.equals("Q")) {
				url = "/viewSubProject.do?subProjectId=";
			} else if (conditionLevel.equals("A")) {
				url = "/viewActivity.do?activityId=";
			}

			url = request.getContextPath() + url + frm.getLevelId();
			request.setAttribute("goBackUrl", url);
			logger.debug(" The Back Url is " + url);

			logger.debug("Exiting ListConditionAction");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));
	}
}
