package elms.control.actions.common;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import sun.jdbc.rowset.CachedRowSet;
import elms.agent.CommonAgent;
import elms.util.StringUtils;

public class ListHoldAction extends Action {
	static Logger logger = Logger.getLogger(ListHoldAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		// Extract attributes and parameters we will need
		Locale locale = getLocale(request);
		MessageResources messages = getResources();
		HttpSession session = request.getSession();

		try {
			int levelId = Integer.parseInt(request.getParameter("levelId"));
			String level = request.getParameter("level");

			if (level.equalsIgnoreCase("Z")) {
				session.setAttribute("peopleId", StringUtils.i2s(levelId));
			}

			logger.info("Entering ListHoldAction with lsoId of " + levelId);

			CommonAgent holdAgent = new CommonAgent();

			CachedRowSet holdRowSet = new CachedRowSet();
			holdRowSet = holdAgent.getHoldList(level, levelId);

			request.setAttribute("holdList", holdRowSet);
			request.setAttribute("levelId", request.getParameter("levelId"));
			request.setAttribute("level", level);

			if (holdAgent.editable(levelId, level)) {
				request.setAttribute("editable", "true");
			} else {
				request.setAttribute("editable", "false");
			}

			logger.info("Exiting ListHoldAction");

			return (mapping.findForward("success"));
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ServletException("Exception occured while getting list of holds " + e.getMessage());
		}
	}
}
