package elms.control.actions.common;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.control.beans.HoldForm;
import elms.util.StringUtils;

public class AddHoldAction extends Action {

	static Logger logger = Logger.getLogger(AddHoldAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		try {
			int levelId = Integer.parseInt(request.getParameter("levelId"));
			String level = request.getParameter("level");
			logger.debug("levelId " + levelId + " level " + level);

			HoldForm holdForm = new HoldForm();

			Calendar calendar = Calendar.getInstance();
			request.setAttribute("statusDate", StringUtils.cal2str(calendar));

			request.setAttribute("levelId", request.getParameter("levelId"));
			request.setAttribute("level", level);

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}

			logger.info("Exiting AddHoldAction");

		} catch (Exception e) {
			logger.warn(e.getMessage());
		}

		return (mapping.findForward("success"));
	}

}