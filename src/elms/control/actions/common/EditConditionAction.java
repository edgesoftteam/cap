package elms.control.actions.common;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.agent.CommonAgent;
import elms.app.common.Condition;
import elms.control.beans.ConditionForm;
import elms.security.User;
import elms.util.StringUtils;

public class EditConditionAction extends Action {
	static Logger logger = Logger.getLogger(EditConditionAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		// Extract attributes and parameters we will need
		HttpSession session = request.getSession();
		AdminAgent userAgent = new AdminAgent();

		try {
			int conditionId = 0;

			if (request.getParameter("conditionId") != null) {
				conditionId = Integer.parseInt(request.getParameter("conditionId"));
			}

			logger.info("Entering EditConditionAction with conditionId of " + conditionId);

			CommonAgent conditionAgent = new CommonAgent();
			Condition condition = conditionAgent.getCondition(conditionId);

			User user = userAgent.getUser(condition.getUpdatedBy());

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope())) {
					request.removeAttribute(mapping.getAttribute());
				} else {
					session.removeAttribute(mapping.getAttribute());
				}
			}

			ActionForm frm = new ConditionForm();
			request.setAttribute("conditionForm", frm);

			ConditionForm conditionForm = (ConditionForm) frm;

			if (condition != null) {
				conditionForm.setConditionId(condition.getConditionId());
				conditionForm.setLevelId(condition.getLevelId());
				conditionForm.setConditionLevel(condition.getConditionLevel());
				conditionForm.setConditionCode(condition.getConditionCode());
				conditionForm.setShortText(condition.getShortText());
				conditionForm.setText(condition.getText());
				conditionForm.setTmpText(condition.getText()); // this will not be displayed

				if ((condition.getWarning() != null) && condition.getWarning().equals("Y")) {
					conditionForm.setWarning("on");
				} else {
					conditionForm.setWarning("off");
				}

				if ((condition.getComplete() != null) && condition.getComplete().equals("Y")) {
					conditionForm.setComplete("on");
				} else {
					conditionForm.setComplete("off");
				}

				if ((condition.getInspectable() != null) && condition.getInspectable().equals("Y")) {
					conditionForm.setInspectable("on");
				} else {
					conditionForm.setInspectable("off");
				}

				conditionForm.setCreatedBy(condition.getCreatedBy());
				conditionForm.setUpdatedBy(user.getUserId());
				conditionForm.setUpdatedByName(StringUtils.nullReplaceWithEmpty(user.getFirstName()) + " " + StringUtils.nullReplaceWithEmpty(user.getLastName()));
				conditionForm.setCreatedDate(StringUtils.cal2str(condition.getCreatedDate()));
				conditionForm.setUpdatedDate(StringUtils.cal2str(condition.getUpdatedDate()));

				// conditionForm.setApprovedBy(condition.getApprovedBy());
			}

			String url = request.getContextPath();
			String conditionLevel = conditionForm.getConditionLevel();

			if (conditionLevel == null) {
				conditionLevel = "";
			}

			if (conditionLevel.equals("L")) {
				url = "/viewLand.do?lsoId=";
			} else if (conditionLevel.equals("S")) {
				url = "/viewStructure.do?lsoId=";
			} else if (conditionLevel.equals("O")) {
				url = "/viewOccupancy.do?lsoId=";
			} else if (conditionLevel.equals("P")) {
				url = "/viewProject.do?projectId=";
			} else if (conditionLevel.equals("Q")) {
				url = "/viewSubProject.do?subProjectId=";
			} else if (conditionLevel.equals("A")) {
				url = "/viewActivity.do?activityId=";
			}

			url = request.getContextPath() + url + conditionForm.getLevelId();
			request.setAttribute("goBackUrl", url);
			logger.debug(" The Back Url is " + url);

			logger.info("Exiting EditConditionAction");
		} catch (Exception e) {
			logger.error("Error in EditConditionAction" + e.getMessage());
		}

		return (mapping.findForward("success"));
	}
}
