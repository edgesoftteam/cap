package elms.control.actions.common;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.CommonAgent;

public class AddAttachmentAction extends Action {

	static Logger logger = Logger.getLogger(AddAttachmentAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		try {

			String levelId = (request.getParameter("levelId") != null) ? request.getParameter("levelId") : (String) request.getAttribute("levelId");
			logger.debug("levelId " + levelId);
			String level = (request.getParameter("level") != null) ? request.getParameter("level") : (String) request.getAttribute("level");
			logger.debug("level " + level);

			List attachments = new CommonAgent().getAttachments(Integer.parseInt(levelId), level);
			logger.debug("obtained attachments list of size " + attachments.size());

			request.setAttribute("attachments", attachments);
			request.setAttribute("levelId", levelId);
			request.setAttribute("level", level);

			return (mapping.findForward("success"));
		} catch (Exception e) {

			logger.error(e.getMessage());

			return (mapping.findForward("error"));
		}
	}
}
