package elms.control.actions.common;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.CommonAgent;
import elms.control.beans.ConditionLibraryForm;

public class SearchConditionLibraryAction extends Action {

	static Logger logger = Logger.getLogger(SearchConditionLibraryAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		MessageResources messages = getResources();
		HttpSession session = request.getSession();

		try {

			ConditionLibraryForm frm = (ConditionLibraryForm) form;

			logger.info("Entering SearchConditionLibraryAction ");
			CommonAgent conditionAgent = new CommonAgent();

			/*
			 * if (mapping.getAttribute() != null) { if ("request".equals(mapping.getScope())) request.removeAttribute(mapping.getAttribute()); else session.removeAttribute(mapping.getAttribute()); }
			 */
			ConditionLibraryForm libSet = conditionAgent.getLibraryItems((ConditionLibraryForm) form);

			form = libSet;
			request.setAttribute("conditionLibraryForm", libSet);
			session.setAttribute("conditionLibraryForm", libSet);

			logger.info("Exiting SearchConditionLibraryAction");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));
	}

}