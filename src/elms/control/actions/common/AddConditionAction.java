package elms.control.actions.common;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.control.beans.ConditionForm;
import elms.security.User;

public class AddConditionAction extends Action {

	static Logger logger = Logger.getLogger(AddConditionAction.class.getName());
	protected ConditionForm conditionForm = null;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		// Extract attributes and parameters we will need
		HttpSession session = request.getSession();
		AdminAgent userAgent = new AdminAgent();

		try {
			int levelId = Integer.parseInt(request.getParameter("levelId"));
			String conditionLevel = request.getParameter("conditionLevel");
			logger.debug("In AddConditionAction with LevelId : " + levelId + " and ConditionLevel : " + conditionLevel);
			ActionForm frm = new ConditionForm();
			conditionForm = (ConditionForm) frm;
			request.setAttribute("conditionForm", frm);
			conditionForm.setLevelId(levelId);
			conditionForm.setConditionLevel(conditionLevel);
			User user = (User) session.getAttribute("user");
			conditionForm.setCreatedBy(user.getUserId());
			conditionForm.setUpdatedBy(user.getUserId());

			String url = request.getContextPath();
			if (conditionLevel.equals("L"))
				url = "/viewLand.do?lsoId=";
			else if (conditionLevel.equals("S"))
				url = "/viewStructure.do?lsoId=";
			else if (conditionLevel.equals("O"))
				url = "/viewOccupancy.do?lsoId=";
			else if (conditionLevel.equals("P"))
				url = "/viewProject.do?projectId=";
			else if (conditionLevel.equals("Q"))
				url = "/viewSubProject.do?subProjectId=";
			else if (conditionLevel.equals("A"))
				url = "/viewActivity.do?activityId=";
			url = request.getContextPath() + url + conditionForm.getLevelId();
			request.setAttribute("goBackUrl", url);

		} catch (Exception e) {
			logger.warn("error " + e.getMessage());
		}
		return (mapping.findForward("success"));

	}

}