package elms.control.actions.common;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.CommonAgent;
import elms.app.common.Attachment;
import elms.util.StringUtils;

public class ViewAttachmentAction extends Action {

	static Logger logger = Logger.getLogger(ViewAttachmentAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		try {

			String attachmentId = request.getParameter("id");
			logger.debug("obtained attachment id as " + attachmentId);
			Attachment attachment = new CommonAgent().getAttachment(StringUtils.s2i(attachmentId));
			logger.debug("obtained the attachment object");
			String fileName = attachment.getFile().getFileName();

			String location = attachment.getFile().getFileLocation();
			String fullFileName = location;
			logger.debug("full name of file is " + fullFileName);
			request.setAttribute("fullFileName", fullFileName);

		} catch (Exception e) {
			logger.error("Error in attachment done action" + e.getMessage());
		}
		return (mapping.findForward("success"));

	}
}