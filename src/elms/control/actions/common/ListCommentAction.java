package elms.control.actions.common;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.CommonAgent;
import elms.agent.LookupAgent;
import elms.control.beans.CommentForm;
import elms.util.StringUtils;

public class ListCommentAction extends Action {
	static Logger logger = Logger.getLogger(ListCommentAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();

		try {
			int levelId = Integer.parseInt(request.getParameter("levelId"));
			String comntLevel = request.getParameter("commentLevel");

			if (comntLevel == null) {
				comntLevel = "";
			}

			logger.debug("Gerring Comment List With Level ID : " + levelId + " and Level : " + comntLevel);

			CommonAgent commentAgent = new CommonAgent();
			CommentForm frm = (CommentForm) form;

			// when Comments selected for delete
			String[] deletedCommentIds = frm.getSelectedComments();

			if (deletedCommentIds != null) {
				for (int i = 0; i < deletedCommentIds.length; i++) {
					logger.debug("Deleted Comment Id  : " + deletedCommentIds[i]);
					commentAgent.deleteComment(StringUtils.s2i(deletedCommentIds[i]));
				}
			}

			frm.setCommentsList(commentAgent.getCommentList(levelId, -1, comntLevel));
			request.setAttribute("commentForm", frm);

			String url = request.getContextPath();

			if (comntLevel.equals("L")) {
				url = "/viewLand.do?lsoId=";
			} else if (comntLevel.equals("S")) {
				url = "/viewStructure.do?lsoId=";
			} else if (comntLevel.equals("O")) {
				url = "/viewOccupancy.do?lsoId=";
			} else if (comntLevel.equals("P")) {
				url = "/viewProject.do?projectId=";
			} else if (comntLevel.equals("Q")) {
				url = "/viewSubProject.do?subProjectId=";
			} else if (comntLevel.equals("A")) {
				String activityType = LookupAgent.getActivityTypeForActId(StringUtils.i2s(levelId));

				if (activityType == null) {
					activityType = "";
				}

				if (activityType.equals("PWFAC")) {
					url = "/pwViewFacilities.do?activityId=";
				} else if (activityType.equals("PWINFO")) {
					url = "/pwViewFacilities.do?activityId=";
				} else if (activityType.equals("PWSAN")) {
					url = "/pwViewSanitation.do.do?activityId=";
				} else if (activityType.equals("PWSEW")) {
					url = "/pwViewSewer.do?activityId=";
				} else if (activityType.equals("PWSTR")) {
					url = "/pwViewStreets.do?activityId=";
				} else if (activityType.equals("PWWAT")) {
					url = "/pwViewWater.do?activityId=";
				} else if (activityType.equals("PWSANC")) {
					url = "/pwViewSanitation.do?activityId=";
				} else if (activityType.equals("PWTSSL")) {
					url = "/pwViewTrafficSignals.do?activityId=";
				} else if (activityType.equals("PWELEC")) {
					url = "/pwViewElectrical.do?activityId=";
				} else {
					url = "/viewActivity.do?activityId=";
				}
			} else if (comntLevel.equals("Z")) {
				url = "/editPeople.do?id=";
			}

			url = request.getContextPath() + url + levelId;

			logger.debug("Url to Back is : " + url);
			request.setAttribute("goBackUrl", url);

			logger.debug("Exiting ListCommentAction");
		} catch (Exception e) {
			logger.warn("Warning in List Comments Action" + e.getMessage());
		}

		return (mapping.findForward("success"));
	}
}
