package elms.control.actions.common;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class LogoutAction extends Action {

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		String f = request.getParameter("f")!=null ? request.getParameter("f"):"";
		HttpSession session = request.getSession();
		session.invalidate();

		if(f.equalsIgnoreCase("o")){
			return (mapping.findForward("successOnline"));
		}
		else{
			return (mapping.findForward("success"));
		}
		

	}

}