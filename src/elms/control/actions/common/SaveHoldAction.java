package elms.control.actions.common;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.CommonAgent;
import elms.app.common.Hold;
import elms.control.beans.HoldForm;
import elms.security.User;
import elms.util.StringUtils;

public class SaveHoldAction extends Action {

	static Logger logger = Logger.getLogger(SaveHoldAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		// Extract attributes and parameters we will need
		logger.debug("inside saveHoldAction");
		HttpSession session = request.getSession();
		int holdId = Integer.parseInt(request.getParameter("id"));
		logger.debug("holdid " + holdId);
		int levelId = Integer.parseInt(request.getParameter("levelId"));
		logger.debug("levelId" + levelId);
		String level = request.getParameter("level");
		logger.debug("level" + level);
		String statusDate = request.getParameter("statusDate");
		logger.debug("statusDate " + statusDate);

		try {
			logger.info("Entering SaveHoldAction :: holdId : " + holdId);

			// make the form like below -- if not coded, struts will assume a null form - AB(4-24-2002)
			HoldForm holdForm = (HoldForm) form;
			logger.debug(holdForm);

			Hold hold = new Hold();
			hold.setHoldId(holdId);
			logger.debug(" set holdId to hold " + holdId);
			hold.setHoldLevel(level);
			hold.setLevelId(levelId);
			// get user who logged on from session
			User user = (User) session.getAttribute("user");
			if (holdId == 0) { // for addition
				hold.setComment(holdForm.getComments());
				logger.debug(" Commnents after method replaceQuot " + holdForm.getComments());
			} else { // for modification
			// hold.setComment("<BR>----------"+user.getUsername()+"--"+StringUtils.cal2str(Calendar.getInstance())+"---------<BR>"+ holdForm.getComments());
				hold.setComment(holdForm.getComments());
				logger.debug(" Commnents after method replaceQuot " + "<BR>----------" + user.getUsername() + "--" + StringUtils.cal2str(Calendar.getInstance()) + "---------<BR>" + holdForm.getComments());
			}
			hold.setType(holdForm.getType());
			hold.setStatus(holdForm.getStatus());
			hold.setTitle(holdForm.getTitle());
			// hold.setCreateDate(holdForm.getCreatedDate());
			// hold.setUpdateDate(holdForm.getUpdatedDate());

			// hold.setStatusDate(StringUtils.str2cal(holdForm.getStatusDate()));
			hold.setStatusDate(StringUtils.str2cal(statusDate));
			logger.debug("status in hold is $$$$$$$ " + hold.getStatusDate());

			// hold.setEnteredBy(holdForm.getCreatedBy());
			// hold.setUpdatedBy(holdForm.getUpdatedBy());

			logger.debug("enterBy###hold " + hold.getEnteredBy() + "enterBy###form " + holdForm.getCreatedBy());

			hold.setUpdatedBy(user.getUserId());
			hold.setEnteredBy(user.getUserId());
			logger.debug("UpdatedBy :" + hold.getUpdatedBy());

			CommonAgent holdAgent = new CommonAgent();
			int id = holdAgent.saveHold(hold);

			request.setAttribute("id", StringUtils.i2s(id));
			request.setAttribute("levelId", request.getParameter("levelId"));
			request.setAttribute("level", level);

			/*
			 * if (mapping.getAttribute() != null) { if ("request".equals(mapping.getScope())) request.removeAttribute(mapping.getAttribute()); else session.removeAttribute(mapping.getAttribute()); }
			 * 
			 * if (hold != null) {
			 * 
			 * CachedRowSet holdRowSet = new CachedRowSet(); holdRowSet = holdAgent.getHoldList(level,levelId); request.setAttribute("holdList",holdRowSet); request.setAttribute("id",StringUtils.i2s(id)); request.setAttribute("levelId",request.getParameter("levelId")); request.setAttribute("level",level);
			 * 
			 * 
			 * } logger.info("Exiting SaveHoldAction");
			 */
		} catch (Exception e) {
			logger.warn("Error in SaveHoldAction" + "/n" + e);
		}

		return (mapping.findForward("success"));
	}

}