package elms.control.actions.common;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.AdminAgent;
import elms.agent.CommonAgent;
import elms.app.common.Condition;
import elms.app.common.ConditionLibraryRecord;
import elms.control.beans.ConditionLibraryForm;
import elms.security.User;
import elms.util.StringUtils;

public class AddToConditionAction extends Action {
	static Logger logger = Logger.getLogger(AddToConditionAction.class.getName());
	String forward = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		List list = new ArrayList();
		AdminAgent userAgent = new AdminAgent();
		User user = (User) session.getAttribute("user");

		if (user == null) {
			user = new User();
		}

		CommonAgent conditionAgent = new CommonAgent();

		try {
			logger.info("Entering AddToConditionAction ");

			ConditionLibraryForm conditionLibraryForm = (ConditionLibraryForm) session.getAttribute("conditionLibraryForm");

			// these are the records that contain all conditions selected from library
			ConditionLibraryRecord[] recList = conditionLibraryForm.getConditionLibraryRecords();

			String condId = conditionLibraryForm.getCondId(); // this condId will not be used, may be removed later

			String level = conditionLibraryForm.getConditionLevel();
			String levelId = conditionLibraryForm.getLevelId();

			// create Condition objects for every record and send them to condition agent
			for (int i = 0; i < recList.length; i++) {
				ConditionLibraryRecord rec = (ConditionLibraryRecord) recList[i];
				String select = rec.getSelect();

				if (select.equals("on")) {
					Condition condition = new Condition();
					String inspectability = rec.getInspectability();

					if (inspectability == null) {
						inspectability = "N";
					}

					if (inspectability.equalsIgnoreCase("No")) {
						inspectability = "N";
					}

					if (inspectability.equalsIgnoreCase("Yes")) {
						inspectability = "Y";
					}

					String warning = rec.getWarning();

					if (warning == null) {
						warning = "N";
					}

					if (warning.equalsIgnoreCase("No")) {
						warning = "N";
					}

					if (warning.equalsIgnoreCase("Yes")) {
						warning = "Y";
					}

					// condition.setConditionId((new Integer(condId)).intValue());
					condition.setConditionId(StringUtils.s2i(rec.getConditionId())); // when Id is 0 a new record is created
					condition.setConditionLevel(level);
					condition.setLevelId((new Integer(levelId)).intValue());
					condition.setShortText(rec.getShort_text());
					condition.setText(rec.getCondition_text());
					condition.setInspectable(inspectability);
					condition.setWarning(warning);
					condition.setComplete("N");
					condition.setLibraryId(rec.getLibraryId());

					condition.setCreatedBy(user.getUserId());
					condition.setUpdatedBy(user.getUserId());

					list.add(condition);
					conditionAgent.saveCondition(condition);

					// }
				}
			}

			request.setAttribute("levelId", levelId);
			request.setAttribute("conditionLevel", level);

			logger.info("Exiting AddToConditionAction.");
			// forward is: "+ inspctForm.getAddOrEdit());
			forward = "addPage";
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward(forward));
	}
}
