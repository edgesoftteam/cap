package elms.control.actions.common;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.CommonAgent;
import elms.control.beans.ConditionLibraryForm;

public class ViewConditionLibraryPageAction extends Action {
	static Logger logger = Logger.getLogger(ViewConditionLibraryPageAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		String forward = "listPage";

		try {
			String level = request.getParameter("conditionLevel");
			String levelId = request.getParameter("levelId");
			logger.debug("Entering ViewConditionLibraryPageAction with LevelId : " + levelId + " and ConditionLevel : " + level);

			CommonAgent conditionAgent = new CommonAgent();
			ConditionLibraryForm conditionLibraryForm = (ConditionLibraryForm) form;

			String type = conditionAgent.getConditionType(level, levelId);
			logger.debug("type is - " + type);
			conditionLibraryForm.setType(type);

			ConditionLibraryForm libSet = conditionAgent.getLibraryItems((ConditionLibraryForm) form);

			request.setAttribute("conditionLibraryForm", conditionLibraryForm);
			session.setAttribute("conditionLibraryForm", conditionLibraryForm);

			logger.debug("Exiting ViewConditionLibraryPageAction");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward(forward));
	}
}
