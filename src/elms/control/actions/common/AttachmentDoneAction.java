package elms.control.actions.common;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.control.actions.lso.ViewLandAction;
import elms.control.actions.lso.ViewOccupancyAction;
import elms.control.actions.lso.ViewStructureAction;
import elms.control.actions.project.ViewActivityAction;
import elms.control.actions.project.ViewProjectAction;
import elms.control.actions.project.ViewSubProjectAction;
import elms.util.StringUtils;

public class AttachmentDoneAction extends Action {
	static Logger logger = Logger.getLogger(AttachmentDoneAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		String nextPage = "";

		try {
			// need not refresh the PSA tree so onloadAction is setting to ""
			String onloadAction = "";
			logger.debug("Setting the refresh tree flag to " + onloadAction);
			session.setAttribute("onloadAction", onloadAction);

			String strLevelId = request.getParameter("levelId");
			int levelId = StringUtils.s2i(strLevelId);
			logger.debug("Got Level Id from request as " + strLevelId);

			String level = request.getParameter("level");
			logger.debug("Got Level  from request as " + level);
			String active = "Y";

			if (level.equals("L")) {
				ViewLandAction viewLandAction = new ViewLandAction();
				viewLandAction.processLand(request, levelId, "0");
			} else if (level.equals("S")) {
				ViewStructureAction viewStructureAction = new ViewStructureAction();
				viewStructureAction.processStructure(request, strLevelId, "0");
			} else if (level.equals("O")) {
				ViewOccupancyAction viewOccupancyAction = new ViewOccupancyAction();
				viewOccupancyAction.processOccupancy(request, levelId, "0");
			} else if (level.equals("P")) {
				ViewProjectAction viewProjectAction = new ViewProjectAction();
				viewProjectAction.getProject(levelId, request);
			} else if (level.equals("Q")) {
				ViewSubProjectAction viewSubProjectAction = new ViewSubProjectAction();
				viewSubProjectAction.doRequestProcess(request, levelId,active);
			} else {
				ViewActivityAction viewActivityAction = new ViewActivityAction();
				viewActivityAction.getActivity(levelId, request);
			}

			nextPage = level;
			logger.debug("Next Page is set to " + nextPage);
		} catch (Exception e) {
			logger.error("Error in attachment done action" + e.getMessage());
		}

		return (mapping.findForward(nextPage));
	}
}
