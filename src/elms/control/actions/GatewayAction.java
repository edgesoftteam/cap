package elms.control.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.common.Constants;
import elms.security.Group;
import elms.security.User;

public final class GatewayAction extends Action {

	static Logger logger = Logger.getLogger(GatewayAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		MessageResources messages = getResources();
		HttpSession session = request.getSession();

		try {
			ActionErrors errors = new ActionErrors();

			User user = new User();

			// This is required for business tax to work without a logon as the jsp checks for user groups.
			List userGroups = new ArrayList();
			Group group = new Group();
			group.setGroupId(0);
			userGroups.add(group);
			user.setGroups(userGroups);

			session.invalidate();
			session = request.getSession();

			session.setAttribute(Constants.USER_KEY, user);

			logger.debug("blank user object put in the session");

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));
	}
}