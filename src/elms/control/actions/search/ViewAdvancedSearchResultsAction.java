package elms.control.actions.search;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.InspectionAgent;
import elms.agent.ProjectAgent;
import elms.app.project.Activity;
import elms.control.beans.BasicSearchForm;

public class ViewAdvancedSearchResultsAction extends Action {
	static Logger logger = Logger.getLogger(ViewAdvancedSearchResultsAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();

		String nextPage = "";

		BasicSearchForm basicSearchForm = (BasicSearchForm) form;

		boolean searchProject = false;
		boolean searchActivity = false;
		boolean searchInspection = false;

		try {
			String query = basicSearchForm.getQuery();
			logger.debug("The query is " + query);

			// validate the query for any insert, update and delete words in there.
			query = query.trim();
			String queryType = query.substring(0, 6);
			logger.debug(queryType);
			if (!(queryType.equalsIgnoreCase("select"))) {
				throw new Exception("Error: Only SELECT statement is allowed in advanced search");
			}

			// proceed further to process the query.

			String tableStr = basicSearchForm.getTable();
			logger.debug("The table string is " + tableStr);

			int table = Integer.parseInt(tableStr);

			if (table == 1) { // Activities
				searchActivity = true;

				ActivityAgent act = new ActivityAgent();
				List actList = act.searchActivities(query);

				request.setAttribute("actList", actList);
				request.setAttribute("activityListSize", "" + actList.size());
				basicSearchForm.setActivityList(actList);
				basicSearchForm.setActivityListSize("" + actList.size());

				if (actList.size() == 1) {
					Activity activity = (Activity) actList.get(0);
					basicSearchForm.setActivityId(activity.getActivityId());
					basicSearchForm.setLsoId(activity.getActivityDetail().getLsoId());
					request.setAttribute("activityId", "" + activity.getActivityId());
					request.setAttribute("lsoId", "" + activity.getActivityDetail().getLsoId());
					logger.debug("Activity Id is " + activity.getActivityId());
					logger.debug("Lso ID is " + activity.getActivityDetail().getLsoId());
				}
			} else if (table == 2) { // Projects
				searchProject = true;

				ProjectAgent proj = new ProjectAgent();
				List projList = proj.searchProjects(query);
				request.setAttribute("projList", projList);
				request.setAttribute("projectListSize", "" + projList.size());
			} else if (table == 3) { // Inspections
				searchInspection = true;

				InspectionAgent insp = new InspectionAgent();
				List inspList = insp.searchInspections(query);
				request.setAttribute("inspList", inspList);
				request.setAttribute("inspectionListSize", "" + inspList.size());
			}

			// redirections
			if (searchProject) {
				nextPage = "successProject";
			}

			if (searchInspection) {
				nextPage = "successInspection";
			}

			if (searchActivity) {
				nextPage = "successActivity";
			}

			logger.debug("forwarding to next page " + nextPage);

		} catch (Exception e) {
			logger.error("exception thrown " + e.getMessage());
			request.setAttribute("message", e.getMessage());
			nextPage = "back";
		}

		return (mapping.findForward(nextPage));

	}
}
