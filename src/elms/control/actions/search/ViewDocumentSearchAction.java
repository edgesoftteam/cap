package elms.control.actions.search;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * Shows the document search screen
 * 
 * @author Anand Belaguly (anand@edgesoftinc.com) updated - Jan 2, 2004,2:36:08 PM - Anand Belaguly
 */
public class ViewDocumentSearchAction extends Action {

	static Logger logger = Logger.getLogger(ViewDocumentSearchAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.info("Entering ViewDocumentSearchAction");

		// do nothing...

		logger.info("Exiting ViewBasicSearchAction");
		return (mapping.findForward("success"));

	}

}