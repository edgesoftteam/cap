package elms.control.actions.people;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;

public class ViewAssociatedActivitiesAction extends Action {

	static Logger logger = Logger.getLogger(ViewAssociatedActivitiesAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.info("Entering ViewAssociatedActivitiesAction");

		try {
			String peopleId = request.getParameter("peopleId");
			String psaId = request.getParameter("psaId");
			String psaType = request.getParameter("psaType");

			logger.debug("PeopleId :" + peopleId + " : PsaId: " + psaId + " : PsaType  : " + psaType);

			List associatedActivities = new ActivityAgent().getActivitiesForPeople(peopleId);
			logger.debug("obtained size results of size " + associatedActivities.size());
			request.setAttribute("associatedActivities", associatedActivities);

			request.setAttribute("peopleId", peopleId);
			request.setAttribute("psaId", psaId);
			request.setAttribute("psaType", psaType);
			logger.info("Exiting ViewTransactionAction");

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));

	}

}