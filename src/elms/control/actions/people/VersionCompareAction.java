/*
 * Created on Oct 24, 2007
 * Window - Preferences - Java - Code Style - Code Templates
 */
package elms.control.actions.people;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.PeopleAgent;
import elms.app.people.People;
import elms.control.beans.PeopleForm;

/**
 * @author Manjuprasad Window - Preferences - Java - Code Style - Code Templates
 */
public class VersionCompareAction extends Action {

	static Logger logger = Logger.getLogger(VersionCompareAction.class.getName());
	String nextPage = "";
	String peopleId = "";
	String typeId = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		PeopleForm peopleForm = (PeopleForm) form;
		PeopleAgent peopleAgent = new PeopleAgent();
		People peopleVer1 = new People();
		People peopleVer2 = new People();
		try {
			peopleId = (String) request.getParameter("peopleId");

			logger.debug("People Id is : " + peopleId);

			typeId = (String) request.getParameter("typeId");
			logger.debug("Type Id is : " + typeId);

			String versions = (String) request.getParameter("checkboxArray");
			logger.debug("Versions List " + versions);

			String[] versionArray = new String[2];
			versionArray = versions.split(",");
			logger.debug("versionArray[0] is " + versionArray[0]);
			logger.debug("versionArray[1] is " + versionArray[1]);

			peopleVer1 = peopleAgent.getVersionValueList(peopleId, versionArray[0], typeId);
			peopleVer2 = peopleAgent.getVersionValueList(peopleId, versionArray[1], typeId);

			request.setAttribute("peopleId", peopleId);
			request.setAttribute("versions", versions);
			request.setAttribute("peopleVer1", peopleVer1);
			request.setAttribute("peopleVer2", peopleVer2);

			nextPage = typeId;
		} catch (Exception e) {
		}

		return (mapping.findForward(nextPage));

	}
}