package elms.control.actions.people;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.LookupAgent;
import elms.app.bl.BusinessLicenseActivity;
import elms.app.people.People;
import elms.common.Constants;
import elms.control.beans.PeopleForm;
import elms.util.StringUtils;

public class AddPeopleAction extends Action {
	static Logger logger = Logger.getLogger(AddPeopleAction.class.getName());
	String nextPage = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession(true);
		BusinessLicenseActivity businessLicenseActivity = new BusinessLicenseActivity();

		try {
			String peopleType = (String) session.getAttribute("peopleType");
			logger.debug("obtained people type from session " + peopleType);

			String peopleTypeId = (String) session.getAttribute("peopleTypeId");
			logger.debug("obtained people typeId from session " + peopleTypeId);

			request.setAttribute("peopleTypeId", peopleTypeId);
			request.setAttribute("peopleType", peopleType);

			String addEditFlag = Constants.ADD;
			logger.debug("Add edit flag is " + addEditFlag);
			request.setAttribute("addEditFlag", addEditFlag);

			nextPage = peopleTypeId;
			logger.debug("  next page is " + nextPage);

			String psaId = (request.getParameter("psaId")) != null ? request.getParameter("psaId") : "";

			if ((psaId == null) || (psaId == "")) {
				psaId = (String) session.getAttribute(Constants.PSA_ID);
			}

			logger.debug("obtained psa id from request as " + psaId);

			String psaType = (request.getParameter("psaType")) != null ? request.getParameter("psaType") : "";

			if ((psaType == null) || (psaType == "")) {
				psaType = (String) session.getAttribute(Constants.PSA_TYPE);
			}

			logger.debug("obtained psa type from request as " + psaType);
			

			if (psaId == null) {
				logger.info("PSA is null");
			} else {
				int lsoId ;
				if(psaType.equalsIgnoreCase("O")){
					lsoId = StringUtils.s2i(psaId);
				} else {
					lsoId = LookupAgent.getLsoIdForPsaId(psaId, psaType);
				}
				
				logger.debug("lso Id is " + lsoId);
				businessLicenseActivity = new ActivityAgent().getBusinessLicenseActivity(StringUtils.s2i(psaId), lsoId);
			}

			PeopleForm peopleForm = new PeopleForm();
			peopleForm.setPsaId(psaId);
			peopleForm.setPsaType(psaType);

			People people = new People();
			people.setLicenseNbr((String) session.getAttribute("p_licenseNumber"));
			people.setName((String) session.getAttribute("p_name"));

			request.setAttribute("psaId", psaId);
			request.setAttribute("psaType", psaType);
			request.setAttribute("peopleForm", peopleForm);
			request.setAttribute("businessLicenseActivity", businessLicenseActivity);
			request.setAttribute("people", people);
			logger.debug("business name is " + peopleForm.getPeople().getBusinessName());
			return (mapping.findForward(nextPage));
		} catch (Exception e) {
			logger.error(e.getMessage());
			return (mapping.findForward("error"));
		}

	}
}
