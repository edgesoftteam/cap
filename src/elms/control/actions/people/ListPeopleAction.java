package elms.control.actions.people;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.LookupAgent;
import elms.agent.PeopleAgent;
import elms.common.Constants;
import elms.util.StringUtils;

public class ListPeopleAction extends Action {
	static Logger logger = Logger.getLogger(ListPeopleAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();

		try {
			String psaId = StringUtils.nullReplaceWithEmpty((String) request.getParameter("id"));

			if (psaId.trim().equals("")) {
				psaId = (String) session.getAttribute(Constants.PSA_ID);
			}

			logger.debug("obtained psa id from request as " + psaId);

			String psaType = StringUtils.nullReplaceWithEmpty((String) request.getParameter("type"));
			
			logger.debug("psaType"+psaType);

			if (psaType.trim().equals("")) {
				psaType = (String) session.getAttribute(Constants.PSA_TYPE);
			}

			logger.debug("obtained psa type from request as " + psaType);
			
			//Added for getting the land id for structure
			if(psaType.equals("S"))
			{
				int lsoid=LookupAgent.getLsoId(Integer.parseInt(psaId),"S");
				psaId=""+lsoid;
				psaType="L";
			}
			

			List peopleList = new PeopleAgent().getPeoples(StringUtils.s2i(psaId), psaType, 0);
			
			/*
			 * Added by Mukesh for Setting the Acitivty Id and level as 'A', when an Activity is selected.
			 * */
			String levelType = StringUtils.nullReplaceWithEmpty((String) request.getParameter("levelType"));
			
			if (!levelType.trim().equals("")) {
				psaType =(String) request.getParameter("levelType");
			}
			
			String levelId = StringUtils.nullReplaceWithEmpty((String) request.getParameter("levelId"));
			
			if (!levelId.trim().equals("")) {
				psaId =(String) request.getParameter("levelId");
			}
			
			request.setAttribute("peopleList", peopleList);
			request.setAttribute("psaId", psaId);
			request.setAttribute("psaType", psaType);
			request.setAttribute("ed", "true");

			session.setAttribute("fromWhere", "List");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		logger.debug("Exit from List People Action");

		return (mapping.findForward("success"));
	}
}
