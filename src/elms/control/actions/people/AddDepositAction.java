package elms.control.actions.people;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import sun.jdbc.rowset.CachedRowSet;
import elms.agent.FinanceAgent;
import elms.common.Constants;
import elms.util.StringUtils;

public class AddDepositAction extends Action {
	static Logger logger = Logger.getLogger(AddDepositAction.class.getName());
	String nextPage = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		FinanceAgent financeAgent = new FinanceAgent();
		CachedRowSet rs = null; // new CachedRowSet();

		try {
			String peopleType = (String) session.getAttribute("peopleType");
			logger.debug("obtained people type from session " + peopleType);

			String strPeopleId = request.getParameter("peopleId");
			logger.debug("peopleId from parameter " + strPeopleId);

			if ((strPeopleId == null) || (strPeopleId == "")) {
				strPeopleId = (String) session.getAttribute("peopleId");
				logger.debug("peopleId from session" + strPeopleId);
			}

			logger.debug("obtained people id  as " + strPeopleId);
			request.setAttribute("peopleId", strPeopleId);

			String psaId = (String) request.getParameter("psaId");

			if ((psaId == null) || (psaId == "")) {
				psaId = (String) session.getAttribute(Constants.PSA_ID);
			}

			logger.debug("obtained psa id from request as " + psaId);

			String psaType = (String) request.getParameter("psaType");

			if ((psaType == null) || (psaType == "")) {
				psaType = (String) session.getAttribute(Constants.PSA_TYPE);
			}

			logger.debug("obtained psa type from request as " + psaType);

			request.setAttribute("peopleType", peopleType);

			String addEditFlag = Constants.ADD;
			request.setAttribute("addEditFlag", addEditFlag);
			request.setAttribute("psaId", psaId);
			request.setAttribute("psaType", psaType);

			rs = financeAgent.depositDetails(StringUtils.s2i(strPeopleId));
			request.setAttribute("rs", rs);
			nextPage = "success";
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward(nextPage));
	}
}
