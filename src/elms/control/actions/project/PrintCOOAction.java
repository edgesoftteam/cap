package elms.control.actions.project;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;
import org.jdom.Document;

import elms.agent.ActivityAgent;
import elms.util.Doc2Pdf;
import elms.util.StringUtils;

public class PrintCOOAction extends Action {
	static Logger logger = Logger.getLogger(PrintCOOAction.class.getName());

	protected String actNbr = "";
	protected String actType = "";
	protected String pdfFileName = "";
	protected String xslFileName = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.debug("Entered Into the PrintCOOAction");

		MessageResources messages = getResources();
		ActionErrors errors = new ActionErrors();

		if (!errors.empty()) {
			saveErrors(request, errors);
			saveToken(request);
			logger.info("Entered PrintCOOAction with errors");

			return (new ActionForward(mapping.getInput()));
		} else {
			try {
				actNbr = (String) request.getParameter("actNbr");
				logger.debug("actNbr is " + actNbr);

				xslFileName = "coo.xsl";

				ActivityAgent ra = new ActivityAgent();
				Document document = ra.getCOOJDOM(actNbr);

				pdfFileName = actNbr + "-" + actType + "-" + StringUtils.cal2Ts(Calendar.getInstance()) + ".pdf";
				request.setAttribute("pdfFileName", pdfFileName);
				logger.debug("pdf file name is set in request " + pdfFileName);

				if (document != null) {
					String getRealPath = this.getServlet().getServletContext().getRealPath("/");
					logger.debug("Real path is " + getRealPath);
					xslFileName = getRealPath + "xsl" + File.separator + xslFileName;
					pdfFileName = getRealPath + "pdf" + File.separator + pdfFileName;
					Doc2Pdf.start(document, xslFileName, pdfFileName);
				}

			} catch (Exception e) {
				logger.warn(e.getMessage());
			}
		}

		return (mapping.findForward("success"));
	}
}
