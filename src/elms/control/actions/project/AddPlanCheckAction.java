package elms.control.actions.project;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.common.Constants;
import elms.control.beans.PlanCheckForm;
import elms.util.StringUtils;

public class AddPlanCheckAction extends Action {
	static Logger logger = Logger.getLogger(AddPlanCheckAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.debug("Entered Into AddPlanCheckAction ....");

		String strActivityId = "";
		int activityId;

		HttpSession session = request.getSession();

		try {
			PlanCheckForm planCheckForm = new PlanCheckForm();
			ActivityAgent planCheckAgent = new ActivityAgent();

			strActivityId = request.getParameter("activityId");

			if ((strActivityId == null) || strActivityId.equals("")) {
				strActivityId = (String) session.getAttribute(Constants.PSA_ID);
			}

			activityId = StringUtils.s2i(strActivityId);

			ActivityAgent activityAgent = new ActivityAgent();
			String projectId = activityAgent.getProjectId(strActivityId);

			String engineer = planCheckAgent.getEngFromProj(projectId);

			planCheckForm.setEngineerId(engineer);
			logger.debug("engineer Id " + engineer);

			planCheckForm.setActivityId(strActivityId);
			logger.debug("activity Id is set to plancheck form as " + activityId);

			// It is to display the History of Plan Check
			planCheckForm.setPlanChecks(planCheckAgent.getPlanChecks(StringUtils.s2i(strActivityId)));
			logger.debug("Plan Check History is set to Plan check form");

			planCheckForm.setPlanCheckDate(StringUtils.cal2str(Calendar.getInstance()));
			logger.debug("Plan Check Date: " + planCheckForm.getPlanCheckDate());

			planCheckForm.setEngineerId(StringUtils.i2s(planCheckAgent.getPlanReviewEngineer(strActivityId)));
			logger.debug("Plan Check Engineer: " + planCheckForm.getEngineerId());

			request.setAttribute("planCheckForm", planCheckForm);
			request.setAttribute("activityId", strActivityId);

			logger.debug("Exit of  AddPlanCheckAction ....");
			return (mapping.findForward("success"));
		} catch (Exception e) {
			logger.error(e.getMessage());
			return (mapping.findForward("error"));
		}

	}
}
