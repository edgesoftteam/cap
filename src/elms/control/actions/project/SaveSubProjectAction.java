package elms.control.actions.project;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.LookupAgent;
import elms.agent.ProjectAgent;
import elms.app.admin.ProjectType;
import elms.app.admin.SubProjectSubType;
import elms.app.admin.SubProjectType;
import elms.app.finance.SubProjectFinance;
import elms.app.project.SubProject;
import elms.app.project.SubProjectDetail;
import elms.common.Constants;
import elms.control.beans.SubProjectForm;
import elms.security.User;
import elms.util.StringUtils;

public class SaveSubProjectAction extends Action {

	static Logger logger = Logger.getLogger(SaveSubProjectAction.class.getName());
	protected List subProjects = null;
	protected SubProject subProject = null;
	protected SubProjectDetail subProjectDetail = null;
	protected SubProjectFinance subProjectFinance = null;
	protected ProjectType projectType = null;
	protected SubProjectType subProjectType = null;
	protected SubProjectSubType subProjectSubType = null;
	protected int projectId = -1;
	protected int subProjectId = -1;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.debug("Entered Into the saveSubProjectAction");
		MessageResources messages = getResources();
		HttpSession session = request.getSession();

		SubProjectForm subProjectForm = (SubProjectForm) form;
		ActionErrors errors = new ActionErrors();

		if (!errors.empty()) {
			saveErrors(request, errors);
			saveToken(request);
			return (new ActionForward(mapping.getInput()));
		} else {
			try {

				projectId = StringUtils.s2i(request.getParameter("projectId"));
				logger.debug("saveSubProjectAction -- got parameter projectId as " + projectId);

				// setting the subProject Details
				subProjectDetail = new SubProjectDetail();
				projectType = new ProjectType();
				projectType.setProjectTypeId(StringUtils.s2i(subProjectForm.getSubProject()));
				logger.debug("set   projectType -- projectTypeId   as   " + projectType.getProjectTypeId());
				subProjectType = new SubProjectType();
				subProjectType.setSubProjectTypeId(StringUtils.s2i(subProjectForm.getType()));
				logger.debug("set  subProjectType -- subProjectTypeId   as   " + subProjectType.getSubProjectTypeId());
				subProjectSubType = new SubProjectSubType();
				subProjectSubType.setSubProjectSubTypeId(StringUtils.s2i(subProjectForm.getSubType()));
				logger.debug("set  SubProjectSubType -- subProjectSubTypeId   as   " + subProjectSubType.getSubProjectSubTypeId());

				subProjectDetail.setProjectType(projectType);
				subProjectDetail.setSubProjectType(subProjectType);
				subProjectDetail.setSubProjectSubType(subProjectSubType);
				subProjectDetail.setDescription(subProjectForm.getDescription());
				subProjectDetail.setCreatedBy((User) session.getAttribute("user"));
				subProjectDetail.setUpdatedBy((User) session.getAttribute("user"));
				int projectNameId = -1;
				try {
					projectNameId = LookupAgent.getProjectNameId("P", projectId);
				} catch (Exception e5) {
					logger.error("Exception occured while getting project name id " + e5.getMessage());
				}
				logger.debug("Obtained project name id is" + projectNameId);

				if (projectNameId != Constants.PROJECT_NAME_PLANNING_ID) {
					subProjectDetail.setStatus("1");
				} else {
					subProjectDetail.setStatus("4");
				}

				logger.debug("set  SubProjectDetail  Object  " + subProjectDetail.toString());

				// Not really using the below objects...but required
				subProjectFinance = new SubProjectFinance();
				List subProjectActivityList = new ArrayList();
				List subProjectHoldList = new ArrayList();
				List subProjectConditionList = new ArrayList();
				List subProjectAttachmentList = new ArrayList();
				List subProjectCommentList = new ArrayList();

				subProject = new SubProject(0, projectId, subProjectDetail, subProjectFinance, subProjectActivityList, subProjectHoldList, subProjectConditionList, subProjectAttachmentList, subProjectCommentList);

				// adding the sub project to database
				subProjectId = new ProjectAgent().addSubProject(subProject);
				logger.debug("SaveSubProjectAction -- got subProject Id from database as  " + subProjectId);
				if (subProjectId > 0)
					session.setAttribute("refreshPSATree", "Y");

				ViewSubProjectAction viewSubProjectAction = new ViewSubProjectAction();
				String active = "Y";
				viewSubProjectAction.doRequestProcess(request, subProjectId,active);
				// removing session object
				session.removeAttribute("subProjectForm");
			} catch (Exception e) {
				logger.error("Exception in SaveSubProjectAction " + e.getMessage());
			}
		}
		return (mapping.findForward("success"));

	}

}