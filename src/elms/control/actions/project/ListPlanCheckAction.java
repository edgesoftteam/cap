package elms.control.actions.project;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.ActivityAgent;
import elms.control.beans.PlanCheckForm;
import elms.util.StringUtils;

public class ListPlanCheckAction extends Action {
	static Logger logger = Logger.getLogger(ListPlanCheckAction.class.getName());
	protected int activityId = -1;
	protected String strActivityId = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.debug("Entered Into the ListPlanCheckAction");
		MessageResources messages = getResources();
		HttpSession session = request.getSession();

		ActionErrors errors = new ActionErrors();
		if (!errors.empty()) {
			saveErrors(request, errors);
			saveToken(request);
			logger.info("Entered ListPlanCheckAction with errors and came Out ");
			return (new ActionForward(mapping.getInput()));
		} else {
			try {
				strActivityId = request.getParameter("activityId");
				if (strActivityId == null || strActivityId.equals("")) {
					strActivityId = (String) session.getAttribute("activityId");
				}
				activityId = StringUtils.s2i(strActivityId);
				ActivityAgent agent = new ActivityAgent();

				PlanCheckForm frm = new PlanCheckForm();
				frm.setPlanChecks(agent.getPlanChecks(activityId));
				request.setAttribute("planCheckForm", frm);

				// List peopleList = new PeopleAgent().getPeoples(new Integer(activityId).intValue(), "A", 3);
				// request.setAttribute("peopleList", peopleList);

			} catch (Exception e) {
				logger.warn(e.getMessage());
			}
		}

		logger.debug("Exiting ListPlanCheckAction");
		return (mapping.findForward("success"));
	}
}