package elms.control.actions.project;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.CommonAgent;
import elms.util.StringUtils;

public class ViewActivityTeamAction extends Action {

	public ViewActivityTeamAction() {
	}

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.debug("Entered Into the viewActivityTeamAction");
		try {
			int subProjectId = StringUtils.s2i(request.getParameter("subProjectId"));
			if (subProjectId < 0)
				subProjectId = StringUtils.s2i((String) request.getAttribute("subProjectId"));
			logger.debug(" Print subProjectId: " + subProjectId);
			java.util.List activityTeamList = (new CommonAgent()).getActivityTeamList(subProjectId);
			request.setAttribute("activityTeamList", activityTeamList);
		} catch (Exception e) {
			logger.error("Error in viewActivityTeamAction " + e.getMessage());
		}
		return mapping.findForward("success");
	}

	static Logger logger;

	static {
		logger = Logger.getLogger(elms.control.actions.project.ViewActivityTeamAction.class.getName());
	}
}
