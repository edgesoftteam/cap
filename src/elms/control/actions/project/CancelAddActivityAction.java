package elms.control.actions.project;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.ProjectAgent;
import elms.control.beans.ActivityForm;
import elms.util.StringUtils;

public class CancelAddActivityAction extends Action {

	static Logger logger = Logger.getLogger(CancelAddActivityAction.class.getName());
	protected int projectId = -1;
	protected int subProjectId = -1;
	protected String nextPage;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.debug("Entered Into CancelAddActivityAction ....");

		MessageResources messages = getResources();
		HttpSession session = request.getSession();

		// need not refresh PSA tree so onloadAction is set to ""
		String onloadAction = "";
		logger.debug("Setting the refresh tree flag to " + onloadAction);
		session.setAttribute("onloadAction", onloadAction);

		// removing the activityForm from session which was kept in addActivity
		session.removeAttribute("activityForm");
		session.removeAttribute("temporaryParkingForm");

		ActivityForm activityForm = (ActivityForm) form;
		ActionErrors errors = new ActionErrors();

		if (!errors.empty()) {
			saveErrors(request, errors);
			saveToken(request);
			return (new ActionForward(mapping.getInput()));
		} else {
			try {
				subProjectId = StringUtils.s2i(request.getParameter("subProjectId"));
				String origin = activityForm.getOrigin();

				// removing the activityForm from session which was kept in addActivity
				session.removeAttribute("activityForm");

				if (origin.equals("P")) {
					nextPage = "project";
					projectId = new ProjectAgent().getProjectId(subProjectId);
					logger.debug("***********projectID is" + projectId);
					ViewProjectAction viewProjectAction = new ViewProjectAction();
					viewProjectAction.getProject(projectId, request);
				} else {
					nextPage = "subProject";
					String active = "Y";
					ViewSubProjectAction viewSubProjectAction = new ViewSubProjectAction();
					viewSubProjectAction.doRequestProcess(request, subProjectId,active);
				}

			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}
		logger.debug("Exit Cancel Add Activity Action ... Next Page is set to " + nextPage);
		return (mapping.findForward(nextPage));
	}
}