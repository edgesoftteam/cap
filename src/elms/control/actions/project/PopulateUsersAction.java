package elms.control.actions.project;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.CommonAgent;
import elms.app.common.ProcessTeamCollectionRecord;
import elms.control.beans.ProcessTeamForm;

public class PopulateUsersAction extends Action {
	static Logger logger = Logger.getLogger(PopulateUsersAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.info("Entering PopulateUsersAction ()");

		HttpSession session = request.getSession();
		CommonAgent agent = new CommonAgent();

		ProcessTeamForm frm = (ProcessTeamForm) form;
		String psaId = frm.getPsaId();

		ProcessTeamCollectionRecord[] processTeamCollectionRecord = frm.getProcessTeamCollectionRecord();
		logger.debug("processTeamCollectionRecord length is: " + processTeamCollectionRecord.length);

		ProcessTeamCollectionRecord rec;
		List list;
		String role;

		for (int ii = 0; ii < processTeamCollectionRecord.length; ii++) {
			rec = processTeamCollectionRecord[ii];
			role = rec.getTitle();
			logger.debug("RoleId (" + ii + ") is : " + role);
			int deptId =(Integer) session.getAttribute("deptId");
			list = agent.getUsersForTitle(role, psaId,deptId); // this gets users which have not already been assigned from the database
			rec.setNameList(list);
			rec.setCheckRemove(rec.getCheckRemove());
			rec.setLead(rec.getLead());
		}

		form = frm;
		session.setAttribute("processTeamForm", frm);

		// Forward control to the specified success URI
		logger.info("Exiting PopulateUsersAction()");

		return (mapping.findForward("success"));
	}
}
