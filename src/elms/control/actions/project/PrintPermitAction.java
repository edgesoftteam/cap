package elms.control.actions.project;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.jdom.Document;

import elms.agent.LookupAgent;
import elms.agent.ReportAgent;
import elms.app.admin.ActivityType;
import elms.util.Doc2Pdf;
import elms.util.StringUtils;

public class PrintPermitAction extends Action {

	static Logger logger = Logger.getLogger(PrintPermitAction.class.getName());

	protected String actNbr = "";
	protected String pdfFileName = "";
	protected String xslFileName = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.debug("Entered Into the PrintPermitAction");

		ActionErrors errors = new ActionErrors();

		if (!errors.empty()) {
			saveErrors(request, errors);
			saveToken(request);
			logger.info("Entered PrintPermitAction with errors");

			return (new ActionForward(mapping.getInput()));
		} else {
			try {
				actNbr = (String) request.getParameter("actNbr");

				String actType = (String) request.getParameter("actType");

				if (actType == null) {
					actType = "";
				}

				logger.debug("actNbr is " + actNbr);
				logger.debug("actType is " + actType);
				
    			String realPath = getServlet().getServletContext().getRealPath("/");
                ReportAgent ra = new ReportAgent(realPath + "xsl"+File.separator);
	                

				Document document = null;

				ActivityType type = LookupAgent.getActivityType(actType);
				logger.debug("type.getDepartmentCode()  "+type.getDepartmentCode());

				if (type != null) {
					if (type.getDepartmentCode().equals("PL")) {
						logger.debug("Control is in Planning");
						xslFileName = "planning.xsl";
						document = ra.getGeneralPermitJDOM(actNbr, "PL", actType);
					} else if (type.getDepartmentCode().equals("PW")) {
						logger.debug("Control is in Public Works");
						xslFileName = "publicworks.xsl";
						document = ra.getGeneralPermitJDOM(actNbr, "", actType);
					} else {
						logger.debug("Control is in Building");

						if (type.getSubProjectType() == 2) {
							// Code Enforcement
							xslFileName = "ce.xsl";
						} else {
							xslFileName = LookupAgent.getKeyValue("GENERIC_PERMIT_XSL");
						}
						document = ra.getGeneralPermitJDOM(actNbr, "", actType);
					}
				}

				logger.debug("The xsl file name is " + xslFileName);

				// for debugging start
				// XMLOutputter outputter = new XMLOutputter();
				//
				// try {
				// outputter.setIndent("  "); // use two space indent
				// outputter.setNewlines(true);
				// outputter.output(document, System.out);
				// }
				// catch (IOException e) {
				// logger.error(e);
				// }

				// for debugging end

				pdfFileName = actNbr + "-" + actType + "-" + StringUtils.cal2Ts(Calendar.getInstance()) + ".pdf";
				request.setAttribute("pdfFileName", pdfFileName);
				logger.debug("pdf file name is set in request " + pdfFileName);

				if (document != null) {
					String getRealPath = this.getServlet().getServletContext().getRealPath("/");
					logger.debug("Real path is " + getRealPath);
					xslFileName = getRealPath + "xsl" + File.separator + xslFileName;
					pdfFileName = getRealPath + "pdf" + File.separator + pdfFileName;
					logger.debug("xslFileName::" +xslFileName);
					logger.debug("pdfFileName::" +pdfFileName);
					Doc2Pdf.start(document, xslFileName, pdfFileName);
				}
				return (mapping.findForward("success"));
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("Unable to print permit " + e.getMessage(),e);
				throw new ServletException(e.getMessage());
			}
		}

	}
}
