package elms.control.actions.project;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.app.project.PlanCheck;
import elms.control.beans.PlanCheckForm;
import elms.security.User;
import elms.util.StringUtils;

public class SavePlanCheckAction extends Action {
	static Logger logger = Logger.getLogger(SavePlanCheckAction.class.getName());
	int activityId = -1;
	int planCheckId = -1;
	String strPlanCheckId = "";
	protected PlanCheck planCheck = null;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.debug("Entered Into SavePlanCheckAction ....");

		HttpSession session = request.getSession();

		// setting the onloadAction of next page not to refresh PSA tree
		String onloadAction = "";
		logger.debug("Setting the refresh tree flag to " + onloadAction);
		session.setAttribute("onloadAction", onloadAction);

		PlanCheckForm planCheckForm = (PlanCheckForm) form;
		ActionErrors errors = new ActionErrors();

		String pcStatusCode = planCheckForm.getPlanCheckStatus();

		try {
			if ((pcStatusCode.equalsIgnoreCase("-1")) || (pcStatusCode == null)) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.planCheck.noStatusCode"));
				planCheckForm.setPlanChecks(new ActivityAgent().getPlanChecks(StringUtils.s2i(planCheckForm.getActivityId())));
				request.setAttribute("planCheckForm", planCheckForm);
				request.setAttribute("activityId", planCheckForm.getActivityId());

				logger.debug("no Plan Check Status");
			}
		} catch (Exception e1) {
			logger.error("Exception occured while getting plan check statuses " + e1.getMessage());
			throw new ServletException("Exception occured while getting plan check status, Error is " + e1.getMessage());
		}

		if (!errors.empty()) {
			saveErrors(request, errors);
			saveToken(request);

			return (new ActionForward(mapping.getInput()));
		} else {
			try {
				PlanCheck planCheck = new PlanCheck();
				strPlanCheckId = planCheckForm.getPlanCheckId();

				if (strPlanCheckId == null)
					strPlanCheckId = request.getParameter("planCheckId");

				if (strPlanCheckId == null)
					strPlanCheckId = "0";

				logger.debug("plan check id is " + strPlanCheckId);
				planCheck.setPlanCheckId(StringUtils.s2i(strPlanCheckId));
				activityId = StringUtils.s2i(planCheckForm.getActivityId());
				logger.debug("Got parameter activityId as " + activityId);
				planCheck.setActivityId(activityId);
				logger.debug("set activityId  as " + planCheck.getActivityId());
				planCheck.setStatusCode(StringUtils.s2i(planCheckForm.getPlanCheckStatus()));
				logger.debug("set PlanCheckStatus as " + planCheck.getStatusCode());
				planCheck.setPlanCheckDate(StringUtils.str2cal(planCheckForm.getPlanCheckDate()));
				logger.debug("set  PlanCheckDate  as " + StringUtils.cal2Ts(planCheck.getPlanCheckDate()));
				planCheck.setEngineer(StringUtils.s2i(planCheckForm.getEngineerId()));
				logger.debug("set  Engineer  as " + planCheck.getEngineer());
				planCheck.setComments(planCheckForm.getComments());
				logger.debug("set  Comment  as " + planCheck.getComments());

				User user = (User) session.getAttribute("user");
				planCheck.setCreatedBy(user);
				logger.debug("set  Created By as " + planCheck.getCreatedBy());
				planCheck.setUpdatedBy(user);
				logger.debug("set  updated By as " + planCheck.getUpdatedBy());

				if (strPlanCheckId.equalsIgnoreCase("0"))
					planCheckId = new ActivityAgent().addPlanCheck(planCheck);
				else
					planCheckId = new ActivityAgent().editPlanCheck(planCheck);
				logger.debug("Exit SaveActivityAction ...  ");

				return (mapping.findForward("success"));

			} catch (Exception e) {
				logger.error(e.getMessage());
				return (mapping.findForward("error"));
			}
		}

	}
}
