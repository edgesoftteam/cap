package elms.control.actions.project;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.CommonAgent;
import elms.agent.LookupAgent;
import elms.common.Constants;
import elms.control.beans.ProcessTeamForm;

public class AddProcessTeamAction extends Action {
	static Logger logger = Logger.getLogger(EditProcessTeamAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();

		String psaId = (String) session.getAttribute(Constants.PSA_ID);
		String psaType = (String) session.getAttribute(Constants.PSA_TYPE);

		ProcessTeamForm frm;

		try {
			frm = new ProcessTeamForm();

			CommonAgent agent = new CommonAgent();

			if ((psaId != null) && !psaId.equals("")) {
				frm.setPsaId(psaId);

				frm.setPsaType(psaType);

				if (psaType == null) {
					psaType = "";
				}

				String backUrl = "";

				if (psaType.equals("P")) {
					backUrl = request.getContextPath() + "/viewProject.do?projectId=" + psaId;
				} else if (psaType.equals("A")) {
					try {
						String activityType = LookupAgent.getActivityTypeForActId(psaId);

						if (activityType == null) {
							activityType = "";
						}

						if (activityType.equals("PWFAC")) {
							backUrl = request.getContextPath() + "/pwViewFacilities.do?activityId=" + psaId;
						} else if (activityType.equals("PWINFO")) {
							backUrl = request.getContextPath() + "/pwViewFacilities.do?activityId=" + psaId;
						} else if (activityType.equals("PWSAN")) {
							backUrl = request.getContextPath() + "/pwViewSanitation.do.do?activityId=" + psaId;
						} else if (activityType.equals("PWSEW")) {
							backUrl = request.getContextPath() + "/pwViewSewer.do?activityId=" + psaId;
						} else if (activityType.equals("PWSTR")) {
							backUrl = request.getContextPath() + "/pwViewStreets.do?activityId=" + psaId;
						} else if (activityType.equals("PWWAT")) {
							backUrl = request.getContextPath() + "/pwViewWater.do?activityId=" + psaId;
						} else if (activityType.equals("PWSANC")) {
							backUrl = request.getContextPath() + "/pwViewSanitation.do?activityId=" + psaId;
						} else if (activityType.equals("PWTSSL")) {
							backUrl = request.getContextPath() + "/pwViewTrafficSignals.do?activityId=" + psaId;
						} else if (activityType.equals("PWELEC")) {
							backUrl = request.getContextPath() + "/pwViewElectrical.do?activityId=" + psaId;
						} else {
							backUrl = request.getContextPath() + "/viewActivity.do?activityId=" + psaId;
						}

						logger.debug("Back URL is " + backUrl);
					} catch (Exception e) {
						logger.error("Error when creating backUrl");
					}
				}

				frm.setBackUrl(backUrl);
				frm = agent.populateTeam(frm);
			}

			// this inserts a blank row
			frm = agent.insertRow(frm);
			session.setAttribute("processTeamForm", frm);

			return (mapping.findForward("success"));
		} catch (Exception e) {
			return (mapping.findForward("error"));
		}
	}
}
