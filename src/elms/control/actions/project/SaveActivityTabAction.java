package elms.control.actions.project;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.CommonAgent;
import elms.agent.ActivityAgent;
import elms.agent.AddressAgent;
import elms.agent.ProjectAgent;
import elms.app.admin.CustomField;
import elms.common.Constants;
import elms.control.beans.ActivityForm;
import elms.security.User;
import elms.util.StringUtils;

public class SaveActivityTabAction extends Action {

	static Logger logger = Logger.getLogger(SaveActivityTabAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		ActivityForm activityForm = (ActivityForm) form;
		ActionErrors errors = new ActionErrors();

		try {

			User user = (User) session.getAttribute("user");
			logger.debug("Logged in user details: id = " + user.getUserId() + " Name = " + user.getFullName());

			// get the location type
			String locationType = activityForm.getLocationType();
			logger.debug("The location type obtained from form is " + locationType);
			String streetNumber = null;
			String streetNameId = null;
			if (locationType.equalsIgnoreCase(Constants.LOCATION_TYPE_ADDRESS)) {

				// check for validation errors(address, sub project name, sub project type, applied date, description)
				streetNumber = activityForm.getStreetNumber();
				if ((streetNumber == null) || streetNumber.equalsIgnoreCase("")) {
					logger.debug("street number required");
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.streetNumber.required"));
				}
				streetNameId = activityForm.getStreetName();
				if ((streetNameId == null) || streetNameId.equalsIgnoreCase("-1")) {
					logger.debug("street name required");
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.streetName.required"));
				}
			}

			String crossStreetName1 = "";
			String crossStreetName2 = "";
			if (locationType.equalsIgnoreCase(Constants.LOCATION_TYPE_CROSS_STREET)) {

				// check for validation errors(cross street)
				crossStreetName1 = activityForm.getCrossStreetName1() != null ? activityForm.getCrossStreetName1() : "-1";
				if ((crossStreetName1 == null) || crossStreetName1.equalsIgnoreCase("")) {
					logger.debug("Cross street information is required");
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.streetName.required"));
				}
				crossStreetName2 = activityForm.getCrossStreetName2() != null ? activityForm.getCrossStreetName2() : "-1";
				if ((crossStreetName2 == null) || crossStreetName2.equalsIgnoreCase("-1")) {
					logger.debug("street name required");
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.streetName.required"));
				}
			}

			// Allow up to 5 street ranges
			String rangeStreetNumberStart1 = activityForm.getRangeStreetNumberStart1();
			logger.debug("rangeStreetNumberStart1 " + rangeStreetNumberStart1);
			String rangeStreetNumberEnd1 = activityForm.getRangeStreetNumberEnd1();
			logger.debug("rangeStreetNumberEnd1 " + rangeStreetNumberEnd1);
			String rangeStreetName1 = activityForm.getRangeStreetName1();
			logger.debug("rangeStreetName1 " + rangeStreetName1);
			String rangeStreetNumberStart2 = activityForm.getRangeStreetNumberStart2();
			logger.debug("rangeStreetNumberStart2 " + rangeStreetNumberStart2);
			String rangeStreetNumberEnd2 = activityForm.getRangeStreetNumberEnd2();
			logger.debug("rangeStreetNumberEnd2 " + rangeStreetNumberEnd2);
			String rangeStreetName2 = activityForm.getRangeStreetName2();
			logger.debug("rangeStreetName2 " + rangeStreetName2);
			String rangeStreetNumberStart3 = activityForm.getRangeStreetNumberStart3();
			logger.debug("rangeStreetNumberStart3 " + rangeStreetNumberStart3);
			String rangeStreetNumberEnd3 = activityForm.getRangeStreetNumberEnd3();
			logger.debug("rangeStreetNumberEnd3 " + rangeStreetNumberEnd3);
			String rangeStreetName3 = activityForm.getRangeStreetName3();
			logger.debug("rangeStreetName3 " + rangeStreetName3);
			String rangeStreetNumberStart4 = activityForm.getRangeStreetNumberStart4();
			logger.debug("rangeStreetNumberStart4 " + rangeStreetNumberStart4);
			String rangeStreetNumberEnd4 = activityForm.getRangeStreetNumberEnd4();
			logger.debug("rangeStreetNumberEnd4 " + rangeStreetNumberEnd4);
			String rangeStreetName4 = activityForm.getRangeStreetName4();
			logger.debug("rangeStreetName4 " + rangeStreetName4);
			String rangeStreetNumberStart5 = activityForm.getRangeStreetNumberStart5();
			logger.debug("rangeStreetNumberStart5 " + rangeStreetNumberStart5);
			String rangeStreetNumberEnd5 = activityForm.getRangeStreetNumberEnd5();
			logger.debug("rangeStreetNumberEnd5 " + rangeStreetNumberEnd5);
			String rangeStreetName5 = activityForm.getRangeStreetName5();
			logger.debug("rangeStreetName5 " + rangeStreetName5);

			if (locationType.equalsIgnoreCase(Constants.LOCATION_TYPE_RANGE)) {

				// check for validation errors(address range)
				if (GenericValidator.isBlankOrNull(rangeStreetNumberStart1)) {
					logger.debug("Range start - street number 1 information is required");
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.addressRangeStreet1.required"));
				}
				if (GenericValidator.isBlankOrNull(rangeStreetNumberEnd1)) {
					logger.debug("Range End - street number 1 information is required");
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.addressRangeStreet2.required"));
				}
				if (GenericValidator.isBlankOrNull(rangeStreetName1)) {
					logger.debug("Range street name information is required");
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.streetName.required"));
				}
				if (rangeStreetName1.equalsIgnoreCase("-1")) {
					logger.debug("Range street name information is required");
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.streetName.required"));
				}
			}

			String landmarkAddressId = activityForm.getLandmark() != null ? activityForm.getLandmark() : "-1";
			if (locationType.equalsIgnoreCase(Constants.LOCATION_TYPE_LANDMARK)) {

				// check for validation errors(landmark)
				if (landmarkAddressId.equalsIgnoreCase("-1")) {
					logger.debug("Landmark is required");
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.landmark.required"));
				}
			}

			String subProjectName = activityForm.getSubProjectName();
			if ((subProjectName == null) || subProjectName.equalsIgnoreCase("-1")) {
				logger.debug("project type required");
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.subProjectName.required"));
			}
			String subProjectType = activityForm.getSubProjectType();
			if ((subProjectType == null)) {
				logger.debug("project sub-type required");
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.subProjectType.required"));
			}

			String activityType = activityForm.getActivityType();
			logger.debug("Activity type is " + activityType);
			if ((activityType == null || activityType.equalsIgnoreCase("-1"))) {
				logger.debug("activityType required");
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.activityType.required"));
			}

			String startDate = activityForm.getStartDate();
			logger.debug("Applied date is " + startDate);
			if ((activityForm.getStartDate() == null) || activityForm.getStartDate().equalsIgnoreCase("")) {
				logger.debug("Applied date is required");
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.startDate.required"));
			}

			// description is required
			String description = activityForm.getDescription();
			description = activityForm.getDescription();
			if (description.length() >= 3000) {
				logger.debug("Work Description length limit exceeded");
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.activityDescription.exceeded"));
			}

			// validate address
			int lsoId = -1;
			int addressId = -1;

			// if location type is address, validate address from database
			if (locationType.equalsIgnoreCase(Constants.LOCATION_TYPE_ADDRESS)) {

				try {
					int streetNumberInt = Integer.parseInt(streetNumber.trim());
					int streetNameIdInt = Integer.parseInt(streetNameId);
					lsoId = new AddressAgent().getLsoId(streetNumberInt, streetNameIdInt);
					logger.debug("LSO Id = " + lsoId);
					addressId = AddressAgent.getLandAddressId(streetNumberInt, streetNameIdInt);
					logger.debug("Address Id = " + addressId);
					if (lsoId == -1 || addressId == -1) {
						if (errors.empty()) {
							errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.address.mismatch"));
						}
					}
				} catch (Exception e) {
					logger.debug("Address not found " + e.getMessage());
					if (errors.empty()) {
						errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.address.mismatch"));
					}
				}

			}

			try {
				// lsoid for 6 citywide
				int nonLocationalLsoId = new AddressAgent().getLsoId(6, 53);
				if (locationType.equalsIgnoreCase(Constants.LOCATION_TYPE_RANGE)) {
					logger.debug("Location Type is Address Range");
					lsoId = nonLocationalLsoId;
					addressId = -1;
				}
				if (locationType.equalsIgnoreCase(Constants.LOCATION_TYPE_CROSS_STREET)) {
					logger.debug("Location Type is cross street");
					lsoId = nonLocationalLsoId;
					addressId = new AddressAgent().getAddressIdForCrossStreet(crossStreetName1, crossStreetName2);
					logger.debug("Address Id is " + addressId);
				}
				if (locationType.equalsIgnoreCase(Constants.LOCATION_TYPE_LANDMARK)) {
					logger.debug("Location Type is landmark");
					lsoId = nonLocationalLsoId;
					addressId = StringUtils.s2i(landmarkAddressId);
					logger.debug("Address Id is " + addressId);
				}
			} catch (Exception e) {
				if (errors.empty()) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.address.nonlocation"));
				}
			}
			// if errors exist, then forward to the input page.
			if (!errors.empty()) {
				logger.debug("Errors exist, going back to the input page");
				saveErrors(request, errors);
				request.setAttribute("hasErrors", "hasErrors");
				return (new ActionForward(mapping.getInput()));
			}

			logger.debug("no errors, inserting data..");

			// check and create project
			ProjectAgent projectAgent = new ProjectAgent();
			int projectId = projectAgent.getProjectId(lsoId, Constants.PROJECT_NAME_PUBLIC_WORKS);
			if (projectId == -1) {
				// project does not exist, create a new building project and proceed
				projectId = projectAgent.createProject(Constants.PROJECT_NAME_PUBLIC_WORKS, Constants.DEPARTMENT_PUBLIC_WORKS, StringUtils.i2s(lsoId), user.getUserId());
				logger.debug("New project created with project Id = " + projectId);
			}

			// check and create sub-project
			ProjectAgent subProjectAgent = new ProjectAgent();
			int subProjectId = subProjectAgent.getSubProjectId(projectId, StringUtils.s2i(subProjectName));
			if (subProjectId == -1) {
				// sub-project does not exist, create a new sub-project and proceed
				subProjectId = subProjectAgent.createSubProject(projectId, subProjectName, subProjectType, user.getUserId());
				logger.debug("New sub-project created with sub-project Id = " + subProjectId);
			}

			// create activity
			ActivityAgent activityAgent = new ActivityAgent();

			Calendar cal = Calendar.getInstance();
			if(activityForm.getActivityStatus().equalsIgnoreCase(""+Constants.ACTIVITY_STATUS_PUBLIC_WORKS_ISSUED)){//status=issued
				activityForm.setIssueDate(StringUtils.cal2str(cal));
			}
			
			cal.add(Calendar.DAY_OF_YEAR, 180); // add 180 days to today's date.
			activityForm.setExpirationDate(StringUtils.cal2str(cal));
			logger.debug("In else of Issuedate condition");

			int activityId = activityAgent.createActivity(subProjectId, addressId, activityForm.getActivityType(), description, StringUtils.$2dbl(activityForm.getValuation()), StringUtils.b2s(activityForm.getPlanCheckRequired()), activityForm.getActivityStatus(), activityForm.getStartDate(), activityForm.getExpirationDate(), user.getUserId(), activityForm.getPlanNo(), activityForm.getSubDivision(), activityForm.getIssueDate(), activityForm.getLocationType());
			logger.debug("New Activity created with activity id = " + activityId);

			if (locationType.equalsIgnoreCase(Constants.LOCATION_TYPE_RANGE)) {
				activityAgent.createActivityAddressRange(activityId, rangeStreetNumberStart1, rangeStreetNumberEnd1, rangeStreetName1, rangeStreetNumberStart2, rangeStreetNumberEnd2, rangeStreetName2, rangeStreetNumberStart3, rangeStreetNumberEnd3, rangeStreetName3, rangeStreetNumberStart4, rangeStreetNumberEnd4, rangeStreetName4, rangeStreetNumberStart5, rangeStreetNumberEnd5, rangeStreetName5);
				logger.debug("New Activity created with activity id = " + activityId);
			}

			// insert Custom fields
			// Custom fields start

			if (activityForm.getCustomFieldList().length > 0) {
				logger.debug("-----------------Entered Custom fields************************** ");
				CustomField[] customFieldList = activityForm.getCustomFieldList();
				new ActivityAgent().saveCustomActvityFields(activityId, customFieldList);
			}
			// end custom fields

			// Add Required condition for Activity
			String typeId = "";
			CommonAgent condAgent = new CommonAgent();
			typeId = condAgent.getActTypeId(activityForm.getActivityType());
			boolean added = condAgent.copyRequiredCondition(typeId, activityId, user);
			logger.debug("added new conditions" + added);

			// redirect back to the view Activity page
			session.setAttribute("refreshTree", "Y");
			session.removeAttribute("siteStructureForm");
			session.removeAttribute("siteUnitForm");

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope())) {
					request.removeAttribute(mapping.getAttribute());
				} else {
					session.removeAttribute(mapping.getAttribute());
				}
			}

			session.setAttribute(Constants.PSA_ID, StringUtils.i2s(activityId));
			session.setAttribute(Constants.PSA_TYPE, "A");
			session.setAttribute(Constants.LSO_ID, StringUtils.i2s(lsoId));

			request.setAttribute("activityId", StringUtils.i2s(activityId));

			ActivityAgent actAgent = new ActivityAgent();
			String lsoAddress = actAgent.getActivityAddressForId(activityId);
			String psaInfo = "";
			session.setAttribute("psaInfo", psaInfo);
			session.setAttribute("lsoAddress", lsoAddress);

			return (mapping.findForward("success"));

		} catch (Exception e) {
			throw new ServletException(e.getMessage());
		}

	}

}
