package elms.control.actions.project;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ProjectAgent;
import elms.app.project.Project;
import elms.app.project.ProjectDetail;
import elms.app.project.SubProject;
import elms.app.project.SubProjectDetail;
import elms.util.StringUtils;

public class DeleteSubProjectAction extends Action {
	static Logger logger = Logger.getLogger(DeleteSubProjectAction.class.getName());
	protected Project project = null;
	protected int projectId = -1;
	protected ProjectDetail projectDetail = null;
	protected List subProjects = null;
	protected SubProject subProject = null;
	protected int subProjectId = -1;
	protected SubProjectDetail subProjectDetail = null;
	protected String ownerName = "";
	protected String streetNumber = "";
	protected String streetName = "";
	String psaType = "P";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();

		try {
			// Getting required values from the request Object
			subProjectId = StringUtils.s2i(request.getParameter("subProjectId"));
			logger.debug("Obtained Sub Project id from request as " + subProjectId);
			projectId = new ProjectAgent().deleteSubProject(subProjectId);
			logger.info("optained projectId back as " + projectId);

			// Cleaning the request Object
			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope())) {
					request.removeAttribute(mapping.getAttribute());
				} else {
					session.removeAttribute(mapping.getAttribute());
				}
			}

			// refresh PSA Tree
			session.setAttribute("refreshPSATree", "Y");

			ViewProjectAction viewProjectAction = new ViewProjectAction();
			viewProjectAction.getProject(projectId, request);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		logger.info("Forwarding the delete Sub Project Action to view project jsp");

		return (mapping.findForward("success"));
	}
}
