package elms.control.actions.project;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.LookupAgent;
import elms.agent.AddressAgent;
import elms.agent.ProjectAgent;
import elms.app.finance.ProjectFinance;
import elms.app.lso.Lso;
import elms.app.lso.Use;
import elms.app.project.Project;
import elms.app.project.ProjectDetail;
import elms.common.Constants;
import elms.control.beans.ProjectForm;
import elms.security.User;
import elms.util.StringUtils;

public class SaveProjectDetailsAction extends Action {

	static Logger logger = Logger.getLogger(SaveProjectDetailsAction.class.getName());
	protected Project project;
	protected ProjectDetail projectDetail;
	protected List subProjects;
	protected int projectId = -1;
	protected String ownerName = "";
	protected String streetNumber = "";
	protected String streetName = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		ProjectForm projectForm = (ProjectForm) form;

		// need to refresh the PSA tree so onloadAction is setting to
		String onloadAction = "parent.f_psa.location.href='" + request.getContextPath() + "/viewPsaTree.do?lsoId=" + (String) session.getAttribute(Constants.LSO_ID) + "'";
		logger.debug("Setting the refresh tree flag to " + onloadAction);
		session.setAttribute("onloadAction", onloadAction);

		try {
			projectId = StringUtils.s2i(request.getParameter("projectId"));
			logger.debug("SaveprojectDetails -- got parameter projectId as " + projectId);

			// Loading project Detail Object
			projectDetail = new ProjectDetail();
			projectDetail.setProjectNumber(request.getParameter("projectNumber"));

			projectDetail.setName(projectForm.getProjectName());
			logger.debug("SaveprojectDetails --  set the name for project detail object as " + projectForm.getProjectName());
			projectDetail.setDescription(projectForm.getDescription());
			logger.debug("SaveprojectDetails --  set the description for project detail object as " + projectForm.getDescription());
			projectDetail.setProjectStatus(LookupAgent.getProjectStatus(StringUtils.s2i(projectForm.getStatus())));
			logger.debug("SaveprojectDetails --  set the status for project detail object as " + projectForm.getStatus());
			projectDetail.setStartDate(StringUtils.str2cal(projectForm.getStartDate()));
			logger.debug("SaveprojectDetails --  set the start date for project detail object as " + StringUtils.str2cal(projectForm.getStartDate()));
			projectDetail.setCompletionDate(StringUtils.str2cal(projectForm.getCompletionDate()));
			logger.debug("SaveprojectDetails -- set the completion date for project detail object as " + StringUtils.str2cal(projectForm.getCompletionDate()));
			projectDetail.setExpriationDate(StringUtils.str2cal(projectForm.getExpirationDate()));
			logger.debug("SaveprojectDetails -- set the expiration date for project detail object as " + StringUtils.str2cal(projectForm.getExpirationDate()));
			projectDetail.setMicrofilm(projectForm.getMicrofilm());
			logger.debug("SaveActivityDetailsAction -- activityDetail -- Microfilm is set to " + projectDetail.getMicrofilm());
			projectDetail.setLabel(projectForm.getLabel());
			logger.debug("SaveActivityDetailsAction -- activityDetail -- Label is set to " + projectDetail.getLabel());
			projectDetail.setCreatedBy((User) session.getAttribute("user"));
			logger.debug(" SaveprojectDetails -- set the created by for project detail object as " + session.getAttribute("user"));
			projectDetail.setUpdatedBy((User) session.getAttribute("user"));
			logger.debug("SaveprojectDetails -- set the updated by for project detail object as " + session.getAttribute("user"));
			Use use = new AddressAgent().getUse(StringUtils.s2i(projectForm.getUse()));
			logger.debug("SaveprojectDetails -- created a use object with " + projectForm.getUse());

			// Not really using the below objects...but required
			Lso lso = new Lso();
			subProjects = new ArrayList();
			ProjectFinance projectFinance = new ProjectFinance();
			List projectHolds = new ArrayList();
			List projectConditions = new ArrayList();
			List projectAttachments = new ArrayList();
			List projectComments = new ArrayList();
			List processTeams = new ArrayList();
			List peoples = new ArrayList();
			String projectType = "";
			project = new Project(projectId, lso, projectDetail, subProjects, projectFinance, projectHolds, projectConditions, projectAttachments, projectComments, processTeams, peoples, projectType, use);
			logger.debug("created the project object as " + project);
			projectId = new ProjectAgent().saveProjectDetails(project);
			ViewProjectAction viewProjectAction = new ViewProjectAction();
			viewProjectAction.getProject(projectId, request);
			return (mapping.findForward("success"));
		} catch (Exception e) {
			logger.error("Going to the error page.");
			return (mapping.findForward("error"));
		}
	}
}
