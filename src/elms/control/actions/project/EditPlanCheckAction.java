package elms.control.actions.project;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.app.project.PlanCheck;
import elms.control.beans.PlanCheckForm;
import elms.util.StringUtils;

public class EditPlanCheckAction extends Action {
	static Logger logger = Logger.getLogger(AddPlanCheckAction.class.getName());
	protected PlanCheckForm planCheckForm;
	protected List pcStatuses;
	String strPlanCheckId = "";
	int activityId = -1;
	Calendar calendar = Calendar.getInstance();

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.debug("Entered Into EditPlanCheckAction ....");

		HttpSession session = request.getSession();

		try {
			PlanCheckForm planCheckForm = new PlanCheckForm();
			ActivityAgent planCheckAgent = new ActivityAgent();

			strPlanCheckId = request.getParameter("planCheckId");

			if (strPlanCheckId == null) {
				strPlanCheckId = "-1";
			}

			planCheckForm.setPlanCheckId(strPlanCheckId);
			logger.debug("plancheck id" + strPlanCheckId);

			PlanCheck plancheck = new PlanCheck();
			plancheck = planCheckAgent.getPlanCheck(StringUtils.s2i(strPlanCheckId));

			planCheckForm.setEngineerId(StringUtils.i2s(plancheck.getEngineer()));
			logger.debug("engineer Id " + plancheck.getEngineer());

			planCheckForm.setPlanCheckStatus(StringUtils.i2s(plancheck.getStatusCode()));

			planCheckForm.setActivityId(StringUtils.i2s(plancheck.getActivityId()));
			logger.debug("activity Id is set to plancheck form as " + plancheck.getActivityId());

			planCheckForm.setPlanCheckDate(plancheck.getDate());
			logger.debug("Plan Check date is set to Plan check form as " + planCheckForm.getPlanCheckDate());

			planCheckForm.setComments(plancheck.getComments());
			logger.debug("plan check comment " + planCheckForm.getComments());

			request.setAttribute("planCheckForm", planCheckForm);
			request.setAttribute("planCheckId", strPlanCheckId);
			request.setAttribute("activityId", StringUtils.i2s(plancheck.getActivityId()));
			logger.debug("Exit of  EditPlanCheckAction ....");

			return (mapping.findForward("success"));
		} catch (Exception e) {
			logger.error(e.getMessage());

			return (mapping.findForward("error"));
		}
	}
}
