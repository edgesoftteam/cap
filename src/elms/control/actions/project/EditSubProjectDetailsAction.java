package elms.control.actions.project;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.LookupAgent;
import elms.agent.ProjectAgent;
import elms.app.admin.SubProjectStatus;
import elms.app.project.SubProject;
import elms.app.project.SubProjectDetail;
import elms.common.Constants;
import elms.control.beans.SubProjectForm;
import elms.util.StringUtils;

public class EditSubProjectDetailsAction extends Action {
	static Logger logger = Logger.getLogger(EditSubProjectDetailsAction.class.getName());
	protected SubProject subProject;
	protected SubProjectDetail subProjectDetail;
	protected SubProjectForm subProjectForm;
	protected int projectId;
	protected int subProjectId;
	protected List subProjectStatuses;
	protected List spstList;
	protected String sprojType;
	protected List caseLogList;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		try {
			logger.debug("Entered into EditSubProjectDetailsAction ..");

			HttpSession session = request.getSession();

			subProjectId = StringUtils.s2i(request.getParameter("subProjectId"));
			logger.debug("Got subProjectId from the request " + subProjectId);
			subProject = new ProjectAgent().getSubProject(subProjectId, "Y");
			logger.debug("Got subProject  from the databse with Id " + subProject.getSubProjectId());
			subProjectDetail = subProject.getSubProjectDetail();

			int projectNameId = -1;

			try {
				projectNameId = LookupAgent.getProjectNameId("Q", subProjectId);
			} catch (Exception e5) {
				logger.error("Exception occured while getting project name id " + e5.getMessage());
			}

			logger.debug("Obtained project name id is" + projectNameId);

			if (subProjectDetail != null) {
				String subProjectName = "";
				String subProjectNumber = "";

				if (subProjectDetail.getProjectType() != null) {
					subProjectName = subProjectDetail.getProjectType().getDescription();
					logger.debug("setting Sub Project name in the request " + subProjectName);
					request.setAttribute("subProjectName", subProjectName);

					subProjectNumber = subProjectDetail.getSubProjectNumber();
					logger.debug("setting subProjectNumber in the request " + subProjectNumber);
					request.setAttribute("subProjectNumber", subProjectNumber);

					// List sptList = LookupAgent.getSubProjectTypes(subProjectDetail.getProjectType().getProjectTypeId());
					List sptList;

					try {
						sptList = LookupAgent.getSubProjectTypes(subProjectDetail.getProjectType().getProjectTypeId());
					} catch (Exception e) {
						sptList = new ArrayList();
						logger.warn("Could not find list of sptList, initializing it to blank arraylist");
					}

					logger.debug("sproj_type id " + subProjectDetail.getProjectType().getProjectTypeId());
					request.setAttribute("sptList", sptList);
					logger.debug("The subProject Type List is set to request  with size " + sptList.size());
				}

				String from = request.getParameter("from");

				if (from == null) {
					from = "";
				}

				// onChange of Type
				if (from.equals("ptList")) {
					subProjectForm = (SubProjectForm) session.getAttribute("subProjectForm");
					sprojType = subProjectForm.getType();

					try {
						spstList = LookupAgent.getSubProjectSubTypes(StringUtils.s2i(sprojType));
					} catch (Exception e) {
						spstList = new ArrayList();
						logger.warn("Could not find list of spstList, initializing it to blank arraylist");
					}
				} else {
					ActionForm frm = new SubProjectForm();
					session.setAttribute("subProjectForm", frm);

					SubProjectForm subProjectForm = (SubProjectForm) frm;

					if (subProjectDetail.getSubProjectType() != null) {
						subProjectForm.setType(StringUtils.i2s(subProjectDetail.getSubProjectType().getSubProjectTypeId()));
						logger.debug("EditSubProjectDetailsAction -- set type to the SubProjectForm " + subProjectForm.getType());

						try {
							spstList = LookupAgent.getSubProjectSubTypes(subProjectDetail.getSubProjectType().getSubProjectTypeId());
						} catch (Exception e) {
							spstList = new ArrayList();
							logger.warn("Could not find list of spstList, initializing it to blank arraylist");
						}
					}

					if (subProjectDetail.getSubProjectSubType() != null) {
						subProjectForm.setSubType(StringUtils.i2s(subProjectDetail.getSubProjectSubType().getSubProjectSubTypeId()));
						logger.debug("EditSubProjectDetailsAction -- set  subtype to the SubProjectForm " + subProjectForm.getSubType());
					}

					subProjectForm.setDescription(subProjectDetail.getDescription());
					logger.debug("EditSubProjectDetailsAction -- set  description to the SubProjectForm " + subProjectForm.getDescription());

					subProjectForm.setLabel(subProjectDetail.getLabel());
					logger.debug("EditSubProjectDetailsAction -- set  label to the SubProjectForm " + subProjectForm.getLabel());

					subProjectForm.setStatus(StringUtils.i2s(subProjectDetail.getSubProjectStatus().getStatusId()));
					logger.debug("EditSubProjectDetailsAction -- set  status to the SubProjectForm " + subProjectForm.getStatus());

					subProjectForm.setCaseLogId(StringUtils.i2s(subProjectDetail.getCaseLogId()));
				}
			}

			request.setAttribute("spstList", spstList);
			logger.debug("The subProject Sub Type List is set to request  with size " + spstList.size());

			try {
				subProjectStatuses = LookupAgent.getSubProjectStatuses();
				caseLogList = LookupAgent.getCaseLogList();
			} catch (Exception e) {
				subProjectStatuses = new ArrayList();
				logger.warn("Could not find list of subProjectStatuses, initializing it to blank arraylist");
			}

			if (projectNameId == Constants.PROJECT_NAME_PLANNING_ID) {
				List tmpStatus = subProjectStatuses;
				Iterator iter = tmpStatus.iterator();

				while (iter.hasNext()) {
					SubProjectStatus subProjectStatus = (SubProjectStatus) iter.next();

					if ("Active".equalsIgnoreCase(subProjectStatus.getDescription())) {
						iter.remove();
					}

					if ("Inactive".equalsIgnoreCase(subProjectStatus.getDescription())) {
						iter.remove();
					}

					if ("Suspense".equalsIgnoreCase(subProjectStatus.getDescription())) {
						iter.remove();
					}
				}

				subProjectStatuses = tmpStatus;
			}

			request.setAttribute("subProjectStatuses", subProjectStatuses);
			logger.debug("setting subProjectStatuses in the request with size" + subProjectStatuses.size());

			request.setAttribute("caseLogList", caseLogList);
			logger.debug("size of caseLogList: " + caseLogList.size());

			return (mapping.findForward("success"));
		} catch (Exception e) {
			logger.error("Exception occured while editing sub project details " + e.getMessage());
			throw new ServletException("Exception occured while editing sub project details " + e.getMessage());
		}
	}
}
