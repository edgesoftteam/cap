package elms.control.actions.project;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.LookupAgent;
import elms.agent.AddressAgent;
import elms.agent.ProjectAgent;
import elms.app.finance.ProjectFinance;
import elms.app.lso.Lso;
import elms.app.lso.Use;
import elms.app.project.Project;
import elms.app.project.ProjectDetail;
import elms.control.beans.ProjectForm;
import elms.security.User;
import elms.util.StringUtils;

public class SaveProjectAction extends Action {

	static Logger logger = Logger.getLogger(SaveProjectAction.class.getName());
	protected Project project;
	protected ProjectDetail projectDetail;
	protected List subProjects;
	protected int projectId = -1;
	protected String ownerName;
	protected String streetNumber;
	protected String streetName;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		ProjectForm projectForm = (ProjectForm) form;

		ActionErrors errors = new ActionErrors();

		if (!errors.empty()) {
			saveErrors(request, errors);
			saveToken(request);
			return (new ActionForward(mapping.getInput()));
		} else {
			try {

				String lsoId = request.getParameter("lsoId");
				logger.debug("got parameter lsoId as " + lsoId);

				int intLsoId = StringUtils.s2i(lsoId);
				logger.debug("converted lsoId to int value " + intLsoId);

				String lsoType = new AddressAgent().getLsoType(intLsoId);
				logger.debug("got lso type as " + lsoType);

				Lso lso = new Lso();
				lso.setLsoId(intLsoId);
				logger.debug("created a new empty lso object");
				// Use useObj =new UseAgent().getUse(StringUtils.s2i(projectForm.getUse()));
				Use useObj = new Use(StringUtils.s2i(projectForm.getUse()), "");
				logger.debug("created a use object with " + projectForm.getUse());
				projectDetail = new ProjectDetail();
				logger.debug("created a new empty project detail object");
				projectDetail.setName(projectForm.getProjectName());
				logger.debug("set the name for project detail object as " + projectForm.getProjectName());
				projectDetail.setDescription(projectForm.getDescription());
				logger.debug("set the description for project detail object as " + projectForm.getDescription());
				projectDetail.setProjectStatus(LookupAgent.getProjectStatus(1));
				logger.debug("set the status for project detail object as 7 i.e Pending(Active) ");
				projectDetail.setStartDate(StringUtils.str2cal(projectForm.getStartDate()));
				logger.debug("set the start date for project detail object as " + StringUtils.str2cal(projectForm.getStartDate()));
				projectDetail.setCompletionDate(StringUtils.str2cal(projectForm.getCompletionDate()));
				logger.debug("set the completion date for project detail object as " + StringUtils.str2cal(projectForm.getCompletionDate()));
				projectDetail.setCreatedBy((User) session.getAttribute("user"));
				// from user in session
				logger.debug("set the created by for project detail object as " + session.getAttribute("user"));
				projectDetail.setCreatedBy((User) session.getAttribute("user"));
				logger.debug("set the created by for project detail object as " + session.getAttribute("user"));
				projectDetail.setUpdatedBy((User) session.getAttribute("user"));
				// from user in session
				logger.debug("set the updated by for project detail object as " + session.getAttribute("user"));

				// not really required
				subProjects = new ArrayList();
				ProjectFinance projectFinance = new ProjectFinance();
				List projectHolds = new ArrayList();
				List projectConditions = new ArrayList();
				List projectAttachments = new ArrayList();
				List projectComments = new ArrayList();
				List processTeams = new ArrayList();
				List peoples = new ArrayList();
				String projectType = "Construction";
				project = new Project(0, lso, projectDetail, subProjects, projectFinance, projectHolds, projectConditions, projectAttachments, projectComments, processTeams, peoples, projectType, useObj);
				logger.debug("created the project object as " + project);
				projectId = new ProjectAgent().addProject(project);
				if (projectId > 0)
					session.setAttribute("refreshPSATree", "Y");
				ViewProjectAction viewProjectAction = new ViewProjectAction();
				viewProjectAction.getProject(projectId, request);
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}
		return (mapping.findForward("success"));

	}
}