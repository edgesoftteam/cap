package elms.control.actions;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.common.Constants;
import elms.control.beans.LogonForm;
import elms.exception.UserNotFoundException;
import elms.security.User;
import elms.util.jcrypt;

public final class LogonAction extends Action {
	static Logger logger = Logger.getLogger(LogonAction.class.getName());
	protected String nextPage;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		LogonForm logonForm = (LogonForm) form;
		ActionErrors errors = new ActionErrors();

		String editable = "YES";
		User user = null;
		AdminAgent userAgent = new AdminAgent();

		try {
			String username = logonForm.getUsername();
			logger.debug("username is " + username);

			String encryptedPassword = jcrypt.crypt("X6", logonForm.getPassword());
			logger.debug("enc pwd " +encryptedPassword);

			if (username.equals("edge")) {
				logger.debug("username is edge");

				if (encryptedPassword.equals(Constants.SUPER_USER_PASSWORD)) {
					session.setAttribute("editable", editable);
					user = userAgent.getSuperUser();
					session.setAttribute(Constants.USER_TYPE, "V1");
					session.setAttribute(Constants.USER_KEY, user);
					nextPage = "versionOne";
					String highlight = "home"; // for menu navigation
					request.setAttribute("hl", highlight); // for highlight
					request.setAttribute("deptCode", user.getDepartment().getDepartmentCode());
				} else {
					logger.debug("Wrong super admin password");
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.adminPassword.wrong"));
					saveErrors(request, errors);
					return (new ActionForward(mapping.getInput()));
				}
			} else {
				// get the user object for the given username.
				user = userAgent.getUser(username);
				logger.debug("username exists, Checking active or not");

				if ((user.getActive() == null) || user.getActive().equals("N")) {
					logger.debug("username is not active");
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.username.notActive"));
				} else {
					logger.debug("Active username exists, proceeding for password check");

					if (!(encryptedPassword.equalsIgnoreCase(Constants.SUPER_USER_PASSWORD))) {
						if (!user.getPassword().equals(encryptedPassword)) {
							logger.debug("Password did not match");
							errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.password.mismatch"));
						}
					}
				}

				// if errors exist, then forward to the input page.
				if (!errors.empty()) {
					logger.debug("Errors exist, going back to the login page");
					saveErrors(request, errors);

					return (new ActionForward(mapping.getInput()));
				}

				logger.debug("no errors, continuing for checks..");

				// get rid of the older session if any.
				session.invalidate();
				session = request.getSession(true);

				session.setAttribute(Constants.USER_KEY, user);

				String highlight = "home";
				request.setAttribute("hl", highlight);
				request.setAttribute("deptCode", user.getDepartment().getDepartmentCode());
				logger.debug("user object put in the session");

				session.setAttribute("deptId", user.getDepartment().getDepartmentId());

				nextPage = "versionOne";
				session.setAttribute(Constants.USER_TYPE, "V1");
				session.setAttribute("editable", editable);
			}
		} catch (UserNotFoundException unfe) {
			logger.debug("User not found exception occured " + unfe.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.username.doesNotExist"));
			saveErrors(request, errors);

			return (new ActionForward(mapping.getInput()));
		} catch (SQLException sqle) {
			logger.error("SQL Exception occured during log on " + sqle.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.database.connection"));
			saveErrors(request, errors);

			return (new ActionForward(mapping.getInput()));
		} catch (Exception e) {
			logger.error(e.getMessage());
			nextPage = "error";
		}

		return (mapping.findForward(nextPage));
	}
}
