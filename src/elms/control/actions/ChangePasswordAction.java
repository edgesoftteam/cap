package elms.control.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.*;
import elms.control.beans.LandForm;
import elms.control.beans.online.LoginForm;
import elms.security.*;

import elms.util.jcrypt;

public class ChangePasswordAction extends Action {
	static Logger logger = Logger.getLogger(ChangePasswordAction.class.getName());	
	protected String nextPage;
	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {		
		logger.debug("Change Password action class");
		String errorMessage = null;
		String successMessage = null;
		try {
			UserAgent ua = new UserAgent();
			LoginForm loginForm = (LoginForm)form;  
			String oldPassword = loginForm.getPassword();
			oldPassword = jcrypt.crypt("X6", oldPassword);
			String userName = loginForm.getUsername();
			logger.debug("username is " + userName);
			logger.debug("nextPage..:"+nextPage);
			User user =new User();
			
			user = ua.getOnlineUser(user,userName);
							nextPage = null;
			

			String newPassword = null;
			String confirmPassword = null;
			if(nextPage == null) {
				newPassword = loginForm.getNewPassword();
				confirmPassword = loginForm.getConfirmPassword();
				if(newPassword != null && !newPassword.equals(confirmPassword)) {
					errorMessage = "New password does not match with confirm password";
					nextPage = "fail";
				}	
			}
			if(nextPage == null) {

				newPassword = jcrypt.crypt("X6", newPassword);
				ua.updatePassword(loginForm.getUsername(), oldPassword, newPassword);
				successMessage = "Success </br>You have successfully changed your password! Please login using your new password.";
				//					EmailSender sender = new EmailSender();
				//					sender.sendEmails(loginForm.getUsername());
				nextPage = "success";	
			}
		} catch(Exception exp) {
			exp.printStackTrace();
			logger.error("Change Password action class:"+exp);
			errorMessage = "Faild change password";
			nextPage = "fail";
		}
		logger.debug("end of Change Password action");
		request.setAttribute("error", errorMessage);
		request.setAttribute("success", successMessage);
		return (mapping.findForward(nextPage));
	}	
}