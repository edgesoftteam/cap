package elms.control.actions.online;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.agent.LookupAgent;
import elms.agent.OnlineAgent;
import elms.common.Constants;
import elms.control.beans.EmailDetails;
import elms.control.beans.PaymentMgrForm;
import elms.control.beans.online.CartDetailsForm;
import elms.control.beans.online.MyPermitForm;
import elms.security.User;
import elms.util.EmailSender;

public class AddPermitAction extends Action {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(AddPermitAction.class.getName());
	
	

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		ActionErrors errors = new ActionErrors();
		MyPermitForm myPermitForm = (MyPermitForm) form;
		logger.debug("Entered Into AddPermitAction... "+myPermitForm.getActivityNo());
		String nextPage="success";
		String action = (String) request.getAttribute("action");
		logger.debug("action... "+action);
		String actNbr = myPermitForm.getActivityNo();
		OnlineAgent onlineAgent = new OnlineAgent();
		String email=null;
		HttpSession session = request.getSession();

		logger.debug("in if..."+((User) session.getAttribute(elms.common.Constants.USER_KEY)).getUsername());
		email =(String)session.getAttribute("email");
		logger.debug("session.. "+email);
		String statusMsg="";
		//String warnMessage="";
		try {
		if(actNbr != null) {
			int actId = new AdminAgent().getActivityId(actNbr);
			if(actId>0){
			int actPeopleId=onlineAgent.checkActNo(actNbr,email);
			logger.debug("actPeopleId... "+actPeopleId);
			if(actPeopleId == 0) {
				statusMsg= onlineAgent.checkAndAddPermitToEmailAddr(actNbr, email);
			}else if(actPeopleId == -1){
				String department = onlineAgent.getDeptDescriptionForActId(actId);
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.mypermits.notenabledforpayment",department));
			}else if(actPeopleId == -2){
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.mypermits.feeamountzero"));
			}else {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.mypermits.permitalreadytagged"));
			}
		}
		else{
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.mypermits.validpermitnumber"));
		}
		}
		if(!errors.empty()){
			logger.debug("error are added");
			saveErrors(request, errors);
		}
			logger.debug("statusMsg.. "+statusMsg);
			logger.debug("nextPage... "+nextPage);
		} catch (Exception e) {
			e.printStackTrace();
		}
		request.setAttribute("displayMsg", statusMsg);
		request.setAttribute("viewPermit", "Yes");
		request.setAttribute("isObc", "Y");
		request.setAttribute("isDot", "N");
		request.setAttribute("action", "view");
		request.setAttribute("inActive", "Yes");
		request.setAttribute("email", email);
		
		return (mapping.findForward(nextPage));
	}
}
