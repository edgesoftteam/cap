package elms.control.actions.online;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.OnlineAgent;
import elms.control.beans.online.CartDetailsForm;
import elms.security.User;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

public class CartDetailsAction extends Action {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(CartDetailsAction.class.getName());
	
	ActionErrors errors = null;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.debug("Entered Into CartDetailsAction ");
		HttpSession session = request.getSession();
		String levelId = request.getParameter("levelId");
		String email = request.getParameter("email");
		if(email == null) {
			email =((User) session.getAttribute(elms.common.Constants.USER_KEY)).getUsername();
		}
		logger.debug("email... "+email);
		
		String amount = "0.00";
		amount = request.getParameter("amount");
		String action =""; 
		if(request.getParameter("action") != null) {
			action=request.getParameter("action");
		}
		logger.debug("action... "+action);
		String from = "";
		if(request.getParameter("from") != null) {
			from=request.getParameter("from");
		}
		if(amount != "0.00" && amount != null) {
			amount = amount.replace("Rs.", "");
			amount = amount.replace("$", "");	
		}
		
		OnlineAgent onlineAgent = new OnlineAgent();
		CartDetailsForm cartDetailsForm = new CartDetailsForm();
		String cartId ="";
		String nextPage="success";
		List actList = new ArrayList();
		String displayMsg = "";
		
		List<CartDetailsForm> cartDetailsForms = new ArrayList<CartDetailsForm>();
		String totalamnt="0.00";
		
		logger.debug("cartId ... "+cartId);
		
		try {
			 cartId = onlineAgent.getCartId(email);
				if(cartId == null) {
					cartId = request.getParameter("cartId");	
				}
				logger.debug("cartId ... "+cartId);
				 if(cartId == "" || cartId == null || cartId.equalsIgnoreCase("") || cartId.equalsIgnoreCase("null")){
				cartId = StringUtils.i2s(new Wrapper().getNextId("CART_ID"));
			}
			logger.debug("cartId ... "+cartId);
			logger.debug(""+levelId+" "+amount);

			cartDetailsForm.setActivityId(levelId);
			NumberFormat formatter = new DecimalFormat("#0.00");
			logger.debug(formatter.format(StringUtils.s2d(amount)));
			cartDetailsForm.setAmount(formatter.format(StringUtils.s2d(amount)));
			cartDetailsForm.setCartId(cartId);
			cartDetailsForm.setEmail(email);
			logger.debug("from.. "+from);
			if(action != null || from !=null) {
				if(action.equalsIgnoreCase("add") ||  from.equalsIgnoreCase("vp_add")) {
					displayMsg = onlineAgent.saveCartDetails(cartDetailsForm);	
				}else if(action.equalsIgnoreCase("remove") ||  from.equalsIgnoreCase("vp_remove")) {
					displayMsg = onlineAgent.removeCartDetails(levelId,cartId);	
				}
			}
			if(cartId != null) {
				cartDetailsForm = onlineAgent.getCartInfo(cartId);
			}
			actList = onlineAgent.getComboActList(email);
			if(cartId != null) {
				cartDetailsForms = onlineAgent.getCartDetails(cartId);
				for(int i=0;i<cartDetailsForms.size();i++) {
					CartDetailsForm cartDetailsForm2 = new CartDetailsForm();
					cartDetailsForm2 = cartDetailsForms.get(i);
					cartId = cartDetailsForm2.getCartId();
					totalamnt = cartDetailsForm2.getTotalFeeAmount();
				}
			}
			
			logger.debug("displayMsg.. "+displayMsg);
			logger.debug("cartDetailsForm.getCartId().. "+cartDetailsForm.getCartId());
	
			if(from.equalsIgnoreCase("vp_add") || from.equalsIgnoreCase("vp_remove")) {
				request.setAttribute("viewPermit", "Yes");
				request.setAttribute("isObc", "Y");
				request.setAttribute("isDot", "N");
				request.setAttribute("action", "view");
				request.setAttribute("inActive", "Yes");
				request.setAttribute("email", email);
				request.removeAttribute("action");
				
				nextPage="vp_success";
			}
			logger.debug("nextPage... "+nextPage);
			logger.debug("cartDetailsForm.getCartId().. "+cartDetailsForm.getCartId());
		} catch (Exception e) {
			e.printStackTrace();
		}	
		request.setAttribute("cartId", cartId);
		request.setAttribute("cartDetailsForm", cartDetailsForm);
		request.setAttribute("cartDetailsForms", cartDetailsForms);
		request.setAttribute("displayMsg", displayMsg);
		request.setAttribute("actList", actList);
		request.setAttribute("totalamnt", totalamnt);
		
		return (mapping.findForward(nextPage));
		
	}

}
