package elms.control.actions.online;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.agent.LookupAgent;
import elms.agent.OnlineAgent;
import elms.common.Constants;
import elms.control.beans.EmailDetails;
import elms.control.beans.online.OnlineRegisterFrom;
import elms.util.Email;
import elms.util.EmailSender;
import elms.util.MessageUtils;
import elms.util.StringUtils;

public class OnlineRegisterOtherDetailsAction extends Action {
	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(OnlineRegisterOtherDetailsAction.class.getName());
	String nextPage = "success";
	ActionErrors errors = null;
	int lsoId = 0;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		OnlineRegisterFrom regForm = (OnlineRegisterFrom) form;
		OnlineAgent oPAgent = new OnlineAgent();
		logger.debug("Entered into OnlineRegisterOtherDetailsAction...");
		try {
			errors = new ActionErrors();
			oPAgent.saveOnlineUser(regForm);
			String obc = regForm.getObc();
			if (obc.equals("Y")) {
				String peopleType = oPAgent.checkOBCUser(regForm.getEmailAddress(), regForm.getPeopleType(), regForm);
				if (!peopleType.equals("")) {
					regForm.setPeopleType(peopleType);
					oPAgent.saveOBCUser(regForm);
				}
			}

			String toEmailAddress = regForm.getEmailAddress();
			if (toEmailAddress != null) {
//				String subject = "REGISTRATION_SUCCESS";
//				subject=new AdminAgent().getEmailSubject("REGISTRATION_SUCCESS");
//				Email email = new Email();
//				email.setToAddress(regForm.getEmailAddress().trim());
//				email.setSubject(subject);
//				String msg = "Hi, \n" + "User Login Request Approval Pending";
//				//email.setMessage(msg);
//				HashMap hm = new HashMap();
//				hm.put("USERNAME", regForm.getEmailAddress());
//				hm.put("PASSWORD", regForm.getPwd());
//				email.setMessage(StringUtils.parseString(new AdminAgent().getEmailMessage("REGISTRATION_SUCCESS"), hm));
//				
//				MessageUtils.sendEmail(email);

				try {
					EmailSender emailSender = new EmailSender();
					EmailDetails emailDetails = new EmailDetails();	
	    			if(LookupAgent.getKeyValue(Constants.PROD_SERVER_FLAG).equalsIgnoreCase("N")) {
	    				emailDetails.setEmailId(LookupAgent.getKeyValue(Constants.EDGESOFT_SUPPORT_EMAIL));
	    			}else if(LookupAgent.getKeyValue(Constants.PROD_SERVER_FLAG).equalsIgnoreCase("Y")) {
		    			emailDetails.setEmailId(regForm.getEmailAddress());	
	    			}
	    			emailDetails.setLkupSystemDataMap(OnlineAgent.getLkupSystemDataMap());
	    			emailDetails.setEmailTemplateAdminForm(OnlineAgent.getEmailData(emailDetails,Constants.REGISTRATION_SUCCESS));
		        	emailSender.sendEmail(emailDetails);
				
//		        	String message = StringUtils.parseString(new AdminAgent().getEmailMessage("REGISTRATION_SUCCESS"), hm);
//					new MessageUtils().sendEmail(regForm.getEmailAddress() + ";", subject, message);
				} catch (Exception e) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.register.email"));
					saveErrors(request, errors);
					logger.error("OnlineRegisterOtherDeatils action " + e.getMessage());
				}
			}

			request.setAttribute("message", "User Registered Successfully");

		} catch (Exception e) {
			logger.error("Exception occured " + e.getMessage());
		}

		return mapping.findForward(nextPage);
	}
}
