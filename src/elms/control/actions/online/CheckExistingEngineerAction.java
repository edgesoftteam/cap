package elms.control.actions.online;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.Operator;
import elms.agent.OnlineAgent;
import elms.control.beans.online.ApplyOnlinePermitForm;
import elms.security.User;
import elms.util.StringUtils;

public class CheckExistingEngineerAction extends Action {
	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(CheckExistingEngineerAction.class.getName());
	String nextPage = "successY";
	ActionErrors errors = null;
	int lsoId = 0;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		ApplyOnlinePermitForm applyOnlinePermitForm = (ApplyOnlinePermitForm) form;
		List tempOnlineDetails = new ArrayList();

		logger.debug("entered CheckExistingEngineerAction with  " + applyOnlinePermitForm.getChkExistPeopleFlag());

		//
		String peopleType = request.getParameter("peopleType");

		int peopleId = applyOnlinePermitForm.getPeopleId();

		int tempOnlineID = StringUtils.s2i((String) request.getParameter("tempOnlineID"));

		String emailAddr = ((User) session.getAttribute(elms.common.Constants.USER_KEY)).getFirstName();

		logger.debug("******************" + emailAddr);

		// String createNewFlag = StringUtils.nullReplaceWithEmpty(request.getParameter("createNewFlag"));
		applyOnlinePermitForm.setTempOnlineID(tempOnlineID);
		OnlineAgent onlinePermitAgent = new OnlineAgent();

		String chkExistPeopleFlag = applyOnlinePermitForm.getChkExistPeopleFlag();
		int peopleTypeId = 0;

		try {

			onlinePermitAgent.updatePeopleType(tempOnlineID, peopleType);
			StringBuilder sb = new StringBuilder();
			sb.append(request.getRequestURI());
			if(Operator.hasValue(request.getQueryString())){
				sb.append("?");
				sb.append(request.getQueryString());
			}
			
			String stepUrl = Operator.replace(sb.toString(), request.getContextPath(), "");
			onlinePermitAgent.updateStepUrl(tempOnlineID, stepUrl);
			
			// onlinePermitAgent.storePeopleId(tempOnlineID,peopleId,peopleType);
			// onlinePermitAgent.storeBusTaxReq(tempOnlineID,chkExistPeopleFlag);
			if (chkExistPeopleFlag.equalsIgnoreCase("Y")) {
				request.setAttribute("tempOnlineID", StringUtils.i2s(tempOnlineID));
				request.setAttribute("peopleType", peopleType);
				nextPage = "successY";

			}

			if (chkExistPeopleFlag.equalsIgnoreCase("N")) {
				logger.debug("entered CheckExistingEngineerAction with  " + chkExistPeopleFlag);
				request.setAttribute("tempOnlineID", StringUtils.i2s(tempOnlineID));
				applyOnlinePermitForm.setChkExistPeopleFlag("");
				nextPage = "successN";

			}

			if (chkExistPeopleFlag.equalsIgnoreCase("S")) {
				logger.debug("entered CheckExistingEngineerAction with  " + chkExistPeopleFlag);

				onlinePermitAgent.storeSelfPeople(tempOnlineID, emailAddr, peopleType);
				onlinePermitAgent.storePeopleId(tempOnlineID, peopleId, peopleType);
				request.setAttribute("tempOnlineID", StringUtils.i2s(tempOnlineID));
				applyOnlinePermitForm.setChkExistPeopleFlag("");

				nextPage = "successN";

			}

			request.setAttribute("tempOnlineID", StringUtils.i2s(tempOnlineID));

			// for self to display or not based on login
			peopleTypeId = onlinePermitAgent.getPeopleType(tempOnlineID, emailAddr, "4");// self
			if (peopleTypeId == 0) {
				peopleTypeId = onlinePermitAgent.getPeopleType(tempOnlineID, emailAddr);
			}
			request.setAttribute("peopleTypeId", StringUtils.i2s(peopleTypeId));

			// left side details bar display
			tempOnlineDetails = onlinePermitAgent.getTempOnlineDetails(tempOnlineID);
			request.setAttribute("tempOnlineDetails", tempOnlineDetails);

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception occured " + e.getMessage());
		}
		return (mapping.findForward(nextPage));
	}
}
