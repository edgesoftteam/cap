package elms.control.actions.online;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import elms.agent.ActivityAgent;
import elms.agent.CommonAgent;
import elms.agent.LookupAgent;
import elms.agent.OnlineAgent;
import elms.app.common.Attachment;
import elms.app.common.DisplayItem;
import elms.app.common.ObcFile;
import elms.common.Constants;
import elms.control.beans.online.OnlinePermitForm;
import elms.control.beans.online.TempPermitForm;
import elms.util.StringUtils;
import elms.util.db.Wrapper;



public class AddAttachmentAction extends Action {
	static Logger logger = Logger.getLogger(AddAttachmentAction.class.getName());
	protected int landId;
	
	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {		
		logger.debug("AddAttachmentAction action class");
		
		try {
			ActionErrors errors = new ActionErrors();
		
			HttpSession session = request.getSession();
			OnlinePermitForm addForm = (OnlinePermitForm) form;
			String action = request.getParameter("action");
			LookupAgent la=new LookupAgent();
			String userId =  StringUtils.i2s(((elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY)).getUserId());
			//<--koushik
			String tempId = null;
			tempId = (String) session.getAttribute("tempId");
			TempPermitForm tempPermitForm = (TempPermitForm) session.getAttribute("tempPermitForm");
			OnlineAgent onlineAgent = new OnlineAgent();
			String levelId=null;
			int attachmentId =0;
			int addedAttachmentId = 0;
			if(tempPermitForm != null){
				try {
					BeanUtils.copyProperties(addForm,tempPermitForm);
					
					//levelId = activityAgent.getTempLevelId(tempId);
					session.removeAttribute("tempPermitForm");
				} catch (IllegalAccessException e) {
					logger.error("Error copying properties" + e);
				} catch (InvocationTargetException e) {
					logger.error("Error copying properties" + e);
				} catch (Exception e) {
					logger.error(e.getMessage());
				}
			} 
			
			logger.debug("action/.....:"+action);
			if(action != null && action.equals("oai")) {
				return mapping.findForward("oai");
			}
			else if(action != null && action.equals("reset")) {
				logger.debug("action :"+action);
				if(tempId ==null)
					tempId = (String) session.getAttribute("tempId");
					
					onlineAgent.deleteTempPermit(tempId);
				addForm.reset1();
				session.removeAttribute("getAttachments");
				session.removeAttribute("levelId");
				request.setAttribute("error", "Permit has been cancelled successfully");				
				return mapping.findForward("can");
			}
			else if(action != null && action.equalsIgnoreCase("attachment")){
				FormFile file = addForm.getTheFile();
			
						Attachment attachment = new Attachment();
						CommonAgent attachmentAgent = new CommonAgent();
						file = addForm.getTheFile();
						String fileName = file.getFileName();
						logger.debug("filename before is " + fileName);
						int fileSize = file.getFileSize();
						logger.debug("filesize is " + fileSize);
						String contentType = file.getContentType();
						logger.debug("content type is " + contentType);
						String description = addForm.getDescription();

						String size = (file.getFileSize() + "");
						String location = "";
						Calendar enterDate = Calendar.getInstance();

						ObcFile obcFile = new ObcFile();
						
						fileName = StringUtils.replaceSpecialCharWithUnderscore(fileName);
						logger.debug("filename after is " + fileName);
						
						obcFile.setFileName(fileName);
						obcFile.setFileLocation(location);
						obcFile.setFileSize(size);
						obcFile.setCreateDate(enterDate);
						InputStream stream = file.getInputStream();
						// destroy the temporary file created
						file.destroy();
						
						if(tempId == null)
							tempId = (String) session.getAttribute("tempId");
						
						levelId = onlineAgent.getTempLevelId(tempId);
						addForm.setActId(levelId);
						logger.debug("levelId  :"+levelId);
						
						if(levelId == null){
							levelId = (String)session.getAttribute("levelId");
						    addForm.setActId(levelId);
						}
						if(levelId==null){
							Wrapper db = new Wrapper();
							levelId = db.getNextId("ACTIVITY_ID")+"";
							session.setAttribute("levelId",levelId);
							addForm.setActId(levelId);
						}

						attachment = new Attachment(attachmentId, "A", StringUtils.s2i(levelId), obcFile, description, enterDate, StringUtils.s2i(userId), "", "", "", "");

						// check if the file name is present and valid.
						if ((fileName == null) || fileName.equals("")) {

							errors.add("fileName", new ActionError("error.file.required"));
						}
						// check if the file size is valid.
						if (fileSize <= 0) {

							errors.add("fileSize", new ActionError("error.validfile.required"));
						}
						// check if description exists

						if ((description == null) || description.equals("")) {

							errors.add("description", new ActionError("error.description.required"));
						}

						// check if duplicate attachment already exists
						if (attachmentAgent.checkDuplicateAttachment(attachment)) {
							errors.add("duplicateAttachment", new ActionError("error.attachment.duplicate"));
						}

						// check if attachment file size and compare it to max allowed size
						if (!(attachmentAgent.checkAttachmentSize(attachment))) {
							errors.add("oversizeAttachment", new ActionError("error.attachment.oversize"));
						}

						// check if errors are empty and redirect back to the input page.
						if (!errors.empty()) {
							logger.debug("Going to validation error view");
							saveErrors(request, errors);
							request.setAttribute("levelId", levelId);
							request.setAttribute("level", "A");
							List attachments = new CommonAgent().getAttachments(Integer.parseInt(levelId), "A");
							logger.debug("size of attachments is " + attachments.size());
							request.setAttribute("attachments", attachments);
							return mapping.findForward("first");
						} else {

							request.setAttribute("contentType", contentType);
							request.setAttribute("attachment", attachment);
						 addedAttachmentId = attachmentAgent.saveAttachment(attachment, contentType, stream);
						logger.debug("Attachment is saved on the server and database with id " + addedAttachmentId);
					}
								
				logger.debug("levelId  :"+levelId);
				List<DisplayItem> getAttachments = la.getAttachments(levelId);
				session.setAttribute("getAttachmentsList", getAttachments);
				
				logger.debug("tempId  :"+tempId);
				
				String resumeUrl = request.getServletPath();
				String param = request.getQueryString();
				if(param != null)
					resumeUrl = resumeUrl + "?" + param;
				
				if(file != null){
					String realPath = (String) request.getSession().getServletContext().getRealPath("/");
			        String attachmentPath = realPath + "/attachments" + File.separator;
					logger.debug("filesize is " + fileSize);
					int attachmentSize = (fileSize/1024)/1024;
					logger.debug("attachmentSize is " + attachmentSize);
					if(attachmentSize<=Constants.ATTACHMENT_SIZE){
					onlineAgent.updateCAPTempWithAttachment(tempId, file.getFileName(),  attachmentPath, addForm.getDescription(), levelId, userId,addedAttachmentId);
				}
				}
				onlineAgent.updateCAPTempWithResumeUrl(tempId, resumeUrl, userId);
				
				if(getAttachments!=null && getAttachments.size()>0)
					request.setAttribute("description", "");
				//-->
				
				
				return mapping.findForward("first");
			}else if(action != null && action.equalsIgnoreCase("nxt")){
				String resumeUrl = request.getServletPath();
				String param = request.getQueryString();
				if(param != null)
					resumeUrl = resumeUrl + "?" + param;
				
				onlineAgent.updateEgovTempWithActType(tempId, addForm.getLsoId(), addForm.getActivityType(), userId, resumeUrl);
				return mapping.findForward("success");
			}else if(action != null && action.equalsIgnoreCase("wnext")){
			
				tempId = (String) session.getAttribute("tempId");
				logger.debug("tempId :"+tempId);
				//levelId = activityAgent.getTempLevelId(tempId);
				logger.debug("levelId  :"+levelId);
		
				session.setAttribute("getAttachmentsList", la.getAttachments(levelId));
				List<DisplayItem> getAttachments = la.getEgovTempAttachments(tempId);
				if(getAttachments!=null && getAttachments.size()>0){
				session.setAttribute("getAttachments", getAttachments);
				request.setAttribute("description","");
				}
			}else{
				
			}
			String attachId = request.getParameter("attachId");
			if(attachId != null){
				la.deleteAttachment(attachId);
				String levelIdNo = new LookupAgent().getLevelIdFromAttachmentId(attachId); //(String)session.getAttribute("levelId");
				session.setAttribute("getAttachmentsList", la.getAttachments(levelIdNo));
			}
		}
        catch (Exception e) {

            logger.error(e);
        }
		
		 return mapping.findForward("first");
}
}
