package elms.control.actions.online;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.OnlineAgent;
import elms.control.beans.PaymentMgrForm;

public class ConfirmProcessPaymentAction extends Action {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(ConfirmProcessPaymentAction.class.getName());
	String nextPage = "success";
	ActionErrors errors = null;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.debug("Entered Into ConfirmProcessPaymentAction ");

		HttpSession session = request.getSession();
		PaymentMgrForm paymentMgrForm = (PaymentMgrForm) form;
		//logger.debug("totalFeeAmt" + paymentMgrForm.getSsl_amount());
		//logger.debug("combo name" + paymentMgrForm.getComboName());
		String ssl_amount = "";
		String tempOnlineID = "";
		String ssl_card_number = "";
		String ssl_exp_date = "";
		String ssl_exp_date_year = "";
		String ssl_exp_date_month = "";
		String ssl_cvv2cvc2 = "";
		String ssl_avs_address = "";
		String ssl_avs_zip = "";
		String comboNo = "";
		String comboName = "";
		String ssl_country = "";
		String ssl_city = "";
		String ssl_ownerFName = "";
		String ssl_ownerLName = "";
		String ssl_state = "";

		String sprojId = paymentMgrForm.getLevelId();
		OnlineAgent onlinePermitAgent = new OnlineAgent();

		String contextHttpsRoot = request.getContextPath();

		request.setAttribute("contextHttpsRoot", contextHttpsRoot);

		try {

			tempOnlineID = paymentMgrForm.getTempOnlineID();
			request.setAttribute("tempOnlineID", tempOnlineID);
			ssl_amount = paymentMgrForm.getSsl_amount();
			ssl_card_number = paymentMgrForm.getSsl_card_number();
			ssl_exp_date = paymentMgrForm.getSsl_exp_date();
			ssl_exp_date_year = paymentMgrForm.getSsl_exp_date_year();
			ssl_exp_date_month = paymentMgrForm.getSsl_exp_date_month();
			ssl_cvv2cvc2 = paymentMgrForm.getSsl_cvv2cvc2();
			ssl_avs_address = paymentMgrForm.getSsl_avs_address();
			ssl_avs_zip = paymentMgrForm.getSsl_avs_zip();
			comboNo = paymentMgrForm.getComboNo();

			comboName = paymentMgrForm.getComboNo();
			ssl_ownerFName = paymentMgrForm.getSsl_ownerFName();
			ssl_ownerLName = paymentMgrForm.getSsl_ownerLName();
			ssl_city = paymentMgrForm.getSsl_city();
			ssl_country = paymentMgrForm.getSsl_country();
			ssl_state = paymentMgrForm.getSsl_state();

			request.setAttribute("comboNo", comboNo);
			request.setAttribute("comboName", paymentMgrForm.getComboName());
			request.setAttribute("ssl_amount", ssl_amount.substring(1));
			request.setAttribute("ssl_card_number", ssl_card_number);
			request.setAttribute("ssl_exp_date", ssl_exp_date);
			request.setAttribute("ssl_exp_date_year", ssl_exp_date_year);
			request.setAttribute("ssl_exp_date_month", ssl_exp_date_month);
			request.setAttribute("paymentMgrForm", paymentMgrForm);
			request.setAttribute("ssl_cvv2cvc2", ssl_cvv2cvc2);
			request.setAttribute("ssl_avs_address", ssl_avs_address);
			request.setAttribute("ssl_avs_zip", ssl_avs_zip);
			request.setAttribute("ssl_ownerFName", ssl_ownerFName);
			request.setAttribute("ssl_ownerLName", ssl_ownerLName);
			request.setAttribute("ssl_city", ssl_city);
			request.setAttribute("ssl_country", ssl_country);
			request.setAttribute("ssl_state", ssl_state);

			session.setAttribute("comboName", paymentMgrForm.getComboName());
			session.setAttribute("comboNo", comboNo);
			session.setAttribute("sprojId", sprojId);

		} catch (Exception e) {
			logger.error("Exception occured " + e.getMessage());
		}
		return (mapping.findForward(nextPage));
	}

}
