package elms.control.actions.online;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.Operator;
import elms.agent.OnlineAgent;
import elms.control.beans.online.ApplyOnlinePermitForm;
import elms.security.User;
import elms.util.StringUtils;

public class ValuationAction extends Action {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(ValuationAction.class.getName());
	String nextPage = "success";
	ActionErrors errors = null;
	int lsoId = 0;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		ApplyOnlinePermitForm applyOnlinePermitForm = (ApplyOnlinePermitForm) form;

		HttpSession session = request.getSession();
		OnlineAgent onlinePermitAgent = new OnlineAgent();
		String emailAddr = ((User) session.getAttribute(elms.common.Constants.USER_KEY)).getFirstName();

		int tempOnlineID = StringUtils.s2i((String) request.getParameter("tempOnlineID"));
		
		List tempOnlineDetails = new ArrayList();

		int peopleTypeId = 0;
		int peopleTypeIdCont = 0;

		try {
			
			boolean s = false;
			if(!Operator.hasValue(applyOnlinePermitForm.getValuation())){
				List al = onlinePermitAgent.getTempOnlineDetails(tempOnlineID);
				if(al.size()>0){
					   s =true;
						ApplyOnlinePermitForm a = (ApplyOnlinePermitForm) al.get(0);
						logger.info(a.getValuation()+"***VALUE");
						applyOnlinePermitForm.setValuation(a.getValuation());
						applyOnlinePermitForm.setSquareFootage(a.getSquareFootage());
						applyOnlinePermitForm.setNumberOfDwellingUnits(a.getNumberOfDwellingUnits());
						applyOnlinePermitForm.setNumberOfFloors(a.getNumberOfFloors());
					}
			}
			
			String valuation = applyOnlinePermitForm.getValuation();
			String squareFootage = applyOnlinePermitForm.getSquareFootage();
			String numberOfDwellingUnits = applyOnlinePermitForm.getNumberOfDwellingUnits();
			String numberOfFloors = applyOnlinePermitForm.getNumberOfFloors();
			
			if(!s){
				valuation = valuation.substring(1).replaceAll(",", "");
			}
			
			logger.info(valuation+"***VALUE");
			logger.info(squareFootage+"***VALUE");
			
			applyOnlinePermitForm.setTempOnlineID(tempOnlineID);

			StringBuilder sb = new StringBuilder();
			sb.append(request.getRequestURI());
			if(Operator.hasValue(request.getQueryString())){
				sb.append("?");
				sb.append(request.getQueryString());
			}
			
			String stepUrl = Operator.replace(sb.toString(), request.getContextPath(), "");
			
			onlinePermitAgent.updateValuation(tempOnlineID, valuation,squareFootage,numberOfDwellingUnits,numberOfFloors,stepUrl);
			/*onlinePermitAgent.updateValuation(tempOnlineID, valuation);
			onlinePermitAgent.updateSquareFootage(tempOnlineID,squareFootage);
			onlinePermitAgent.updateNumberOfDwellingUnits(tempOnlineID,numberOfDwellingUnits);
			onlinePermitAgent.updateNumberOfFloors(tempOnlineID,numberOfFloors);*/
			
			
			// for self to display or not based on login
			//peopleTypeId = onlinePermitAgent.getPeopleType(tempOnlineID, emailAddr, "8"); // OwnerBuilder
			//peopleTypeIdCont = onlinePermitAgent.getPeopleType(tempOnlineID, emailAddr, "4");// contractor
			//if (peopleTypeId == 0 && peopleTypeIdCont == 0) {
			//	nextPage = "search";
			//	applyOnlinePermitForm.setChkExistPeopleFlag("Y");
			//	request.setAttribute("peopleType", "C");
			//	request.setAttribute("applyOnlinePermitForm", applyOnlinePermitForm);
			//} else {
				nextPage = "success";
			//}
			//request.setAttribute("peopleTypeId", StringUtils.i2s(peopleTypeId));
			//request.setAttribute("peopleTypeIdCont", StringUtils.i2s(peopleTypeIdCont));

			// left side details bar display
			tempOnlineDetails = onlinePermitAgent.getTempOnlineDetails(tempOnlineID);
			request.setAttribute("tempOnlineDetails", tempOnlineDetails);

		} catch (Exception e) {
			logger.error("Exception occured " + e.getMessage());
		}
		return (mapping.findForward(nextPage));
	}

}
