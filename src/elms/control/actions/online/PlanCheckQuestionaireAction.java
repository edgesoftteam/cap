package elms.control.actions.online;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.Operator;
import elms.agent.OnlineAgent;
import elms.control.beans.online.ApplyOnlinePermitForm;
import elms.control.beans.online.OnlineRegisterFrom;
import elms.security.User;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

public class PlanCheckQuestionaireAction extends Action {
	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(PlanCheckQuestionaireAction.class.getName());

	ActionErrors errors = null;
	int lsoId = 0;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		ApplyOnlinePermitForm applyOnlinePermitForm = (ApplyOnlinePermitForm) form;
		OnlineAgent onlinePermitAgent = new OnlineAgent();
		int tempOnlineID = StringUtils.s2i((String) request.getParameter("tempOnlineID"));
		HttpSession session = request.getSession();
		try {
			int peopleId =  StringUtils.s2i((String) request.getParameter("peopleId"));
			
			if(peopleId<=0){
				User user = (elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY);
				
				//adding function to check people when navigating Back and front in the online screen while applying permit, if really people doesn't exist then create a new people
				peopleId = onlinePermitAgent.getPeopleId(user.getUsername(), 8); 
				if(peopleId <= 0){
					int userId = user.getUserId();
					OnlineRegisterFrom regForm = onlinePermitAgent.getOnlineUsers(user.getUsername(), "N");
					regForm.setPeopleType("8");
					peopleId = new Wrapper().getNextId("PEOPLE_ID");
					
					onlinePermitAgent.saveOBCUser(regForm,peopleId);
				}
				
			}
			onlinePermitAgent.updateStartDate(tempOnlineID);
			onlinePermitAgent.updatePeopleId(tempOnlineID, "W", peopleId);
		} catch (Exception e) {
		}
		List tempOnlineDetails = new ArrayList();

		request.setAttribute("tempOnlineID", StringUtils.i2s(tempOnlineID));
		List questionList = new ArrayList();
		List ackList = new ArrayList();

		try {
			
			applyOnlinePermitForm = new ApplyOnlinePermitForm();
			questionList = OnlineAgent.getQuestions(tempOnlineID);
			int size = questionList.size();
			ackList = OnlineAgent.getAcknowledgments(tempOnlineID);
			applyOnlinePermitForm.setTempOnlineID(tempOnlineID);
			
			request.setAttribute("questionList", questionList);
			request.setAttribute("ackList", ackList);
			
			StringBuilder sb = new StringBuilder();
			sb.append(request.getRequestURI());
			if(Operator.hasValue(request.getQueryString())){
				sb.append("?");
				sb.append(request.getQueryString());
			}
			
			String stepUrl = Operator.replace(sb.toString(), request.getContextPath(), "");
			onlinePermitAgent.updateStepUrl(tempOnlineID, stepUrl);
			// left side details bar display
			tempOnlineDetails = onlinePermitAgent.getTempOnlineDetails(tempOnlineID);

			request.setAttribute("tempOnlineDetails", tempOnlineDetails);

		} catch (Exception e) {
			logger.error("Exception occured " + e.getMessage());
		}

		return (mapping.findForward("success"));
	}
}
