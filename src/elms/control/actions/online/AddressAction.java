package elms.control.actions.online;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.LookupAgent;
import elms.agent.OnlineAgent;
import elms.app.common.DisplayItem;
import elms.common.Constants;
import elms.control.beans.online.OnlinePermitForm;
import elms.control.beans.online.TempPermitForm;
import elms.util.StringUtils;

public class AddressAction extends Action {

	static Logger logger = Logger.getLogger(AddressAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		logger.debug("Address action class");
		HttpSession session = request.getSession();
		String userId = StringUtils
				.i2s(((elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY)).getUserId());// (String)
																												// session.getAttribute("userId");
		OnlinePermitForm addForm = (OnlinePermitForm) form;
		logger.debug("unit... "+addForm.getStreetUnit());
		LookupAgent la = new LookupAgent();
		TempPermitForm tempPermitForm = (TempPermitForm) session.getAttribute("tempPermitForm");
		String tempId = null;
		if (tempPermitForm != null) {
			try {
				BeanUtils.copyProperties(addForm, tempPermitForm);
				tempId = (String) session.getAttribute("tempId");
				addForm.setTempId(tempId);
				session.removeAttribute("tempPermitForm");
			} catch (IllegalAccessException e) {
				logger.error("Error copying properties" + e);
			} catch (InvocationTargetException e) {
				logger.error("Error copying properties" + e);
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}
logger.debug("addForm"+addForm.getStreetName());
		request.setAttribute("onlinePermitFrom", addForm);
		if (userId == null || userId.length() == 0) {
			return (mapping.findForward("index"));
		}
		try {
			String action = request.getParameter("action");

			if (action == null && tempPermitForm == null) {
				addForm.reset1();
			}

			String streetNoFlag = (request.getParameter("streetNoFlag") != null) ? request.getParameter("streetNoFlag")
					: "";

			String unitFlag = (request.getParameter("unitFlag") != null) ? request.getParameter("unitFlag") : "";

			String streetNo = (request.getParameter("streetNo") != null) ? request.getParameter("streetNo") : "";

			String streetName = (request.getParameter("streetName") != null) ? request.getParameter("streetName").trim()
					: "";
			LookupAgent lkupAgent = new LookupAgent();
			List<DisplayItem> displayItemLst = new ArrayList<DisplayItem>();

			System.out.println(" streetNoFlag  " + streetNoFlag + " unitFlag " + unitFlag + " streetNo " + streetNo
					+ " streetName " + streetName);
			if (streetNoFlag.equalsIgnoreCase("Yes")) {

				// displayItemLst=lkupAgent.getStreetNumber(streetName);
				displayItemLst = lkupAgent.getStreetNum(streetName);
				String buffer = "<select name=\"streetNoList\" class=\"form-control\" onchange=\"getUnitDropdown()\"><option value=\"\">Select</option>";

				for (int i = 0; i < displayItemLst.size(); i++) {
					DisplayItem dsplyItem = displayItemLst.get(i);
					buffer = buffer + "<option value=\"" + dsplyItem.getFieldOne() + "\">" + dsplyItem.getFieldOne()
							+ "</option>";
				}
				buffer = buffer + "</select>";

				if (displayItemLst.size() <= 0) {
					buffer = "";
				}
				response.getWriter().println(buffer);
				System.out.println(buffer);
				return (mapping.findForward(null));
			}

			if (unitFlag.equalsIgnoreCase("Yes")) {

				displayItemLst = lkupAgent.getUnitLst(streetName, streetNo);
				String buffer = "<select name=\"unitLst\" class=\"form-control\" onchange=\"getUnitVal();\"><option value=\"\">Please Select</option>";

				for (int i = 0; i < displayItemLst.size(); i++) {
					DisplayItem dsplyItem = displayItemLst.get(i);
					buffer = buffer + "<option value=\"" + dsplyItem.getFieldOne().trim() + "\">"
							+ dsplyItem.getFieldOne().trim() + "</option>";
				}
				buffer = buffer + "</select>";

				if (displayItemLst.size() <= 0) {
					buffer = "";
				}
				response.getWriter().println(buffer);
				// System.out.println(buffer);
				session.removeAttribute("levelId");
				session.removeAttribute("getAttachmentsList");
				return (mapping.findForward(null));

			}
			if (action != null && action.equals("validateAddress")) {
				String strName = request.getParameter("streetName");
				String streetNumber = request.getParameter("streetNum");
				String unit = request.getParameter("streetUnit");
				String streetMod = request.getParameter("streetMod");
				logger.debug("strName :" + strName + "streetNumber :" + streetNumber + "unit :" + unit+"streetMod : "+streetMod);
				String message = "";
//				logger.debug("lsoId " + addForm.getLsoId());
				String lsoId = la.getLsoIdFromAddress(streetNumber, strName, unit,streetMod);
				logger.debug("lsoId " + lsoId);
				String useDesc=la.checkLSOId(lsoId);
				
				//lsoid not in residential LSO_USE
				if(useDesc.equalsIgnoreCase("")) {
					
					if (lsoId != null && !lsoId.equalsIgnoreCase("")) {
						int holdId = la.checkForAddressHold(lsoId);
						if (holdId != 0) {
							message = "Hold";
						} else {
							message = "success";
							String addressId = la.getAddrIdfromLsoId(lsoId);
							logger.debug("addressId ::"+addressId);
							OnlineAgent onlineAgent = new OnlineAgent();
							int count = onlineAgent.applyForGaragesalePermit(addressId);
							if(count >= 2){
								message="notallow";
							}else{
								message = "success";
								Date lastIssuedDate = onlineAgent.getlastIssuedDate(addressId);
								/*SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy MM dd");
								String input= formatter1.format(lastIssuedDate);
								DateTimeFormatter formatter = DateTimeFormatter.ofPattern ( "yyyy MM dd" );
								LocalDate issuedDate = formatter.parse ( input , LocalDate :: from );
								
								Period period = Period.between ( LocalDate.now() , issuedDate );
								logger.debug("days: "+period.getDays ());*/
								if(lastIssuedDate!= null){
								Calendar cal= Calendar.getInstance();
								cal.setTime(lastIssuedDate);
								cal.add(Calendar.DATE, StringUtils.s2i(LookupAgent.getSystemURL(Constants.GS_PERMIT_ISSUE_PERIOD_IN_DAYS)));
								logger.debug("cal : "+cal.getTime());
								
								Calendar today = Calendar.getInstance();
								today.setTime(new Date());
								logger.debug("today : "+today.getTime());
								if(today.compareTo(cal)<0){
									message="period";
								}else{
									message="success";
								}
								}
							}
						}
					} else {
						message = "fail";
					}
				}else {
					message="Residential";
				}
				logger.debug("message.. "+message);
				PrintWriter pw = response.getWriter();
				pw.write(message);
				// pw.write("@@@@@");
				return null;

			} else if (action != null && action.equals("next")) {

			
				logger.debug(addForm.getStreetUnit() + addForm.getStreetNum() + addForm.getStreetName() + addForm.getStreet());
				String activityReset = "reset";
				request.setAttribute("activityReset", activityReset);

				//String flag = la.searchAddress(addForm.getStreetNum(), addForm.getStreetName(), addForm.getStreetUnit());
				String flag = la.getLsoIdFromAddress(addForm.getStreetNum(), addForm.getStreetName(), addForm.getStreetUnit(),addForm.getStreetMod());
				logger.debug("lsoId/flag :: " + flag);
				OnlineAgent egovAgent = new OnlineAgent();

				if (flag != null && flag.length() > 0) {
					addForm.setLsoId(flag);
					logger.debug(addForm.getLsoId());
					// --<koushik
					String resumeUrl = request.getServletPath();
					tempId = addForm.getTempId();
					// logger.debug("tempId :"+tempId);
					if (tempId == null && addForm.getActivityType() != null && addForm.getDepartmentType() != null) {
						tempId = (String) session.getAttribute("tempId");
					}

					if (tempId == null) {
						tempId = egovAgent.insertEgovTempWithAddress(addForm.getLsoId(), addForm.getStreetNum(),
								addForm.getStreetName(), userId, resumeUrl, addForm.getStreetUnit());
					} else {
						egovAgent.updateEgovTempWithAddress(addForm.getLsoId(), addForm.getStreetNum(),
								addForm.getStreetName(), userId, resumeUrl, addForm.getStreetUnit(), tempId);
					}

					session.setAttribute("tempId", tempId);
					// -->
					session.removeAttribute("peopleListAll");
					session.removeAttribute("getAttachments");
					// session.removeAttribute("levelId");
					return mapping.findForward("success");
				} else {
					addForm.setStreetNum("0");
					addForm.setStreetName("Non Locational");
					addForm.setLsoId("1");
					logger.debug(addForm.getLsoId());
					session.removeAttribute("peopleListAll");
					session.removeAttribute("getAttachments");
					session.removeAttribute("levelId");
					return mapping.findForward("success");

					// String error = "Invalid Address";
					// request.setAttribute("error", error);
					// fillDropDown(request);
					// return mapping.findForward("first");
				}
			} else {
				logger.debug("addForm"+addForm.getStreetName());
				fillDropDown(request,addForm);
				
				String buffer="";
				logger.debug("form..."+addForm.getStreetName());
				if(addForm.getStreetName() != null) {
					displayItemLst = la.getUnitLst(addForm.getStreetName(),addForm.getStreetNum());
					buffer = "<select name=\"unitLst\" class=\"form-control\" onchange=\"getUnitVal();\"><option value=\"\">Please Select</option>";

					for (int i = 0; i < displayItemLst.size(); i++) {
						DisplayItem dsplyItem = displayItemLst.get(i);
						buffer = buffer + "<option value=\"" + dsplyItem.getFieldOne().trim() + "\">"
								+ dsplyItem.getFieldOne().trim() + "</option>";
					}
					buffer = buffer + "</select>";
		
				}
				
				if (displayItemLst.size() <= 0) {
					buffer = "";
				}
				logger.debug("buffer.. "+buffer);
				session.setAttribute("buffer", buffer);
				return mapping.findForward("first");
			}
		} catch (Exception exp) {
			logger.error("error in address action page...:" + exp);
			exp.printStackTrace();
		}
		logger.debug("end of Address action");
		return (mapping.findForward("success"));
	}

	public static void fillDropDown(HttpServletRequest request,OnlinePermitForm form) {
		try {
			LookupAgent la = new LookupAgent();
			List<DisplayItem> displayItemLst = new ArrayList<DisplayItem>();
			String buffer="";
			logger.debug("form..."+form.getStreetName());
			if(form.getStreetName() != null) {
				displayItemLst = la.getUnitLst(form.getStreetName(),form.getStreetNum());
				buffer = "<select name=\"unitLst\" class=\"form-control\" onchange=\"getUnitVal();\"><option value=\"\">Please Select</option>";

				for (int i = 0; i < displayItemLst.size(); i++) {
					DisplayItem dsplyItem = displayItemLst.get(i);
					buffer = buffer + "<option value=\"" + dsplyItem.getFieldOne().trim() + "\">"
							+ dsplyItem.getFieldOne().trim() + "</option>";
				}
				buffer = buffer + "</select>";
	
			}
			
			if (displayItemLst.size() <= 0) {
				buffer = "";
			}
			
			request.setAttribute("streetPreList", la.getStreetUnitInfo("pre_dir"));
			request.setAttribute("unitList", buffer);
			// request.setAttribute("streetNameList",
			// la.getStreetUnitInfo("str_name"));
			request.setAttribute("streetNameList", la.getStreetNames());
		} catch (Exception exp) {
			exp.printStackTrace();
			logger.error("error while filling dropdowns:" + exp);
		}
	}
}