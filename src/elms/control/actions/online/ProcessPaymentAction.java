package elms.control.actions.online;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.LookupAgent;
import elms.agent.OnlineAgent;
import elms.common.Constants;
import elms.control.beans.PaymentMgrForm;
import elms.security.User;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

public class ProcessPaymentAction extends Action {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(ProcessPaymentAction.class.getName());
	
	ActionErrors errors = null;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.debug("Entered Into ProcessPaymentAction "+request.getParameter("levelId"));
		String nextPage = "success";
		HttpSession session = request.getSession();
		PaymentMgrForm paymentMgrForm = (PaymentMgrForm) form;

		String ssl_amount = "";
		String tempOnlineID = "";
		String sprojId = "";
		String actId = "";

		OnlineAgent onlinePermitAgent = new OnlineAgent();
		String action = (String) (request.getAttribute("action")!=null? request.getAttribute("action"):"");
		try {
			Calendar d = Calendar.getInstance();
			List YY = new ArrayList();

			int yyCurrent = d.get(Calendar.YEAR);
			for (int i = 0; i < Constants.ONLINE_CREDIT_EXP_YEAR_SIZE; i++) {
				YY.add(new Integer(yyCurrent));
				yyCurrent = yyCurrent + 1;

			}
			request.setAttribute("YY", YY);
			
			Wrapper db = new Wrapper();
			String activityNumber = "";

			tempOnlineID = db.getNextId("PARKING_TEMP_ID")+"";

			String contextHttpsRoot = request.getContextPath();
			request.setAttribute("contextHttpsRoot", contextHttpsRoot);

			//tempOnlineID = paymentMgrForm.getTempOnlineID();
			
			request.setAttribute("tempOnlineID", tempOnlineID);
			// actId = request.getParameter("levelId");
			actId = (String) ((request.getParameter("levelId")) != null ? (request.getParameter("levelId")) : request.getAttribute("levelId"));

			String comboNo = "";
			comboNo = new ActivityAgent().getActivtiyNumber(StringUtils.s2i(actId));
			request.setAttribute("comboNo", comboNo);
			String comboId=actId;
			request.setAttribute("comboId", comboId);
          
			List subTyp = new ActivityAgent().getSubTypes(StringUtils.s2i(actId));
			String comboName = Constants.ONLINE_RESIDENTIAL_PERMIT;
			if (subTyp.size() > 0) {
				for (int i = 0; i < subTyp.size(); i++)
					comboName = comboName + " - " + subTyp.get(i);

			}
			
			

			// ssl_amount = request.getParameter("amount");
			ssl_amount = (String) ((request.getParameter("amount")) != null ? (request.getParameter("amount")) : request.getAttribute("amount"));
			// String action = (String)((request.getParameter("amount")) != null ? (request.getParameter("amount")) : request.getAttribute("amount"));
			logger.debug("ssl_amount..."+ request.getParameter("amount"));
			logger.debug("ssl_amount..."+ssl_amount);
			 String from = "";
			 if(request.getParameter("from") != null) {
				 from = (String)((request.getParameter("from")) != null ? (request.getParameter("from")) : request.getAttribute("from")); 
			 }
			action="lncvPayment";
			if(action.equalsIgnoreCase("lncvPayment")){
//				ssl_amount = (String) session.getAttribute("lncvamt");
				comboName = Constants.ONLINE_LNCV_PERMIT;
				actId = (String) (session.getAttribute("activityId")!=null? session.getAttribute("activityId"):"0");
				String actNum = LookupAgent.getActivityNbrForActId(Integer.parseInt(actId));
				
				session.setAttribute("actNum", actNum);
				
//				ssl_amount= StringUtils.str2$(ssl_amount); //(throwing error while value is like "1208.55")
				tempOnlineID = (String) (session.getAttribute("payfull")!=null? session.getAttribute("payfull"):"N");
				paymentMgrForm.setTempOnlineID(tempOnlineID);
			}
			logger.debug("comboName" + comboName);
			logger.debug("ssl_amount " +ssl_amount);
			
//			if(StringUtils.s2i(ssl_amount) == 0 || ssl_amount == null || ssl_amount.equalsIgnoreCase("$0.00") || ssl_amount.equals("")){
//				ssl_amount = "0";
//				nextPage = "cPayment";
//			}
			
	
			request.setAttribute("comboName", comboName);
			
			request.setAttribute("ssl_amount", ssl_amount);
			request.setAttribute("sprojId", sprojId);
			paymentMgrForm.setAmount(ssl_amount);
			paymentMgrForm.setComboNo(comboNo);
			paymentMgrForm.setComboName(comboName);
			paymentMgrForm.setLevelId(actId);
			session.setAttribute("paymentMgrForm", paymentMgrForm);
			User user=(User)session.getAttribute("user");  
			logger.debug("user firstname "+user.getFirstName());
			logger.debug("user fullname "+user.getFullName());
			logger.debug("user useremail "+user.getUserEmail());
			logger.debug("user username "+user.getUsername());
			logger.debug("user password "+user.getPassword());
			
			OnlineAgent onlineAgent = new OnlineAgent();

			logger.debug(""+ ((PaymentMgrForm)request.getSession().getAttribute("paymentMgrForm")).getLevelId());
			Map<String, String> lkupSystemDataMap = new HashMap<String,String>(); 		
			lkupSystemDataMap = onlineAgent.getLkupSystemDataMap();			
			session.setAttribute("lkupSystemDataMap",lkupSystemDataMap);
			session.setAttribute("from", from);
			session.setAttribute("user", user);
			nextPage = "successCart";
			logger.debug("nextPage  "+nextPage);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception occured " + e.getMessage());
		}
		return (mapping.findForward(nextPage));
	}

}
