package elms.control.actions.online;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.LookupAgent;
import elms.agent.OnlineAgent;
import elms.control.beans.online.OnlinePermitForm;
import elms.control.beans.online.TempPermitForm;
import elms.util.StringUtils;

public class GSPermitTypeAction extends Action {

	static Logger logger = Logger.getLogger(GSPermitTypeAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		
		logger.debug("PermitType action class");
		HttpSession session = request.getSession();           
		String userId = StringUtils.i2s(((elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY)).getUserId());
		OnlinePermitForm addForm = (OnlinePermitForm) form;
		OnlineAgent egovAgent = new OnlineAgent();
		LookupAgent la = new LookupAgent();
		 
				
		String tempId = null;
		TempPermitForm tempPermitForm = (TempPermitForm) session.getAttribute("tempPermitForm");
		
		if(tempPermitForm != null){
			try {
				BeanUtils.copyProperties(addForm, tempPermitForm);
				tempId = (String) session.getAttribute("tempId");
				session.removeAttribute("tempPermitForm");
			} catch (IllegalAccessException e) {
				logger.error("Error copying properties" + e);
			} catch (InvocationTargetException e) {
				logger.error("Error copying properties" + e);
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		} 
		
		request.setAttribute("onlinePermitFrom", addForm);		
		logger.debug("selected activity type..:"+addForm.getActivityType());
				if(userId == null || userId.length() == 0) {
					return (mapping.findForward("index"));
				}
				try {
					String action = request.getParameter("action");
					if(action != null && action.equals("pnext")) {
						logger.debug("next action in permit type action..."+action);
						if(addForm.getActivityType() != null){
							if(tempId == null)
								tempId = (String) session.getAttribute("tempId");
							
							String resumeUrl = request.getServletPath();
							String param = request.getQueryString();
							if(param != null)
								resumeUrl = resumeUrl + param;
							
							egovAgent.updateEgovTempWithActType(tempId, addForm.getLsoId(), addForm.getActivityType(), userId, resumeUrl);
						}
						
					} else if(action != null && action.equals("prev")) {
						return mapping.findForward("prev");
					}
					else if(action != null && action.equals("reset")) {
						if(tempId ==null)
							tempId = (String) session.getAttribute("tempId");
							//egovAgent.deleteTempPermit(tempId);
						addForm.reset1();
						session.removeAttribute("peopleListAll");
						session.removeAttribute("getAttachments");
						session.removeAttribute("levelId");
						addForm = new OnlinePermitForm();
						
						request.setAttribute("streetNameList", la.getStreetNames());
						request.setAttribute("onlinePermitFrom", addForm);
						request.setAttribute("error", "Permit has been cancelled successfully");				
						logger.debug("Action...:"+action);
						return mapping.findForward("ptb");
					}else if(action != null && action.equals("nn")){
						return mapping.findForward("sub");
					}else if(action != null && action.equals("sub")){
						if(tempId == null)
							tempId = (String) session.getAttribute("tempId");
						
						String resumeUrl = request.getServletPath();
						String param = request.getQueryString();
						if(param != null)
							resumeUrl = resumeUrl + "?" + param;
						
						return mapping.findForward("sub");
					}else{
						//fillDropDown(request,session, addForm.getActivityType(),addForm.getDepartmentType());
						
						
						
						return mapping.findForward("first");
					}
				} catch(Exception exp) {
					logger.error("error in permit type action page...:"+exp);
				}
				logger.debug("end of permit type");
				return (mapping.findForward("success"));
			}
			
			/*public static void fillDropDown(HttpServletRequest request,HttpSession session, String type,String departmentType) {
				try {
					 LookupAgent la = new LookupAgent();
					 //request.setAttribute("sites", la.getSites());
					 request.setAttribute("activityType", la.getActivityTypes(departmentType));
					 request.setAttribute("subActivityType", la.getActivitySubTypes(type));
					 session.setAttribute("subActivityType", la.getActivitySubTypes(type));

					 //session.setAttribute("subActivityType", la.getActivitySubTypes(type));
				 } catch(Exception exp) {
					 exp.printStackTrace();
					 logger.error("error while filling dropdowns:"+exp);
				 }
			}
*/
}