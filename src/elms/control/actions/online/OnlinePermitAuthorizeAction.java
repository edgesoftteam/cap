package elms.control.actions.online;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.Operator;
import elms.agent.FinanceAgent;
import elms.agent.OnlineAgent;
import elms.app.finance.ActivityFee;
import elms.app.finance.ActivityFeeEdit;
import elms.app.finance.FinanceSummary;
import elms.app.project.ActivityDetail;
import elms.control.beans.FeesMgrForm;
import elms.control.beans.online.ApplyOnlinePermitForm;
import elms.security.User;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

/**
 * @author sunvoyage
 *
 */
public class OnlinePermitAuthorizeAction extends Action {
	
	static Logger logger = Logger.getLogger(OnlinePermitAuthorizeAction.class.getName());
	String nextPage = "success";
	ActionErrors errors = null;
	String lsoId = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		ApplyOnlinePermitForm applyOnlinePermitForm = (ApplyOnlinePermitForm) form;

		int tempOnlineID = StringUtils.s2i((String) request.getParameter("tempOnlineID"));
		//String forcePC = "N";//(String) request.getParameter("forcePC");
		String forcePC = (String) request.getParameter("forcePC");
		logger.debug("********forcePC**********::::::" + forcePC);

		String emailAddr = ((User) session.getAttribute(elms.common.Constants.USER_KEY)).getUsername();
		logger.debug("******************" + emailAddr);

		applyOnlinePermitForm.setTempOnlineID(tempOnlineID);
		OnlineAgent onlinePermitAgent = new OnlineAgent();

		int peopleId = applyOnlinePermitForm.getPeopleId();
		String contextHttpsRoot = request.getContextPath();
		logger.debug("contextHttpsRoot is " + contextHttpsRoot);
		request.setAttribute("contextHttpsRoot", contextHttpsRoot);

		try {
			request.setAttribute("tempOnlineID", StringUtils.i2s(tempOnlineID));
			// added from update new action
			String userId = "0";
			applyOnlinePermitForm = (ApplyOnlinePermitForm) onlinePermitAgent.getAllDetails(tempOnlineID, forcePC, emailAddr);

			lsoId = StringUtils.i2s(applyOnlinePermitForm.getLsoId());

			String lsoType = applyOnlinePermitForm.getLsoType();
			
			StringBuilder sb = new StringBuilder();
			sb.append(request.getRequestURI());
			if(Operator.hasValue(request.getQueryString())){
				sb.append("?");
				sb.append(request.getQueryString());
			}
			
			String stepUrl = Operator.replace(sb.toString(), request.getContextPath(), "");
			onlinePermitAgent.updateStepUrl(tempOnlineID, stepUrl);
			
			// parameter to decide which method to call.
			// String router = lsoType;
			String router = "";

			String actid = StringUtils.i2s(applyOnlinePermitForm.getSubProjectId());
			logger.debug("Activity ID ------------------" + actid);

			request.setAttribute("id", actid);
			session.setAttribute("sTypeId", applyOnlinePermitForm.getStypeId() + "");
			session.setAttribute("online", "Y");

			// Changing Redirecting to payment after calculating fees

			int activityId = StringUtils.s2i(actid);
			//int sTypeId = onlinePermitAgent.getSubTypeId(tempOnlineID);
			
			logger.debug("Activity Id :" + activityId);

			String amt = "$0.00";

			if (forcePC.equalsIgnoreCase("N")) {
	
				onlinePermitAgent.processfees(activityId, tempOnlineID);
				amt = StringUtils.dbl2$(new OnlineAgent().getAmount(actid));
			    logger.debug("STEP       3 ------------------ DOING" + amt);
				logger.debug("STEP       3 ------------------ DOING" + actid);
				// end
				router = "N";
			} else {

				router = "Y";
			}

			request.setAttribute("level", "A");
			request.setAttribute("levelId", actid);
			request.setAttribute("amount", amt);
			session.setAttribute("level", "A");
			session.setAttribute("levelId", actid);

			if ("N".equalsIgnoreCase(router)) {
				nextPage = "successPayment";
			}

			if ("Y".equalsIgnoreCase(router)) {
				nextPage = "successPC";
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception occured " + e.getMessage());
		}

		return (mapping.findForward(nextPage));
	}
	
	
	
	
}
