package elms.control.actions.online;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.Operator;
import elms.agent.OnlineAgent;
import elms.control.beans.online.ApplyOnlinePermitForm;
import elms.security.User;
import elms.util.StringUtils;

public class ApplyPermitAction extends Action {
	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(ApplyPermitAction.class.getName());
	String nextPage = "success";
	ActionErrors errors = null;
	int lsoId = 0;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		ApplyOnlinePermitForm applyOnlinePermitForm = (ApplyOnlinePermitForm) form;

		OnlineAgent onlinePermitAgent = new OnlineAgent();
		String checkAddress = "";
		String unitNumber = null;
		String streetNumber = null;
		String streetFraction = null;
		int streetId = 0;
		int sprojId = 0;
		String lso_type = "";
		List tempOnlineDetails = new ArrayList();

		checkAddress = StringUtils.nullReplaceWithEmpty((String) request.getParameter("checkAddress"));
		HttpSession session = request.getSession();
		try {
			if (checkAddress.equalsIgnoreCase("yes")) {

				errors = new ActionErrors();
				
				
				
				// address components
				streetNumber = StringUtils.nullReplaceWithEmpty(applyOnlinePermitForm.getStreetNumber());
				logger.debug("streetNumber is " + streetNumber);

				streetId = StringUtils.s2i(StringUtils.nullReplaceWithEmpty(applyOnlinePermitForm.getStreetName()));
				logger.debug("streetId is " + streetId);

				streetFraction = (StringUtils.nullReplaceWithEmpty(applyOnlinePermitForm.getStreetFraction()));
				logger.debug("streetFraction is " + streetFraction);

				unitNumber = applyOnlinePermitForm.getUnitNumber();
				logger.debug("unitNumber is " + unitNumber);

				List lsoLists = new ArrayList();
				lsoLists = onlinePermitAgent.checkAddress(streetNumber, streetId, StringUtils.checkString(streetFraction), unitNumber, lso_type, applyOnlinePermitForm);

				lso_type = applyOnlinePermitForm.getLsoType();
				logger.debug("LSO Type is " + lso_type);
				ApplyOnlinePermitForm applyOnlinePermitForm1 = null;

				if (lsoLists.size() > 0) {
					int toid = applyOnlinePermitForm.getTempOnlineID();

					for (int i = 0; i < lsoLists.size(); i++) {
						applyOnlinePermitForm1 = (ApplyOnlinePermitForm) lsoLists.get(i);
						if (applyOnlinePermitForm1.getLsoType().equalsIgnoreCase("S")) {
							lsoId = applyOnlinePermitForm1.getLsoId();
						}
					}

				} else if (lsoLists.size() <= 0) {
					// if errors exist, then forward to the input page.
					logger.debug("Address did not match");
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.address.onlinemismatch"));
					saveErrors(request, errors);
				}

				logger.debug("LSO ID is "+lsoId);
				if (!errors.empty()) {

					logger.debug("Errors exist, going back to the Address page");
					logger.debug("Input Is " + mapping.getInput());
					return (new ActionForward(mapping.getInput()));
				} else {
					applyOnlinePermitForm.setLsoType(lso_type);
					
					User user = (elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY);
					int userId = user.getUserId();
					
					
				
					logger.debug("Entered into insert Function");
					int tempOnlineID = onlinePermitAgent.addTempAddress(lsoId, streetNumber, streetFraction, streetId, unitNumber, lso_type,userId,"");

					// left side details bar display
					tempOnlineDetails = onlinePermitAgent.getTempOnlineDetails(tempOnlineID);
					request.setAttribute("tempOnlineDetails", tempOnlineDetails);

					applyOnlinePermitForm.setTempOnlineID(tempOnlineID);

					nextPage = "success";
				}
			}
			
		} catch (Exception e) {
			logger.error("Exception occured " + e.getMessage());
		}
		return (mapping.findForward(nextPage));
	}
}
