package elms.control.actions.online;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.PeopleAgent;
import elms.app.people.People;
import elms.common.Constants;
import elms.control.beans.PeopleForm;
import elms.control.beans.PeopleSearchForm;
import elms.util.StringUtils;

public class OnlineRegistrationSearchResultsAction extends Action {
	static Logger logger = Logger.getLogger(OnlineRegistrationSearchResultsAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.info("Entering ViewPeopleSearchResultsAction");
		HttpSession session = request.getSession();
		PeopleForm peopleForm = (PeopleForm) form;
		ActionErrors errors = new ActionErrors();

		String nextPage = "";
		String peopleType = "";
		try {
			String psaType = (String) session.getAttribute(Constants.PSA_TYPE);
			String psaId = (String) session.getAttribute(Constants.PSA_ID);

			logger.debug("obtained psa type from session as " + psaType);
			request.setAttribute("psaType", psaType);
			logger.debug("obtained psaId from session as " + psaId);
			request.setAttribute("psaId", psaId);

			String peopleTypeId = peopleForm.getPeople().getPeopleType().getCode();
			if (!(peopleTypeId == null) && !peopleTypeId.trim().equals("")) {
				// peopleTypeId = (String) session.getAttribute("peopleTypeId");
				peopleType = PeopleAgent.getPeopleType(peopleTypeId);
				session.setAttribute("peopleType", peopleType);
				session.setAttribute("peopleTypeId", peopleTypeId);
				logger.debug("People Type Id : " + peopleTypeId + " : People Type :" + peopleType);
			}

			String licenseNumber = peopleForm.getPeople().getLicenseNbr();
			logger.debug("Got license number as " + licenseNumber);

			String name = peopleForm.getPeople().getName();
			logger.debug("Got name as " + name);

			String phoneNumber = StringUtils.phoneFormat(peopleForm.getPeople().getPhoneNbr());
			logger.debug("Got phone number as " + phoneNumber);
			if (phoneNumber == null) {
				phoneNumber = "";
			}

			String phoneExt = peopleForm.getPeople().getExt();
			logger.debug("Got phone ext as " + phoneExt);

			List peoples = new PeopleAgent().getSearchResults(peopleTypeId, licenseNumber, name, phoneNumber, phoneExt, "");
			logger.debug("obtained people search results of size " + peoples.size());
			People people = null;
			String strPeopleId = null;

			logger.debug("In people size != 1 condition");
			nextPage = "listPeople";

			PeopleSearchForm peopleSearchForm = new PeopleSearchForm();
			peopleSearchForm.setPsaId(psaId);
			peopleSearchForm.setPsaType(psaType);
			peopleSearchForm.setPeopleList(peoples);

			request.setAttribute("peopleList", peoples);
			request.setAttribute("peopleSearchForm", peopleSearchForm);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		logger.info("Exiting to (" + nextPage + ")");

		return (mapping.findForward(nextPage));
	}
}
