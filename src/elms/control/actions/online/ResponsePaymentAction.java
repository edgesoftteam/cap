package elms.control.actions.online;



import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.LookupAgent;
import elms.agent.OnlineAgent;
import elms.agent.UserAgent;
import elms.common.Constants;
import elms.control.beans.PaymentMgrForm;
import elms.security.User;
import elms.util.StringUtils;


public class ResponsePaymentAction extends Action {
	static Logger logger = Logger.getLogger(ResponsePaymentAction.class.getName());
	protected String nextPage;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {		
		logger.debug("Get ResponsePaymentAction action class");
		try {    
			
			HttpSession session = request.getSession();
			
			PaymentMgrForm paymentMgrForm = (PaymentMgrForm) session.getAttribute("paymentMgrForm");	    
			        
			 int userId =0; 
			 logger.debug(" Activity id from payeezy "+StringUtils.nullReplaceWithZero(request.getParameter("x_invoice_num")));
			 String actId =(String) StringUtils.nullReplaceWithZero(request.getParameter("x_invoice_num"));    
			 userId =LookupAgent.getCreatedByForActId(actId);
			    
			 logger.debug(" User id from payeezy "+userId);
			 
			if(userId<= 0) {
				return (mapping.findForward("index"));
			}
			
			request.setAttribute("userId", userId);
			logger.debug(" User Id from  x_invoice_num"  + userId);
			
			   
			UserAgent ua = new UserAgent();
			
			User user = new User();
			user = ua.getOnlineUser(user, userId);       
			//LoginForm logonForm = new LoginForm();
			
			//logonForm.setUsername(user.getUsername());
			//logonForm.setPassword(user.getPassword());
			
			session.setAttribute("activityId",actId);  
			
			session.setAttribute("user", user);
			
			String ssl_amount = "";
			String tempOnlineID = "";
			String comboNo = "";
			String comboName = "";
			String ssl_ownerFName = "";
			String ssl_ownerLName = "";
			String sprojId = actId;  
			
			//tempOnlineID = paymentMgrForm.getTempOnlineID();
			//request.setAttribute("tempOnlineID", tempOnlineID);
			
			ssl_amount = paymentMgrForm.getSsl_amount();
			comboNo = LookupAgent.getActivityNbrForActId(StringUtils.s2i(actId));
			
			paymentMgrForm.setComboNo(comboNo);
			paymentMgrForm.setComboName(Constants.ONLINE_LNCV_PERMIT);

			comboName = paymentMgrForm.getComboName();
			ssl_ownerFName = paymentMgrForm.getSsl_ownerFName();
			ssl_ownerLName = paymentMgrForm.getSsl_ownerLName();
			
			request.setAttribute("comboNo", comboNo);
			request.setAttribute("comboName", paymentMgrForm.getComboName());
			//request.setAttribute("ssl_amount", ((String)session.getAttribute("x_amount")).substring(1));
			request.setAttribute("paymentMgrForm", paymentMgrForm);
	
			//request.setAttribute("ssl_avs_address", request.getParameter("x_address"));
			//request.setAttribute("ssl_avs_zip", request.getParameter("x_zip"));
			//request.setAttribute("ssl_ownerFName", request.getParameter("CardHoldersName"));
			//request.setAttribute("ssl_ownerLName", "");
			//request.setAttribute("ssl_city", request.getParameter("x_city"));
			//request.setAttribute("ssl_country", "United States");
			//request.setAttribute("ssl_state", request.getParameter("x_state"));

			session.setAttribute("comboName", paymentMgrForm.getComboName());
			session.setAttribute("comboNo", comboNo);
			session.setAttribute("sprojId", actId);
            String payerName =StringUtils.nullReplaceWithEmpty(request.getParameter("CardHoldersName"));
			 
			logger.debug("**********************"+request.getParameter("CardHoldersName"));   
			
			logger.debug("**********************"+request.getParameter("x_ship_to_last_name"));
			logger.debug("**********************"+request.getParameter("x_trans_id") + " "+request.getParameter("Bank_Resp_Code_2")+ " "+request.getParameter("x_auth_code")+ " "+request.getParameter("Bank_Resp_Code"));
			logger.debug("**********************"+request.getParameter("x_address") + " " +request.getParameter("x_zip")+" "+request.getParameter("x_city")+" "+request.getParameter("x_state"));  
            String responseCode=StringUtils.nullReplaceWithEmpty((String)request.getParameter("x_response_code"));
            logger.debug("**********responseCode************"+responseCode);  
            logger.debug("***********x_amount***********"+StringUtils.nullReplaceWithEmpty(request.getParameter("x_amount")));    
            
            session.setAttribute("responseAction","payment");   
			OnlineAgent opa = new OnlineAgent();
			
			session.setAttribute("x_amount",StringUtils.nullReplaceWithEmpty(request.getParameter("x_amount")));
            if( responseCode.equalsIgnoreCase("1")) {
				// storing result into tables.
				  

				//Important    
				paymentMgrForm.setOnlineTxnId(request.getParameter("x_trans_id"));
     
            int invoiceExistCount=0;  
            paymentMgrForm.setAmount(StringUtils.nullReplaceWithEmpty(request.getParameter("x_amount")));    
			//if (paymentMgrForm.getComboName().equals(Constants.ONLINE_LNCV_PERMIT)) {
				List lncvDtList = new ArrayList();
				//String actId = (String) (session.getAttribute("activityId") != null ? session.getAttribute("activityId") : "0");
				//
				logger.debug(" Activity id from x_invoice_num" + actId);
				String lncvNextYear = (String) (session.getAttribute("lncvNextYear") != null ? session.getAttribute("lncvNextYear") : "N");
				//lncvDtList = (ArrayList) session.getAttribute("lncvDtList");
				//logger.debug("lncvDtList :"+lncvDtList+""+lncvDtList.size()+""+lncvDtList.get(0));   
				//String activityId = opa.processLncvPermit(StringUtils.nullReplaceWithEmpty(actId), (elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY), ((String)session.getAttribute("x_amount")), lncvDtList, paymentMgrForm,StringUtils.nullReplaceWithEmpty(lncvNextYear));   
				//session.setAttribute("activityId", activityId);
				OnlineAgent.saveOnlinePayment( StringUtils.nullReplaceWithEmpty(actId), Constants.ONLINE_LNCV_PERMIT,StringUtils.nullReplaceWithEmpty(request.getParameter("x_trans_id")), StringUtils.nullReplaceWithEmpty(request.getParameter("x_amount")), userId,StringUtils.nullReplaceWithEmpty(request.getParameter("x_auth_code")), StringUtils.nullReplaceWithEmpty(request.getParameter("x_auth_code")), StringUtils.nullReplaceWithEmpty(request.getParameter("x_address")) +" "+StringUtils.nullReplaceWithEmpty(request.getParameter("x_city"))+" "+StringUtils.nullReplaceWithEmpty(request.getParameter("x_state"))+ " " +StringUtils.nullReplaceWithEmpty(request.getParameter("x_zip")), "",payerName);
           // } 

            }else {
            	
            	opa.rollBackTransaction(Integer.parseInt(actId));   
            }
			session.setAttribute("paymentMgrForm", paymentMgrForm);
			session.setAttribute("responseAction","payment");
			session.setAttribute("user", (elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY));   
			
			return (mapping.findForward("successPayment"));
			
			  
						
		} catch(Exception exp) {
			exp.printStackTrace();  
			logger.error("Error in responsePayment action class:"+exp);
			return (mapping.findForward("error"));
		}
		//logger.debug("end of get responsePayment action");
		
	}	
}