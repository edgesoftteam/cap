/*
 * Created on Apr 24, 2008 by Sunil V
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package elms.control.actions.online;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.Operator;
import elms.agent.FinanceAgent;
import elms.agent.OnlineAgent;
import elms.app.finance.ActivityFee;
import elms.app.finance.ActivityFeeEdit;
import elms.app.finance.FinanceSummary;
import elms.app.project.ActivityDetail;
import elms.control.actions.finance.ViewPaymentMgrAction;
import elms.control.beans.FeesMgrForm;
import elms.control.beans.online.ApplyOnlinePermitForm;
import elms.security.User;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

public class UpdateOnlinePermitAction extends Action {
	/**
	 * author Sunil
	 */
	static Logger logger = Logger.getLogger(UpdateOnlinePermitAction.class.getName());
	String nextPage = "successQ";
	ActionErrors errors = null;
	String lsoId = "";
	String updatePermit = "no";
	List tempOnlineDetails = new ArrayList();

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.debug("Entered Action UpdateOnlinePermitAction ");

		ApplyOnlinePermitForm applyOnlinePermitForm = (ApplyOnlinePermitForm) form;
		HttpSession session = request.getSession();
		String emailAddr = ((User) session.getAttribute(elms.common.Constants.USER_KEY)).getFirstName();
		OnlineAgent onlinePermitAgent = new OnlineAgent();
		int tempOnlineID = StringUtils.s2i((String) request.getParameter("tempOnlineID"));

		String contextHttpsRoot = request.getContextPath();

		request.setAttribute("contextHttpsRoot", contextHttpsRoot);

		logger.debug("Entered Action UpdateOnlinePermitAction with tempOnlineID=  " + tempOnlineID);
		try {
		String forcePC = request.getParameter("forcePC");
		if (((forcePC.equals(""))) && !(forcePC != null)) {
			forcePC = (String) request.getAttribute("forcePC");
		}
		StringBuilder sb = new StringBuilder();
		sb.append(request.getRequestURI());
		if(Operator.hasValue(request.getQueryString())){
			sb.append("?");
			sb.append(request.getQueryString());
		}
		
		String stepUrl = Operator.replace(sb.toString(), request.getContextPath(), "");
		onlinePermitAgent.updateStepUrl(tempOnlineID, stepUrl);
		logger.debug("Entered Action UpdateOnlinePermitAction with forcePC=  " + forcePC);

		request.setAttribute("tempOnlineID", StringUtils.i2s(tempOnlineID));
		int qSize = 0 ;
		if(Operator.hasValue(applyOnlinePermitForm.getCheckArrayQuestionSize())){
			qSize = Integer.parseInt(applyOnlinePermitForm.getCheckArrayQuestionSize());
			logger.debug("Answer to the question size :" + qSize);
			if (qSize > 0) {
				
				ArrayList<String> a = new ArrayList<String>();
				for(int i=0;i<qSize;i++){
					sb = new StringBuilder();
					String q[] = Operator.split((String) request.getParameter("checkQuestionFlagY"+i),"_");
					sb.append("insert into TEMP_ONLINE_QUESTIONAIRE (TEMP_ONLINEID,QUES_ID,OPTION_VALUE) VALUES (");
					sb.append(tempOnlineID);
					sb.append(",");
					sb.append(q[1]);
					sb.append(",");
					sb.append(StringUtils.checkString(q[0]));
					sb.append(")");
					a.add(sb.toString());
				}
				
				boolean result = false;
				if(a.size()>0){
					result = new OnlineAgent().deleteOnlineQuestionaire(tempOnlineID);
					if(result){
						 result = new OnlineAgent().saveOnlineQuestionaire(a);
					}
				}
				
				logger.debug("question save :" + result);
			}
		}
		

			if (forcePC.equalsIgnoreCase("Y")) {

				// updating the start date
				String startDate = applyOnlinePermitForm.getStartDate();
				logger.debug("Start Date is " + startDate);
				//if (startDate != null)
					onlinePermitAgent.updateStartDate(tempOnlineID);

				updatePermit = StringUtils.nullReplaceWithEmpty((String) request.getParameter("updatePermit"));
				logger.debug("updatePermit " + updatePermit);
				if (updatePermit.equalsIgnoreCase("yes")) {

					String userId = "0";
					applyOnlinePermitForm = (ApplyOnlinePermitForm) onlinePermitAgent.getAllDetails(tempOnlineID, forcePC, emailAddr);

					lsoId = StringUtils.i2s(applyOnlinePermitForm.getLsoId());

					String lsoType = applyOnlinePermitForm.getLsoType();

					String router = "";

					router = "payment";
					String sprojId = StringUtils.i2s(applyOnlinePermitForm.getSubProjectId());
					request.setAttribute("level", "A");
					request.setAttribute("levelId", sprojId);

					session.setAttribute("level", "A");
					session.setAttribute("levelId", sprojId);

					logger.debug("Entering Now to Payment------------------");
					
					
					//start copy 
					
					String actid = StringUtils.i2s(applyOnlinePermitForm.getSubProjectId());
					logger.debug("Activity ID ------------------" + actid);

					request.setAttribute("id", actid);
					session.setAttribute("sTypeId", applyOnlinePermitForm.getStypeId() + "");
					session.setAttribute("online", "Y");

					// Changing Redirecting to payment after calculating fees

					int activityId = StringUtils.s2i(actid);
					//int sTypeId = onlinePermitAgent.getSubTypeId(tempOnlineID);
					
					
					onlinePermitAgent.processfees(activityId, tempOnlineID);
					
					logger.debug("Activity Id :" + activityId);

					String amt = "$0.00";
					amt = StringUtils.dbl2$(new OnlineAgent().getAmount(actid));
					logger.debug("STEP       3 ------------------ DOING" + amt);
					logger.debug("STEP       3 ------------------ DOING" + actid);
					
					request.setAttribute("amt", amt);
						
					// end copy
					
				
					if ("payment".equalsIgnoreCase(router)) {

						new ViewPaymentMgrAction().perform(mapping, form, request, response);
						// processPayment();

					}

				

				
					nextPage = "successQ";
				
				}
			}

			request.setAttribute("forcePC", forcePC);
			if (forcePC.equalsIgnoreCase("N")) {

				nextPage = "successAuthorize";
			}
			
			logger.debug("FINISH PROCESSSSSSSSSSSSSSSSS " + nextPage);
			// left side details bar display
			tempOnlineID = StringUtils.s2i((String) request.getParameter("tempOnlineID"));
			tempOnlineDetails = onlinePermitAgent.getTempOnlineDetails(tempOnlineID);

			int peopleTypeId = onlinePermitAgent.getPeopleType(tempOnlineID, emailAddr, "2");// self
			if (peopleTypeId == 0) {
				peopleTypeId = onlinePermitAgent.getPeopleType(tempOnlineID, emailAddr);
			}
			request.setAttribute("peopleTypeId", StringUtils.i2s(peopleTypeId));
			request.setAttribute("tempOnlineDetails", tempOnlineDetails);

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception occured " + e.getMessage());
		}
		return mapping.findForward(nextPage);
	}

}
