package elms.control.actions.online;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.Operator;
import elms.agent.LookupAgent;
import elms.agent.OnlineAgent;
import elms.control.beans.online.ApplyOnlinePermitForm;
import elms.security.User;
import elms.util.StringUtils;

public class StructureAction extends Action {
	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(StructureAction.class.getName());
	String nextPage = "success";
	ActionErrors errors = null;
	int lsoId = 0;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		ApplyOnlinePermitForm applyOnlinePermitForm = (ApplyOnlinePermitForm) form;
		int tempOnlineID = StringUtils.s2i((String) request.getParameter("tempOnlineID"));
		int activityTypeId = applyOnlinePermitForm.getStypeId();
		String activityType = "";
		String activityTypeDescription = "";
		String lsoType = (String) request.getParameter("lsoType");
		String sName = (String) request.getParameter("sName");
		logger.debug("sName is " + sName);
		
		try {
			activityType = LookupAgent.getLkupActivityTypeForActId(activityTypeId);
			activityTypeDescription = LookupAgent.getActivityTypeForTypeId(sName);
		} catch (Exception e1) {
			logger.error("Unable to get activity type from id " + e1.getMessage());
		}

		List comboNames = new ArrayList();
		HttpSession session = request.getSession();
		OnlineAgent onlinePermitAgent = new OnlineAgent();
		
		User user = (User) session.getAttribute(elms.common.Constants.USER_KEY);
		logger.debug("User::" + user);
		String emailAddr = user.getFirstName();

		List tempOnlineDetails = new ArrayList();

		try {

			StringBuilder sb = new StringBuilder();
			sb.append(request.getRequestURI());
			if(Operator.hasValue(request.getQueryString())){
				sb.append("?");
				sb.append(request.getQueryString());
			}
			
			String stepUrl = Operator.replace(sb.toString(), request.getContextPath(), "");
			
			
			applyOnlinePermitForm.setSubProjectName(activityTypeDescription);
			applyOnlinePermitForm.setTempOnlineID(tempOnlineID);
			onlinePermitAgent.updateActivityNameAndType(tempOnlineID, activityTypeDescription, sName,stepUrl);
			onlinePermitAgent.storeSelfPeople(tempOnlineID, emailAddr, "A");
			comboNames = LookupAgent.getActivitySubTypesForActivityTypeId(sName);
			request.setAttribute("comboNames", comboNames);

			// left side details bar display
			tempOnlineDetails = onlinePermitAgent.getTempOnlineDetails(tempOnlineID);
			request.setAttribute("tempOnlineDetails", tempOnlineDetails);
			nextPage = "success";

		}

		catch (Exception e) {
			logger.error("Exception occured " + e.getMessage());
		}
		return (mapping.findForward(nextPage));
	}
}
