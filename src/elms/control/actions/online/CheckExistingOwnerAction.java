package elms.control.actions.online;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.Operator;
import elms.agent.OnlineAgent;
import elms.control.beans.online.ApplyOnlinePermitForm;
import elms.security.User;
import elms.util.StringUtils;

public class CheckExistingOwnerAction extends Action {

	static Logger logger = Logger.getLogger(CheckExistingOwnerAction.class.getName());
	String nextPage = "successY";
	ActionErrors errors = null;
	int lsoId = 0;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		ApplyOnlinePermitForm applyOnlinePermitForm = (ApplyOnlinePermitForm) form;

		logger.debug("entered CheckExistingOwnerAction with  " + applyOnlinePermitForm.getChkExistPeopleFlag());
		List tempOnlineDetails = new ArrayList();
		String peopleType = request.getParameter("peopleType") != null ? request.getParameter("peopleType") : "W";

		int tempOnlineID = StringUtils.s2i((String) request.getParameter("tempOnlineID"));
		String emailAddr = ((User) session.getAttribute(elms.common.Constants.USER_KEY)).getFirstName();
		logger.debug("Email Address= "+ emailAddr);

		applyOnlinePermitForm.setTempOnlineID(tempOnlineID);
		OnlineAgent onlinePermitAgent = new OnlineAgent();

		String chkExistPeopleFlag = applyOnlinePermitForm.getChkExistPeopleFlag();

		try {

			logger.info(chkExistPeopleFlag+"***********************");
			/*if(!Operator.hasValue(chkExistPeopleFlag)){
				List al = onlinePermitAgent.getTempOnlineDetails(tempOnlineID);
				if(al.size()>0){
					  
						ApplyOnlinePermitForm a = (ApplyOnlinePermitForm) al.get(0);
						logger.info(a.getUrl()+"***VALUE");
						URL u = new URL("http://localhost:8080"+a.getUrl());
						selectPeopleType = Operator.toInt(onlinePermitAgent.splitQuery(u).get("sName"));
						logger.info(selectPeopleType+"***selectPeopleType");
						applyOnlinePermitForm.setSelectPeopleType(selectPeopleType);
					}
			}*/
			
			onlinePermitAgent.updatePeopleType(tempOnlineID, peopleType);
			
			

			if (chkExistPeopleFlag.equalsIgnoreCase("Y")) {
				request.setAttribute("tempOnlineID", StringUtils.i2s(tempOnlineID));
				request.setAttribute("peopleType", peopleType);
				nextPage = "successY";

			}

			if (!(chkExistPeopleFlag.equalsIgnoreCase("Y")) && !(chkExistPeopleFlag.equalsIgnoreCase("S"))) {
				String checkOwnerOrPeople = applyOnlinePermitForm.getCheckOwnerOrPeople();
				logger.debug("entered CheckExistingOwnerAction with People or Owner " + chkExistPeopleFlag);
				logger.debug("entered CheckExistingOwnerAction with People or Owner " + checkOwnerOrPeople);

				onlinePermitAgent.createPeopleOnOwner(tempOnlineID, chkExistPeopleFlag, checkOwnerOrPeople);
				request.setAttribute("tempOnlineID", StringUtils.i2s(tempOnlineID));

				applyOnlinePermitForm.setChkExistPeopleFlag("");
				nextPage = "successN";

			}

			if (chkExistPeopleFlag.equalsIgnoreCase("S")) {
				logger.debug("entered CheckExistingOwnerAction with  " + chkExistPeopleFlag);

				onlinePermitAgent.storeSelfPeople(tempOnlineID, emailAddr, peopleType);

				request.setAttribute("tempOnlineID", StringUtils.i2s(tempOnlineID));
				applyOnlinePermitForm.setChkExistPeopleFlag("");

				nextPage = "successN";

			}
			
			StringBuilder sb = new StringBuilder();
			sb.append(request.getRequestURI());
			if(Operator.hasValue(request.getQueryString())){
				sb.append("?");
				sb.append(request.getQueryString());
			}
			
			String stepUrl = Operator.replace(sb.toString(), request.getContextPath(), "");
			onlinePermitAgent.updateStepUrl(tempOnlineID, stepUrl);
			// left side details bar display
			tempOnlineDetails = onlinePermitAgent.getTempOnlineDetails(tempOnlineID);
			request.setAttribute("tempOnlineDetails", tempOnlineDetails);

		} catch (Exception e) {
			logger.error("Exception occured " + e.getMessage());
		}
		return (mapping.findForward(nextPage));
	}
}
