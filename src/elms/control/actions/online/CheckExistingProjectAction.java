package elms.control.actions.online;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.Operator;
import elms.agent.OnlineAgent;
import elms.control.beans.online.ApplyOnlinePermitForm;
import elms.util.StringUtils;

public class CheckExistingProjectAction extends Action {
	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(CheckExistingProjectAction.class.getName());
	String nextPage = "exist";
	ActionErrors errors = null;
	int lsoId = 0;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		ApplyOnlinePermitForm applyOnlinePermitForm = (ApplyOnlinePermitForm) form;

		int tempOnlineID = StringUtils.s2i((String) request.getParameter("tempOnlineID"));

		applyOnlinePermitForm.setTempOnlineID(tempOnlineID);
		OnlineAgent onlinePermitAgent = new OnlineAgent();

		String chkExistProjFlag = applyOnlinePermitForm.getChkExistProjFlag();

		chkExistProjFlag = "N";
		logger.debug("chkExistProjFlag valure is " + chkExistProjFlag);

		List newProjectDetails = new ArrayList();
		List tempOnlineDetails = new ArrayList();
		
		try {
			String projectNumber = StringUtils.nullReplaceWithEmpty(applyOnlinePermitForm.getProjectNumber());
			String selectProject = StringUtils.nullReplaceWithEmpty(applyOnlinePermitForm.getSelectProject());

			logger.debug("projectNumber valure is " + projectNumber);
			logger.debug("selectProject valure is " + selectProject);
			
			StringBuilder sb = new StringBuilder();
			sb.append(request.getRequestURI());
			if(Operator.hasValue(request.getQueryString())){
				sb.append("?");
				sb.append(request.getQueryString());
			}
			
			String stepUrl = Operator.replace(sb.toString(), request.getContextPath(), "");
			onlinePermitAgent.updateStepUrl(tempOnlineID, stepUrl);

			String createNewFlag = "NEW";

			newProjectDetails = OnlineAgent.getOnlineProjectDetails(tempOnlineID, selectProject, createNewFlag);
			logger.debug("tempOnlineID in else" + tempOnlineID);
			applyOnlinePermitForm.setTempOnlineID(tempOnlineID);

			request.setAttribute("tempOnlineID", StringUtils.i2s(tempOnlineID));
			request.setAttribute("newProjectDetails", newProjectDetails);
			onlinePermitAgent.setProjectNumber(tempOnlineID, projectNumber, createNewFlag);
			nextPage = "new";

			tempOnlineDetails = onlinePermitAgent.getTempOnlineDetails(tempOnlineID);
			int stypeID = 0;
			for (int i = 0; i < tempOnlineDetails.size(); i++) {
				applyOnlinePermitForm = (ApplyOnlinePermitForm) tempOnlineDetails.get(i);
				stypeID = applyOnlinePermitForm.getStypeId();
			}
			int startsAfter = OnlineAgent.getStartsAfter(tempOnlineID, stypeID);
			logger.debug("startsAfter is " + startsAfter);
			request.setAttribute("startsAfter", StringUtils.i2s(startsAfter));
			request.setAttribute("tempOnlineDetails", tempOnlineDetails);

		} catch (Exception e) {
			logger.error("Exception occured " + e.getMessage());
		}
		logger.debug("going to nextpage = "+nextPage);
		return (mapping.findForward(nextPage));
	}
}
