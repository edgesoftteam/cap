package elms.control.actions.online;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.Operator;
import elms.agent.OnlineAgent;
import elms.control.beans.online.ApplyOnlinePermitForm;
import elms.util.StringUtils;

public class PermitTypeAction extends Action {
	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(PermitTypeAction.class.getName());
	String nextPage = "success";
	ActionErrors errors = null;
	int lsoId = 0;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		ApplyOnlinePermitForm applyOnlinePermitForm = (ApplyOnlinePermitForm) form;
		String activitySubTypeId = request.getParameter("useMapId");
		logger.debug("activitySubTypeId = "+activitySubTypeId);
		
		OnlineAgent onlinePermitAgent = new OnlineAgent();
		String valuation = "0.0";
		int tempOnlineID = StringUtils.s2i((String) request.getParameter("tempOnlineID"));
		int useMapId = StringUtils.s2i((String) request.getParameter("useMapId"));
		
		String feeIds = (String) request.getParameter("feeIds");

		int feeUnitNo = applyOnlinePermitForm.getFeeUnitNo();
		List tempOnlineDetails = new ArrayList();

		try {

			errors = new ActionErrors();
			
			StringBuilder sb = new StringBuilder();
			sb.append(request.getRequestURI());
			if(Operator.hasValue(request.getQueryString())){
				sb.append("?");
				sb.append(request.getQueryString());
			}
			
			String stepUrl = Operator.replace(sb.toString(), request.getContextPath(), "");

			request.setAttribute("valuation", valuation);
			logger.debug("valuation is " + valuation);
			onlinePermitAgent.setComboName(tempOnlineID, useMapId, feeUnitNo);
			onlinePermitAgent.setFeeNames(tempOnlineID, feeIds,stepUrl);

			applyOnlinePermitForm.setTempOnlineID(tempOnlineID);
			// left side details bar display
			tempOnlineDetails = onlinePermitAgent.getTempOnlineDetails(tempOnlineID);

			request.setAttribute("tempOnlineDetails", tempOnlineDetails);

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception occured " + e.getMessage());
		}
		return (mapping.findForward(nextPage));
	}

}
