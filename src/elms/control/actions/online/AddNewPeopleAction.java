package elms.control.actions.online;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.OnlineAgent;
import elms.agent.PeopleAgent;
import elms.app.admin.PeopleType;
import elms.app.people.People;
import elms.control.beans.PeopleForm;

public class AddNewPeopleAction extends Action {
	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(AddNewPeopleAction.class.getName());
	String nextPage = "success";
	ActionErrors errors = null;
	int lsoId = 0;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.info("Entered AddNewPeopleAction...");
		HttpSession session = request.getSession();
		String peopleType = request.getParameter("peopleType");
		String tempOnlineID = request.getParameter("tempOnlineID");
		request.setAttribute("tempOnlineID", tempOnlineID);
		request.setAttribute("peopleType", peopleType);
		nextPage = "success";
		try {
			String action = request.getParameter("action") != null ? request.getParameter("action") : "";
			if (action.equals("save")) {
				PeopleForm peopleForm = (PeopleForm) form;
				PeopleAgent peopleAgent = new PeopleAgent();
				People people = null;
				people = peopleForm.getPeople();
				PeopleType peopleTypeObj = peopleAgent.getPeopleTypeObj(peopleType);
				people.setPeopleType(peopleTypeObj);
				int people_id = peopleAgent.checkPeople(people);
				if (people_id == 0) {
					people = peopleAgent.addPeople(people);
					if (people == null) {
						errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.people.addFailed"));
					}
				} else {
					people.setPeopleId(people_id);
				}
				OnlineAgent onlinePermitAgent = new OnlineAgent();
				request.setAttribute("peopleId", people.getPeopleId() + "");
				List peopleSearchList = new ArrayList();
				peopleSearchList = onlinePermitAgent.getSearchResults(people.getName(), "FS", peopleType);
				request.setAttribute("peopleSearchList", peopleSearchList);
				nextPage = "listPeople";
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception occured " + e.getMessage());
		}
		logger.info("Exiting AddNewPeopleAction...");
		return (mapping.findForward(nextPage));
	}
}
