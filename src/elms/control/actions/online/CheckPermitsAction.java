package elms.control.actions.online;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class CheckPermitsAction extends Action {
	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(CheckPermitsAction.class.getName());
	String nextPage = "success";
	ActionErrors errors = null;
	int lsoId = 0;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.info("Entered CheckPermitsAction...");
		HttpSession session = request.getSession();

		logger.info("Exiting CheckPermitsAction...");
		return (mapping.findForward(nextPage));
	}
}
