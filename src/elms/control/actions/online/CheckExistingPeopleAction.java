package elms.control.actions.online;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.Operator;
import elms.agent.OnlineAgent;
import elms.control.beans.online.ApplyOnlinePermitForm;
import elms.security.User;
import elms.util.StringUtils;

public class CheckExistingPeopleAction extends Action {
	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(CheckExistingPeopleAction.class.getName());
	String nextPage = "successY";
	ActionErrors errors = null;
	int lsoId = 0;
	String peopleType = null;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		ApplyOnlinePermitForm applyOnlinePermitForm = (ApplyOnlinePermitForm) form;

		logger.debug("entered CheckExistingPeopleAction with  " + applyOnlinePermitForm.getChkExistPeopleFlag());

		peopleType = request.getParameter("peopleType") != null ? request.getParameter("peopleType") : (String) request.getAttribute("peopleType");
		List tempOnlineDetails = new ArrayList();
		int tempOnlineID = StringUtils.s2i((String) request.getParameter("tempOnlineID"));

		String emailAddr = ((User) session.getAttribute(elms.common.Constants.USER_KEY)).getFirstName();

		// String createNewFlag = StringUtils.nullReplaceWithEmpty(request.getParameter("createNewFlag"));
		applyOnlinePermitForm.setTempOnlineID(tempOnlineID);
		OnlineAgent onlinePermitAgent = new OnlineAgent();

		String chkExistPeopleFlag = applyOnlinePermitForm.getChkExistPeopleFlag();
		int peopleTypeId = 0;
		int peopleId = applyOnlinePermitForm.getPeopleId();

		try {
			
			StringBuilder sb = new StringBuilder();
			sb.append(request.getRequestURI());
			if(Operator.hasValue(request.getQueryString())){
				sb.append("?");
				sb.append(request.getQueryString());
			}
			
			String stepUrl = Operator.replace(sb.toString(), request.getContextPath(), "");
			onlinePermitAgent.updateStepUrl(tempOnlineID, stepUrl);
			
			onlinePermitAgent.updatePeopleType(tempOnlineID, peopleType);
			onlinePermitAgent.storeBusTaxReq(tempOnlineID, chkExistPeopleFlag);
			if (chkExistPeopleFlag.equalsIgnoreCase("Y")) {
				request.setAttribute("tempOnlineID", StringUtils.i2s(tempOnlineID));
				request.setAttribute("peopleType", peopleType);
				nextPage = "successY";

			}

			if (chkExistPeopleFlag.equalsIgnoreCase("N")) {
				logger.debug("entered CheckExistingPeopleAction with  " + chkExistPeopleFlag);
				List ownerList = new ArrayList();
				onlinePermitAgent.storePeopleId(tempOnlineID, peopleId, peopleType);
				ownerList = onlinePermitAgent.getOwnerList(tempOnlineID);
				request.setAttribute("ownerList", ownerList);
				request.setAttribute("tempOnlineID", StringUtils.i2s(tempOnlineID));
				applyOnlinePermitForm.setChkExistPeopleFlag("");
				nextPage = "successN";

			}

			if (chkExistPeopleFlag.equalsIgnoreCase("S")) {
				logger.debug("entered CheckExistingPeopleAction with  " + chkExistPeopleFlag);

				onlinePermitAgent.storeSelfPeople(tempOnlineID, emailAddr, peopleType);
				List ownerList = new ArrayList();
				ownerList = onlinePermitAgent.getOwnerList(tempOnlineID);
				request.setAttribute("ownerList", ownerList);

				request.setAttribute("tempOnlineID", StringUtils.i2s(tempOnlineID));
				applyOnlinePermitForm.setChkExistPeopleFlag("");
				nextPage = "successN";

			} else if (chkExistPeopleFlag.equalsIgnoreCase("O")) {
				logger.debug("entered CheckExistingPeopleAction with  " + chkExistPeopleFlag);

				// onlinePermitAgent.storeSelfPeople(tempOnlineID,emailAddr,peopleType);
				List ownerList = new ArrayList();
				ownerList = onlinePermitAgent.getOwnerList(tempOnlineID);
				request.setAttribute("ownerList", ownerList);

				request.setAttribute("tempOnlineID", StringUtils.i2s(tempOnlineID));
				applyOnlinePermitForm.setChkExistPeopleFlag("");
				request.setAttribute("ownerBuilder", "true");
				nextPage = "successN";

			}

			// left side details bar display
			tempOnlineDetails = onlinePermitAgent.getTempOnlineDetails(tempOnlineID);
			request.setAttribute("tempOnlineDetails", tempOnlineDetails);

			// for self to display or not based on login
			peopleTypeId = onlinePermitAgent.getPeopleType(tempOnlineID, emailAddr, "8");// self
			if (peopleTypeId == 0) {
				peopleTypeId = onlinePermitAgent.getPeopleType(tempOnlineID, emailAddr);
			}
			request.setAttribute("peopleTypeId", StringUtils.i2s(peopleTypeId));

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception occured " + e.getMessage());
		}
		return (mapping.findForward(nextPage));
	}
}
