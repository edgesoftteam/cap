package elms.control.actions.online;


import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.poi.ss.formula.functions.Lookup;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.AdminAgent;
import elms.agent.LookupAgent;
import elms.agent.OnlineAgent;
import elms.app.project.Activity;
import elms.app.project.ActivityDetail;
import elms.common.Constants;
import elms.control.beans.EmailDetails;
import elms.security.User;
import elms.util.EmailSender;
import elms.util.StringUtils;


public class CancelPermitAction extends Action {
	static Logger logger = Logger.getLogger(CancelPermitAction.class.getName());
	
	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {		
		logger.debug("CancelPermitAction action class");		
		HttpSession session = request.getSession();  
		User user = (elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY);
		logger.debug("user name::"+user.getUsername()+" :: user email::"+user.getUserEmail());
		String userId = StringUtils.i2s(((elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY)).getUserId());//String userId = (String) session.getAttribute("userId");
		ActivityAgent activityAgent = new ActivityAgent();
		ActivityDetail activity = new ActivityDetail();
		String levelId= (String) request.getParameter("levelId");
		String online = (String) request.getParameter("online");
		logger.debug("levelId: "+levelId);
		String action = request.getParameter("action");
		String message ="";
		try {
			
			if(action != null && action.equalsIgnoreCase("changeActivityStatus")){
				logger.debug("action : "+ action +" :: levelId : "+levelId);
				activityAgent.updateActivityStatus(Constants.ONLINE_GS_PERMIT_FINAL_STATUS,levelId);
				request.setAttribute("actId", levelId);
				return mapping.findForward("permits");
			}
			
		activity =activityAgent.getActivityDetail(StringUtils.s2i(levelId));
		
		Calendar startDate=activity.getStartDate();
		//startDate.add(Calendar.WEEK_OF_YEAR, Constants.GS_PERMIT_CANCEL_PERIOD_IN_WEEKS);
		startDate.add(Calendar.DATE, new LookupAgent().getCancelPeriodInDays());   //Constants.GS_PERMIT_CANCEL_PERIOD_IN_DAYS
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		
		logger.debug("startdate::"+startDate+" :: Today date:"+calendar);
		
		logger.debug("1::"+startDate.compareTo(calendar)+":");
		logger.debug("2::"+calendar.compareTo(startDate));
		
		if(calendar.compareTo(startDate)<0){
			activityAgent.updateActivityStatus(Constants.ONLINE_GS_PERMIT_CANCEL_STATUS,levelId);
			AdminAgent adminAgent = new AdminAgent();
			EmailSender emailSender = new EmailSender();
			//String body = adminAgent.getEmailMessage(Constants.ONLINE_PERMIT_CANCEL_SUCCESS);
			//emailSender.sendEmail(0,user.getUsername(), Constants.EMAIL_TYPE_PERMIT_CANCEL_SUCCESS,new OnlineAgent().getLkupSystemDataMap(), adminAgent.getEmailSubject(Constants.ONLINE_PERMIT_CANCEL_SUCCESS),getEmailBody(body, activity.getActivityType().getDescription(),activity.getAddress(),activity.getActivityNumber()));	
			OnlineAgent onlineAgent = new OnlineAgent();
			EmailDetails emailDetails = new EmailDetails();
			emailDetails.setPermitNo(activity.getActivityNumber());
			emailDetails.setPermitType(activity.getActivityType().getDescription());
			emailDetails.setAddress(activity.getAddress());
			if(LookupAgent.getKeyValue(Constants.PROD_SERVER_FLAG).equalsIgnoreCase("N")) {
				emailDetails.setEmailId(LookupAgent.getKeyValue(Constants.EDGESOFT_SUPPORT_EMAIL));
			}else if(LookupAgent.getKeyValue(Constants.PROD_SERVER_FLAG).equalsIgnoreCase("Y")) {
    			emailDetails.setEmailId(user.getUsername());	
			}
			emailDetails.setLkupSystemDataMap(onlineAgent.getLkupSystemDataMap());
			emailDetails.setEmailTemplateAdminForm(onlineAgent.getEmailData(emailDetails,Constants.ONLINE_PERMIT_CANCEL_SUCCESS));
        	emailSender.sendEmail(emailDetails);
            message = activity.getActivityNumber() + " Permit cancelled successfully";
                   
        }else{
            message = "<strong>" + activity.getActivityNumber() + "</strong> - Please email <font color='blue'><u>cdd-license@burbankca.gov</u></font> to cancel Garage Sale Permit. ";
    	} 
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		request.setAttribute("message", message);
		logger.debug("End of CancelPermitAction action class");	
		return (mapping.findForward("success"));
		}
		private String getEmailBody(String emailBody, String acttype,String address,String actNumber) {
		logger.debug("getEmailBody :: " + emailBody);
		
		if(emailBody!=null) {
			if(emailBody.contains("##ACT_TYPE##")) emailBody = emailBody.replaceAll("##ACT_TYPE##", acttype);
			if(emailBody.contains("##ADDRESS##")) emailBody = emailBody.replaceAll("##ADDRESS##", address);
			if(emailBody.contains("##ACT_NUMBER##")) emailBody = emailBody.replaceAll("##ACT_NUMBER##", actNumber);
		}
		emailBody = emailBody.replace("\\\"", "\"");
		logger.debug("getEmailBody :: " + emailBody);
		return emailBody;
	}
}
