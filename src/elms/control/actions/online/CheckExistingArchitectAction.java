package elms.control.actions.online;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.Operator;
import elms.agent.OnlineAgent;
import elms.control.beans.online.ApplyOnlinePermitForm;
import elms.security.User;
import elms.util.StringUtils;

public class CheckExistingArchitectAction extends Action {
	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(CheckExistingArchitectAction.class.getName());
	String nextPage = "successY";
	ActionErrors errors = null;
	String lsoId = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		ApplyOnlinePermitForm applyOnlinePermitForm = (ApplyOnlinePermitForm) form;

		logger.debug("entered CheckExistingPeopleAction with  " + applyOnlinePermitForm.getChkExistPeopleFlag());

		//
		String peopleType = request.getParameter("peopleType");
		List tempOnlineDetails = new ArrayList();
		int tempOnlineID = StringUtils.s2i((String) request.getParameter("tempOnlineID"));
		request.setAttribute("tempOnlineID", StringUtils.i2s(tempOnlineID));
		String emailAddr = ((User) session.getAttribute(elms.common.Constants.USER_KEY)).getFirstName();

		logger.debug("******************" + emailAddr);

		// String createNewFlag = StringUtils.nullReplaceWithEmpty(request.getParameter("createNewFlag"));
		applyOnlinePermitForm.setTempOnlineID(tempOnlineID);
		OnlineAgent onlinePermitAgent = new OnlineAgent();

		String chkExistPeopleFlag = applyOnlinePermitForm.getChkExistPeopleFlag();
		// ApplicationScope applicationScope=new ApplicationScope();
		String contextHttpsRoot = request.getContextPath();
		int peopleId = applyOnlinePermitForm.getPeopleId();
		request.setAttribute("contextHttpsRoot", contextHttpsRoot);
		// forcePC is set to "Y" check give message if the force PC is yes
		request.setAttribute("forcePC", "Y");

		try {
			onlinePermitAgent.updatePeopleType(tempOnlineID, peopleType);

			StringBuilder sb = new StringBuilder();
			sb.append(request.getRequestURI());
			if(Operator.hasValue(request.getQueryString())){
				sb.append("?");
				sb.append(request.getQueryString());
			}
			
			String stepUrl = Operator.replace(sb.toString(), request.getContextPath(), "");
			onlinePermitAgent.updateStepUrl(tempOnlineID, stepUrl);
			
			// onlinePermitAgent.storeBusTaxReq(tempOnlineID,chkExistPeopleFlag);
			if (chkExistPeopleFlag.equalsIgnoreCase("Y")) {
				request.setAttribute("tempOnlineID", StringUtils.i2s(tempOnlineID));
				request.setAttribute("peopleType", peopleType);
				nextPage = "successY";

			}

			if (chkExistPeopleFlag.equalsIgnoreCase("N")) {

				logger.debug("entered CheckExistingPeopleAction with  " + chkExistPeopleFlag);
				request.setAttribute("tempOnlineID", StringUtils.i2s(tempOnlineID));
				onlinePermitAgent.storePeopleId(tempOnlineID, peopleId, peopleType);
				request.setAttribute("updatePermit", "yes");
				request.setAttribute("forcePC", "Y");

				// added from update new action

				nextPage = "successAuthorize";
			}

			if (chkExistPeopleFlag.equalsIgnoreCase("S")) {
				logger.debug("entered CheckExistingPeopleAction with  " + chkExistPeopleFlag);

				onlinePermitAgent.storeSelfPeople(tempOnlineID, emailAddr, peopleType);

				request.setAttribute("tempOnlineID", StringUtils.i2s(tempOnlineID));
				request.setAttribute("forcePC", "Y");
				request.setAttribute("updatePermit", "yes");

				// added Update Action

				nextPage = "successAuthorize";

			}

			// left side details bar display
			tempOnlineDetails = onlinePermitAgent.getTempOnlineDetails(tempOnlineID);
			request.setAttribute("tempOnlineDetails", tempOnlineDetails);

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception occured " + e.getMessage());
		}
		return (mapping.findForward(nextPage));
	}
}
