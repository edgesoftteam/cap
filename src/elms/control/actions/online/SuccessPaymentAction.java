package elms.control.actions.online;



import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.OnlineAgent;
import elms.common.Constants;
import elms.control.beans.PaymentMgrForm;
import elms.security.User;
import elms.util.StringUtils;


public class SuccessPaymentAction extends Action {
	static Logger logger = Logger.getLogger(SuccessPaymentAction.class.getName());
	protected String nextPage;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {		
		logger.debug("Get SuccessPaymentAction action class");
		try {    
			
			HttpSession session = request.getSession();
			
			PaymentMgrForm paymentMgrForm = (PaymentMgrForm) session.getAttribute("paymentMgrForm");	
			
			String isObc = ((elms.security.User)session.getAttribute(elms.common.Constants.USER_KEY)).getIsObc();
			String isDot = ((elms.security.User)session.getAttribute(elms.common.Constants.USER_KEY)).getIsDot();
            String userId = ((elms.security.User)session.getAttribute(elms.common.Constants.USER_KEY)).getUserId()+"";
			   
			request.setAttribute("userId", userId);    
			   
			if(userId == null || userId.length() == 0) {
				return (mapping.findForward("index"));
			}
			   
			//UserAgent ua = new UserAgent();
			
			User user = new User();
			user =((elms.security.User)session.getAttribute(elms.common.Constants.USER_KEY));
			return (mapping.findForward("successPayment"));			  
						
		} catch(Exception exp) {
			exp.printStackTrace();  
			logger.error("Error in responsePayment action class:"+exp);
			return (mapping.findForward("error"));
		}
		//logger.debug("end of get responsePayment action");
		
	}	
}