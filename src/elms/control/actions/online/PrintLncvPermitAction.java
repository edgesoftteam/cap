package elms.control.actions.online;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.OnlineAgent;

public class PrintLncvPermitAction extends Action {
	static Logger logger = Logger.getLogger(PrintLncvPermitAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		String nextPage="success";
		String permitNo = (String) (request.getParameter("permitNo")!=null? request.getParameter("permitNo"): request.getAttribute("permitNo"));
		logger.debug("Entered permit number... "+permitNo);
		OnlineAgent onlineAgent = new OnlineAgent();
		try {
			int actId = 0;
			actId=onlineAgent.checkLNCVPermitNo(permitNo);
			logger.debug("actId.. "+actId);
			PrintWriter pw = response.getWriter();
			pw.write("result" + "," +actId);
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return (mapping.findForward(nextPage));
	}
}