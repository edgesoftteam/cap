package elms.control.actions.online;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.LookupAgent;
import elms.agent.OnlineAgent;
import elms.common.Constants;
import elms.control.beans.PaymentMgrForm;
import elms.security.User;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

public class ProcessLncvPaymentAction extends Action {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(ProcessLncvPaymentAction.class.getName());    
	
	ActionErrors errors = null;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.debug("Entered Into ProcessLncvPaymentAction ");
		String nextPage = "success";
		HttpSession session = request.getSession();
		PaymentMgrForm paymentMgrForm = (PaymentMgrForm) form;

		String ssl_amount = "";
		String tempOnlineID = "";
		String sprojId = "";
		String actId = "0";

		OnlineAgent onlinePermitAgent = new OnlineAgent();
		String action = (String) (request.getAttribute("action")!=null? request.getAttribute("action"):"");
		try {
			Calendar d = Calendar.getInstance();
			List YY = new ArrayList();  

			
			Wrapper db = new Wrapper();
			String activityNumber = "";

			tempOnlineID = db.getNextId("PARKING_TEMP_ID")+"";

			String contextHttpsRoot = request.getContextPath();
			request.setAttribute("contextHttpsRoot", contextHttpsRoot);

			logger.debug(" tempOnlineID "+tempOnlineID);
			request.setAttribute("tempOnlineID", tempOnlineID);
		
			String comboNo = "";   
			String comboName = "";
		
			ssl_amount = (String) ((request.getParameter("amount")) != null ? (request.getParameter("amount")) : request.getAttribute("amount"));
			action="lncvPayment";
			if(action.equalsIgnoreCase("lncvPayment")){
				ssl_amount = (String) session.getAttribute("lncvamt");
				comboName = Constants.ONLINE_LNCV_PERMIT;
				
				ssl_amount= StringUtils.str2$(ssl_amount);
				tempOnlineID = (String) (session.getAttribute("payfull")!=null? session.getAttribute("payfull"):"N");
				paymentMgrForm.setTempOnlineID(tempOnlineID);
			}
			logger.debug("comboName" + comboName);
			logger.debug("ssl_amount " +ssl_amount);
			
			if(StringUtils.s2i(ssl_amount) == 0 || ssl_amount == null || ssl_amount.equalsIgnoreCase("$0.00") || ssl_amount.equals("")){
				ssl_amount = "0";
				nextPage = "cPayment";
			}
			OnlineAgent opa = new OnlineAgent();
	
			request.setAttribute("comboName", comboName);
			
			request.setAttribute("ssl_amount", ssl_amount);
			request.setAttribute("sprojId", sprojId);
			paymentMgrForm.setAmount(ssl_amount);
			
			paymentMgrForm.setComboName(comboName);
			User usr = (elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY);
			actId= opa.getActIdByLabel(usr.getUsername())+""; 
			paymentMgrForm.setLevelId(actId);   
			
			List lncvDtList = new ArrayList();
			String lncvNextYear = (String) (session.getAttribute("lncvNextYear") != null ? session.getAttribute("lncvNextYear") : "N");
			lncvDtList = (ArrayList) session.getAttribute("lncvDtList");
			logger.debug("lncvDtList :"+lncvDtList+""+lncvDtList.size()+""+lncvDtList.get(0));
			
			String activityId = opa.processLncvPermit(StringUtils.nullReplaceWithZero(actId), (elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY), ((String)session.getAttribute("x_amount")), lncvDtList, paymentMgrForm,StringUtils.nullReplaceWithEmpty(lncvNextYear));   
			session.setAttribute("activityId", activityId);
			comboNo=LookupAgent.getActivityNbrForActId(Integer.parseInt(activityId));
			paymentMgrForm.setLevelId(activityId);
			paymentMgrForm.setComboNo(comboNo);
			session.setAttribute("paymentMgrForm", paymentMgrForm);
			
			logger.debug("Check if it reaches here *******");         
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception occured " + e.getMessage());
		}
		return (mapping.findForward("success"));
	}

}
