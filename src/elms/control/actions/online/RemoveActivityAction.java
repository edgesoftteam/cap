package elms.control.actions.online;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.OnlineAgent;
import elms.control.beans.PaymentMgrForm;
import elms.control.beans.online.CartDetailsForm;

public class RemoveActivityAction extends Action {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(RemoveActivityAction.class.getName());
	
	ActionErrors errors = null;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		PaymentMgrForm paymentMgrForm = (PaymentMgrForm) form;
		logger.debug("Entered Into RemoveActivityAction... "+paymentMgrForm.getEmail());
		logger.debug(paymentMgrForm.getActNum());	
		logger.debug(paymentMgrForm.getLevelId());	
		String action = null;
		if(request.getParameter("action") != null) {
			action = request.getParameter("action");
		}
		logger.debug("action... "+action);
		String email = paymentMgrForm.getEmail();
		logger.debug("email... "+email);
		
		String nextPage="success";
		
		OnlineAgent onlineAgent = new OnlineAgent();
		List actList = new ArrayList<>();
		String displayMsg="";
		String cartId=null;
		CartDetailsForm cartDetailsForm = new CartDetailsForm();
		String totalamnt="0.00";
		List<CartDetailsForm> cartDetailsForms = new ArrayList<CartDetailsForm>();
		logger.debug("email "+request.getParameter("email"));
		logger.debug("level id  "+request.getParameter("levelId"));
		try {
			if(request.getParameter("email") != null && request.getParameter("levelId") != null) {
				cartId = onlineAgent.getCartId(email);
				
				displayMsg = onlineAgent.removeActivity(onlineAgent.getPeopleID(request.getParameter("email")), request.getParameter("levelId"),request.getParameter("email"));
				if(cartId != null) {
					String cartMsg= onlineAgent.removeCartDetails( request.getParameter("levelId"), cartId);
				}
			}
		
			if(request.getParameter("email") != null) {
				cartDetailsForm = onlineAgent.getCartInfo(cartId);
				cartDetailsForm.setCartId(cartId);
			}
			if(cartId != null) {
				cartDetailsForms = onlineAgent.getCartDetails(cartId);
//				cartDetailsForm = onlineAgent.getCartInfo(cartId);
				for(int i=0;i<cartDetailsForms.size();i++) {
					CartDetailsForm cartDetailsForm1 = new CartDetailsForm();
					cartDetailsForm1= cartDetailsForms.get(i);
					cartId = cartDetailsForm1.getCartId();
					totalamnt = cartDetailsForm1.getTotalFeeAmount();
				}
			}

			actList = onlineAgent.getComboActList(email);
			logger.debug(""+actList.size());
			logger.debug("nextPage... "+nextPage);
	
		} catch (Exception e) {
			logger.debug("Exception in removeactivityaction... "+e+e.getMessage());
			try {
				throw new Exception();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		}


		
		request.setAttribute("displayMsg", displayMsg);
		request.setAttribute("paymentMgrForm", paymentMgrForm);
		request.setAttribute("actList", actList);
		request.setAttribute("emailAddr", email);
		request.setAttribute("cartDetailsForm", cartDetailsForm);
		request.setAttribute("cartDetailsForms", cartDetailsForms);
		request.setAttribute("totalamnt", totalamnt);
		request.setAttribute("cartId", cartId);
		
		if(action!= null && action.equalsIgnoreCase("view")) {
			request.setAttribute("viewPermit", "Yes");
			request.setAttribute("isObc", "Y");
			request.setAttribute("isDot", "N");
//			request.removeAttribute("action");
//			request.setAttribute("action", "view");
			request.setAttribute("inActive", "Yes");	
			nextPage="vp_success";
		}	

		logger.debug("action... "+request.getAttribute("action"));
		logger.debug("nextpage... "+nextPage);
        return mapping.findForward(nextPage);
	}
}
