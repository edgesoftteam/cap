package elms.control.actions.online;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.OnlineAgent;
import elms.control.beans.online.OnlineRegisterFrom;

public class OnlineRegisterDetailsAction extends Action {
	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(OnlineRegisterDetailsAction.class.getName());
	String nextPage = "success";
	ActionErrors errors = null;
	int lsoId = 0;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		OnlineRegisterFrom regForm = (OnlineRegisterFrom) form;
		HttpSession session = request.getSession();
		OnlineAgent oPAgent = new OnlineAgent();
		try {
			errors = new ActionErrors();

			if (oPAgent.checkOnlineUser(regForm.getEmailAddress())) {
				nextPage = "exist";
				regForm = new OnlineRegisterFrom();
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.register.userExist"));
				saveErrors(request, errors);
			} else
				nextPage = "success";
			request.setAttribute("regForm", regForm);

		} catch (Exception e) {
			logger.error("Exception occured " + e.getMessage());
		}
		return (mapping.findForward(nextPage));
	}
}
