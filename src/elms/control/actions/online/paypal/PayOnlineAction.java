package elms.control.actions.online.paypal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.paypal.soap.api.AckCodeType;

import elms.agent.ActivityAgent;
import elms.agent.AdminAgent;
import elms.agent.OnlineAgent;
import elms.common.Constants;
import elms.control.actions.online.paypal.util.Configuration;
import elms.control.beans.PayPalResult;
import elms.control.beans.PaymentMgrForm;
import elms.util.Email;
import elms.util.MessageUtils;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

public class PayOnlineAction extends Action {

	static Logger logger = Logger.getLogger(PayOnlineAction.class.getName());
	String nextPage = "failure";
	ActionErrors errors = null;
	public static ResourceBundle elmsProperties;
	
	static{
		try {
			elmsProperties = Wrapper.getResourceBundle();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Configuration.userName = elmsProperties.getString("API_USER_NAME").trim();
		Configuration.password = elmsProperties.getString("API_PASSWORD").trim();
		Configuration.signature = elmsProperties.getString("API_SIGNATURE").trim();
		Configuration.mode = elmsProperties.getString("PAYPAL_ENVIRONMENT").trim();
	}
	
	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.info("Entered into PayOnlineAction ...");
		try {
			HttpSession session = request.getSession();
			PaymentMgrForm paymentMgrForm = (PaymentMgrForm) form;

			String amount = paymentMgrForm.getSsl_amount();
			String creditCard = paymentMgrForm.getSsl_card_number();
			String expYr = paymentMgrForm.getSsl_exp_date_year();
			String expMon = paymentMgrForm.getSsl_exp_date_month();
			String street = paymentMgrForm.getSsl_avs_address();
			String city = paymentMgrForm.getSsl_city();
			String country = paymentMgrForm.getSsl_country();
			String zip = paymentMgrForm.getSsl_avs_zip();
			String state = paymentMgrForm.getSsl_state();
			String ownerFName = paymentMgrForm.getSsl_ownerFName();
			String ownerLName = paymentMgrForm.getSsl_ownerLName();
			String cvv = paymentMgrForm.getSsl_cvv2cvc2();

			// paypal online payment
			//PayPalSOAP app = new PayPalSOAP(Constants.API_USER_NAME, Constants.API_PASSWORD, Constants.API_SIGNATURE, Constants.ENVIRONMENT, Constants.OPERATION);
			
			PayPalSOAP app = new PayPalSOAP();
			//PayPalResult result = app.run(amount, creditCard, expYr, expMon, street, city, state, country, zip, ownerFName, ownerLName);
			//app.doDirectPayment("Akshay", "RP", "MainStreet", "Culver City", "CA", "US", "95131", "4024007185826731", "11", "2018", "111", "4.6");
			PayPalResult result = app.doDirectPayment(ownerFName, ownerLName, street, city, state, country, zip, creditCard, expMon, expYr, "", amount);
			//PayPalResult result = app.doDirectPayment(ownerFName, ownerLName, street, city, state, country, zip, creditCard, expMon, expYr, "", amount);

			PaymentMgrForm sessionForm = (PaymentMgrForm) session.getAttribute("paymentMgrForm");
			
			if ((result.getAck().equals(AckCodeType._Failure.toString())) || (result.getAck()).equals(AckCodeType._FailureWithWarning.toString())) {
				System.out.println("Transaction Error in PayOnlineAction: " + result.getErrCode() + " " + result.getErrShortMsg() + " - " + result.getErrLongMsg());

				if (!result.getErrShortMsg().equals("Invalid Data")) {
					// storing result into tables.
					OnlineAgent.saveOnlinePayment(sessionForm.getLevelId(), sessionForm.getComboName(), "1", result.getAmount(), ((elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY)).getUserId(), result.getAck(), result.getErrCode() + " " + result.getErrLongMsg(), request.getRemoteAddr(), "");
					nextPage = "decline";
				} else {
					nextPage = "failure";
				}

				request.setAttribute("ssl_result", result.getErrCode());
				request.setAttribute("ssl_result_message", result.getErrLongMsg());
				request.setAttribute("ssl_result_shortMsg", result.getErrShortMsg());
				
				request.setAttribute("sprojId", sessionForm.getLevelId());
				request.setAttribute("comboNo", sessionForm.getComboNo());
				request.setAttribute("ssl_amount", sessionForm.getAmount());
				request.setAttribute("comboName", sessionForm.getComboName());
			} else if ((result.getAck().equals(AckCodeType._Success.toString())) || (result.getAck().equals(AckCodeType._SuccessWithWarning.toString()))) {
				System.out.println();
				System.out.println("---------- Results in PayOnlineAction----------");
				System.out.println("Transaction ID: " + result.getTranId());
				System.out.println("Transaction Amount: " + result.getAmount());
				System.out.println("Payment Status: " + result.getPaymentStatus());
				System.out.println("Pending Reason: " + result.getPendingReason());
				System.out.println("CVV2: " + result.getCvv2code());
				System.out.println("AVS: " + result.getAvscode());

				// storing result into tables.
				OnlineAgent.saveOnlinePayment(sessionForm.getLevelId(), sessionForm.getComboName(), result.getTranId(), result.getAmount(), ((elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY)).getUserId(), result.getAck(), result.getAck(), request.getRemoteAddr(), "");

				OnlineAgent opa = new OnlineAgent();
				//Important 
				sessionForm.setOnlineTxnId(result.getTranId());
				// lncv
				if (sessionForm.getComboName().equals(Constants.ONLINE_LNCV_PERMIT)) {
					List lncvDtList = new ArrayList();
					String actId = (String) (session.getAttribute("activityId") != null ? session.getAttribute("activityId") : "0");
					String lncvNextYear = (String) (session.getAttribute("lncvNextYear") != null ? session.getAttribute("lncvNextYear") : "N");
					lncvDtList = (ArrayList) session.getAttribute("lncvDtList");
					logger.debug("lncvDtList :"+lncvDtList+""+lncvDtList.size()+""+lncvDtList.get(0));
					String activityId = opa.processLncvPermit(actId, (elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY), result.getAmount(), lncvDtList, sessionForm,lncvNextYear);
					session.setAttribute("activityId", activityId);

				} else {
					// normal permits
					opa.processInternalPayment(sessionForm, sessionForm.getLevelId(), (elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY));
					opa.checkComboMappingApproval(sessionForm.getLevelId(), sessionForm);
				}
				// String subject = "Your Permit been processed";
				// new MessageUtils().sendEmail(((elms.security.User)session.getAttribute(elms.common.Constants.USER_KEY)).getUsername()+";",subject,(elms.agent.online.OnlinePermitAgent.composeEmailMessage(((elms.security.User)session.getAttribute(elms.common.Constants.USER_KEY)).getUsername(),sessionForm.getComboNo(),sessionForm.getAmount())));
				String subject = "PAYMENT_SUCCESSFUL";
				Email email = new Email();
				email.setFromAddress("EMAIL_FROM");
				email.setToAddress("akshay.rp@edgesoft.in");
				email.setSubject(subject);
				String msg = "Hi, \n" + "Payment Successful";
				email.setMessage(msg);

		//		MessageUtils.sendEmail(email);
				HashMap hm = new HashMap();
				String permitType=sessionForm.getComboName();
				
				int actId = Integer.parseInt((String) (session.getAttribute("activityId")!=null? session.getAttribute("activityId"): sessionForm.getLevelId()));
				String actNo = new ActivityAgent().getActivtiyNumber(actId);
				logger.debug("****&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&****"+actNo);
				hm.put("PERMITNO", actNo);
				hm.put("AMOUNT", sessionForm.getAmount());
				hm.put("TRANSID", result.getTranId());
				hm.put("PERMITTYPE", permitType);
				/*try {
					String message = StringUtils.parseString(new AdminAgent().getEmailMessage("PAYMENT_SUCCESSFULL"), hm);
					new MessageUtils().sendEmail(((elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY)).getUsername() + ";", subject, message);
				} catch (Exception e) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.register.email"));
					saveErrors(request, errors);
					logger.error("PayOnline action " + e.getMessage());
				}*/

				request.setAttribute("sprojId", sessionForm.getLevelId());
				request.setAttribute("comboNo", sessionForm.getComboNo());
				request.setAttribute("comboName", sessionForm.getComboName());
				request.setAttribute("ssl_txn_id", result.getTranId());
				request.setAttribute("ssl_result", result.getAck());
				request.setAttribute("ssl_result_message", result.getAck());
				request.setAttribute("ssl_result_shortMsg", result.getAck());
				request.setAttribute("ssl_amount", sessionForm.getAmount());
				nextPage = "success";
			}

		} catch (Exception e) {
			logger.debug("Error in PayOnlineAction ..." + e.getMessage());
			/*saveErrors(request, errors);

			return (new ActionForward("/jsp/error.jsp"));*/
		}
		return (mapping.findForward(nextPage));
	}
}
