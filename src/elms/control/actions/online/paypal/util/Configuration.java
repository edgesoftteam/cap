package elms.control.actions.online.paypal.util;

import java.util.HashMap;
import java.util.Map;

/**
 *  For a full list of configuration parameters refer in wiki page.(https://github.com/paypal/sdk-core-java/blob/master/README.md) 
 */
public class Configuration {
	
	public static String userName = "";
	public static String password = "";
	public static String signature = "";
	public static String mode = "";
	
	// Creates a configuration map containing credentials and other required configuration parameters.
	public static final Map<String,String> getAcctAndConfig(){
		Map<String,String> configMap = new HashMap<String,String>();
		configMap.putAll(setConfig(mode));
				
		// Account Credential
		configMap.put("acct1.UserName", userName); //"akshay_api1.edgesoft.in");
		configMap.put("acct1.Password", password); //"PUEEQQA3F968B57Q");
		configMap.put("acct1.Signature", signature); //"AFcWxV21C7fd0v3bYYYRCpSSRl31AjHAo7wl0D0X0FYqggI6fLQBVv-f");
		
		/*configMap.put("acct1.UserName", "akshay.rp-facilitator_api1.oroprise.com");
		configMap.put("acct1.Password", "R8ZRF6X8ZF9K5YQ7");
		configMap.put("acct1.Signature", "AFcWxV21C7fd0v3bYYYRCpSSRl31AY4cc6oWEEv7dvu89dB9taWLQv43");*/
		
		return configMap;
	}
	
	public static final Map<String,String> setConfig(String mode){
		Map<String,String> configMap = new HashMap<String,String>();
		
		// Endpoints are varied depending on whether sandbox OR live is chosen for mode
		configMap.put("mode", mode);
		

		// These values are defaulted in SDK. If you want to override default values, uncomment it and add your value.
		// configMap.put("http.ConnectionTimeOut", "5000");
		// configMap.put("http.Retry", "2");
		// configMap.put("http.ReadTimeOut", "30000");
		// configMap.put("http.MaxConnection", "100");
		return configMap;
	}
}
