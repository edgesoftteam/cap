package elms.control.actions.online.paypal;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.paypal.exception.ClientActionRequiredException;
import com.paypal.exception.HttpErrorException;
import com.paypal.exception.InvalidCredentialException;
import com.paypal.exception.InvalidResponseDataException;
import com.paypal.exception.MissingCredentialException;
import com.paypal.exception.SSLConfigurationException;
import com.paypal.sdk.exceptions.OAuthException;
import com.paypal.soap.api.AckCodeType;

import elms.control.actions.online.paypal.util.Configuration;
import elms.control.beans.PayPalResult;
import urn.ebay.api.PayPalAPI.DoDirectPaymentReq;
import urn.ebay.api.PayPalAPI.DoDirectPaymentRequestType;
import urn.ebay.api.PayPalAPI.DoDirectPaymentResponseType;
import urn.ebay.api.PayPalAPI.PayPalAPIInterfaceServiceService;
import urn.ebay.apis.CoreComponentTypes.BasicAmountType;
import urn.ebay.apis.eBLBaseComponents.AddressType;
import urn.ebay.apis.eBLBaseComponents.CountryCodeType;
import urn.ebay.apis.eBLBaseComponents.CreditCardDetailsType;
import urn.ebay.apis.eBLBaseComponents.CreditCardTypeType;
import urn.ebay.apis.eBLBaseComponents.CurrencyCodeType;
import urn.ebay.apis.eBLBaseComponents.DoDirectPaymentRequestDetailsType;
import urn.ebay.apis.eBLBaseComponents.PayerInfoType;
import urn.ebay.apis.eBLBaseComponents.PaymentActionCodeType;
import urn.ebay.apis.eBLBaseComponents.PaymentDetailsType;
import urn.ebay.apis.eBLBaseComponents.PersonNameType;

public class PayPalSOAP {

	// Credit card details
	// Use buyer account card number registered in paypal sandbox. All other
	// details are not verified.
	private static String CARD_NO = "";
	private static CreditCardTypeType CARD_TYPE = CreditCardTypeType.VISA;
	private static Integer EXP_MONTH = new Integer(0);
	private static Integer EXP_YEAR = new Integer(0);
	private static String CVV = "";
	// Credit card address
	private static String STREET1 = "";
	private static String CITY = "";
	private static String STATE = "";
	private static String COUNTRY = "";
	private static String ZIP = "";
	// Creidt card owner
	private static String OWNER_FNAME = "";
	private static String OWNER_LNAME = "";
	// Payment details
	private static CurrencyCodeType CURRENCY = CurrencyCodeType.USD;
	private static String AMOUNT = "";
	private static PaymentActionCodeType PAYMENT_TYPE = PaymentActionCodeType.SALE;

	public PayPalResult doDirectPayment(String fName, String lName, String street, String city, String state, String country, String zip,
			String card, String month, String year, String cvv, String amt) {

		CARD_NO = card;
		EXP_MONTH = new Integer(month);
		EXP_YEAR = new Integer(year);
		CVV = cvv;
		// Credit card address
		STREET1 = street;
		CITY = city;
		STATE = state;
		COUNTRY = country;
		ZIP = zip;
		// Creidt card owner
		OWNER_FNAME = fName;
		OWNER_LNAME = lName;
		// Payment details
		AMOUNT = amt;

		// ## DoDirectPaymentReq
		DoDirectPaymentReq doPaymentReq = new DoDirectPaymentReq();

		DoDirectPaymentRequestType pprequest = new DoDirectPaymentRequestType();
		DoDirectPaymentRequestDetailsType details = new DoDirectPaymentRequestDetailsType();
		PaymentDetailsType paymentDetails = new PaymentDetailsType();
		DoDirectPaymentResponseType ddresponse = new DoDirectPaymentResponseType();
		PayPalResult result = new PayPalResult();

		// Total cost of the transaction to the buyer. If shipping cost and tax
		// charges are known, include them in this value. If not, this value
		// should be the current sub-total of the order.
		//
		// If the transaction includes one or more one-time purchases, this
		// field must be equal to
		// the sum of the purchases. Set this field to 0 if the transaction does
		// not include a one-time purchase such as when you set up a billing
		// agreement for a recurring payment that is not immediately charged.
		// When the field is set to 0, purchase-specific fields are ignored.
		//
		// * `Currency Code` - You must set the currencyID attribute to one of
		// the
		// 3-character currency codes for any of the supported PayPal
		// currencies.
		// * `Amount`
		BasicAmountType amount = new BasicAmountType();
		amount.setValue(AMOUNT);
		amount.setCurrencyID(CURRENCY);
		paymentDetails.setOrderTotal(amount);

		AddressType shipTo = new AddressType();
		shipTo.setName(OWNER_FNAME + " " + OWNER_LNAME);
		/*
		 * (Required) First street address. Character length and limitations:
		 * 100 single-byte characters
		 */
		shipTo.setStreet1(STREET1);
		/*
		 * (Optional) Second street address. Character length and limitations:
		 * 100 single-byte characters
		 */
		shipTo.setStreet2("");
		/*
		 * (Required) Name of city. Character length and limitations: 40
		 * single-byte characters
		 */
		shipTo.setCityName(CITY);
		/*
		 * (Required) State or province. Character length and limitations: 40
		 * single-byte characters
		 */
		shipTo.setStateOrProvince(STATE);
		/*
		 * (Required) Country code. Character length and limitations: 2
		 * single-byte characters
		 */
		shipTo.setCountry(CountryCodeType.fromValue(COUNTRY));
		/*
		 * (Required) U.S. ZIP code or other country-specific postal code.
		 * Character length and limitations: 20 single-byte characters
		 */
		shipTo.setPostalCode(ZIP);
		paymentDetails.setShipToAddress(shipTo);
		/*
		 * (Optional) Your URL for receiving Instant Payment Notification (IPN)
		 * about this transaction. If you do not specify this value in the
		 * request, the notification URL from your Merchant Profile is used, if
		 * one exists. Important: The notify URL applies only to
		 * DoExpressCheckoutPayment. This value is ignored when set in
		 * SetExpressCheckout or GetExpressCheckoutDetails.
		 */
		paymentDetails.setNotifyURL("");
		details.setPaymentDetails(paymentDetails);

		CreditCardDetailsType cardDetails = new CreditCardDetailsType();
		// Type of credit card. For UK, only Maestro, MasterCard, Discover, and
		// Visa are allowable. For Canada, only MasterCard and Visa are
		// allowable and Interac debit cards are not supported. It is one of the
		// following values:
		//
		// * Visa
		// * MasterCard
		// * Discover
		// * Amex
		// * Solo
		// * Switch
		// * Maestro: See note.
		// `Note:
		// If the credit card type is Maestro, you must set currencyId to GBP.
		// In addition, you must specify either StartMonth and StartYear or
		// IssueNumber.`
		cardDetails.setCreditCardType(CARD_TYPE);
		// Credit Card number
		cardDetails.setCreditCardNumber(CARD_NO);
		// ExpiryMonth of credit card
		cardDetails.setExpMonth(EXP_MONTH);
		// Expiry Year of credit card
		cardDetails.setExpYear(EXP_YEAR);
		// cvv2 number
		cardDetails.setCVV2(CVV);

		PayerInfoType payer = new PayerInfoType();
		PersonNameType name = new PersonNameType();
		/*
		 * (Required) Buyer's first name. Character length and limitations: 25
		 * single-byte characters
		 */
		name.setFirstName(OWNER_FNAME);
		/*
		 * (Required) Buyer's last name. Character length and limitations: 25
		 * single-byte characters
		 */
		name.setLastName(OWNER_LNAME);
		payer.setPayerName(name);
		/*
		 * (Required) Country code. Character length and limitations: 2
		 * single-byte characters
		 */
		payer.setPayerCountry(CountryCodeType.fromValue(COUNTRY));
		payer.setAddress(shipTo);

		cardDetails.setCardOwner(payer);

		details.setCreditCard(cardDetails);

		details.setIPAddress("127.0.0.1");
		/*
		 * (Optional) How you want to obtain payment. It is one of the following
		 * values: Authorization – This payment is a basic authorization subject
		 * to settlement with PayPal Authorization and Capture. Sale – This is a
		 * final sale for which you are requesting payment (default). Note:
		 * Order is not allowed for Direct Payment. Character length and limit:
		 * Up to 13 single-byte alphabetic characters
		 */
		details.setPaymentAction(PAYMENT_TYPE);

		pprequest.setDoDirectPaymentRequestDetails(details);

		doPaymentReq.setDoDirectPaymentRequest(pprequest);

		try {
			// Configuration map containing signature credentials and other
			// required configuration.
			// For a full list of configuration parameters refer in wiki page.
			// (https://github.com/paypal/sdk-core-java/blob/master/README.md)
						
			Map<String, String> configurationMap = Configuration.getAcctAndConfig();

			// Creating service wrapper object to make an API call by loading
			// configuration map.
			PayPalAPIInterfaceServiceService service = new PayPalAPIInterfaceServiceService(configurationMap);

			ddresponse = service.doDirectPayment(doPaymentReq);

			if (ddresponse != null) {
				if ((ddresponse.getAck().getValue().equals(AckCodeType._Success.toString())) || (ddresponse.getAck().getValue().equals(AckCodeType._SuccessWithWarning.toString()))) {
					result.setAck(ddresponse.getAck().getValue());
					
					/*
					 * Unique transaction ID of the payment. Note: If the
					 * PaymentAction of the request was Authorization, the value
					 * of TransactionID is your AuthorizationID for use with the
					 * Authorization and Capture APIs. Character length and
					 * limitations: 19 single-byte characters
					 */
					result.setTranId(ddresponse.getTransactionID());
					/*
					 * This value is the amount of the payment as specified by
					 * you on DoDirectPaymentRequest for reference transactions
					 * with direct payments.
					 */
					result.setAmount(ddresponse.getAmount().getValue());
					result.setCurrencyId(ddresponse.getAmount().getCurrencyID().getValue());
					result.setPaymentStatus(ddresponse.getPaymentStatus()==null?ddresponse.getAck().getValue():ddresponse.getPaymentStatus().toString());
				} else if ((ddresponse.getAck().getValue().equals(AckCodeType._Failure.toString())) || (ddresponse.getAck().getValue().equals(AckCodeType._FailureWithWarning.toString()))) {	
					result.setAck(ddresponse.getAck().getValue());
					result.setErrShortMsg(ddresponse.getErrors().toString());
				}
			}
		} catch (FileNotFoundException e) {
			result.setAck(AckCodeType._Failure.toString());
			result.setErrCode("500");
			result.setErrShortMsg("Internal Server Error : ");
			result.setErrLongMsg(e.getMessage());
		} catch (SAXException e) {
			result.setAck(AckCodeType._Failure.toString());
			result.setErrCode("500");
			result.setErrShortMsg("Internal Server Error : ");
			result.setErrLongMsg(e.getMessage());
		} catch (ParserConfigurationException e) {
			result.setAck(AckCodeType._Failure.toString());
			result.setErrCode("500");
			result.setErrShortMsg("Internal Server Error : ");
			result.setErrLongMsg(e.getMessage());
		} catch (SSLConfigurationException e) {
			result.setAck(AckCodeType._Failure.toString());
			result.setErrCode("500");
			result.setErrShortMsg("Internal Server Error : ");
			result.setErrLongMsg(e.getMessage());
		} catch (InvalidCredentialException e) {
			result.setAck(AckCodeType._Failure.toString());
			result.setErrCode("500");
			result.setErrShortMsg("Internal Server Error : ");
			result.setErrLongMsg(e.getMessage());
		} catch (UnsupportedEncodingException e) {
			result.setAck(AckCodeType._Failure.toString());
			result.setErrCode("500");
			result.setErrShortMsg("Internal Server Error : ");
			result.setErrLongMsg(e.getMessage());
		} catch (HttpErrorException e) {
			result.setAck(AckCodeType._Failure.toString());
			result.setErrCode("500");
			result.setErrShortMsg("Internal Server Error : ");
			result.setErrLongMsg(e.getMessage());
		} catch (InvalidResponseDataException e) {
			result.setAck(AckCodeType._Failure.toString());
			result.setErrCode("500");
			result.setErrShortMsg("Internal Server Error : ");
			result.setErrLongMsg(e.getMessage());
		} catch (ClientActionRequiredException e) {
			result.setAck(AckCodeType._Failure.toString());
			result.setErrCode("500");
			result.setErrShortMsg("Internal Server Error : ");
			result.setErrLongMsg(e.getMessage());
		} catch (MissingCredentialException e) {
			result.setAck(AckCodeType._Failure.toString());
			result.setErrCode("500");
			result.setErrShortMsg("Internal Server Error : ");
			result.setErrLongMsg(e.getMessage());
		} catch (OAuthException e) {
			result.setAck(AckCodeType._Failure.toString());
			result.setErrCode("500");
			result.setErrShortMsg("Internal Server Error : ");
			result.setErrLongMsg(e.getMessage());
		} catch (IOException e) {
			result.setAck(AckCodeType._Failure.toString());
			result.setErrCode("500");
			result.setErrShortMsg("Internal Server Error : ");
			result.setErrLongMsg(e.getMessage());
		} catch (InterruptedException e) {
			result.setAck(AckCodeType._Failure.toString());
			result.setErrCode("500");
			result.setErrShortMsg("Internal Server Error : ");
			result.setErrLongMsg(e.getMessage());
		} catch (Exception e) {
			result.setAck(AckCodeType._Failure.toString());
			result.setErrCode("500");
			result.setErrShortMsg("Internal Server Error : ");
			result.setErrLongMsg(e.getMessage());
		}
		return result;
	}
}
