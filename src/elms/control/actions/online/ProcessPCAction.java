package elms.control.actions.online;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.OnlineAgent;
import elms.common.Constants;
import elms.control.beans.PaymentMgrForm;
import elms.util.StringUtils;

public class ProcessPCAction extends Action {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(ProcessPCAction.class.getName());
	String nextPage = "success";
	ActionErrors errors = null;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.debug("Entered Into ProcessPCAction ");

		HttpSession session = request.getSession();
		PaymentMgrForm paymentMgrForm = (PaymentMgrForm) form;

		String tempOnlineID = "";
		String sprojId = "";
		String actId = "";

		OnlineAgent onlinePermitAgent = new OnlineAgent();

		try {
			Calendar d = Calendar.getInstance();
			List YY = new ArrayList();

			int yyCurrent = d.get(Calendar.YEAR);
			for (int i = 0; i < Constants.ONLINE_CREDIT_EXP_YEAR_SIZE; i++) {
				YY.add(new Integer(yyCurrent));
				yyCurrent = yyCurrent + 1;

			}
			request.setAttribute("YY", YY);

			String contextHttpsRoot = request.getContextPath();
			request.setAttribute("contextHttpsRoot", contextHttpsRoot);

			tempOnlineID = paymentMgrForm.getTempOnlineID();
			request.setAttribute("tempOnlineID", tempOnlineID);
			// actId = request.getParameter("levelId");
			actId = (String) ((request.getParameter("levelId")) != null ? (request.getParameter("levelId")) : request.getAttribute("levelId"));

			String comboNo = "";
			comboNo = new ActivityAgent().getActivtiyNumber(StringUtils.s2i(actId));
			request.setAttribute("comboNo", comboNo);

			List subTyp = new ActivityAgent().getSubTypes(StringUtils.s2i(actId));
			String comboName = Constants.ONLINE_RESIDENTIAL_PERMIT;
			if (subTyp.size() > 0) {
				for (int i = 0; i < subTyp.size(); i++)
					comboName = comboName + " - " + subTyp.get(i);

			}
			logger.debug("comboName" + comboName);
			request.setAttribute("comboName", comboName);

			request.setAttribute("sprojId", sprojId);

			paymentMgrForm.setComboNo(comboNo);
			paymentMgrForm.setComboName(comboName);
			paymentMgrForm.setLevelId(actId);
			session.setAttribute("paymentMgrForm", paymentMgrForm);

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception occured " + e.getMessage());
		}
		return (mapping.findForward(nextPage));
	}

}
