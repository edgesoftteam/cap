package elms.control.actions.online;


import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.AdminAgent;
import elms.agent.LookupAgent;
import elms.agent.OnlineAgent;
import elms.common.Constants;
import elms.control.beans.EmailDetails;
import elms.control.beans.online.OnlinePermitForm;
import elms.control.beans.online.TempPermitForm;
import elms.security.User;
import elms.util.EmailSender;
import elms.util.StringUtils;
import elms.util.db.Wrapper;


public class PermitPreviewAction extends Action {
	static Logger logger = Logger.getLogger(PermitPreviewAction.class.getName());
	
	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {		
		logger.debug("PermitPreviewAction action class");		
		HttpSession session = request.getSession(); 
		User user=(elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY);
		//String userId = StringUtils.i2s(((elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY)).getUserId());//String userId = (String) session.getAttribute("userId");
		String userId = StringUtils.i2s(user.getUserId());
		
		OnlinePermitForm addForm = (OnlinePermitForm) form;
		OnlineAgent onlineAgent = new OnlineAgent();
		String tempId = (String) session.getAttribute("tempId");
		TempPermitForm tempPermitForm = (TempPermitForm) session.getAttribute("tempPermitForm");
		
		String levelId=null;
		
		//ActivityAgent activityAgent = new ActivityAgent();
		try {
			if(tempId !=null){
			tempPermitForm= onlineAgent.getTempPermit(tempId);
			logger.debug("tempPermitForm.getLsoId()... "+tempPermitForm.getLsoId());
			/*OnlineRegisterFrom regForm = onlineAgent.getOnlineUsers(user.getUsername(), user.getIsObc());
			tempPermitForm.setFirstName(regForm.getFirstName());
			tempPermitForm.setLastName(regForm.getLastName());
			tempPermitForm.setAddress(regForm.getAddress());
			tempPermitForm.setCity(regForm.getCity());
			tempPermitForm.setState(regForm.getState());
			tempPermitForm.setZip(regForm.getZip());
			tempPermitForm.setPhoneNbr(regForm.getPhoneNbr());
			tempPermitForm.setPhoneExt(regForm.getPhoneExt());
			tempPermitForm.setWorkPhone(regForm.getWorkPhone());
			tempPermitForm.setWorkExt(regForm.getWorkExt());
			tempPermitForm.setEmailAddress(regForm.getEmailAddress());*/
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if(tempPermitForm != null){
			try {
				BeanUtils.copyProperties(addForm, tempPermitForm);
				logger.debug("addforem lsoid.. "+addForm.getLsoId());
				levelId = onlineAgent.getTempLevelId(tempId);
				session.removeAttribute("tempPermitForm");
				//session.setAttribute("peopleListAll", tempPermitForm.getList());
			} catch (IllegalAccessException e) {
				logger.error("Error copying properties" + e);
			} catch (InvocationTargetException e) {
				logger.error("Error copying properties" + e);
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		} 
		
		String act= addForm.getActivityType();
		logger.debug("actType:"+act);
		LookupAgent la=new LookupAgent();
		String actDesc = null;
		
		try {
			actDesc = la.getactivitydec(act);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		/*if(addForm.getStreetNum().equalsIgnoreCase("") || addForm.getStreetNum()== null) {
			addForm.setStreetNum("0");
		}
		
		if(addForm.getStreetName().equalsIgnoreCase("") || addForm.getStreetName()== null) {
			addForm.setStreetName("Non Locational");
		}*/
		logger.debug(addForm.getStreetNum());
	 
		
		/*String flag = (la.searchAddress(addForm.getStreetNum(), addForm.getStreetName(),addForm.getStreetUnit())==null) ?"":la.searchAddress(addForm.getStreetNum(), addForm.getStreetName(),addForm.getStreetUnit());
		if(flag.equalsIgnoreCase("") && flag.length() <= 0) {   
			addForm.setStreetNum("0");
			addForm.setStreetName("Non Locational");
		}*/
		addForm.setActivityTypeDesc(actDesc);
		if(addForm.getStreetUnit()!=null && !addForm.getStreetUnit().equalsIgnoreCase("")&& addForm.getStreetUnit().startsWith("unit")){
			addForm.setStreetUnit("");
		}
		request.setAttribute("onlinePermitFrom", addForm);
		logger.debug("selected ActDescription..:"+addForm.getActDescription());
		logger.debug("selected activity type..:"+addForm.getActivityTypeDesc());
		
		logger.debug("selected Permit description ..:"+addForm.getActDescription());
		
		String Act_type="";
		if(userId == null || userId.length() == 0) {
			return (mapping.findForward("index"));
		}
		try {
			
			String action = request.getParameter("action");
			logger.debug("action:-" +action);
			if(action != null && action.equals("save")) {
				logger.debug("save permit");
				ActivityAgent aa = new ActivityAgent();
				
				addForm.setUserId(userId);
				logger.debug("selected userId :"+addForm.getUserId());
				logger.debug(addForm.getLsoId());
				
				 levelId = (String)session.getAttribute("levelId");
				 addForm.setActId(levelId);
				if(addForm.getActId()==null){
					Wrapper db = new Wrapper();
					levelId = db.getNextId("ACTIVITY_ID")+"";
				}
				addForm.setActId(levelId);
				logger.debug("actId:"+addForm.getActId());
				String EmailAddress=  (String) request.getServletContext().getAttribute("EMAIL_ADDRESS");
				
				aa.insertGarageSalePermit(addForm,user);
				AdminAgent adminAgent = new AdminAgent();
				EmailSender emailSender = new EmailSender();
				//String body = adminAgent.getEmailMessage(Constants.ONLINE_SUBMIT_SUCCESS);
				//emailSender.sendEmail(0,addForm.getEmailAddress(), Constants.EMAIL_TYPE_PERMIT_SUBMIT_SUCCESS,onlineAgent.getLkupSystemDataMap(), adminAgent.getEmailSubject(Constants.ONLINE_SUBMIT_SUCCESS),getEmailBody(body, addForm.getActivityTypeDesc(),addForm.getAddress()));	
				EmailDetails emailDetails = new EmailDetails();
				emailDetails.setPermitNo(ActivityAgent.getActivtiyNumber(StringUtils.s2i(addForm.getActId())));
				emailDetails.setPermitType(addForm.getActivityTypeDesc());
				emailDetails.setAddress(StringUtils.nullReplaceWithEmpty(addForm.getStreetNum())+" "+StringUtils.nullReplaceWithEmpty(addForm.getStreetName())+" "+StringUtils.nullReplaceWithEmpty(addForm.getStreetUnit()));
				if(LookupAgent.getKeyValue(Constants.PROD_SERVER_FLAG).equalsIgnoreCase("N")) {
					emailDetails.setEmailId(LookupAgent.getKeyValue(Constants.EDGESOFT_SUPPORT_EMAIL));
				}else if(LookupAgent.getKeyValue(Constants.PROD_SERVER_FLAG).equalsIgnoreCase("Y")) {
	    			emailDetails.setEmailId(addForm.getEmailAddress());	
				}
				emailDetails.setLkupSystemDataMap(onlineAgent.getLkupSystemDataMap());
				emailDetails.setEmailTemplateAdminForm(onlineAgent.getEmailData(emailDetails,Constants.ONLINE_SUBMIT_SUCCESS));
	        	emailSender.sendEmail(emailDetails);
				String statusMsg="Email sent successfully.";
				onlineAgent.deleteTempPermit(tempId);
				if(session != null){
					session.removeAttribute("getAttachmentsList");
				}				
				request.setAttribute("success", "Permit Submitted Successfully");
				request.setAttribute("levelId", levelId);
				
				return (mapping.findForward("success"));
			} else if(action != null && action.equals("reset")) {
				if(tempId ==null)
					tempId = (String) session.getAttribute("tempId");
					onlineAgent.deleteTempPermit(tempId);
				addForm.reset1();
				session.removeAttribute("levelId");
				session.removeAttribute("getAttachments");
				request.setAttribute("error", "Permit has been cancelled successfully");				
				return mapping.findForward("pprs");
				
			}else if(action != null && action.equals("cpp")) {
				String description="abc";
				request.setAttribute("description", description);
				return mapping.findForward("cpp");
			}else if(action != null && action.equals("oai")) {
				return mapping.findForward("oai");
			}
			else 
			{
			}
			
		} catch(Exception exp) {
			String error = "Error while adding permit:"+exp.getMessage();
			request.setAttribute("error", error);
			logger.error("error in PermitPreviewAction page...:"+exp);			
		}
		logger.debug("end of PermitPreviewAction action class");
		request.setAttribute("addForm", addForm);
		
		onlineAgent = new OnlineAgent();
		
		if(tempId == null)
			tempId = (String) session.getAttribute("tempId");
		
		String resumeUrl = request.getServletPath();
		String param = request.getQueryString();
		if(param != null)
			resumeUrl = resumeUrl + "?" + param;
		
		try {
			onlineAgent.updateCAPTempWithResumeUrl(tempId, resumeUrl, userId);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		//-->
		
		return (mapping.findForward("first"));
	}
	private String getEmailBody(String emailBody, String acttype,String address) {
		logger.debug("getEmailBody :: " + emailBody);
		
		if(emailBody!=null) {
			if(emailBody.contains("##ACT_TYPE##")) emailBody = emailBody.replaceAll("##ACT_TYPE##", acttype);
			if(emailBody.contains("##ADDRESS##")) emailBody = emailBody.replaceAll("##ADDRESS##", address);
		}
		emailBody = emailBody.replace("\\\"", "\"");
		logger.debug("getEmailBody :: " + emailBody);
		return emailBody;
	}
}
