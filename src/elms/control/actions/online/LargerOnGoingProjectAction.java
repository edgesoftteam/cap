package elms.control.actions.online;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.Operator;
import elms.agent.OnlineAgent;
import elms.control.beans.online.ApplyOnlinePermitForm;
import elms.security.User;
import elms.util.StringUtils;

public class LargerOnGoingProjectAction extends Action {
	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(LargerOnGoingProjectAction.class.getName());
	String nextPage = "success";
	ActionErrors errors = null;
	String lsoId = "";
	String peopleType = null;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		ApplyOnlinePermitForm applyOnlinePermitForm = (ApplyOnlinePermitForm) form;

		List tempOnlineDetails = new ArrayList();
		String emailAddr = ((User) session.getAttribute(elms.common.Constants.USER_KEY)).getFirstName();

		int tempOnlineID = StringUtils.s2i((String) request.getParameter("tempOnlineID"));

		// String createNewFlag = StringUtils.nullReplaceWithEmpty(request.getParameter("createNewFlag"));
		applyOnlinePermitForm.setTempOnlineID(tempOnlineID);
		OnlineAgent onlinePermitAgent = new OnlineAgent();

		int peopleId = StringUtils.s2i((String) request.getParameter("peopleId"));
		logger.debug("+++++++++++" + peopleId);
		int peopleTypeId = 0;
		// String chkExistPeopleFlag = applyOnlinePermitForm.getChkExistPeopleFlag();

		// forcePC is set to "Y" check give message if the force PC is yes
		request.setAttribute("forcePC", "Y");

		String contextHttpsRoot = request.getContextPath();

		request.setAttribute("contextHttpsRoot", contextHttpsRoot);

		try {

			peopleType = onlinePermitAgent.getPeopleType(tempOnlineID);
			
			StringBuilder sb = new StringBuilder();
			sb.append(request.getRequestURI());
			if(Operator.hasValue(request.getQueryString())){
				sb.append("?");
				sb.append(request.getQueryString());
			}
			
			String stepUrl = Operator.replace(sb.toString(), request.getContextPath(), "");
			onlinePermitAgent.updateStepUrl(tempOnlineID, stepUrl);

			onlinePermitAgent.storePeopleId(tempOnlineID, peopleId, peopleType);

			request.setAttribute("tempOnlineID", StringUtils.i2s(tempOnlineID));
			String type = "";
			if (peopleType.equalsIgnoreCase("E")) {
				nextPage = "successR";
				type = "4";

			} else if (peopleType.equalsIgnoreCase("R")) {

				logger.debug("AFTER ARCIIIIIIIIIIIII" + peopleType);
				request.setAttribute("updatePermit", "yes");
				request.setAttribute("forcePC", "Y");
				type = null;
				// payment process is done in between is removed

				nextPage = "successAuthorize";

			} else if (peopleType.equalsIgnoreCase("C")) {

				List ownerList = new ArrayList();
				ownerList = onlinePermitAgent.getOwnerList(tempOnlineID);
				request.setAttribute("ownerList", ownerList);
				nextPage = "successW";
				type = "7";
			} else {
				nextPage = "success";
				type = "1";
			}

			// for self to display or not based on login
			peopleTypeId = onlinePermitAgent.getPeopleType(tempOnlineID, emailAddr, type);// self
			if (peopleTypeId == 0) {
				peopleTypeId = onlinePermitAgent.getPeopleType(tempOnlineID, emailAddr);
			}
			request.setAttribute("peopleTypeId", StringUtils.i2s(peopleTypeId));
			// left side details bar display
			tempOnlineDetails = onlinePermitAgent.getTempOnlineDetails(tempOnlineID);
			request.setAttribute("tempOnlineDetails", tempOnlineDetails);

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception occured " + e.getMessage());
		}
		return (mapping.findForward(nextPage));
	}
}
