package elms.control.actions.online;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.OnlineAgent;
import elms.common.Constants;
import elms.control.beans.online.OnlineRegisterFrom;
import elms.security.User;
import elms.util.StringUtils;

public class EditProfileAction extends Action {
	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(EditProfileAction.class.getName());
	String nextPage = "success";
	int lsoId = 0;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.info("inside EditProfileAction");
		ActionErrors errors = null;
		OnlineRegisterFrom regForm = (OnlineRegisterFrom) form;
		HttpSession session = request.getSession();
		OnlineAgent oPAgent = new OnlineAgent();
		String edit = (String) request.getParameter("editProfile") != null ? (String) request.getParameter("editProfile") : "";
		String update = (String) request.getParameter("update") != null ? (String) request.getParameter("update") : "";
		
		String accNo = ((elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY)).getAccountNbr();
		String email = ((elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY)).getUsername();
	
		 errors = new ActionErrors();
		boolean checkVehicleStatus = oPAgent.checkVehicleNo(email);

		//if(checkVehicleStatus)
		//{
			boolean validateVehicleInfo=oPAgent.validateVehicleInfo(StringUtils.nullReplaceWithEmpty(regForm.getVehicleNo()),StringUtils.nullReplaceWithEmpty(regForm.getDlNo()),email);
			

		//}
		
		logger.debug("edit  ::  "+edit);
		logger.debug("update  ::  "+update);
		try {
			logger.debug("email:   "+email);
			if(validateVehicleInfo)
			{
				logger.debug("License plate or Vehicle number is used by another account");
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.vehicle.info.conflicts"));
				saveErrors(request, errors);
			}
			if (update.equalsIgnoreCase("yes")) {
				

				logger.debug("In updating...  ");
				String obc = regForm.getObc();
				logger.debug("obc is "+obc);
				String dot = regForm.getDot();
				if (errors.empty()) {
				oPAgent.updateOnlineUser(regForm, email);
				oPAgent.updatePeople(email, regForm);
				}
				//Updating session
				User user=new User();
				user.setUsername(regForm.getEmailAddress());
				user.setFirstName(regForm.getFirstName());
				user.setLastName(regForm.getLastName());
				user.setIsObc(regForm.getObc());
				user.setIsDot(regForm.getDot());
				session.setAttribute(elms.common.Constants.USER_KEY, user);
				
				String peopleTypes="";
				if(!regForm.getLicenseNbr().equalsIgnoreCase(""))
				{
					peopleTypes= peopleTypes  + "4,";
				}
				peopleTypes="8,2";
	

				if (obc.equals("Y")) {
					String peopleType = oPAgent.checkOBCUser(email, peopleTypes, regForm);
                    
					if (!peopleType.equals("")) {
						
						regForm.setPeopleType(peopleType);
						oPAgent.saveOBCUser(regForm);
					}

				}


			}
			String isObc = ((elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY)).getIsObc();
			regForm = oPAgent.getOnlineUsers(((User) session.getAttribute(Constants.USER_KEY)).getUsername(), isObc);
			request.setAttribute("peopleTypes", regForm.getPeopleType());
			request.setAttribute("licenseNbr", regForm.getLicenseNbr());

			String vehicle = "Y";
			if (!regForm.getVehicleNo().equals("")) {
				vehicle = "N";
			}
			request.setAttribute("vehicle", vehicle);

			request.setAttribute("onlineRegisterFrom", regForm);

		} catch (Exception e) {
			logger.error("Exception occured " + e.getMessage());
		}
		return (mapping.findForward("success"));
	}
}
