package elms.control.actions.online;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.Operator;
import elms.agent.OnlineAgent;
import elms.agent.PeopleAgent;
import elms.control.beans.online.ApplyOnlinePermitForm;
import elms.security.User;
import elms.util.StringUtils;

public class SelectContractorAction extends Action {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(SelectContractorAction.class.getName());
	String nextPage = "success";
	ActionErrors errors = null;
	int lsoId = 0;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		logger.debug("Entering SelectContractorAction...");
		ApplyOnlinePermitForm applyOnlinePermitForm = (ApplyOnlinePermitForm) form;

		HttpSession session = request.getSession();
		OnlineAgent onlinePermitAgent = new OnlineAgent();
		
		String fullName = ((User) session.getAttribute(elms.common.Constants.USER_KEY)).getFullName();
		String email = ((User) session.getAttribute(elms.common.Constants.USER_KEY)).getUsername();
		
		int tempOnlineID = StringUtils.s2i((String) request.getParameter("tempOnlineID"));
		
		List tempOnlineDetails = new ArrayList();
		
		int peopleId =0;

		try {
			
			peopleId = new PeopleAgent().getPeopleId(email);
			applyOnlinePermitForm.setTempOnlineID(tempOnlineID);
			StringBuilder sb = new StringBuilder();
			sb.append(request.getRequestURI());
			if(Operator.hasValue(request.getQueryString())){
				sb.append("?");
				sb.append(request.getQueryString());
			}
			
			String stepUrl = Operator.replace(sb.toString(), request.getContextPath(), "");
			onlinePermitAgent.updateStepUrl(tempOnlineID, stepUrl);

				request.setAttribute("fullName",fullName);
				applyOnlinePermitForm.setContractorName(fullName);
				onlinePermitAgent.updatePeopleId(tempOnlineID, "C", peopleId);
				nextPage = "success";
				
			tempOnlineDetails = onlinePermitAgent.getTempOnlineDetails(tempOnlineID);
			request.setAttribute("tempOnlineDetails", tempOnlineDetails);

		} catch (Exception e) {
			logger.error("Exception occured in SelectContractorAction" + e.getMessage(),e);
		}
		return (mapping.findForward(nextPage));
	}

}
