package elms.control.actions.online;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.LookupAgent;
import elms.agent.OnlineAgent;
import elms.control.beans.online.HolidayEditorForm;
import elms.control.beans.online.OnlinePermitForm;
import elms.control.beans.online.TempPermitForm;
import elms.util.StringUtils;

public class AdditionalInfoAction extends Action {

	static Logger logger = Logger.getLogger(AdditionalInfoAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();           
		String userId = StringUtils.i2s(((elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY)).getUserId());
		OnlinePermitForm addForm = (OnlinePermitForm) form;
		OnlineAgent egovAgent = new OnlineAgent();
		LookupAgent la = new LookupAgent();
		 
				
		String tempId = null;
		TempPermitForm tempPermitForm = (TempPermitForm) session.getAttribute("tempPermitForm");
		
		if(tempPermitForm != null){
			try {
				BeanUtils.copyProperties(addForm, tempPermitForm);
				tempId = (String) session.getAttribute("tempId");
				session.removeAttribute("tempPermitForm");
			} catch (IllegalAccessException e) {
				logger.error("Error copying properties" + e);
			} catch (InvocationTargetException e) {
				logger.error("Error copying properties" + e);
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		} 
		
		request.setAttribute("onlinePermitFrom", addForm);		
		logger.debug("selected activity type..:"+addForm.getActivityType());
				if(userId == null || userId.length() == 0) {
					return (mapping.findForward("index"));
				}
				List<HolidayEditorForm> holidaysList =  la.getHolidaysList();
				logger.debug("holidaysList  :"+holidaysList.size());
				
				request.setAttribute("holidaysList", holidaysList);
				try {
					String action = request.getParameter("action");
					logger.debug(action+" action");
							
					if(action != null && action.equals("adnext")) {
						//request.setAttribute("actionReset", "first");
						
						
						if(tempId == null)
							tempId = (String) session.getAttribute("tempId");
						
						String resumeUrl = request.getServletPath();
						String param = request.getQueryString();
						if(param != null)
							resumeUrl = resumeUrl + "?" + param;

						String desc="Garage Sale permit from:"+" "+addForm.getIssueDate()+" to "+addForm.getEndDate()+" - "+ addForm.getFirstName()+" "+addForm.getLastName();
						addForm.setActDescription(desc);
						OnlineAgent onlineAgent = new OnlineAgent();
						onlineAgent.updateEgovTempAdditonalInfo(tempId, addForm, resumeUrl, userId);
						
						return mapping.findForward("success");
						
					} else if(action != null && action.equals("bck")) {
						return mapping.findForward("bck");
					} else if(action != null && action.equals("reset")) {
						if(tempId ==null)
							tempId = (String) session.getAttribute("tempId");
						OnlineAgent onlineAgent = new OnlineAgent();
						onlineAgent.deleteTempPermit(tempId);
						addForm.reset1();
						session.removeAttribute("levelId");
						addForm = new OnlinePermitForm();
						request.setAttribute("onlinePermitFrom", addForm);
						request.setAttribute("error", "Permit has been cancelled successfully");				
						logger.debug("next action in permit type action..."+action);
						return mapping.findForward("rs");
					}else{
						
						/*CustomField[] customFieldList=addForm.getCustomFieldList();
						addForm=fillDropDown(request, addForm);
						
						if(action.equalsIgnoreCase("val"))
						{
						if(customFieldList !=null){
							logger.debug(" customFieldList : "+customFieldList.length);
							logger.debug(" action : "+action);
						for(int i=0;i<customFieldList.length;i++)
						{
						  addForm.getCustomFieldList()[i].setFieldValue(customFieldList[i].getFieldValue());	
						}
						}
						//}
*/						

						logger.debug("getActDescription... "+addForm.getActDescription());
						request.setAttribute("onlinePermitFrom", addForm);
						return mapping.findForward("first");
						/*if(addForm.getCustomFieldList().length > 0){
							return mapping.findForward("first");	
						}*/
					
				}
				} catch(Exception exp) {
					logger.error("error in AdditionalInfoAction page...:"+exp);
				}
				logger.debug("end of AdditionalInfoAction");
				return (mapping.findForward("success"));
			}
			
			/*public static OnlinePermitForm fillDropDown(HttpServletRequest request, OnlinePermitForm form) {
				try {
					 LookupAgent la = new LookupAgent();
					 CustomField[] customFieldArray = la.getCustomFieldArrayReq(form.getActivityType());
					 form.setCustomFieldList(customFieldArray);
					 if(customFieldArray != null && customFieldArray.length >0){
							
							for(int i=0; i< customFieldArray.length; i++){
								CustomField cf = customFieldArray[i];
								logger.debug("field Type::"+cf.getFieldType());
								if(cf.getFieldType() != null && cf.getFieldType().equalsIgnoreCase("DROPDOWN")){
									List<DropdownValue> optionList = la.getOptionsAsList(cf.getFieldId());
									request.setAttribute("dropdownOptionList"+cf.getFieldId(), optionList);
								}else if(cf.getFieldType() != null && cf.getFieldType().equalsIgnoreCase("CHECKBOX")){
									if(cf.getFieldValue()!= null && cf.getFieldValue().equalsIgnoreCase("Y")){
										cf.setFieldValue("on");
									}							
								}
							}
						}
					
				 } catch(Exception exp) {
					 exp.printStackTrace();
					 logger.error("error while filling dropdowns:"+exp);
				 }
				return form;
	}*/
}