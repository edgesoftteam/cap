package elms.control.actions.online.inspections;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import sun.jdbc.rowset.CachedRowSet;
import elms.agent.InspectionAgent;
import elms.agent.OnlineAgent;
import elms.control.beans.online.HolidayEditorForm;

public class HolidayEditorAction extends Action {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(HolidayEditorAction.class.getName());
	String nextPage = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		String action = request.getParameter("action")!=null?request.getParameter("action"):"list";
		
		
		try{
			if(action.equalsIgnoreCase("list")){
				logger.debug("holiday list selected");
				CachedRowSet holidayList = (CachedRowSet) new InspectionAgent().getHolidayList();
				request.setAttribute("holidayList", holidayList);
				logger.debug("obtained holiday list of size "+holidayList.size());
				return (mapping.findForward("list"));
			}
			else if(action.equalsIgnoreCase("add")){
				logger.debug("Add action requested");
				return (mapping.findForward("add"));
			}
			else if(action.equalsIgnoreCase("delete")){
				logger.debug("Delete action requested");
				String holidayId = request.getParameter("holidayId");
				logger.debug("holiday id is "+holidayId);
				new OnlineAgent().deleteHoliday(holidayId);
				CachedRowSet holidayList = (CachedRowSet) new InspectionAgent().getHolidayList();
				request.setAttribute("holidayList", holidayList);
				logger.debug("obtained holiday list of size "+holidayList.size());
				return (mapping.findForward("list"));
			}
			else if(action.equalsIgnoreCase("save")){
				logger.debug("Save action requested");
				HolidayEditorForm holidayEditorForm = (HolidayEditorForm)form;
				new OnlineAgent().saveHoliday(holidayEditorForm); 

				//get the list to display
				CachedRowSet holidayList = (CachedRowSet) new InspectionAgent().getHolidayList();
				request.setAttribute("holidayList", holidayList);
				logger.debug("obtained holiday list of size "+holidayList.size());
				return (mapping.findForward("list"));
			}
			else{
				logger.error("no mapping defined");
				return (mapping.findForward("error"));
			}
			
			
		}
		catch(Exception e){
			logger.error("Exception happenned while saving holidays "+e.getMessage());
			return (mapping.findForward("error"));
		}
		
			
		
		

	}
}
