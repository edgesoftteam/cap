package elms.control.actions.online.inspections;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.CommonAgent;
import elms.agent.InspectionAgent;
import elms.agent.LookupAgent;
import elms.agent.OnlineAgent;
import elms.app.admin.InspectionItem;
import elms.app.inspection.Inspection;
import elms.app.people.People;
import elms.app.project.Activity;
import elms.common.Constants;
import elms.control.beans.InspectionForm;
import elms.control.beans.online.HolidayEditorForm;
import elms.control.beans.online.InspectionRoutingForm;
import elms.security.User;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

public class InspectionRequestResultsAction extends Action {
	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(InspectionRequestResultsAction.class.getName());
	String nextPage = "";
	String message = "";
	protected String fromEmail = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		try {
			InspectionRoutingForm inspForm = (InspectionRoutingForm) form;
			InspectionAgent inspectionAgent =new InspectionAgent();

			fromEmail = Wrapper.getResourceBundle().getString("EMAIL_FROM");
			OnlineAgent onlineAgent = new OnlineAgent();

			InspectionAgent inspectionReqResAgent = new InspectionAgent();

			String action = (request.getParameter("action") != null) ? request.getParameter("action") : "view";

			String actNbr = (request.getParameter("actNbr") != null) ? request.getParameter("actNbr") : "";
			logger.debug("action is " + action);

			String activityNumber = inspForm.getPermitNumber();

			String inspDate = inspForm.getInspectionDate();

			String inspectionCode = inspForm.getInspectionCode();
			String address = "";
			if(activityNumber !=null && !activityNumber.equalsIgnoreCase("")){
			address =inspectionAgent.getAddressFromPermitNumber(activityNumber);
			}
			logger.debug("inspectionCode is :: " + inspectionCode);
			request.setAttribute("address", address);
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			Date date = new Date();
			String currentDate = sdf.format(date);
			
			Calendar cal1 = Calendar.getInstance();
			
			String inspectionDateRequest = inspectionReqResAgent.getNextInspectionDate(cal1);
			
				logger.debug("Inside cancelAddress");
				String inspectionDateReq ="";
				if (inspectionDateRequest != null) {
					inspectionDateReq =inspectionDateRequest.substring(5, 7) + "/" + inspectionDateRequest.substring(8, 10) + "/" + inspectionDateRequest.substring(0, 4);
					logger.debug("inspectionDateRequest is " + inspectionDateReq);
					
				}				
				
				currentDate=inspectionDateReq;
			
				
			if (action.equalsIgnoreCase("resaddress")) {
				logger.debug("Inside Result Address");

				inspForm.setInspectionDate(currentDate);
				request.setAttribute("decisionButton", new String("fromResult"));
				List<HolidayEditorForm> holidaysList =  inspectionAgent.getHolidaysList();
				request.setAttribute("holidaysList", holidaysList);
				return (mapping.findForward("viewReq"));
			}
			
			// For results page
			if (action.equalsIgnoreCase("res")) {
				logger.debug("Inside Result");

				if (!actNbr.equals("")) {
					inspForm.setPermitNumber(actNbr);
				}
				inspForm.setInspectionDate(currentDate);
				request.setAttribute("decisionButton", new String("fromResult"));
				List<HolidayEditorForm> holidaysList =  inspectionAgent.getHolidaysList();
				request.setAttribute("holidaysList", holidaysList);
				if (!actNbr.equals("")) {
					inspForm.setPermitNumber(actNbr);
				}

				return (mapping.findForward("viewRes"));
			}

			// For Cancel Page
			Calendar c1 = new GregorianCalendar();
			String inspectionDateCancel = inspectionReqResAgent.getNextInspectionDateForCancel(c1);
			if (action.equalsIgnoreCase("cancelAddress")) {
				logger.debug("Inside cancelAddress");
				String inspectionDate1 ="";
				if (inspectionDateCancel != null) {
					inspectionDate1 =inspectionDateCancel.substring(5, 7) + "/" + inspectionDateCancel.substring(8, 10) + "/" + inspectionDateCancel.substring(0, 4);
					logger.debug("inspectionDateCancel is " + inspectionDate1);
					request.setAttribute("inspectionDateCancel", inspectionDate1);
				}				
				inspForm.setInspectionDate(inspectionDate1);
				List<HolidayEditorForm> holidaysList =  inspectionAgent.getHolidaysList();
				request.setAttribute("holidaysList", holidaysList);
				return (mapping.findForward("viewReq"));
			}
			if (action.equalsIgnoreCase("cancel")) {
				logger.debug("Inside cancel");
				String inspectionDate1 ="";
				if (inspectionDateCancel != null) {
					inspectionDate1 =inspectionDateCancel.substring(5, 7) + "/" + inspectionDateCancel.substring(8, 10) + "/" + inspectionDateCancel.substring(0, 4);
					logger.debug("inspectionDateCancel is " + inspectionDate1);
					request.setAttribute("inspectionDateCancel", inspectionDate1);
				}
				inspForm.setInspectionDate(inspectionDate1);
				List<HolidayEditorForm> holidaysList =  inspectionAgent.getHolidaysList();
				request.setAttribute("holidaysList", holidaysList);
				if (inspectionDateCancel == null) {
					logger.debug("Cannot cancel inspection at this point of time. Please try again later ");
					request.setAttribute("error", "Cannot cancel  inspection at this point of time. Please try again later");

					request.setAttribute("decisionButton", new String("fromRequest"));

					return (mapping.findForward("viewReq"));
				}

				if (!actNbr.equals("")) {
					inspForm.setPermitNumber(actNbr);
				}

				return (mapping.findForward("viewRes"));
			}
			
			if (action.equalsIgnoreCase("displayCancel")) {
				logger.debug("Inside displayCancel");
				String inspectionDate1 ="";
				
				List<HolidayEditorForm> holidaysList =  inspectionAgent.getHolidaysList();
				request.setAttribute("holidaysList", holidaysList);
				String actId = inspectionReqResAgent.getActivityId(activityNumber)[0];
				 List<Inspection> inspectionList =	inspectionReqResAgent.getNonCancelInspections(actId, inspForm.getInspectionDate());
				request.setAttribute("inspectionList", inspectionList);
				logger.debug("inspectionList "+inspectionList.size());
				if (inspectionList == null || inspectionList.size() ==0) {
					request.setAttribute("error", "Cannot find the scheduled Inspection for the given activity number ,date");

					return (mapping.findForward("viewRes"));
				}	

				return (mapping.findForward("viewRes"));
			}
			
			// for view cancel
			if (action.equalsIgnoreCase("viewCancel")) {

				if (inspectionDateCancel != null) {
					String inspectionDate1 = inspectionDateCancel.substring(5, 7) + "/" + inspectionDateCancel.substring(8, 10) + "/" + inspectionDateCancel.substring(0, 4);
					logger.debug("inspectionDateCancel is " + inspectionDate1);
					request.setAttribute("inspectionDateCancel", inspectionDate1);
				}
				
				logger.debug("Inside viewCancel action");

				if ((activityNumber == null) || activityNumber.equals("")) {
					request.setAttribute("error", "Please enter your permit number");

					return (mapping.findForward("viewRes"));
				}

				String actId = inspectionReqResAgent.getActivityId(activityNumber)[0];

				if ((actId == null) || actId.equals("")) {
					logger.debug("Cannot find the Permit Number.");

					request.setAttribute("error", "Cannot find the Permit Number");

					return (mapping.findForward("viewRes"));
				}
				String cancelInspDate= StringUtils.mmddyyyytoyyyymmdd(inspForm.getInspectionDate());
				logger.debug("cancelInspDate  :"+cancelInspDate);
				String ids = request.getParameter("ids");
				logger.debug("ids :"+ids);
				
				boolean result = inspectionReqResAgent.cancelInspectionResults(activityNumber,cancelInspDate  , inspectionCode,ids);
			    List<Inspection> inspectionList =	inspectionReqResAgent.getNonCancelInspections(actId, inspForm.getInspectionDate());
				request.setAttribute("inspectionList", inspectionList);
			//	boolean result =false;
				if(inspectionList !=null){
					result = true;
				}
			    if (result == true) {

					StringBuffer emailTo = new StringBuffer();
					StringBuffer nullEmail = new StringBuffer();
					List emailList = new ActivityAgent().getEmailBlastforActivityStatusChange(StringUtils.s2i(actId));

					if (!emailList.isEmpty()) {

						for (int i = 0; i < emailList.size(); i++) {

							People people = (People) emailList.get(i);

							// Check if email is null means send mail to webmaster saying people does not have email address else send email to the concerned person

							if (people.getEmailAddress().equalsIgnoreCase(fromEmail)) {

								nullEmail.append("People ID: " + people.getPeopleId() + " \n Name: " + people.getName() + "  \n  Address:  " + people.getAddress() + "\n\n");
							} else {
								emailTo.append(people.getEmailAddress() + ";");
							}
						}
					}

					StringUtils.sendEmail(emailTo + "", "cancel", LookupAgent.getActivityType(inspectionReqResAgent.getActivityId(activityNumber)[1]).getDescription(), LookupAgent.getActivityAddress(StringUtils.s2i(actId))[0], inspectionDateCancel, LookupAgent.getInspctItemDescFromId("" + new InspectionAgent().getInspectionItemCodeCancel(actId)), "Cancelled", activityNumber, new InspectionAgent().getInspectionCommentsForCancel(actId));
					List<HolidayEditorForm> holidaysList =  inspectionAgent.getHolidaysList();
					request.setAttribute("holidaysList", holidaysList);
					request.setAttribute("message", "Inspection schedule has been sucessfully cancelled");

					return (mapping.findForward("viewRes"));
				}

				/*if (result == false) {
					request.setAttribute("error", "Cannot find the scheduled Inspection for the given activity number ,date and Inspection Code.");

					return (mapping.findForward("viewRes"));
				}
				*/
				
				logger.debug("Before Setting List in Request Object");

				return (mapping.findForward("viewRes"));
			}
			// For viewing inspection results
			else if (action.equalsIgnoreCase("view")) {
				if ((activityNumber == null) || activityNumber.equals("")) {
					request.setAttribute("error", "Please enter your permit number");

					request.setAttribute("decisionButton", new String("fromResult"));

					return (mapping.findForward("viewRes"));
				}
				inspForm.setInspectionDate(currentDate);
				String DatefromReq = (request.getAttribute("DatefromReq") != null) ? (String) request.getAttribute("DatefromReq") : "";

				if (!DatefromReq.equalsIgnoreCase("")) {
					inspDate = DatefromReq;
				}

				// logger.debug("Code is "+StringUtils.s2i(inspectionCode));
				if ((inspDate == null) || inspDate.equals("")) {
					request.setAttribute("error", "Please enter correct Date");
					request.setAttribute("decisionButton", new String("fromResult"));

					return (mapping.findForward("viewRes"));
				}

				String actId = inspectionReqResAgent.getActivityId(activityNumber)[0];

				if ((actId == null) || actId.equals("")) {
					logger.debug("Cannot find the Permit Number.");

					request.setAttribute("error", "Cannot find the Permit Number");

					request.setAttribute("decisionButton", new String("fromResult"));

					return (mapping.findForward("viewRes"));
				}

				String inspectionCode1 = inspForm.getInspectionCode();
				logger.debug(inspectionCode1);

				List inspection = (ArrayList) inspectionReqResAgent.fetchInspectionResults(activityNumber, inspDate, inspectionCode1);

				if (inspection == null) {
					request.setAttribute("error", "Please check your permit number.");
					request.setAttribute("decisionButton", new String("fromResult"));

					return (mapping.findForward("viewRes"));
				}

				logger.debug("Before Setting List in Request Object");

				request.getSession().setAttribute("inspection", inspection);

				return (mapping.findForward("success"));
			}

			// Find out the next inspection day.
			// Find calendar next day and check whether its not a holiday from
			// holiday editor and
			// its a weekday
			Calendar cal = new GregorianCalendar();
			String[] result = inspectionReqResAgent.isNoScheduleWindow(cal);

			if (result[0].equals("true")) {
				logger.debug("Cannot schedule  inspection until" + result[1] + " . Please try again later");

				if (StringUtils.s2i(result[1]) <= 1159) {
					if (result[1].length() > 3) {
						request.setAttribute("error", "Cannot schedule  inspections until " + result[1].substring(0, 2) + ":" + result[1].substring(2, 4) + " a.m., please try again after " + result[1].substring(0, 2) + ":" + result[1].substring(2, 4) + " a.m. today");
					} else {
						request.setAttribute("error", "Cannot schedule  inspections until " + result[1].substring(0, 1) + ":" + result[1].substring(1, 3) + " a.m., please try again after " + result[1].substring(0, 1) + ":" + result[1].substring(1, 3) + " a.m. today");
					}
				} else {
					int a = StringUtils.s2i(result[1]);
					String pm = a - 1200 + "";
					
					if (pm.length() > 3) {
						request.setAttribute("error", "Cannot schedule  inspections until " + pm.substring(0, 2) + ":" + pm.substring(2, 4) + " p.m., please try again after " + pm.substring(0, 2) + ":" + pm.substring(2, 4) + " p.m today");
					} else {
						request.setAttribute("error", "Cannot schedule  inspections until " + pm.substring(0, 1) + ":" + pm.substring(1, 3) + " p.m., please try again after" + pm.substring(0, 1) + ":" + pm.substring(1, 3) + " p.m. today");
					}
				}

				// request.setAttribute("error",
				// "Cannot schedule  inspection now. Please try again later");
				request.setAttribute("decisionButton", new String("fromRequest"));

				return (mapping.findForward("viewReq"));
			}
			String inspectionDate ="";
			inspectionDate= StringUtils.mmddyyyytoyyyymmdd(inspDate);
			logger.debug("inspDate  :"+inspDate);
			
			if(inspectionDate == null || inspectionDate.equalsIgnoreCase("")){
			
			 inspectionDate = inspectionReqResAgent.getNextInspectionDate(cal);
			}/*else{
				Date date1=new SimpleDateFormat("MM/dd/yyyy").parse(inspectionDate);  
			    System.out.println("\t"+date1); 
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				String strTimeStamp = sdf.format(date1);
				logger.debug("strTimeStamp  :"+strTimeStamp);
				logger.debug("inspectionDate  :"+inspectionDate);
				logger.debug("inspectionDate  :"+inspectionDate.substring(0, 1));
				logger.debug("inspectionDate  :"+inspectionDate.substring(2,3));
				logger.debug("inspectionDate  :"+inspectionDate.substring(4, 8));
				//inspectionDate= "0"+inspectionDate.substring(0, 1) + "-0" + inspectionDate.substring(2, 3) + "-" + inspectionDate.substring(4, 8);
				//inspectionDate= inspectionDate.substring(4, 8) + "-0" + inspectionDate.substring(0, 1) + "-0" + inspectionDate.substring(2, 3);
				inspectionDate= strTimeStamp;
				logger.debug("inspectionDate  :"+inspectionDate);
			}*/
			logger.debug("inspectionDate :"+inspectionDate);
			logger.debug("inspDate :"+inspDate);
			if (inspDate != null && !inspDate.equalsIgnoreCase("")) {
				String inspectionDate1 = inspectionDate.substring(5, 7) + "/" + inspectionDate.substring(8, 10) + "/" + inspectionDate.substring(0, 4);
				//String inspectionDate1 =  "0" + inspDate.substring(0, 1) + "/0" + inspDate.substring(2, 3) + "/" + inspDate.substring(4, 8);
				request.setAttribute("inspectionDate", inspectionDate1);
			}else if (inspectionDate != null && !inspectionDate.equalsIgnoreCase("")) {
				String inspectionDate1 = inspectionDate.substring(5, 7) + "/" + inspectionDate.substring(8, 10) + "/" + inspectionDate.substring(0, 4);

				request.setAttribute("inspectionDate", inspectionDate1);
			}

			if (inspectionDate == null) {
				logger.debug("Cannot schedule  inspection at this point of time. Please try again later.");
				request.setAttribute("error", "Cannot schedule  inspection at this point of time. Please try again later");

				request.setAttribute("decisionButton", new String("fromRequest"));
				List<HolidayEditorForm> holidaysList =  inspectionAgent.getHolidaysList();
				request.setAttribute("holidaysList", holidaysList);
				return (mapping.findForward("viewReq"));
			}

			// For request page
			if (!actNbr.equals("")) {
				logger.debug("Cannot find the Permit Number.");
				inspForm.setPermitNumber(actNbr);
				activityNumber = actNbr;
				action = new String("getCode");
				logger.debug("" + action);
			} else if (action.equalsIgnoreCase("req")) {
				request.setAttribute("decisionButton", new String("fromRequest"));
				List<HolidayEditorForm> holidaysList =  inspectionAgent.getHolidaysList();
				request.setAttribute("holidaysList", holidaysList);
				//InspectionAgent inspectionAgent= new InspectionAgent();
				//date=StringUtils.str2cal(()); 
				
				String InspectionDate = inspectionAgent.getNextInspectionDate(Calendar.getInstance());
				InspectionDate=InspectionDate.substring(5, 7) + "/" + InspectionDate.substring(8, 10) + "/" + InspectionDate.substring(0, 4);
				inspForm.setInspectionDate(InspectionDate);//currentDate);  
				
				logger.debug("Cannot find the Permit Number.");
				return (mapping.findForward("viewReq"));  
			}

			if ((activityNumber == null) || activityNumber.equals("")) {
				logger.debug("Cannot find the Permit Number.");
				request.setAttribute("error", "Please enter your permit number");

				request.setAttribute("decisionButton", new String("fromRequest"));
				List<HolidayEditorForm> holidaysList =  inspectionAgent.getHolidaysList();
				request.setAttribute("holidaysList", holidaysList);
				return (mapping.findForward("viewReq"));
			}

			String actId = inspectionReqResAgent.getActivityId(activityNumber)[0];
			String type = inspectionReqResAgent.getActivityId(activityNumber)[1];
			String status = inspectionReqResAgent.getActivityId(activityNumber)[2];
			
		
			if ((actId == null) || actId.equals("")) {
				logger.debug("Cannot find the Permit Number.");

				request.setAttribute("error", "Cannot find the Permit Number");

				request.setAttribute("decisionButton", new String("fromRequest"));
				List<HolidayEditorForm> holidaysList =  inspectionAgent.getHolidaysList();
				request.setAttribute("holidaysList", holidaysList);
				return (mapping.findForward("viewReq"));
			}

			// Check weather the permit status is issued
			logger.debug("The Status is" + status.toUpperCase() + "     " + Constants.ISSUED);

			if (status != null) {
				if (!(status.toUpperCase().trim().equals(Constants.ISSUED)) && !(status.toUpperCase().trim().equals(Constants.EXTENDED)) && !(status.toUpperCase().trim().equalsIgnoreCase(Constants.REISSUED))) {
					logger.debug("Cannot add an inspection request. The permit has not been issued for this Activity");

					request.setAttribute("error", "Cannot add an inspection request. The permit status is not valid for adding Inspections");

					request.setAttribute("decisionButton", new String("fromRequest"));
					List<HolidayEditorForm> holidaysList =  inspectionAgent.getHolidaysList();
					request.setAttribute("holidaysList", holidaysList);
					return (mapping.findForward("viewReq"));
				}
			}

			Activity a = new ActivityAgent().getActivity(StringUtils.s2i(actId));
			logger.debug("Fees due is " + a.isInspectable());
			if (a.isInspectable() == false) {

				logger.debug("Cannot add an inspection request. There is fees due");

				request.setAttribute("error", "Cannot add an inspection request. There is fees due");

				request.setAttribute("decisionButton", new String("fromRequest"));
				List<HolidayEditorForm> holidaysList =  inspectionAgent.getHolidaysList();
				request.setAttribute("holidaysList", holidaysList);
				return (mapping.findForward("viewReq"));

			}

			// Checking for maximum limit of Request per day for inspections
			int maxLimitScheduled = onlineAgent.getMaxInspectionScheduledPerDay(inspectionDate);
			int maxLimit = onlineAgent.getLimitPerDay();
			logger.debug("maxLimitScheduled" + maxLimitScheduled + ">" + maxLimit + "maxLimit");

			if (maxLimitScheduled >= maxLimit) {
				logger.debug("The  limit for requesting Inspection  has exceded today. " + maxLimitScheduled);

				//request.setAttribute("error", "The  limit for requesting Inspection  has exceded today. Please try to schedule tommorow");  
				request.setAttribute("error", "The maximum number of inspections for the day you are requesting has been reached.  This system is not able to schedule the inspection for the day you requested but may accept a request for a different business day up to three days in advance. For questions, please contact Inspection staff during Inspector office hours between 7:00 am and 8:00 am and 3:00 pm to 3:30 pm. We apologize for the inconvenience.");
				request.setAttribute("decisionButton", new String("fromRequest"));
				List<HolidayEditorForm> holidaysList =  inspectionAgent.getHolidaysList();
				request.setAttribute("holidaysList", holidaysList);
				return (mapping.findForward("viewReq"));
			}

			/*// Check for one inspection per permit per day.
			if (inspectionReqResAgent.checkOneInspectionPerPermitPerDay(actId, inspectionDate)) {
				logger.debug("Only one request for inspection per permit per day is allowed. ");

				request.setAttribute("error", "Only one inspection per permit per day is allowed.");

				request.setAttribute("decisionButton", new String("fromRequest"));

				return (mapping.findForward("viewReq"));
			}

			// Check for one inspection Code per day .
			if ((inspectionCode != null) && !inspectionCode.equals("")) {
				if (inspectionReqResAgent.checkOneInspectionCodePerDay(inspectionCode, inspectionDate, actId)) {
					logger.debug("Only one request for one inspection Code per day is allowed. ");

					request.setAttribute("error", "Only one request for one inspection code per day is allowed.");

					request.setAttribute("decisionButton", new String("fromRequest"));

					return (mapping.findForward("viewReq"));
				}
			}*/

			// Populate Activity Type
			if (action.equalsIgnoreCase("getCode")) {
				logger.debug("in getCode action.");
				List inspectionItemCodeList = InspectionAgent.getInspectionItemCodes(type);
				inspForm.setInspectionItemCodeList(inspectionItemCodeList);
				inspForm.setInspectionDate(currentDate);
				if ((inspectionItemCodeList != null) && !inspectionItemCodeList.isEmpty()) {
					if (((InspectionItem) inspectionItemCodeList.get(0)).getCode() == -1) {
						inspectionItemCodeList.remove(0);
					}
				}
				List<HolidayEditorForm> holidaysList =  inspectionAgent.getHolidaysList();
				request.setAttribute("holidaysList", holidaysList);
				request.setAttribute("inspectionItemCodeList", inspectionItemCodeList);
				request.setAttribute("decisionButton", new String("fromRequest"));

				return (mapping.findForward("viewReq"));
			}

			// For adding inspection
			if (action.equalsIgnoreCase("schedule")) {
				/*if ((inspectionCode == null) || inspectionCode.equals("") || inspectionCode.equals("-1")) {
					logger.debug("Please enter the Inspection Code.");

					request.setAttribute("error", "Please enter the Inspection Code.");

					request.setAttribute("decisionButton", new String("fromRequest"));

					return (mapping.findForward("viewReq"));
				}*/
				if ((inspForm.getInspectionCodes() == null) || inspForm.getInspectionCodes().equals("") ) {
					logger.debug("Please enter the Inspection Type.");

					request.setAttribute("error", "Please enter the Inspection Type.");

					request.setAttribute("decisionButton", new String("fromRequest"));
					List<HolidayEditorForm> holidaysList =  inspectionAgent.getHolidaysList();
					List inspectionItemCodeList = InspectionAgent.getInspectionItemCodes(type);
					inspForm.setInspectionItemCodeList(inspectionItemCodeList);
					inspForm.setInspectionDate(currentDate);
					if ((inspectionItemCodeList != null) && !inspectionItemCodeList.isEmpty()) {
						if (((InspectionItem) inspectionItemCodeList.get(0)).getCode() == -1) {
							inspectionItemCodeList.remove(0);
						}
					}
					request.setAttribute("holidaysList", holidaysList);
					request.setAttribute("inspectionItemCodeList", inspectionItemCodeList);
					return (mapping.findForward("viewReq"));
				}
				if ((inspForm.getComments() != null) && (inspForm.getComments().length() > 7200)) {
					logger.debug("Comments cannot be greate than 7200 charecters. Please reduce your comments");

					request.setAttribute("error", "Comments cannot be greate than 7200 charecters. Please reduce your comments");

					request.setAttribute("decisionButton", new String("fromRequest"));
					List<HolidayEditorForm> holidaysList =  inspectionAgent.getHolidaysList();
					request.setAttribute("holidaysList", holidaysList);
					return (mapping.findForward("viewReq"));
				}

				/*if ((inspDate != null) && ((String) request.getAttribute("inspectionDate") != inspDate)) {
					logger.debug("The inspection date has changed ");

					request.setAttribute("error", "Comments cannot be greate than 7200 charecters. Please limit your comments");

					request.setAttribute("decisionButton", new String("fromRequest"));

					return (mapping.findForward("viewReq"));
				}*/

				boolean rights = false;

				if (new CommonAgent().hasHolds(StringUtils.s2i(actId))) {
					logger.debug("Cannot schedule since there is a hold on this account ");

					request.setAttribute("error", "Cannot schedule since there is a hold on this account ");

					request.setAttribute("decisionButton", new String("fromRequest"));
					List<HolidayEditorForm> holidaysList =  inspectionAgent.getHolidaysList();
					request.setAttribute("holidaysList", holidaysList);
					return (mapping.findForward("viewReq"));
				}
				User user = (User) (request.getSession().getAttribute(Constants.USER_KEY));
				
				//List actList = new InspectionAgent().getMyPermits(((User) (request.getSession().getAttribute(Constants.USER_KEY))).getUsername());
				//int peopleId = inspectionAgent.getPeopleId(((User) (request.getSession().getAttribute(Constants.USER_KEY))).getUsername());
				//inspectionAgent.addActivityPeople(StringUtils.s2i(actId),peopleId,user,inspForm.getContactName(),inspForm.getContactPhone());	   
							
					   		
							
			/*	if (actList != null) {
					for (int i = 0; i < actList.size(); i++) {
						int actId1 = ((elms.app.project.Activity) actList.get(i)).getActivityId();

						if (actId1 == (elms.util.StringUtils.s2i(actId))) {
							rights = true;
						}
					}
				}

				if (rights == false) {
					logger.debug("Cannot schedule since you are not associated with that permit number ");

					request.setAttribute("error", "Cannot schedule Inspections as  you are not associated with this permit number");

					request.setAttribute("decisionButton", new String("fromRequest"));

					return (mapping.findForward("viewReq"));
				}*/

				InspectionForm frm = new InspectionForm();
				frm.setActivityId(actId);
				frm.setActivityType(type);
				frm.setActionCode(Constants.INSPECTION_CODES_REQUEST_FOR_INSPECTION + "");
				frm.setComments("");
				frm.setAddOrEdit("add");
				//frm.setInspectionDate(inspectionDate);
				frm.setInspectionDate(inspForm.getInspectionDate());
				frm.setInspectionItem(inspectionCode);
				frm.setInspectionItems(inspForm.getInspectionCodes());
				frm.setInspectorId("-1");
				frm.setComments(inspForm.getComments());  
				
				StringBuffer inspCode=new StringBuffer();
				if(inspForm.getInspectionCodes() !=null){
				for(int i=0;i<inspForm.getInspectionCodes().length;i++){
				//String inspCode =	LookupAgent.getInspctItemDescFromId(inspForm.getInspectionCodes()[i]);
				inspCode.append(LookupAgent.getInspctItemDescFromId(inspForm.getInspectionCodes()[i])+",");
				}
				frm.setInspectionItemDesc(inspCode.deleteCharAt(inspCode.length()-1).toString());
				}

				//frm.setInspectionItemDesc(LookupAgent.getInspctItemDescFromId(inspectionCode));
				
				request.getSession().setAttribute("inspectionForm", frm);
				
				request.setAttribute("inspectionDate", inspForm.getInspectionDate());
				request.setAttribute("contactName", inspForm.getContactName());
				request.setAttribute("contactPhone", inspForm.getContactPhone());
				//request.setAttribute("inspectionDate", inspectionDate);
				request.setAttribute("fromOnline", new String("fromOnline"));
				
				RequestDispatcher rd = this.getServlet().getServletContext().getRequestDispatcher("/saveNewInspection.do");

				rd.forward(request, response);
			}
		} catch (Exception e) {
			logger.error("Exception occured in InspectionRequestResult " + e.getMessage());

			return (mapping.findForward("error"));
		}

		return (null);
	}
}
