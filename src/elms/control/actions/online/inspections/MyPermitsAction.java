package elms.control.actions.online.inspections;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.CommonAgent;
import elms.agent.FinanceAgent;
import elms.agent.InspectionAgent;
import elms.agent.LookupAgent;
import elms.agent.OnlineAgent;
import elms.agent.UserAgent;
import elms.app.admin.ActivityStatus;
import elms.app.admin.MicrofilmStatus;
import elms.app.finance.FinanceSummary;
import elms.app.project.Activity;
import elms.app.project.ActivityDetail;
import elms.common.Constants;
import elms.control.actions.finance.ViewPaymentMgrAction;
import elms.control.beans.online.ApplyOnlinePermitForm;
import elms.control.beans.online.CartDetailsForm;
import elms.control.beans.online.MyPermitForm;
import elms.security.User;
import elms.util.StringUtils;

public class MyPermitsAction extends Action {
	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(MyPermitsAction.class.getName());
	String nextPage = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String viewPermit = request.getParameter("viewPermit");
		
		String action = (request.getParameter("action") != null) ? request.getParameter("action") : "view";
		String ref = (request.getParameter("ref") != null) ? request.getParameter("ref") : "";
		String inActive = (String) request.getParameter("inActive");
		
		if (action == null) {
			action = (String) request.getAttribute("action");
		}
		if (viewPermit == null) {
			viewPermit = (String) request.getAttribute("viewPermit");
		}
		if (inActive == null) {
			inActive = (String) request.getAttribute("inActive");
		}

		logger.debug("action is " + action);
		logger.debug("inActive is " + inActive);
		String displayMsg = "";
		if (request.getAttribute("displayMsg") != null) {
			displayMsg = (String) request.getAttribute("displayMsg");
		}
		logger.debug("displayMsg... " + displayMsg);
		request.setAttribute("message", displayMsg);

		InspectionAgent insAgent = new InspectionAgent();
		HttpSession session = request.getSession();

		User user = (User) session.getAttribute(elms.common.Constants.USER_KEY);

		String email = user.getUsername();
		logger.debug("email... " + email);
		if (email == null) {
			if (request.getParameter("email") != null) {
				email = (String) request.getParameter("email");
			} else {
				email = (String) session.getAttribute("email");
				logger.debug("email... " + email);
			}
		}

		logger.debug("email... " + email);
		if (!email.equalsIgnoreCase("")) {

			try {
				user = new UserAgent().getOnlineUser(user, email);
				session.setAttribute(Constants.USER_KEY, user);
				logger.debug("user object put in the session");

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			email = user.getUsername();
		}

		session.setAttribute(elms.common.Constants.USER_KEY, user);

		List actList = new ArrayList();
		List comboList = new ArrayList();
		List dotList = new ArrayList();
		CartDetailsForm cartDetailsForm = new CartDetailsForm();
		OnlineAgent onlineAgent = new OnlineAgent();
		String cartId = "";
		List<CartDetailsForm> cartDetailsForms = new ArrayList<CartDetailsForm>();
		try {
			// for Hide/Unhide changes showing up Combo Name and its respective Activities
			if (viewPermit.equalsIgnoreCase("Yes")) {
				logger.debug("in if");
				String isObc = request.getParameter("isObc");
				logger.debug("isObc.. " + isObc);
				if (isObc == null) {
					isObc = (String) request.getAttribute("isObc");
				}
				logger.debug("isObc.. " + isObc);
				String isDot = request.getParameter("isDot");
				if (isDot == null) {
					isDot = (String) request.getAttribute("isDot");
				}
				if (action.equalsIgnoreCase("view")) {
					if (isObc.equals("Y")) {
						logger.debug("in if..." + email);
						comboList = insAgent.getComboList(email, inActive);
						int comboActSize = 0;
						List<Integer> actIds = new ArrayList<Integer>();
						List<Integer> actIdsAddedToCart = new ArrayList<Integer>();
						ApplyOnlinePermitForm applyOnlinePermitObj = new ApplyOnlinePermitForm();
						for (int i = 0; i < comboList.size(); i++) {
							applyOnlinePermitObj = (ApplyOnlinePermitForm) comboList.get(i);
							comboActSize = applyOnlinePermitObj.getComboActSize();
							logger.debug("comboActSize::" + comboActSize);

							if (comboActSize > 0)
								for (int j = 0; j < comboActSize; j++) {
									actIds.add(StringUtils
											.s2i(((MyPermitForm) applyOnlinePermitObj.getComboActList().get(j))
													.getActivityId()));
								}

						}
						logger.debug("actIds::" + actIds.toString());
						if ((email) != null) {
							cartId = onlineAgent.getCartId(email);
							if (cartId != null) {
								actIdsAddedToCart = onlineAgent.getActIdsAddedToCart(cartId);
							}
						}
						logger.debug("actIdsAddedToCart::" + actIdsAddedToCart.toString());
						
					//Removing CART Details for permits which are not displaying for logged in user.(ReadyToPay flag having 'N')
						if (actIdsAddedToCart != null)
							for (int i = 0; i < actIdsAddedToCart.size(); i++) {
								if (actIds == null || !(actIds.contains(actIdsAddedToCart.get(i)))) {
									displayMsg = onlineAgent
											.removeCartDetails(StringUtils.i2s(actIdsAddedToCart.get(i)), cartId);
								}
							}
						logger.debug("actIdsAddedToCart::" + actIdsAddedToCart.toString());
						if ((email) != null) {
							cartId = onlineAgent.getCartId(email);
							if (cartId != null) {
								onlineAgent.updateCartInfoInitially(cartId);
							}
							cartDetailsForm = onlineAgent.getCartInfo(cartId);
							cartDetailsForm.setCartId(cartId);
							cartDetailsForm.setEmail(email);
							request.setAttribute("cartDetailsForm", cartDetailsForm);
						}
						if (cartId != null) {
							cartDetailsForms = onlineAgent.getCartDetails(cartId);
							for (int i = 0; i < cartDetailsForms.size(); i++) {
								CartDetailsForm cartDetailsForm2 = new CartDetailsForm();
								cartDetailsForm2 = cartDetailsForms.get(i);
								cartId = cartDetailsForm2.getCartId();
							}
						}
						request.setAttribute("comboSize", StringUtils.i2s(comboList.size()));
						request.setAttribute("comboActSize", StringUtils.i2s(comboActSize));

						request.setAttribute("cartDetailsForms", cartDetailsForms);

					}
					if (isDot.equals("Y")) {
						// request.setAttribute("comboSize","");
						dotList = insAgent.getDotList(user.getAccountNbr(), action);
						request.setAttribute("dotListSize", StringUtils.i2s(dotList.size()));
					}
					request.setAttribute("dotList", dotList);
					request.setAttribute("comboList", comboList);
					request.setAttribute("inActive", inActive);
					session.setAttribute("online", "Y");
					session.setAttribute("user", user);
					return (mapping.findForward("success"));
				} else if (action.equalsIgnoreCase("explore")) {
					logger.debug("in explore");
					String activityId = request.getParameter("activityId");
					String activityType = request.getParameter("activityType");
					Activity act = new Activity();
					act.setActivityId(StringUtils.s2i(activityId));
					getActivity(request, session, act);
					return (mapping.findForward("view"));
				} else if (action.equalsIgnoreCase("exploreOnline")) {
					logger.debug("in explore online");
					String activityId = request.getParameter("activityId");
					String activityType = request.getParameter("activityType");
					Activity act = new Activity();
					act.setActivityId(StringUtils.s2i(activityId));
					getActivity(request, session, act);
					return (mapping.findForward("viewOnline"));
				} else if (action.equalsIgnoreCase("pay")) {
					String router = (request.getParameter("action") != null) ? request.getParameter("action") : "Empty";
					router = "payment";
					String sprojId = request.getParameter("sprojId");
					// Checks if it is L S or O to identify the request is coming from which level
					request.setAttribute("level", "Q");
					request.setAttribute("levelId", sprojId);

					session.setAttribute("level", "Q");
					session.setAttribute("levelId", sprojId);

					if ("payment".equalsIgnoreCase(router)) {
						new ViewPaymentMgrAction().perform(mapping, form, request, response);
						// processPayment();
					}
					request.setAttribute("forcePC", new OnlineAgent().checkPlanCheck(sprojId));
					return (mapping.findForward("successQ"));
				}
				return null;
				// end of Hide/Unhide changes
			} else { // Else part Wont be used Left uncommented.

				if (action.equalsIgnoreCase("view")) {

					actList = insAgent.getMyPermits(email, action);
					session.setAttribute("actList", actList);

					return (mapping.findForward("success"));
				} else if (action.equalsIgnoreCase("explore")) {
					int ref1 = StringUtils.s2i(ref);
					logger.debug("In Explore method");

					actList = insAgent.getMyPermits(user.getFirstName(), action);
					session.setAttribute("actList", actList);
					logger.debug("Act List" + actList.size());

					Activity act = (Activity) ((List) actList).get(ref1);
					logger.debug("1 Act 3 rd record" + act.getActivityId());

					getActivity(request, session, act);

					return (mapping.findForward("view"));
				}
				// Payment start

				else if (action.equalsIgnoreCase("pay")) {

					String router = (request.getParameter("action") != null) ? request.getParameter("action") : "Empty";
					router = "payment";
					String sprojId = request.getParameter("sprojId");

					logger.debug("sprojId ID ------------------" + sprojId);
					// Checks if it is L S or O to identify the request is coming from which level
					request.setAttribute("level", "Q");
					request.setAttribute("levelId", sprojId);

					session.setAttribute("level", "Q");
					session.setAttribute("levelId", sprojId);

					if ("payment".equalsIgnoreCase(router)) {
						new ViewPaymentMgrAction().perform(mapping, form, request, response);
						// processPayment();
					}

					nextPage = "successQ";
				}
				// Payment End
				return (mapping.findForward(nextPage));
			}
		} catch (Exception e) {
			logger.error("Exception occured while setting limit per day " + e.getMessage() + e);

			return (mapping.findForward("error"));
		}
	}

	public void getActivity(HttpServletRequest request, HttpSession session, Activity act) throws Exception {
		try {
			int activityId = act.getActivityId();
			
			Activity activity = null;
			ActivityDetail activityDetail = null;
			boolean editable = false;
			String lsoAddress = null;
			ActivityAgent activityAgent = new ActivityAgent();
			lsoAddress = activityAgent.getActivityAddressForId(activityId);
			request.setAttribute("lsoAddress", lsoAddress);
			logger.debug("Set the lso address to session as " + lsoAddress);
			activity = new ActivityAgent().getActivity(activityId);

			if (activity == null) {
				logger.debug("Null Activity Object is created with activityId .." + activityId);
			} else {
				activityDetail = activity.getActivityDetail();
			}

			if (activityDetail == null) {
				logger.debug("Null ActivityDetail  Object is created from activity  Object ..");
			} else {
				request.setAttribute("activityId", StringUtils.i2s(activity.getActivityId()));
				logger.debug("activityId:" + request.getAttribute("activityId"));
				request.setAttribute("subProjectId", StringUtils.i2s(activity.getSubProjectId()));
				logger.debug("subprojectId:" + request.getAttribute("subProjectId"));
				request.setAttribute("activityNumber", activityDetail.getActivityNumber());

				String psaInfo = " -- " + activityDetail.getActivityNumber();
				String actNumber = activityDetail.getActivityNumber();
				logger.debug("activityNumber:" + request.getAttribute("activityNumber"));

				if (activityDetail.getActivityType() != null) {
					request.setAttribute("activityType", activityDetail.getActivityType().getType());
					request.setAttribute("activityTypeDesc", activityDetail.getActivityType().getDescription());
					psaInfo = psaInfo + " - " + activityDetail.getActivityType().getDescription();
					logger.debug("activityType:" + request.getAttribute("activityType"));
					request.setAttribute("completionDateLabel", activityDetail.getFinalDateLabel());
					logger.debug("Completion Date Label is set to request  as  " + activityDetail.getFinalDateLabel());
					request.setAttribute("expirationDateLabel", activityDetail.getExpirationDateLabel());
					logger.debug("Expiration Date Label is set to request  as  " + activityDetail.getFinalDateLabel());
				}

				if (activityDetail.getActivitySubTypes() != null) {
					logger.debug("entering activityDetail.getActivitySubType" + activityDetail.getActivitySubTypes());
					request.setAttribute("activitySubTypes", activityDetail.getActivitySubTypes());
				}

				session.setAttribute("psaInfo", psaInfo);
				session.setAttribute("actNumber", actNumber);
				request.setAttribute("description", activityDetail.getDescription());
				logger.debug("description:" + request.getAttribute("description"));

				if (activityDetail.getAddress() == null) {
					request.setAttribute("address", "");
				} else {
					request.setAttribute("address", activityDetail.getAddress());
				}

				logger.debug("address:" + request.getAttribute("address"));

				// setting addressId in constants
				session.setAttribute(Constants.ADDR_ID, StringUtils.i2s(activityDetail.getAddressId()));
				request.setAttribute("valuation", StringUtils.dbl2$(activityDetail.getValuation()));
				logger.debug("Valuation:" + request.getAttribute("valuation"));
				request.setAttribute("planCheckRequired", activityDetail.getPlanCheckRequired());
				logger.debug("planCheckRequired:" + request.getAttribute("planCheckRequired"));
				request.setAttribute("inspectionRequired", activityDetail.getInspectionRequired());
				logger.debug("inspectionRequired:" + request.getAttribute("inspectionRequired"));

				ActivityStatus activityStatus = activityDetail.getStatus();

				if (activityStatus != null) {
					request.setAttribute("status", activityStatus.getDescription());
				}

				logger.debug(
						"ViewActivityAction-- Parameter status is set to Request as " + request.getAttribute("status"));
				request.setAttribute("startDate", StringUtils.cal2str(activityDetail.getStartDate()));
				logger.debug("startDate:" + request.getAttribute("startDate"));
				request.setAttribute("appliedDate", StringUtils.cal2str(activityDetail.getAppliedDate()));
				logger.debug("appliedDate:" + request.getAttribute("appliedDate"));
				request.setAttribute("issueDate", StringUtils.cal2str(activityDetail.getIssueDate()));
				logger.debug("issue:" + request.getAttribute("issueDate"));
				request.setAttribute("expirationDate", StringUtils.cal2str(activityDetail.getExpirationDate()));
				logger.debug("expirationDate:" + request.getAttribute("expirationDate"));
				request.setAttribute("completionDate", StringUtils.cal2str(activityDetail.getFinaledDate()));
				logger.debug("completionDate:" + request.getAttribute("completionDate"));
				request.setAttribute("planCheckFeeDate", StringUtils.cal2str(activityDetail.getPlanCheckFeeDate()));
				logger.debug("planCheckFeeDate:" + request.getAttribute("planCheckFeeDate"));
				request.setAttribute("permitFeeDate", StringUtils.cal2str(activityDetail.getPermitFeeDate()));
				logger.debug("permitFeeDate:" + request.getAttribute("permitFeeDate"));

				MicrofilmStatus status = LookupAgent.getMicrofilmStatus(activityDetail.getMicrofilm());

				request.setAttribute("microfilm", status.getDescription());
				logger.debug("microFilm:" + request.getAttribute("microFilm"));

				String createdBy = "";

				if (activityDetail.getCreatedBy() != null) {
					String firstName = "";
					String lastName = "";

					if (activityDetail.getCreatedBy().getFirstName() != null) {
						firstName = activityDetail.getCreatedBy().getFirstName();
					}

					if (activityDetail.getCreatedBy().getLastName() != null) {
						lastName = activityDetail.getCreatedBy().getLastName();
					}

					createdBy = firstName + " " + lastName;
				}

				request.setAttribute("createdBy", createdBy);
				logger.debug("createdBy:" + request.getAttribute("createdBy"));
			}

			
			String disableFees = "N";

			logger.debug(" *** control is b4 block  ");

			
			request.setAttribute("disableFees", disableFees);
			logger.debug("disableFees:" + disableFees);

			
			List attachmentList = new CommonAgent().getAttachments(activityId, "A");
			request.setAttribute("attachmentList", attachmentList);

			
			request.setAttribute("commentList", new ArrayList());

			request.setAttribute("fromOnline", "fromOnline");
			// finance for this activity
			FinanceSummary financeSummary = new FinanceAgent().getActivityFinance(activityId);
			request.setAttribute("financeSummary", financeSummary);
			request.setAttribute("editable", "false");

			request.setAttribute("activity", activity);
			logger.info("Exiting ViewActivityAction");
		} catch (Exception e) {
			throw e;
		}
	}
}
