package elms.control.actions.online;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.agent.LookupAgent;
import elms.agent.OnlineAgent;
import elms.common.Constants;
import elms.control.beans.EmailDetails;
import elms.control.beans.online.OnlineRegisterFrom;
import elms.util.Email;
import elms.util.EmailSender;
import elms.util.MessageUtils;
import elms.util.StringUtils;

public class ForgotPasswordAction extends Action {

	static Logger logger = Logger.getLogger(ForgotPasswordAction.class.getName());
	ActionErrors errors = null;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		OnlineRegisterFrom onlineRegisterFrom = (OnlineRegisterFrom) form;

		HttpSession session = request.getSession();
		OnlineAgent onlinePermitAgent = new OnlineAgent();
		try {
			logger.debug("EMAIL ID" + onlineRegisterFrom.getEmailAddress());
			onlinePermitAgent.getForgotPassword(onlineRegisterFrom);
			logger.debug("Message" + onlineRegisterFrom.getMessage());

			// Prompt Msg & send email.
			if (onlineRegisterFrom.getMessage() != null && onlineRegisterFrom.getMessage().startsWith("COMP")) {

//				String subject = "City of Burbank - Your Password";
//				subject=new AdminAgent().getEmailSubject("FORGOT_PASSWORD");
//				Email email = new Email();
//				email.setFromAddress("EMAIL_FROM");
//				email.setToAddress(onlineRegisterFrom.getEmailAddress());
//				email.setSubject(subject);
//				String msg = "Hi, \n" + "Your Passord";
//				HashMap hm = new HashMap();
//				hm.put("USERNAME", onlineRegisterFrom.getEmailAddress());
//				hm.put("PASSWORD", onlineRegisterFrom.getPwd());
//				email.setMessage(StringUtils.parseString(new AdminAgent().getEmailMessage("FORGOT_PASSWORD"), hm));
//
//				MessageUtils.sendEmail(email);

				try {
						
				EmailSender emailSender = new EmailSender();
				EmailDetails emailDetails = new EmailDetails();	
				emailDetails.setUserName( onlineRegisterFrom.getEmailAddress());
				emailDetails.setPassword( onlineRegisterFrom.getPwd());
    			if(LookupAgent.getKeyValue(Constants.PROD_SERVER_FLAG).equalsIgnoreCase("N")) {
    				emailDetails.setEmailId(LookupAgent.getKeyValue(Constants.EDGESOFT_SUPPORT_EMAIL));
    			}else if(LookupAgent.getKeyValue(Constants.PROD_SERVER_FLAG).equalsIgnoreCase("Y")) {
	    			emailDetails.setEmailId(onlineRegisterFrom.getEmailAddress());	
    			}
    			emailDetails.setLkupSystemDataMap(OnlineAgent.getLkupSystemDataMap());
    			emailDetails.setEmailTemplateAdminForm(OnlineAgent.getEmailData(emailDetails,Constants.FORGOT_PASSWORD));
	        	emailSender.sendEmail(emailDetails);
			
					System.out.println(onlineRegisterFrom.getEmailAddress() + "Email  ");
//					String message = StringUtils.parseString(new AdminAgent().getEmailMessage("FORGOT_PASSWORD"), hm);
//					new MessageUtils().sendEmail(onlineRegisterFrom.getEmailAddress() + ";", subject, message);
				} catch (Exception e) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.register.email"));
					saveErrors(request, errors);
					logger.error("OnlineRegisterOtherDeatils action " + e.getMessage());
				}

				request.setAttribute("message", "Password was sent to your e-mail address. If you don't see the e-mail message in your inbox, look for it in your junk mail folder or check your e-mail again later.");

			} else if (onlineRegisterFrom.getMessage() != null && onlineRegisterFrom.getMessage().startsWith("INCOMP")) {
				request.setAttribute("error", "Your e-mail address could not be found.");
			} else if (onlineRegisterFrom.getMessage() != null && onlineRegisterFrom.getMessage().startsWith("ERR")) {
				request.setAttribute("error", onlineRegisterFrom.getMessage());
			}
			onlineRegisterFrom = new OnlineRegisterFrom();

		} catch (Exception e) {
			logger.error("An error occured in ForgotPasswordAction" + e.getMessage());
		}

		// Forward control to the specified success URI
		return (mapping.findForward("success"));

	}

}