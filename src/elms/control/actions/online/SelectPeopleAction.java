package elms.control.actions.online;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.Operator;
import elms.agent.OnlineAgent;
import elms.agent.PeopleAgent;
import elms.control.beans.online.ApplyOnlinePermitForm;
import elms.security.User;
import elms.util.StringUtils;

public class SelectPeopleAction extends Action {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(SelectPeopleAction.class.getName());
	String nextPage = "success";
	ActionErrors errors = null;
	int lsoId = 0;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		logger.debug("Entering SelectPeopleAction...");
		ApplyOnlinePermitForm applyOnlinePermitForm = (ApplyOnlinePermitForm) form;

		HttpSession session = request.getSession();
		OnlineAgent onlinePermitAgent = new OnlineAgent();
		
		String fullName = ((User) session.getAttribute(elms.common.Constants.USER_KEY)).getFullName();
		String email = ((User) session.getAttribute(elms.common.Constants.USER_KEY)).getUsername();
		
		int tempOnlineID = StringUtils.s2i((String) request.getParameter("tempOnlineID"));
		int  selectPeopleType = applyOnlinePermitForm.getSelectPeopleType();
		
		List tempOnlineDetails = new ArrayList();
		
		int peopleId =0;

		try {
		
			if(selectPeopleType<=0){
				List al = onlinePermitAgent.getTempOnlineDetails(tempOnlineID);
				if(al.size()>0){
					  
						ApplyOnlinePermitForm a = (ApplyOnlinePermitForm) al.get(0);
						logger.info(a.getUrl()+"***VALUE");
						URL u = new URL("http://localhost:8080"+a.getUrl());
						selectPeopleType = Operator.toInt(onlinePermitAgent.splitQuery(u).get("sName"));
						logger.info(selectPeopleType+"***selectPeopleType");
						applyOnlinePermitForm.setSelectPeopleType(selectPeopleType);
						
					}
			}
			
			peopleId = new PeopleAgent().getPeopleId(email);
			applyOnlinePermitForm.setTempOnlineID(tempOnlineID);
			
			
			
			if(selectPeopleType==1){//iContractor
				request.setAttribute("fullName",fullName);
				applyOnlinePermitForm.setContractorName(fullName);
				onlinePermitAgent.updatePeopleId(tempOnlineID, "C", peopleId);
				nextPage = "iContractor";
				
				
			}
			else if (selectPeopleType==2){//iOwnerBuilder
				request.setAttribute("fullName",fullName);
				applyOnlinePermitForm.setOwnerName(fullName);
				onlinePermitAgent.updatePeopleId(tempOnlineID, "W", peopleId);
				nextPage = "iOwnerBuilder";
				
			}
			else if (selectPeopleType==3){//contractor
				request.setAttribute("peopleType","C");
				request.setAttribute("showContractorText","Y");
				nextPage = "contractor";
				
			}
			else if (selectPeopleType==4){//noContractor
				nextPage = "noContractor";
				
			}
			else{
				logger.error("No Mapping exists for next page ");
			}
			
			
			StringBuilder sb = new StringBuilder();
			sb.append(request.getRequestURI());
			if(Operator.hasValue(request.getQueryString())){
				sb.append("?");
				sb.append(request.getQueryString());
			}
			
			String stepUrl = Operator.replace(sb.toString(), request.getContextPath(), "");
			onlinePermitAgent.updateStepUrl(tempOnlineID, stepUrl);
				// left side details bar display
			tempOnlineDetails = onlinePermitAgent.getTempOnlineDetails(tempOnlineID);
			request.setAttribute("tempOnlineDetails", tempOnlineDetails);

		} catch (Exception e) {
			logger.error("Exception occured " + e.getMessage(),e);
		}
		return (mapping.findForward(nextPage));
	}

}
