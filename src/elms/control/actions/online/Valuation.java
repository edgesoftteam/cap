package elms.control.actions.online;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.LookupAgent;
import elms.agent.OnlineAgent;
import elms.control.beans.online.ApplyOnlinePermitForm;
import elms.util.StringUtils;

public class Valuation extends Action {
	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(PermitTypeAction.class.getName());
	String nextPage = "success";
	ActionErrors errors = null;
	int lsoId = 0;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		ApplyOnlinePermitForm applyOnlinePermitForm = (ApplyOnlinePermitForm) form;

		HttpSession session = request.getSession();
		OnlineAgent onlinePermitAgent = new OnlineAgent();
		String checkAddress = "";
		String unitNumber = null;
		String streetNumber = null;
		String streetFraction = null;
		int streetId = 0;
		checkAddress = StringUtils.nullReplaceWithEmpty((String) request.getParameter("checkAddress"));
		System.out.println(checkAddress);
		try {
			if (checkAddress.equalsIgnoreCase("yes")) {

				errors = new ActionErrors();

				List subProjectTypes = LookupAgent.getSubProjectTypes();
				request.setAttribute("subProjectTypes", subProjectTypes);

				if (lsoId == 1) {
					// if errors exist, then forward to the input page.
					logger.debug("Address did not match");
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.address.mismatch"));
					saveErrors(request, errors);

				}

				if (!errors.empty()) {

					logger.debug("Errors exist, going back to the Address page");
					logger.debug("Input Is " + mapping.getInput());
					return (new ActionForward(mapping.getInput()));
				} else {

					logger.debug("Entered into insert Function");
					// onlinePermitAgent.addTempAddress(lsoId,streetNumber,streetFraction,streetId,unitNumber);
					nextPage = "success";

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception occured " + e.getMessage());
		}
		return (mapping.findForward(nextPage));
	}

}
