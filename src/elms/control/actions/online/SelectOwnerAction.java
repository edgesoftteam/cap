package elms.control.actions.online;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.Operator;
import elms.agent.OnlineAgent;
import elms.control.beans.online.ApplyOnlinePermitForm;
import elms.security.User;
import elms.util.StringUtils;

public class SelectOwnerAction extends Action {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(SelectOwnerAction.class.getName());
	String nextPage = "success";
	ActionErrors errors = null;
	int lsoId = 0;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		ApplyOnlinePermitForm applyOnlinePermitForm = (ApplyOnlinePermitForm) form;

		HttpSession session = request.getSession();
		OnlineAgent onlinePermitAgent = new OnlineAgent();

		String emailAddr = ((User) session.getAttribute(elms.common.Constants.USER_KEY)).getFirstName();

		int tempOnlineID = StringUtils.s2i((String) request.getParameter("tempOnlineID"));
		int selectPeopleType = applyOnlinePermitForm.getSelectOwnerType();

		List tempOnlineDetails = new ArrayList();

		try {
			applyOnlinePermitForm.setTempOnlineID(tempOnlineID);
			
			
			if(selectPeopleType<=0){
				List al = onlinePermitAgent.getTempOnlineDetails(tempOnlineID);
				if(al.size()>0){
					  
						ApplyOnlinePermitForm a = (ApplyOnlinePermitForm) al.get(0);
						logger.info(a.getUrl()+"***VALUE");
						URL u = new URL("http://localhost:8080"+a.getUrl());
						selectPeopleType = Operator.toInt(onlinePermitAgent.splitQuery(u).get("sName"));
						logger.info(selectPeopleType+"***selectPeopleType");
						applyOnlinePermitForm.setSelectPeopleType(selectPeopleType);
					}
			}

			
			
			if (selectPeopleType == 1) {// i am the owner
				nextPage = "iOwner";
			} else if (selectPeopleType == 2) {// select owner
				nextPage = "selectOwner";
				applyOnlinePermitForm.setChkExistPeopleFlag("Y");

			} else {
				logger.error("No Mapping exists for next page ");
			}

			
			StringBuilder sb = new StringBuilder();
			sb.append(request.getRequestURI());
			if(Operator.hasValue(request.getQueryString())){
				sb.append("?");
				sb.append(request.getQueryString());
			}
			
			String stepUrl = Operator.replace(sb.toString(), request.getContextPath(), "");
			onlinePermitAgent.updateStepUrl(tempOnlineID, stepUrl);
			
			// left side details bar display
			tempOnlineDetails = onlinePermitAgent.getTempOnlineDetails(tempOnlineID);
			request.setAttribute("tempOnlineDetails", tempOnlineDetails);

		} catch (Exception e) {
			logger.error("Exception occured " + e.getMessage());
		}
		return (mapping.findForward(nextPage));
	}

}
