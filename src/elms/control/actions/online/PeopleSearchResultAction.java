package elms.control.actions.online;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.OnlineAgent;
import elms.control.beans.online.ApplyOnlinePermitForm;
import elms.util.StringUtils;

public class PeopleSearchResultAction extends Action {
	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(PeopleSearchResultAction.class.getName());
	String nextPage = "success";
	ActionErrors errors = null;
	int lsoId = 0;
	String peopleType = null;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		ApplyOnlinePermitForm applyOnlinePermitForm = (ApplyOnlinePermitForm) form;

		logger.debug("entered PeopleSearchResultAction with  ");

		int tempOnlineID = StringUtils.s2i((String) request.getParameter("tempOnlineID"));

		// String createNewFlag = StringUtils.nullReplaceWithEmpty(request.getParameter("createNewFlag"));
		applyOnlinePermitForm.setTempOnlineID(tempOnlineID);
		OnlineAgent onlinePermitAgent = new OnlineAgent();

		String chkExistPeopleFlag = applyOnlinePermitForm.getChkExistPeopleFlag();

		String searchEntry = applyOnlinePermitForm.getSearchEntry();
		logger.debug("Got searchEntry as " + applyOnlinePermitForm.getSearchEntry());

		String searchBased = applyOnlinePermitForm.getSearchBased();
		logger.debug("Got searchBased as " + applyOnlinePermitForm.getSearchBased());

		String peopleType1 = (String)request.getParameter("peopleType");
	 	System.out.println("peopleType1 is  :::   "+peopleType1);
		try {

//			peopleType = onlinePermitAgent.getPeopleType(tempOnlineID);
			request.setAttribute("peopleType", peopleType1);
			List peopleSearchList = new ArrayList();

			peopleSearchList = onlinePermitAgent.getSearchResults(searchEntry, searchBased, peopleType1);

			request.setAttribute("peopleSearchList", peopleSearchList);
			request.setAttribute("tempOnlineID", StringUtils.i2s(tempOnlineID));
			nextPage = "success";

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception occured " + e.getMessage());
		}
		return (mapping.findForward(nextPage));
	}
}
