package elms.control.actions.online;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.InspectionAgent;
import elms.util.StringUtils;

public class EmailInspectionScheduleAction extends Action {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(EmailInspectionScheduleAction.class.getName());
	String nextPage = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		try {
			// InspectionForm frm = (InspectionForm)form ;
			// ViewAllInspectionForm frm = (ViewAllInspectionForm) form;

			int insId = StringUtils.s2i((String) request.getParameter("inspectorId"));
			// int insId = StringUtils.s2i(frm.getInspector());
			boolean res = new InspectionAgent().findInspectionForEmail(insId);

			request.setAttribute("message", "Email sent Sucessfully");
			RequestDispatcher rd = this.getServlet().getServletContext().getRequestDispatcher("inspectionRoutingAction.do?inspectorId=" + insId);

			rd.forward(request, response);

			return null;
		} catch (Exception e) {
			logger.error("Exception occured while emailing inspection schedule " + e.getMessage());
			return (mapping.findForward("error"));
		}
		// logger.debug("returning");
		// return (mapping.findForward("success"));

	}
}
