package elms.control.actions.online;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.agent.UserAgent;
import elms.common.Constants;
import elms.control.beans.LogonForm;
import elms.exception.UserNotFoundException;
import elms.security.User;
import elms.util.StringUtils;
import elms.util.jcrypt;

public class OnlineLogonAction extends Action {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(OnlineLogonAction.class.getName());
	String nextPage = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		logger.debug("In logon action... ");
		ActionErrors errors = new ActionErrors();
		String action = (String) request.getParameter("action");
		logger.debug("action... "+action);
		try {

			LogonForm logonForm = new LogonForm();

			if (action != null && action.equalsIgnoreCase("change")) {
				logger.debug("Inside Change");

				String pass = changePassword(request, logonForm,
						((elms.security.User) request.getSession().getAttribute("user")));
				if (!pass.equals("logon") && new AdminAgent().oldPwdCheck(
						((elms.security.User) request.getSession().getAttribute("user")),
						logonForm.getPassword()) == false) {
					request.setAttribute("error",
							"The password you entered is incorrect. If you've forgotten your password, please use the Forgot Password assistance page and enter your e-mail to recover your password.");
					errors.clear();
					return (mapping.findForward("change"));
				}

				if (pass.equals("logon")) {
					return (mapping.findForward(pass));
				}

			}  
			User user = null;
			String username ="";
			UserAgent userAgent = new UserAgent();
			String encryptedPassword="";
			HttpSession session = request.getSession();
			String responseAction= StringUtils.nullReplaceWithEmpty((String)session.getAttribute("responseAction"));
			System.out.println(" responseAction **********      "+responseAction);
			if(responseAction.equalsIgnoreCase("payment"))
			{  
				user = (User) session.getAttribute("user");	
				if(user ==null)
				{
				user =((elms.security.User)session.getAttribute(elms.common.Constants.USER_KEY));
				}   
    		    System.out.println("Hi hello finally redirecting");
    		    user = userAgent.getOnlineUser(user, (user.getUsername()+""));  
				username = user.getUsername();
				logonForm.setUsername(username);
				logger.debug("username from payment is " + username);
				encryptedPassword = user.getPassword();
				logonForm.setPassword(encryptedPassword);
				//encryptedPassword=encryptedPassword;  
				logger.debug("password is " + encryptedPassword);
       	}else{  
       		 logonForm = (LogonForm) form;   
			 username = logonForm.getUsername();
			logger.debug("username is " + username);
			
			encryptedPassword = jcrypt.crypt("X6", logonForm.getPassword());
       	 }
			if (username == null || encryptedPassword == null) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.onlineusername.required"));
				saveErrors(request, errors);
				return (new ActionForward(mapping.getInput()));

			}
			if (username.equalsIgnoreCase("edge")) {
				logger.debug("username is Administrator");
				if (encryptedPassword.equals(Constants.SUPER_USER_PASSWORD)) {
					user = new User();
					user.setActive("Y");
					user.setUsername("Admin");
					session.setAttribute(Constants.USER_KEY, user);
				} else {
					logger.error("Wrong super admin password");
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.onlineadminPassword.wrong"));
					saveErrors(request, errors);
					return (new ActionForward(mapping.getInput()));
				}
			} else {

				try {
					user = new AdminAgent().getOnlineUser(user, username);
					if (user == null)
						throw new UserNotFoundException("user not found ");
				} catch (UserNotFoundException u) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.username.doesNotExist"));
					saveErrors(request, errors);
					return (new ActionForward(mapping.getInput()));
				}
				if (user.getActive().equalsIgnoreCase("N")) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.username.register.notActive"));
					saveErrors(request, errors);
					return (new ActionForward(mapping.getInput()));
				}
				if (!user.getPassword().equals(encryptedPassword)) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.onlinepassword.mismatch"));
					saveErrors(request, errors);
					return (new ActionForward(mapping.getInput()));
				}

			}
			session.setAttribute(Constants.USER_KEY, user);
			logger.debug("user object put in the session");

			// these are required for the menu navigation
			if (!errors.empty()) {
				logger.debug("Errors exist, going back to the login page");
				saveErrors(request, errors);

				return (new ActionForward(mapping.getInput()));
			}

			if (logonForm.isChangePassword()) {
				return (mapping.findForward("change"));
			}

			return (mapping.findForward("success"));

		} catch (Exception e) {
			logger.error("Exception occured while online logon " + e.getMessage());
			return (mapping.findForward("error"));
		}

	}

	public String changePassword(HttpServletRequest request, LogonForm lf, User user) {
		ActionErrors errors = new ActionErrors();
		logger.debug("Inside Change ");

		if (lf.getPassword() != null && lf.getNewPassword() != null && lf.getConfirmPassword() != null
				&& !lf.getNewPassword().equals("") && !lf.getConfirmPassword().equals("")) {
			logger.debug("****pass" + lf.getPassword());
			String password = jcrypt.crypt("X6", lf.getPassword());
			logger.debug("****pass" + password);
			logger.debug("****user.getPassword()" + user.getPassword());
			logger.debug("****lf.getNewPassword()" + lf.getNewPassword());
			if (!password.equalsIgnoreCase(user.getPassword())) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.onlinepassword.mismatch"));
				saveErrors(request, errors);
				logger.debug("Inside Change 1 ");
				return "change";

			}

			if (lf.getNewPassword() != null && lf.getNewPassword().length() < 5
					|| (lf.getNewPassword().equalsIgnoreCase(lf.getPassword()))) {
				logger.debug("Inside Change 3 ");
				return "change";
			}

			if (!lf.getNewPassword().equalsIgnoreCase(lf.getConfirmPassword())) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.newpassword.mismatch"));
				saveErrors(request, errors);
				logger.debug("Inside Change 2 ");
				return "change";
			}
			boolean result = new AdminAgent().updateOnlinePwd(user, lf.getNewPassword());
			logger.debug("****user.getPassword()" + result);
			request.getSession().setAttribute("message", "Password changed sucessfully");
			return ("logon");
		}
		errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.allValues.ok"));
		saveErrors(request, errors);
		return null;

	}

}
