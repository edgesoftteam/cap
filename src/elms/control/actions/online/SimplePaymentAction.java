package elms.control.actions.online;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.agent.LookupAgent;
import elms.agent.OnlineAgent;
import elms.common.Constants;
import elms.control.beans.EmailDetails;
import elms.control.beans.PaymentMgrForm;
import elms.control.beans.online.CartDetailsForm;
import elms.control.beans.online.MyPermitForm;
import elms.security.User;
import elms.util.EmailSender;
import elms.util.StringUtils;

public class SimplePaymentAction extends Action {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(SimplePaymentAction.class.getName());
	
	

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		ActionErrors errors = new ActionErrors();
		PaymentMgrForm paymentMgrForm = (PaymentMgrForm) form;
		logger.debug("Entered Into SimplePaymentAction... "+paymentMgrForm.getEmail());
		logger.debug("Entered Into SimplePaymentAction... "+paymentMgrForm.getActNum());
		logger.debug("getAction"+paymentMgrForm.getAction());	
		String email = paymentMgrForm.getEmail();
		if(email == null) {
			paymentMgrForm.setEmail(request.getParameter("emailAddr"));
			email = request.getParameter("emailAddr");
		}
		logger.debug("email... "+email);
		String actNbr = paymentMgrForm.getActNum() ;
		if(actNbr == null) {
			actNbr =request.getParameter("actNbr");
		}
		logger.debug("actNbr... "+actNbr);
		int code =0;
		
		String action =request.getParameter("action");
		logger.debug("action... "+action);
		logger.debug("action... "+request.getAttribute("action"));
	
		String message = "";
		String nextPage="success";
		OnlineAgent onlineAgent = new OnlineAgent();
		List actList = new ArrayList<>();
		String displayMsg="";
		List<CartDetailsForm> cartDetailsForms = new ArrayList<CartDetailsForm>();
		CartDetailsForm cartDetailsForm = new CartDetailsForm();
		String cartId=null;
		String totalamnt="0.00";
		HttpSession session = request.getSession();
		User user = null;
		String statusMsg="";
		List<Integer> actIds = new ArrayList<Integer>();
		List<Integer> actIdsAddedToCart = new ArrayList<Integer>();
		
		try {
			cartId = onlineAgent.getCartId(email);
			actList = onlineAgent.getComboActList(email);
			logger.debug(""+actList.size());
			if(action != null && action.equalsIgnoreCase("initialOne") && email != null) {
				for (int i = 0; i < actList.size(); i++) {
					actIds.add(StringUtils.s2i(((MyPermitForm) actList.get(i)).getActivityId()));
				}
				logger.debug("actIds::" + actIds.toString());
				if ((email) != null) {
					cartId = onlineAgent.getCartId(email);
					if (cartId != null) {
						actIdsAddedToCart = onlineAgent.getActIdsAddedToCart(cartId);
					}
				}
				logger.debug("actIdsAddedToCart::" + actIdsAddedToCart.toString());
				// Removing CART Details for permits which are not displaying for logged in user.(ReadyToPay flag having 'N')
				if (actIdsAddedToCart != null)
					for (int i = 0; i < actIdsAddedToCart.size(); i++) {
						if (actIds == null || !(actIds.contains(actIdsAddedToCart.get(i)))) {
							displayMsg = onlineAgent.removeCartDetails(StringUtils.i2s(actIdsAddedToCart.get(i)),
									cartId);
						}
					}
				onlineAgent.updateCartInfoBasedOnActivities(cartId,actList);
				onlineAgent.updateCartInfoInitially(cartId);
				onlineAgent.updateCartStatus(cartId);
				user = (User) onlineAgent.saveUserIfnotExistBasedonEmailAddr(email);
				onlineAgent.updateEmailVerified(email);
				message = "User added for this Email Address";
				session.setAttribute(Constants.USER_KEY, user);
				logger.debug("user object put in the session");

				request.setAttribute("message", message);
			}
			if(action != null && action.equalsIgnoreCase("emailCheck")) {
				if(email != null) {
					EmailSender emailSender = new EmailSender();
					code=onlineAgent.generateRandomDigits(8);
					logger.debug("code "+code);
					EmailDetails emailDetails = new EmailDetails();	
					
	    			emailDetails.setSecurityCode(code);
	    			if(LookupAgent.getKeyValue(Constants.PROD_SERVER_FLAG).equalsIgnoreCase("N")) {
	    				emailDetails.setEmailId(LookupAgent.getKeyValue(Constants.EDGESOFT_SUPPORT_EMAIL));
	    			}else if(LookupAgent.getKeyValue(Constants.PROD_SERVER_FLAG).equalsIgnoreCase("Y")) {
		    			emailDetails.setEmailId(email);	
	    			}
	    			emailDetails.setLkupSystemDataMap(onlineAgent.getLkupSystemDataMap());
	    			emailDetails.setEmailTemplateAdminForm(onlineAgent.getEmailData(emailDetails,Constants.SECURITY_CODE_VERIFICATION));
		        	emailSender.sendEmail(emailDetails);
	    			statusMsg="Email sent successfully.";
				}
				
				logger.debug(code);
				logger.debug(statusMsg);
				PrintWriter pw = response.getWriter();
				pw.write(statusMsg + "," + code ); 

				request.setAttribute("paymentMgrForm", paymentMgrForm);
				request.setAttribute("actList", actList);
				request.setAttribute("emailAddr", email);
				request.setAttribute("cartDetailsForm", cartDetailsForm);
			return null;
		}	
		if(action != null && action.equalsIgnoreCase("addPermit") && actNbr != null) {
			int actId = new AdminAgent().getActivityId(actNbr);
			if(actId>0){
			int actPeopleId=onlineAgent.checkActNo(actNbr,email);
			logger.debug("actPeopleId... "+actPeopleId);
			if(actPeopleId == 0) {
				displayMsg= onlineAgent.checkAndAddPermitToEmailAddr(actNbr, email);
				actList = onlineAgent.getComboActList(email);
				logger.debug(""+actList.size());
				statusMsg = displayMsg;
				request.setAttribute("displayMsg", displayMsg);
			}else if(actPeopleId == -1){
				String department = onlineAgent.getDeptDescriptionForActId(actId);
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.mypermits.notenabledforpayment",department));	
			}else if(actPeopleId == -2){
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.mypermits.feeamountzero"));
			}else {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.mypermits.permitalreadytagged"));	
			}
			logger.debug(statusMsg);
			displayMsg=statusMsg;
			}else{
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.mypermits.validpermitnumber"));
			}
		}
		if(!errors.empty()){
			logger.debug("error are added");
			saveErrors(request, errors);
		}
		if(action != null && action.equalsIgnoreCase("removePermit") && actNbr != null) {
			if(request.getParameter("email") != null && request.getParameter("levelId") != null) {
//				cartId = onlineAgent.getCartId(email);
				displayMsg = onlineAgent.removeActivity(onlineAgent.getPeopleID(request.getParameter("email")), request.getParameter("levelId"),request.getParameter("email"));
				if(cartId != null) {
					String cartMsg= onlineAgent.removeCartDetails( request.getParameter("levelId"), cartId);
				}
			}
		}
			if(email != null) {
				cartDetailsForm = onlineAgent.getCartInfo(cartId);
				cartDetailsForm.setCartId(cartId);
				request.setAttribute("cartDetailsForm", cartDetailsForm);
			}
			if(cartId != null) {
				cartDetailsForms = onlineAgent.getCartDetails(cartId);
//				cartDetailsForm = onlineAgent.getCartInfo(cartId);
				for(int i=0;i<cartDetailsForms.size();i++) {
					CartDetailsForm cartDetailsForm1 = new CartDetailsForm();
					cartDetailsForm1= cartDetailsForms.get(i);
					cartId = cartDetailsForm1.getCartId();
					totalamnt = cartDetailsForm1.getTotalFeeAmount();
				}
			}

			logger.debug("nextPage... "+nextPage);
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.debug(""+actList.size());
		request.setAttribute("displayMsg", displayMsg);
		request.setAttribute("paymentMgrForm", paymentMgrForm);
		request.setAttribute("actList", actList);
		request.setAttribute("emailAddr", email);
		request.setAttribute("cartDetailsForm", cartDetailsForm);
		request.setAttribute("cartDetailsForms", cartDetailsForms);
		request.setAttribute("totalamnt", totalamnt);
		request.setAttribute("cartId", cartId);

		session.setAttribute("user", session.getAttribute("user"));		
		return (mapping.findForward(nextPage));
	}
}
