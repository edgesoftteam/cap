package elms.control.actions.online;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.OnlineAgent;
import elms.common.Constants;

public class UpdateCartLockAction extends Action{
	static Logger logger = Logger.getLogger(UpdateCartLockAction.class.getName());
	
	@Override
	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		
		String cartId = request.getParameter("cartId");
		logger.debug("cartId in UpdateLockCartAction = "+cartId);
		String action = request.getParameter("action");
		logger.debug("action in UpdateLockCartAction = "+action);
		
		response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        PrintWriter pw = response.getWriter();       
		
		OnlineAgent onlineAgent = new OnlineAgent();		
		try{
			if(action.equals(Constants.UPDATE_CART_LOCK)){
				onlineAgent.updateCartLock(cartId, "Y");
				//pw.write(onlineAgent.checkCartLockedOrNot(cartId));	
			}else if(action.equals(Constants.CHECK_ID_EXIST_OR_NOT)){	
				long levelId = Long.parseLong(request.getParameter("levelId"));		
				pw.write(onlineAgent.checkCartLockedOrNot(cartId, levelId));
			}else{
				onlineAgent.updateCartLock(cartId, "N");
				//pw.write(onlineAgent.checkCartLockedOrNot(cartId));				
			}
		}catch(Exception e){
			logger.error("Error while updating cart lock flag "+e.getMessage());
			e.printStackTrace();
		}		
		return null;
	}
}
