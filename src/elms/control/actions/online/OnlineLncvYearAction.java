package elms.control.actions.online;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.OnlineAgent;
import elms.app.people.People;
import elms.util.StringUtils;

public class OnlineLncvYearAction extends Action {
	static Logger logger = Logger.getLogger(OnlineLncvAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		String forward ="success";
		HttpSession session = request.getSession();
		String email = ((elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY)).getUsername();
		OnlineAgent oPAgent = new OnlineAgent();
		try {
			//check if license and vehicle no exists
			boolean checkVehicleStatus = oPAgent.checkVehicleNo(email);
			request.setAttribute("checkVehicleStatus", StringUtils.b2s(checkVehicleStatus));
			
			
			
			int activityId = Integer.parseInt((String) (request.getParameter("activityId")!=null? request.getParameter("activityId"):"0"));
			
			activityId= oPAgent.getActNextYearId(email);
			int dtcount = StringUtils.s2i(new ActivityAgent().getLncvCount(activityId));
			
			
			
			request.setAttribute("dtcount", StringUtils.i2s(dtcount));
			
			String action = (String) (request.getParameter("action")!=null? request.getParameter("action"):"");
			//online list
			if(action.equalsIgnoreCase("onlinePrint")){
				activityId = Integer.parseInt((String) (request.getParameter("activityId")!=null? request.getParameter("activityId"):"0"));
				
				logger.debug("***********************Online my profile print section"+activityId);
				String lncvList = new ActivityAgent().getLNCVCount(activityId);
				request.setAttribute("lncvList", lncvList);
				
				List lncvCurrentList = new ActivityAgent().getLNCVCurrentList(activityId);
				request.setAttribute("lncvCurrentList", lncvCurrentList);
				
				List lncvPreviousList = new ActivityAgent().getLNCVPreviousList(activityId);
				request.setAttribute("lncvPreviousList", lncvPreviousList);
				
				request.setAttribute("activityId", StringUtils.i2s(activityId));
				forward= "successPermitPrint";
				
			}else {
			
			// check paymentLess then 48Hrs
			//boolean payless48 = false;
			String exisitingDt = "";
			String btn ="Next";
			
			if(activityId>0){
				//payless48 = oPAgent.payLess48Hrs(activityId);
				exisitingDt = oPAgent.checkExisitingDatesNextYear(activityId);
				int total = oPAgent.getFeeUnits(activityId);
				if(total==96){
					btn="Process";
				}
			}
			//request.setAttribute("payless48", StringUtils.b2s(payless48));
			request.setAttribute("actId", StringUtils.i2s(activityId));
			request.setAttribute("exisitingDt", exisitingDt);
			request.setAttribute("btn", btn);
			
			
			
			logger.debug("action************"+action+"*****"+activityId);
			if(action.equals("pay")){
				int lncvCount = 0;
				int totalSelectedDates =0;
				String selectedDtForPrint ="";
				int pblkcount=0;
				HashSet s = new HashSet();
				List lncvDtList = new ArrayList();
				int spcount = StringUtils.s2i((String) (request.getParameter("spcount")!=null? request.getParameter("spcount"):"0"));
				logger.debug("spcount************"+spcount);
					if(spcount!=0){
						int plncv = new ActivityAgent().getPLncvDtBlock(activityId);
							for(int i=dtcount;i<= 96;i++){
								String lncvdt= (String) (request.getParameter("lncvDate"+i+""))!=null? (request.getParameter("lncvDate"+i+"")):"";
								//logger.debug("lncvdt before "+lncvdt);
								String pblk= (String) (request.getParameter("pblk"+i+""))!=null? (request.getParameter("pblk"+i+"")):"";
								
									if(!lncvdt.equalsIgnoreCase("")){
										//logger.debug("lncvdt :: i"+lncvdt+"   i:: "+i );
										if(!pblk.equalsIgnoreCase("")){
											//plncv = StringUtils.s2i(pblk);
											plncv++;
											
											pblkcount++;
										}
										
										logger.debug("lncvdt :: pblk"+lncvdt+"   pblk:: "+plncv );
										People people = new People();
										people.setDate(lncvdt);
										people.setVersionNumber(plncv);
										
										lncvDtList.add(people);
										lncvCount++;
										
										// for print if already paid
										totalSelectedDates = i;
										selectedDtForPrint += lncvdt + ",";
									}
							}
					}
					logger.debug("lncvCount"+lncvCount);
					logger.debug("*****pblkcount***** "+pblkcount);

					
					if(lncvCount >0){
						int k = lncvCount % 3 ;
						if(k==1){
							lncvCount = lncvCount + 2;
							k = lncvCount % 3;
						}
						if(k==2){
							lncvCount = lncvCount + 1;
							k = lncvCount % 3;
						}
						if(k==0){
							lncvCount = lncvCount / 3;
							//paymentAmt = paymentAmt * lncvCount;
						
						}
						int paymentAmt = 5;
						paymentAmt= paymentAmt * pblkcount;
						
						String  payfull = (String)(request.getParameter("payfull")!=null? request.getParameter("payfull"):"N");
						logger.debug("****payfull*****"+payfull);
						if(payfull.equals("Y")){
							paymentAmt = 160;
						}
						
						int feeUnit = oPAgent.getFeeUnits(activityId);
						logger.debug("****paymentAmt*****"+paymentAmt);
						logger.debug("****feeUnit*****"+feeUnit);
						logger.debug("****totalSelectedDates*****"+totalSelectedDates);
						
						session.setAttribute("printVal",selectedDtForPrint); 
						
						if(totalSelectedDates <= feeUnit){
							if(lncvDtList.size()>0){
							    int pblock = new ActivityAgent().getPBlock(activityId);	
								for(int i=0;i< lncvDtList.size();i++){
									String lncvdt= ((People)lncvDtList.get(i)).getDate();
									if(!lncvdt.equalsIgnoreCase("")){
										//((People)lncvDtList.get(i)).getVersionNumber()
										
										new ActivityAgent().insertPkFullActivity(activityId,lncvdt,pblock);
										
										
									}
								}
							}
							session.setAttribute("activityId",StringUtils.i2s(activityId));
							session.setAttribute("lncvDtList",lncvDtList);
							forward= "successPrint";
						}else {
							request.setAttribute("action","lncvPayment");
							session.setAttribute("lncvamt",StringUtils.i2s(paymentAmt));
							session.setAttribute("activityId",StringUtils.i2s(activityId));
							session.setAttribute("payfull",payfull);
							session.setAttribute("lncvDtList",lncvDtList);
							session.setAttribute("lncvNextYear","Y");
							forward= "successPayment";
						}
						
					}
				
			}
			
			}//online
			
			
			
			logger.info("Exiting OnlineLncvActions");
		
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return (mapping.findForward(forward));
	}
}