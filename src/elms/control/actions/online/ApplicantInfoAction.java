package elms.control.actions.online;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.LookupAgent;
import elms.agent.OnlineAgent;
import elms.app.common.DisplayItem;
import elms.common.Constants;
import elms.control.beans.online.OnlinePermitForm;
import elms.control.beans.online.OnlineRegisterFrom;
import elms.control.beans.online.TempPermitForm;
import elms.security.User;
import elms.util.StringUtils;

public class ApplicantInfoAction extends Action {
	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(ApplicantInfoAction.class.getName());
	String nextPage = "success";
	int lsoId = 0;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.info("inside ApplicantInfoAction");
		OnlinePermitForm addForm = (OnlinePermitForm) form;
		HttpSession session = request.getSession();
		OnlineAgent oPAgent = new OnlineAgent();
		LookupAgent la = new LookupAgent();
		String accNo = ((elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY)).getAccountNbr();
		String email = ((elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY)).getUsername();
		logger.debug("email:   "+email);
		
		String userId= StringUtils.i2s(((elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY)).getUserId());
		String tempId = (String) session.getAttribute("tempId");
		logger.debug("tempId.. "+tempId);
		addForm.setTempId(tempId);
		TempPermitForm tempPermitForm = (TempPermitForm) session.getAttribute("tempPermitForm");
		
		if(tempPermitForm != null){
			try {
				BeanUtils.copyProperties(addForm, tempPermitForm);
				tempId = (String) session.getAttribute("tempId");
				session.removeAttribute("tempPermitForm");
			} catch (IllegalAccessException e) {
				logger.error("Error copying properties" + e);
			} catch (InvocationTargetException e) {
				logger.error("Error copying properties" + e);
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		} 
		
		//request.setAttribute("onlinePermitFrom", addForm);		
		
		if(userId == null || userId.length() == 0) {
			return (mapping.findForward("index"));
		}
		try {
			
			String action = request.getParameter("action");
			logger.debug("action::"+action);
			if(action != null && action.equals("next")){
				
				String isObc = ((elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY)).getIsObc();
//				OnlineRegisterFrom regForm = oPAgent.getOnlineUsers(((User) session.getAttribute(Constants.USER_KEY)).getUsername(), isObc);
				OnlineRegisterFrom regForm = oPAgent.getOnlineUsers(((User) session.getAttribute(Constants.USER_KEY)).getUsername(), isObc,tempId);
				
				//addForm.setUserId(regForm.getUserId());
				addForm.setFirstName(regForm.getFirstName());
				addForm.setLastName(regForm.getLastName());
				addForm.setAddress(regForm.getAddress());
				addForm.setCity(regForm.getCity());
				addForm.setState(regForm.getState());
				addForm.setZip(regForm.getZip());
				addForm.setPhoneNbr(regForm.getPhoneNbr());
				addForm.setPhoneExt(regForm.getPhoneExt());
				addForm.setWorkPhone(regForm.getWorkPhone());
				addForm.setWorkExt(regForm.getWorkExt());
				addForm.setEmailAddress(regForm.getEmailAddress());
//				addForm.setActivityType("Garage Sale");
				//session.setAttribute("onlinePermitForm", addForm);
				request.setAttribute("onlinePermitForm", addForm);
				
			}
			if(action != null && action.equals("reset")) {
				if(tempId ==null)
					tempId = (String) session.getAttribute("tempId");
					oPAgent .deleteTempPermit(tempId);
				addForm.reset1();
				session.removeAttribute("peopleListAll");
				session.removeAttribute("getAttachments");
				session.removeAttribute("levelId");
				addForm = new OnlinePermitForm();
				
				request.setAttribute("streetNameList", la.getStreetNames());
				request.setAttribute("onlinePermitFrom", addForm);
				request.setAttribute("error", "Permit has been cancelled successfully");				
				logger.debug("Action...:"+action);
				return mapping.findForward("prev");
			}
			else if(action != null && action.equals("apnext")) {
				oPAgent.updateEgovTempWithApplicantInfo(addForm);
//				String desc="Garage Sale permit from:"+" "+addForm.getIssueDate()+" to "+addForm.getEndDate()+" -- "+ addForm.getFirstName()+" "+addForm.getLastName();
//				addForm.setActDescription(desc);
				request.setAttribute("onlinePermitFrom", addForm);
				return (mapping.findForward("success"));
			}else if(action != null && action.equals("update")) {
				logger.debug("update applicant info..");
				/*oPAgent.updateOnlineApplicant(addForm, email);
				oPAgent.updatePeople(email, addForm);*/
				//oPAgent.updatePeople(email, addForm);
				
			}
			else if(action != null && action.equals("prev")) {		
				request.setAttribute("streetNameList", la.getStreetNames());
				request.setAttribute("checkbox", "Yes");
				return mapping.findForward("prev");
				
			}
			/*if (update.equalsIgnoreCase("yes")) {
				

				logger.debug("In updating...  ");
				String obc = addForm.getObc();
				logger.debug("obc is "+obc);
				String dot = addForm.getDot();
				//oPAgent.updateOnlineUser(addForm, email);
				//oPAgent.updatePeople(email, addForm);
				//Updating session
				User user=new User();
				user.setUsername(addForm.getEmailAddress());
				user.setFirstName(addForm.getFirstName());
				user.setLastName(addForm.getLastName());
				user.setIsObc(addForm.getObc());
				user.setIsDot(addForm.getDot());
				session.setAttribute(elms.common.Constants.USER_KEY, user);
				
				String peopleTypes="";
				if(!addForm.getLicenseNbr().equalsIgnoreCase(""))
				{
					peopleTypes= peopleTypes  + "4,";
				}
				peopleTypes="8,2";
	

				if (obc.equals("Y")) {
					String peopleType = oPAgent.checkOBCUser(email, peopleTypes, addForm);
                    
					if (!peopleType.equals("")) {
						
						addForm.setPeopleType(peopleType);
						oPAgent.saveOBCUser(addForm);
					}

				}


			}
*/		/*	String isObc = ((elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY)).getIsObc();
			addForm = oPAgent.getOnlineUser(((User) session.getAttribute(Constants.USER_KEY)).getUsername(), isObc);
			request.setAttribute("peopleTypes", addForm.getPeopleType());
			
			session.setAttribute("onlinePermitForm", addForm);
			request.setAttribute("onlinePermitForm", addForm);*/
			

		} catch (Exception e) {
			logger.error("Exception occured " + e.getMessage());
		}
		return (mapping.findForward("first"));
	}
}
