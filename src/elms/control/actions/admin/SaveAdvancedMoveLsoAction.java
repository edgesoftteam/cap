package elms.control.actions.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.control.beans.admin.AdvancedMoveLsoForm;

public class SaveAdvancedMoveLsoAction extends Action {

	static Logger logger = Logger.getLogger(SaveAdvancedMoveLsoAction.class.getName());
	String nextPage = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		ActionErrors errors = new ActionErrors();
		try {

			logger.debug("entered the saveAdvancedmove lso action");
			HttpSession session = request.getSession();
			AdvancedMoveLsoForm frm = (AdvancedMoveLsoForm) form;
			logger.debug("The form is " + frm);
			String toLsoId = frm.getToLsoId();
			String moveLsoId = frm.getFromLsoId();
			String moveType = frm.getMoveType();

			logger.debug("Move :" + moveType + ": " + moveLsoId + " to " + toLsoId);

			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("success", "Successfully Completed"));
			saveErrors(request, errors);
			request.setAttribute("advancedMoveLsoForm", frm);

			AdminAgent moveLsoAgent = new AdminAgent();
			boolean updno = moveLsoAgent.moveLso(moveType, moveLsoId, toLsoId);

			nextPage = "success";
		} catch (Exception e) {
			logger.error(e.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("failure", e.getMessage()));
			saveErrors(request, errors);
		}

		return (mapping.findForward(nextPage));

	}

}