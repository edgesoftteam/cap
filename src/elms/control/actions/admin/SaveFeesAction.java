package elms.control.actions.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.FinanceAgent;
import elms.agent.LookupAgent;
import elms.app.finance.Fee;
import elms.control.beans.FeesForm;

public class SaveFeesAction extends Action {

	static Logger logger = Logger.getLogger(SaveFeesAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		FeesForm feesForm = (FeesForm) form;

		try {
			boolean done = false;
			// Edited fees are saved here...
			Fee fee = feesForm.getFee();
			fee.setFeeType("test");// i dont know what this is ..need to be
			// modified after change
			FinanceAgent FinanceAgent = new FinanceAgent();
			logger.debug("obtained fee object from fees form");
			done = FinanceAgent.saveFee(fee);
			if (done)
				fee = FinanceAgent.getFee(fee.getFeeId());

			logger.debug("Fee updated successfully");

			// static list of activity types are obtained here
			List activityTypes = LookupAgent.getActivityTypes();
			logger.debug("obtained list of size " + activityTypes.size());
			request.setAttribute("activityTypes", activityTypes);

			/*
			 * selected activity type is taken from fee object and the control forwards to corresponding fees schedule page with modified details. This goes as per the business requirements,This is only to fill up the request based on the page to be forwarded to. if the page nees to forward to somewhere else, the 'selectedActivityType' and fees list may not be required.
			 */

			String selectedActivityType = fee.getActType();
			logger.debug("selected activity type is " + selectedActivityType);
			request.setAttribute("selectedActivityType", selectedActivityType);

			// obtain the list of fees for a selected acivivity type
			List fees = new FinanceAgent().getFees(selectedActivityType);
			logger.debug("obtained a list of fees with size " + fees.size());
			request.setAttribute("fees", fees);

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));
	}

}