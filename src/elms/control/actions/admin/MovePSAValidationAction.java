/**
 * 
 */
package elms.control.actions.admin;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.common.Constants;
import elms.exception.AgentException;

/**
 * @author Gaurav Garg
 * 
 */
public class MovePSAValidationAction extends Action {
	static Logger logger = Logger.getLogger(MovePSAValidationAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.debug(" in MovePSAValidationAction");
		PrintWriter pw = response.getWriter();
		AdminAgent moveProjectAgent = new AdminAgent();
		String action = request.getParameter("action");
		if (action != null && action.equalsIgnoreCase("projAndLSO")) {
			String projectNbr = request.getParameter("projectNbr");
			String lsoId = request.getParameter(Constants.LSO_ID);
			try {
				int projId = moveProjectAgent.getProjectId(projectNbr);
				int addressId = moveProjectAgent.getAddressId(lsoId);
				pw.write(projId + "@&@" + addressId);
			} catch (AgentException e) {
				throw new ServletException("", e);
			}
		} else if (action != null && action.equalsIgnoreCase("sProjAndProj")) {
			String projectNbr = request.getParameter("projectNbr");
			String subProjectNbr = request.getParameter("sProjectNbr");
			try {
				int projId = moveProjectAgent.getProjectId(projectNbr);
				int subProjectId = moveProjectAgent.getSubProjectId(subProjectNbr);
				pw.write(projId + "@&@" + subProjectId);
			} catch (Exception e) {
				throw new ServletException("", e);
			}
		} else if (action != null && action.equalsIgnoreCase("activityAndSProj")) {
			String activityNbr = request.getParameter("activityNbr");
			String subProjectNbr = request.getParameter("sProjectNbr");
			try {
				int actId = moveProjectAgent.getActivityId(activityNbr);
				int subProjectId = moveProjectAgent.getSubProjectId(subProjectNbr);
				pw.write(actId + "@&@" + subProjectId);
			} catch (Exception e) {
				throw new ServletException("", e);
			}
		}
		return null;
	}
}
