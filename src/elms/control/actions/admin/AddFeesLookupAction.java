package elms.control.actions.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.control.beans.AddLookupFeesForm;
import elms.control.beans.LookupFeeEdit;

public class AddFeesLookupAction extends Action {

	static Logger logger = Logger.getLogger(AddFeesLookupAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		// Extract attributes and parameters we will need

		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		logger.info("Entering  AddFeesLookupAction");
		try {

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}

			LookupFeeEdit[] aryLookupFee = { new LookupFeeEdit() };

			ActionForm frm = new AddLookupFeesForm();
			AddLookupFeesForm frmFee = (AddLookupFeesForm) frm;

			frmFee.setLkupFeeEditList(aryLookupFee);
			session.setAttribute("addLookupFeesForm", frmFee);

		} catch (Exception e) {
			logger.error("Error in AddFeesLookupAction" + e.getMessage());
		}
		return (mapping.findForward("success"));
	}

} // End class