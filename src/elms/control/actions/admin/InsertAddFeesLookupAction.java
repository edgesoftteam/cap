package elms.control.actions.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.control.beans.AddLookupFeesForm;
import elms.control.beans.LookupFeeEdit;
import elms.util.StringUtils;

public class InsertAddFeesLookupAction extends Action {

	static Logger logger = Logger.getLogger(InsertAddFeesLookupAction.class.getName());

	boolean insertRowTrue;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		// Extract attributes and parameters we will need

		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		insertRowTrue = StringUtils.s2b((String) request.getAttribute(StringUtils.b2s(insertRowTrue)));

		logger.info("Entering InsertAddFeesLookupAction");
		try {

			AddLookupFeesForm frmFee = (AddLookupFeesForm) session.getAttribute("addLookupFeesForm");
			LookupFeeEdit[] lst = frmFee.getLkupFeeEditList();

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}

			LookupFeeEdit[] aryLookupFee = new LookupFeeEdit[lst.length + 1];
			for (int i = 0; i < lst.length; i++) {
				aryLookupFee[i] = lst[i];
			}
			aryLookupFee[lst.length] = new LookupFeeEdit();

			insertRowTrue = true;
			logger.debug("######### " + insertRowTrue);

			frmFee.setLkupFeeEditList(aryLookupFee);
			request.setAttribute("insertRowTrue", StringUtils.b2s(insertRowTrue));
			request.setAttribute("lst.length", ((lst.length + 1) + ""));
			session.setAttribute("addLookupFeesForm", frmFee);

		} catch (Exception e) {
			logger.error("Error in InsertAddFeesLookupAction" + e.getMessage());
		}
		return (mapping.findForward("success"));
	}

} // End class