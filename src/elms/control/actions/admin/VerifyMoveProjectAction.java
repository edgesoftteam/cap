package elms.control.actions.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.control.beans.MoveProjectForm;
import elms.control.beans.MoveVerifyForm;

//import sun.jdbc.rowset.CachedRowSet;

public class VerifyMoveProjectAction extends Action {

	static Logger logger = Logger.getLogger(VerifyMoveProjectAction.class.getName());
	String nextPage = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		// CachedRowSet rs = null;

		MoveProjectForm moveProjectForm = (MoveProjectForm) form;
		try {

			String projectNbr = moveProjectForm.getPprojectno();
			logger.debug("Got Project Nbr from form " + projectNbr);
			String lsoId = moveProjectForm.getPlsono();
			logger.debug("Got Lso No from form " + lsoId);

			session.setAttribute("projectNbr", projectNbr);
			session.setAttribute("lsoId", lsoId);

			MoveVerifyForm verifyForm = new MoveVerifyForm();
			AdminAgent moveProjectAgent = new AdminAgent();

			verifyForm = moveProjectAgent.getProject(projectNbr, verifyForm);
			verifyForm = moveProjectAgent.getLso(lsoId, verifyForm);
			session.setAttribute("moveVerifyForm", verifyForm);

			nextPage = "success";

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward(nextPage));

	}

}
