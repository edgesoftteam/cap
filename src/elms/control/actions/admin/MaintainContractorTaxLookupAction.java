package elms.control.actions.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.FinanceAgent;
import elms.control.beans.LookupFeesForm;
import elms.security.User;

public class MaintainContractorTaxLookupAction extends Action {

	static Logger logger = Logger.getLogger(MaintainContractorTaxLookupAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.debug("Entered MaintainContractorTaxLookupAction ");
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		LookupFeesForm lookupFeesForm = (LookupFeesForm) form;
		FinanceAgent feeAgent = new FinanceAgent();
		List taxList = new ArrayList();
		try {

			List atList = new ArrayList();

			if (request.getParameter("save") != null) {
				try {
					feeAgent.saveBusTax(lookupFeesForm, user);
					lookupFeesForm = new LookupFeesForm();
					request.setAttribute("message", "Contractor Tax Details saved successfully.");
				} catch (Exception e) {
					request.setAttribute("error", "Error while saving Contractor Tax Details.");
				}

			}
			lookupFeesForm = new LookupFeesForm();
			taxList = feeAgent.getBusTaxList(lookupFeesForm);
			logger.debug("lookup" + taxList.size());
			lookupFeesForm.setTaxList(taxList);

			request.setAttribute("lookupFeesForm", lookupFeesForm);

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));
	}

} // End class