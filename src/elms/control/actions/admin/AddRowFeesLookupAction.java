package elms.control.actions.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.control.beans.LookupFeeEdit;
import elms.control.beans.LookupFeesForm;
import elms.util.StringUtils;

public class AddRowFeesLookupAction extends Action {

	static Logger logger = Logger.getLogger(AddRowFeesLookupAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		// Extract attributes and parameters we will need

		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		boolean insertRowTrue;
		String lookupFeeListSize4AddRow;
		try {
			String lookupId = (String) session.getAttribute("lookupId");
			List lookupFeeList = new ArrayList();

			LookupFeesForm subForm = (LookupFeesForm) session.getAttribute("lookupFeesForm");
			LookupFeeEdit[] lst = subForm.getLkupFeeEditList();

			boolean added = false;
			String insertFlag = "";

			for (int i = 0; i < lst.length; i++) {
				insertFlag = lst[i].getCheck();
				logger.info(i + "-" + lst[i].getCheck() + "-");
				lst[i].setCheck("");
				lookupFeeList.add((LookupFeeEdit) lst[i]);
				if (insertFlag.equals("on")) {
					LookupFeeEdit lookupFee = new LookupFeeEdit();
					lookupFee.setLookupFeeId(lst[i].getLookupFeeId());
					lookupFee.setLookupFee(lst[i].getLookupFee());
					lookupFee.setCreationDate(lst[i].getCreationDate());
					lookupFeeList.add(lookupFee);
					added = true;
				}
			}

			if (added == false) {
				int k = StringUtils.s2i(lst[(lst.length)-1].getHighRange());
				LookupFeeEdit lookupFee = new LookupFeeEdit();
				lookupFee.setLookupFeeId(lst[0].getLookupFeeId());
				lookupFee.setLookupFee(lst[0].getLookupFee());
				lookupFee.setCreationDate(lst[0].getCreationDate());
				lookupFee.setLowRange(StringUtils.i2s(k + 1));
				lookupFeeList.add(lookupFee);
			}

			LookupFeeEdit[] aryLookupFee = new LookupFeeEdit[lookupFeeList.size()];

			for (int i = 0; i < lookupFeeList.size(); i++) {
				aryLookupFee[i] = (LookupFeeEdit) lookupFeeList.get(i);
			}

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}

			ActionForm frm = new LookupFeesForm();
			LookupFeesForm frmFee = (LookupFeesForm) frm;

			try {
				frmFee.setLkupFeeEditList(aryLookupFee);
			} catch (Exception ex) {
				logger.info(ex.getMessage());
			}

			insertRowTrue = true;
			lookupFeeListSize4AddRow = StringUtils.i2s(lookupFeeList.size());

			request.setAttribute("insertRowTrue", StringUtils.b2s(insertRowTrue));
			request.setAttribute("lookupFeeListSize4AddRow", lookupFeeListSize4AddRow);

			session.setAttribute("lookupFeesForm", frmFee);
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
		}
		return (mapping.findForward("success"));
	}

} // End class