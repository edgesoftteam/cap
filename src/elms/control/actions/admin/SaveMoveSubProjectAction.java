package elms.control.actions.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.control.beans.MoveVerifyForm;
import elms.util.StringUtils;

public class SaveMoveSubProjectAction extends Action {

	static Logger logger = Logger.getLogger(SaveMoveSubProjectAction.class.getName());
	String nextPage = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		try {
			ActionErrors errors = new ActionErrors();
			MoveVerifyForm frm = (MoveVerifyForm) session.getAttribute("moveVerifyForm");
			String projectNbr = frm.getProjectNbr();
			logger.debug("Got Project No from session " + projectNbr);
			String subProjectNbr = frm.getSubProjectNbr();
			subProjectNbr = (String) session.getAttribute("subProjectNbr");
			logger.debug("Got SubProject no  from session " + subProjectNbr);

			AdminAgent moveProjectAgent = new AdminAgent();
			String updno = "";

			List subProjecNamesList = new ArrayList();
			String pName = "";
			String pType = "";
			String SubProjName = "";

			/* get sub project Name */
			SubProjName = moveProjectAgent.getsubProjectNameBySPNo(subProjectNbr);
			List pTypeList = moveProjectAgent.getpType(projectNbr);

			if (pTypeList.size() == 0) {
				frm.setMessage("Sub Project cannot be moved.");
			} else {
				pType = pTypeList.get(0).toString();
				pName = pTypeList.get(1).toString();
			}
			/* check whether sub project name starts with BT,BL or RP */
			if (SubProjName.startsWith("BL") || SubProjName.startsWith("BT")) {
				/* check whether project type is at of occupancy level and project name is License and Code Services */
				if (pType.equalsIgnoreCase("O") && pName.equalsIgnoreCase("License and Code Services")) {
					/* move sub project */
					updno = moveProjectAgent.moveSubProject(subProjectNbr, projectNbr);
					if (StringUtils.s2i(updno) == 0 || updno.equals(null)) {
						frm.setMessage("Sub Project cannot be moved.");
					} else
						frm.setMessage("Move completed successfully");
				} else {
					// if errors exist, then forward to the input page.
					logger.debug("The sub project of type  BT or BL  cannot be moved to Land /Structure level ,hence try moving it again. ");
					errors.add("statusCodeExists", new ActionError("statusCodeExists", "The sub project of type  BT or BL  cannot be moved to Land /Structure level ,hence try again. "));
					saveErrors(request, errors);
				}
			} else {
				/* move sub project */
				updno = moveProjectAgent.moveSubProject(subProjectNbr, projectNbr);
				if (StringUtils.s2i(updno) == 0 || updno.equals(null)) {
					frm.setMessage("Sub Project cannot be moved.");
				} else
					frm.setMessage("Move completed successfully");

			}
			session.setAttribute("moveVerifyForm", frm);

			nextPage = "success";
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return (mapping.findForward(nextPage));

	}

}