package elms.control.actions.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.agent.LookupAgent;
import elms.app.admin.SubProjectType;
import elms.app.common.DisplayItem;
import elms.control.beans.admin.SubProjectLookupsForm;
import elms.util.StringUtils;

public class SubProjectTypeAdminAction extends Action {

	static Logger logger = Logger.getLogger(SubProjectTypeAdminAction.class.getName());
	int result = -1;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.info("Entered into SubProjectTypeAdminAction .. ");

		HttpSession session = request.getSession();
		AdminAgent adminAgent = new AdminAgent();
		SubProjectLookupsForm sprojLookupsForm = (SubProjectLookupsForm) form;

		List subProjectTypes = new ArrayList();

		if (request.getParameter("lookup") != null) {
			result = 0;
			sprojLookupsForm.setType(StringUtils.nullReplaceWithEmpty(request.getParameter("type")));
			sprojLookupsForm.setDescription(StringUtils.nullReplaceWithEmpty(request.getParameter("desc")));
			sprojLookupsForm.setSubProjectName(StringUtils.nullReplaceWithEmpty(request.getParameter("subProjName")));
			sprojLookupsForm.setDepartment(StringUtils.nullReplaceWithEmpty(request.getParameter("dept")));

			subProjectTypes = adminAgent.getSubProjectTypes(sprojLookupsForm);

			session.removeAttribute("sprojLookupsForm");
			session.setAttribute("oldsprojLookupsForm", sprojLookupsForm);

			sprojLookupsForm.setSubProjectTypeList(subProjectTypes);

			PrintWriter pw = response.getWriter();
			if (subProjectTypes.size() == 0) {
				pw.write("No Records Found");
			} else {
				pw.write(innerTable(subProjectTypes));
			}
			return null;
		} else if (request.getParameter("id") != null) {
			result = 0;
			SubProjectType subProjectType = adminAgent.getSubProjectType(StringUtils.s2i(request.getParameter("id")));

			sprojLookupsForm.setSubProjectTypeId(subProjectType.getSubProjectTypeId());
			sprojLookupsForm.setType(subProjectType.getSubProjectType());
			sprojLookupsForm.setDescription(subProjectType.getDescription());
			sprojLookupsForm.setSubProjectName(StringUtils.i2s(subProjectType.getPtypeId()));
			sprojLookupsForm.setDepartment(subProjectType.getDepartmentId());

			PrintWriter pw = response.getWriter();
			pw.write(sprojLookupsForm.getType().toString() + ',' + sprojLookupsForm.getDescription().toString() + ',' + sprojLookupsForm.getSubProjectName().toString() + ',' + sprojLookupsForm.getDepartment().toString());
			return null;

		} else if (request.getParameter("save") != null) {

			SubProjectLookupsForm subProjForm = (SubProjectLookupsForm) session.getAttribute("oldsprojLookupsForm");

			int spId = StringUtils.s2i(request.getParameter("subProjTypeId"));
			if (spId <= 0)
				spId = 0;
			sprojLookupsForm.setSubProjectTypeId(spId);

			sprojLookupsForm.setType(StringUtils.nullReplaceWithEmpty(request.getParameter("type")));
			sprojLookupsForm.setDescription(StringUtils.nullReplaceWithEmpty(request.getParameter("desc")));
			sprojLookupsForm.setSubProjectName(StringUtils.nullReplaceWithEmpty(request.getParameter("subProjName")));
			sprojLookupsForm.setDepartment(StringUtils.nullReplaceWithEmpty(request.getParameter("dept")));

			int pTypeId = StringUtils.s2i(sprojLookupsForm.getSubProjectName());
			if (pTypeId <= 0)
				pTypeId = 0;

			result = adminAgent.saveSubProjectType(sprojLookupsForm.getType(), sprojLookupsForm.getDescription(), pTypeId, sprojLookupsForm.getDepartment(), sprojLookupsForm.getSubProjectTypeId());
			PrintWriter pw = response.getWriter();

			if (result == 1) {
				pw.write("Project Type Saved Successfully");
			} else if (result == 2) {
				subProjectTypes = adminAgent.getSubProjectTypes(subProjForm);

				pw.write(innerTable(subProjectTypes));
			} else {
				pw.write("");
			}
			return null;

		} else if (request.getParameter("delete") != null) {

			sprojLookupsForm.setType(StringUtils.nullReplaceWithEmpty(request.getParameter("type")));
			sprojLookupsForm.setDescription(StringUtils.nullReplaceWithEmpty(request.getParameter("desc")));
			sprojLookupsForm.setSubProjectName(StringUtils.nullReplaceWithEmpty(request.getParameter("subProjName")));
			sprojLookupsForm.setDepartment(StringUtils.nullReplaceWithEmpty(request.getParameter("dept")));

			String idList = (String) request.getParameter("checkboxArray");

			result = adminAgent.deleteSubProjectType(idList);

			SubProjectLookupsForm subProjDelForm = (SubProjectLookupsForm) session.getAttribute("oldsprojLookupsForm");
			// session.removeAttribute("oldActTypeForm");

			subProjectTypes = adminAgent.getSubProjectTypes(subProjDelForm);

			PrintWriter pw = response.getWriter();
			pw.write(innerTable(subProjectTypes));
			return null;
		} else {
			sprojLookupsForm = new SubProjectLookupsForm();
			subProjectTypes = new ArrayList();
			sprojLookupsForm.setSubProjectTypes(subProjectTypes);

		}
		session.setAttribute("sprojLookupsForm", sprojLookupsForm);

		List departments;
		List subProjectNames;
		try {
			subProjectNames = LookupAgent.getSubProjectNames("ALL");
			departments = LookupAgent.getDepartmentList();
		} catch (Exception e) {
			subProjectNames = new ArrayList();
			departments = new ArrayList();

			logger.warn("could not find list of subProjectNames, initializing it to a blank arrayList");
			logger.warn("no department list obtained, so initializing it to blank arraylist");
		}
		request.setAttribute("subProjectNames", subProjectNames);
		request.setAttribute("departments", departments);

		return (mapping.findForward("success"));
	}

	public String innerTable(List subprojTypes) {
		String subProjTypeHtml = "";
		if (subprojTypes != null && subprojTypes.size() > 0) {
			StringBuffer sb = new StringBuffer("");
			DisplayItem sprojTypes = null;

			if (subprojTypes != null && subprojTypes.size() > 0) {
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"10\" cellpadding=\"0\" >");
				sb.append("<tr valign=\"top\">");
				sb.append("<td width=\"99%\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
				sb.append("<tr>");
				sb.append("  <td><font class=\"con_hdr_3b\">Sub Project Type List<br>");
				sb.append("<br>");
				sb.append("</font></td></tr><tr>");
				if (result == 2) {
					sb.append("<tr>");
					sb.append("  <td><font class=\"con_hdr_3b\" style=\"color:#088A08\">Record Saved Successfully<br>");
					sb.append("<br>");
					sb.append("</font></td></tr><tr>");
				} else if (result == 3) {
					sb.append("<tr>");
					sb.append("  <td><font class=\"con_hdr_3b\" style=\"color:#088A08\">Record Deleted Successfully<br>");
					sb.append("<br>");
					sb.append("</font></td></tr><tr>");
				}
				sb.append("<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
				sb.append("<tr><td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>");
				sb.append("<td width=\"95%\" height=\"25\" background=\"../images/site_bg_B7C1CB.jpg\"><font class=\"con_hdr_2b\">Sub Project Types</font></td>");
				sb.append("<td width=\"1%\"><img src=\"../images/site_hdr_split_1_tall.jpg\" width=\"26\" height=\"25\"></td>");
				sb.append("<td width=\"1%\" background=\"../images/site_bg_D7DCE2.jpg\"><nobr>");
				sb.append("<input type=\"button\" property=\"delete\" value=\"Delete\" styleClass=\"button\" onclick=\"removeTable()\"/>");
				sb.append("<input type=\"button\" property=\"add\" value=\"Add Type\" styleClass=\"button\" onclick=\"refresh()\"/>");
				sb.append("&nbsp;</nobr></td></tr></table></td></tr>");
				sb.append("<tr>");
				sb.append("<td background=\"../images/site_bg_B7C1CB.jpg\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"2\">");
				sb.append("<tr>");
				sb.append("<td width=\"10%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Delete</font></td>");
				sb.append("<td width=\"15%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Sub Project Type</font></td>");
				sb.append("<td width=\"25%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Description</font></td>");
				sb.append("<td width=\"25%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Sub Project Name</font></td>");
				sb.append("<td width=\"25%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Department</font></td>");
				sb.append("</tr>");

				for (int i = 0; i < subprojTypes.size(); i++) {
					sprojTypes = (DisplayItem) subprojTypes.get(i);

					sb.append("<tr bgcolor=\"#FFFFFF\">");
					if (sprojTypes.getFieldSix().equalsIgnoreCase("Y")) {
						sb.append("<td>");
						sb.append("<input type=\"checkbox\" name=\"selectedMaping\" value=\"" + sprojTypes.getFieldOne() + "\"  onclick=\"checkIds(this)\" >");
						sb.append("</td>");
					} else {
						sb.append("<td>&nbsp;</td>");
					}
					sb.append("<td bgcolor=\"#FFFFFF\">");
					sb.append("<font class=\"con_text_1\"><a href=\"javascript:void(0)\" onclick=\"CreateXmlHttp(" + sprojTypes.getFieldOne() + "," + StringUtils.checkString(sprojTypes.getFieldSix()) + ")\" >");
					sb.append(sprojTypes.getFieldTwo());
					sb.append("</a></font></td>");
					sb.append("<td bgcolor=\"#FFFFFF\"><font class=\"con_text_1\">");
					sb.append(sprojTypes.getFieldThree());
					sb.append("</font></td>");
					sb.append("<td bgcolor=\"#FFFFFF\"><font class=\"con_text_1\">");
					sb.append(sprojTypes.getFieldFour());
					sb.append("</font></td>");
					sb.append("<td bgcolor=\"#FFFFFF\"><font class=\"con_text_1\">");
					sb.append(sprojTypes.getFieldFive());
					sb.append("</font></td>");
					sb.append("</tr>");
				}
				sb.append("</table></td></tr></table></td></tr><tr><td>&nbsp;</td></tr></table></td><td width=\"1%\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td height=\"32\">&nbsp;</td>");
				sb.append("</tr><tr><td>&nbsp;</td></tr></table></td></tr></table>");
			}
			subProjTypeHtml = sb.toString();
			// logger.debug(actTypesHtml);
		}
		return subProjTypeHtml;
	}
}
