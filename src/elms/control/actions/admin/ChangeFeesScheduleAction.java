package elms.control.actions.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.FinanceAgent;
import elms.app.admin.FeeEdit;
import elms.control.beans.FeeScheduleForm;
import elms.control.beans.LookupFeesForm;

public class ChangeFeesScheduleAction extends Action {

	static Logger logger = Logger.getLogger(PreviewFeesLookupAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		MessageResources messages = getResources();
		HttpSession session = request.getSession();

		try {
			List feeList = new ArrayList();

			FeeScheduleForm subForm = (FeeScheduleForm) session.getAttribute("feeScheduleForm");
			FeeEdit[] lst = subForm.getFeeEditList();

			for (int i = 0; i < lst.length; i++) {
				if (lst[i].getEdited().equals("1")) {
					feeList.add(lst[i]);
				}
			}

			boolean success = new FinanceAgent().saveFeeFactors(feeList);
			ActionForm frm = new LookupFeesForm();
			FeeScheduleForm frmFee = new FeeScheduleForm();

			FeeEdit[] aryFee = new FeeEdit[0];
			frmFee.setFeeEditList(aryFee);

			session.setAttribute("feeScheduleForm", frmFee);
			session.setAttribute("activityType", "");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return (mapping.findForward("success"));
	}
}