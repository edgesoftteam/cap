package elms.control.actions.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;

public class YearEndMaintenanceAction extends Action {

	static Logger logger = Logger.getLogger(YearEndMaintenanceAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		try {

			String from = (request.getParameter("from") != null) ? request.getParameter("from") : "";
			if (!(from.equals("menu"))) {

				AdminAgent adminAgent = new AdminAgent();
				String message = adminAgent.yearEndInitialize();
				request.setAttribute("message", message);
			}

			return (mapping.findForward("success"));
		} catch (Exception e) {

			logger.error("problem while performing year end maintenance");

			return (mapping.findForward("error"));
		}
	}
}
