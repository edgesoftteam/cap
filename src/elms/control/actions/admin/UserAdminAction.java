package elms.control.actions.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.agent.LookupAgent;
import elms.control.beans.admin.UserForm;
import elms.util.StringUtils;

public class UserAdminAction extends Action {

	static Logger logger = Logger.getLogger(UserAdminAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.debug("Entered into UserAdminAction .. ");
		HttpSession session = request.getSession();
		ActionErrors errors = new ActionErrors();
		UserForm userForm = (UserForm) form;
		AdminAgent adminAgent = new AdminAgent();
		AdminAgent userAgent = new AdminAgent();

		try {

			if (request.getParameter("lookup") != null) {

				List users = adminAgent.getUsers(userForm);
				userForm = new UserForm();
				userForm.setUsers(users);
				userForm.setDisplayUserList("YES");
			} else if (request.getParameter("save") != null) {

				// get the username from the form
				int userId = adminAgent.saveUser(userForm);
				if (userId == -1) {
					// username already exists, choose a different username.
					logger.debug("username already exists..choose a different username");
					errors.add("usernameExists", new ActionError("error.username.exists"));
					saveErrors(request, errors);
				} else {
					userForm = adminAgent.getUserForm(new AdminAgent().getUser(userId));
					userForm.setDisplayUserList("NO");
					String message = "User Details saved successfully.";
					request.setAttribute("message", message);
				}
			} else if (request.getParameter("id") != null) {

				int userId = StringUtils.s2i(request.getParameter("id"));
				userForm = adminAgent.getUserForm(new AdminAgent().getUser(userId));
				userForm.setDisplayUserList("NO");
			} else {

				userForm = new UserForm();
				userForm.setDisplayUserList("NO");
				userForm.setActive(true);

			}

			session.setAttribute("userForm", userForm);
			List departments = LookupAgent.getDepartmentList();
			request.setAttribute("departments", departments);
			logger.debug("The User Admin departments set with size .. " + departments.size());

			List roles = LookupAgent.getRoles();
			request.setAttribute("roles", roles);
			logger.debug("The user Admin Roles set with size " + roles.size());

			List groups = LookupAgent.getGroups();
			request.setAttribute("groups", groups);
			logger.debug("The user Admin Groups set with size " + groups.size());

			return (mapping.findForward("success"));
		} catch (Exception e) {
			logger.error("Exception thrown " + e.getMessage(),e);
			return (mapping.findForward("error"));
		}
	}
}
