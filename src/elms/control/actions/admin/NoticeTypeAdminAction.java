package elms.control.actions.admin;

/**@author Manju
 * Notice type data for Admin Module
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.agent.LookupAgent;
import elms.app.admin.ActivitySubType;
import elms.app.admin.NoticeType;
import elms.app.common.DisplayItem;
import elms.control.beans.admin.ActivitySubTypeForm;
import elms.control.beans.admin.NoticeTypeAdminForm;
import elms.util.StringUtils;

public class NoticeTypeAdminAction extends Action {
	String contextRoot = "";
	static Logger logger = Logger.getLogger(NoticeTypeAdminAction.class.getName());
	int result = -1;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.debug("Entered into NoticeTypeAdminAction .. ");
		HttpSession session = request.getSession();

		AdminAgent adminAgent = new AdminAgent();
		NoticeTypeAdminForm noticeTypeAdminForm = (NoticeTypeAdminForm) form;
		List noticeTypes = new ArrayList();

		if (request.getParameter("lookup") != null) {
			result = 0;
			
			noticeTypeAdminForm.setDescription(StringUtils.nullReplaceWithEmpty(request.getParameter("noticeType")));

			noticeTypes = adminAgent.getNoticeTypes(noticeTypeAdminForm);
			session.removeAttribute("noticeTypeAdminForm");
			session.setAttribute("oldNoticeTypeForm", noticeTypeAdminForm);

			noticeTypeAdminForm.setNoticeTypes(noticeTypes);
			noticeTypeAdminForm.setDisplayNoticeTypes("YES");

			PrintWriter pw = response.getWriter();
			if (noticeTypes.size() == 0) {
				pw.write("No Records Found");
			} else {
				pw.write(innerTable(noticeTypes));
			}
			return null;
		} else if (request.getParameter("id") != null) {
			result = 0;
			NoticeType noticeType = adminAgent.getNoticeType(StringUtils.s2i(request.getParameter("id")));

			noticeTypeAdminForm.setNoticeTypeId(noticeType.getId());
			
			noticeTypeAdminForm.setDescription(noticeType.getDescription());
			noticeTypeAdminForm.setDisplayNoticeTypes("YES");

			PrintWriter pw = response.getWriter();
			pw.write(noticeTypeAdminForm.getDescription().toString());
			return null;

		} else if (request.getParameter("save") != null) {

			NoticeTypeAdminForm noticeTypeForm = (NoticeTypeAdminForm) session.getAttribute("oldNoticeTypeForm");

			
			noticeTypeAdminForm.setDescription(StringUtils.nullReplaceWithEmpty(request.getParameter("description")));
			noticeTypeAdminForm.setNoticeTypeId(StringUtils.s2i(request.getParameter("noticeTypeId")));

			result = adminAgent.saveNoticeType(noticeTypeAdminForm.getDescription(), StringUtils.i2s(noticeTypeAdminForm.getNoticeTypeId()));
			//saveActivitySubType
			noticeTypeAdminForm.setDisplayNoticeTypes("NO");
			PrintWriter pw = response.getWriter();

			if (result == 1) {
				pw.write("Notice Type Saved Successfully");
			} else if (result == 2) {
				noticeTypes = adminAgent.getNoticeTypes(noticeTypeForm);

				pw.write(innerTable(noticeTypes));
			} else {
				pw.write("");
			}
			return null;

		} else if (request.getParameter("delete") != null) {

			noticeTypeAdminForm.setDescription(StringUtils.nullReplaceWithEmpty(request.getParameter("noticeTypeStr")));
			

			String idList = (String) request.getParameter("checkboxArray");

			result = adminAgent.deleteNoticeType(idList);
			//deleteActivitySubType

			NoticeTypeAdminForm noticeDelForm = (NoticeTypeAdminForm) session.getAttribute("oldNoticeTypeForm");
			// session.removeAttribute("oldActTypeForm");

			noticeTypes = adminAgent.getNoticeTypes(noticeDelForm);
			noticeTypeAdminForm.setDisplayNoticeTypes("YES");
			//getActivitySubTypes

			PrintWriter pw = response.getWriter();
			pw.write(innerTable(noticeTypes));
			return null;
		} else {
			noticeTypeAdminForm = new NoticeTypeAdminForm();
			
			List nTypes = new ArrayList();
			noticeTypeAdminForm.setNoticeTypes(nTypes);

			noticeTypeAdminForm.setDisplayNoticeTypes("NO");
			session.setAttribute("noticeTypeAdminForm", noticeTypeAdminForm);
			
		}
		
		return (mapping.findForward("success"));
	}

	public String innerTable(List noticeTypes) {
		String noticeTypesHtml = "";
		if (noticeTypes != null && noticeTypes.size() > 0) {
			StringBuffer sb = new StringBuffer("");
			DisplayItem nTypes = null;

			if (noticeTypes != null && noticeTypes.size() > 0) {
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"10\" cellpadding=\"0\" >");
				sb.append("<tr valign=\"top\">");
				sb.append("<td width=\"99%\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
				sb.append("<tr>");
				sb.append("<td><font class=\"con_hdr_3b\">Notice Type List<br>");
				sb.append("<br>");
				sb.append("</font></td></tr><tr>");
				if (result == 2) {
					sb.append("<tr>");
					sb.append("<td><font class=\"con_hdr_3b\" style=\"color:#088A08\">Record Saved Successfully<br>");
					sb.append("<br>");
					sb.append("</font></td></tr><tr>");
				} else if (result == 3) {
					sb.append("<tr>");
					sb.append("<td><font class=\"con_hdr_3b\" style=\"color:#088A08\">Record Deleted Successfully<br>");
					sb.append("<br>");
					sb.append("</font></td></tr><tr>");
				}
				sb.append("<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
				sb.append("<tr><td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>");
				sb.append("<td width=\"95%\" height=\"25\" background=\"../images/site_bg_B7C1CB.jpg\"><font class=\"con_hdr_2b\">Notice Type</font></td>");
				sb.append("<td width=\"1%\"><img src=\"../images/site_hdr_split_1_tall.jpg\" width=\"26\" height=\"25\"></td>");
				sb.append("<td width=\"1%\" background=\"../images/site_bg_D7DCE2.jpg\"><nobr>");
				sb.append("<input type=\"button\" property=\"delete\" value=\"Delete\" styleClass=\"button\" onclick=\"removeTable()\"/>");
				sb.append("<input type=\"button\" property=\"add\" value=\"Add Notice Type\" styleClass=\"button\" onclick=\"refresh()\"/>");
				sb.append("&nbsp;</nobr></td></tr></table></td></tr>");
				sb.append("<tr>");
				sb.append("<td background=\"../images/site_bg_B7C1CB.jpg\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"2\">");
				sb.append("<tr>");
				sb.append("<td width=\"10%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Delete</font></td>");
				sb.append("<td width=\"30%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Activity Sub Type</font></td>");
				sb.append("</tr>");

				for (int i = 0; i < noticeTypes.size(); i++) {
					nTypes = (DisplayItem) noticeTypes.get(i);

					sb.append("<tr bgcolor=\"#FFFFFF\">");
					logger.debug("In action removable check  "+nTypes.getFieldSix());
					if (nTypes.getFieldSix().equalsIgnoreCase("Y")) {
						sb.append("<td>");
						sb.append("<input type=\"checkbox\" name=\"selectedMaping\" value=\"" + nTypes.getFieldOne() + "\"  onclick=\"checkIds(this)\" >");
						sb.append("</td>");
					} else {
						sb.append("<td>&nbsp;</td>");
					}
					sb.append("<td bgcolor=\"#FFFFFF\">");
					sb.append("<font class=\"con_text_1\"><a href=\"javascript:void(0)\" onclick=\"CreateXmlHttp(" + nTypes.getFieldOne() + "," + StringUtils.checkString(nTypes.getFieldSix()) + ")\" >");
					sb.append(nTypes.getFieldTwo());
					sb.append("</a></font></td>");
					//sb.append("<td bgcolor=\"#FFFFFF\"><font class=\"con_text_1\">");
					//sb.append(nTypes.getFieldThree());
					//sb.append("</font></td>");
					sb.append("</tr>");
				}
				sb.append("</table></td></tr></table></td></tr><tr><td>&nbsp;</td></tr></table></td><td width=\"1%\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td height=\"32\">&nbsp;</td>");
				sb.append("</tr><tr><td>&nbsp;</td></tr></table></td></tr></table>");
			}
			noticeTypesHtml = sb.toString();
			// logger.debug(actTypesHtml);
		}
		return noticeTypesHtml;
	}
}
