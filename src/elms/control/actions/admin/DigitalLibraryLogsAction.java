package elms.control.actions.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import sun.jdbc.rowset.CachedRowSet;
import elms.agent.AddressAgent;

public class DigitalLibraryLogsAction extends Action {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(DigitalLibraryLogsAction.class.getName());
	String nextPage = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		/**
		 * The HTTP Session
		 */
		HttpSession session = request.getSession();

		String action = request.getParameter("action");
		if (action == null)
			action = "list";
		String recordId = request.getParameter("recordId");
		if (recordId == null)
			recordId = "0";

		logger.debug("requested action is " + action);
		try {
			if (action.equals("list")) {
				CachedRowSet dlLogs = (CachedRowSet) new AddressAgent().getDigitalLibraryLogList();
				logger.debug("obtained list of digital library logs and set it to the request");
				request.setAttribute("dlLogs", dlLogs);
				nextPage = "success";
			} else if (action.equals("delete")) {
				int intRecordId = Integer.parseInt(recordId);
				if (intRecordId > 0) {
					new AddressAgent().clearDigitalLibraryLogs(intRecordId);
					logger.debug("digital library record cleared for record id of " + intRecordId);
					request.setAttribute("message", "Digital Library Logs Record # " + intRecordId + "removed successfully");
				} else {
					new AddressAgent().clearDigitalLibraryLogs(-1);// delete all records
					logger.debug("digital library logs cleared");
					request.setAttribute("message", "Digital Library Logs Cleared Successfully");
				}
				nextPage = "successDelete";
			} else {
				logger.error("no action defined");
				throw new ServletException("No Action mapping defined for Digital Library Log request");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return (mapping.findForward(nextPage));
	}
}
