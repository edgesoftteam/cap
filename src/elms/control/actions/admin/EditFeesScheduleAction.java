package elms.control.actions.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.LookupAgent;
import elms.app.admin.FeeEdit;
import elms.control.beans.FeeScheduleForm;
import elms.control.beans.LookupFeesForm;

public class EditFeesScheduleAction extends Action {
	static Logger logger = Logger.getLogger(EditFeesScheduleAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		MessageResources messages = getResources();
		HttpSession session = request.getSession();

		try {
			logger.info("Entering EditFeesAction ()");
			
			String method = request.getParameter("method")!=null?request.getParameter("method"):"";
			if (method.equalsIgnoreCase("subtypes")) {
				logger.debug("******CAME TO subtypes********");
				 try{
					String actType = request.getParameter("actType")!=null?request.getParameter("actType"):""; 
					String s = LookupAgent.getActivitySubTypesOptions(actType);
					
            		PrintWriter out = response.getWriter();
    				out.println(s);
    				// Close the writer
    				out.close();
    				
    				return (mapping.findForward(null));

			        }catch(Exception e) { 
			        	//e.printStackTrace();
		        		logger.error("Get subtypes FAILED"+e.getMessage());
		        	}
				
			}

			// Get the static list of fee types from the lookup agent
			List feeTypes = LookupAgent.getFeeTypes();
			logger.debug("obtained list of size " + feeTypes.size());
			request.setAttribute("feeTypes", feeTypes);

			// Get the static list of fee types from the lookup agent
			List feeLookups = LookupAgent.getFeeLookups();
			request.setAttribute("feeLookups", feeLookups);

			FeeScheduleForm subForm = (FeeScheduleForm) session.getAttribute("feeScheduleForm");
			FeeEdit[] lst = subForm.getFeeEditList();

			List feeList = new ArrayList();

			if (subForm.getAction().equals("Add")) {
				feeList.add(new FeeEdit());
			} else {
				for (int i = 0; i < lst.length; i++) {
					if (lst[i].getCheck().equals("on")) {
						if (subForm.getAction().equals("Duplicate")) {
							lst[i].setFeeId("0");
						}

						feeList.add(lst[i]);
					}
				}
			}

			FeeEdit[] aryFee = new FeeEdit[feeList.size()];

			for (int i = 0; i < feeList.size(); i++) {
				aryFee[i] = (FeeEdit) feeList.get(i);
			}

			ActionForm frm = new LookupFeesForm();
			FeeScheduleForm frmFee = new FeeScheduleForm();

			frmFee.setFeeEditList(aryFee);
			logger.debug("FeeSheduleForm has " + feeList.size() + " elements");
			session.setAttribute("feeScheduleForm", frmFee);
			request.setAttribute("action", subForm.getAction());
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return (mapping.findForward("success"));
	}
}
