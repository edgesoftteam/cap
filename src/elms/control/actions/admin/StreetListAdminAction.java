package elms.control.actions.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AddressAgent;
import elms.control.beans.admin.StreetListAdminForm;

public class StreetListAdminAction extends Action {
	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(StreetListAdminAction.class.getName());

	/**
	 * The Perform
	 */
	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.info("Entered into StreetListAdminAction");

		HttpSession session = request.getSession();
		String id = request.getParameter("id");
		logger.debug("id" + id);

		String update = request.getParameter("update");
		logger.debug("update" + update);
		request.setAttribute("id", id);

		String action = request.getParameter("action");
		logger.debug("action" + action);

		String delete = request.getParameter("delete");
		logger.debug("delete" + delete);

		AddressAgent streetAgent = new AddressAgent();
		ActionErrors errors = new ActionErrors();

		StreetListAdminForm streetListAdminForm = (StreetListAdminForm) form;

		try {
			if (id == null) {
				List streetList = new ArrayList();

				if (action != null) {
					return (mapping.findForward("next"));
				}

				// Gets the street list from DB
				streetList = streetAgent.getStreetListAdmin();
				logger.debug("streetList" + streetList.size());
				request.setAttribute("streetList", streetList);
				if (request.getParameter("delete") != null) { // condition to delete

					// get maping ids for delete
					List useList = new ArrayList();
					String[] mapIds = streetListAdminForm.getSelectedMaping();

					for (int i = 0; i < mapIds.length; i++) {
						// delete function called from streetAgent
						streetAgent.deleteStreetList(mapIds[i]);
						logger.debug("mapIds" + mapIds[i]);
					}

					streetList = streetAgent.getStreetListAdmin();
					request.setAttribute("streetList", streetList);
					errors.add("linkNotification", new ActionError("linkNotification", " Street list deleted successfully"));
					saveErrors(request, errors);
				} else {
					streetListAdminForm = new StreetListAdminForm();

				}

				session.removeAttribute("streetListAdminForm");

				return (mapping.findForward("success"));
			}

			if (id != null) {
				if (update != null) { // condition to update or save

					int streetid = streetListAdminForm.getStreetListId();
					String streetName = streetListAdminForm.getStreetName();
					String streetPrefix = streetListAdminForm.getStreetPrefix();
					String streetType = streetListAdminForm.getStreetType();
					String streetSuffix = streetListAdminForm.getStreetSuffix();

					// update or saves the street related data
					int result = streetAgent.saveStreetList(streetid, streetName, streetPrefix, streetType, streetSuffix);

					streetListAdminForm = streetAgent.getstreetIdList(request.getParameter("id"), streetListAdminForm);
					session.setAttribute("streetListAdminForm", streetListAdminForm);

					errors.add("linkNotification", new ActionError("linkNotification", " Street list saved successfully"));
					saveErrors(request, errors);

					return (mapping.findForward("next"));
				}

				streetListAdminForm = streetAgent.getstreetIdList(request.getParameter("id"), streetListAdminForm);
				session.setAttribute("streetListAdminForm", streetListAdminForm);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return (mapping.findForward("next"));
	}
}
