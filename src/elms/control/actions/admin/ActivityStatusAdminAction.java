package elms.control.actions.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.app.common.DisplayItem;
import elms.control.beans.admin.ActivityStatusAdminForm;
import elms.util.StringUtils;

public class ActivityStatusAdminAction extends Action {

	static Logger logger = Logger.getLogger(ActivityStatusAdminAction.class.getName());
	int result = -1;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.debug("Entered into ActivityStatusAdminAction .. ");

		HttpSession session = request.getSession();
		ActionErrors errors = new ActionErrors();
		AdminAgent adminAgent = new AdminAgent();
		ActivityStatusAdminForm activityStatusForm = (ActivityStatusAdminForm) form;
		List activityStatuses = new ArrayList();
		activityStatusForm.setDisplayActStatus("NO");

		String statusId = request.getParameter("statusId");

		// condition to lookup
		if (request.getParameter("lookup") != null) {
			result = 0;
			// getting and setting all the values to the form
			activityStatusForm.setStatusCode(StringUtils.nullReplaceWithEmpty(request.getParameter("statusCodeStr")));
			activityStatusForm.setDescriptionType(StringUtils.nullReplaceWithEmpty(request.getParameter("descriptionType")));

			boolean active = StringUtils.s2b(request.getParameter("active"));
			boolean psaActive = StringUtils.s2b(request.getParameter("psaActive"));

			activityStatusForm.setActive(active);
			activityStatusForm.setPsaActive(psaActive);
			activityStatusForm.setProjectNameId(request.getParameter("projectNameId"));
			activityStatusForm.setModuleId(request.getParameter("moduleId"));

			activityStatuses = adminAgent.getActivityStatus(activityStatusForm);

			session.removeAttribute("activityStatusForm");
			session.setAttribute("oldActStatusForm", activityStatusForm);

			activityStatusForm.setActivityStatus(activityStatuses);
			activityStatusForm.setDisplayActStatus("YES");

			PrintWriter pw = response.getWriter();
			if (activityStatuses.size() == 0) {
				pw.write("No Records Found");
			} else {
				pw.write(innerTable(activityStatuses));
			}
			return null;
		} else if (request.getParameter("id") != null) { // condition to populate values to jsp page
			result = 0;
			activityStatusForm = adminAgent.getActivityStatus(StringUtils.s2i(request.getParameter("id")), activityStatusForm);
			activityStatusForm.setDisplayActStatus("NO");

			PrintWriter pw = response.getWriter();
			pw.write(activityStatusForm.getStatusCode().toString() + ',' + activityStatusForm.getDescriptionType().toString() + ',' + activityStatusForm.isActive() + ',' + activityStatusForm.isPsaActive() + ',' + StringUtils.nullReplaceWithEmpty(activityStatusForm.getProjectNameId()) + ',' + activityStatusForm.getModuleId());
			return null;
		} else if (request.getParameter("save") != null) { // condition to save

			ActivityStatusAdminForm actStForm = (ActivityStatusAdminForm) session.getAttribute("oldActStatusForm");

			activityStatusForm.setStatusCode(request.getParameter("statusCodeStr"));
			activityStatusForm.setDescriptionType(request.getParameter("descriptionType"));
			activityStatusForm.setActive(StringUtils.s2b(request.getParameter("active")));
			activityStatusForm.setPsaActive(StringUtils.s2b(request.getParameter("psaActive")));
			activityStatusForm.setProjectNameId(request.getParameter("projectNameId"));
			activityStatusForm.setModuleId(request.getParameter("moduleId"));

			// save function called from Adminagent
			result = adminAgent.saveActivityStatus(statusId, activityStatusForm.getStatusCode(), activityStatusForm.getDescriptionType(), activityStatusForm.isActive(), StringUtils.s2i(activityStatusForm.getProjectNameId()), activityStatusForm.isPsaActive(), StringUtils.s2i(activityStatusForm.getModuleId()));
			activityStatusForm.setDisplayActStatus("NO");

			if (result == 1) {
				PrintWriter pw = response.getWriter();
				pw.write("Activity Status Saved Successfully");
			} else if (result == 2) {
				activityStatuses = adminAgent.getActivityStatus(actStForm);

				PrintWriter pw = response.getWriter();
				pw.write(innerTable(activityStatuses));
			} else {
				errors.add("statusCodeExists", new ActionError("statusCodeExists", " Activity Status is not added"));
				saveErrors(request, errors);
			}
			return null;
		} else if (request.getParameter("delete") != null) {// condition to delete

			// getting and setting all the values to the form
			activityStatusForm.setStatusCode(StringUtils.nullReplaceWithEmpty(request.getParameter("statusCodeStr")));
			activityStatusForm.setDescriptionType(StringUtils.nullReplaceWithEmpty(request.getParameter("descriptionType")));

			boolean active = StringUtils.s2b(request.getParameter("active"));
			boolean psaActive = StringUtils.s2b(request.getParameter("psaActive"));

			String idList = (String) request.getParameter("checkboxArray");

			activityStatusForm.setActive(active);
			activityStatusForm.setPsaActive(psaActive);
			activityStatusForm.setProjectNameId(request.getParameter("projectNameId"));
			activityStatusForm.setModuleId(request.getParameter("moduleId"));

			result = adminAgent.deleteActivityStatus(idList);

			ActivityStatusAdminForm oldActStForm = (ActivityStatusAdminForm) session.getAttribute("oldActStatusForm");

			activityStatusForm.setActivityStatus(adminAgent.getActivityStatus(oldActStForm));
			activityStatusForm.setDisplayActStatus("YES");

			PrintWriter pw = response.getWriter();
			pw.write(innerTable(activityStatusForm.getActivityStatus()));
			return null;
		} else {
			activityStatusForm = new ActivityStatusAdminForm();
			activityStatusForm.setDisplayActStatus("NO");
			session.setAttribute("activityStatusAdminForm", activityStatusForm);
		}// end of if
		return (mapping.findForward("success"));
	}

	public String innerTable(List actStatuses) {
		String actStatusHtml = "";
		if (actStatuses != null && actStatuses.size() > 0) {
			StringBuffer sb = new StringBuffer("");
			DisplayItem activityStatus = null;

			if (actStatuses != null && actStatuses.size() > 0) {
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"10\" cellpadding=\"0\" >");
				sb.append("<tr valign=\"top\">");
				sb.append("<td width=\"100%\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
				sb.append("<tr><td><font class=\"con_hdr_3b\">Activity Status List<br><br></font></td></tr>");
				if (result == 2) {
					sb.append("<tr>");
					sb.append("<td><font class=\"con_hdr_3b\" style=\"color:#088A08\">Record Saved Successfully<br>");
					sb.append("<br>");
					sb.append("</font></td></tr><tr>");
				} else if (result == 3) {
					sb.append("<tr>");
					sb.append("<td><font class=\"con_hdr_3b\" style=\"color:#088A08\">Record Deleted Successfully<br>");
					sb.append("<br>");
					sb.append("</font></td></tr><tr>");
				}
				sb.append("<tr><td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>");
				sb.append("<td width=\"98%\" height=\"25\" bgcolor=\"#B7C1CB\"><font class=\"con_hdr_2b\">Activity Status</font></td>");
				sb.append("<td width=\"1%\"><img src=\"../images/site_hdr_split_1_tall.jpg\" width=\"26\" height=\"25\"></td>");
				sb.append("<td width=\"1%\" background=\"../images/site_bg_D7DCE2.jpg\"><nobr>");
				sb.append("<input type=\"button\" property=\"delete\" value=\"Delete\" styleClass=\"button\" onclick=\"removeTable()\"/>");
				sb.append("<input type=\"button\" property=\"add\" value=\"Add Status\" styleClass=\"button\" onclick=\"refresh()\"/>");
				sb.append("&nbsp;</nobr></td></tr></table></td></tr>");
				sb.append("<tr>");
				sb.append("<td background=\"../images/site_bg_B7C1CB.jpg\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"1\">");
				sb.append("<tr bgcolor=\"#FFFFFF\">");
				sb.append("<td width=\"10%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Delete</font></td>");
				sb.append("<td width=\"30%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Activity Status Code</font></td>");
				sb.append("<td width=\"30%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Description</font></td>");
				sb.append("<td width=\"15%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\"><nobr>Project Description</nobr></font></td>");
				sb.append("<td width=\"15%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Module Description</font></td>");
				sb.append("</tr>");

				for (int i = 0; i < actStatuses.size(); i++) {
					activityStatus = (DisplayItem) actStatuses.get(i);

					sb.append("<tr bgcolor=\"#FFFFFF\">");
					if (activityStatus.getFieldSix().equalsIgnoreCase("Y")) {
						sb.append("<td>");
						sb.append("<input type=\"checkbox\" name=\"selectedMaping\" value=\"" + activityStatus.getFieldOne() + "\"  onclick=\"checkIds(this)\" >");
						sb.append("</td>");
					} else {
						sb.append("<td>&nbsp;</td>");
					}
					sb.append("<td bgcolor=\"#FFFFFF\">");
					sb.append("<font class=\"con_text_1\"><a href=\"javascript:void(0)\" onclick=\"CreateXmlHttp(" + activityStatus.getFieldOne() + "," + StringUtils.checkString(activityStatus.getFieldSix()) + ")\" >");
					sb.append(activityStatus.getFieldTwo());
					sb.append("</a></font></td>");
					sb.append("<td bgcolor=\"#FFFFFF\"><font class=\"con_text_1\">");
					sb.append(activityStatus.getFieldThree());
					sb.append("</font></td>");
					sb.append("<td bgcolor=\"#FFFFFF\"><font class=\"con_text_1\">");
					sb.append(StringUtils.nullReplaceWithEmpty(activityStatus.getFieldFour()));
					sb.append("</font></td>");
					sb.append("<td bgcolor=\"#FFFFFF\"><font class=\"con_text_1\">");
					sb.append(activityStatus.getFieldFive());
					sb.append("</font></td>");
					sb.append("</tr>");
				}
				sb.append("</table></td></tr></table></td></tr><tr><td>&nbsp;</td></tr></table></td><td width=\"1%\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td height=\"32\">&nbsp;</td>");
				sb.append("</tr><tr><td>&nbsp;</td></tr></table></td></tr></table>");
			}
			actStatusHtml = sb.toString();
			// logger.debug(actStatusHtml);
		}
		return actStatusHtml;
	}
}