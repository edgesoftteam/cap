package elms.control.actions.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.control.beans.MoveVerifyForm;
import elms.control.beans.admin.MoveLsoForm;

public class VerifyMoveLsoAction extends Action {

	static Logger logger = Logger.getLogger(VerifyMoveLsoAction.class.getName());
	String nextPage = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		MoveLsoForm moveLsoForm = (MoveLsoForm) form;
		ActionErrors errors = new ActionErrors();

		logger.info("Entering VerifyMoveLsoAction");
		try {
			AdminAgent moveLsoAgent = new AdminAgent();

			MoveVerifyForm verifyForm = new MoveVerifyForm();
			boolean success = moveLsoAgent.getAddresses(moveLsoForm, verifyForm);
			if (success) {
				request.setAttribute("moveVerifyForm", verifyForm);
				nextPage = "success";
			} else {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.lso.notpresent"));
				saveErrors(request, errors);
				request.setAttribute("moveStreetId", moveLsoForm.getMoveName());
				request.setAttribute("toStreetId", moveLsoForm.getToName());
				return (new ActionForward(mapping.getInput()));
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		logger.info("Exiting VerifyMoveLsoAction(" + nextPage + ")");
		return (mapping.findForward(nextPage));

	}

}