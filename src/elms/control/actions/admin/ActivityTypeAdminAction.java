package elms.control.actions.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.agent.LookupAgent;
import elms.app.admin.ActivityType;
import elms.app.common.DisplayItem;
import elms.control.beans.admin.ActivityTypeForm;
import elms.util.StringUtils;

public class ActivityTypeAdminAction extends Action {
	String contextRoot = "";

	static Logger logger = Logger.getLogger(ActivityTypeAdminAction.class.getName());
	int result = -1;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.debug("Entered into ActivityAdminAction .. ");

		HttpSession session = request.getSession();
		ActionErrors errors = new ActionErrors();
		AdminAgent adminAgent = new AdminAgent();
		ActivityTypeForm activityTypeForm = (ActivityTypeForm) form;
		List activityTypes = new ArrayList();

		if (request.getParameter("lookup") != null) {
			result = 0;
			activityTypeForm.setActivityType(StringUtils.nullReplaceWithEmpty(request.getParameter("activityTypeStr")));
			activityTypeForm.setDescription(StringUtils.nullReplaceWithEmpty(request.getParameter("description")));
			activityTypeForm.setSubProjectType(StringUtils.nullReplaceWithEmpty(request.getParameter("subProjectType")));
			activityTypeForm.setDepartment(StringUtils.nullReplaceWithEmpty(request.getParameter("department")));
			activityTypeForm.setModuleId(StringUtils.nullReplaceWithEmpty(request.getParameter("moduleId")));
			activityTypeForm.setMuncipalCode(StringUtils.nullReplaceWithEmpty(request.getParameter("muncipalCode")));
			activityTypeForm.setSicCode(StringUtils.nullReplaceWithEmpty(request.getParameter("sicCode")));
			activityTypeForm.setClassCode(StringUtils.nullReplaceWithEmpty(request.getParameter("classCode")));

			activityTypes = adminAgent.getActivityTypes(activityTypeForm);

			session.removeAttribute("activityTypeForm");
			session.setAttribute("oldActTypeForm", activityTypeForm);

			activityTypeForm.setActivityTypes(activityTypes);
			activityTypeForm.setDisplayActTypes("YES");

			PrintWriter pw = response.getWriter();
			if (activityTypes.size() == 0) {
				pw.write("No Records Found");
			} else {
				pw.write(innerTable(activityTypes));
			}

			return null;
		} else if (request.getParameter("id") != null) {
			result = 0;
			ActivityType activityType = adminAgent.getActivityType(StringUtils.s2i(request.getParameter("id")));

			activityTypeForm.setActivityTypeId(activityType.getTypeId());
			activityTypeForm.setActivityType(activityType.getType());
			activityTypeForm.setDescription(activityType.getDescription());
			activityTypeForm.setDepartment(StringUtils.i2s(activityType.getDepartmentId()));
			activityTypeForm.setSubProjectType(StringUtils.i2s(activityType.getSubProjectType()));
			activityTypeForm.setModuleId(StringUtils.i2s(activityType.getModuleId()));
			activityTypeForm.setMuncipalCode(activityType.getMuncipalCode());
			activityTypeForm.setSicCode(activityType.getSicCode());
			activityTypeForm.setClassCode(activityType.getClassCode());
			activityTypeForm.setDisplayActTypes("YES");

			PrintWriter pw = response.getWriter();
			pw.write(activityTypeForm.getActivityType().toString() + ',' + activityTypeForm.getDescription().toString() + ',' + activityTypeForm.getSubProjectType().toString() + ',' + activityTypeForm.getDepartment().toString() + ',' + activityTypeForm.getModuleId().toString() + ',' + activityTypeForm.getMuncipalCode().toString() + ',' + activityTypeForm.getSicCode().toString() + ',' + activityTypeForm.getClassCode().toString());
			return null;

		} else if (request.getParameter("save") != null) {

			ActivityTypeForm actForm = (ActivityTypeForm) session.getAttribute("oldActTypeForm");
			// session.removeAttribute("oldActTypeForm");

			activityTypeForm.setActivityType(request.getParameter("activityTypeStr"));
			activityTypeForm.setDescription(request.getParameter("description"));
			activityTypeForm.setSubProjectType(request.getParameter("subProjectType"));
			activityTypeForm.setDepartment(request.getParameter("department"));
			activityTypeForm.setModuleId(request.getParameter("moduleId"));
			activityTypeForm.setMuncipalCode(request.getParameter("muncipalCode"));
			activityTypeForm.setSicCode(request.getParameter("sicCode"));
			activityTypeForm.setClassCode(request.getParameter("classCode"));

			result = adminAgent.saveActivityType(activityTypeForm.getActivityType(), activityTypeForm.getDescription(), activityTypeForm.getDepartment(), activityTypeForm.getSubProjectType(), StringUtils.s2i(activityTypeForm.getModuleId()), activityTypeForm.getMuncipalCode(), activityTypeForm.getSicCode(), activityTypeForm.getClassCode());
			activityTypeForm.setDisplayActTypes("NO");

			if (result == 1) {
				PrintWriter pw = response.getWriter();
				pw.write("Activity Type Saved Successfully");
			} else if (result == 2) {
				activityTypes = adminAgent.getActivityTypes(actForm);

				PrintWriter pw = response.getWriter();
				pw.write(innerTable(activityTypes));
			} else {
				errors.add("statusCodeExists", new ActionError("statusCodeExists", " Activity Type is not added"));
				saveErrors(request, errors);
			}
			return null;
		} else if (request.getParameter("delete") != null) {

			activityTypeForm.setActivityType(StringUtils.nullReplaceWithEmpty(request.getParameter("activityTypeStr")));
			activityTypeForm.setDescription(StringUtils.nullReplaceWithEmpty(request.getParameter("description")));
			activityTypeForm.setSubProjectType(StringUtils.nullReplaceWithEmpty(request.getParameter("subProjectTypeText")));
			activityTypeForm.setDepartment(StringUtils.nullReplaceWithEmpty(request.getParameter("department")));
			activityTypeForm.setModuleId(StringUtils.nullReplaceWithEmpty(request.getParameter("moduleId")));
			activityTypeForm.setMuncipalCode(StringUtils.nullReplaceWithEmpty(request.getParameter("muncipalCode")));
			activityTypeForm.setSicCode(StringUtils.nullReplaceWithEmpty(request.getParameter("sicCode")));
			activityTypeForm.setClassCode(StringUtils.nullReplaceWithEmpty(request.getParameter("classCode")));

			String idList = (String) request.getParameter("checkboxArray");

			result = adminAgent.deleteActivityType(idList);
			logger.debug("Result %%%%% " + result);

			ActivityTypeForm actdelForm = (ActivityTypeForm) session.getAttribute("oldActTypeForm");
			// session.removeAttribute("oldActTypeForm");

			activityTypes = adminAgent.getActivityTypes(actdelForm);
			activityTypeForm.setDisplayActTypes("YES");

			PrintWriter pw = response.getWriter();
			pw.write(innerTable(activityTypes));
			return null;
		} else {
			activityTypeForm = new ActivityTypeForm();
			activityTypeForm.setActivityTypes(activityTypes);
			activityTypeForm.setDisplayActTypes("NO");
			session.setAttribute("activityTypeForm", activityTypeForm);
		}

		List departments;
		try {
			departments = LookupAgent.getDepartmentList();
		} catch (Exception e) {
			departments = new ArrayList();
			logger.warn("no department list obtained, so initializing it to blank arraylist");
		}
		request.setAttribute("departments", departments);
		logger.debug("The Activity Type Admin departments set with size .. " + departments.size());

		List modules;
		try {
			modules = LookupAgent.getModules();
		} catch (Exception e) {
			modules = new ArrayList();
			logger.warn("no module list obtained, so initializing it to blank arraylist");
		}
		request.setAttribute("modules", modules);
		logger.debug("The modules set with size .. " + departments.size());

		List subProjectTypes;
		try {
			subProjectTypes = LookupAgent.getSubProjectNames("ALL");
		} catch (Exception e1) {
			subProjectTypes = new ArrayList();
			logger.warn("no subProjectTypes list obtained, so initializing it to blank arraylist");
		}
		request.setAttribute("subProjectTypes", subProjectTypes);
		request.setAttribute("activityTypes", activityTypes);
		logger.debug("The Activity Type Admin Sub Project Types set with size " + subProjectTypes.size());

		return (mapping.findForward("success"));
	}

	public String innerTable(List actTypes) {
		String actTypesHtml = "";
		if (actTypes != null && actTypes.size() > 0) {
			StringBuffer sb = new StringBuffer("");
			DisplayItem activityTypes = null;

			if (actTypes != null && actTypes.size() > 0) {
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"10\" cellpadding=\"0\" >");
				sb.append("<tr valign=\"top\">");
				sb.append("<td width=\"99%\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
				sb.append("<tr>");
				sb.append("<td><font class=\"con_hdr_3b\">Activity Type List<br>");
				sb.append("<br>");
				sb.append("</font></td></tr><tr>");
				logger.debug("$#$#$#$#$#$ " + result);
				if (result == 2) {
					sb.append("<tr>");
					sb.append("<td><font class=\"con_hdr_3b\" style=\"color:#088A08\">Record Saved Successfully<br>");
					sb.append("<br>");
					sb.append("</font></td></tr><tr>");
				} else if (result == 3) {
					logger.debug("$#$#$#$#$#$ ");
					sb.append("<tr>");
					sb.append("<td><font class=\"con_hdr_3b\" style=\"color:#088A08\">Record Deleted Successfully<br>");
					sb.append("<br>");
					sb.append("</font></td></tr><tr>");
				}
				sb.append("<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
				sb.append("<tr><td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>");
				sb.append("<td width=\"95%\" height=\"25\" background=\"../images/site_bg_B7C1CB.jpg\"><font class=\"con_hdr_2b\">Activity Types</font></td>");
				sb.append("<td width=\"1%\"><img src=\"../images/site_hdr_split_1_tall.jpg\" width=\"26\" height=\"25\"></td>");
				sb.append("<td width=\"1%\" background=\"../images/site_bg_D7DCE2.jpg\"><nobr>");
				sb.append("<input type=\"button\" property=\"delete\" value=\"Delete\" styleClass=\"button\" onclick=\"removeTable()\"/>");
				sb.append("<input type=\"button\" property=\"add\" value=\"Add Type\" styleClass=\"button\" onclick=\"refresh()\"/>");
				sb.append("&nbsp;</nobr></td></tr></table></td></tr>");
				sb.append("<tr>");
				sb.append("<td background=\"../images/site_bg_B7C1CB.jpg\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"2\">");
				sb.append("<tr>");
				sb.append("<td width=\"10%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Delete</font></td>");
				sb.append("<td width=\"15%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Activity Type</font></td>");
				sb.append("<td width=\"30%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Description</font></td>");
				sb.append("<td width=\"25%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\"><nobr>Sub Project Type</nobr></font></td>");
				sb.append("<td width=\"20%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Department</font></td>");
				sb.append("</tr>");

				for (int i = 0; i < actTypes.size(); i++) {
					activityTypes = (DisplayItem) actTypes.get(i);

					sb.append("<tr bgcolor=\"#FFFFFF\">");
					if (activityTypes.getFieldSix().equalsIgnoreCase("Y")) {
						sb.append("<td>");
						sb.append("<input type=\"checkbox\" name=\"selectedMaping\" value=\"" + activityTypes.getFieldOne() + "\"  onclick=\"checkIds(this)\" >");
						sb.append("</td>");
					} else {
						sb.append("<td>&nbsp;</td>");
					}
					sb.append("<td bgcolor=\"#FFFFFF\">");
					sb.append("<font class=\"con_text_1\"><a href=\"javascript:void(0)\" onclick=\"CreateXmlHttp(" + activityTypes.getFieldOne() + "," + StringUtils.checkString(activityTypes.getFieldSix()) + ")\" >");
					sb.append(activityTypes.getFieldTwo());
					sb.append("</a></font></td>");
					sb.append("<td bgcolor=\"#FFFFFF\"><font class=\"con_text_1\">");
					sb.append(activityTypes.getFieldThree());
					sb.append("</font></td>");
					sb.append("<td bgcolor=\"#FFFFFF\"><font class=\"con_text_1\">");
					sb.append(StringUtils.nullReplaceWithEmpty(activityTypes.getFieldFour()));
					sb.append("</font></td>");
					sb.append("<td bgcolor=\"#FFFFFF\"><font class=\"con_text_1\">");
					sb.append(activityTypes.getFieldFive());
					sb.append("</font></td>");
					sb.append("</tr>");
				}
				sb.append("</table></td></tr></table></td></tr><tr><td>&nbsp;</td></tr></table></td><td width=\"1%\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td height=\"32\">&nbsp;</td>");
				sb.append("</tr><tr><td>&nbsp;</td></tr></table></td></tr></table>");
			}
			actTypesHtml = sb.toString();
			// logger.debug(actTypesHtml);
		}
		return actTypesHtml;
	}
}
