package elms.control.actions.lso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AddressAgent;

public class ViewLsoWithLsoTreeAction extends Action {
	static Logger logger = Logger.getLogger(ViewLsoWithLsoTreeAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.info("Entering ViewLsoWithLsoTreeAction");

		HttpSession session = request.getSession();
		String lsoId = request.getParameter("lsoId");
		logger.debug("LsoId received is :" + lsoId);

		AddressAgent lsoTreeAgent = new AddressAgent();
		List lsoIdList = new ArrayList();
		List lsoTreeList = new ArrayList();

		try {
			lsoIdList = lsoTreeAgent.getLandLsoIdList(new Long(lsoId).longValue());
			lsoTreeList = lsoTreeAgent.getLsoTreeList(lsoIdList, request.getContextPath());
			logger.debug("no of elements after calling getLandLsoIdList = " + lsoIdList.size());
		} catch (Exception e1) {
			throw new ServletException("couldn not get lso id list " + e1.getMessage());
		}

		String streetId = "-1";

		// String streetId = lsoTreeAgent.getStreetIdForLsoId(lsoId);
		ArrayList treeList = new ArrayList();
		String tree;

		for (int i = 0; (lsoTreeList != null) && (i < lsoTreeList.size()); i++) {
			elms.app.lso.LsoTree lsoTree = (elms.app.lso.LsoTree) lsoTreeList.get(i);

			// build the tree here...
			tree = "	Tree[" + i + "] = \"" + lsoTree.getNodeId() + "|" + lsoTree.getNodeLevel() + "|" + ((lsoTree.isOnHold() == true) ? ("<font color='" + ((lsoTree.getHoldColor() != null) ? lsoTree.getHoldColor() : "black") + "'><b>(H)</b></font>") : "") + lsoTree.getNodeText() + "|" + lsoTree.getNodeLink() + "|" + lsoTree.getNodeAlt() + "|" + lsoTree.getId() + "\"";

			logger.debug(" Tree string  is " + tree);
			treeList.add(tree);
		}

		session.setAttribute("treeList", treeList);

		// End Test -- deva
		session.setAttribute("lsoTreeList", lsoTreeList);
		session.setAttribute("streetId", streetId);

		logger.debug("Tree string created and set into the session. Number of elements - " + lsoTreeList.size());

		// Added by Raj for Radio Button All.

		request.setAttribute("lsoId", lsoId);
		String radioStatus = "All";

		request.setAttribute("radioStatus", radioStatus);

		logger.info("Radio Button is set to All:" + radioStatus);

		logger.info("Exiting ViewLsoWithLsoTreeAction");

		return (mapping.findForward("success"));
	}
}
