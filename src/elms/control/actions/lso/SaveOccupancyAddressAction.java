package elms.control.actions.lso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AddressAgent;
import elms.app.lso.Occupancy;
import elms.app.lso.OccupancyAddress;
import elms.app.lso.OccupancyDetails;
import elms.app.lso.OccupancySiteData;
import elms.app.lso.Street;
import elms.control.beans.OccupancyForm;
import elms.util.StringUtils;

public class SaveOccupancyAddressAction extends Action {

	static Logger logger = Logger.getLogger(SaveOccupancyAddressAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		// Extract attributes and parameters we will need
		HttpSession session = request.getSession();
		OccupancyForm occupancyForm = (OccupancyForm) form;

		try {
			int lsoId = 0;
			String strLsoId = request.getParameter("lsoId");
			if ((strLsoId != null) && (!strLsoId.equals(""))) {
				lsoId = Integer.parseInt(request.getParameter("lsoId"));
			} else {
				logger.error("***********error lsoId = " + lsoId);
			}

			int addressId = 0;
			String strAddrId = request.getParameter("addressId");
			if ((strAddrId != null) && (!strAddrId.equals(""))) {
				addressId = Integer.parseInt(request.getParameter("addressId"));
			} else {
				logger.error("***********error addressId = " + addressId);
			}

			OccupancyDetails occupancyDetails = new OccupancyDetails();
			List occupancyAddress = new ArrayList();

			Street street = new Street();
			street.setStreetId(StringUtils.s2i(occupancyForm.getStreetName()));

			OccupancyAddress occupancyAddressObj = new OccupancyAddress(addressId, lsoId, "O", Integer.parseInt(occupancyForm.getStreetNumber()), occupancyForm.getStreetMod(), street, occupancyForm.getUnit(), occupancyForm.getCity(), occupancyForm.getState(), occupancyForm.getZip(), occupancyForm.getZip4(), occupancyForm.getAddrDescription(), StringUtils.b2s(occupancyForm.getPrimary()), StringUtils.b2s(occupancyForm.getActiveCheck()));

			logger.warn("************** unit is :" + occupancyForm.getUnit());
			logger.warn("************** desc is :" + occupancyForm.getAddrDescription());
			logger.warn("************** primary is :" + occupancyForm.getPrimary());
			logger.warn("@@@@@@ unit is " + occupancyAddressObj.getUnit());

			occupancyAddress.add(occupancyAddressObj);

			List occupancyHold = new ArrayList();
			List occupancyCondition = new ArrayList();
			List occupancyAttachment = new ArrayList();
			List occupancyComment = new ArrayList();
			List occupancyApn = new ArrayList();
			OccupancySiteData occupancySiteData = new OccupancySiteData();
			Occupancy occupancy = new Occupancy(lsoId, 0, occupancyDetails, occupancyAddress, occupancyApn, occupancyHold, occupancyCondition, occupancyAttachment, occupancySiteData, occupancyComment);
			logger.debug("Occupancy object created for new occupancy creation");

			AddressAgent occupancyAgent = new AddressAgent();
			logger.debug("before sending to the occupancy agent");

			Occupancy newOccupancy = new Occupancy();

			if ((addressId == 0) & (lsoId > 0)) {

				newOccupancy = occupancyAgent.addOccupancyAddress(occupancy);

			} else {
				newOccupancy = occupancyAgent.updateOccupancyAddress(occupancy);

			}

			// Occupancy newOccupancy = occupancyAgent.updateOccupancyAddress(occupancy);
			if (newOccupancy != null)
				logger.debug("Created new occupancy and obtained new occupancy object with id" + newOccupancy.getLsoId());

			request.setAttribute("occupancy", newOccupancy);
		} catch (Exception e) {
			logger.error("Error thrown, not successful in Save Occuapncy Address" + e.getMessage());
			System.out.println("Error in SaveOccupancyAddressAction");
		}
		return (mapping.findForward("success"));
	}

}