package elms.control.actions.lso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.CommonAgent;
import elms.agent.AddressAgent;
import elms.app.common.Condition;
import elms.app.lso.Address;
import elms.app.lso.ForeignAddress;
import elms.app.lso.Occupancy;
import elms.app.lso.OccupancyAddress;
import elms.app.lso.OccupancyApn;
import elms.app.lso.OccupancyDetails;
import elms.app.lso.Owner;
import elms.app.lso.Street;
import elms.common.Constants;
import elms.control.beans.ConditionForm;
import elms.control.beans.ViewOccupancyForm;
import elms.util.StringUtils;

public class ViewOccupancyAction extends Action {
	static Logger logger = Logger.getLogger(ViewOccupancyAction.class.getName());
	String apn = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		logger.info("Entering ViewOccupancyAction");

		ViewOccupancyForm viewOccupancyForm = (ViewOccupancyForm) form;
		ActionErrors errors = new ActionErrors();

		try {
			String lsoId = request.getParameter("lsoId");
			session.setAttribute(Constants.LSO_ID, lsoId);

			int intLsoId = StringUtils.s2i(lsoId);
			apn = viewOccupancyForm.getApn();
			logger.debug("obtained apn from form is " + apn);
			processOccupancy(request, intLsoId, apn);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		logger.info("Exiting ViewOccupancyAction");

		return (mapping.findForward("success"));
	}

	public void processOccupancy(HttpServletRequest request, int intLsoId, String apn) {
		logger.info("processOccupancy(request, " + intLsoId + ", " + apn + ")");

		HttpSession session = request.getSession();

		session.setAttribute(Constants.LSO_ID, StringUtils.i2s(intLsoId));
		session.setAttribute(Constants.LSO_TYPE, Constants.OCCUPANCY);
		logger.debug("LSO_ID is set in the session as  " + (String) session.getAttribute(Constants.LSO_ID));


		session.setAttribute(Constants.PSA_ID, StringUtils.i2s(intLsoId));
		logger.debug("set the psa type into session under Constants.PSA_ID as " + (String) session.getAttribute(Constants.PSA_ID));
		session.setAttribute(Constants.PSA_TYPE, "O");
		logger.debug("set the psa type into session under Constants.PSA_TYPE as " + (String) session.getAttribute(Constants.PSA_TYPE));
			
			
		String onloadAction = "parent.f_lso.location.href='" + request.getContextPath() + "/lsoSearch.do?lsoNodeId=" + intLsoId + "'";
		logger.debug("Setting the refresh tree flag to " + onloadAction);
		//override on load parameter if coming from tree
		String from = request.getParameter("from")!=null ? request.getParameter("from") : "";
		if(from.equalsIgnoreCase("tree")){
			onloadAction ="parent.f_psa.location.href='" + request.getContextPath() + "/viewPsaTree.do?lsoId=" + intLsoId + "'";
		}

		session.setAttribute("onloadAction", onloadAction);
		session.removeAttribute("siteStructureForm");

		ViewOccupancyForm viewOccupancyForm = new ViewOccupancyForm();
		viewOccupancyForm.setApn(apn);
		viewOccupancyForm.setLsoId(StringUtils.i2s(intLsoId));

		try {
			Occupancy occupancy = new AddressAgent().getOccupancy(intLsoId);

			if (occupancy != null) {
				String lsoId = StringUtils.i2s(occupancy.getLsoId());
				logger.debug("setting lsoId to the request " + lsoId);
				request.setAttribute("lsoId", lsoId);

				OccupancyDetails occupancyDetails = occupancy.getOccupancyDetails();

				if (occupancyDetails != null) {
					String alias = occupancyDetails.getAlias();
					logger.debug("setting alias to the request " + alias);
					request.setAttribute("alias", alias);

					String description = occupancyDetails.getDescription();
					logger.debug("setting description to the request " + description);
					request.setAttribute("description", description);

					String use = occupancyDetails.getUseAsString();
					logger.debug("setting use to the request " + use);
					request.setAttribute("use", use);

					String unitNumber = occupancyDetails.getUnitNumber();
					logger.debug("setting unitNumber to the request " + unitNumber);
					request.setAttribute("unitNumber", unitNumber);

					String beginFloor = occupancyDetails.getBeginFloor();
					logger.debug("setting beginFloor to the request " + beginFloor);
					request.setAttribute("beginFloor", beginFloor);

					String endFloor = occupancyDetails.getEndFloor();
					logger.debug("setting endFloor to the request " + endFloor);
					request.setAttribute("endFloor", endFloor);

					String active = occupancyDetails.getActive();
					logger.debug("setting active to the request " + active);
					request.setAttribute("active", active);

					String label = occupancyDetails.getLabel();
					logger.debug("setting label to the request " + label);
					request.setAttribute("label", label);
					
					String householdSize = occupancyDetails.getHouseholdSize();
					logger.debug("setting householdSize to the request " + householdSize);
					request.setAttribute("householdSize", householdSize);
					
					String bedroomSize = occupancyDetails.getBedroomSize();
					logger.debug("setting bedroomSize to the request " + bedroomSize);
					request.setAttribute("bedroomSize", bedroomSize);
					
					String grossRent = occupancyDetails.getGrossRent();
					logger.debug("setting grossRent to the request " + grossRent);
					request.setAttribute("grossRent", grossRent);
					
					String utilityAllowance = occupancyDetails.getUtilityAllowance();
					logger.debug("setting v to the request " + utilityAllowance);
					request.setAttribute("utilityAllowance", utilityAllowance);
					
					String propertyManager = occupancyDetails.getPropertyManager();
					logger.debug("setting propertyManager to the request " + propertyManager);
					request.setAttribute("propertyManager", propertyManager);
					
					String restrictedUnits = occupancyDetails.getRestrictedUnits();
					logger.debug("setting restrictedUnits to the request " + restrictedUnits);
					request.setAttribute("restrictedUnits", restrictedUnits);

					String pZone = "";

					if (occupancyDetails.getParkingZone() != null) {
						pZone = occupancyDetails.getParkingZone().getName();
					}

					logger.debug("setting pzone to the request " + pZone);
					request.setAttribute("pZone", pZone);

					String rZone = occupancyDetails.getRZone();

					if (rZone == null) {
						rZone = "";
					}

					if (rZone.equals("66")) {
						rZone = "R-1";
					} else if (rZone.equals("67")) {
						rZone = "R-4";
					} else if (rZone.equals("68")) {
						rZone = "None";
					} else if (rZone.equals("69")) {
						rZone = "C-1";
					}

					request.setAttribute("rZone", rZone);
				}
				// end if occupancyDetails

				List occupancyAddress = occupancy.getOccupancyAddress();

				if (occupancyAddress != null) {
					if (occupancyAddress.size() > 0) {
						OccupancyAddress occupancyAddressObj = (OccupancyAddress) occupancyAddress.get(0);

						if (occupancyAddressObj != null) {
							viewOccupancyForm.setAddressStreetNumber(StringUtils.i2s(occupancyAddressObj.getStreetNbr()));
							viewOccupancyForm.setAddressStreetModifier(occupancyAddressObj.getStreetModifier());

							Street addressStreet = occupancyAddressObj.getStreet();

							if (addressStreet != null) {
								viewOccupancyForm.setAddressStreetName(StringUtils.nullReplaceWithEmpty(addressStreet.getStreetName()));
								logger.debug(" Street name  deva as " + viewOccupancyForm.getAddressStreetName());

								String lsoAddress = occupancyAddressObj.getStreetNbr() + " " + StringUtils.nullReplaceWithEmpty(occupancyAddressObj.getStreetModifier()) + " " + StringUtils.nullReplaceWithEmpty(addressStreet.getStreetName());
								session.setAttribute("lsoAddress", lsoAddress);
								logger.debug("The Attribute lsoAddress is set in the session as " + lsoAddress);
								session.removeAttribute("psaInfo");
							}

							viewOccupancyForm.setAddressCity(occupancyAddressObj.getCity());
							viewOccupancyForm.setAddressState(occupancyAddressObj.getState());
							viewOccupancyForm.setAddressUnit(occupancyAddressObj.getUnit());
							
							logger.debug(" got the unit number deva as " + viewOccupancyForm.getAddressUnit());
							viewOccupancyForm.setAddressZip(occupancyAddressObj.getZip());
							viewOccupancyForm.setAddressZip4(occupancyAddressObj.getZip4());

							session.setAttribute("city", viewOccupancyForm.getAddressCity());
							session.setAttribute("state", viewOccupancyForm.getAddressState());
							session.setAttribute("unit", viewOccupancyForm.getAddressUnit());
							session.setAttribute("zip", viewOccupancyForm.getAddressZip());
							
							viewOccupancyForm.setAddressPrimary(StringUtils.yn(occupancyAddressObj.getPrimary()));
							viewOccupancyForm.setAddressActive(StringUtils.yn(occupancyAddressObj.getActive()));
							viewOccupancyForm.setAddressDescription(occupancyAddressObj.getDescription());
						}
					}
				}

				List occupancyApn = occupancy.getOccupancyApn();

				if (occupancyApn != null) {
					if (occupancyApn.size() == 0) {
						request.setAttribute("occupancyApnList", new ArrayList());
					} else {
						request.setAttribute("occupancyApnList", occupancyApn);
						logger.debug("set occupancy apn list to request with size" + occupancyApn.size());

						int j = 0;

						for (int i = 0; i < occupancyApn.size(); i++) {
							OccupancyApn occupancyApnObj = (OccupancyApn) occupancyApn.get(i);

							if ((occupancyApnObj.getApn()).equals(apn)) {
								j = i;
							}
						}

						logger.debug("selecting the element number of " + j + " from occupancy list");

						OccupancyApn occupancyApnObj = (OccupancyApn) occupancyApn.get(j);

						if (occupancyApnObj != null) {
							apn = occupancyApnObj.getApn();
							viewOccupancyForm.setApn(apn);
							session.setAttribute(Constants.APN, apn);
							logger.debug("Occupancy APN set to " + apn);

							Owner owner = occupancyApnObj.getOwner();

							if (owner != null) {
								viewOccupancyForm.setApnOwnerId(StringUtils.i2s(owner.getOwnerId()));
								viewOccupancyForm.setApnOwnerName(owner.getName());
								viewOccupancyForm.setApnEmail(owner.getEmail());
								viewOccupancyForm.setApnPhone(owner.getPhone());
								viewOccupancyForm.setApnFax(owner.getFax());
								viewOccupancyForm.setApnforeignflag(owner.getForeignFlag());

								Address localAddress = owner.getLocalAddress();

								if (localAddress != null) {
									viewOccupancyForm.setApnStreetNumber(StringUtils.zeroReplaceWithEmpty(localAddress.getStreetNbr()));
									logger.debug(" got street nbr " + viewOccupancyForm.getApnStreetNumber());
									viewOccupancyForm.setApnStreetModifier(localAddress.getStreetModifier());
									viewOccupancyForm.setApnCity(localAddress.getCity());
									viewOccupancyForm.setApnState(localAddress.getState());
									viewOccupancyForm.setApnZip(localAddress.getZip());

									Street localAddressStreet = localAddress.getStreet();

									if (localAddressStreet != null) {
										viewOccupancyForm.setApnStreetName(StringUtils.nullReplaceWithEmpty(localAddressStreet.getStreetName()));
										viewOccupancyForm.setApnStreetDirection(StringUtils.nullReplaceWithEmpty(localAddressStreet.getStreetPrefix()));

										logger.debug(" got apn street name as " + StringUtils.nullReplaceWithEmpty(localAddressStreet.getStreetName()));
									}
								}

								ForeignAddress foreignAddress = owner.getForeignAddress();

								if (foreignAddress != null) {
									viewOccupancyForm.setApnForeignAddress1(foreignAddress.getLineOne());
									viewOccupancyForm.setApnForeignAddress2(foreignAddress.getLineTwo());
									viewOccupancyForm.setApnForeignAddress3(foreignAddress.getLineThree());
									viewOccupancyForm.setApnForeignAddress4(foreignAddress.getLineFour());
									viewOccupancyForm.setApnCountry(foreignAddress.getCountry());
								}
							}
						}
					}
				}

				request.setAttribute("viewOccupancyForm", viewOccupancyForm);

				// setting Holdlist
				List holds = new ArrayList();
				holds = occupancy.getLsoHold();
				request.setAttribute("holds", holds);
				logger.debug("set Holds to request with size " + holds.size());

				if (new CommonAgent().editable(intLsoId, "O")) {
					request.setAttribute("editable", "true");
				} else {
					request.setAttribute("editable", "false");
				}
				
				// setting Tenant List
				List tenants = new ArrayList();
				tenants = occupancy.getLsoTenant();
				request.setAttribute("tenants", tenants);
				logger.debug("set tenants to request with size " + tenants.size());

				// **** Condition List
				ConditionForm condForm = new ConditionForm();
				CommonAgent condAgent = new CommonAgent();
				condForm.setLevelId(intLsoId);
				condForm.setConditionLevel("O");
				condForm = condAgent.populateConditionForm(condForm);

				Condition[] conditions = condForm.getCondition();
				List condList = Arrays.asList(conditions);

				if (condList.size() > 3) {
					condList = condList.subList(0, 3);
				}

				request.setAttribute("condList", condList);

				// list of comments for this lso.
				List commentList = new CommonAgent().getCommentList(intLsoId, 3, "O");
				request.setAttribute("commentList", commentList);

				// list of attachments for this lso.
				List attachmentList = new CommonAgent().getAttachments(intLsoId, "O");
				request.setAttribute("attachmentList", attachmentList);
			}

			logger.info("Exiting ViewOccupancyAction .. processOccupancy()");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}
}
