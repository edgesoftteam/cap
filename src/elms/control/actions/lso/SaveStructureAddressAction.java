package elms.control.actions.lso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.AddressAgent;
import elms.app.lso.Street;
import elms.app.lso.Structure;
import elms.app.lso.StructureAddress;
import elms.app.lso.StructureDetails;
import elms.app.lso.StructureSiteData;
import elms.control.beans.StructureForm;
import elms.util.StringUtils;

public class SaveStructureAddressAction extends Action {

	static Logger logger = Logger.getLogger(SaveStructureAddressAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		StructureForm structureForm = (StructureForm) form;

		try {
			int lsoId = 0;
			String strLsoId = request.getParameter("lsoId");
			if ((strLsoId != null) && (!strLsoId.equals(""))) {
				lsoId = Integer.parseInt(request.getParameter("lsoId"));
			} else {
				logger.error("***********error lsoId = " + lsoId);
			}

			StructureDetails structureDetails = new StructureDetails();
			List structureAddress = new ArrayList();
			StructureAddress structureAddressObj = new StructureAddress();
			structureAddressObj.setStreetModifier(structureForm.getAddressStreetModifier());
			logger.debug("got street mod");
			structureAddressObj.setStreetNbr(StringUtils.s2i(structureForm.getAddressStreetNumber()));
			logger.debug("got street no");
			structureAddressObj.setCity(structureForm.getAddressCity());
			logger.debug("got city");
			structureAddressObj.setState(structureForm.getAddressState());
			logger.debug("got state");
			structureAddressObj.setZip(structureForm.getAddressZip());
			logger.debug("got zip");
			structureAddressObj.setZip4(structureForm.getAddressZip4());
			logger.debug("got zip4");
			structureAddressObj.setDescription(structureForm.getAddressDescription());
			logger.debug("got desc");
			structureAddressObj.setActive(StringUtils.b2s(structureForm.getAddressActive()));
			logger.debug("got active");
			structureAddressObj.setPrimary(StringUtils.b2s(structureForm.getAddressPrimary()));
			logger.debug("got primary");
			Street street = new Street();
			street.setStreetId(StringUtils.s2i(structureForm.getAddressStreetName()));
			logger.debug("set the street name value is " + structureForm.getAddressStreetName());
			structureAddressObj.setStreet(street);
			structureAddress.add(structureAddressObj);

			List structureHold = new ArrayList();
			List structureCondition = new ArrayList();
			List structureAttachment = new ArrayList();
			List structureComment = new ArrayList();
			StructureSiteData structureSiteData = new StructureSiteData();
			Structure structure = new Structure(lsoId, 0, structureDetails, structureAddress, structureHold, structureCondition, structureAttachment, structureSiteData, structureComment);
			logger.debug("Land object created for new land creation");

			AddressAgent structureAgent = new AddressAgent();
			logger.debug("before sending to the structure agent");
			Structure newStructure = structureAgent.updateStructureAddress(structure);
			if (newStructure != null)
				logger.debug("Created new structure and obtained new structure object with id" + newStructure.getLsoId());

			request.setAttribute("structure", newStructure);
		} catch (Exception e) {
			logger.error("Error thrown, not successful in saveStructure address" + e.getMessage());
		}
		return (mapping.findForward("success"));

	}

}