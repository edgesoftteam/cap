package elms.control.actions.lso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.AddressAgent;
import elms.app.lso.Address;
import elms.app.lso.ForeignAddress;
import elms.app.lso.Occupancy;
import elms.app.lso.OccupancyApn;
import elms.app.lso.OccupancyDetails;
import elms.app.lso.OccupancySiteData;
import elms.app.lso.Owner;
import elms.app.lso.Street;
import elms.control.beans.OccupancyForm;
import elms.util.StringUtils;

public class SaveOccupancyApnAction extends Action {

	static Logger logger = Logger.getLogger(SaveOccupancyApnAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		// Extract attributes and parameters we will need
		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		OccupancyForm occupancyForm = (OccupancyForm) form;

		try {
			int lsoId = 0;
			String strLsoId = request.getParameter("lsoId");
			if ((strLsoId != null) && (!strLsoId.equals(""))) {
				lsoId = StringUtils.s2i(strLsoId);
			} else {
				lsoId = 0;
			}
			int ownerId = 0;
			String strOwnerId = request.getParameter("ownerId");
			if ((strOwnerId != null) && (!strOwnerId.equals(""))) {
				ownerId = StringUtils.s2i(strOwnerId);
			} else {
				ownerId = 0;
			}

			OccupancyDetails occupancyDetails = new OccupancyDetails();
			List occupancyAddress = new ArrayList();
			List occupancyHold = new ArrayList();
			List occupancyCondition = new ArrayList();
			List occupancyAttachment = new ArrayList();
			List occupancyComment = new ArrayList();
			List occupancyApn = new ArrayList();

			OccupancyApn occupancyApnObj = new OccupancyApn();
			occupancyApnObj.setApn(occupancyForm.getApn());
			Owner owner = new Owner();
			owner.setEmail(occupancyForm.getEmail());
			owner.setFax(occupancyForm.getFax());
			owner.setForeignFlag(occupancyForm.getForeignAddress());
			owner.setName(occupancyForm.getOwnerName());
			owner.setPhone(occupancyForm.getPhone());
			owner.setOwnerId(StringUtils.s2i(occupancyForm.getOwnerId()));

			ForeignAddress foreignAddress = new ForeignAddress();
			foreignAddress.setCountry(occupancyForm.getCountry());
			foreignAddress.setLineOne(occupancyForm.getLine1());
			foreignAddress.setLineTwo(occupancyForm.getLine2());
			foreignAddress.setLineThree(occupancyForm.getLine3());
			foreignAddress.setLineFour(occupancyForm.getLine4());
			owner.setForeignAddress(foreignAddress);

			Address localAddress = new Address();
			localAddress.setCity(occupancyForm.getApnCity());
			localAddress.setState(occupancyForm.getApnState());

			Street localAddressStreet = new Street();
			String streetName = "" + occupancyForm.getApnStreetName();
			localAddressStreet.setStreetName(streetName);
			localAddress.setStreet(localAddressStreet);

			localAddress.setStreetModifier(occupancyForm.getApnStreetMod());
			localAddress.setStreetNbr(StringUtils.s2i(occupancyForm.getApnStreetNumber()));
			localAddress.setZip(occupancyForm.getApnZip());
			owner.setLocalAddress(localAddress);
			occupancyApnObj.setOwner(owner);
			occupancyApn.add(occupancyApnObj);

			OccupancySiteData occupancySiteData = new OccupancySiteData();
			Occupancy occupancy = new Occupancy(lsoId, 0, occupancyDetails, occupancyApn, occupancyApn, occupancyHold, occupancyCondition, occupancyAttachment, occupancySiteData, occupancyComment);
			logger.debug("Occupancy object created for new occupancy creation");

			AddressAgent occupancyAgent = new AddressAgent();
			logger.debug("before sending to the occupancy agent");
			Occupancy newOccupancy = null;

			if ((ownerId == 0) & (lsoId > 0)) {

				newOccupancy = occupancyAgent.addOccupancyApn(occupancy);

			} else {
				newOccupancy = occupancyAgent.updateOccupancyApn(occupancy);

			}

			// Occupancy newOccupancy = occupancyAgent.updateOccupancyApn(occupancy);
			if (newOccupancy != null) {
				if (mapping.getAttribute() != null) {
					if ("request".equals(mapping.getScope()))
						request.removeAttribute(mapping.getAttribute());
					else
						session.removeAttribute(mapping.getAttribute());
				}

				lsoId = newOccupancy.getLsoId();
				String apn = "";
				logger.debug("Created new occupancy and obtained new occupancy object with id" + lsoId);
				ViewOccupancyAction viewOccupancyAction = new ViewOccupancyAction();
				viewOccupancyAction.processOccupancy(request, lsoId, apn);
			}

		} catch (Exception e) {
			logger.error("Error thrown, not successful in saveOccupancy address" + e.getMessage());
		}
		return (mapping.findForward("success"));
	}

}