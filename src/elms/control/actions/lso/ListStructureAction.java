package elms.control.actions.lso;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import sun.jdbc.rowset.CachedRowSet;
import elms.agent.AddressAgent;

public class ListStructureAction extends Action {

	static Logger logger = Logger.getLogger(ListStructureAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		MessageResources messages = getResources();
		HttpSession session = request.getSession();

		try {

			String lsoId = request.getParameter("lsoId");
			logger.info("Entering ListLandAction with lsoId of " + lsoId);
			AddressAgent LsoAgent = new AddressAgent();

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}

			CachedRowSet structureRowSet = (CachedRowSet) new AddressAgent().getLsoList(lsoId);

			request.setAttribute("structureList", structureRowSet);
			request.setAttribute("lsoId", lsoId);

			logger.info("Exiting ListStructureAction");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));
	}

}