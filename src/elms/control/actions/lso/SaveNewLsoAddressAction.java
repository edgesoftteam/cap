package elms.control.actions.lso;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import sun.jdbc.rowset.CachedRowSet;
import elms.agent.AddressAgent;
import elms.app.lso.LsoAddress;
import elms.app.lso.Street;
import elms.control.beans.LsoForm;
import elms.security.User;
import elms.util.StringUtils;

public class SaveNewLsoAddressAction extends Action {

	static Logger logger = Logger.getLogger(SaveNewLsoAddressAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		LsoForm lsoForm = (LsoForm) form;
		String origin = "";

		try {
			origin = "";
			int lsoId = -1;
			String strLsoId = request.getParameter("lsoId");
			lsoId = StringUtils.s2i(strLsoId);

			LsoAddress lsoAddressObj = new LsoAddress();
			lsoAddressObj.setStreetModifier(lsoForm.getAddressStreetModifier());
			logger.debug("got street mod " + lsoForm.getAddressStreetModifier());
			lsoAddressObj.setStreetNbr(StringUtils.s2i(lsoForm.getAddressStreetNumber()));
			logger.debug("got street no");
			lsoAddressObj.setUnit(lsoForm.getAddressUnit());
			lsoAddressObj.setCity(lsoForm.getAddressCity());
			logger.debug("got city " + lsoForm.getAddressCity());
			lsoAddressObj.setState(lsoForm.getAddressState());
			logger.debug("got state" + lsoForm.getAddressState());
			lsoAddressObj.setZip(lsoForm.getAddressZip());
			logger.debug("got zip" + lsoForm.getAddressZip());
			lsoAddressObj.setZip4(lsoForm.getAddressZip4());
			logger.debug("got zip4" + lsoForm.getAddressZip4());
			lsoAddressObj.setDescription(lsoForm.getAddressDescription());
			logger.debug("got desc" + lsoForm.getAddressDescription());
			lsoAddressObj.setActive(StringUtils.b2s(lsoForm.getAddressActive()));
			logger.debug("got active" + StringUtils.b2s(lsoForm.getAddressActive()));
			lsoAddressObj.setPrimary(StringUtils.b2s(lsoForm.getAddressPrimary()));
			logger.debug("got primary" + StringUtils.b2s(lsoForm.getAddressPrimary()));
			User user = (User) session.getAttribute("user");
			lsoAddressObj.setUpdatedBy(user);
			logger.debug("set updated user details" + (user.getUserId()));
			Street street = new Street();
			street.setStreetId(StringUtils.s2i(lsoForm.getAddressStreetName()));
			logger.debug("set the street name value is " + lsoForm.getAddressStreetName());
			lsoAddressObj.setStreet(street);
			lsoAddressObj.setLsoId(lsoId);
			logger.debug("set lso id to address object " + lsoId);

			AddressAgent lsoAgent = new AddressAgent();
			logger.debug("before sending to the lso agent");
			String lsoType = lsoAgent.getLsoType(lsoId);
			logger.debug("obtained lso type as " + lsoType);
			lsoAddressObj = lsoAgent.addLsoAddress(lsoAddressObj, lsoType);
			logger.debug("insertion successful - obtained lso address object to action");
			if (lsoAddressObj != null) {
				if (mapping.getAttribute() != null) {
					if ("request".equals(mapping.getScope()))
						request.removeAttribute(mapping.getAttribute());
					else
						session.removeAttribute(mapping.getAttribute());
				}

				CachedRowSet lsoAddressRowSet = (CachedRowSet) new AddressAgent().getLsoAddressList(strLsoId);

				request.setAttribute("lsoAddressList", lsoAddressRowSet);
				request.setAttribute("lsoId", strLsoId);
				lsoType = new AddressAgent().getLsoType(lsoId);
				String lsoTypeFull = "";
				if (lsoType.equalsIgnoreCase("L"))
					lsoTypeFull = "Land";
				else if (lsoType.equalsIgnoreCase("S"))
					lsoTypeFull = "Structure";
				else if (lsoType.equalsIgnoreCase("O"))
					lsoTypeFull = "Occupancy";
				request.setAttribute("lsoType", lsoTypeFull);
				logger.debug("set lso type to page as " + lsoTypeFull);
				origin = request.getParameter("origin");
			}

			logger.debug("Forward path is- " + mapping.findForward(origin).getPath());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		logger.debug("The origin of this form is " + origin);
		if (origin.equals("LM"))
			return (mapping.findForward("landmgr"));
		else if (origin.equals("SM"))
			return (mapping.findForward("structuremgr"));
		else if (origin.equals("OM"))
			return (mapping.findForward("occupancymgr"));
		else {
			request.setAttribute("ed", "true");
			return (mapping.findForward("success"));
		}

	}

}