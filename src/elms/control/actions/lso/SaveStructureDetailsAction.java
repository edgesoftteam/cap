package elms.control.actions.lso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AddressAgent;
import elms.app.lso.Street;
import elms.app.lso.Structure;
import elms.app.lso.StructureAddress;
import elms.app.lso.StructureDetails;
import elms.app.lso.StructureSiteData;
import elms.app.lso.Use;
import elms.control.beans.StructureForm;
import elms.util.StringUtils;

public class SaveStructureDetailsAction extends Action {

	static Logger logger = Logger.getLogger(SaveStructureDetailsAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		StructureForm structureForm = (StructureForm) form;

		try {
			int lsoId = -1;
			String strLsoId = structureForm.getLsoId();
			lsoId = StringUtils.s2i(strLsoId);

			List useList = new ArrayList();
			String[] use = structureForm.getSelectedUse();

			for (int i = 0; i < use.length; i++) {
				Use useObj = new AddressAgent().getUse(use[i], "S");
				logger.debug("adding to useList value " + useObj.getDescription().trim() + "  " + useObj.getId());
				useList.add(useObj);
			}
			logger.debug("Use List created for new structure creation");

			String active = StringUtils.b2s(structureForm.getActive());
			logger.debug("obtained value for active as " + active);

			// land details object here
			StructureDetails structureDetails = new StructureDetails(structureForm.getAlias(), structureForm.getDescription(), useList, active, structureForm.getTotalFloors(), structureForm.getLabel());

			logger.debug("Structure details object created for new Structure creation");
			List structureAddress = new ArrayList();
			logger.debug("Structure address object created for new Structure creation");
			List structureHold = new ArrayList();
			logger.debug("Structure hold list object created for new Structure creation");
			List structureCondition = new ArrayList();
			logger.debug("Structure condition list object created for new Structure creation");
			List structureAttachment = new ArrayList();
			logger.debug("Structure attachment list object created for new Structure creation");
			List structureComment = new ArrayList();
			logger.debug("Structure comment list object created for new Structure creation");
			StructureSiteData structureSiteData = new StructureSiteData();
			logger.debug("Structure site data object created for new Structure creation");
			Structure structure = new Structure(lsoId, 0, structureDetails, structureAddress, structureHold, structureCondition, structureAttachment, structureSiteData, structureComment);
			logger.debug("structure object created for new structure creation");

			logger.debug("before sending to the structure agent");
			structure = new AddressAgent().updateStructureDetails(structure);
			if (structure != null) {
				structureDetails = structure.getStructureDetails();
				if (structureDetails != null) {
					strLsoId = StringUtils.i2s(structure.getLsoId());
					logger.debug("setting lsoId to the request " + strLsoId);
					request.setAttribute("lsoId", strLsoId);
					String alias = structureDetails.getAlias();
					logger.debug("setting alias to the request " + alias);
					request.setAttribute("alias", alias);
					String description = structureDetails.getDescription();
					logger.debug("setting description to the request " + description);
					request.setAttribute("description", description);
					String totalFloors = structureDetails.getTotalFloors();
					logger.debug("setting totalFloors to the request " + totalFloors);
					request.setAttribute("totalFloors", totalFloors);
					String strUse = structureDetails.getUseAsString();
					logger.debug("setting use to the request " + strUse);
					request.setAttribute("use", strUse);
					active = structureDetails.getActive();
					logger.debug("setting active to the request " + active);
					request.setAttribute("active", active);
				}
				List structureAddressList = structure.getStructureAddress();
				if (structureAddressList != null) {
					if (structureAddressList.size() > 0) {
						StructureAddress structureAddressObj = (StructureAddress) structureAddressList.get(0);
						if (structureAddressObj != null) {
							String addressStreetNumber = StringUtils.i2s(structureAddressObj.getStreetNbr());
							logger.debug("setting addressStreetNumber to the request " + addressStreetNumber);
							request.setAttribute("addressStreetNumber", addressStreetNumber);
							String addressStreetModifier = structureAddressObj.getStreetModifier();
							logger.debug("setting addressStreetModifier to the request " + addressStreetModifier);
							request.setAttribute("addressStreetModifier", addressStreetModifier);
							Street addressStreet = structureAddressObj.getStreet();
							if (addressStreet != null) {
								String addressStreetName = addressStreet.getStreetName();
								logger.debug("setting addressStreetName to the request " + addressStreetName);
								request.setAttribute("addressStreetName", addressStreetName);
							}
							String addressCity = structureAddressObj.getCity();
							logger.debug("setting addressCity to the request " + addressCity);
							request.setAttribute("addressCity", addressCity);
							String addressState = structureAddressObj.getState();
							logger.debug("setting addressState to the request " + addressState);
							request.setAttribute("addressState", addressState);
							String addressZip = structureAddressObj.getZip();
							logger.debug("setting addressZip to the request " + addressZip);
							request.setAttribute("addressZip", addressZip);
							String addressZip4 = structureAddressObj.getZip4();
							logger.debug("setting addressZip4 to the request " + addressZip4);
							request.setAttribute("addressZip4", addressZip4);
							String addressPrimary = StringUtils.yn(structureAddressObj.getPrimary());
							logger.debug("setting addressPrimary to the request " + addressPrimary);
							request.setAttribute("addressPrimary", addressPrimary);
							String addressActive = StringUtils.yn(structureAddressObj.getActive());
							logger.debug("setting addressActive to the request " + addressActive);
							request.setAttribute("addressActive", addressActive);
							String addressDescription = structureAddressObj.getDescription();
							logger.debug("setting addressDescription to the request " + addressDescription);
							request.setAttribute("addressDescription", addressDescription);
						}
					}
				}
			}

			// Transfering request to viewStructureAction
			if (structure != null) {
				lsoId = structure.getLsoId();
				logger.debug("obtained structure for id as " + lsoId);
				ViewStructureAction viewStructureAction = new ViewStructureAction();
				viewStructureAction.processStructure(request, StringUtils.i2s(lsoId), "0");
				logger.debug("set view Structure action with reqd values");

			}

			logger.debug("request set to the page with structure object");

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));
	}

}