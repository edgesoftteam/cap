package elms.control.actions.lso;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.AddressAgent;
import elms.app.lso.LsoAddress;
import elms.control.beans.StructureForm;
import elms.util.StringUtils;

public class EditStructureAddressAction extends Action {

	static Logger logger = Logger.getLogger(EditStructureAddressAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		MessageResources messages = getResources();
		HttpSession session = request.getSession();

		try {

			int addressId = -1;
			String strAddressId = request.getParameter("addressId");
			addressId = StringUtils.s2i(strAddressId);
			int lsoId = -1;
			String strLsoId = request.getParameter("lsoId");
			lsoId = StringUtils.s2i(strLsoId);
			LsoAddress lsoAddress = null;

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}

			ActionForm frm = new StructureForm();
			request.setAttribute("structureForm", frm);
			request.setAttribute("lsoId", strLsoId);
			StructureForm structureForm = (StructureForm) frm;
			logger.debug("created structure form for addressId = " + addressId);

			lsoAddress = new AddressAgent().getLsoAddress(addressId);
			logger.debug("obtained the lso address object as " + lsoAddress);
			structureForm.setLsoAddress(lsoAddress);

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));
	}

}