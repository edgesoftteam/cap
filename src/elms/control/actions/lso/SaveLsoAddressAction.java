package elms.control.actions.lso;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import sun.jdbc.rowset.CachedRowSet;
import elms.agent.AddressAgent;
import elms.app.lso.LsoAddress;
import elms.app.lso.Street;
import elms.control.beans.LsoForm;
import elms.security.User;
import elms.util.StringUtils;

public class SaveLsoAddressAction extends Action {

	static Logger logger = Logger.getLogger(SaveLsoAddressAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		LsoForm lsoForm = (LsoForm) form;

		try {

			int addressId = -1;
			String strAddressId = request.getParameter("addressId");
			addressId = StringUtils.s2i(strAddressId);

			int lsoId = -1;
			String strLsoId = request.getParameter("lsoId");
			lsoId = StringUtils.s2i(strLsoId);

			LsoAddress lsoAddressObj = new LsoAddress();
			lsoAddressObj.setAddressId(addressId);
			logger.debug("set address id " + addressId);
			lsoAddressObj.setLsoId(lsoId);
			logger.debug("set Lso id " + lsoId);
			lsoAddressObj.setStreetModifier(lsoForm.getAddressStreetModifier());
			logger.debug("got street mod " + lsoForm.getAddressStreetModifier());
			lsoAddressObj.setStreetNbr(StringUtils.s2i(lsoForm.getAddressStreetNumber()));
			logger.debug("got street no");
			lsoAddressObj.setUnit(lsoForm.getAddressUnit());
			logger.debug("got unit " + lsoForm.getAddressUnit());
			lsoAddressObj.setCity(lsoForm.getAddressCity());
			logger.debug("got city " + lsoForm.getAddressCity());
			lsoAddressObj.setState(lsoForm.getAddressState());
			logger.debug("got state" + lsoForm.getAddressState());
			lsoAddressObj.setZip(lsoForm.getAddressZip());
			logger.debug("got zip" + lsoForm.getAddressZip());
			lsoAddressObj.setZip4(lsoForm.getAddressZip4());
			logger.debug("got zip4" + lsoForm.getAddressZip4());
			lsoAddressObj.setDescription(lsoForm.getAddressDescription());
			logger.debug("got desc" + lsoForm.getAddressDescription());
			lsoAddressObj.setActive(StringUtils.b2s(lsoForm.getAddressActive()));
			logger.debug("got active" + StringUtils.b2s(lsoForm.getAddressActive()));
			lsoAddressObj.setPrimary(StringUtils.b2s(lsoForm.getAddressPrimary()));
			logger.debug("got primary" + StringUtils.b2s(lsoForm.getAddressPrimary()));
			User user = (User) session.getAttribute("user");
			lsoAddressObj.setUpdatedBy(user);
			logger.debug("set updated user details" + (user.getUserId()));
			Street street = new Street();
			street.setStreetId(StringUtils.s2i(lsoForm.getAddressStreetName()));
			logger.debug("set the street name value is " + lsoForm.getAddressStreetName());
			lsoAddressObj.setStreet(street);

			AddressAgent lsoAgent = new AddressAgent();
			logger.debug("before sending to the lso agent");
			lsoAddressObj = lsoAgent.updateLsoAddress(lsoAddressObj);
			if (lsoAddressObj != null) {
				CachedRowSet lsoAddressRowSet = (CachedRowSet) new AddressAgent().getLsoAddressList(strLsoId);
				logger.debug("obtained cached row set");
				request.setAttribute("lsoAddressList", lsoAddressRowSet);
				logger.debug("set cached row set to request");
				request.setAttribute("lsoId", strLsoId);
				logger.debug("set lso id to attribute :" + strLsoId);
				String lsoType = new AddressAgent().getLsoType(lsoId);
				String lsoTypeFull = "";
				if (lsoType.equalsIgnoreCase("L"))
					lsoTypeFull = "Land";
				else if (lsoType.equalsIgnoreCase("S"))
					lsoTypeFull = "Structure";
				else if (lsoType.equalsIgnoreCase("O"))
					lsoTypeFull = "Occupancy";
				request.setAttribute("lsoType", lsoTypeFull);
				logger.debug("set lso type to page as " + lsoTypeFull);

			}

			logger.debug("Forward path is- " + mapping.findForward("success").getPath());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return (mapping.findForward("success"));

	}

}