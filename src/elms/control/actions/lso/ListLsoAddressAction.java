package elms.control.actions.lso;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import sun.jdbc.rowset.CachedRowSet;
import elms.agent.AddressAgent;
import elms.util.StringUtils;

public class ListLsoAddressAction extends Action {

	static Logger logger = Logger.getLogger(ListLsoAddressAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		try {
			String lsoId = request.getParameter("lsoId");
			logger.info("Entering ListLandAction with lsoId of " + lsoId);
			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}
			CachedRowSet lsoAddressRowSet = (CachedRowSet) new AddressAgent().getLsoAddressList(lsoId);
			request.setAttribute("lsoAddressList", lsoAddressRowSet);
			request.setAttribute("lsoId", lsoId);
			String lsoType = new AddressAgent().getLsoType(StringUtils.s2i(lsoId));
			String lsoTypeFull = "";
			if (lsoType.equalsIgnoreCase("L"))
				lsoTypeFull = "Land";
			else if (lsoType.equalsIgnoreCase("S"))
				lsoTypeFull = "Structure";
			else if (lsoType.equalsIgnoreCase("O"))
				lsoTypeFull = "Occupancy";
			request.setAttribute("lsoType", lsoTypeFull);
			logger.debug("set lso type to page as " + lsoTypeFull);
			logger.info("Exiting ListLsoAddressAction");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return (mapping.findForward("success"));
	}
}