package elms.control.actions.lso;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AddressAgent;

public class AddOccupancyAction extends Action {

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		String parentId = request.getParameter("parentId");
		if (mapping.getAttribute() != null) {
			if ("request".equals(mapping.getScope()))
				request.removeAttribute(mapping.getAttribute());
			else
				session.removeAttribute(mapping.getAttribute());
		}

		request.setAttribute("parentId", parentId);
		java.util.List streetList = new AddressAgent().getStreetArrayList();
		request.setAttribute("streetList", streetList);
		List occupancyUseList = new AddressAgent().getUseList("O");
		request.setAttribute("occupancyUseList", occupancyUseList);

		return (mapping.findForward("success"));

	}

}