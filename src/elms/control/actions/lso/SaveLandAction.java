package elms.control.actions.lso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AddressAgent;
import elms.app.admin.ParkingZone;
import elms.app.lso.Address;
import elms.app.lso.ForeignAddress;
import elms.app.lso.Land;
import elms.app.lso.LandAddress;
import elms.app.lso.LandApn;
import elms.app.lso.LandDetails;
import elms.app.lso.LandSiteData;
import elms.app.lso.Owner;
import elms.app.lso.Street;
import elms.app.lso.Use;
import elms.app.lso.Zone;
import elms.common.Constants;
import elms.control.beans.LandForm;
import elms.exception.DuplicateLandException;
import elms.security.User;

public class SaveLandAction extends SaveLsoAction {
	static Logger logger = Logger.getLogger(SaveLandAction.class.getName());
	protected int landId;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		LandForm landForm = (LandForm) form;

		ActionErrors errors = new ActionErrors();

		if (!errors.empty()) {
			saveErrors(request, errors);
			saveToken(request);

			return (new ActionForward(mapping.getInput()));
		} else {
			try {
				LandDetails landDetails = this.getLandDetails(landForm);
				List landAddress = this.getLandAddressList(landForm, session);
				List landApn = this.getLandApnList(landForm, session);
				List landHold = new ArrayList();
				List landCondition = new ArrayList();
				List landAttachment = new ArrayList();
				List landComment = new ArrayList();
				LandSiteData landSiteData = new LandSiteData();
				Land land = new Land(0, landDetails, landAddress, landApn, landHold, landCondition, landAttachment, landSiteData, landComment);
				logger.debug("Land object created for new land creation");

				landId = new AddressAgent().addLand(land);
			} catch (DuplicateLandException dle) {
				logger.debug("Duplicate land exception occured " + dle.getMessage());
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.land.duplicate"));
				saveErrors(request, errors);
				return (new ActionForward(mapping.getInput()));
			} catch (Exception e) {
				throw new ServletException("Problem while adding land " + e.getMessage());
			}
		}

		return (mapping.findForward("success"));
	}

	public LandDetails getLandDetails(LandForm landForm) {
		LandDetails landDetails = null;

		try {
			// use list object here
			List useList = new ArrayList();
			String[] use = landForm.getSelectedUse();

			for (int i = 0; i < use.length; i++) {
				AddressAgent useAgent = new AddressAgent();
				Use useObj = useAgent.getUse(use[i], "L");
				useList.add(useObj);
			}

			logger.debug("Use List created for new land creation");

			// zone list object here
			List zoneList = new ArrayList(); // zone ready here
			String[] zone = landForm.getSelectedZone();

			if (zone != null) {
				for (int i = 0; i < zone.length; i++) {
					AddressAgent zoneAgent = new AddressAgent();
					Zone zoneObj = zoneAgent.getZone(zone[i]);
					zoneList.add(zoneObj);
				}
			}

			logger.debug("Zone List created for new land creation");

			String active = (landForm.getActive()) ? "Y" : "N";
			landDetails = new LandDetails(landForm.getAlias(), landForm.getDescription(), useList, active, zoneList, landForm.getCoordinateX(), landForm.getCoordinateY(), landForm.getLabel());
			logger.debug("Land details object created for new land creation");

			// PZONE and RZONE
			ParkingZone pzone = new ParkingZone();
			pzone.setPzoneId(landForm.getParkingZone());
			landDetails.setParkingZone(pzone);
			landDetails.setRZone(landForm.getResZone());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return landDetails;
	}

	public List getLandAddressList(LandForm landForm, HttpSession session) throws Exception {
		List landAddress = new ArrayList();
		Street addressStreet = new Street();

		int addressStreetNumber = 0;
		String strAddressStreetNumber = landForm.getAddressStreetNumber();
		String strAddressStreetName = landForm.getAddressStreetName();

		if ((strAddressStreetNumber != null) && !(strAddressStreetNumber.equals(""))) {
			addressStreetNumber = Integer.parseInt(strAddressStreetNumber);
		}

		if ((strAddressStreetName != null) && !(strAddressStreetName.equals(""))) {
			AddressAgent streetAgent = new AddressAgent();
			addressStreet = streetAgent.getStreet(Integer.parseInt(strAddressStreetName));
		}

		String addressActive = (landForm.getAddressActive()) ? "Y" : "N";
		String addressPrimary = (landForm.getAddressPrimary()) ? "Y" : "N";

		// Added by Raj for Add checking land address to be identical.
		logger.debug("The reqiured parameters are:  " + landForm.getAddressStreetModifier() + strAddressStreetName + addressStreetNumber + addressActive + addressPrimary);

		boolean result = new AddressAgent().checkLandAlreadyExist(landForm.getAddressStreetModifier(), addressStreetNumber, strAddressStreetName);

		if (result == true) {
			logger.debug("Land already exist " + result);
			throw new DuplicateLandException("landAddress already exist");
		} else {
			logger.debug("land does not exist, continuing to add land");
		}

		LandAddress landAddressObj = new LandAddress(0, 0, addressStreetNumber, landForm.getAddressStreetModifier(), addressStreet, landForm.getAddressCity(), landForm.getAddressState(), landForm.getAddressZip(), landForm.getAddressZip4(), landForm.getAddressDescription(), addressPrimary, addressActive);
		landAddressObj.setUpdatedBy((User) session.getAttribute(Constants.USER_KEY));
		logger.debug("Userid in SaveLandAction is " + landAddressObj.getUpdatedBy().getUserId());
		landAddress.add(landAddressObj);
		logger.debug("Land address object created for new land creation");

		return landAddress;
	}

	public List getLandApnList(LandForm landForm, HttpSession session) {
		List landApn = new ArrayList();

		try {
			LandApn landApnObj = new LandApn();

			if ((landForm.getApn() != null) && !(landForm.getApn().equals(""))) {
				logger.debug(" Apn is not null or empty ");

				landApnObj.setApn(landForm.getApn());

				int apnStreetNumber = 0;
				String strApnStreetNumber = landForm.getApnStreetNumber();

				if ((strApnStreetNumber != null) && !(strApnStreetNumber.equals(""))) {
					apnStreetNumber = Integer.parseInt(strApnStreetNumber);
				}

				logger.debug("got apn as :" + apnStreetNumber);

				AddressAgent streetAgent = new AddressAgent();
				Street apnStreet = new Street();
				apnStreet.setStreetName(landForm.getApnStreetName());
				logger.debug("created apnStreet object as " + apnStreet);

				String apnZip4 = "0000";
				Address localAddress = new Address(apnStreetNumber, landForm.getApnStreetModifier(), apnStreet, landForm.getApnCity(), landForm.getApnState(), landForm.getApnZip(), apnZip4);
				logger.debug("created localAddress as" + localAddress);

				ForeignAddress foreignAddress = new ForeignAddress(landForm.getApnForeignAddress1(), landForm.getApnForeignAddress2(), landForm.getApnForeignAddress3(), landForm.getApnForeignAddress4(), landForm.getApnCountry());
				logger.debug("created foreign address as " + foreignAddress);

				String foreignFlag = landForm.getApnForeignAddress();
				Owner owner = new Owner(0, landForm.getApnOwnerName(), localAddress, foreignFlag, foreignAddress, landForm.getApnPhone(), landForm.getApnFax(), landForm.getApnEmail());
				owner.setUpdatedBy((User) session.getAttribute(Constants.USER_KEY));
				logger.debug("created owner as" + owner);
				landApnObj.setOwner(owner);
				landApnObj.setActive("Y");
				logger.debug("added owner to land apn object");
			}

			landApn.add(landApnObj);
			logger.debug("added land apn object to land apn array");
		} catch (Exception e) {
			logger.error("Thrown in getLand apn of save land action");
		}

		return landApn;
	}
}
