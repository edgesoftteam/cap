package elms.control.actions.lso;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class AddOccupancyApnAction extends Action {

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		try {

			String lsoId = request.getParameter("lsoId"); // 21762
			String ownerId = "0"; // for new address

			// Remove the obsolete form bean

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}

			request.setAttribute("lsoId", lsoId);
			request.setAttribute("ownerId", ownerId);

			// AddressAgent streetAgent = new AddressAgent();
			// java.util.List streetList = streetAgent.getStreetArrayList();
			// request.setAttribute("streetList", streetList);

		} catch (Exception e) {
			// logger.warn("error anand"+ e.getMessage());
			System.out.println("Error in EditOccupancyAddressAction");
		}
		return (mapping.findForward("success"));

	}
}