package elms.control.actions.lso;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.util.StringUtils;

public class AddLandAddressAction extends Action {

	static Logger logger = Logger.getLogger(AddLandAddressAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		MessageResources messages = getResources();
		HttpSession session = request.getSession();

		try {
			int lsoId = -1;
			String strLsoId = request.getParameter("lsoId");
			lsoId = StringUtils.s2i(strLsoId);
			request.setAttribute("lsoId", strLsoId);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return (mapping.findForward("success"));
	}

}