package elms.control.actions.lso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AddressAgent;
import elms.app.admin.lso.OccupancyEdit;
import elms.app.admin.lso.StructureEdit;
import elms.app.lso.StreetModifier;
import elms.common.Constants;
import elms.control.beans.AddressSummaryForm;
import elms.security.User;
import elms.util.StringUtils;

/**
 * @author shekhar
 * 
 *         To change this generated comment edit the template variable "typecomment": Window>Preferences>Java>Templates. To enable and disable the creation of type comments go to Window>Preferences>Java>Code Generation.
 */
public class StructureAddressMaintenanceAction extends Action {

	static Logger logger = Logger.getLogger(StructureAddressMaintenanceAction.class.getName());

	protected AddressSummaryForm asFrm;
	protected List structures;
	protected StructureEdit structure;
	protected AddressAgent lsoAgent;
	protected String nextPage = "samePage";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.info("Entering Structure Address Maintenance Action");

		lsoAgent = new AddressAgent();
		structures = new ArrayList();

		HttpSession session = request.getSession();
		asFrm = (AddressSummaryForm) session.getAttribute("addressSummaryForm");
		String landId = asFrm.getLsoId();
		logger.debug("The land Id of The Structure is .." + landId);
		String lsoId = (String) request.getAttribute("theLsoId");
		logger.debug("The Structure Id is .." + lsoId);
		if (lsoId != null) {
			StructureEdit structure = lsoAgent.getStructureEdit(StringUtils.s2i(lsoId));
			structures.add(structure);
			asFrm.getAddressSummary().setStructureList(structures);
			nextPage = "samePage";
		} else {
			StructureEdit[] structureArray = asFrm.getAddressSummary().getStructureList();
			StructureEdit structure = structureArray[0];
			lsoId = structure.getStructureId();

			List occupancies = StringUtils.arrayToList(structure.getOccupancyList());
			if (request.getParameter("addressSummary.structureList[0].addOccupancy") != null) {
				OccupancyEdit oe = new OccupancyEdit();
				oe.setIsLot("N");
				occupancies.add(oe);
				structure.setOccupancyList(occupancies);
				nextPage = "samePage";
			} else if (request.getParameter("addressSummary.structureList[0].save") != null) {
				structure.setUpdatedBy((User) session.getAttribute(Constants.USER_KEY));
				Iterator occIter = occupancies.iterator();
				int i = 0;
				List newOccupancies = new ArrayList();
				while (occIter.hasNext()) {
					OccupancyEdit occupancy = (OccupancyEdit) occIter.next();
					String active = request.getParameter("addressSummary.structureList[0].occupancyList[" + i + "].active");
					occupancy.setActive(active);
					occupancy.setCreatedBy((User) session.getAttribute(Constants.USER_KEY));
					occupancy.setUpdatedBy((User) session.getAttribute(Constants.USER_KEY));
					newOccupancies.add(occupancy);
					i++;
				}
				structure.setOccupancyList(newOccupancies);
				structure = lsoAgent.updateStructure(structure);
				nextPage = "addressSummary";
			}
			structures.add(structure);
			asFrm.getAddressSummary().setStructureList(structures);

		}

		session.setAttribute("addressSummaryForm", asFrm);

		List streets = new AddressAgent().getStreets(landId);
		request.setAttribute("streets", streets);

		List modifiers = new ArrayList();
		modifiers.add(new StreetModifier("1/4"));
		modifiers.add(new StreetModifier("1/2"));
		modifiers.add(new StreetModifier("3/4"));
		request.setAttribute("modifiers", modifiers);

		return (mapping.findForward(nextPage));

		/*
		 * Iterator iter = occupancies.iterator(); int i = 0; while (iter.hasNext()){ OccupancyEdit occupancy = (OccupancyEdit)iter.next(); if (occupancy.getDelete() == null || occupancy.getDelete().equals("")){ occupancy.setUpdatedBy((User) session.getAttribute(Constants.USER_KEY)); if (occupancy.getOccupancyId() == null || occupancy.getOccupancyId().equals("")){ //lots.add(lsoAgent.addLot(lotEdit)); lsoAgent.addOccupancy(occupancy,StringUtils.s2i(lsoId)); } else{ String active = request.getParameter("addressSummary.structureList[0].occupancyList["+ i +"].active"); occupancy.setActive(active); lsoAgent.updateOccupancy(occupancy); } }else{ if (occupancy.getOccupancyId() != null && !occupancy.getOccupancyId().equals("")) lsoAgent.deleteOccupancy(occupancy); } i++; } occupancies = lsoAgent.getOccupancies(StringUtils.s2i(lsoId)); }
		 * 
		 * structure.setOccupancyList(occupancies);
		 */
		// asFrm.getAddressSummary().setStructureList(structures);
		// }

	}

}
