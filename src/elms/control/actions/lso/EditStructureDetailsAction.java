package elms.control.actions.lso;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.AddressAgent;
import elms.app.lso.Structure;
import elms.app.lso.StructureDetails;
import elms.control.beans.StructureForm;
import elms.util.StringUtils;

public class EditStructureDetailsAction extends Action {

	static Logger logger = Logger.getLogger(EditStructureDetailsAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		MessageResources messages = getResources();
		HttpSession session = request.getSession();

		try {

			int lsoId = -1;
			String strLsoId = request.getParameter("lsoId");
			if ((strLsoId != null) && (!strLsoId.equals(""))) {
				lsoId = Integer.parseInt(request.getParameter("lsoId"));
			}
			if (lsoId != -1) {
				Structure structure = null;

				structure = new AddressAgent().getStructure(lsoId);
				logger.debug("Got the structure object for above lso id of " + lsoId);

				if (mapping.getAttribute() != null) {
					if ("request".equals(mapping.getScope()))
						request.removeAttribute(mapping.getAttribute());
					else
						session.removeAttribute(mapping.getAttribute());
				}

				ActionForm frm = new StructureForm();
				request.setAttribute("structureForm", frm);
				request.setAttribute("lsoId", strLsoId);
				StructureForm structureForm = (StructureForm) frm;

				if (structure != null) {
					StructureDetails structureDetails = structure.getStructureDetails();
					if (structureDetails != null) {
						boolean active = StringUtils.s2b(structureDetails.getActive());
						structureForm.setActive(active);
						logger.debug("set active to structure form as " + active);
						structureForm.setAlias(structureDetails.getAlias());
						logger.debug("set alias to structure form as " + structureDetails.getAlias());
						structureForm.setDescription(structureDetails.getDescription());
						logger.debug("set desc to structure form as " + structureDetails.getDescription());
						structureForm.setSelectedUse(structureDetails.getUseAsArray());
						logger.debug("set selected use to structure form " + structureDetails.getUseAsArray());
						structureForm.setTotalFloors(structureDetails.getTotalFloors());
						logger.debug("set total floors to structure form as " + structureDetails.getTotalFloors());
						structureForm.setLabel(structureDetails.getLabel());
						logger.debug("setLabel to structure form as " + structureDetails.getLabel());
					}
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return (mapping.findForward("success"));
	}

}