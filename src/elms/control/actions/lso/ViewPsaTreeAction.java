package elms.control.actions.lso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ProjectAgent;
import elms.app.lso.PsaTree;
import elms.control.beans.PsaTreeForm;

public class ViewPsaTreeAction extends Action {
	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(ViewPsaTreeAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.info("Entering ViewPsaTreeAction");
		HttpSession session = request.getSession();
		String nextPage = "";

		session.setAttribute("onloadAction", "");
		ProjectAgent psaTreeAgent = new ProjectAgent();

		PsaTreeForm frm = (PsaTreeForm) session.getAttribute("psaTreeForm");
		if (frm == null) {
			frm = new PsaTreeForm();
		}

		String lsoStr = request.getParameter("lsoId");
		if ((lsoStr == null) || lsoStr.equals("-1")) {
			nextPage = "framePsa";
		} else {
			nextPage = "success";
			long lsoId = -1;
			try {
				lsoId = new Long(lsoStr).longValue();
			} catch (NumberFormatException e) {
				// do nothing
			}

			// set radio_active by default
			String radio = frm.getRadiobutton();
			if (radio.equals("")) {
				frm.setRadiobutton("radio_active");
			}

			boolean active = !(frm.getRadiobutton().equals("radio_all"));
			String psaNodeId = request.getParameter("psaNodeId");

			// true gets only active projects, false gets all the projects
			try {
				logger.debug("lsoId is:" + lsoId);
				logger.debug("boolean is:" + active);

				List psaTreeList = null;
				// check for out of town address
				String actId = psaTreeAgent.checkForOutOfTownAddress(psaNodeId);
				if (actId.equals("") || actId == "") {
					psaTreeList = psaTreeAgent.getPsaTreeList(lsoId, active, request.getContextPath()); // getPsaTree(long lsoId, boolean active)
				} else {
					psaTreeList = psaTreeAgent.getOnlyOnePsaTreeList(lsoId, active, psaNodeId, request.getContextPath()); // for out of town address
				}

				logger.debug("Got the PSA tree, checking for non-locational projects");
				// get NonLocational Projects - 6 CityWide Address Range search
				psaTreeList = psaTreeAgent.getNonLocationProjects(psaTreeList, lsoId, active, request.getContextPath()); // Non Loaction address also

				// Create all the javascript out here instead of in the browser
				List treeLinks = new ArrayList();
				StringBuffer link = new StringBuffer();
				for (int i = 0; i < psaTreeList.size(); i++) {
					PsaTree item = (PsaTree) psaTreeList.get(i);
					link = new StringBuffer();
					link.append("Tree[" + i + "]=\"" + item.getNodeId() + "|");
					link.append(item.getNodeLevel() + "|" + "");
					link.append((item.isOnHold() == true ? "<font color='" + (item.getHoldColor() != null ? item.getHoldColor() : "black") + "'><b>(H)</b></font>" : ""));
					link.append(item.getNodeText() + "|");
					link.append(item.getNodeLink() + "|");
					link.append(item.getNodeAlt() + "|");
					link.append(item.getLsoType() + "|");
					link.append(item.getStatus() + "|");
					link.append(item.getId() + "|\";");
					treeLinks.add(link.toString());
					logger.debug(link);
				}

				request.setAttribute("psaTreeList", treeLinks);

				session.setAttribute("psaTreeForm", frm);

				logger.info("Psa Tree List Created : " + psaTreeList.size() + " Elements");

			} catch (Exception e1) {
				logger.error("unknown exception occured " + e1.getMessage());
				throw new ServletException(e1.getMessage());
			}
		}
		return (mapping.findForward(nextPage));
	}
}
