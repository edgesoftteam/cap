package elms.control.actions.lso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AddressAgent;
import elms.app.admin.ParkingZone;
import elms.app.admin.lso.AddressRangeEdit;
import elms.app.lso.Land;
import elms.app.lso.LandDetails;
import elms.app.lso.LandSiteData;
import elms.app.lso.Use;
import elms.app.lso.Zone;
import elms.common.Constants;
import elms.control.beans.LandForm;
import elms.util.StringUtils;

public class SaveLandDetailsAction extends Action {

	static Logger logger = Logger.getLogger(SaveLandDetailsAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		LandForm landForm = (LandForm) form;

		try {
			int lsoId = -1;
			String strLsoId = landForm.getLsoId();
			lsoId = StringUtils.s2i(strLsoId);

			List useList = new ArrayList();
			String[] use = landForm.getSelectedUse();

			for (int i = 0; i < use.length; i++) {
				Use useObj = new AddressAgent().getUse(use[i], "L");
				logger.debug("adding to useList value " + useObj.getDescription().trim());
				useList.add(useObj);
			}
			logger.debug("Use List created for new land creation");

			// zone list object here
			List zoneList = new ArrayList(); // zone ready here
			if (landForm.getSelectedZone() != null) {
				String[] zone = landForm.getSelectedZone();
				for (int i = 0; i < zone.length; i++) {
					AddressAgent zoneAgent = new AddressAgent();
					Zone zoneObj = zoneAgent.getZone(zone[i]);
					logger.debug("adding to zone list value " + zoneObj.getDescription().trim());
					zoneList.add(zoneObj);
				}
			}
			logger.debug("Zone List created for new land creation");

			String active = StringUtils.b2s(landForm.getActive());
			logger.debug("obtained value for active as " + active);

			// land details object here
			LandDetails landDetails = new LandDetails(landForm.getAlias(), landForm.getDescription(), useList, active, zoneList, landForm.getCoordinateX(), landForm.getCoordinateY(), landForm.getLabel());

			// PZONE and RZONE
			ParkingZone pzone = new ParkingZone();
			pzone.setPzoneId(landForm.getParkingZone());
			landDetails.setParkingZone(pzone);
			landDetails.setRZone(landForm.getResZone());
			/**
			 * Loop thru the AddressRangeList. Ignore the ones that are deleted and create an arraylist of AddressRangeEdit
			 */

			List addressRangeList = new ArrayList();
			AddressRangeEdit[] addressRanges = landForm.getAddressRangeList();
			for (int i = 0; i < addressRanges.length; i++) {
				if (addressRanges[i].getDelete().equals(""))
					addressRangeList.add(addressRanges[i]);
			}
			logger.debug("AddressRangeList(" + addressRangeList.size() + ")");

			landDetails.setAddressRangeList(addressRangeList);
			logger.debug("Land details object created for new land creation");
			List landAddress = new ArrayList();
			logger.debug("Land address object created for new land creation");
			List landApn = new ArrayList();
			logger.debug("Land apn list object created for new land creation");
			List landHold = new ArrayList();
			logger.debug("Land hold list object created for new land creation");
			List landCondition = new ArrayList();
			logger.debug("Land condition list object created for new land creation");
			List landAttachment = new ArrayList();
			logger.debug("Land attachment list object created for new land creation");
			List landComment = new ArrayList();
			logger.debug("Land comment list object created for new land creation");
			LandSiteData landSiteData = new LandSiteData();
			logger.debug("Land site data object created for new land creation");
			Land land = new Land(lsoId, landDetails, landAddress, landApn, landHold, landCondition, landAttachment, landSiteData, landComment);
			logger.debug("Land object created for new land creation");

			logger.debug("before sending to the land agent");
			land = new AddressAgent().updateLandDetails(land);
			if (land != null) {
				String apn = "";
				strLsoId = StringUtils.i2s(land.getLsoId());
				logger.debug("setting lsoId to the request " + strLsoId);
				request.setAttribute("lsoId", strLsoId);
				session.setAttribute(Constants.LSO_ID, strLsoId);
				logger.debug("obtained apn from form is " + apn);
				ViewLandAction viewLandAction = new ViewLandAction();
				viewLandAction.processLand(request, lsoId, apn);

			}

			logger.debug("request set to the page with land object");

		} catch (Exception e) {
			logger.error("Error thrown, not successful in saveLand details" + e.getMessage());
		}

		return (mapping.findForward("success"));
	}

}