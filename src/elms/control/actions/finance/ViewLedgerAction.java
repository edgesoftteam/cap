package elms.control.actions.finance;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.RowSet;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.FinanceAgent;
import elms.control.beans.LedgerForm;

public class ViewLedgerAction extends Action {

	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(ViewLedgerAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();

		int activityId = 0;
		int paymentId = 0;
		String activityNbr = "";

		try {

			LedgerForm ledgerForm = new LedgerForm();

			try {
				LedgerForm frm = (LedgerForm) form;
				activityId = frm.getActivityId();
				activityNbr = frm.getActivityNbr();
				paymentId = frm.getPaymentId();
			} catch (Exception e) {
				logger.debug("No Stuff in form");
			}

			if (activityId == 0) {
				activityId = Integer.parseInt(request.getParameter("activityId"));
				activityNbr = request.getParameter("activityNbr");

				try {
					paymentId = Integer.parseInt(request.getParameter("paymentId"));
				} catch (Exception e) {
					logger.debug("No Detail");
				}
			}

			String tmpAction = request.getParameter("editaction");
			logger.debug(" tmp Action : " + tmpAction);

			if (!"edit".equals(tmpAction)) {
				paymentId = 0;
			}

			logger.debug("Activity Id : " + activityId);
			logger.debug("Activtiy Nbr: " + activityNbr);
			logger.debug("Payment Id : " + paymentId);

			ledgerForm.setActivityId(activityId);
			ledgerForm.setActivityNbr(activityNbr);
			ledgerForm.setPaymentId(paymentId);

			RowSet rs = new FinanceAgent().getPaymentHistory(activityId);
			ledgerForm.setTransactionList(rs);
			logger.debug("Transaction List set to form");

			if (paymentId > 0) {
				rs = new FinanceAgent().getPaymentDetailHistory(paymentId);
				ledgerForm.setDetailList(rs);
				logger.debug("Detail List set to form");
			}

			session.setAttribute("ledgerForm", ledgerForm);
			logger.info("Exiting ViewLedgerAction");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));
	}
}
