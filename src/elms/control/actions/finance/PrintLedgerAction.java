package elms.control.actions.finance;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class PrintLedgerAction extends Action {

	static Logger logger = Logger.getLogger(ViewLedgerAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		// int activityId=0,paymentId =0;
		// String activityNbr = "";

		try {
			logger.info("Exiting ViewLedgerAction");
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}

		return (mapping.findForward("success"));

	}

}