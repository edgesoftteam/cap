package elms.control.actions.finance;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.FinanceAgent;
import elms.app.finance.LedgerEdit;
import elms.app.finance.PaymentDetailEdit;
import elms.control.beans.LedgerForm;
import elms.util.db.Wrapper;

public class SaveLedgerAction extends Action {

	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(SaveLedgerAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();

		try {
			int activityId = 0;
			int paymentId = 0;
			logger.info("Entering SaveLedgerAction");

			LedgerForm ledgerForm = (LedgerForm) form;
			activityId = ledgerForm.getActivityId();
			paymentId = ledgerForm.getPaymentId();

			if (!(request.getParameter("Cancel") == null)) {
				logger.debug("Cancel Button Pressed");
				request.setAttribute("levelId", "" + activityId);
				request.setAttribute("level", "A");
				session.removeAttribute("ledgerForm");

				return (mapping.findForward("cancel"));
			} else {
				LedgerEdit[] transactionList = ledgerForm.getTransactionList();
				PaymentDetailEdit[] detailList = ledgerForm.getDetailList();

				Wrapper db = new Wrapper();
				FinanceAgent financeAgent = new FinanceAgent();
				financeAgent.savePaymentComments(db, transactionList, detailList);

				session.setAttribute("ledgerForm", ledgerForm);

				logger.info("Exiting SaveLedgerAction");

				return (mapping.findForward("success"));
			}
		} catch (Exception e) {
			logger.error("Exception occured while saving ledger action " + e.getMessage());
			throw new ServletException(e);
		}
	}
}
