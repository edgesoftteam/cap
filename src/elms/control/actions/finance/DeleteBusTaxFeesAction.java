/*
 * Created on Dec 11, 2008
 *
 * Window - Preferences - Java - Code Style - Code Templates
 */
package elms.control.actions.finance;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.FinanceAgent;
import elms.control.beans.FeesMgrForm;
import elms.util.StringUtils;

/**
 * @author Gayathri
 * 
 *         Window - Preferences - Java - Code Style - Code Templates
 */
public class DeleteBusTaxFeesAction extends Action {

	boolean chkError = true;
	ActionErrors errors = new ActionErrors();

	static Logger logger = Logger.getLogger(DeleteBusTaxFeesAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();

		logger.info("Entering DeleteBusTaxFeesAction");

		FeesMgrForm feesMgrForm = (FeesMgrForm) session.getAttribute("feesMgrForm");

		int activityId = 0;
		int peopleId = 0;

		// setting the onloadAction of next page not to refresh PSA tree
		String onloadAction = "";
		session.setAttribute("onloadAction", onloadAction);

		try {
			activityId = StringUtils.s2i(request.getParameter("activityId"));
			peopleId = StringUtils.s2i(request.getParameter("pId"));

			FinanceAgent financeAgent = new FinanceAgent();

			if (!(peopleId == -1 || peopleId == 0) && !(activityId == -1 || activityId == 0)) {

				chkError = financeAgent.deleteBusinessTax(activityId, peopleId);

				if (chkError == false) {
					if (errors.empty()) {
						errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.feeMgr.btFee"));
					}
					if (!errors.empty()) {
						saveErrors(request, errors);
					}
				}
			}

			request.setAttribute("id", elms.util.StringUtils.i2s(activityId));
			request.setAttribute("onloadScript", "frmLoad()");
			logger.debug("Exiting from DeletebusTaxFee Action");

			return (mapping.findForward("success"));
		} catch (Exception e) {
			return (mapping.findForward("error"));
		}
	}

}
