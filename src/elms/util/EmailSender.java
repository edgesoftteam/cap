package elms.util;

import java.net.URI;
import java.util.Map;

import org.apache.log4j.Logger;

import elms.common.Constants;
import elms.control.beans.EmailDetails;
import elms.control.beans.EmailTemplateAdminForm;

public class EmailSender {
	
	static Logger logger = Logger.getLogger(EmailSender.class); 
	
	EmailTemplateAdminForm emailTemplateAdminForm;
		
	public void sendEmail(EmailDetails emailDetails){
		try {			
				Map<String, String> lkupSystemDataMap = emailDetails.getLkupSystemDataMap();  
				
				String serviceURL = lkupSystemDataMap.get(Constants.EMAIL_SERVER_URL);
				String userName = lkupSystemDataMap.get(Constants.EMAIL_AUTHENTICATION_USERNAME);
				String password = lkupSystemDataMap.get(Constants.EMAIL_AUTHENTICATION_PASSWORD); 
				String fromAddress = lkupSystemDataMap.get(Constants.EMAIL_FROM_ADDRESS);
				String emailForStaff = lkupSystemDataMap.get(Constants.EMAIL_FOR_STAFF);
				
				logger.error("serviceURL :: " +serviceURL+ " :: userName :: " +userName+ " :: password :: " +password+ " :: fromAddress :: " +fromAddress + " :: emailForStaff ::" +emailForStaff);
				logger.debug("emaildetails... "+emailDetails.getEmailId());
				microsoft.exchange.webservices.data.ExchangeService service = new microsoft.exchange.webservices.data.ExchangeService();
				microsoft.exchange.webservices.data.ExchangeCredentials credentials = new microsoft.exchange.webservices.data.WebCredentials(userName, password);
				service.setCredentials(credentials);
					
			    service.setUrl(new URI(serviceURL)); 
			    microsoft.exchange.webservices.data.EmailMessage msg;				
				msg = new microsoft.exchange.webservices.data.EmailMessage(service);
				
				emailTemplateAdminForm = emailDetails.getEmailTemplateAdminForm();
				
			    msg.setSubject(emailTemplateAdminForm.getEmailSubject()); 
			    msg.setBody(microsoft.exchange.webservices.data.MessageBody.getMessageBodyFromText(emailTemplateAdminForm.getEmailMessage()));
			    msg.getToRecipients().add(emailDetails.getEmailId());
			    msg.sendAndSaveCopy();				
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}