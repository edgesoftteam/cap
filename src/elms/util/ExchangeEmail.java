package elms.util;

import java.net.URI;
import java.util.HashMap;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import elms.util.db.Wrapper;
import microsoft.exchange.webservices.data.BodyType;
import microsoft.exchange.webservices.data.EmailMessage;
import microsoft.exchange.webservices.data.ExchangeCredentials;
import microsoft.exchange.webservices.data.ExchangeService;
import microsoft.exchange.webservices.data.MessageBody;
import microsoft.exchange.webservices.data.WebCredentials;
 

public class ExchangeEmail {

	

	static Logger logger = Logger.getLogger(ExchangeEmail.class.getName());

	public static ResourceBundle elmsProperties;
	
	
	public ExchangeEmail() { 
		
		
			try {
				if (elmsProperties == null) {
					elmsProperties = Wrapper.getResourceBundle();
				}
			} catch (Exception e) {
				logger.fatal("Exception occured while reading properties " + e.getMessage());
			}
		


		String email = elmsProperties.getString("EMAIL_USERNAME").trim();
		String password = elmsProperties.getString("EMAIL_PASSWORD").trim(); 
		String uri = elmsProperties.getString("EMAIL_SERVER").trim(); 
		String emailturn = elmsProperties.getString("EMAIL_TURN").trim(); 
		String defaultemail = elmsProperties.getString("DEFAULT_EMAIL").trim(); 
		String attachmentUrl = "";
		
		if(Operator.hasValue(uri)){
			set("URI", uri);
		}else{
			set("URI", "https://outlook.office365.com/ews/Exchange.asmx");
		}
		
		if(Operator.hasValue(password)){
			set("PASSWORD", password);
		}else{
			set("PASSWORD", "Edge2007");
		}
		
		if(Operator.hasValue(email)){
			set("EMAIL", email);
		}else{
			set("EMAIL", "support@edgesoftinc.com");
		}
		
		if(Operator.hasValue(attachmentUrl)){
			set("ATTACH_URL", attachmentUrl);
		}else{
			set("ATTACH_URL", "C:\\temp\\exchange\\");
		}
		
		if(Operator.hasValue(emailturn)){
			set("EMAIL_TURN", emailturn);
		}else{
			set("EMAIL_TURN", "OFF");
		}
		
		if(Operator.hasValue(defaultemail)){
			set("DEFAULT_EMAIL", defaultemail);
		}else{
			set("DEFAULT_EMAIL", "svijay@edgesoftinc.com");
		}
		
		
	}
	
	
	
	
	
	
	
	 private static HashMap<String, String> KEYS = new HashMap<String, String>();
	
	/* #################################################################################################
	##   SET METHODS                                                                                  ##
	################################################################################################# */

	    /**
	     * Set values for specified field.
	     * 
	     * 
	     * RECIPIENT: email address(es) to send email to. Separate multiple email addresses with a comma.
	     * FROM: email address to place in the from field of the email.
	     * REPLYTO: email address where replies to the email should be sent
	     * CC: email address(es) to send a carbon copy of the email. Separate multiple email addresses with a comma.
	     * BCC: email address(es) to send a blind carbon copy of the email. Separate multiple email addresses with a comma.
	     * SUBJECT: Subject of the email
	     * TEXTCONTENT: Body of the email
	     * HTMLCONTENT: Body of the email in HTML format
	     * 
	     * @param field - field name.
	     * @param value - value to be set to specified field name
	     */
	    public void set(String field, String value) {
	        if (!Operator.hasValue(value)) { value = ""; }
	        if (Operator.hasValue(field))  {
	            String FIELD = field.toUpperCase();
	            if (FIELD.equals("URI")        ||
	            	FIELD.equals("PASSWORD")        ||
	            	FIELD.equals("EMAIL")        ||
	            	FIELD.equals("ATTACH_URL")        ||
	            	FIELD.equals("EMAIL_TURN")        ||
	            	FIELD.equals("DEFAULT_EMAIL")        ||
	                FIELD.equals("RECIPIENT")   ||
	                FIELD.equals("FROM")        ||
	                FIELD.equals("REPLYTO")     ||
	                FIELD.equals("CC")          ||
	                FIELD.equals("BCC")         ||
	                FIELD.equals("SUBJECT")     ||
	                FIELD.equals("ATTACHMENTS")          ||
					FIELD.equals("TEXTCONTENT") ||
	                FIELD.equals("HTMLCONTENT")) {
	                this.KEYS.put(FIELD, value);
	            }
	        }
	    }

	    public void setRecipient(String email) {
	    	set("RECIPIENT", email);
	    }
	    
	   

	    public void setCC(String email) {
	    	set("CC", email);
	    }

	    public void setBCC(String email) {
	    	set("BCC", email);
	    }

	    public void setReplyTo(String email) {
	    	set("REPLYTO", email);
	    }

	    public void setSubject(String subject) {
	    	set("SUBJECT", subject);
	    }

	    public void setContent(String content) {
	    	set("HTMLCONTENT", content);
	    }
	    
	    public void setAttachments(String attachments) {
	    	set("ATTACHMENTS", attachments);
	    }

	    /**
	     * Retrieve value of specified field
	     * 
	     * @param field - field name
	     * @return the value of specified field
	     */
	    public static String get(String field) {
	        String FIELD = field.toUpperCase();
	        String value = (String) KEYS.get(FIELD);
	        return value;
	    }
	
    public static boolean sendEmail(String to,String cc, String bcc,String subject,String body,String attachments){
    	if(!Operator.hasValue(to)) { return false; } 
    	String t[] = Operator.split(to,",");
    	String c[] = Operator.split(cc,",");
    	String b[] = Operator.split(bcc,",");
    	String a[] = Operator.split(attachments,",");
    	
    	return sendEmail(t,c,b,subject,body,a);
	}
    
    public boolean deliver() {
        return sendEmail(get("RECIPIENT"),get("CC"),get("BCC"),get("SUBJECT"), get("HTMLCONTENT"),get("ATTACHMENTS"));
    }
	
	public static boolean sendEmail(String[] to,String[] cc,String[] bcc,String subject,String body){
		if(!Operator.hasValue(to)) { return false; }
		return sendEmail(to,cc,bcc,subject,body,null);
	}
	
	public static boolean sendEmail(String[] to,String[] cc,String[] bcc,String subject,String body,String[] attachments){
		if(!Operator.hasValue(to)) { return false; }
		boolean result = false;
		logger.info(" DOIN eMAIL ");
		try{
			int l = Operator.toInt(Operator.toString(to.length));
			if(l>0){ 
				ExchangeService service = new ExchangeService();
			    ExchangeCredentials credentials = new WebCredentials(get("EMAIL"),get("PASSWORD"));
			    service.setCredentials(credentials);
			    service.setUrl(new URI(get("URI"))); 
			    
			    EmailMessage msg  = new EmailMessage(service);
			    logger.info(" DOIN eMAIL &&&&&&"+get("EMAIL_TURN"));
			    logger.info(" DOIN eMAIL *********"+get("DEFAULT_EMAIL"));
			    
			    if (get("EMAIL_TURN").equalsIgnoreCase("OFF")) {
			    	logger.info(" DOIN eMAIL FLAG OFF");
			    	msg.getToRecipients().add(get("DEFAULT_EMAIL"));
	    		}else {
			        if(to!=null){
					    for(int i=0;i<to.length;i++){
					    	msg.getToRecipients().add(to[i]);
					    }
				    }
				    
				    if(cc!=null){
					    for(int i=0;i<cc.length;i++){
					    	msg.getCcRecipients().add(cc[i]);
					    }
				    }
				    
				    if(bcc!=null){
					    for(int i=0;i<bcc.length;i++){
					    	msg.getBccRecipients().add(bcc[i]);
					    }
				    }
	    		}
			    
			    
			   
			    
			    msg.setSubject(subject);
			    //msg.setBody(MessageBody.getMessageBodyFromText(body));
			    
			    MessageBody b = MessageBody.getMessageBodyFromText(body);
			    b.setBodyType(BodyType.HTML);
			    msg.setBody(b);
			    
			    
			  
			   
			  
			    if(attachments !=null){
				    for(int i=0;i<attachments.length;i++){
				    	msg.getAttachments().addFileAttachment(attachments[i]);
				    }
			    }
			    
			    msg.sendAndSaveCopy();
			   	result = true;
			}
		}catch(Exception e){
			//e.printStackTrace();
			result = false;
			System.err.println(e.getMessage());
			logger.error(e.getMessage(),e);
		}
		return result;
	}
	
	
	
	
	/*public static void main(String[] args){
		try {
		microsoft.exchange.webservices.data.ExchangeService service = new microsoft.exchange.webservices.data.ExchangeService();
		microsoft.exchange.webservices.data.ExchangeCredentials credentials = new microsoft.exchange.webservices.data.WebCredentials("elms-workflow@eastvaleca.gov","e1M$w0rkf!0w");
	    service.setCredentials(credentials);    
	    service.setUrl(new URI("https://outlook.office365.com/ews/Exchange.asmx"));      


	    microsoft.exchange.webservices.data.EmailMessage msg;
		
			msg = new microsoft.exchange.webservices.data.EmailMessage(service);
		
	    msg.setSubject("Eastvale email Test!"); 
	    msg.setBody(microsoft.exchange.webservices.data.MessageBody.getMessageBodyFromText("Sent from application email. Testing eastvale email"));
	    msg.getToRecipients().add("sunvoyage@gmail.com");
	    msg.sendAndSaveCopy();

	   System.out.println("done");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
	

}
