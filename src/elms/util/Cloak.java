package elms.util;

import java.io.*;
import java.util.*;
import java.net.*;

/**
 * Cloak parses a <code>String</code> or a text file in a local or remote drive so that it may be
 * used as a template for a java application. It works by iterating through the specified <code>String</code>
 * or file, pausing when it encounters a known tag allowing the application developer to create code
 * in the place of the tag.<br><br>
 *
 * Known tags can be either a simple tag:<br><br>
 *
 * <pre>
 * &lt;APP [FIELD] = [VALUE] /&gt;
 * </pre>
 *
 * <p>or a complex tag:</p>
 *
 * <pre>
 * &lt;APP [FIELD] = [VALUE] &gt;
 *   Hello World
 * &lt;/APP [FIELD]&gt;
 * </pre>
 *
 * <hr>
 *
 * Below is a proper use example.<br><br>
 *
 * <pre>
 * Cloak CLOAK = new Cloak("http://www.url.com/template.html");
 * while (CLOAK.next()) {
 *     out.print(CLOAK.LINE);
 *     if (CLOAK.equals("FIELD")) {
 *         out.print("The value is " + CLOAK.VALUE);
 *     }
 * }
 * CLOAK.clear();
 * </pre>
 *
 * @author Alain Romero
 *
 */
public class Cloak {

    /**
     * Path to local files. - optional
     */
    private String PATH = "/home/ajpe/misc-apps/websubmit/templates/"; /*### INCLUDE TRAILING SLASH ###*/

    /**
     * Next line in the iterator.
     */
    private String NEXT = "";

    /**
     * Current tag field.
     */
    public String FIELD = "";

    /**
     * Current tag value.
     */
    public String VALUE = "";

    /**
     * Current tag frill. A frill is the enclosed text inside a complex tag.
     */
    public String FRILL = "";

    /**
     * Current line. A line is the read string.
     */
    public String LINE  = null;

    /**
     * BufferedReader containing the file stream.
     */
    private BufferedReader BUFFEREDREADER = null;

    /**
     * Position of field in a tagArray().
     */
    public static int _field  = 0;

    /**
     * Position of value in a tagArray().
     */
    public static int _value  = 1;

    /**
     * Position of before in a tagArray().
     */
    public static int _before = 2;

    /**
     * Position of after in a tagArray().
     */
    public static int _after  = 3;

    /**
     * Position of exists in a tagArray().
     */
    public static int _exists = 4;

    /**
     * Position of type in a tagArray().
     */
    public static int _type   = 5;



/* #################################################################################################
##  CONSTRUCTORS                                                                                  ##
################################################################################################# */

    /**
     * Constructs an instance of this class with default template.
     */
    public Cloak() { this.NEXT = Skivvy.html(); }

    /**
     * Constructs an instance of this class with specified URL.
     *
     * @param template The url of the remote file to use as template.
     */
    public Cloak(URL template) {
        retrieveURL(template.toString());
    }

    /**
     * Constructs an instance of this class with specified template.
     *
     * <p>A <code>null</code> or empty string input will return default template.
     *
     * @param template The url of the remote file, the path to a local file or a string to use as template.
     */
    public Cloak(String template) {

        if (template.equalsIgnoreCase("skivvy") || !Operator.hasValue(template)) { this.NEXT = Skivvy.html(); }
        else {
            boolean RETRIEVED = false;
            int type = Operator.addressType(template);
            int length = template.length();

            if (type > 2)  { RETRIEVED = retrieveURL(template); }
            else if (type == 2) {
                StringBuffer sb = new StringBuffer(length + 7);
                sb.append("http://").append(template);
                RETRIEVED = retrieveURL(sb.toString());
            }
            else {
                StringBuffer sb = new StringBuffer(length + 28);
                sb.append(PATH).append(template);
                File FILE = new File(sb.toString());
                RETRIEVED = retrieveFile(FILE);
            }
            if (!RETRIEVED) { this.NEXT = template; }
        }

    }


/* #################################################################################################
##  ITERATE                                                                                       ##
################################################################################################# */

    /**
     * Iterate through the template and process the next line.
     *
     * @return Return <code>false</code> at end of template.
     */
    public boolean next() {

        if (BUFFEREDREADER == null && !Operator.hasValue(NEXT)) { return false; }

        String TEMPLINE = "";

        if (Operator.hasValue(NEXT)) { TEMPLINE = NEXT; }
        else {
            try { TEMPLINE = BUFFEREDREADER.readLine(); }
            catch (IOException E) { return false; }
            if (TEMPLINE == null) { return false; }
            else { TEMPLINE = TEMPLINE.concat("\n"); }
        }

        containsTag(TEMPLINE);
        return true;

    }

    /**
     * Get the current line.
     *
     * @return Return current line.
     */
    public String getString() {
        if (Operator.hasValue(LINE)) { return LINE; }
        else { return ""; }
    }

    /**
     * Get the current field.
     *
     * @return Return current tag field.
     */
    public String getField()  {
        if (Operator.hasValue(FIELD)) { return FIELD; }
        else { return ""; }
    }

    /**
     * Get the current value.
     *
     * @return Return current tag value.
     */
    public String getValue()  {
        if (Operator.hasValue(VALUE)) { return VALUE; }
        else { return ""; }
    }

    /**
     * Check if current tag field is equal to specified string.
     *
     * @param field The <code>String</code> to compare to the current tag field.
     * @return Return <code>true</code> if current field is equal to specified string.
     */
    public boolean equals(String field) {
        return fieldEquals(field);
    }

    /**
     * Check if current tag field and tag value are equal to specified strings.
     *
     * @param field The string to compare to the current tag field.
     * @param value The string to compare to the current tag value.
     * @return Return <code>true</code> if current tag field and tag value are equal to specified strings.
     */
    public boolean equals(String field, String value) {
        if (fieldEquals(field) && valueEquals(value)) { return true; }
        else { return false; }
    }

    /**
     * Check if current tag field and tag value are equal to specified strings.
     *
     * @param field The string to compare to the current tag field.
     * @param value The string to compare to the current tag value.
     * @return Return <code>true</code> if current tag field and tag value are equal to specified strings.
     */
    public boolean tagEquals(String field, String value) {
        if (fieldEquals(field) && valueEquals(value)) { return true; }
        else { return false; }
    }

    /**
     * Check if current tag field is equal to specified string.
     *
     * @param field The string to compare to the current tag field.
     * @return Return <code>true</code> if current tag field is equal to specified string.
     */
    public boolean fieldEquals(String field) {
        if (FIELD.equalsIgnoreCase(field)) { return true; }
        else { return false; }
    }

    /**
     * Check if current tag value is equal to specified string.
     *
     * @param value The string to compare to the current tag value.
     * @return Return <code>true</code> if current tag value is equal to specified string.
     */
    public boolean valueEquals(String value) {
        if (VALUE.equalsIgnoreCase(value)) { return true; }
        else { return false; }
    }

    /**
     * Check if current tag field exists.
     *
     * @return Return <code>true</code> if current tag field exists.
     */
    public boolean hasField()  {
        if (Operator.hasValue(FIELD)) { return true; }
        else { return false; }
    }

    /**
     * Check if current tag value exists.
     *
     * @return Return <code>true</code> if current tag value exists.
     */
    public boolean hasValue()  {
        if (Operator.hasValue(VALUE)) { return true; }
        else { return false; }
    }

    /**
     * Check if current tag frill exists.
     *
     * @return Return <code>true</code> if current tag frill exists.
     */
    public boolean hasFrill()  {
        if (Operator.hasValue(FRILL)) { return true; }
        else { return false; }
    }


/* #################################################################################################
##  READ TEMPLATE                                                                                 ##
################################################################################################# */

    /**
     * Read file and place in <code>BufferedReader</code>.
     *
     * @param file Path to file.
     * @return Return <code>true</code> if file was read successfully.
     */
    private boolean retrieveFile(File file) {

        try {
            FileReader FILEREADER = new FileReader(file);
            this.BUFFEREDREADER = new BufferedReader(FILEREADER);
            return true;
        }
        catch(IOException E) { return false; }

    }

    /**
     * Read remote file in a url and place in <code>BufferedReader</code>.
     *
     * @param url URL address of file.
     * @return Return <code>true</code> if file was read successfully.
     */
    private boolean retrieveURL(String url) {

        try {
            URL URLADDRESS = new URL(url);
            URLConnection URLCONNECTION = URLADDRESS.openConnection();
            InputStream INPUTSTREAM = URLCONNECTION.getInputStream();
            BufferedInputStream BUFFEREDINPUTSTREAM = new BufferedInputStream(INPUTSTREAM);
            this.BUFFEREDREADER = new BufferedReader(new InputStreamReader(BUFFEREDINPUTSTREAM));

            return true;
        }
        catch(MalformedURLException E) { return false; }
        catch(IOException E) { return false; }

    }

/* #################################################################################################
##  MANAGEMENT                                                                                    ##
################################################################################################# */

    /**
     * Clear memory.
     */
    public void clear() {

        NEXT = "";
        FIELD = "";
        VALUE = "";
        FRILL = "";
        LINE = null;

        if (BUFFEREDREADER != null) {
            try { this.BUFFEREDREADER.close(); }
            catch (IOException E) { }
        }

    }

    /**
     * Clear current fields.
     */
    private void clearCurrent() {
        this.LINE = "";
        this.FIELD = "";
        this.VALUE = "";
        this.NEXT = "";
        this.FRILL = "";
    }

    /**
     * Create instance of Frill().
     *
     * @return a Frill() of current frill.
     */
    public Frill frill() { return new Frill(FRILL); }


/* #################################################################################################
##  TAG TRANSLATION                                                                               ##
################################################################################################# */

    /**
     * Read through next line and determine if it contains a tag.
     *
     * @param str next line.
     * @return Return <code>true</code> if line contains tag.
     */
    private boolean containsTag(String str) {

        clearCurrent();

        if (str == null || str.length() == 0) { return false; }

        String[] open_array = openTagArray(str);

        if (open_array[_exists].equals("true")) {

            this.LINE  = open_array[_before];
            this.FIELD = open_array[_field];
            this.VALUE = open_array[_value];

            if (open_array[_type].equals("multiline")) {

                String[] close_array = closeTagArray(open_array[_after], open_array[_field]);

                if (close_array[_exists].equals("true")) {
                    this.FRILL = close_array[_before];
                    this.NEXT  = close_array[_after];
                }
                else {

                    boolean frillCondition = true;
                    StringBuffer TEMPFRILL = new StringBuffer(10000);
                    TEMPFRILL.append(open_array[_after]);

                    while (frillCondition) {

                        try {

                            String line = BUFFEREDREADER.readLine();
                            if (line == null) { frillCondition = false; }
                            else {

                                close_array = closeTagArray(line, open_array[_field]);

                                if (close_array[_exists].equals("true")) {
                                    TEMPFRILL.append(close_array[_before]);
                                    this.NEXT = close_array[_after];
                                    frillCondition = false;
                                }
                                else { TEMPFRILL.append(line).append("\n"); }

                            }

                        }
                        catch (IOException E) { frillCondition = false; }
                    }

                    this.FRILL = TEMPFRILL.toString();

                }
            }
            else {
                this.NEXT = open_array[_after];
            }
            return true;
        }
        else {
            this.LINE = str;
            return false;
        }

    }

    /**
     * Parse tag and place parts in an array.
     *
     * @param str The tag to parse.
     * @return Return <code>String[]</code> containing tag parts.
     */
    public static String[] openTagArray(String str) {

        String array[] = {"","","","","false",""};

        int open_position  = str.indexOf("<APP ");
        int close_position = str.indexOf(">", open_position + 5);

        if (open_position > -1 && close_position > -1) {

            String tag = str.substring(open_position, close_position+1);
            String[] tagArray = tagArray(tag);

            array[_field]  = tagArray[_field];
            array[_value]  = tagArray[_value];
            array[_before] = str.substring(0, open_position);
            array[_after]  = str.substring(close_position+1, str.length());
            array[_exists] = "true";

            if (tag.endsWith("/>")) { array[_type] = "single"; }
            else { array[_type] = "multiline"; }

        }

        return array;

    }

    /**
     * Parse tag and place parts in an array.
     *
     * @param str The tag to parse.
     * @return Return <code>String[]</code> containing tag parts.
     */
    public static String[] closeTagArray(String str, String field) {

        String array[] = {"","","","","false",""};

        String tagtype = "</APP ".concat(field);

        int open_position  = str.indexOf(tagtype);
        int close_position = str.indexOf(">", open_position + tagtype.length());

        if (open_position > -1 && close_position > -1) {

            String[] tag = tagArray(str.substring(open_position, close_position+1));

            array[_field]  = tag[_field];
            array[_before] = str.substring(0, open_position);
            array[_after]  = str.substring(close_position+1, str.length());
            array[_exists] = "true";

        }

        return array;

    }

    /**
     * Parse tag and place parts in an array.
     *
     * @param str The tag to parse.
     * @return Return <code>String[]</code> containing tag parts.
     */
    public static String[] tagArray(String tag) {
        String[] array = {"",""};
        if (Operator.hasValue(tag)) {
            String temptag = Operator.replace(tag,"<APP ","");
            temptag = Operator.replace(temptag,"</APP ","");
            temptag = Operator.replace(temptag,"/>","");
            temptag = Operator.replace(temptag,">","");
            temptag = Operator.replace(temptag,"\"","");
            String[] fieldvalue = Operator.split(temptag,"=");
            if (fieldvalue.length > 0) { array[_field] = fieldvalue[0]; }
            if (fieldvalue.length > 1) { array[_value] = fieldvalue[1]; }
        }
        return array;
    }

}