package elms.util;

public class Email {

	protected String host;
	protected String username;
	protected String password;
	protected String fromAddress;
	protected String toAddress;
	protected String subject;
	protected String message;

	public Email() {
	}

	public Email(String subject, String fromAddress, String toAddress, String message) {
		super();
		this.subject = subject;
		this.fromAddress = fromAddress;
		this.toAddress = toAddress;
		this.message = message;
	}

	/**
	 * @return
	 */
	public String getFromAddress() {
		return fromAddress;
	}

	/**
	 * @return
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @return
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @return
	 */
	public String getToAddress() {
		return toAddress;
	}

	/**
	 * @param string
	 */
	public void setFromAddress(String string) {
		fromAddress = string;
	}

	/**
	 * @param string
	 */
	public void setMessage(String string) {
		message = string;
	}

	/**
	 * @param string
	 */
	public void setSubject(String string) {
		subject = string;
	}

	/**
	 * @param string
	 */
	public void setToAddress(String string) {
		toAddress = string;
	}

	/**
	 * @return
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @return
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param string
	 */
	public void setPassword(String string) {
		password = string;
	}

	/**
	 * @param string
	 */
	public void setUsername(String string) {
		username = string;
	}

	/**
	 * @return
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @param string
	 */
	public void setHost(String string) {
		host = string;
	}

}
