//Source file: C:\\Datafile\\OBC\\war\\src\\main\\classes\\obc\\app\\common\\Hold.java

package elms.util;


/**
 * @author Shekhar Jain
 */
public class DDList {
    public Integer value = new Integer(0);
    public String display = null;

    public DDList(Integer aValue, String aDisplay) {
        this.value = aValue;
        this.display = aDisplay;
    }

    /**
     * Access method for the holdId property.
     *
     * @return   the current value of the holdId property
     */
    public Integer getValue() {
        return value;
    }

    /**
     * Sets the value of the holdId property.
     *
     */
    public void setValue(Integer aValue) {
        value = aValue;
    }

    /**
     * Access method for the display property.
     *
     * @return   the current value of the display property
     */
    public String getDisplay() {
        return display;
    }

    /**
     * Sets the value of the display property.
     *
     * @param aDisplay the new value of the display property
     */
    public void setDisplay(String aDisplay) {
        display = aDisplay;
    }


    public String toString() {
        return (this.getDisplay());
    }
}
