package elms.util;

import java.io.*;
import java.util.*;
import java.net.*;

/**
 * Skivvy creates generic templates for use with Cloak and Frill.
 *
 * @author Alain Romero
 */
public class Skivvy {


    /**
     * Create an instance of Skivvy.
     */
    public Skivvy() {
    }

    /**
     * Create a generic html template.
     */
    public static String html() {

        StringBuffer SKIVVY = new StringBuffer(5000);

        SKIVVY.append("<html>\n");
        SKIVVY.append("    <head>\n");
        SKIVVY.append("        <title>APP</title>\n\n");

        SKIVVY.append("        <style type=\"text/css\">\n");
        SKIVVY.append("            BODY        { background-color: #FFFFFF; font-family: Arial, Helvetica; font-size: 12px; color: #000000 }\n");
        SKIVVY.append("            TD          { background-color: #FFFFFF; font-family: Arial, Helvetica; font-size: 12px; color: #000000 }\n");
        SKIVVY.append("            TD.standard { background-color: #FFFFFF; font-family: Arial, Helvetica; font-size: 12px; color: #000000 }\n");
        SKIVVY.append("            TD.header   { background-color: #FFFFFF; font-family: Arial, Helvetica; font-size: 16px; color: #000000; font-weight: bold }\n");
        SKIVVY.append("            TD.title    { background-color: #FFFFFF; font-family: Arial, Helvetica; font-size: 12px; color: #000000; font-weight: bold }\n");
        SKIVVY.append("            HR          { border: #BDE78C 1px dotted; height: 1px }\n");
        SKIVVY.append("            A:link      { color: #205720; text-decoration: none }\n");
        SKIVVY.append("            A:visited   { color: #205720; text-decoration: none }\n");
        SKIVVY.append("            A:active    { color: #205720; text-decoration: none }\n");
        SKIVVY.append("            A:hover     { color: #205720; text-decoration: none }\n");
        SKIVVY.append("        </style>\n\n");

        SKIVVY.append("    </head>\n\n");

        SKIVVY.append("<body>\n\n");

        SKIVVY.append("    <APP CONTENT/>\n");
        SKIVVY.append("    <APP SKIVVY/>\n");

        SKIVVY.append("</body>\n");
        SKIVVY.append("</html>\n");

        return SKIVVY.toString();

    }




}