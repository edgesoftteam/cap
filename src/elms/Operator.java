package elms;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.File;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URL;
import java.net.MalformedURLException;

public class Operator {  

    /**
     * Checks if specified <code>String</code> is a number.
     *
     * @param str The String to parse.
     * @return <code>true</code> if <code>String</code> is a number
     */
    public static boolean isNumber(String str) {
        try {
            @SuppressWarnings("unused")
			int num = Integer.parseInt(str);
            return true;
        }
        catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * <p>Checks if a String is any type of address.</p>
     *
     * <p>A <code>null</code> string input will return <code>false</code>.
     *
     * <pre>
     * Operator.isAddress("me@myurl.com") = true
     * Operator.isAddress("http://www.url.com") = true
     * Operator.isAddress("hello") = false
     * </pre>
     *
     * @param str  the String to parse, may be null
     * @return <code>true</code> if is an address, and is non-null
     * @since 1.0
     */
    public static boolean isAddress(String str) {
        if (str == null || str.length() == 0) { return false; }
        else if (addressType(str) > 0) { return true; }
        else { return false; }
    }

    /**
     * <p>Checks if a String is an email address.</p>
     *
     * <pre>
     * Operator.isEmail("me@myurl.com") = true
     * Operator.isEmail("hello") = false
     * </pre>
     *
     * @param str  the String to check, may be null
     * @return <code>true</code> if the String is an email address
     */
    public static boolean isEmail(String str) {

        if (str == null || str.length() == 0) { return false; }

        String invalidChars = "/:,;!| ";
        for (int i=0; i<invalidChars.length(); i++) {
            if (str.indexOf(invalidChars.charAt(i)) != -1) { return false; }
        }

        if (str.indexOf("@",1) == -1) { return false; }
        else { return true; }

    }

    /**
     * <p>Checks if a String is a url address.</p>
     *
     * <pre>
     * Operator.isURL("www.url.com") = true
     * Operator.isURL("http://www.url.com") = true
     * Operator.isURL("https://www.url.com") = true
     * Operator.isURL("ftp://www.url.com") = true
     * Operator.isURL("hello") = false
     * </pre>
     *
     * @param str  the String to check, may be null
     * @return <code>true</code> if the String is a url address
     */
    public static boolean isURL(String str) {
        if (str == null || str.length() == 0)  { return false; }
        else if (addressType(str) > 1) { return true; }
        else { return false; }
    }

    /**
     * <p>Checks if a String is a properly formatted url address.</p>
     *
     * <pre>
     * Operator.isProperURL("www.url.com") = false
     * Operator.isProperURL("http://www.url.com") = true
     * Operator.isProperURL("https://www.url.com") = true
     * Operator.isProperURL("ftp://www.url.com") = true
     * Operator.isProperURL("hello") = false
     * </pre>
     *
     * @param str  the String to check, may be null
     * @return <code>true</code> if the String is a url address
     */
    public static boolean isProperURL(String str) {
        if (str == null || str.length() == 0) { return false; }
        else if (addressType(str) > 2) { return true; }
        else { return false; }
    }

    /**
     * <p>Checks String and determine the type of address.</p>
     *
     * <pre>
     * Operator.addressType("hello") = 0
     * Operator.addressType("me@myurl.com") = 1
     * Operator.addressType("www.url.com") = 2
     * Operator.addressType("http://www.url.com") = 3
     * Operator.addressType("https://www.url.com") = 4
     * Operator.addressType("ftp://www.url.com") = 5
     * </pre>
     *
     * @param str  the String to check, may be null
     * @return value of address
     */
    public static int addressType(String str) {

        if (str == null || str.length() == 0) { return 0; }

        if (isEmail(str)) { return 1; }
        else if (str.indexOf("www.") == 0) { return 2; }
        else {
            try {
                URL URLADDRESS = new URL(str);
                String protocol = URLADDRESS.getProtocol();
                if (protocol.equalsIgnoreCase("http")) { return 3; }
                else if (protocol.equalsIgnoreCase("https")) { return 4; }
                else if (protocol.equalsIgnoreCase("ftp")) { return 5; }
            }
            catch(MalformedURLException E) { return 0; }
        }
        return 0;

    }

	/**
	 * Get the proper url address of a string
     * <pre>
     * properURL("www.url.com") returns "http://www.url.com"
     * </pre>
	 * @param str - a url address that may not be complete
	 * @return - proper address of specified string
	 */
	public static String properURL(String str) {
		int at = addressType(str);
		if (at == 2) { return "http://".concat(str); }
		else if (at > 2) { return str; }
		return "";
	}

	public static String getDomain(String url) {
		try {
		    URI uri = new URI(url);
		    String domain = uri.getHost();
		    return domain.startsWith("www.") ? domain.substring(4) : domain;
		}
		catch (Exception e) { return ""; }
	}


    // Check if empty
    //-----------------------------------------------------------------------
    /**
     * <p>Checks if a String contains value, and is not null.</p>
     *
     * <pre>
     * Operator.hasValue("hello world") = true
     * Operator.hasValue(null) = false
     * Operator.hasValue("") = false
     * </pre>
     *
     * @param str  the String to analyze, may be null
     * @return <code>true</code> if is string is not null and has value.
     */
    public static boolean hasValue(String str) {
        if (str == null || str.length() == 0) { return false; }
        else { return true; }
    }

    /**
     * <p>Checks if a StringBuffer contains value, and is not null.</p>
     *
     * @param sb  the StringBuffer to analyze, may be null
     * @return <code>true</code> if is string is not null and has value.
     */
	public static boolean hasValue(StringBuffer sb) {
		return hasValue(sb.toString());
	}

    /**
     * <p>Checks if a MapSet contains value, and is not null.</p>
     *
     * <pre>
     * Operator.hasValue(null) = false
     * Operator.hasValue(new MapSet()) = false
     * </pre>
     *
     * @param map  the MapSet to analyze, may be null
     * @return <code>true</code> if is string is not null and has value.
     */
/*    public static boolean hasValue(MapSet map) {
    	if (map == null) { return false; }
    	else if (map.size() < 1) { return false; }
        else { return true; }
    }*/

    /**
     * <p>Checks if a string array has value, and is not null.</p>
     *
     * <pre>
     * Operator.hasValue([1, 2, 3]) = true
     * Operator.hasValue(null) = false
     * Operator.hasValue("[]") = false
     * </pre>
     *
     * @param array  the object to analyze, may be null
     * @return <code>true</code> if the object is not null and has value.
     */
    public static boolean hasValue(int[] array) {
        if (array == null || array.length == 0) { return false; }
        else { return true; }
    }

	/**
	 * <p>Checks if an object contains value, and is not null.</p>
	 *
	 * <pre>
	 * Operator.hasValue(["a", "b", "c"]) = true
	 * Operator.hasValue(null) = false
	 * Operator.hasValue("[]") = false
	 * </pre>
	 *
	 * @param array  the object to analyze, may be null
	 * @return <code>true</code> if the object is not null and has value.
	 * @since 1.0
	 */
	public static boolean hasValue(Object[] array) {
		if (array == null || array.length == 0) { return false; }
		else { return true; }
	}

    /**
     * <p>Checks if an ArrayList contains value, and is not null.</p>
     *
     * <pre>
     * Operator.hasValue(["a", "b", "c"]) = true
     * Operator.hasValue(null) = false
     * Operator.hasValue("[]") = false
     * </pre>
     *
     * @param list  the object to analyze, may be null
     * @return <code>true</code> if the object is not null and has value.
     */
    public static boolean hasValue(ArrayList<String> list) {
        if (list == null || list.size() == 0) {
            return false;
        }
        else { return true; }
    }


    /**
     * Compares if two strings are identical. This method is not case sensitive and will not throw exception if any specified values are null
     * @param one
     * @param two
     * @return true if specified values are identical without regard for case
     */
    public static boolean equalsIgnoreCase(String one, String two) {
		if (one == null && two == null) {
			return true;
		}
		if (one == null) { return false; }
		if (two == null) { return false; }
		return one.equalsIgnoreCase(two);
	}

    // Convert String
    //-----------------------------------------------------------------------
    /**
     * <p>Convert a string to int.</p>
     *
     * <pre>
     * Operator.toInt("5") = 5
     * Operator.toInt(null) = 0
     * Operator.toInt("") = 0
     * Operator.toInt("Hello World") = 0
     * </pre>
     *
     * @param str  the String to convert, may be null
     * @return the int represented by the string, or 0 if conversion fails
     */
    public static int toInt(String str) {
        return toInt(str,0);
    }

    /**
     * <p>Convert a string to int.</p>
     *
     * <pre>
     * Operator.toInt("5",-1) = 5
     * Operator.toInt(null,-1) = -1
     * Operator.toInt("",-1) = -1
     * Operator.toInt("Hello World",-1) = -1
     * </pre>
     *
     * @param str  the String to convert, may be null
     * @param defaultValue  the default value
     * @return the int represented by the string, or the default if conversion fails
     */
    public static int toInt(String str, int defaultValue) {
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException nfe) {
        	double dv = defaultValue;
        	double d = toDouble(str, dv);
            return (int) Math.round(d);
        }
    }

    /**
     * <p>Convert a string to double.</p>
     *
     * <pre>
     * Operator.toInt("5") = 5
     * Operator.toInt(null) = 0
     * Operator.toInt("") = 0
     * Operator.toInt("Hello World") = 0
     * </pre>
     *
     * @param str  the String to convert, may be null
     * @return the double represented by the string, or 0 if conversion fails
     */
	public static double toDouble(String str) {
		return toDouble(str, 0);
	}

    /**
     * <p>Convert a string to double.</p>
     *
     * <pre>
     * Operator.toInt("5",-1) = 5
     * Operator.toInt(null,-1) = -1
     * Operator.toInt("",-1) = -1
     * Operator.toInt("Hello World",-1) = -1
     * </pre>
     *
     * @param str  the String to convert, may be null
     * @param defaultValue  the default value
     * @return the double represented by the string, or the default if conversion fails
     */
	public static double toDouble(String str, double defaultValue) {
		if (!Operator.hasValue(str)) { return defaultValue; }
		try {
			return Double.valueOf(str.trim()).doubleValue();
		}
		catch (NumberFormatException nfe) {
			return defaultValue;
		}
	}

    /**
     * <p>Convert a double to String.</p>
     *
     * <pre>
     * Operator.toString(5.0) = "5.0"
     * </pre>
     *
     * @param num  the double to convert.
     * @return the String represented by the double.
     */
	public static String toString(double num) {
		return Double.toString(num);
	}


    /**
     * <p>Convert a int to String.</p>
     *
     * <pre>
     * Operator.toString(5) = "5"
     * </pre>
     *
     * @param num  the int to convert.
     * @return the String represented by the int.
     */
    public static String toString(int num) {
        return Integer.toString(num);
    }

    /**
     * <p>Convert a long to String.</p>
     *
     * <pre>
     * Operator.toString(5555555555555L) = "5555555555555"
     * </pre>
     *
     * @param num  the long to convert.
     * @return the String represented by the long.
     * @since 1.0
     */
    public static String toString(long num) {
        return Long.toString(num);
    }

    public static String toString(Object o) {
    	return toString(o, "");
    }

    public static String toString(Object o, String def) {
		try {
			if (o == null) { return def; }
			String type = o.getClass().getSimpleName();
			if (Operator.hasValue(type)) {
				if (type.equalsIgnoreCase("String")) { return (String) o; }
				else if (type.equalsIgnoreCase("StringBuffer")) {
					StringBuffer sb = (StringBuffer) o;
					return sb.toString();
				}
				else if (type.equalsIgnoreCase("Integer")) {
					Integer i = (Integer) o;
					return i.toString();
				}
				else if (type.equalsIgnoreCase("Double")) {
					Double d = (Double) o;
					return d.toString();
				}
				else if (type.equalsIgnoreCase("BigDecimal")) {
					BigDecimal b = (BigDecimal) o;
					return b.toString();
				}
				else if (type.equalsIgnoreCase("Boolean")) {
					Boolean b = (Boolean) o;
					return b.toString();
				}
				else if (type.equalsIgnoreCase("Date")) {
					Date d = (Date) o;
					return d.toString();
				}
				/*else if (type.equalsIgnoreCase("OBCTimekeeper")) {
					OBCTimekeeper d = (OBCTimekeeper) o;
					return d.getString("FULLDATETIME");
				}*/
			}
		}
		catch (Exception e) { }
		return def;
    }

    /**
     * <p>Convert an ArrayList to String[].</p>
     *
     * @param list  the ArrayList to convert.
     * @return the converted String[] array.
     */
    public static String[] toArray(ArrayList<String> list) {
    	try {
            list.trimToSize();
            return (String[]) list.toArray(new String[list.size()]);
    	}
    	catch (Exception e) { return new String[0]; }
    }

    /**
     * <p>Convert an ArrayList to Object[].</p>
     *
     * @param list  the ArrayList to convert.
     * @return the converted Object[] array.
     */
    public static Object[] toObjectArray(ArrayList<?> list) {
    	Object o[] = list.toArray();
    	return o;
    }

    public static String[] toStringArray(ArrayList<?> al) {
    	if (al == null) { return new String[0]; }
    	if (al.size() < 1) { return new String[0]; }
    	ArrayList<String> l = new ArrayList<String>();
    	for (int i=0; i < al.size(); i++) {
			String v = toString(al.get(i));
			l.add(v);
		}
		return toArray(l);
    }

    public static String[] toStringArray(Object[] o) {
    	if (o == null) { return new String[0]; }
    	if (o.length < 1) { return new String[0]; }
    	ArrayList<String> a = new ArrayList<String>();
		for (int i=0; i < o.length;i++) {
			String v = toString(o[i]);
			a.add(v);
		}
		return toArray(a);
    }

    /**
     * <p>Convert a String[] to ArrayList.</p>
     *
     * @param str  the String[] to convert.
     * @return the converted ArrayList.
     */
    public static ArrayList<String> toArrayList(String[] str) {
    	if (!hasValue(str)) {
    		return new ArrayList<String>();
    	}
        ArrayList<String> al = new ArrayList<String>();
		for (int i = 0; i < str.length; i++) {
			al.add(str[i]);
		}
        return al;
    }

	/**
	 * Sort a string array
	 * @param arr - the string array to perform a sort on
	 * @return a new string array containing sorted strings from a specified string array
	 */
	public static String[] sort(String[] arr) {
		if (!hasValue(arr)) { return new String[0]; }
		Arrays.sort(arr, String.CASE_INSENSITIVE_ORDER);
		return arr;
	}


	public static String[] reverse(String[] arr) {
		if (!Operator.hasValue(arr)) { return new String[0]; }
		ArrayList<String> a = new ArrayList<String>();
		for(int i = arr.length; i < 0; i--){
			a.add(arr[i]);
		}
		return toArray(a);
	}

	/**
	 * Remove an item from a string array
	 * @param array - the string array to perform a remove action
	 * @param item - the item to remove from the specified string array
	 * @return the original string array without the specified item
	 */
	public static String[] remove(String[] array, String item)
	{
		ArrayList<String> a = new ArrayList<String>();
		int arraySize = array.length;
		for (int i = 0; i < arraySize; i++) {
			if (!Operator.equalsIgnoreCase(array[i], item)) {
				a.add(array[i]);
			}
		}
		return toArray(a);
	}
 
	/**
	 * Remove an item from a string array
	 * @param array - the string array to perform a remove action
	 * @param index - the index to the item which will be removed
	 * @return the original string array without the specified item
	 */
	public static String[] remove(String[] array, int index)
	{
		ArrayList<String> a = new ArrayList<String>();
		int arraySize = array.length;
		for (int i = 0; i < arraySize; i++) {
			if (i != index) {
				a.add(array[i]);
			}
		}
		return toArray(a);
	}

    // Splitting
    //-----------------------------------------------------------------------
    /**
     * <p>Splits the provided text into an array, using whitespace as the
     * separator.
     * Whitespace is defined by {@link Character#isWhitespace(char)}.</p>
     *
     * <p>The separator is not included in the returned String array.
     * Adjacent separators are treated as one separator.</p>
     *
     * <p>A <code>null</code> input String returns <code>null</code>.</p>
     *
     * <pre>
     * Operator.split(null)       = null
     * Operator.split("")         = []
     * Operator.split("abc def")  = ["abc", "def"]
     * Operator.split("abc  def") = ["abc", "def"]
     * Operator.split(" abc ")    = ["abc"]
     * </pre>
     *
     * @param str  the String to parse, may be null
     * @return an array of parsed Strings, <code>null</code> if null String input
     */
    public static String[] split(String str) {
        return split(str, null, -1);
    }

    /**
     * <p>Splits the provided text into an array, separator specified.
     * This is an alternative to using StringTokenizer.</p>
     *
     * <p>The separator is not included in the returned String array.
     * Adjacent separators are treated as one separator.</p>
     *
     * <p>A <code>null</code> input String returns <code>null</code>.</p>
     *
     * <pre>
     * Operator.split(null, *)         = null
     * Operator.split("", *)           = []
     * Operator.split("a.b.c", '.')    = ["a", "b", "c"]
     * Operator.split("a..b.c", '.')   = ["a", "b", "c"]
     * Operator.split("a:b:c", '.')    = ["a:b:c"]
     * Operator.split("a\tb\nc", null) = ["a", "b", "c"]
     * Operator.split("a b c", ' ')    = ["a", "b", "c"]
     * </pre>
     *
     * @param str  the String to parse, may be null
     * @param separatorChar  the character used as the delimiter,
     *  <code>null</code> splits on whitespace
     * @return an array of parsed Strings, <code>null</code> if null String input
     */
    public static String[] split(String str, char separatorChar) {
        // Performance tuned for 2.0 (JDK1.4)

        if (str == null) {
            return null;
        }
        int len = str.length();
        if (len == 0) {
            return new String[0];
        }
        List<String> list = new ArrayList<String>();
        int i =0, start = 0;
        boolean match = false;
        while (i < len) {
            if (str.charAt(i) == separatorChar) {
                if (match) {
                    list.add(str.substring(start, i).trim());
                    match = false;
                }
                start = ++i;
                continue;
            }
            match = true;
            i++;
        }
        if (match) {
            list.add(str.substring(start, i).trim());
        }
        return (String[]) list.toArray(new String[list.size()]);
    }

    /**
     * <p>Splits the provided text into an array, separators specified.
     * This is an alternative to using StringTokenizer.</p>
     *
     * <p>The separator is not included in the returned String array.
     * Adjacent separators are treated as one separator.</p>
     *
     * <p>A <code>null</code> input String returns <code>null</code>.
     * A <code>null</code> separatorChars splits on whitespace.</p>
     *
     * <pre>
     * Operator.split(null, *)         = null
     * Operator.split("", *)           = []
     * Operator.split("abc def", null) = ["abc", "def"]
     * Operator.split("abc def", " ")  = ["abc", "def"]
     * Operator.split("abc  def", " ") = ["abc", "def"]
     * Operator.split("ab:cd:ef", ":") = ["ab", "cd", "ef"]
     * </pre>
     *
     * @param str  the String to parse, may be null
     * @param separatorChars  the characters used as the delimiters,
     *  <code>null</code> splits on whitespace
     * @return an array of parsed Strings, <code>null</code> if null String input
     */
    public static String[] split(String str, String separatorChars) {
        return split(str, separatorChars, -1);
    }

    /**
     * <p>Splits the provided text into an array, separators specified.
     * This is an alternative to using StringTokenizer.</p>
     *
     * <p>The separator is not included in the returned String array.
     * Adjacent separators are treated as one separator.</p>
     *
     * <p>A <code>null</code> input String returns <code>null</code>.
     * A <code>null</code> separatorChars splits on whitespace.</p>
     *
     * <pre>
     * Operator.split(null, *, *)            = null
     * Operator.split("", *, *)              = []
     * Operator.split("ab de fg", null, 0)   = ["ab", "cd", "ef"]
     * Operator.split("ab   de fg", null, 0) = ["ab", "cd", "ef"]
     * Operator.split("ab:cd:ef", ":", 0)    = ["ab", "cd", "ef"]
     * Operator.split("ab:cd:ef", ":", 2)    = ["ab", "cdef"]
     * </pre>
     *
     * @param str  the String to parse, may be null
     * @param separatorChars  the characters used as the delimiters,
     *  <code>null</code> splits on whitespace
     * @param max  the maximum number of elements to include in the
     *  array. A zero or negative value implies no limit
     * @return an array of parsed Strings, <code>null</code> if null String input
     */
    public static String[] split(String str, String separatorChars, int max) {
        // Performance tuned for 2.0 (JDK1.4)
        // Direct code is quicker than StringTokenizer.
        // Also, StringTokenizer uses isSpace() not isWhitespace()

        if (str == null) {
            return null;
        }
        int len = str.length();
        if (len == 0) {
            return new String[0];
        }
        List<String> list = new ArrayList<String>();
        int sizePlus1 = 1;
        int i =0, start = 0;
        boolean match = false;
        if (separatorChars == null) {
            // Null separator means use whitespace
            while (i < len) {
                if (Character.isWhitespace(str.charAt(i))) {
                    if (match) {
                        if (sizePlus1++ == max) {
                            i = len;
                        }
                        list.add(str.substring(start, i).trim());
                        match = false;
                    }
                    start = ++i;
                    continue;
                }
                match = true;
                i++;
            }
        } else if (separatorChars.length() == 1) {
            // Optimise 1 character case
            char sep = separatorChars.charAt(0);
            while (i < len) {
                if (str.charAt(i) == sep) {
                    if (match) {
                        if (sizePlus1++ == max) {
                            i = len;
                        }
                        list.add(str.substring(start, i).trim());
                        match = false;
                    }
                    start = ++i;
                    continue;
                }
                match = true;
                i++;
            }
        } else {
            // standard case
            while (i < len) {
                if (separatorChars.indexOf(str.charAt(i)) >= 0) {
                    if (match) {
                        if (sizePlus1++ == max) {
                            i = len;
                        }
                        list.add(str.substring(start, i).trim());
                        match = false;
                    }
                    start = ++i;
                    continue;
                }
                match = true;
                i++;
            }
        }
        if (match) {
            list.add(str.substring(start, i).trim());
        }
        return (String[]) list.toArray(new String[list.size()]);
    }


    /**
     * Checks  for an occurence of specified value in an ArrayList
     * @param array - the ArrayList to check
     * @param value - the value to find
     * @return true if specified ArrayList contains the specified value
     */
    public static boolean contains(ArrayList<String> array, String value) {
		String[] a = toArray(array);
		return contains(a, value);
	}

    /**
     * Checks  for an occurence of specified value in an ArrayList
     * @param array - the ArrayList to check
     * @param value - the value to find
     * @return true if specified ArrayList contains the specified value
     */
    public static boolean containsIgnoreCase(ArrayList<String> array, String value) {
		String[] a = toArray(array);
		return containsIgnoreCase(a, value);
	}

    /**
     * Checks  for an occurence of specified value in a string array
     * @param array - the string array to check
     * @param value - the value to find
     * @return true if specified string array contains the specified value
     */
	public static boolean contains(String[] array, String value) {
		int arraySize = array.length;
		for (int i = 0; i < arraySize; i++) {
			if (array[i].equals(value)) { return true; }
		}
		return false;
	}

	public static boolean contains(int[] array, int value) {
		int arraySize = array.length;
		for (int i = 0; i < arraySize; i++) {
			if (array[i] == value) { return true; }
		}
		return false;
	}

    /**
     * Checks  for an occurence of specified value in a string array. This method ignores case.
     * @param array - the string array to check
     * @param value - the value to find
     * @return true if specified string array contains the specified value
     */
	public static boolean containsIgnoreCase(String[] array, String value) {
		int arraySize = array.length;
		for (int i = 0; i < arraySize; i++) {
			if (array[i].equalsIgnoreCase(value)) { return true; }
		}
		return false;
	}

    // Joining
    //-----------------------------------------------------------------------
    /**
     * <p>Joins the elements of the provided array into a single String
     * containing the provided list of elements.</p>
     *
     * <p>No separator is added to the joined String.
     * Null objects or empty strings within the array are represented by
     * empty strings.</p>
     *
     * <pre>
     * Operator.join(null)            = null
     * Operator.join([])              = ""
     * Operator.join([null])          = ""
     * Operator.join(["a", "b", "c"]) = "abc"
     * Operator.join([null, "", "a"]) = "a"
     * </pre>
     *
     * @param array  the array of values to join together, may be null
     * @return the joined String, <code>null</code> if null array input
     * @since 2.0
     */
    public static String join(Object[] array) {
        return join(array, null);
    }

    /**
     * <p>Joins the elements of the provided array into a single String
     * containing the provided list of elements.</p>
     *
     * <p>No delimiter is added before or after the list.
     * Null objects or empty strings within the array are represented by
     * empty strings.</p>
     *
     * <pre>
     * Operator.join(null, *)               = null
     * Operator.join([], *)                 = ""
     * Operator.join([null], *)             = ""
     * Operator.join(["a", "b", "c"], ';')  = "a;b;c"
     * Operator.join(["a", "b", "c"], null) = "abc"
     * Operator.join([null, "", "a"], ';')  = ";;a"
     * </pre>
     *
     * @param array  the array of values to join together, may be null
     * @param separator  the separator character to use
     * @return the joined String, <code>null</code> if null array input
     * @since 2.0
     */
    public static String join(Object[] array, char separator) {
        if (array == null) {
            return null;
        }
        int arraySize = array.length;
        int bufSize = (arraySize == 0 ? 0 : ((array[0] == null ? 16 : array[0].toString().length()) + 1) * arraySize);
        StringBuffer buf = new StringBuffer(bufSize);

        for (int i = 0; i < arraySize; i++) {
            if (i > 0) {
                buf.append(separator);
            }
            if (array[i] != null) {
                buf.append(array[i]);
            }
        }
        return buf.toString();
    }

	/**
	 * <p>Joins the elements of the provided array into a single String
	 * containing the provided list of elements.</p>
	 *
	 * <p>No delimiter is added before or after the list.
	 * Null objects or empty strings within the array are represented by
	 * empty strings.</p>
	 *
	 * <pre>
	 * Operator.join(null, *)               = null
	 * Operator.join([], *)                 = ""
	 * Operator.join([null], *)             = ""
	 * Operator.join(["a", "b", "c"], ';')  = "a;b;c"
	 * Operator.join(["a", "b", "c"], null) = "abc"
	 * Operator.join([null, "", "a"], ';')  = ";;a"
	 * </pre>
	 *
	 * @param array  the array of values to join together, may be null
	 * @param separator  the separator character to use
	 * @return the joined String, <code>null</code> if null array input
	 * @since 2.0
	 */
	public static String join(int[] array, String separator) {
		if (array == null) {
			return null;
		}
		int arraySize = array.length;
		StringBuffer buf = new StringBuffer();

		for (int i = 0; i < arraySize; i++) {
			if (i > 0) {
				buf.append(separator);
			}
			buf.append(array[i]);
		}
		return buf.toString();
	}

	/**
	 * <p>Joins the elements of the provided array into a single String
	 * containing the provided list of elements.</p>
	 *
	 * <p>No delimiter is added before or after the list.
	 * A <code>null</code> separator is the same as an empty String ("").
	 * Null objects or empty strings within the array are represented by
	 * empty strings.</p>
	 *
	 * <pre>
	 * Operator.join(null, *)                = null
	 * Operator.join([], *)                  = ""
	 * Operator.join([null], *)              = ""
	 * Operator.join(["a", "b", "c"], "--")  = "a--b--c"
	 * Operator.join(["a", "b", "c"], null)  = "abc"
	 * Operator.join(["a", "b", "c"], "")    = "abc"
	 * Operator.join([null, "", "a"], ',')   = ",,a"
	 * </pre>
	 *
	 * @param array  the array of values to join together, may be null
	 * @param separator  the separator character to use, null treated as ""
	 * @return the joined String, <code>null</code> if null array input
	 */
	public static String join(Object[] array, String separator) {
		if (array == null) {
			return null;
		}
		if (separator == null) {
			separator = "";
		}
		int arraySize = array.length;

		// ArraySize ==  0: Len = 0
		// ArraySize > 0:   Len = NofStrings *(len(firstString) + len(separator))
		//           (Assuming that all Strings are roughly equally long)
		int bufSize
			= ((arraySize == 0) ? 0
				: arraySize * ((array[0] == null ? 16 : array[0].toString().length())
					+ ((separator != null) ? separator.length(): 0)));

		StringBuffer buf = new StringBuffer(bufSize);

		for (int i = 0; i < arraySize; i++) {
			if ((separator != null) && (i > 0)) {
				buf.append(separator);
			}
			if (array[i] != null) {
				buf.append(array[i]);
			}
		}
		return buf.toString();
	}

	/**
	 * <p>Joins the elements of the provided array into a single String
	 * containing the provided list of elements.</p>
	 *
	 * <p>No delimiter is added before or after the list.
	 * A <code>null</code> separator is the same as an empty String ("").
	 * Null objects or empty strings within the array are represented by
	 * empty strings.</p>
	 *
	 * <pre>
	 * Operator.join(null, *)                = null
	 * Operator.join([], *)                  = ""
	 * Operator.join([null], *)              = ""
	 * Operator.join(["a", "b", "c"], "--")  = "a--b--c"
	 * Operator.join(["a", "b", "c"], null)  = "abc"
	 * Operator.join(["a", "b", "c"], "")    = "abc"
	 * Operator.join([null, "", "a"], ',')   = ",,a"
	 * </pre>
	 *
	 * @param array  the array of values to join together, may be null
	 * @param separator  the separator character to use, null treated as ""
	 * @return the joined String, <code>null</code> if null array input
	 */
	public static String joinSafe(String[] array, String separator) {
		if (array == null) {
			return null;
		}
		if (separator == null) {
			separator = "";
		}
		int arraySize = array.length;

		// ArraySize ==  0: Len = 0
		// ArraySize > 0:   Len = NofStrings *(len(firstString) + len(separator))
		//           (Assuming that all Strings are roughly equally long)
		int bufSize
			= ((arraySize == 0) ? 0
				: arraySize * ((array[0] == null ? 16 : array[0].toString().length())
					+ ((separator != null) ? separator.length(): 0)));

		StringBuffer buf = new StringBuffer(bufSize);

		for (int i = 0; i < arraySize; i++) {
			if ((separator != null) && (i > 0)) {
				buf.append(separator);
			}
			if (array[i] != null) {
				buf.append(safe(array[i]));
			}
		}
		return buf.toString();
	}

	public static String join(ArrayList<String> array, String delimiter) {
		boolean empty = true;
		StringBuilder sb = new StringBuilder();
		for (String s : array) {
			if (hasValue(s)) {
				if (!empty) {
					sb.append(delimiter);
				}
				sb.append(s);
				empty = false;
			}
		}
		return sb.toString();
	}

	// Replacing
    //-----------------------------------------------------------------------
    /**
     * <p>Replaces a String with another String inside a larger String, once.</p>
     *
     * <p>A <code>null</code> reference passed to this method is a no-op.</p>
     *
     * <pre>
     * Operator.replaceOnce(null, *, *)        = null
     * Operator.replaceOnce("", *, *)          = ""
     * Operator.replaceOnce("aba", null, null) = "aba"
     * Operator.replaceOnce("aba", null, null) = "aba"
     * Operator.replaceOnce("aba", "a", null)  = "aba"
     * Operator.replaceOnce("aba", "a", "")    = "aba"
     * Operator.replaceOnce("aba", "a", "z")   = "zba"
     * </pre>
     *
     * @see #replace(String text, String repl, String with, int max)
     * @param text  text to search and replace in, may be null
     * @param repl  the String to search for, may be null
     * @param with  the String to replace with, may be null
     * @return the text with any replacements processed,
     *  <code>null</code> if null String input
     */
    public static String replaceOnce(String text, String repl, String with) {
        return replace(text, repl, with, 1);
    }

    /**
     * <p>Replaces all occurences of a String within another String.</p>
     *
     * <p>A <code>null</code> reference passed to this method is a no-op.</p>
     *
     * <pre>
     * Operator.replace(null, *, *)        = null
     * Operator.replace("", *, *)          = ""
     * Operator.replace("aba", null, null) = "aba"
     * Operator.replace("aba", null, null) = "aba"
     * Operator.replace("aba", "a", null)  = "aba"
     * Operator.replace("aba", "a", "")    = "aba"
     * Operator.replace("aba", "a", "z")   = "zbz"
     * </pre>
     *
     * @see #replace(String text, String repl, String with, int max)
     * @param text  text to search and replace in, may be null
     * @param repl  the String to search for, may be null
     * @param with  the String to replace with, may be null
     * @return the text with any replacements processed,
     *  <code>null</code> if null String input
     */
    public static String replace(String text, String repl, String with) {
        return replace(text, repl, with, -1);
    }

    /**
     * <p>Replaces a String with another String inside a larger String,
     * for the first <code>max</code> values of the search String.</p>
     *
     * <p>A <code>null</code> reference passed to this method is a no-op.</p>
     *
     * <pre>
     * Operator.replace(null, *, *, *)         = null
     * Operator.replace("", *, *, *)           = ""
     * Operator.replace("abaa", null, null, 1) = "abaa"
     * Operator.replace("abaa", null, null, 1) = "abaa"
     * Operator.replace("abaa", "a", null, 1)  = "abaa"
     * Operator.replace("abaa", "a", "", 1)    = "abaa"
     * Operator.replace("abaa", "a", "z", 0)   = "abaa"
     * Operator.replace("abaa", "a", "z", 1)   = "zbaa"
     * Operator.replace("abaa", "a", "z", 2)   = "zbza"
     * Operator.replace("abaa", "a", "z", -1)  = "zbzz"
     * </pre>
     *
     * @param text  text to search and replace in, may be null
     * @param repl  the String to search for, may be null
     * @param with  the String to replace with, may be null
     * @param max  maximum number of values to replace, or <code>-1</code> if no maximum
     * @return the text with any replacements processed,
     *  <code>null</code> if null String input
     */
    public static String replace(String text, String repl, String with, int max) {
        if (text == null || repl == null || with == null || repl.length() == 0 || max == 0) {
            return text;
        }

        StringBuffer buf = new StringBuffer(text.length());
        int start = 0, end = 0;
        while ((end = text.indexOf(repl, start)) != -1) {
            buf.append(text.substring(start, end)).append(with);
            start = end + repl.length();

            if (--max == 0) {
                break;
            }
        }
        buf.append(text.substring(start));
        return buf.toString();
    }

    /**
     * <p>Replaces a char with a String inside a larger String.</p>
     *
     * <p>A <code>null</code> reference passed to this method is a no-op.</p>
     *
     * <pre>
     * Operator.replace(null, *, *, *)         = null
     * Operator.replace("", *, *, *)           = ""
     * Operator.replace("abaa", null, null) = "abaa"
     * Operator.replace("abaa", null, null) = "abaa"
     * Operator.replace("abaa", 'a', null)  = "abaa"
     * Operator.replace("abaa", 'a', "z")  = "zbzz"
     * </pre>
     *
     * @param text  text to search and replace in, may be null
     * @param repl  the char to search for, may be null
     * @param with  the String to replace with, may be null
     * @return the text with any replacements processed,
     *  <code>null</code> if null String input
     */
    public static String replace(String text, char repl, String with) {
        String r = new Character(repl).toString();
        return replace(text,r,with);
    }

    /**
     * <p>Replaces a String with a char inside a larger String.</p>
     *
     * <p>A <code>null</code> reference passed to this method is a no-op.</p>
     *
     * <pre>
     * Operator.replace(null, *, *, *)         = null
     * Operator.replace("", *, *, *)           = ""
     * Operator.replace("abaa", null, null) = "abaa"
     * Operator.replace("abaa", null, null) = "abaa"
     * Operator.replace("abaa", "a", null)  = "abaa"
     * Operator.replace("abaa", "a", '')    = "abaa"
     * Operator.replace("abaa", "a", 'z')  = "zbzz"
     * </pre>
     *
     * @param text  text to search and replace in, may be null
     * @param repl  the char to search for, may be null
     * @param with  the char to replace with, may be null
     * @return the text with any replacements processed,
     *  <code>null</code> if null String input
     */
    public static String replace(String text, String repl, char with) {
        String w = new Character(with).toString();
        return replace(text,repl,w);
    }

    /**
     * <p>Replaces a char with a char inside a larger String.</p>
     *
     * <p>A <code>null</code> reference passed to this method is a no-op.</p>
     *
     * <pre>
     * Operator.replace(null, *, *, *)         = null
     * Operator.replace("", *, *, *)           = ""
     * Operator.replace("abaa", null, null) = "abaa"
     * Operator.replace("abaa", null, null) = "abaa"
     * Operator.replace("abaa", 'a', null)  = "abaa"
     * Operator.replace("abaa", 'a', '')    = "abaa"
     * Operator.replace("abaa", 'a', '')   = "abaa"
     * Operator.replace("abaa", 'a', 'z')   = "zbaa"
     * Operator.replace("abaa", 'a', 'z')   = "zbza"
     * Operator.replace("abaa", 'a', 'z')  = "zbzz"
     * </pre>
     *
     * @param text  text to search and replace in, may be null
     * @param repl  the String to search for, may be null
     * @param with  the char to replace with, may be null
     * @return the text with any replacements processed,
     *  <code>null</code> if null String input
     */
    public static String replace(String text, char repl, char with) {
        return text.replace(repl,with);
    }

    /**
     * <p>Replaces all occurences of a String within another String, ignoring case.</p>
     *
     * <p>A <code>null</code> reference passed to this method is a no-op.</p>
     *
     * <pre>
     * Operator.replace(null, *, *)        = null
     * Operator.replace("", *, *)          = ""
     * Operator.replace("aba", null, null) = "aba"
     * Operator.replace("aba", null, null) = "aba"
     * Operator.replace("aba", "a", null)  = "aba"
     * Operator.replace("aba", "a", "")    = "aba"
     * Operator.replace("aba", "a", "z")   = "zbz"
     * </pre>
     *
     * @see #replace(String text, String repl, String with, int max)
     * @param str  text to search and replace in, may be null
     * @param repl  the String to search for, may be null
     * @param with  the String to replace with, may be null
     * @return the text with any replacements processed,
     *  <code>null</code> if null String input
     */
	public static String replaceIgnoreCase(String str, String repl, String with) {
		if (!hasValue(str)) { return ""; }
		if (!hasValue(repl))   { return str; }
		int s = -1;
		StringBuffer sb = new StringBuffer(); 
		String in = str.toLowerCase();
		if (in.indexOf(repl) == -1) {
			return str;
		}
		while (in.indexOf(repl) > -1) {
			sb = new StringBuffer();
			in = str.toLowerCase();
			s = in.indexOf(repl);
			if (s > 0) {
				sb.append(str.substring(0, s));
			}
			sb.append(with);
			if (str.length() > s+repl.length()) {
				sb.append(str.substring(s+repl.length()));
			}
			str = sb.toString();
			in = str.toLowerCase();
		}
		return sb.toString();
	}

    /**
     * <p>Replaces all occurences of smart quotes with regular quotes.</p>
     * @param text  text to search and replace in, may be null
     * @return the text with any replacements processed
     */
    public static String replaceSmartQuotes(String text) {

        String TEXT = "";

        //smart quotes
        TEXT = replace(text, (char)0x93, (char)0x22);
        text = replace(TEXT, (char)0x94, (char)0x22);
        TEXT = replace(text, (char)0x201C, (char)0x22);
        text = replace(TEXT, (char)0x201D, (char)0x22);

        //smart single quotes
        TEXT = replace(text, (char)0x91, (char)0x27);
        text = replace(TEXT, (char)0x92, (char)0x27);
        TEXT = replace(text, (char)0x2018, (char)0x27);
        text = replace(TEXT, (char)0x2019, (char)0x27);

        return text;

    }

    /**
     * <p>Replaces all occurences of smart characters with regular characters.</p>
     * @param text  text to search and replace in, may be null
     * @return the text with any replacements processed
     */
    public static String replaceSmartChars(String text) {

        String TEXT = replaceSmartQuotes(text);

        //em/en dash
        text = replace(TEXT, (char)0x96, (char)0x2D);
        TEXT = replace(text, (char)0x97, (char)0x2D);
        text = replace(TEXT, (char)0x2013, (char)0x2D);
        TEXT = replace(text, (char)0x2014, (char)0x2D);

        //elipse
        text = replace(TEXT, (char)0x85, "...");

        return text;

    }

    /**
     * <p>Capitalizes all the whitespace separated words in a String.
     * All letters are changed, so the resulting string will be fully changed.</p>
     *
     * <p>Whitespace is defined by {@link Character#isWhitespace(char)}.
     * A <code>null</code> input String returns <code>null</code>.
     * Capitalization uses the unicode title case, normally equivalent to
     * upper case.</p>
     *
     * @param str  the String to capitalize, may be null
     * @return capitalized String, <code>null</code> if null String input
     */
    public static String toTitleCase(String str) {
        int strLen;
        if (str == null || (strLen = str.length()) == 0) {
            return "";
        }
        StringBuffer buffer = new StringBuffer(strLen);
        boolean whitespace = true;
        for (int i = 0; i < strLen; i++) {
            char ch = str.charAt(i);
            if (Character.isWhitespace(ch)) {
                buffer.append(ch);
                whitespace = true;
            } else if (whitespace) {
                buffer.append(Character.toTitleCase(ch));
                whitespace = false;
            } else {
                buffer.append(Character.toLowerCase(ch));
            }
        }
        return buffer.toString();
    }

    // Special Characters
    //-----------------------------------------------------------------------
    /**
     * <p>Replaces occurences of special characters within a String with
     * proper HTML code.</p>
     *
     * <p>A <code>null</code> reference passed to this method returns "".</p>
     *
     * <pre>
     * Operator.toHTML(null)    = ""
     * Operator.toHTML("")      = ""
     * Operator.toHTML("10%") = "10&#37;"
     * </pre>
     *
     * @param str  string to search and replace in, may be null
     * @return the string with any replacements processed
     */
    public static String toHTML(String str) {

        if (str == null) { return ""; }

        String RESULT = str;
        String result = replaceSmartChars(RESULT);

        RESULT = replace(result,"'","&#39;");
        result = replace(RESULT,"%","&#37;");
        RESULT = replace(result,"\"","&#34;");
        result = replace(RESULT,"(","&#40;");
        RESULT = replace(result,")","&#41;");
        result = replace(RESULT,"\\","&#92;");
        RESULT = replace(result,"^","&#94;");
        result = replace(RESULT,"_","&#95;");
//        RESULT = replace(result,"<","&#60;");
//       result = replace(RESULT,">","&#62;");
		RESULT = replace(result,",","&#44;");

        result = replace(RESULT,"\r\n","<BR>");
        RESULT = replace(result,"\n","<BR>");

        return RESULT;

    }

    /**
     * <p>Replaces occurences of HTML codes within a String with
     * corresponding special character.</p>
     *
     * <p>A <code>null</code> reference passed to this method returns "".</p>
     *
     * <pre>
     * Operator.fromHTML(null)    = ""
     * Operator.fromHTML("")      = ""
     * Operator.fromHTML("10&#37;") = "10%"
     * </pre>
     *
     * @param str  string to search and replace in, may be null
     * @return the string with any replacements processed
     */
    public static String fromHTML(String str) {

        if (str == null) { return ""; }

        String RESULT = str;
        String result = RESULT;

        RESULT = replace(result,"&#39;","'");
        result = replace(RESULT,"&#37;","%");
        RESULT = replace(result,"&#34;","\"");
        result = replace(RESULT,"&#40;","(");
        RESULT = replace(result,"&#41;",")");
        result = replace(RESULT,"&#92;","\\");
        RESULT = replace(result,"&#94;","^");
        result = replace(RESULT,"&#95;","_");
        RESULT = replace(result,"&#60;","<");
        result = replace(RESULT,"&#62;",">");
        RESULT = replace(result,"&#44;",",");

        result = replace(RESULT,"<BR>","\n");

        return result;

    }

    /**
     * <p>Replaces occurences of special characters within a String with
     * corresponding ASCII codes.</p>
     *
     * <p>A <code>null</code> reference passed to this method returns "".</p>
     *
     * <pre>
     * Operator.toASCII(null)    = ""
     * Operator.toASCII("")      = ""
     * Operator.toASCII("a b c") = "a%20b%20c"
     * </pre>
     *
     * @param str  string to search and replace in, may be null
     * @return the string with any replacements processed
     */
    public static String toASCII(String str) {

        if (str == null) { return ""; }

        String RESULT = replaceSmartChars(str);
        String result = replaceSmartChars(str);

		RESULT = replace(result,"%","%25");
        result = replace(RESULT," ","%20");
        RESULT = replace(result,"\"","%22");
        result = replace(RESULT,"&","%26");
        RESULT = replace(result,"=","%3D");
        result = replace(RESULT,"?","%3F");
        RESULT = replace(result,"<","%3C");
        result = replace(RESULT,">","%3E");
        RESULT = replace(result,":","%3A");
        result = replace(RESULT,";","%3B");
        RESULT = replace(result,"'","%27");
        result = replace(RESULT,"+","%2B");
        RESULT = replace(result,"#","%23");

        result = replace(RESULT,"\r\n","%0A");
        RESULT = replace(result,"\n","%0A");

        return RESULT;

    }

    /**
     * <p>Replaces occurences of ASCII codes within a String with
     * proper ASCII code.</p>
     *
     * <p>A <code>null</code> reference passed to this method returns "".</p>
     *
     * <pre>
     * Operator.fromASCII(null)    = ""
     * Operator.fromASCII("")      = ""
     * Operator.fromASCII("a%20b%20a") = "a b a"
     * </pre>
     *
     * @param str  string to search and replace in, may be null
     * @return the string with any replacements processed
     */
    public static String fromASCII(String str) {

        if (str == null) { return ""; }

        String RESULT = str;
        String result = "";

        result = replace(RESULT,"%20"," ");
        RESULT = replace(result,"%22","\"");
        result = replace(RESULT,"%26","&");
        RESULT = replace(result,"%3D","=");
        result = replace(RESULT,"%3F","?");
        RESULT = replace(result,"%3C","<");
        result = replace(RESULT,"%3E",">");
        RESULT = replace(result,"%3A",":");
        result = replace(RESULT,"%3B",";");
        RESULT = replace(result,"%27","'");
        result = replace(RESULT,"%2B","+");

        RESULT = replace(result,"%0A","\n");

        return RESULT;

    }

    /**
     * <p>Replaces occurences of ASCII codes and HTML codes within a String with
     * corresponding special character.</p>
     *
     * <p>A <code>null</code> reference passed to this method returns "".</p>
     *
     * <pre>
     * Operator.toText(null)    = ""
     * Operator.toText("")      = ""
     * Operator.toText("a%20b%20a<BR>") = "a b a\n"
     * </pre>
     *
     * @param str  string to search and replace in, may be null
     * @return the string with any replacements processed
     */
    public static String toText(String str) {

        if (str == null) { return ""; }

        String RESULT = fromASCII(str);
        String result = fromHTML(RESULT);

        return result;

    }

	/**
	 * Replace characters in specified string that would create an error in a vCalendar.
	 *
	 * @param str The string to convert.
	 * @return Returns converted string.
	 */
	public static String vCalFriendly(String str) {
		String RESULT = toText(str);
		String result = Operator.replace(RESULT,"=","=3D");
		RESULT = Operator.replace(result,"\r\n","=0D=0A");
		result = Operator.replace(RESULT,"\n\r","=0D=0A");
		RESULT = Operator.replace(result,"\n","=0D=0A");
		result = Operator.replace(RESULT,"\r","=0D=0A");
		return Operator.replace(result,"<BR>","=0D=0A");
	}

	/**
	 * Escape single quotes in specified string to remove possibility of sql injections.
	 *
	 * @param str  string to search and replace, may be null
	 * @return the string with any replacements processed
	 */
	public static String sqlEscape(String str) {
		if (str == null) { return ""; }
		String result = replaceSmartChars(str);
		result = replace(result,"'","''");
		return result;
	}

	/**
	 * Escape single quotes in specified string to remove possibility of sql injections.
	 *
	 * @param str  string to search and replace, may be null
	 * @return the string with any replacements processed
	 */
	public static String sqlTextEscape(String str) {
		if (str == null) { return ""; }
		String result = replace(str,"'","''");
		return result;
	}

	/**
	 * Replace characters in specified string so that the resulting string can be placed as a query in a url address.
	 *
	 * @param str  string to search and replace, may be null
	 * @return the string with any replacements processed
	 */
	public static String urlFriendly(String str) {
		if (str == null) { return ""; }
		return toASCII(str);
	}

    /**
     * Replace characters in a specified string that could create errors in a cookie
     *
     * @param str  string to search and replace, may be null
     * @return the string with any replacements processed
     */
    public static String cookieFriendly(String str) {

        if (str == null) { return ""; }

        String RESULT = replaceSmartChars(str);
        String result = "";

        result = replace(RESULT," ","|W|");
        RESULT = replace(result,";","|S|");
        result = replace(RESULT,",","|C|");
        RESULT = replace(result,"\n","|N|");
        result = replace(RESULT,"\t","|T|");
        RESULT = replace(result,"@","|A|");
        result = replace(RESULT,"=","|E|");
        RESULT = replace(result,"\"","|Q|");
        result = replace(RESULT,"/","|SL|");
        RESULT = replace(result,"?","|QU|");
        result = replace(RESULT,"(","|OP|");
        RESULT = replace(result,")","|CP|");
        result = replace(RESULT,"[","|OB|");
        RESULT = replace(result,"]","|CB|");

        return RESULT;

    }

    /**
     * Return string that may have been processed in cookieFriendly(str) to it's original state
     *
     * @param str  string to search and replace, may be null
     * @return the string with any replacements processed
     */
    public static String cookieUnfriendly(String str) {
        if (str == null) { return ""; }
        String RESULT = str;
        String result = "";
        result = replace(RESULT,"|W|"," ");
        RESULT = replace(result,"|S|",";");
        result = replace(RESULT,"|C|",",");
        RESULT = replace(result,"|N|","\n");
        result = replace(RESULT,"|T|","\t");
        RESULT = replace(result,"|A|","@");
        result = replace(RESULT,"|E|","=");
        RESULT = replace(result,"|Q|","\"");
        result = replace(RESULT,"|SL|","/");
        RESULT = replace(result,"|QU|","?");
        result = replace(RESULT,"|OP|","(");
        RESULT = replace(result,"|CP|",")");
        result = replace(RESULT,"|OB|","[");
        RESULT = replace(result,"|CB|","]");
        return RESULT;

    }

    /**
     * Escape all occurences of quotes and new lines.
     *
     * @param str  string to search and replace in, may be null
     * @return the string with any replacements processed
     */
    public static String escape(String str) {

        if (str == null) { return ""; }

        String RESULT = replaceSmartChars(str);
        String result = "";

        result = replace(RESULT,"\"","\\\"");
        RESULT = replace(result,"\n","\\n");

        return RESULT;

    }

    /**
	 * Replace characters in specified string so that the resulting string can be placed in an html script.
     *
     * @param str  string to search and replace in, may be null
     * @return the string with any replacements processed
     */
    public static String javascriptFriendly(String str) {

        if (str == null) { return ""; }

        String result = Operator.replaceSmartChars(str);

		result = replace(result,"\"","\\\"");
		result = replace(result,"\'","\\\'");
		result = replace(result,"\r\n","\n");
        result = replace(result,"\n","\\n");

        return result;

    }

    public static String javascriptFriendly(String str, String modifier) {

        if (str == null) { return ""; }

        String result = Operator.replaceSmartChars(str);

        if (modifier.equalsIgnoreCase("\"")) {
    		result = replace(result,"\"","\\\"");
        }
        if (modifier.equalsIgnoreCase("'")) {
    		result = replace(result,"\'","\\\'");
        }
		result = replace(result,"\r\n","\n");
        result = replace(result,"\n","\\n");

        return result;

    }

    /**
	 * Replace characters in specified string so that the resulting string can be placed in an html input field.
     *
     * @param str  string to make friendly, may be null
     * @return the string with any replacements processed
     */
    public static String formFriendly(String str) {

        if (str == null) { return ""; }

        String RESULT = replaceSmartChars(str);
        String result = "";

        result = replace(RESULT,"\"","&quot;");
        RESULT = replace(result,"\r\n","\n");

        return RESULT;

    }

	/**
	 * Examines specified string and replaces strings that could provide problems inside an html textarea
	 * @param str - the String to search and replace in
	 * @return String that could be placed inside an html textarea
	 */
	public static String toTextarea(String str) {
		if (str == null) { return ""; }
		String result = Operator.replace(str, "\r\n", "\n");
		result = Operator.replace(str, "\r", "\n");
		result = Operator.replace(str, "<br>", "\n");
		result = Operator.replace(str, "<BR>", "\n");
		result = Operator.replace(str, "<Br>", "\n");
		result = Operator.replace(str, "<bR>", "\n");
		result = Operator.replace(str, "<textarea>", "");
		result = Operator.replace(str, "</textarea>", "");
		result = Operator.replace(str, "<TEXTAREA>", "");
		result = Operator.replace(str, "</TEXTAREA>", "");
		return result;
	}

	/**
	 * Replace characters in specified string so that the resulting string can be placed in an html hidden input field.
	 *
	 * @param str  string to make friendly, may be null
	 * @return the string with any replacements processed
	 */
	public static String hiddenFriendly(String str) {
		if (str == null) { return ""; }
		str = replaceSmartChars(str);
		str = replace(str,"\"","&quot;");
		str = replace(str,">","&gt;");
		str = replace(str,"<","&lt;");
		str = replace(str,"\r\n","|NL|");
		str = replace(str,"\n","|NL|");
		str = replace(str,"\r","|NL|");
		return str;
	}

    /**
     * Return string that may have been processed in hiddenFriendly(str) to it's original state
     *
     * @param str  string to search and replace, may be null
     * @return the string with any replacements processed
     */
	public static String hiddenUnfriendly(String str) {
		if (str == null) { return ""; }
		str = replace(str,"&quot;","\"");
		str = replace(str,"&gt;",">");
		str = replace(str,"&lt;","<");
		str = replace(str,"|NL|","\n");
		return str;
	}

    /**
     * Make string xml friendly. This method will not replace the ampersand symbols to html format(&#38;).</p>
     *
     * @param str  string to make friendly, may be null
     * @return the string with any replacements processed
     */
    public static String xmlFriendly(String str) {
        return xmlFriendly(str, true);
    }

    /**
     * Make string xml friendly.
     *
     * @param str  string to make friendly, may be null
     * @param and If <code>true</code> the method will replace the ampersand symbols to html format(&#38;).
     * @return the string with any replacements processed
     */
    public static String xmlFriendly(String str, boolean and) {

        if (str == null) { return ""; }

        String RESULT;
        String result;

        if (and) { RESULT = Operator.replace(str,"&","&#38;"); }
        else { RESULT = str; }

        result = Operator.replace(RESULT,"<","&#60;");
        RESULT = Operator.replace(result,">","&#62;");
        result = Operator.replace(RESULT,"'","&#39;");
        RESULT = Operator.replace(result,"\"","&#34;");

        return RESULT;

    }

    /**
     * <p>Add HTML hyperlinks to all found addresses in a string.
     *
     * <p>A <code>null</code> reference passed to this method returns "".</p>
     *
     * <pre>
     * Operator.hyperlink(null)    = ""
     * Operator.hyperlink("")      = ""
     * Operator.hyperlink("the address is http://url.com") = "the address is <a href=http://url.com>http://url.com</a>"
     * </pre>
     *
     * @param str  string to search and replace in, may be null
     * @return the string with any replacements processed
     */
    public static String hyperlink(String str) {

        String[] ARRAY = split(str," ");
        String STRING = "";
        StringBuffer ARRAYBUFFER = new StringBuffer(0);
        int ARRAYLENGTH = ARRAY.length;

        for (int i=0; i<ARRAYLENGTH; i++) {
            STRING = ARRAY[i];
            if (Operator.isURL(STRING)) {
                ARRAYBUFFER = new StringBuffer(5000);
                ARRAYBUFFER.append("<a href=\"").append(STRING).append("\">").append(STRING).append("</a>");
                ARRAY[i] = ARRAYBUFFER.toString();
            }
            else if (Operator.isEmail(STRING)) {
                ARRAYBUFFER = new StringBuffer(5000);
                ARRAYBUFFER.append("<a href=\"mailto:").append(STRING).append("\">").append(STRING).append("</a>");
                ARRAY[i] = ARRAYBUFFER.toString();
            }
        }

        return (join(ARRAY," "));
    }

    /**
     * Create a random string.
     *
     * @param length Length of string.
     * @return Returns random <code>String</code>.
     */
    public static String randomString(int length) {

        char[] chars = new char[52];
        for (int i = 0; i < 26; i ++) {
            chars[i] = (char) (97 + i);
            chars[i + 26] = (char) (65 + i);
        }

        Random random = new Random ();

        char[] array = new char[length];
        for (int i = 0; i < length; i ++) {
            array[i] = chars[random.nextInt (chars.length)];
        }
        return new String (array);

    }

    /**
     * Create a random 10 character string.
     *
     * @return Returns random <code>String</code>.
     */
    public static String randomString() { return randomString(10); }

    /**
     * Create a random string.
     *
     * @param length Length of string.
     * @return Returns random <code>String</code>.
     */
    public static String randomString(String length) {
        if (!Operator.hasValue(length)) { return randomString(10); }
        else if (isNumber(length)) { return randomString(toInt(length)); }
        else { return length; }
    }

	/**
	 * Make specified string safe to receive from an HttpRequest
	 * @param str - The string to search and replace in
	 * @return String that is safe to receive from an HttpRequest
	 */
	public static String safe(String str) {
		str = replaceIgnoreCase(str, "script ", "&#115;cript ");
		str = replaceIgnoreCase(str, " script", " &#115;cript");
		str = replaceIgnoreCase(str, " script\n", "&#115;cript\n");
		str = replaceIgnoreCase(str, "\nscript", "\n&#115;cript");
		str = replaceIgnoreCase(str, "onmouseover", "&#111;nmouseover");
		str = replaceIgnoreCase(str, "object", "&#111;bject");
		str = replaceIgnoreCase(str, "embed", "&#101;mbed");
		str = replaceIgnoreCase(str, "applet", "&#97;pplet");
		str = replaceIgnoreCase(str, "iframe", "&#105;frame");
		str = replaceIgnoreCase(str, "%3c", "&gt;");
		str = replaceIgnoreCase(str, "x3c", "&lt;");
		str = replace(str, "!", "&#33;");
		str = replace(str, "*", "&#42;");
//		str = replace(str, "'", "&#39;");
//		str = replace(str, "{", "&#123;");
//		str = replace(str, "}", "&#125;");
		str = replace(str, "\\", "&#92;");
		return str;
	}

	/**
	 * Check if specified string is equal to a known and permitted extension type
	 * @param str - file extension
	 * @return true if specified string is equal to a known and permitted extension type
	 */
	public static boolean permittedExt(String str) {
		String ct = contentType(str);
		return Operator.hasValue(ct);
	}

	public static String contentType(File f) {
		String e = getExt(f);
		return contentType(e);
	}

	public static String getExt(File f) {
		String n = f.getName();
		return getExt(n);
	}

	public static String getExt(String filename) {
		if (!Operator.hasValue(filename)) { return ""; }
		int dot = filename.lastIndexOf(".");
		String e = filename.substring(dot + 1);
		return e;
	}

	public static String fileType(String filename) {
		if (!Operator.hasValue(filename)) { return ""; }
		String ext = getExt(filename);
		if (!Operator.hasValue(ext)) { return ""; }
		else if (ext.equalsIgnoreCase("gif"))   { return "image"; }
		else if (ext.equalsIgnoreCase("jpg"))   { return "image"; }
		else if (ext.equalsIgnoreCase("jpeg"))  { return "image"; }
		else if (ext.equalsIgnoreCase("png"))   { return "image"; }
		else if (ext.equalsIgnoreCase("bmp"))   { return "image"; }
		else if (ext.equalsIgnoreCase("css"))   { return "style"; }
		else if (ext.equalsIgnoreCase("js"))    { return "script"; }
		else if (ext.equalsIgnoreCase("aiff"))  { return "audio"; }
		else if (ext.equalsIgnoreCase("au"))    { return "audio"; }
		else if (ext.equalsIgnoreCase("mp3"))   { return "audio"; }
		else if (ext.equalsIgnoreCase("avi"))   { return "video"; }
		else if (ext.equalsIgnoreCase("mov"))   { return "video"; }
		else if (ext.equalsIgnoreCase("mpg"))   { return "video"; }
		else if (ext.equalsIgnoreCase("mpeg"))  { return "video"; }
		else if (ext.equalsIgnoreCase("wma"))   { return "video"; }
		else if (ext.equalsIgnoreCase("wmv"))   { return "video"; }
		else if (ext.equalsIgnoreCase("mp4"))   { return "video"; }
		else if (ext.equalsIgnoreCase("m4v"))   { return "video"; }
		else if (ext.equalsIgnoreCase("ra"))   { return "video"; }
		else if (ext.equalsIgnoreCase("ram"))   { return "video"; }
		
		else if (ext.equalsIgnoreCase("html"))  { return "webdoc"; }
		else if (ext.equalsIgnoreCase("xml"))   { return "webdoc"; }
		else if (ext.equalsIgnoreCase("rss"))   { return "webdoc"; }
		else if (ext.equalsIgnoreCase("jsp"))   { return "webdoc"; }
		
		else if (ext.equalsIgnoreCase("pdf"))   { return "document"; }
		else if (ext.equalsIgnoreCase("ppt"))   { return "document"; }
		else if (ext.equalsIgnoreCase("pptx"))   { return "document"; }
		else if (ext.equalsIgnoreCase("doc"))   { return "document"; }
		else if (ext.equalsIgnoreCase("docx"))   { return "document"; }
		else if (ext.equalsIgnoreCase("vdx"))   { return "document"; }
		else if (ext.equalsIgnoreCase("vsd"))   { return "document"; }
		else if (ext.equalsIgnoreCase("xls"))   { return "document"; }
		else if (ext.equalsIgnoreCase("xlsx"))   { return "document"; }
		else if (ext.equalsIgnoreCase("csv"))   { return "document"; }
		
		else if (ext.equalsIgnoreCase("ai"))   { return "document"; }
		else if (ext.equalsIgnoreCase("mpp"))   { return "document"; }
		else if (ext.equalsIgnoreCase("wks"))   { return "document"; }
		
		
		
		else { return "webdoc"; }
	}

	/**
	 * Check if specified string is equal to a known and permitted extension type
	 * @param str - file extension
	 * @return true if specified string is equal to a known and permitted extension type
	 */
	public static String contentType(String str) {
		if (str.equalsIgnoreCase("ai"))         { return "application/postscript"; } // Adobe Illustrator artwork
		else if (str.equalsIgnoreCase("aiff"))  { return "audio/x-aiff"; } // Mac audio file
		else if (str.equalsIgnoreCase("au"))    { return "audio/basic"; } // Mac/Unix audio file
		else if (str.equalsIgnoreCase("avi"))   { return "video/x-msvideo"; } // Video file
		else if (str.equalsIgnoreCase("bmp"))   { return "image/bmp"; } // Bitmap
		else if (str.equalsIgnoreCase("doc"))   { return "application/msword"; } // Microsoft Word document
		else if (str.equalsIgnoreCase("docx"))  { return "application/msword"; } // Microsoft Word document
		else if (str.equalsIgnoreCase("eps"))   { return "application/postscript"; } // Encapsulated postscript document
		else if (str.equalsIgnoreCase("gif"))   { return "image/gif"; } // gif image
		else if (str.equalsIgnoreCase("ics"))   { return "text/calendar"; } // iCalendar File
		else if (str.equalsIgnoreCase("jpeg"))  { return "image/jpeg"; } // jpg image
		else if (str.equalsIgnoreCase("jpg"))   { return "image/jpeg"; } // jpg image
		else if (str.equalsIgnoreCase("mov"))   { return "video/quicktime"; } // Quicktime movie
		else if (str.equalsIgnoreCase("mp3"))   { return "audio/mpeg"; } // mp3 audio
		else if (str.equalsIgnoreCase("mp4"))   { return "video/mp4"; } // mpeg movie
		else if (str.equalsIgnoreCase("mpeg"))  { return "video/mpeg"; } // mpeg movie
		else if (str.equalsIgnoreCase("mpg"))   { return "video/mpeg"; } // mpeg movie
		else if (str.equalsIgnoreCase("mpp"))   { return "application/vnd.ms-project"; } // Microsoft Project Document
		else if (str.equalsIgnoreCase("pdf"))   { return "application/pdf"; } // Adobe Acrobat pdf document
		else if (str.equalsIgnoreCase("png"))   { return "image/png"; } // png image
		else if (str.equalsIgnoreCase("ppt"))   { return "application/vnd.ms-powerpoint"; } // Microsoft Powerpoint presentation
		else if (str.equalsIgnoreCase("pptx"))  { return "application/vnd.ms-powerpoint"; } // Microsoft Powerpoint presentation
		else if (str.equalsIgnoreCase("psd"))   { return "application/photoshop"; } // Adobe Photoshop file
		else if (str.equalsIgnoreCase("qt"))    { return "video/quicktime"; } // Quicktime movie
		else if (str.equalsIgnoreCase("ra"))    { return "audio/x-pn-realaudio"; } // Real Audio file
		else if (str.equalsIgnoreCase("ram"))   { return "audio/x-pn-realaudio"; } // Real Audio file
		else if (str.equalsIgnoreCase("swf"))   { return "application/x-shockwave-flash"; } // Macromedia flash movie
		else if (str.equalsIgnoreCase("tif"))   { return "image/tiff"; } // Tiff image
		else if (str.equalsIgnoreCase("tiff"))  { return "image/tiff"; } // Tiff image
		else if (str.equalsIgnoreCase("ttf"))   { return "application/x-font"; } // True type font
		else if (str.equalsIgnoreCase("txt"))   { return "text/plain"; } // Text file
		else if (str.equalsIgnoreCase("vcf"))   { return "text/calendar"; } // vCard File
		else if (str.equalsIgnoreCase("vcs"))   { return "text/calendar"; } // vCalendar File
		else if (str.equalsIgnoreCase("vdx"))   { return "application/x-visio"; } // Microsoft Visio drawing
		else if (str.equalsIgnoreCase("vsd"))   { return "application/x-visio"; } // Microsoft Visio drawing
		else if (str.equalsIgnoreCase("wav"))   { return "audio/x-wav"; } // Windows wav audio file
		else if (str.equalsIgnoreCase("wks"))   { return "application/vnd.ms-works"; } // Microsoft Works document
		else if (str.equalsIgnoreCase("wma"))   { return "audio/x-ms-wma"; } // Microsoft Windows Audio file
		else if (str.equalsIgnoreCase("wmv"))   { return "audio/x-ms-wmv"; } // Microsoft Windows Movie file
		else if (str.equalsIgnoreCase("xls"))   { return "application/vnd.ms-excel"; } // Microsoft Excel workbook
		else if (str.equalsIgnoreCase("xlsx"))  { return "application/vnd.ms-excel"; } // Microsoft Excel workbook
		else if (str.equalsIgnoreCase("xml"))   { return "text/xml"; } // XML file
		else if (str.equalsIgnoreCase("csv"))   { return "text/csv"; } // CSV Comma Separated Values file
		return "";
	}

	/**
	 * Get the substring of a specified string. This method will not throw a null pointer exception
	 * @param str - The string which substring is to be returned.
	 * @param start - the beginning index of the substring
	 * @param end - the ending index of the substing
	 * @return the substring of the specified string 
	 */
	public static String subString(String str, int start, int end) {
		if (str.length() < start) { return ""; }
		if (str.length() < end) { end = str.length(); }
		return str.substring(start, end);
	}

	/**
	 * Get the substring of a specified string starting from the beginning of the string to the specified number of characters. This method will not throw a null pointer exception.
	 * @param str - The string which substring is to be returned.
	 * @param chars - the ending index of the substing
	 * @param def - if the result is null or empty, return this value
	 * @return the substring of the specified string 
	 */
	public static String getBegString(String str, int chars, String def) {
		String value = subString(str, 0, chars);
		if (!Operator.hasValue(value)) { return def; }
		return value;
	}

	/**
	 * Get the substring of a specified string starting from the beginning of the string to the specified number of characters. This method will not throw a null pointer exception.
	 * @param str - The string which substring is to be returned
	 * @param chars - the ending index of the substing
	 * @return the substring of the specified string 
	 */
	public static String getBegString(String str, int chars) {
		String value = subString(str, 0, chars);
		if (!Operator.hasValue(value)) { return ""; }
		return value;
	}

	/**
	 * Get the last n number of characters of a specified string where n is specified. This method will not throw a null pointer exception.
	 * @param str - The string which substring is to be returned
	 * @param chars - the number of characters to return
	 * @return the substring of the specified string 
	 */
	public static String getEndString(String str, int chars) {
		int l = str.length();
		int start = l - chars;
		if (start < 0) { start = 0; } 
		return subString(str, start, l);
	}

	/**
	 * Remove non alphanumeric characters from specified string
	 * @param str - The string to search and replace
	 * @return specified string minus non alphanumeric characters
	 */
	public static String alphanumeric(String str) {
		 char[] array = new char[str.length()];
		 String newString = "";
         
		 array = str.toCharArray();
		 for (int i = 0; i < str.length(); i++) {
			  char ch = array[i];
			  if (     ((ch >= 'a') && (ch <= 'z')) ||
					   ((ch >= 'A') && (ch <= 'Z')) ||
					   ((ch >= '0') && (ch <= '9'))
				  ) {
					   newString += ch;
			  }
		 }
		 return newString;
	}

	/**
	 * Get the class name of specified object
	 * @param o - the object
	 * @return class name of specified object
	 */
	public static String getClassName(Object o) {
		String n = o.getClass().getSimpleName();
		return n;
	}

	/**
	 * Add two specified doubles.
	 * @param a
	 * @param b
	 * @return sum of specified doubles
	 */
	public static double addDouble(double a, double b){
		
		BigDecimal num1 = new BigDecimal(a+"");
		BigDecimal num2 = new BigDecimal(b+"");
 
		return(num1.add(num2).doubleValue());
	}

	/**
	 * Multiply two specified doubles.
	 * @param a
	 * @param b
	 * @return multiplication of specified doubles
	 */
	public static double multiplyDouble(double a, double b){
		BigDecimal num1 = new BigDecimal(a+"");
		BigDecimal num2 = new BigDecimal(b+"");
		return(num1.multiply(num2).doubleValue());
	}

   public static String sizeToHumanReadable(long size) {
		String out = Math.round(size / 1024) + "K";
		if (out.equals("0K")) {
			out = size + "B";
		}
		return out;
	}

	public static String doCharsetCorrections(String str) {
		if (str == null) return "";

		String extraChars[] = {"\u00FD","\u00DD","\u00FE","\u00DE","\u00F0","\u00D0"};
		String unicode[] = {"\u0131", "\u0130", "\u015F", "\u015E", "\u011F", "\u011E"};
		for (int i=0;i<extraChars.length;i++) {
			while (str.indexOf(extraChars[i]) != -1) {
				String tmp = str;
				str = tmp.substring(0, tmp.indexOf(extraChars[i]))
					+ unicode[i] + tmp.substring (tmp.indexOf(extraChars[i])+1, tmp.length());
			}
		}
		return str;
	}


   /**
	 *
	 * @param str
	 * @param trimStr
	 * @return
	 */
	public static String extendedTrim(String str, String trimStr) {
		if (str == null || str.length() == 0)
			return str;
		for (str = str.trim(); str.startsWith(trimStr); str = str.substring(trimStr.length()).trim());
		for (; str.endsWith(trimStr); str = str.substring(0, str.length() - trimStr.length()).trim());
		return str;
	}

	public static String removeOpeningSlash(String str) {
		if (Operator.hasValue(str) && str.startsWith("/")) {
			str = subString(str, 1, str.length());
		}
		return str;
	}

	public static String removeTrailingSlash(String str) {
		if (Operator.hasValue(str) && str.endsWith("/")) {
			str = subString(str, 0, str.length() - 1);
		}
		return str;
	}

	public static String removeOpeningAndTrailingSlash(String str) {
		str = removeOpeningSlash(str);
		str = removeTrailingSlash(str);
		return str;
	}

	public static String addTrailingSlash(String str) {
		if (!Operator.hasValue(str)) { return ""; }
		str = removeTrailingSlash(str);
		StringBuffer sb = new StringBuffer();
		sb.append(str);
		sb.append("/");
		return sb.toString();
	}

	public static int getTotalPages(int numofrecords, int maxperpage) {
		int totalpages = (int) Math.ceil((double) numofrecords / (double) maxperpage);
		return totalpages;
	}

	public static String pageIndex(String url, int totalpages, int currentpage) {
		return pageIndex(url, totalpages, currentpage, 8);
	}

	public static String pageIndex(String url, int totalpages, int currentpage, int numtodisplay) {
		int num = numtodisplay - 2;
		String cssstyle = "pageindex";
		if (totalpages <= 1) { return ""; }
		int start = 0;
		int end = 0;
		start = currentpage - ((int) (num/2));
		if (start < 1) { start = 1; }
		end = start + num;
		if (end > totalpages) { end = totalpages; }
		if (start > totalpages - num) { start = totalpages - num; }
		if (start < 1) { start = 1; }

		if (url.endsWith("&") || url.endsWith("?")) {
			url = Operator.subString(url, 0, url.length() - 1);
		}

		StringBuffer sb = new StringBuffer();
		sb.append(url);
		if (url.indexOf("?") > -1) { sb.append("&"); }
		else { sb.append("?"); }
		url = sb.toString();

		sb = new StringBuffer();

		if (currentpage > 1) {
			sb.append(" <a href=\"").append(url).append("PAGE=").append(currentpage - 1).append("\" class=\"").append(cssstyle).append("\">&lt;&lt;</a> ");
			sb.append("&nbsp;");
		}

		if (start > 1) {
			if (start > 2) {
				sb.append(" <a href=\"").append(url).append("PAGE=1\" class=\"").append(cssstyle).append("\">1 ...</a> ");
			}
			else {
				sb.append(" <a href=\"").append(url).append("PAGE=1\" class=\"").append(cssstyle).append("\">1</a> ");
			}
			sb.append("&nbsp;");
		}

		for (int i = start; i <= end; i++) {
			if (i > start) {
				sb.append("&nbsp;");
			}
			if (i == currentpage) {
				sb.append(" <a href=\"").append(url).append("PAGE=").append(i).append("\" class=\"").append(cssstyle).append("-current\">").append(i).append("</a> ");
			}
			else {
				sb.append(" <a href=\"").append(url).append("PAGE=").append(i).append("\" class=\"").append(cssstyle).append("\">").append(i).append("</a> ");
			}
		}

		if (end < totalpages) {
			sb.append("&nbsp;");
			if (end < (totalpages-1)) {
				sb.append(" <a href=\"").append(url).append("PAGE=").append(totalpages).append("\" class=\"").append(cssstyle).append("\">... ").append(totalpages).append("</a> ");
			}
			else {
				sb.append(" <a href=\"").append(url).append("PAGE=").append(totalpages).append("\" class=\"").append(cssstyle).append("\">").append(totalpages).append("</a> ");
			}
		}

		if (currentpage < totalpages) {
			if (currentpage < totalpages) {
				sb.append("&nbsp;");
			}
			sb.append(" <a href=\"").append(url).append("PAGE=").append(currentpage + 1).append("\" class=\"").append(cssstyle).append("\">&gt;&gt;</a> ");
		}

		return sb.toString();
	}

	public static ArrayList<String> moveListItemUp(ArrayList<String> list, String item) {
		int index = list.indexOf(item);
		if (index < 1) { return list; }
		int above = index - 1;
		String aboveitem = list.get(above);
		list.set(index, aboveitem);
		list.set(above, item);
		return list;
	}

	public static ArrayList<String> moveListItemDown(ArrayList<String> list, String item) {
		int index = list.indexOf(item);
		if (index >= list.size() - 1) { return list; }
		int below = index + 1;
		String belowitem = list.get(below);
		list.set(index, belowitem);
		list.set(below, item);
		return list;
	}

	public static String[] moveUp(String[] array, String item) {
		if (!Operator.hasValue(array)) { return new String[0]; }
		int l = array.length;
		int index = -1;
		for (int i=0; i < l; i++) {
			if (array[i].equalsIgnoreCase(item)) {
				index = i;
				break;
			}
		}
		if (index < 1) { return array; }
		int above = index - 1;
		String theitem = array[index];
		String aboveitem = array[above];
		array[above] = theitem;
		array[index] = aboveitem;
		return array;
	}

	public static int[] moveUp(int[] array, int item) {
		if (!Operator.hasValue(array)) { return new int[0]; }
		int l = array.length;
		int index = -1;
		for (int i=0; i < l; i++) {
			if (array[i] == item) {
				index = i;
				break;
			}
		}
		if (index < 1) { return array; }
		int above = index - 1;
		int theitem = array[index];
		int aboveitem = array[above];
		array[above] = theitem;
		array[index] = aboveitem;
		return array;
	}

	public static String[] moveDown(String[] array, String item) {
		if (!Operator.hasValue(array)) { return new String[0]; }
		int l = array.length;
		int index = -1;
		for (int i=0; i < l; i++) {
			if (array[i].equalsIgnoreCase(item)) {
				index = i;
				break;
			}
		}
		if (index >= l - 1) { return array; }
		int below = index + 1;
		String theitem = array[index];
		String belowitem = array[below];
		array[below] = theitem;
		array[index] = belowitem;
		return array;
	}

	public static int[] moveDown(int[] array, int item) {
		if (!Operator.hasValue(array)) { return new int[0]; }
		int l = array.length;
		int index = -1;
		for (int i=0; i < l; i++) {
			if (array[i] == item) {
				index = i;
				break;
			}
		}
		if (index >= l - 1) { return array; }
		int below = index + 1;
		int theitem = array[index];
		int belowitem = array[below];
		array[below] = theitem;
		array[index] = belowitem;
		return array;
	}

	public static String[] parseEnclosedString(String str) {
		String str1 = str;
		ArrayList<String> a = new ArrayList<String>();
		Pattern p = Pattern.compile("\"([^\"]*)\"");
		Matcher m = p.matcher(str);
		while (m.find()) {
			String e = m.group(1);
			a.add(e);
			str1 = Operator.replace(str1, "\"".concat(e).concat("\""), "");
		}

		p = Pattern.compile("'([^\"]*)'");
		m = p.matcher(str1);
		while (m.find()) {
			String e = m.group(1);
			a.add(e);
			str1 = Operator.replace(str1, "'".concat(e).concat("'"), "");
		}

		String[] str1array = Operator.split(str1, " ");
		int l = str1array.length;
        for (int i=0; i<l; i++) {
        	a.add(str1array[i]);
        }
		return toArray(a);
	}

	
	/**
     * <p>Convert a String to String[].</p>
     * Based on separator.
     * @param list  the String to convert.
     * @return the converted String[] array.
     */
	 public static String[] stringtoArray(String s, String sep) {
	        // convert a String s to an Array, the elements
	        // are delimited by sep
	        StringBuffer buf = new StringBuffer(s);
	        int arraysize = 1;

	        for (int i = 0; i < buf.length(); i++) {
	            if (sep.indexOf(buf.charAt(i)) != -1) {
	                arraysize++;
	            }
	        }

	        String[] elements = new String[arraysize];
	        int y;
	        int z = 0;

	        if (buf.toString().indexOf(sep) != -1) {
	            while (buf.length() > 0) {
	                if (buf.toString().indexOf(sep) != -1) {
	                    y = buf.toString().indexOf(sep);

	                    if (y != buf.toString().lastIndexOf(sep)) {
	                        elements[z] = buf.toString().substring(0, y);
	                        z++;
	                        buf.delete(0, y + 1);
	                    } else if (buf.toString().lastIndexOf(sep) == y) {
	                        elements[z] = buf.toString().substring(0, buf.toString().indexOf(sep));
	                        z++;
	                        buf.delete(0, buf.toString().indexOf(sep) + 1);
	                        elements[z] = buf.toString();
	                        z++;
	                        buf.delete(0, buf.length());
	                    }
	                }
	            }
	        } else {
	            elements[0] = buf.toString();
	        }

	        buf = null;

	        return elements;
	    }
	 
	 /**
	     * <p>Convert a String to boolean</p>
	 	 * @return the converted boolean.
	     */
	 
	 public static boolean s2b(String s) {
	        boolean result = false;

	        if (s != null) {
	            if (s.equalsIgnoreCase("Y")) {
	                result = true;
	            }
	        }

	        return result;
	    }
	 
	
	 /** <p>Convert a boolean to string</p>
	 	 * @return the converted string Y or N
	     */
	 
	 public static String b2s(boolean b) {
	        return (!b) ? "N" : "Y";
	    }
	 
	
	 /** <p>Convert a string[] to arrayList</p>
	 	 * @return the converted arrayList
	     */
	 public static List<Object> arrayToList(Object[] objects) {
	        List<Object> objList = new ArrayList<Object>();

	        if (objects != null) {
	            for (int i = 0; i < objects.length; i++) {
	                objList.add(objects[i]);
	            }
	        }

	        return objList;
	    }

	 /** <p>Convert a string[] to commaSeparated String[]</p>
	 	 * @return the converted commaSeparated String[]
	     */
	    public static String stringArrayToCommaSeperatedString(String[] stringArray) {
	      	        if (stringArray == null) {
	            return "";
	        } else {
	            String commanSeperatedString = "";

	            for (int i = 0; i < stringArray.length; i++) {
	                commanSeperatedString += (stringArray[i] + ",");
	            }

	            return commanSeperatedString.substring(0, commanSeperatedString.length() - 1);
	        }
	    }
	    
	    
	    //converts any string to proper case
	    public static String properCase(String s) {
	        StringBuffer b = new StringBuffer(s.toLowerCase());
	        boolean icap = false;

	        for (int i = 0; i < b.length(); i++) {
	            char c = b.charAt(i);

	            if (!icap && Character.isLetter(c)) {
	                b.setCharAt(i, Character.toUpperCase(c));
	                icap = true;
	            }

	            if (!Character.isLetter(c)) {
	                icap = false;
	            }
	        }

	        return b.toString();
	    }

	    /** <p>Convert a escape string to append 'string'</p>
	 	 * @return the converted 'string' for db insert
	     */
	    public static String checkString(String aValue) {
	        if (aValue == null) {
	            return "null";
	        } else {
	            if (aValue.equals("")) {
	                return "null";
	            }

	            // preparing string for database insert
	            String tempStr = "";
	            char quot = '\'';

	            if ((aValue != null) && (aValue.length() > 0)) {
	                for (int i = 0; i < aValue.length(); i++) {
	                    char c = aValue.charAt(i);

	                    if (c != quot) {
	                        tempStr = tempStr + aValue.charAt(i);
	                    } else {
	                        tempStr = tempStr + aValue.charAt(i) + aValue.charAt(i);
	                    }
	                }

	                aValue = tempStr;
	            }

	            //else {
	            aValue = "'" + aValue + "'";

	            return aValue;

	            //}
	        }
	    }
	    
/*	    public static String dynamicEncoder(String keyword){
	    	StringBuffer sb = new StringBuffer();
	    	try{
		    	OBCTimekeeper k = new OBCTimekeeper();
		    	if(Operator.hasValue(keyword)){
		    		sb.append(keyword);
		    	}
		    	sb.append(k.DATECODE());
		    	String encode = Operator.sqlEscape(jcrypt.crypt("X6", sb.toString()));
		    	return encode;
	    	}catch(Exception e){
	    		return "";
	    	}
	    }*/
	    
	    
	/*    public static boolean dynamicEncoder(String keyword,String encoded){
	    	boolean result = false;
	    	try{
		    	if(Operator.hasValue(encoded)){
			    	StringBuffer sb = new StringBuffer();
			    	OBCTimekeeper k = new OBCTimekeeper();
			    	if(Operator.hasValue(keyword)){
			    		sb.append(keyword);
			    	}
			    	sb.append(k.DATECODE());
			    	String encode = Operator.sqlEscape(jcrypt.crypt("X6", sb.toString()));
			    	if(equalsIgnoreCase(encode, encoded)){
			    		result = true;
			    	}
			    	
			    	if(!result){
			    		sb = new StringBuffer();
				    	k.addDay(-1);
				    	if(Operator.hasValue(keyword)){
				    		sb.append(keyword);
				    	}
				    	sb.append(k.DATECODE());
				    	encode = Operator.sqlEscape(jcrypt.crypt("X6", sb.toString()));
				    	if(equalsIgnoreCase(encode, encoded)){
				    		result = true;
				    	}
			    	}
		    	}
	    	}catch(Exception e){
	    		result = false;
	    	}
	    	return result;
	    }*/

	    
	    public static void main(String args[]){
	    	String s ="1323524Zoning Surcharge (PC)-0-0-pc-1323524|1323477Plan Check Fee -0-0-pc-1323477|1323554Plan Check Renewal 50%-0-0-pc-1323554|1323538Express Plan Check Fee-0-0-pc-1323538|";
	    	String desc = "1323554Plan Check Renewal 50%";
	    	int l = s.length();
	    	int index = s.indexOf(desc);
	    	System.out.println(index);
	    	String b = s.substring(index,l);
	    	System.out.println(b);
	    	
	    	
	    	System.out.println(b.substring(0,b.indexOf("|")));
	    	String r[] = Operator.split(b.substring(0,b.indexOf("|")));
	    	/*if(r.length>0){
	    		for(int i=0;i<r)
	    	}*/
	    	
	    }

}