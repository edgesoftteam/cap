package elms.common;

public class Version {

	public static String number = "4.7.1";

	public static String date = "01/29/2021";  


	/**
	 * @return
	 */
	public static String getDate() {
		return date;
	}

	/**
	 * @return
	 */
	public static String getNumber() {
		return number;
	}

	/**
	 * @param string
	 */
	public void setDate(String string) {
		date = string;
	}

	/**
	 * @param string
	 */
	public void setNumber(String string) {
		number = string;
	}

}
