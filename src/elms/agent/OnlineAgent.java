package elms.agent;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.sql.RowSet;

import org.apache.log4j.Logger;

import elms.app.admin.PayType;
import elms.app.admin.SubProjectType;
import elms.app.common.Agent;
import elms.app.finance.ActivityFee;
import elms.app.finance.ActivityFeeEdit;
import elms.app.finance.Fee;
import elms.app.finance.FinanceSummary;
import elms.app.finance.Payment;
import elms.app.people.People;
import elms.app.project.Activity;
import elms.app.project.ActivityDetail;
import elms.common.Constants;
import elms.control.actions.project.SaveParkingActivityTabAction;
import elms.control.beans.ActivityForm;
import elms.control.beans.EmailDetails;
import elms.control.beans.EmailTemplateAdminForm;
import elms.control.beans.FeesMgrForm;
import elms.control.beans.PaymentMgrForm;
import elms.control.beans.admin.OnlinePermitForm;
import elms.control.beans.online.ApplyOnlinePermitForm;
import elms.control.beans.online.CartDetailsForm;
import elms.control.beans.online.CutoffTimesForm;
import elms.control.beans.online.HolidayEditorForm;
import elms.control.beans.online.MyPermitForm;
import elms.control.beans.online.OnlineRegisterFrom;
import elms.control.beans.online.TempPermitForm;
import elms.security.User;
import elms.util.EmailSender;
import elms.util.OBCTimekeeper;
import elms.Operator;
import elms.util.StringUtils;
import elms.util.jcrypt;
import elms.util.db.Wrapper;
import sun.jdbc.rowset.CachedRowSet;

public class OnlineAgent extends Agent {
	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(OnlineAgent.class.getName());
	public static final String INSERT_EXT_USER = "INSERT INTO EXT_USER (EXT_USER_ID, EXT_ACCT_NBR, EXT_USERNAME, EXT_PASSWORD, PIN_EXP_DT, CREATED, FIRSTNAME, LASTNAME, ACTIVEOBC, ACTIVEDOT, ADDRESS, PHONE, OBC, DOT, COMPANYNAME, PHONE_EXT, WORK_PHONE, WORK_EXT, FAX, CITY, STATE, ZIP) values (?, ?, ?, ?, null, current_timestamp,?,?,'Y','N',?,?,?,?,?,?,?,?,?,?,?,?)";

	public static final String INSERT_PEOPLE = "insert into people(PEOPLE_ID,PEOPLE_TYPE_ID,NAME,ADDR,PHONE,EMAIL_ADDR,LIC_NO,LIC_EXP_DT,PASSWORD,AGENT_NAME,EXT,FAX,CITY,STATE,ZIP,WORK_COMP_EXP_DT,WORKERS_COMP_WAIVE) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	int activityId = 0;

	Wrapper db = new Wrapper();
	RowSet rs = null;

	NumberFormat formatter = NumberFormat.getCurrencyInstance();
	/**
	 * default constructor
	 */
	public OnlineAgent() {
	}

	/**
	 * Gets the limit per day
	 * 
	 * @return
	 * @throws Exception  
	 */
	public int getMaxInspectionScheduledPerDay(String InspectionDate) throws Exception {
		int maxInspectionScheduledToday = 0;

		String sql = "select  count(*) as count   from  inspection  where INSPECTION_DT = to_date(" + StringUtils.checkString(InspectionDate) + ",'YYYY-MM-DD') and actn_code= (select actn_code  from LKUP_INSPCT_CODE where dept_id="+Constants.DEPARTMENT_DEPARTMENT_BUILDING_SAFETY+" and upper(description) = upper('" + Constants.REQUEST_FOR_INSPECTION + "'))";
		logger.debug("maxLimitScheduled" + sql);
		RowSet rs = new Wrapper().select(sql);

		if (rs.next()) {
			maxInspectionScheduledToday = rs.getInt("count");
		}

		rs.close();

		return maxInspectionScheduledToday;
	}

	public int getLimitPerDay() throws Exception {
		int limitPerDay = 0;

		String sql = "select * from lkup_day_max";

		RowSet rs = new Wrapper().select(sql);
		if (rs.next()) {
			limitPerDay = rs.getInt("day_max_value");
		}

		return limitPerDay;
	}

	/**
	 * Saves the limit per day to the database.
	 * 
	 * @param limitPerDay
	 * @throws Exception
	 */
	public void saveLimitPerDay(int limitPerDay) throws Exception {
		String sql = "update lkup_day_max set day_max_value=" + limitPerDay;
		new Wrapper().update(sql);
	}

	public void getCutoffTimes(CutoffTimesForm cutoffTimesForm) throws Exception {
		String sql = "select * from lkup_insp_time";
		RowSet rs = new Wrapper().select(sql);
		String day = "";
		String time = "";
		while (rs.next()) {
			day = rs.getString("insp_time_day");
			logger.debug("day is " + day);
			time = rs.getString("insp_time_value");
			logger.debug("time is " + time);

			if (day.equalsIgnoreCase("SCHEDULE_MONDAY_START")) {
				cutoffTimesForm.setScheduleMondayStart(time);
			} else if (day.equalsIgnoreCase("SCHEDULE_MONDAY_END")) {
				cutoffTimesForm.setScheduleMondayEnd(time);
			} else if (day.equalsIgnoreCase("SCHEDULE_TUESDAY_START")) {
				cutoffTimesForm.setScheduleTuesdayStart(time);
			} else if (day.equalsIgnoreCase("SCHEDULE_TUESDAY_END")) {
				cutoffTimesForm.setScheduleTuesdayEnd(time);
			} else if (day.equalsIgnoreCase("SCHEDULE_WEDNESDAY_START")) {
				cutoffTimesForm.setScheduleWednesdayStart(time);
			} else if (day.equalsIgnoreCase("SCHEDULE_WEDNESDAY_END")) {
				cutoffTimesForm.setScheduleWednesdayEnd(time);
			} else if (day.equalsIgnoreCase("SCHEDULE_THURSDAY_START")) {
				cutoffTimesForm.setScheduleThursdayStart(time);
			} else if (day.equalsIgnoreCase("SCHEDULE_THURSDAY_END")) {
				cutoffTimesForm.setScheduleThursdayEnd(time);
			} else if (day.equalsIgnoreCase("SCHEDULE_FRIDAY_START")) {
				cutoffTimesForm.setScheduleFridayStart(time);
			} else if (day.equalsIgnoreCase("SCHEDULE_FRIDAY_END")) {
				cutoffTimesForm.setScheduleFridayEnd(time);
			} else if (day.equalsIgnoreCase("CUTOFF_MONDAY_SCHEDULE")) {
				cutoffTimesForm.setCutoffMondaySchedule(time);
			} else if (day.equalsIgnoreCase("CUTOFF_MONDAY_CANCEL")) {
				cutoffTimesForm.setCutoffMondayCancel(time);
			} else if (day.equalsIgnoreCase("CUTOFF_TUESDAY_SCHEDULE")) {
				cutoffTimesForm.setCutoffTuesdaySchedule(time);
			} else if (day.equalsIgnoreCase("CUTOFF_TUESDAY_CANCEL")) {
				cutoffTimesForm.setCutoffTuesdayCancel(time);
			} else if (day.equalsIgnoreCase("CUTOFF_WEDNESDAY_SCHEDULE")) {
				cutoffTimesForm.setCutoffWednesdaySchedule(time);
			} else if (day.equalsIgnoreCase("CUTOFF_WEDNESDAY_CANCEL")) {
				cutoffTimesForm.setCutoffWednesdayCancel(time);
			} else if (day.equalsIgnoreCase("CUTOFF_THURSDAY_SCHEDULE")) {
				cutoffTimesForm.setCutoffThursdaySchedule(time);
			} else if (day.equalsIgnoreCase("CUTOFF_THURSDAY_CANCEL")) {
				cutoffTimesForm.setCutoffThursdayCancel(time);
			} else if (day.equalsIgnoreCase("CUTOFF_FRIDAY_SCHEDULE")) {
				cutoffTimesForm.setCutoffFridaySchedule(time);
			} else if (day.equalsIgnoreCase("CUTOFF_FRIDAY_CANCEL")) {
				cutoffTimesForm.setCutoffFridayCancel(time);
			}
		}
	}

	public void saveCutoffTimes(CutoffTimesForm cutoffTimesForm) throws Exception {
		Wrapper db = new Wrapper();
		db.beginTransaction();
		String sql = "";
		sql = "update lkup_insp_time set insp_time_value=" + StringUtils.checkString(cutoffTimesForm.getScheduleMondayStart()) + " where insp_time_day='SCHEDULE_MONDAY_START'";
		logger.debug(sql);
		db.addBatch(sql);
		sql = "update lkup_insp_time set insp_time_value=" + StringUtils.checkString(cutoffTimesForm.getScheduleMondayEnd()) + " where insp_time_day='SCHEDULE_MONDAY_END'";
		logger.debug(sql);
		db.addBatch(sql);
		sql = "update lkup_insp_time set insp_time_value=" + StringUtils.checkString(cutoffTimesForm.getScheduleTuesdayStart()) + " where insp_time_day='SCHEDULE_TUESDAY_START'";
		logger.debug(sql);
		db.addBatch(sql);
		sql = "update lkup_insp_time set insp_time_value=" + StringUtils.checkString(cutoffTimesForm.getScheduleTuesdayEnd()) + " where insp_time_day='SCHEDULE_TUESDAY_END'";
		logger.debug(sql);
		db.addBatch(sql);
		sql = "update lkup_insp_time set insp_time_value=" + StringUtils.checkString(cutoffTimesForm.getScheduleWednesdayStart()) + " where insp_time_day='SCHEDULE_WEDNESDAY_START'";
		logger.debug(sql);
		db.addBatch(sql);
		sql = "update lkup_insp_time set insp_time_value=" + StringUtils.checkString(cutoffTimesForm.getScheduleWednesdayEnd()) + " where insp_time_day='SCHEDULE_WEDNESDAY_END'";
		logger.debug(sql);
		db.addBatch(sql);
		sql = "update lkup_insp_time set insp_time_value=" + StringUtils.checkString(cutoffTimesForm.getScheduleThursdayStart()) + " where insp_time_day='SCHEDULE_THURSDAY_START'";
		logger.debug(sql);
		db.addBatch(sql);
		sql = "update lkup_insp_time set insp_time_value=" + StringUtils.checkString(cutoffTimesForm.getScheduleThursdayEnd()) + " where insp_time_day='SCHEDULE_THURSDAY_END'";
		logger.debug(sql);
		db.addBatch(sql);
		sql = "update lkup_insp_time set insp_time_value=" + StringUtils.checkString(cutoffTimesForm.getScheduleFridayStart()) + " where insp_time_day='SCHEDULE_FRIDAY_START'";
		logger.debug(sql);
		db.addBatch(sql);
		sql = "update lkup_insp_time set insp_time_value=" + StringUtils.checkString(cutoffTimesForm.getScheduleFridayEnd()) + " where insp_time_day='SCHEDULE_FRIDAY_END'";
		logger.debug(sql);
		db.addBatch(sql);
		sql = "update lkup_insp_time set insp_time_value=" + StringUtils.checkString(cutoffTimesForm.getCutoffMondaySchedule()) + " where insp_time_day='CUTOFF_MONDAY_SCHEDULE'";
		logger.debug(sql);
		db.addBatch(sql);
		sql = "update lkup_insp_time set insp_time_value=" + StringUtils.checkString(cutoffTimesForm.getCutoffMondayCancel()) + " where insp_time_day='CUTOFF_MONDAY_CANCEL'";
		logger.debug(sql);
		db.addBatch(sql);
		sql = "update lkup_insp_time set insp_time_value=" + StringUtils.checkString(cutoffTimesForm.getCutoffTuesdaySchedule()) + " where insp_time_day='CUTOFF_TUESDAY_SCHEDULE'";
		logger.debug(sql);
		db.addBatch(sql);
		sql = "update lkup_insp_time set insp_time_value=" + StringUtils.checkString(cutoffTimesForm.getCutoffTuesdayCancel()) + " where insp_time_day='CUTOFF_TUESDAY_CANCEL'";
		logger.debug(sql);
		db.addBatch(sql);
		sql = "update lkup_insp_time set insp_time_value=" + StringUtils.checkString(cutoffTimesForm.getCutoffWednesdaySchedule()) + " where insp_time_day='CUTOFF_WEDNESDAY_SCHEDULE'";
		logger.debug(sql);
		db.addBatch(sql);
		sql = "update lkup_insp_time set insp_time_value=" + StringUtils.checkString(cutoffTimesForm.getCutoffWednesdayCancel()) + " where insp_time_day='CUTOFF_WEDNESDAY_CANCEL'";
		logger.debug(sql);
		db.addBatch(sql);
		sql = "update lkup_insp_time set insp_time_value=" + StringUtils.checkString(cutoffTimesForm.getCutoffThursdaySchedule()) + " where insp_time_day='CUTOFF_THURSDAY_SCHEDULE'";
		logger.debug(sql);
		db.addBatch(sql);
		sql = "update lkup_insp_time set insp_time_value=" + StringUtils.checkString(cutoffTimesForm.getCutoffThursdayCancel()) + " where insp_time_day='CUTOFF_THURSDAY_CANCEL'";
		logger.debug(sql);
		db.addBatch(sql);
		sql = "update lkup_insp_time set insp_time_value=" + StringUtils.checkString(cutoffTimesForm.getCutoffFridaySchedule()) + " where insp_time_day='CUTOFF_FRIDAY_SCHEDULE'";
		logger.debug(sql);
		db.addBatch(sql);
		sql = "update lkup_insp_time set insp_time_value=" + StringUtils.checkString(cutoffTimesForm.getCutoffFridayCancel()) + " where insp_time_day='CUTOFF_FRIDAY_CANCEL'";
		logger.debug(sql);
		db.addBatch(sql);
		db.executeBatch();
	}

	/**
	 * save holiday
	 * 
	 * @param holidayEditorForm
	 * @throws Exception
	 */
	public void saveHoliday(HolidayEditorForm holidayEditorForm) throws Exception {
		String sql = "insert into LKUP_INSP_CAL (INSP_CAL_ID, INSP_CAL_DATE, DESCRIPTION) values (" + new Wrapper().getNextId("INSP_CAL_ID") + ", to_date(" + StringUtils.checkString(holidayEditorForm.getDate()) + ",'MM-DD-YYYY'), " + StringUtils.checkString(holidayEditorForm.getDescription()) + ")";
		logger.debug(sql);
		new Wrapper().insert(sql);
	}

	/**
	 * delete holiday
	 * 
	 * @param holidayId
	 * @throws Exception
	 */
	public void deleteHoliday(String holidayId) throws Exception {
		String sql = "delete from LKUP_INSP_CAL where insp_cal_id=" + holidayId;
		logger.debug(sql);
		new Wrapper().update(sql);
	}

	/**
	 * Check Address
	 * 
	 * @param streetNumber
	 * @param streetId
	 * @param streetFraction
	 * @param unit
	 * @param lso_type
	 * @param applyOnlinePermitForm
	 * @return
	 * @throws Exception
	 */
	public List checkAddress(String streetNumber, int streetId, String streetFraction, String unit, String lso_type, ApplyOnlinePermitForm applyOnlinePermitForm) throws Exception {
		logger.info("checkAddress(" + streetNumber + " , " + streetId + ", " + streetFraction + ", " + unit + ")");

		Wrapper db = new Wrapper();

		String sql = "";
		int lsoId = 1;

		String lsoType = "";
		int addressId = 0;
		List lsoLists = new ArrayList();
		RowSet rs;
		sql = "SELECT * FROM V_ADDRESS_LIST val left outer join OCCUPANCY o on val.LSO_ID=o.OCCUPANCY_ID WHERE val.STR_NO=" + StringUtils.s2i(StringUtils.checkString(streetNumber)) + " and val.STREET_ID=" + streetId + " and ";

		if (!(streetFraction.equals("null"))) {
			sql += ("( val.STR_MOD=" + StringUtils.nullReplaceWithEmpty(streetFraction) + ") and ");
		}

		if (unit.equalsIgnoreCase("")) {
			sql += "(val.UNIT IS NULL OR val.UNIT='') and ";
		} else if (unit.equalsIgnoreCase("null")) {
			sql += "((val.UNIT IS NULL) OR val.UNIT='') and ";
		} else {
			sql += ("( val.UNIT='" + StringUtils.nullReplaceWithEmpty(unit) + "') and ");
		}
		if (unit.equalsIgnoreCase("")) {
			sql += "  val.LSO_TYPE in ('S') order by val.LSO_ID";
		} else {
			sql = sql + "  val.LSO_TYPE = 'O' and o.ACTIVE='Y' order by val.LSO_ID ";
			logger.debug(sql);
		}

		rs = new Wrapper().select(sql);

		while (rs.next()) {
			lsoId = rs.getInt("LSO_ID");
			applyOnlinePermitForm = new ApplyOnlinePermitForm();
			lsoType = rs.getString("LSO_TYPE");
			applyOnlinePermitForm.setLsoType(lsoType);
			applyOnlinePermitForm.setLsoId(lsoId);
			lsoLists.add(applyOnlinePermitForm);
			logger.debug("LSO Type " + lsoType);

			logger.debug("LSO Id Is " + lsoId);
			addressId = rs.getInt("ADDR_ID");
			logger.debug("Address Id is " + addressId);
		}

		return lsoLists;
	}

	/**
	 * Add temp Address
	 * 
	 * @param lsoId
	 * @param streetNumber
	 * @param streetMod
	 * @param streetId
	 * @param unit
	 * @param lso_type
	 * @return
	 * @throws Exception
	 */
	public int addTempAddress(int lsoId, String streetNumber, String streetMod, int streetId, String unit, String lso_type, int userId,String stepUrl) throws Exception {
		Wrapper db = new Wrapper();

		int tempOnlineID = db.getNextId("TEMP_ONLINEID");

		try {
			String sql = "";

			sql = "insert into TEMP_ONLINE_LSO_ADDRESS (LSO_ID,TEMP_OnlineId,LSO_TYPE,STR_NO,STR_MOD,STREET_ID, UNIT,CREATED_BY,UPDATED_BY,STEP_URL) values(" + lsoId + "," + tempOnlineID + "," + StringUtils.checkString(lso_type) + "," + streetNumber + ",'" + streetMod + "'," + streetId + ",'" + unit + "'," + userId + "," + userId +  ","  + StringUtils.checkString(stepUrl) +")";

			logger.debug(" Temporary Insert " + sql);

			db.insert(sql);
		} catch (Exception e) {
			logger.error("Exception thrown while trying to add online Permit  :" + e.getMessage());
		}

		return tempOnlineID;
	}

	/**
	 * Check Level
	 * 
	 * @param streetNumber
	 * @param streetId
	 * @param streetFraction
	 * @param unit
	 * @param lso_type
	 * @param applyOnlinePermitForm
	 * @return
	 * @throws Exception
	 */
	public String checkLevel(String streetNumber, int streetId, String streetFraction, String unit, String lso_type, ApplyOnlinePermitForm applyOnlinePermitForm) throws Exception {
		logger.info("checkLevel(" + streetNumber + " , " + streetId + ", " + streetFraction + ", " + unit + ")");

		String sql = "";
		int lsoId = 1;
		String lsoType = "";
		int addressId = 0;
		String addressLevel = "";
		RowSet rs;
		sql = "SELECT * FROM V_ADDRESS_LIST WHERE ";

		sql += ("( STR_NO=" + StringUtils.s2i(StringUtils.checkString(streetNumber)) + ") and ");

		sql += ("( STREET_ID=" + streetId + ") and ");

		logger.debug("before streetFraction " + streetFraction);

		if (!(streetFraction.equals("null"))) {
			logger.debug("after streetFraction " + streetFraction);
			sql += ("( STR_MOD=" + StringUtils.nullReplaceWithEmpty(streetFraction) + ") and ");
		}

		if (unit.equalsIgnoreCase("")) {
			sql += "(UNIT IS NULL OR UNIT='') and ";
			logger.debug("sql statement is in if" + sql);
		} else if (unit.equalsIgnoreCase("null")) {
			sql += "((UNIT IS NULL) OR UNIT='') and ";
			logger.debug("sql statement is in else if" + sql);
		} else {
			sql += ("( UNIT='" + StringUtils.nullReplaceWithEmpty(unit) + "') and ");
			logger.debug("sql statement is in else" + sql);
		}

		String sqlCommon = sql;
		sql += "  LSO_TYPE = 'L'";
		addressLevel = "L";
		logger.debug("sql statement is " + sql);

		rs = new Wrapper().select(sql);

		while (rs.next()) {
			lsoId = rs.getInt("LSO_ID");
			lsoType = rs.getString("LSO_TYPE");
			applyOnlinePermitForm.setLsoType(lsoType);

			logger.debug("LSO Type " + lsoType);

			logger.debug("LSO Id Is " + lsoId);
			addressId = rs.getInt("ADDR_ID");
			logger.debug("Address Id is " + addressId);
		}

		if (lsoId == 1) {
			sqlCommon = sqlCommon + "  LSO_TYPE = 'O'";
			addressLevel = "O";
			logger.debug("sqlCommon statement is " + sqlCommon);
			rs = new Wrapper().select(sqlCommon);

			while (rs.next()) {
				lsoId = rs.getInt("LSO_ID");
				lsoType = rs.getString("LSO_TYPE");
				applyOnlinePermitForm.setLsoType(lsoType);

				logger.debug("LSO Type " + lsoType);

				logger.debug("LSO Id Is " + lsoId);
				addressId = rs.getInt("ADDR_ID");
				logger.debug("Address Id is " + addressId);
			}
		}

		return addressLevel;
	}

	/**
	 * Update start date
	 * 
	 * @param tempOnlineId
	 * @param startDate
	 * @return
	 * @throws Exception
	 */
	public int updateStartDate(int tempOnlineId) throws Exception {
		Wrapper db = new Wrapper();
		logger.info("updateStartDate");

		try {
			String sql = "";

			sql = "UPDATE TEMP_ONLINE_LSO_ADDRESS SET START_DATE = sysdate WHERE TEMP_ONLINEID  = " + tempOnlineId + " ";
			logger.debug(" Update Query is " + sql);

			db.insert(sql);
		} catch (Exception e) {
			logger.error("Exception thrown while trying to add online Permit  :" + e.getMessage());
		}

		return tempOnlineId;
	}

	/**
	 * update subproject id
	 * 
	 * @param tempOnlineId
	 * @param lsoId
	 * @param landLsoId
	 * @return
	 * @throws Exception
	 */
	public int updateSprojId(int tempOnlineId, int landLsoId) throws Exception {
		Wrapper db = new Wrapper();

		try {
			String sql = "";

			sql = "UPDATE TEMP_ONLINE_LSO_ADDRESS	SET  LAND_LSO_ID=" + landLsoId + " WHERE TEMP_ONLINEID  = " + tempOnlineId + " ";
			logger.debug(" Update Query is " + sql);

			db.insert(sql);
		} catch (Exception e) {
			logger.error("Exception thrown while trying to add online Permit  :" + e.getMessage());
		}

		return tempOnlineId;
	}

	/**
	 * gets the list of Combo Use Map List - Added by Manjuprasad
	 * 
	 * @return
	 * @throws Exception
	 */

	/*
	 * public static List getComboUseMapList(int tempOnlineID, int lsoId, String sName) throws Exception { logger.debug("getComboUseMapList(" + tempOnlineID + ")");
	 * 
	 * List comboNameList = new ArrayList();
	 * 
	 * String sql; String sqlLandUse; int lsoUseId = 0; String sqlComboNames;
	 * 
	 * try { sqlLandUse = "select distinct lu.lso_use_id from lso_use lu , v_lso_owner vlo where vlo.land_use = lu.description and vlo.lso_id=" + lsoId; logger.debug(sqlLandUse);
	 * 
	 * RowSet rsLandUse = new Wrapper().select(sqlLandUse); String useId = ""; while (rsLandUse.next()) { lsoUseId = rsLandUse.getInt("lso_use_id"); useId = useId + lsoUseId + ","; } useId = useId.substring(0, (useId.length() - 1));
	 * 
	 * String sName1 = "'door replacement','drywall','electrical branch circuit','electrical fixtures','electrical outlets','gas outlet','hvac changeout','hvac duct replacement/ relocation','insulation','plumbing fixtures','repipe','restucco','roofing','siding','wet sandblast','window replacement','seismic bolting'"; String sName2 = "'door replacement','drywall','electrical branch circuit','electrical fixtures','electrical outlets','gas outlet','hvac changeout','hvac duct replacement/ relocation','insulation','plumbing fixtures','repipe','restucco','roofing','siding','wet sandblast','window replacement','seismic bolting'"; String sName3 = "'dishwasher','door replacement','drywall','electrical branch circuit','electrical fixtures','electrical outlets','gas outlet','hvac changeout','hvac duct replacement/ relocation','insulation','plumbing fixtures','repipe','restucco','roofing','sewer repair','siding','water heater','water service','wet sandblast','window replacement','bathroom remodel','kitchen remodel','laundry remodel','seismic bolting','lawn sprinkler backflow'"; String sName4 = "'dishwasher','drywall','electrical branch circuit','electrical fixtures','electrical outlets','gas outlet','hvac changeout','hvac duct replacement/ relocation','plumbing fixtures','repipe','roofing','water heater','bathroom remodel','kitchen remodel','laundry remodel'";
	 * 
	 * logger.debug(StringUtils.i2s(lsoUseId)); sqlComboNames = "select distinct(lsam.sproj_name),lcum.use_mapid  from lkup_sproj_act_mapp lsam left outer join lkup_combo_use_mapp lcum on " + "(lcum.sproj_name = lsam.sproj_name)  left outer join LSO_USE lu on (lu.lso_use_id = lcum.lso_USEID  ) where " + " lsam.isActive='Y' and lsam.ISONLINEREQ='Y'";
	 * 
	 * if (sName != null && sName.equalsIgnoreCase("Accessory Structure")) { sqlComboNames = sqlComboNames + " and lower(lsam.SPROJ_NAME) in (" + sName1 + ")"; } if (sName != null && sName.equalsIgnoreCase("Garage")) { sqlComboNames = sqlComboNames + " and lower(lsam.SPROJ_NAME) in (" + sName2 + ")"; } if (sName != null && sName.equalsIgnoreCase("Main Residence")) { sqlComboNames = sqlComboNames + " and lower(lsam.SPROJ_NAME) in (" + sName3 + ")"; } if (sName != null && sName.equalsIgnoreCase("Unit")) { sqlComboNames = sqlComboNames + " and lower(lsam.SPROJ_NAME) in (" + sName4 + ")"; }
	 * 
	 * sqlComboNames = sqlComboNames + " order by lsam.SPROJ_NAME"; logger.debug("sqlComboNames  is " + sqlComboNames);
	 * 
	 * RowSet rsComboNames = new Wrapper().select(sqlComboNames);
	 * 
	 * ApplyOnlinePermitForm applyOnlinePermitForm = new ApplyOnlinePermitForm();
	 * 
	 * while (rsComboNames.next()) { String sproj_name = StringUtils.properCase(rsComboNames.getString("sproj_name")); int use_mapid = rsComboNames.getInt("use_mapid");
	 * 
	 * applyOnlinePermitForm = new ApplyOnlinePermitForm(use_mapid, sproj_name); comboNameList.add(applyOnlinePermitForm); }
	 * 
	 * return comboNameList; } catch (Exception e) { logger.error(e.getMessage()); throw e; } }
	 */

	/**
	 * gets the project list on LSO
	 * 
	 * @param tempOnlineID
	 * @return
	 * @throws Exception
	 */
	public static List getProjectListOnLSO(int tempOnlineID) throws Exception {
		logger.debug("getProjectListOnLSO(" + tempOnlineID + ")");

		ApplyOnlinePermitForm applyOnlinePermitForm = new ApplyOnlinePermitForm();
		List projectDetails = new ArrayList();
		int lsoId = 0;
		String sql;
		String sqlLandUse;
		String projectName;
		String projectNumber;

		String sqlComboNames;

		try {
			sql = "SELECT LSO_ID FROM TEMP_ONLINE_LSO_ADDRESS where TEMP_ONLINEID =" + tempOnlineID;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				lsoId = rs.getInt("LSO_ID");
			}

			// sqlLandUse = "select lpt.DESCRIPTION as PROJECT,a.ACT_NBR as PERMIT,a.DESCRIPTION as ACT_DESC  from activity a left outer join sub_project sp on a.sproj_id = sp.sproj_id left outer join lkup_ptype lpt on sp.sproj_type=lpt.ptype_id left outer join Project P on sp.PROJ_ID=P.PROJ_ID where P.lso_id= " + lsoId + " and P.NAME = " + StringUtils.checkString(Constants.PROJECT_NAME_BUILDING);

			sqlLandUse = "select sp.sproj_id,lpt.DESCRIPTION as PROJECT from sub_project sp  left outer join Project P on sp.PROJ_ID=P.PROJ_ID  left outer join lkup_ptype lpt on sp.sproj_type=lpt.ptype_id where P.lso_id= " + lsoId + " and P.NAME = " + StringUtils.checkString(Constants.PROJECT_NAME_BUILDING);
			logger.debug(sqlLandUse);

			RowSet rsLandUse = new Wrapper().select(sqlLandUse);

			while (rsLandUse.next()) {
				projectName = rsLandUse.getString("PROJECT");
				projectNumber = StringUtils.i2s(rsLandUse.getInt("SPROJ_ID"));

				applyOnlinePermitForm = new ApplyOnlinePermitForm(projectName, projectNumber, tempOnlineID);
				// applyOnlinePermitForm.setDescription(rsLandUse.getString("ACT_DESC") != null ? rsLandUse.getString("ACT_DESC") : "");
				projectDetails.add(applyOnlinePermitForm);
			}

			rs.close();

			return projectDetails;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets online project details
	 * 
	 * @param tempOnlineID
	 * @param projectNumber
	 * @param createNewFlag
	 * @return
	 * @throws Exception
	 */
	public static List getOnlineProjectDetails(int tempOnlineID, String projectNumber, String createNewFlag) throws Exception {
		logger.debug("getOnlineProjectDetails(" + tempOnlineID + ")");

		ApplyOnlinePermitForm applyOnlinePermitForm = new ApplyOnlinePermitForm();
		List newProjectDetails = new ArrayList();
		int lsoId = 0;
		String valuation;
		String sql;
		String sqlLandUse;
		String projectName = "";
		String comboName;
		String sqlProjectName;
		String forcePC;
		String comboNames = "";
		String sqlComboNames;
		String comboSql = "";

		try {
			logger.debug("projectNumber :" + projectNumber);

			comboSql = "SELECT LCUM.lso_useid,LCUM.SPROJ_NAME,LCUM.DEPT_CODE from LKUP_COMBO_USE_MAPP LCUM left outer join temp_online_lso_address TOLA on LCUM.USE_MAPID= TOLA.USE_MAP_ID where temp_onlineid=" + tempOnlineID;
			logger.debug("Query to get Combo Name from database is " + comboSql);
			RowSet rsComboName = new Wrapper().select(comboSql);

			while (rsComboName.next()) {
				comboNames = rsComboName.getString("SPROJ_NAME");

			}

			if (createNewFlag.equalsIgnoreCase("EXIST")) {
				// sqlProjectName = "SELECT * FROM project where project_nbr =" + StringUtils.checkString(projectNumber);
				sqlProjectName = "SELECT p.name,p.description from sub_project sp left outer join Project p on sp.proj_id=p.proj_id  where sp.sproj_id =" + projectNumber;
				logger.debug(sqlProjectName);

				RowSet rsProjectName = new Wrapper().select(sqlProjectName);

				while (rsProjectName.next()) {
					projectName = rsProjectName.getString("name");
				}
			} else {
				Date dt = new Date();
				projectName = comboNames + " - " + StringUtils.date2str(dt);

			}

			sql = "SELECT *  FROM TEMP_ONLINE_LSO_ADDRESS tola " + " left outer join LKUP_COMBO_USE_MAPP LCUM on LCUM.USE_MAPID= tola.USE_MAP_ID" + " left outer join LKUP_SPROJ_ACT_MAPP LSAM on (LSAM.SPROJ_NAME= LCUM.SPROJ_NAME and LSAM.DEPT_CODE= LCUM.DEPT_CODE)" + " where TEMP_ONLINEID =" + tempOnlineID;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				lsoId = rs.getInt("LSO_ID");
				valuation = rs.getString("valuation");

				comboName = rs.getString("sproj_name");
				forcePC = rs.getString("forcePC");
				applyOnlinePermitForm = new ApplyOnlinePermitForm(lsoId, valuation, projectName, comboName, forcePC);
				newProjectDetails.add(applyOnlinePermitForm);

				break;
			}

			rs.close();

			return newProjectDetails;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}
	
	
	/**
	 * 
	 * 
	 * @param tempAddress
	 * @param valuation
	 * @return
	 * @throws Exception
	 */
	public int updateValuation(int tempAddress, String valuation,String squareFootage, String numberOfDwellingUnits, String numberOfFloors, String stepUrl) throws Exception {
		Wrapper db = new Wrapper();

		try {
			String sql = "";

			sql = "UPDATE TEMP_ONLINE_LSO_ADDRESS	SET VALUATION = " + valuation + ",SQ_FOOTAGE = " + StringUtils.checkString(squareFootage) + ",NO_DWELL_UNITS = " + StringUtils.checkString(numberOfDwellingUnits) + ",NO_FLOORS = " + StringUtils.checkString(numberOfFloors) + ",STEP_URL = " + StringUtils.checkString(stepUrl) + "  WHERE TEMP_ONLINEID  = " + tempAddress + " ";
			logger.debug(" Update Query is " + sql);

			db.insert(sql);
		} catch (Exception e) {
			logger.error("Exception thrown while trying to add online Permit  :" + e.getMessage());
		}

		return tempAddress;
	}
	
	
	/**
	 * 
	 * 
	 * @param tempAddress
	 * @param valuation
	 * @return
	 * @throws Exception
	 */
	public int updateStepUrl(int tempAddress, String stepUrl) throws Exception {
		Wrapper db = new Wrapper();

		try {
			String sql = "";

			sql = "UPDATE TEMP_ONLINE_LSO_ADDRESS	SET STEP_URL = " + StringUtils.checkString(stepUrl) + "  WHERE TEMP_ONLINEID  = " + tempAddress + " ";
			logger.debug(" Update Query is " + sql);

			db.insert(sql);
		} catch (Exception e) {
			logger.error("Exception thrown while trying to add online Permit  :" + e.getMessage());
		}

		return tempAddress;
	}
	

	/**
	 * Update valuation
	 * 
	 * @param tempAddress
	 * @param valuation
	 * @return
	 * @throws Exception
	 */
	public int updateValuation(int tempAddress, String valuation) throws Exception {
		Wrapper db = new Wrapper();

		try {
			String sql = "";

			sql = "UPDATE TEMP_ONLINE_LSO_ADDRESS	SET VALUATION = " + valuation + " WHERE TEMP_ONLINEID  = " + tempAddress + " ";
			logger.debug(" Update Query is " + sql);

			db.insert(sql);
		} catch (Exception e) {
			logger.error("Exception thrown while trying to add online Permit  :" + e.getMessage());
		}

		return tempAddress;
	}

	/**
	 * Update Square Footage
	 * 
	 * @param tempAddress
	 * @param squareFootage
	 * @return
	 * @throws Exception
	 */
	public int updateSquareFootage(int tempAddress, String squareFootage) throws Exception {
		Wrapper db = new Wrapper();

		try {
			String sql = "";

			sql = "UPDATE TEMP_ONLINE_LSO_ADDRESS	SET SQ_FOOTAGE = " + StringUtils.checkString(squareFootage) + " WHERE TEMP_ONLINEID  = " + tempAddress + " ";
			logger.debug(" Update Query is " + sql);

			db.insert(sql);
		} catch (Exception e) {
			logger.error("Exception thrown while trying to updateSquareFootage  :" + e.getMessage());
		}

		return tempAddress;
	}

	/**
	 * Update number of dwelling units
	 * 
	 * @param tempAddress
	 * @param numberOfDwellingUnits
	 * @return
	 * @throws Exception
	 */
	public int updateNumberOfDwellingUnits(int tempAddress, String numberOfDwellingUnits) throws Exception {
		Wrapper db = new Wrapper();

		try {
			String sql = "";

			sql = "UPDATE TEMP_ONLINE_LSO_ADDRESS	SET NO_DWELL_UNITS = " + StringUtils.checkString(numberOfDwellingUnits) + " WHERE TEMP_ONLINEID  = " + tempAddress + " ";
			logger.debug(" Update Query is " + sql);

			db.insert(sql);
		} catch (Exception e) {
			logger.error("Exception thrown while trying to updateNumberOfDwellingUnits  :" + e.getMessage());
		}

		return tempAddress;
	}

	/**
	 * update number of floors
	 * 
	 * @param tempAddress
	 * @param numberOfFloors
	 * @return
	 * @throws Exception
	 */
	public int updateNumberOfFloors(int tempAddress, String numberOfFloors) throws Exception {
		Wrapper db = new Wrapper();

		try {
			String sql = "";

			sql = "UPDATE TEMP_ONLINE_LSO_ADDRESS SET NO_FLOORS = " + StringUtils.checkString(numberOfFloors) + " WHERE TEMP_ONLINEID  = " + tempAddress + " ";
			logger.debug(" Update Query is " + sql);

			db.insert(sql);
		} catch (Exception e) {
			logger.error("Exception thrown while trying to updateNumberOfFloors  :" + e.getMessage());
		}

		return tempAddress;
	}

	/**
	 * Set project number
	 * 
	 * @param tempOnlineID
	 * @param projectNumber
	 * @param createNewFlag
	 * @throws Exception
	 */
	public void setProjectNumber(int tempOnlineID, String projectNumber, String createNewFlag) throws Exception {
		Wrapper db = new Wrapper();

		try {
			String projectName = "";
			String projectDesc = "";
			String comboName = "";
			String comboSql = "";

			if ((!(projectNumber.equals(""))) && (projectNumber != null)) {
				String sqlProjectName = "SELECT p.name,p.description from sub_project sp left outer join Project p on sp.proj_id=p.proj_id  where sp.sproj_id =" + projectNumber;

				logger.debug(sqlProjectName);

				RowSet rsPname = new Wrapper().select(sqlProjectName);

				while (rsPname.next()) {
					projectName = rsPname.getString("name");
					projectDesc = rsPname.getString("description");
				}
			}

			logger.debug("projectName " + projectName);

			comboSql = "SELECT LCUM.lso_useid,LCUM.SPROJ_NAME,LCUM.DEPT_CODE from LKUP_COMBO_USE_MAPP LCUM left outer join temp_online_lso_address TOLA on LCUM.USE_MAPID= TOLA.USE_MAP_ID where temp_onlineid=" + tempOnlineID;
			logger.debug("Query to get Combo Name from database is " + comboSql);
			RowSet rsComboName = new Wrapper().select(comboSql);

			while (rsComboName.next()) {
				comboName = rsComboName.getString("SPROJ_NAME");

			}

			// if (createNewFlag.equalsIgnoreCase("NEW")) {
			Date dt = new Date();
			projectName = comboName + " - " + StringUtils.date2str(dt);
			projectDesc = comboName + " - " + StringUtils.date2str(dt);
			// }

			String sql = "";

			sql = "UPDATE TEMP_ONLINE_LSO_ADDRESS	SET project_nbr = " + StringUtils.checkString(projectNumber) + " , project_name= " + StringUtils.checkString(projectName) + " , project_desc= " + StringUtils.checkString(projectDesc) + " WHERE TEMP_ONLINEID  = " + tempOnlineID + " ";
			logger.debug(" Update Query is " + sql);

			db.insert(sql);
		} catch (Exception e) {
			logger.error("Exception thrown while trying to add online Permit  :" + e.getMessage());
		}
	}

	/**
	 * set combo name
	 * 
	 * @param tempOnlineID
	 * @param useMapId
	 * @throws Exception
	 */
	public void setComboName(int tempOnlineID, int useMapId, int feeUnitNo) throws Exception {
		Wrapper db = new Wrapper();

		try {
			String sql = "";

			sql = "UPDATE TEMP_ONLINE_LSO_ADDRESS	SET use_map_id = " + useMapId + ",Fee_Unit_No = " + feeUnitNo + " WHERE TEMP_ONLINEID  = " + tempOnlineID + " ";
			logger.debug(" Update Query is " + sql);

			db.insert(sql);

			logger.debug(sql);
		} catch (Exception e) {
			logger.error("Exception thrown while trying to add online Permit  :" + e.getMessage());
		}
	}

	/**
	 * 
	 * @param tempOnlineId
	 * @return
	 */
	public static String getActivitySubTypeId(int tempOnlineId) {
		logger.info("getActivitySubTypeId(" + tempOnlineId + ")");

		String activitySubTypeId = null;
		String sql1 = "select USE_MAP_ID from temp_online_lso_address where TEMP_ONLINEID =" + tempOnlineId;
		logger.debug("getActivitySubTypeId sql  "+sql1);
		try {
			RowSet rs1 = new Wrapper().select(sql1);

			while (rs1 != null && rs1.next()) {
				activitySubTypeId = rs1.getString("USE_MAP_ID");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return activitySubTypeId;
	}

	/**
	 * 
	 * @param tempOnlineId
	 * @return
	 */
	public static String getProjectName(int tempOnlineId) {
		String actType = null;
		String sql1 = "select PROJECT_NAME from temp_online_lso_address where TEMP_ONLINEID =" + tempOnlineId;
		try {
			RowSet rs1 = new Wrapper().select(sql1);

			while (rs1 != null && rs1.next()) {
				if (rs1.getString("PROJECT_NAME") != null) {
					String[] actType1 = rs1.getString("PROJECT_NAME").split("-");
					actType = actType1[0].trim();
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return actType;
	}

	/**
	 * 
	 * @param tempOnlineId
	 * @return
	 */
	public static String getActivityType(int tempOnlineId) {
		logger.info("getActivityType("+ tempOnlineId+")");
		
		String actType = "";
		String sql1 = "select ACT_TYPE from temp_online_lso_address where TEMP_ONLINEID =" + tempOnlineId;
		logger.debug("getActivityType sql "+sql1);
		try {
			RowSet rs1 = new Wrapper().select(sql1);

			while (rs1 != null && rs1.next()) {
				if (rs1.getString("ACT_TYPE") != null) {
					actType = rs1.getString("ACT_TYPE");
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return actType;
	}
	/**
	 * get questions
	 * 
	 * @param tempOnlineId
	 * @return
	 * @throws Exception
	 */
	public static List getQuestions(int tempOnlineId) throws Exception {
		logger.info("getQuestions(" + tempOnlineId + ")");

		List questionList = new ArrayList();
		String tempQuestionID = "";
		String sql;
		String description = "";
		String acknowledgements = "";

		try {
			String actType = getActivityType(tempOnlineId);
			String activitySubTypeId = getActivitySubTypeId(tempOnlineId);

			sql = "SELECT distinct lq.QUESTION_ID as question_id,q.ques_desc as question from LKUP_QUESTIONAIRE lq join lkup_questions q on q.ques_id=lq.QUESTION_ID where " +
					"lower(lq.act_type) =lower('" + actType + "') and " +
							"lq.act_subtype_id = " + activitySubTypeId ;
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs != null && rs.next()) {
				tempQuestionID = rs.getString("QUESTION_ID");
				description = rs.getString("QUESTION");

				ApplyOnlinePermitForm applyOnlinePermitForm = new ApplyOnlinePermitForm();

				applyOnlinePermitForm.setTempQuestionId(tempQuestionID);
				applyOnlinePermitForm.setQuestionaireDescription(description);
				questionList.add(applyOnlinePermitForm);

				logger.debug("questionList.size();" + questionList.size());
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}

		return questionList;
	}
	
	/**
	 * get ACKNOWLEDGMENTS
	 * 
	 * @param tempOnlineId
	 * @return
	 * @throws Exception
	 */
	public static List getAcknowledgments(int tempOnlineId) throws Exception {
		logger.info("getAcknowledgments(" + tempOnlineId + ")");

		List acknowledgementList = new ArrayList();
		String tempAckID = "";
		String sql;
		String description = "";
		String acknowledgements = "";

		try {
			String actType = getActivityType(tempOnlineId);
			String activitySubTypeId = getActivitySubTypeId(tempOnlineId);

			sql = "SELECT distinct lq.ack_id as ACKNOWLEDGMENT_ID,q.ack_desc as ACKNOWLEDGMENT from LKUP_ACKNOWLEDGMENT lq join lkup_ACKNOWLEDGMENTS q on q.ACK_ID=lq.ACK_ID where lower(lq.act_type) =lower('" + actType + "') and lq.act_subtype_id = " + activitySubTypeId ;
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs != null && rs.next()) {
				tempAckID = rs.getString("ACKNOWLEDGMENT_ID");
				description = rs.getString("ACKNOWLEDGMENT");

				ApplyOnlinePermitForm applyOnlinePermitForm = new ApplyOnlinePermitForm();

				applyOnlinePermitForm.setTempAcknowledgementId(tempAckID);
				applyOnlinePermitForm.setAcknowledgementDescription(description);
				acknowledgementList.add(applyOnlinePermitForm);

				logger.debug("acknowledgementList();" + acknowledgementList.size());
			}
			

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}

		return acknowledgementList;
	}

	


	/**
	 * get all details
	 * 
	 * @param tempOnlineID
	 * @param forcePC
	 * @param email_addr
	 * @return
	 * @throws Exception
	 */
	public ApplyOnlinePermitForm getAllDetails(int tempOnlineID, String forcePC, String email_addr) throws Exception {
		logger.debug("Entered getAllDetails List");

		logger.debug("Force PC " + forcePC);

		String userId = "0";
		String sql;
		String lsoId = "";
		String lsoType;
		String valuation = "";
		String projno = "";
		int comboUseMapId = 0;
		ApplyOnlinePermitForm applyOnlinePermitForm = new ApplyOnlinePermitForm();
		List peopleIdList = new ArrayList();
		
		// project parameters
		String projectName = "";
		String projectDesc = "";
		String status = Constants.PROJECTDEFAULTSTATUS;
		String projectUseId = "";

		// sub project parameters
		String sproj = "";
		String sprojTypeId = "0";
		String subProjectSubTypeId = "0";
		String sprojStatus = Constants.SUBPROJECTDEFAULTSTATUS;
		String sprojDesc = "Online SubProject";
		String deptCode = "";
		String projTypeId = "1";
		
		// Refers construction activity from lkup_ptype table.\
		int projectId = 0;
		Calendar startDt = Calendar.getInstance();

		// activity parameters
		String actStatus = Constants.ACTIVITYDEFAULTSTATUS;
		String actType="";
		String actDescription="";
		int subtypeId = 0;
		
		try {
			sql = "SELECT TOLA.*,D.*,LAT.DESCRIPTION AS ACTIVITY_TYPE_DESCRIPTION,LAT.TYPE FROM TEMP_ONLINE_LSO_ADDRESS  TOLA LEFT OUTER JOIN LKUP_ACT_SUBTYPE LAS ON TOLA.USE_MAP_ID=LAS.ACT_SUBTYPE_ID LEFT OUTER JOIN  LKUP_ACT_TYPE LAT ON LAS.ACT_TYPE=LAT.TYPE  LEFT OUTER JOIN  DEPARTMENT D ON LAT.DEPT_ID=D.DEPT_ID WHERE TEMP_ONLINEID = " + tempOnlineID;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				lsoId = StringUtils.i2s(rs.getInt("LSO_ID"));
				lsoType = rs.getString("LSO_TYPE");
				projectUseId = rs.getString("LSO_ID");
				deptCode = rs.getString("DEPT_CODE");
				startDt = StringUtils.dbDate2cal(rs.getString("START_DATE"));
				projectName = "Building";
				projectDesc = "Building";
				valuation = rs.getString("VALUATION");
				projno = StringUtils.nullReplaceWithEmpty(rs.getString("PROJECT_NBR"));
				comboUseMapId = rs.getInt("USE_MAP_ID");
				actType = rs.getString("TYPE");
				actDescription = rs.getString("ACTIVITY_TYPE_DESCRIPTION");
				subtypeId = rs.getInt("USE_MAP_ID");
				if ((rs.getInt("C_PEOPLE_ID")) > 0) {
					peopleIdList.add("" + rs.getInt("C_PEOPLE_ID"));
				}

				if ((rs.getInt("E_PEOPLE_ID")) > 0) {
					peopleIdList.add("" + rs.getInt("E_PEOPLE_ID"));
				}

				if ((rs.getInt("R_PEOPLE_ID")) > 0) {
					peopleIdList.add("" + rs.getInt("R_PEOPLE_ID"));
				}

				if ((rs.getInt("W_PEOPLE_ID")) > 0) {
					peopleIdList.add("" + rs.getInt("W_PEOPLE_ID"));
				}

				if ((rs.getInt("A_PEOPLE_ID")) > 0) {
					peopleIdList.add("" + rs.getInt("A_PEOPLE_ID"));
				}
				
				applyOnlinePermitForm.setLsoId(StringUtils.s2i(lsoId));
				applyOnlinePermitForm.setLsoType(lsoType);
			}


			projectId = createCombopermitProject(lsoId, projectName, projectDesc, status, userId, projectUseId);

			// for subproject
			sprojTypeId = new OnlineAgent().getComboPermitSprojTypeId(sproj);
			subProjectSubTypeId = new OnlineAgent().getComboPermitSubProjectSubTypeId(sproj);
			logger.debug("PROJECT ID RETRIEVED =" + projectId);

			int subProjectId = 0;
			if ((!(projno.equals(""))) && (projno != null)) {
				subProjectId = StringUtils.s2i(projno);
			} else {
				subProjectId = new OnlineAgent().createComboPermitSubProject(sproj, projectId + "", projTypeId, sprojTypeId, subProjectSubTypeId, sprojStatus, sprojDesc, userId, valuation, sproj, deptCode);
			}
			

			String sql5 = "UPDATE TEMP_ONLINE_LSO_ADDRESS	SET SPROJ_ID = " + subProjectId + " WHERE TEMP_ONLINEID  = " + tempOnlineID + " ";
			logger.debug(" Update Query is " + sql);
			new Wrapper().update(sql5);
			logger.debug("sql " + sql5);

			// for activities
			String addrId = getAddressidforLsoId(lsoId);

			int i = 0;
			String actListArray[] = { "" };
			String pcListArray[] = { "" };
			logger.debug("**************" + forcePC);
			logger.debug("**************" + deptCode);
			activityId = new OnlineAgent().addActivities(deptCode, actListArray, subProjectId + "", addrId, actType, actDescription, valuation, actStatus, userId, forcePC, startDt);

			int j = 0;

			String[] selectedPeople = new String[peopleIdList.size()];

			int k = 0;

			while (peopleIdList != null && peopleIdList.size() > k) {
				selectedPeople[k] = StringUtils.nullReplaceWithEmpty((String) peopleIdList.get(k));

				logger.debug("People ID-----------------" + selectedPeople[k]);
				k++;

			}

			PeopleAgent peopleAgent = new PeopleAgent();
			String peopleLevel = "A";
			String psaId = "" + activityId;

			peopleAgent.assignPeople(selectedPeople, psaId, "A");

			applyOnlinePermitForm.setSubProjectId(activityId);
	/*//		sql = "select ACT_SUBTYPE_ID from LKUP_ACT_SUBTYPE LAS left outer join LKUP_COMBO_USE_MAPP LCUM on lower(LAS.ACT_SUBTYPE) = lower(LCUM.SPROJ_NAME) where LCUM.USE_MAPID = " + comboUseMapId;
			sql = "select ACT_SUBTYPE_ID from LKUP_ACT_SUBTYPE LAS left outer join LKUP_COMBO_USE_MAPP LCUM on lower(LAS.ACT_SUBTYPE) = lower(LCUM.SPROJ_NAME) where LCUM.USE_MAPID = " + comboUseMapId;
			RowSet rs9 = new Wrapper().select(sql);
			
			while (rs9.next()) {
				subtypeId = rs9.getInt("ACT_SUBTYPE_ID");
				applyOnlinePermitForm.setStypeId(subtypeId);
			}
			
			sql = "update act_subtype set ACT_SUBTYPE_ID =  " + subtypeId + " where act_id in (" + activityId + ")";
			logger.debug(sql);
			new Wrapper().update(sql);

*/
			if(subtypeId>0){
				sql = "insert into act_subtype(ACT_SUBTYPE_ID,act_id) VALUES("+ subtypeId +","+ psaId +")";
				logger.debug(sql);
				new Wrapper().insert(sql);
			}
			
			// for saving Combo Permit Number
			String sqlComboNo = "";
			String comboNo = "";
			sqlComboNo = " select act_nbr  from activity  where act_id=" + activityId;
			logger.debug(" Select COMBo_NBR" + sqlComboNo);

			ResultSet rsComboNo = new Wrapper().select(sqlComboNo);

			while (rsComboNo.next()) {
				comboNo = rsComboNo.getString("act_nbr");
			}
			applyOnlinePermitForm.setComboNumber(comboNo);
			String sqlComboNo1 = "UPDATE TEMP_ONLINE_LSO_ADDRESS	SET COMBO_NBR = " + StringUtils.checkString(comboNo) + " WHERE TEMP_ONLINEID  = " + tempOnlineID + " ";
			logger.debug(" Update Query is " + sqlComboNo1);
			new Wrapper().update(sqlComboNo1);
			

			// Combo Number store

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}

		return applyOnlinePermitForm;
	}

	/**
	 * Create combo permit
	 * 
	 * @param lsoId
	 * @param projectName
	 * @param projectDescription
	 * @param status
	 * @param userId
	 * @param projectUseId
	 * @return
	 * @throws Exception
	 */
	public int createCombopermitProject(String lsoId, String projectName, String projectDescription, String status, String userId, String projectUseId) throws Exception {
		logger.debug("Entered createCombopermitProject");

		int projectId = -1;
		Wrapper db = new Wrapper();

		try {
			elms.app.project.Project p = new elms.app.project.Project();

			projectId = new ActivityAgent().addBSDeptProject(lsoId, projectName, projectDescription, status, userId, projectUseId);
		} catch (Exception e) {
			throw e;
		}

		// Get All the Paramaters. insert in to project , subproject and
		// activites.
		return projectId;
	}

	/**
	 * for inserting into Subproject table all 3 based on sprojtypeID
	 * 
	 * @param sprj_name
	 * @return
	 * @throws Exception
	 */
	public String getComboPermitSprojTypeId(String sprj_name) throws Exception {
		logger.info("Entered getComboPermitSprojTypeId().." + sprj_name);

		Wrapper db = new Wrapper();
		RowSet rs = null;
		int sprojTypeId = 0;

		try {
			String sql = "select STYPE_ID from LKUP_SPROJ_ACT_MAPP where SPROJ_NAME = " + StringUtils.checkString(sprj_name);
			logger.info(sql);
			rs = new Wrapper().select(sql);

			while ((rs != null) && rs.next()) {
				sprojTypeId = rs.getInt("STYPE_ID");
			}
		} catch (Exception e) {
			logger.error("Exception occured in getComboPermitSprojTypeId method . Message-" + e.getMessage());
			throw e;
		}

		return sprojTypeId + "";
	}

	/**
	 * inserting into subproject table based on sprojname based on sprojsubtypeID
	 * 
	 * @param sprj_name
	 * @return
	 * @throws Exception
	 */
	public String getComboPermitSubProjectSubTypeId(String sprj_name) throws Exception {
		logger.info("Entered getComboPermitSubProjectSubTypeId().." + sprj_name);

		Wrapper db = new Wrapper();
		RowSet rs = null;
		int subProjectSubTypeId = 0;

		try {
			String sql = "select SSTYPE_ID from LKUP_SPROJ_ACT_MAPP where SPROJ_NAME = " + StringUtils.checkString(sprj_name);
			logger.info(sql);
			rs = new Wrapper().select(sql);

			while ((rs != null) && rs.next()) {
				subProjectSubTypeId = rs.getInt("SSTYPE_ID");
			}
		} catch (Exception e) {
			logger.error("Exception occured in getComboPermitSubProjectSubTypeId method . Message-" + e.getMessage());
			throw e;
		}

		return subProjectSubTypeId + "";
	}

	/**
	 * Create combo permit sub project
	 * 
	 * @param comboName
	 * @param projectId
	 * @param projTypeId
	 * @param sprojTypeId
	 * @param subProjectSubTypeId
	 * @param sprojStatus
	 * @param description
	 * @param userId
	 * @param valuation
	 * @param sproj
	 * @param deptCode
	 * @return
	 * @throws Exception
	 */
	public int createComboPermitSubProject(String comboName, String projectId, String projTypeId, String sprojTypeId, String subProjectSubTypeId, String sprojStatus, String description, String userId, String valuation, String sproj, String deptCode) throws Exception {
		int subProjectId = -1;
		Wrapper db = new Wrapper();

		try {
			subProjectId = new ActivityAgent().addSubProject(comboName, projectId, projTypeId, sprojTypeId, subProjectSubTypeId, sprojStatus, description, userId, valuation, deptCode);
		} catch (Exception e) {
			throw e;
		}

		return subProjectId;
	}

	/**
	 * add combo activities
	 * 
	 * @param deptCode
	 * @param actList
	 * @param sprojId
	 * @param addrId
	 * @param actDesc
	 * @param valuation
	 * @param status
	 * @param userId
	 * @param pcList
	 * @param startDt
	 * @return
	 * @throws Exception
	 */
	public int addActivities(String deptCode, String[] actList, String sprojId, String addrId, String actType,String actDesc, String valuation, String status, String userId, String pcReq, Calendar startDt) throws Exception {
		boolean success = false;
		logger.debug("Entered addaddComboActivities");

		Wrapper db = new Wrapper();
		int activityId = 0;
		try {
			activityId = new ActivityAgent().addActivities(deptCode, actList, sprojId, addrId,actType, actDesc, valuation, status, userId, pcReq, startDt);

		} catch (Exception e) {
			throw e;
		}

		return activityId;
	}
	/**
	 * Get search results
	 * 
	 * @param searchEntry
	 * @param searchBased
	 * @param peopleType
	 * @return
	 */
	public List getSearchResults(String searchEntry, String searchBased, String peopleType) {
		List peopleSearchList = new ArrayList();
		Wrapper db = new Wrapper();
		int peopleTypeId = 0;
		String sql = "";

		try {
			if (searchBased.equalsIgnoreCase("NAME")) {
				sql = "select * from People where  (Upper(NAME) like '%" + searchEntry.toUpperCase() + "%' OR  Upper(AGENT_NAME) like '%" + searchEntry.toUpperCase() + "%') and";
			} else if (searchBased.equalsIgnoreCase("FS")) {
				sql = "select * from People where  (Upper(NAME) like '%" + searchEntry.toUpperCase() + "%' OR Upper(AGENT_NAME) like '%" + searchEntry.toUpperCase() + "%' OR Upper(LIC_NO) like '" + searchEntry.toUpperCase() + "%' OR Upper(EMAIL_ADDR) like '%" + searchEntry.toUpperCase() + "%') and ";
			} else {
				sql = "select * from People where  Upper(" + searchBased + ") like '%" + searchEntry.toUpperCase() + "%' and ";
			}

			if (peopleType.equalsIgnoreCase("C")) {
				sql = sql + "   PEOPLE_TYPE_ID=4";
			} else if (peopleType.equalsIgnoreCase("E")) {
				sql = sql + "   people_type_id=7";
			} else if (peopleType.equalsIgnoreCase("R")) {
				sql = sql + "  people_type_id=3";
			} else if (peopleType.equalsIgnoreCase("W")) {
				sql = sql + "   people_type_id=8";
			}

			if (!searchBased.equalsIgnoreCase("FS")) {
				sql = sql + " order by " + searchBased + " asc";
			}

			logger.debug("sql " + sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				ApplyOnlinePermitForm applyOnlinePermitForm = new ApplyOnlinePermitForm();

				applyOnlinePermitForm.setPeopleId(rs.getInt("PEOPLE_ID"));
				logger.debug("sql " + applyOnlinePermitForm.getPeopleId());

				applyOnlinePermitForm.setName(StringUtils.nullReplaceWithEmpty(rs.getString("NAME")));
				logger.debug("sql " + applyOnlinePermitForm.getName());

				applyOnlinePermitForm.setPhoneNo(StringUtils.nullReplaceWithEmpty(StringUtils.phoneFormat(rs.getString("PHONE"))));
				logger.debug("sql " + applyOnlinePermitForm.getPhoneNo());

				if (rs.getString("LIC_NO") != null) {
					applyOnlinePermitForm.setLicNo(StringUtils.nullReplaceWithEmpty(rs.getString("LIC_NO")));
				} else {
					applyOnlinePermitForm.setLicNo("");
				}

				logger.debug("sql " + applyOnlinePermitForm.getLicNo());

				applyOnlinePermitForm.setAgentName(StringUtils.nullReplaceWithEmpty(rs.getString("AGENT_NAME")));

				logger.debug("sql " + applyOnlinePermitForm.getAgentName());

				applyOnlinePermitForm.setEmailAddr(StringUtils.nullReplaceWithEmpty(rs.getString("email_addr")));
				logger.debug("sql " + applyOnlinePermitForm.getEmailAddr());

				applyOnlinePermitForm.setAddress(StringUtils.nullReplaceWithEmpty(rs.getString("ADDR")));

				peopleTypeId = rs.getInt("PEOPLE_TYPE_ID");

				if (peopleTypeId == 1) {
					applyOnlinePermitForm.setPeopleType("Contractor");
				} else if (peopleTypeId == 2) {
					applyOnlinePermitForm.setPeopleType("Engineer");
				} else if (peopleTypeId == 4) {
					applyOnlinePermitForm.setPeopleType("Architect");
				} else if (peopleTypeId == 7) {
					applyOnlinePermitForm.setPeopleType("Owner");
				}

				String licExp = StringUtils.cal2str(StringUtils.dbDate2cal(rs.getString("LIC_EXP_DT")));
				String today = StringUtils.cal2str(Calendar.getInstance());
				licExp = licExp.replaceAll("/", "");
				today = today.replaceAll("/", "");
				int iToday = StringUtils.s2i(today);
				int iLic = StringUtils.s2i(licExp);

				peopleSearchList.add(applyOnlinePermitForm);
			}
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.getMessage(),e);
			
		}

		return peopleSearchList;
	}

	/**
	 * Store people id
	 * 
	 * @param tempOnlineId
	 * @param peopleId
	 * @param peopleType
	 * @return
	 * @throws Exception
	 */
	public int storePeopleId(int tempOnlineId, int peopleId, String peopleType) throws Exception {
		Wrapper db = new Wrapper();

		try {
			String sql = "";

			sql = "UPDATE TEMP_ONLINE_LSO_ADDRESS	SET ";

			if (peopleType.equalsIgnoreCase("C")) {
				sql = sql + " C_People_ID = " + peopleId;
			} else if (peopleType.equalsIgnoreCase("E")) {
				sql = sql + " E_People_ID = " + peopleId;
			} else if (peopleType.equalsIgnoreCase("R")) {
				sql = sql + " R_People_ID = " + peopleId;
			} else if (peopleType.equalsIgnoreCase("W")) {
				sql = sql + " W_People_ID = " + peopleId;
			}

			sql = sql + " WHERE TEMP_ONLINEID  = " + tempOnlineId;
			logger.debug(" Update Query is " + sql);

			db.insert(sql);
		} catch (Exception e) {
			logger.error("Exception thrown while trying to add online Permit  :" + e.getMessage());
		}

		return tempOnlineId;
	}

	/**
	 * Store business tax req
	 * 
	 * @param tempOnlineId
	 * @param chkExistPeopleFlag
	 * @return
	 * @throws Exception
	 */
	public int storeBusTaxReq(int tempOnlineId, String chkExistPeopleFlag) throws Exception {
		Wrapper db = new Wrapper();

		try {
			String sql = "";

			sql = "UPDATE TEMP_ONLINE_LSO_ADDRESS	SET ISBUSTAXREQ = " + StringUtils.checkString(chkExistPeopleFlag) + " WHERE TEMP_ONLINEID  = " + tempOnlineId + " ";
			logger.debug(" Update Query is " + sql);

			db.insert(sql);
		} catch (Exception e) {
			logger.error("Exception thrown while trying to add online Permit  :" + e.getMessage());
		}

		return tempOnlineId;
	}

	/**
	 * Store self people
	 * 
	 * @param tempOnlineId
	 * @param emailAddr
	 * @param peopleType
	 * @return
	 * @throws Exception
	 */
	public int storeSelfPeople(int tempOnlineId, String emailAddr, String peopleType) throws Exception {
		Wrapper db = new Wrapper();

		int peopleId = 0;
		String sql1 = null;

		try {
			sql1 = "select * from people where email_addr=" + StringUtils.checkString(emailAddr) + " and ";

			if (peopleType.equalsIgnoreCase("C")) {
				sql1 = sql1 + "people_type_id=4";
			} else if (peopleType.equalsIgnoreCase("E")) {
				sql1 = sql1 + "people_type_id=7";
			} else if (peopleType.equalsIgnoreCase("R")) {
				sql1 = sql1 + "people_type_id=3";
			} else if (peopleType.equalsIgnoreCase("W")) {
				sql1 = sql1 + "people_type_id=8";
			} else if (peopleType.equalsIgnoreCase("A")) {
				sql1 = sql1 + "people_type_id=2";
			}

			logger.debug("ABC" + sql1);

			RowSet rs1 = db.select(sql1);

			while (rs1.next()) {
				peopleId = rs1.getInt("PEOPLE_ID");
			}

			if (peopleType.equalsIgnoreCase("A")) {
				if (peopleId == 0) {
					sql1 = "select * from people where email_addr=" + StringUtils.checkString(emailAddr) + " and people_type_id IN (3,4,7,8) order by people_type_id  desc";
					logger.debug(sql1);
					rs1 = db.select(sql1);
					OnlineRegisterFrom regForm = new OnlineRegisterFrom();
					if (rs1.next()) {
						regForm.setPeopleType(rs1.getString("PEOPLE_TYPE_ID"));
						regForm.setFirstName(rs1.getString("NAME"));
						regForm.setLastName("");
						regForm.setAddress(rs1.getString("ADDR"));// Address
						regForm.setPhoneNbr(StringUtils.phoneFormat(rs1.getString("PHONE"))); // Phone
						regForm.setEmailAddress(rs1.getString("EMAIL_ADDR")); // EXT_USERNAME
						regForm.setLicenseNbr(""); // LicenseNbr
						regForm.setLicExpDate(null); // LicExpDate
						regForm.setPwd(""); // LicExpDate
						regForm.setPhoneExt(rs1.getString("EXT"));
						regForm.setFax(StringUtils.phoneFormat(rs1.getString("FAX")));
						saveOBCUser(regForm);
					}
					sql1 = "select * from people where email_addr=" + StringUtils.checkString(emailAddr) + " and people_type_id IN (4)";
					logger.debug(sql1);
					rs1 = db.select(sql1);
					if (rs1.next()) {
						peopleId = rs1.getInt("PEOPLE_ID");
					}
				}
			}

			String sql = "";

			sql = "UPDATE TEMP_ONLINE_LSO_ADDRESS	SET ";

			if (peopleType.equalsIgnoreCase("C")) {
				sql = sql + " C_People_ID = " + peopleId;
			} else if (peopleType.equalsIgnoreCase("E")) {
				sql = sql + " E_People_ID = " + peopleId;
			} else if (peopleType.equalsIgnoreCase("R")) {
				sql = sql + " R_People_ID = " + peopleId;
			} else if (peopleType.equalsIgnoreCase("W")) {
				sql = sql + " W_People_ID = " + peopleId;
			} else if (peopleType.equalsIgnoreCase("A")) {
				sql = sql + " A_People_ID = " + peopleId;
			}

			sql = sql + " WHERE TEMP_ONLINEID  = " + tempOnlineId;
			logger.debug(" Update Query is " + sql);

			db.insert(sql);
		} catch (Exception e) {
			logger.error("Exception thrown while trying to add online Permit  :" + e.getMessage());
		}
		return tempOnlineId;
	}

	/**
	 * update people type
	 * 
	 * @param tempOnlineId
	 * @param peopleType
	 * @return
	 * @throws Exception
	 */
	public int updatePeopleType(int tempOnlineId, String peopleType) throws Exception {
		Wrapper db = new Wrapper();

		try {
			String sql = "";

			sql = "UPDATE TEMP_ONLINE_LSO_ADDRESS	SET People_Type = " + StringUtils.checkString(peopleType) + " WHERE TEMP_ONLINEID  = " + tempOnlineId + " ";
			logger.debug(" Update Query is " + sql);

			db.insert(sql);
		} catch (Exception e) {
			logger.error("Exception thrown while trying to add online Permit  :" + e.getMessage());
		}

		return tempOnlineId;
	}

	/**
	 * get people type
	 * 
	 * @param tempOnlineId
	 * @return
	 * @throws Exception
	 */
	public String getPeopleType(int tempOnlineId) throws Exception {
		String sql;
		String peopleType = "";

		try {
			sql = "SELECT * from TEMP_ONLINE_LSO_ADDRESS WHERE TEMP_ONLINEID  = " + tempOnlineId;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				peopleType = rs.getString("PEOPLE_TYPE");
				logger.debug("peopleType" + peopleType);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}

		return peopleType;
	}

	/**
	 * get the current permit entered details from Temp Online LSO Address Table and Display in side Steps
	 * 
	 * @param tempOnlineID
	 * @return
	 */
	public List getTempOnlineDetails(int tempOnlineID) {
		logger.info("Entered getTempOnlineDetails(" + tempOnlineID + ")..");

		List tempOnlineDetails = new ArrayList();
		String sql = "";
		ApplyOnlinePermitForm applyOnlinePermitForm = new ApplyOnlinePermitForm();
		int lsoId = 0;
		int sTypeId = 0;

		try {
			sql = "select VAL.ADDRESS,TOLA.STRUCTURENAME,LAS.ACT_SUBTYPE,TOLA.VALUATION,TOLA.SQ_FOOTAGE,TOLA.NO_DWELL_UNITS,TOLA.NO_FLOORS,TOLA.STEP_URL,TOLA.STYPE_ID,TOLA.START_DATE as START_DATE,TOLA.PROJECT_NAME,E.NAME  as Engineer,O.NAME as Owner,R.NAME  as Architect,C.NAME  as Contractor  from temp_Online_lso_address TOLA left outer join LKUP_ACT_SUBTYPE LAS on TOLA.USE_MAP_ID=LAS.ACT_SUBTYPE_ID left outer join V_ADDRESS_LIST VAL on VAL.LSO_ID=TOLA.LSO_ID left outer join People O on O.PEOPLE_ID=TOLA.W_PEOPLE_ID left outer join People R on R.PEOPLE_ID=TOLA.R_PEOPLE_ID left outer join People C on C.PEOPLE_ID=TOLA.C_PEOPLE_ID left outer join People E on E.PEOPLE_ID=TOLA.E_PEOPLE_ID where TEMP_ONLINEID = " + tempOnlineID;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				applyOnlinePermitForm.setAddress(rs.getString("address"));
				applyOnlinePermitForm.setStructureName(rs.getString("STRUCTURENAME"));
				applyOnlinePermitForm.setSubProjectName(rs.getString("ACT_SUBTYPE"));
				applyOnlinePermitForm.setValuation(rs.getString("valuation"));
				applyOnlinePermitForm.setProjectName(rs.getString("project_name"));
				applyOnlinePermitForm.setContractorName(rs.getString("contractor"));
				applyOnlinePermitForm.setOwnerName(rs.getString("owner"));
				applyOnlinePermitForm.setArchitectName(rs.getString("architect"));
				applyOnlinePermitForm.setEngineerName(rs.getString("engineer"));
				applyOnlinePermitForm.setStypeId(StringUtils.s2i(rs.getString("stype_id")));
				applyOnlinePermitForm.setStartDate(StringUtils.cal2str(StringUtils.dbDate2cal(rs.getString("START_DATE"))));
				
				applyOnlinePermitForm.setNumberOfDwellingUnits(rs.getString("NO_DWELL_UNITS"));
				applyOnlinePermitForm.setNumberOfFloors(rs.getString("NO_FLOORS"));
				applyOnlinePermitForm.setSquareFootage(rs.getString("SQ_FOOTAGE"));
				applyOnlinePermitForm.setUrl(rs.getString("STEP_URL"));

			}

			logger.debug("applyOnlinePermitForm " + applyOnlinePermitForm.getAddress());

			tempOnlineDetails.add(applyOnlinePermitForm);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return tempOnlineDetails;
	}

	/**
	 * Get owner list
	 * 
	 * @param tempOnlineId
	 * @return
	 * @throws Exception
	 */
	public List getOwnerList(int tempOnlineId) throws Exception {
		List peopleOwnerList = new ArrayList();

		String sql;
		int lsoId = 0;

		try {
			sql = "SELECT * from TEMP_ONLINE_LSO_ADDRESS WHERE TEMP_ONLINEID  = " + tempOnlineId;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				lsoId = rs.getInt("land_lso_id");

				logger.debug("lsoId" + lsoId);
			}

			String name = "";
			int ownerId = 0;
			String sql1 = "select * from Owner O left outer join Apn_owner AO on O.owner_id=AO.Owner_id " + " left outer join LSO_APN LA on LA.APN=AO.APN 	where LA.lso_id=" + lsoId + " and o.name is not null";
			logger.debug("OWNER ----" + sql1);

			RowSet rs1 = new Wrapper().select(sql1);

			while (rs1 != null && rs1.next()) {
				name = rs1.getString("name");
				ownerId = rs1.getInt("Owner_Id");
				logger.debug("name" + name);
			}

			String sql2 = "select * from people where Upper(name)=" + StringUtils.checkString(name != null ? name.toUpperCase() : "") + "  and People_type_id=8 ";
			logger.debug("OWNER ----" + sql2);

			RowSet rs2 = new Wrapper().select(sql2);
			int i = 0;
			ApplyOnlinePermitForm applyOnlinePermitForm = new ApplyOnlinePermitForm();

			while (rs2.next()) {
				applyOnlinePermitForm.setPeopleId(rs2.getInt("people_id"));
				applyOnlinePermitForm.setAgentName(rs2.getString("name"));
				applyOnlinePermitForm.setCheckOwnerOrPeople("P");

				// logger.debug("peopleId" + peopleId);
				peopleOwnerList.add(applyOnlinePermitForm);
				i++;
			}

			if (i <= 0) {
				rs1 = new Wrapper().select(sql1);

				while (rs1.next()) {
					name = rs1.getString("name");
					logger.debug("Owner NAme" + name);
					ownerId = rs1.getInt("Owner_Id");
					logger.debug("ownerId" + ownerId);
					applyOnlinePermitForm = new ApplyOnlinePermitForm();
					applyOnlinePermitForm.setPeopleId(ownerId);
					applyOnlinePermitForm.setAgentName(rs1.getString("name"));
					applyOnlinePermitForm.setCheckOwnerOrPeople("O");
					peopleOwnerList.add(applyOnlinePermitForm);
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}

		return peopleOwnerList;
	}

	/**
	 * create people on owner
	 * 
	 * @param tempOnlineId
	 * @param chkExistPeopleFlag
	 * @param checkOwnerOrPeople
	 * @throws Exception
	 */
	public void createPeopleOnOwner(int tempOnlineId, String chkExistPeopleFlag, String checkOwnerOrPeople) throws Exception {
		String sql;
		String peopleType = "";
		String ownerName = "";
		String ownerAddress = "";
		String ownerCity = "";
		String ownerState = "";
		String ownerZip = "";
		String ownerEmail = "";
		String ownerPhone = "";
		String ownerFax = "";
		int peopleId = 0;

		try {
			if (checkOwnerOrPeople.equalsIgnoreCase("O")) {
				sql = "SELECT * from OWNER WHERE OWNER_ID  = " + chkExistPeopleFlag;
				logger.debug(sql);

				RowSet rs = new Wrapper().select(sql);

				while (rs.next()) {
					ownerName = StringUtils.nullReplaceWithEmpty(rs.getString("name"));
					ownerAddress = StringUtils.nullReplaceWithEmpty(rs.getString("STR_NO")) + " " + StringUtils.nullReplaceWithEmpty(rs.getString("STR_MOD")) + " " + StringUtils.nullReplaceWithEmpty(rs.getString("PRE_DIR")) + " " + StringUtils.nullReplaceWithEmpty(rs.getString("STR_NAME"));
					ownerCity = rs.getString("CITY");
					ownerState = rs.getString("STATE");
					ownerZip = rs.getString("ZIP");
					ownerEmail = rs.getString("EMAIL_ID");
					ownerPhone = rs.getString("PHONE");
					ownerFax = rs.getString("FAX");
				}

				// now generating peopleID and adding it to people table
				peopleId = new Wrapper().getNextId("PEOPLE_ID");
				logger.debug("The ID generated for new People is " + peopleId);

				sql = "insert into people(PEOPLE_ID,PEOPLE_TYPE_ID,NAME,ADDR,CITY,STATE,ZIP,PHONE,EMAIL_ADDR,FAX,COMNTS) values(";
				sql += peopleId;
				logger.debug("got people id " + peopleId);
				sql += ",";
				sql += Constants.PEOPLE_TYPE_OWNER_ID;

				sql += ",";
				sql += StringUtils.checkString(ownerName);

				sql += ",";
				sql += StringUtils.checkString(ownerAddress);

				sql += ",";
				sql += StringUtils.checkString(ownerCity);

				sql += ",";
				sql += StringUtils.checkString(ownerState);

				sql += ",";
				sql += StringUtils.checkString(ownerZip);

				sql += ",";

				// sql += StringUtils.checkString(people.getPhoneNbr());
				sql += StringUtils.checkString(StringUtils.phoneFormat(ownerPhone));
				sql += ",";

				String email = (!StringUtils.checkString(ownerEmail).equals("null")) ? ("LOWER(" + StringUtils.checkString(ownerEmail) + ")") : null;
				logger.debug("got email from varaibale  " + email);

				sql += email;
				sql += ",";
				sql += StringUtils.checkString(StringUtils.phoneFormat(ownerFax));

				sql += ",";
				sql += StringUtils.checkString("Online Purpose People Added from Assesor data");

				sql += ")";
				new Wrapper().insert(sql);
				logger.debug("People ID newly created" + peopleId);
			} else {
				peopleId = StringUtils.s2i(chkExistPeopleFlag);
				logger.debug("People ID already present" + peopleId);
			}

			sql = "UPDATE TEMP_ONLINE_LSO_ADDRESS	SET ";

			sql = sql + " W_People_ID = " + peopleId;

			sql = sql + " WHERE TEMP_ONLINEID  = " + tempOnlineId;
			logger.debug(" Update Query is " + sql);

			new Wrapper().insert(sql);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public int getPeopleType(int tempOnlineId, String emailAddr) throws Exception {
		return getPeopleType(tempOnlineId, emailAddr, null);
	}

	public int getPeopleType(int tempOnlineId, String emailAddr, String people_Type) throws Exception {
		Wrapper db = new Wrapper();

		int peopleTypeId = 0;
		String sql1 = null;

		try {
			sql1 = "select * from people where Upper(email_addr)=" + StringUtils.checkString(emailAddr.toUpperCase());
			if (people_Type == null) {
				sql1 += " and   people_type_id not in(2)";
			} else {
				sql1 += " and   people_type_id = " + people_Type;
			}

			logger.debug("peopleTypeId" + sql1);

			RowSet rs1 = db.select(sql1);

			while (rs1.next()) {
				peopleTypeId = rs1.getInt("PEOPLE_TYPE_ID");
			}
		} catch (Exception e) {
			logger.error("Exception thrown while trying to add online Permit  :" + e.getMessage());
		}
		return peopleTypeId;
	}

	// strucutre based on mapping present

	/**
	 * gets the list of sub project types
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getSubProjectTypes(List lsoLists) throws Exception {
		logger.debug("getSubProjectTypes()");

		List subProjectTypeList = new ArrayList();

		String sql = "";

		try {

			RowSet rs = null;

			for (int i = 0; i < 1; i++) {
				ApplyOnlinePermitForm applyOnlinePermitForm = (ApplyOnlinePermitForm) lsoLists.get(i);

				applyOnlinePermitForm.setStructureName("Accessory Structure");
				applyOnlinePermitForm.setStructureId(applyOnlinePermitForm.getLsoId());
				subProjectTypeList.add(applyOnlinePermitForm);

				applyOnlinePermitForm.setStructureName("Garage");
				applyOnlinePermitForm.setStructureId(applyOnlinePermitForm.getLsoId());
				subProjectTypeList.add(applyOnlinePermitForm);

				applyOnlinePermitForm.setStructureName("Main Residence");
				applyOnlinePermitForm.setStructureId(applyOnlinePermitForm.getLsoId());
				subProjectTypeList.add(applyOnlinePermitForm);

				applyOnlinePermitForm.setStructureName("Unit");
				applyOnlinePermitForm.setStructureId(applyOnlinePermitForm.getLsoId());
				subProjectTypeList.add(applyOnlinePermitForm);
			}

			if (rs != null) {
				rs.close();
			}

			return subProjectTypeList;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	// onlinePermitAgent.getPermitNo(tempOnlineID);
	public String getPermitNo(int tempOnlineID) throws Exception {
		Wrapper db = new Wrapper();

		String comboNo = "";
		String sql1 = null;

		try {
			sql1 = "select * from TEMP_ONLINE_LSO_ADDRESS where TEMP_ONLINEID=" + tempOnlineID;

			RowSet rs1 = db.select(sql1);

			while (rs1.next()) {
				comboNo = rs1.getString("COMBO_NBR");
			}
		} catch (Exception e) {
			logger.error("Exception thrown while trying to add online Permit  :" + e.getMessage());
		}

		return comboNo;
	}

	// subproject no
	public String getPermitComboNo(int levelId) throws Exception {
		logger.debug("getPermitComboNo");

		Wrapper db = new Wrapper();

		String comboNo = "";

		String sql1 = null;

		try {
			sql1 = "select SPROJ_NBR,COMBO_NBR from SUB_PROJECT where SPROJ_ID=" + levelId;
			logger.debug(sql1);

			RowSet rs1 = db.select(sql1);

			while (rs1.next()) {
				comboNo = (rs1.getString("COMBO_NBR") != null) ? rs1.getString("COMBO_NBR") : rs1.getString("SPROJ_NBR");
			}
		} catch (Exception e) {
			logger.error("Exception thrown while trying to add online Permit  :" + e.getMessage());
		}

		return comboNo;
	}

	/**
	 * gets the list of Combo Use Map List - Added by Manjuprasad
	 * 
	 * @return
	 * @throws Exception
	 */
	public static int getStartsAfter(int tempOnlineID, int stypeId) throws Exception {
		logger.debug("getStartsAfter(" + tempOnlineID + ")");

		int lsoId = 0;
		String sql;
		String sqlLandUse;
		int lsoUseId = 0;
		String sqlComboNames;
		int useMapId = 0;

		try {
			sql = "SELECT LSO_ID,USE_MAP_ID FROM TEMP_ONLINE_LSO_ADDRESS where TEMP_ONLINEID =" + tempOnlineID;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				lsoId = rs.getInt("LSO_ID");
				useMapId = rs.getInt("USE_MAP_ID");

			}

			sqlLandUse = "select lu.lso_use_id from lso_use lu , v_lso_owner vlo where vlo.land_use = lu.description and vlo.lso_id=" + lsoId;
			logger.debug(sqlLandUse);

			RowSet rsLandUse = new Wrapper().select(sqlLandUse);

			while (rsLandUse.next()) {
				lsoUseId = rsLandUse.getInt("lso_use_id");
			}

			logger.debug(StringUtils.i2s(lsoUseId));
			sqlComboNames = "select distinct(lsam.start_after) from lkup_sproj_act_mapp lsam left outer join lkup_combo_use_mapp lcum on " + "(lcum.sproj_name = lsam.sproj_name)  left outer join LSO_USE lu on (lu.lso_use_id = lcum.lso_USEID  ) where lcum.use_mapid=" + useMapId + " and lcum.lso_useid=" + lsoUseId + " and lsam. isActive='Y'";
			logger.debug("sqlComboNames  is " + sqlComboNames);

			RowSet rsStartsAfter = new Wrapper().select(sqlComboNames);
			int startsAfter = 0;
			ApplyOnlinePermitForm applyOnlinePermitForm = new ApplyOnlinePermitForm();

			while (rsStartsAfter.next()) {
				startsAfter = rsStartsAfter.getInt("start_after");
			}

			rs.close();

			return startsAfter;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * 
	 * @param sprojId
	 * @param comboName
	 * @param txn_id
	 * @param amt
	 * @param userId
	 * @param statusCode
	 * @param statusMessage
	 *            The function to insert the Transaction details to ONLINE_PAYMENT_TRANSACTION after online payment is done
	 */
	/**
	 * 
	 * @param sprojId
	 * @param comboName
	 * @param txn_id
	 * @param amt
	 * @param userId
	 * @param statusCode
	 * @param statusMessage
	 *            The function to insert the Transaction details to ONLINE_PAYMENT_TRANSACTION after online payment is done
	 */
	public static void saveOnlinePayment(String actId, String comboName, String txn_id, String amt, int userId, String statusCode, String statusMessage, String remoteIP, String applied,String payerName) {
		logger.info("Entered into saveOnlinePayment...");
		Wrapper db = new Wrapper();

		String sql="";
		try {
					
			if(actId != null) {			
				
				sql = "SELECT * FROM QUERY_TRANSACTION WHERE ACTIVE='Y' and QUERY_ACT_ID="+actId+"  ORDER BY QUERY_TRANS_ID ASC";   
				
				logger.debug(sql);
				ResultSet rs = null;
				String sqlStr="";   
				
					rs = db.select(sql);
		            int i=0;
					while (rs.next()) {
						logger.debug("****Query update***** " + rs.getString("QUERY_STRING"));
						db.update(rs.getString("QUERY_STRING"));
						i++;
					} 
					if (rs != null) {
						rs.close();
					}
					
					if(i>0)
					{
					sql ="UPDATE QUERY_TRANSACTION set ACTIVE='N' WHERE ACTIVE='Y' and QUERY_ACT_ID="+actId;
					db.update(sql);
					}
					
				sql = "UPDATE PAYMENT SET ONLINETXNID =" + StringUtils.checkString(txn_id) + ",PAYEE="+ StringUtils.checkString(payerName) +" WHERE PYMNT_ID IN(SELECT MAX(PYMNT_ID) FROM PAYMENT_DETAIL WHERE ACT_ID ="+actId+")";
						//+ "SELECT PYMNT_ID FROM PAYMENT_DETAIL WHERE ACT_ID ="+actId+")";
     		    new Wrapper().update(sql);
    			logger.debug("People Manager update sql is :" + sql);
    			
    			String updateSql = "UPDATE ACTIVITY SET UPDATED = CURRENT_TIMESTAMP, UPDATED_BY = " + userId +" WHERE ACT_ID=" + actId;
                logger.debug(updateSql);
                db.update(updateSql);
			}

			int transId = db.getNextId("TRANS_ID");
			sql = "insert into ONLINE_PAYMENT_TRANSACTION(TRANS_ID,TRANS_CODE,SPROJ_ID,COMBO_NAME,AMOUNT_PAID,USER_ID,STATUS_CODE,STATUS_DESC,IPADDR,TRANS_TIME,APPLIED) " +
					"values(" + transId + "," + StringUtils.checkString(txn_id) + "," + actId + "," + StringUtils.checkString(comboName) + "," + amt + "," + userId + "," + StringUtils.checkString(statusCode) + "," + StringUtils.checkString(statusMessage) + ",'',current_timestamp," + StringUtils.checkString(StringUtils.nullReplaceWithEmpty(applied)) + ")";
			logger.debug("Sql query is : " + sql);
			db.insert(sql);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception while inserting payment details" + e.getMessage());
		}

		logger.info("Exiting from saveOnlinePayment...");
	}



	public static void saveOnlinePaymentForCart(String cartId, String comboName, String txn_id, String amt, int userId, String statusCode, String statusMessage, String remoteIP, String applied,String payerName) {
		logger.info("Entered into saveOnlinePaymentForCart...");
		Wrapper db = new Wrapper();

		String sql="";
		try {
			int transId = db.getNextId("TRANS_ID");
			//In place of sproj_id we are passing the cart _id
			sql = "insert into ONLINE_PAYMENT_TRANSACTION(TRANS_ID,TRANS_CODE,SPROJ_ID,COMBO_NAME,AMOUNT_PAID,USER_ID,STATUS_CODE,STATUS_DESC,IPADDR,TRANS_TIME,APPLIED) " +
					"values(" + transId + "," + StringUtils.checkString(txn_id) + "," + cartId + "," + StringUtils.checkString(comboName) + "," + amt + "," + userId + "," + StringUtils.checkString(statusCode) + "," + StringUtils.checkString(statusMessage) + ",'',current_timestamp," + StringUtils.checkString(StringUtils.nullReplaceWithEmpty(applied)) + ")";
			logger.debug("Sql query is : " + sql);
			db.insert(sql);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception while inserting payment details" + e.getMessage());
		}

		logger.info("Exiting from saveOnlinePayment...");
	}


public void rollBackTransaction(int actId) {
	
	logger.debug("Enter rollBackTransaction " + actId);   
				
		try{
			String sql ="UPDATE QUERY_TRANSACTION set ACTIVE='N' WHERE ACTIVE='Y' and QUERY_ACT_ID="+actId;
			logger.debug(" slq is "+sql);
			db.update(sql);
			
		}catch(Exception e){
			logger.error(e.getMessage());
		}
		
		
	}


	// Internal Payment Processing - Sunil
	public void processInternalPayment(PaymentMgrForm paymentMgrForm, String sprojId, User user) {
		logger.debug("Enter processInternalPayment ");

		FinanceAgent financeAgent = new FinanceAgent();

		try {
			int levelId = StringUtils.s2i(sprojId);

			String level = "A";

			char voided = 'N';
			String payee = "";
			String feevalues = "";
			String somename = "";
			String planCheck = "";
			String permitFees = "";
			String businessTax = "";
			String other = "";
			String otherText = "";
			double amount = 0;
			String feeType = "";
			String method = "";
			String checkNo = "";
			String comments = "";
			int paidBy = 0;
			String departmentCode;
			double pcCredit = 0;
			double pmtCredit = 0;
			boolean isCheckNo = false;

			// int tType =0;
			PayType payType = new PayType();
			String authorizedBy = "0";

			planCheck = paymentMgrForm.getPlanCheck();

			if (planCheck == null) {
				planCheck = "off";
			}

			permitFees = paymentMgrForm.getPermitFees();

			if (permitFees == null) {
				permitFees = "off";
			}

			businessTax = paymentMgrForm.getBusinessTax();

			if (businessTax == null) {
				businessTax = "off";
			}

			if (planCheck.equals("on")) {
				feeType = "'1',";
			}

			if (permitFees.equals("on")) {
				feeType += "'2',";
			}

			if (businessTax.equals("on")) {
				feeType += "'5',";
			}

			feeType += "'X'";

			logger.debug("plancheck= " + planCheck);
			logger.debug("permitfees= " + permitFees);
			logger.debug("businessTax= " + businessTax);
			logger.debug("feeType = " + feeType);

			method = "creditcard";
			logger.debug("method = " + method);

			payType.setPayTypeId(StringUtils.s2i("1"));
			logger.debug("Transaction Type = " + payType);

			if (payType.getPayTypeId() == 3) {
				voided = 'Y';
			}

			paidBy = (user.getUserId());
			logger.debug("Paid By " + paidBy);
			other = paymentMgrForm.getOther();
			logger.debug("Other = " + other);

			if (other == null) {
				other = "off";
			}

			if (other.equals("on")) {
				otherText = paymentMgrForm.getOtherText();
				logger.debug("othertext = " + otherText);
			}

			amount = StringUtils.s2bd(paymentMgrForm.getAmount()).doubleValue();
			logger.debug("Amount =  " + amount);

			checkNo = "";
			logger.debug("checkNo= " + checkNo);

			comments = "";
			logger.debug("comment= " + comments);

			authorizedBy = paymentMgrForm.getAuthorizedBy();
			logger.debug("authorizedby= " + authorizedBy);

			pcCredit = StringUtils.s2bd(financeAgent.pcpCreditForActivity('1', levelId)).doubleValue();
			logger.debug("plan check credit= " + pcCredit);

			pmtCredit = StringUtils.s2bd(financeAgent.pcpCreditForActivity('2', levelId)).doubleValue();
			logger.debug("permit credit= " + pmtCredit);

			Payment payment = new Payment(method, payType, paidBy, otherText, amount, checkNo, comments, user, authorizedBy, voided);
			payment.setLevelId(levelId);

			departmentCode = (new ActivityAgent()).getDepartmentCode(levelId);
			payment.setDepartmentCode(departmentCode);

			payment.setOnlineTxnId(paymentMgrForm.getOnlineTxnId());

			// String[] selectedAct = paymentMgrForm.getSeleectedActivity();

			String[] selectedAct = { "" + sprojId };

			String activityIds = "";

			if ((selectedAct != null) && (selectedAct.length > 0)) {
				for (int i = 0; i < selectedAct.length; i++) {
					activityIds = activityIds + selectedAct[i] + "," + activityIds;
				}

				activityIds += "0";
			}

			logger.debug("The Paramater string before passing" + activityIds);

			// financeAgent.paySubProjectBalance(levelId, payment);
			if (activityIds.length() > 0) {
				logger.debug("*** Final Step");
				if(payment.getPeopleId() == 0){
					payment.setPeopleId(user.getUserId());
				}
				financeAgent.payComboSubProjectBalance(levelId, payment, activityIds);
			}
		} catch (Exception e) {
			logger.error("Exception while inserting payment details" + e.getMessage());
		}

		logger.info("Exiting from saveOnlinePayment...");
	}

	// Internal Plan check fees Payment Processing - Sunil PC only
	/*
	 * public void processInternalPCPayment( PaymentMgrForm paymentMgrForm, String sprojId, User user) { logger.debug("Enter processInternalPayment ");
	 * 
	 * FinanceAgent financeAgent = new FinanceAgent();
	 * 
	 * try { int levelId = StringUtils.s2i(sprojId);
	 * 
	 * String level = "Q";
	 * 
	 * char voided = 'N'; String payee = ""; String feevalues = ""; String somename = ""; String planCheck = ""; String permitFees = ""; String businessTax = ""; String other = ""; String otherText = ""; double amount = 0; String feeType = ""; String method = ""; String checkNo = ""; String comments = ""; int paidBy = 0; String departmentCode; double pcCredit = 0; double pmtCredit = 0; boolean isCheckNo = false;
	 * 
	 * // int tType =0; PayType payType = new PayType(); int authorizedBy = 0;
	 * 
	 * planCheck = paymentMgrForm.getPlanCheck();
	 * 
	 * if (planCheck == null) { planCheck = "off"; }
	 * 
	 * if (planCheck.equals("on")) { feeType = "'1',"; }
	 * 
	 * feeType += "'X'";
	 * 
	 * logger.debug("plancheck= " + planCheck); logger.debug("permitfees= " + permitFees); logger.debug("businessTax= " + businessTax); logger.debug("feeType = " + feeType);
	 * 
	 * method = "creditcard"; logger.debug("method = " + method);
	 * 
	 * payType.setPayTypeId(StringUtils.s2i("1")); logger.debug("Transaction Type = " + payType);
	 * 
	 * if (payType.getPayTypeId() == 3) { voided = 'Y'; }
	 * 
	 * paidBy = (user.getUserId()); logger.debug("Paid By " + paidBy); other = paymentMgrForm.getOther(); logger.debug("Other = " + other);
	 * 
	 * if (other == null) { other = "off"; }
	 * 
	 * if (other.equals("on")) { otherText = paymentMgrForm.getOtherText(); logger.debug("othertext = " + otherText); }
	 * 
	 * amount = StringUtils.s2bd(paymentMgrForm.getAmount()).doubleValue(); logger.debug("Amount =  " + amount);
	 * 
	 * checkNo = ""; logger.debug("checkNo= " + checkNo);
	 * 
	 * comments = ""; logger.debug("comment= " + comments);
	 * 
	 * authorizedBy = elms.util.StringUtils.s2i(paymentMgrForm.getAuthorizedBy()); logger.debug("authorizedby= " + authorizedBy);
	 * 
	 * pcCredit = StringUtils .s2bd(financeAgent.pcpCreditForActivity('1', levelId)) .doubleValue(); logger.debug("plan check credit= " + pcCredit);
	 * 
	 * Payment payment = new Payment( method, payType, paidBy, otherText, amount, checkNo, comments, user, authorizedBy, voided); payment.setLevelId(levelId);
	 * 
	 * departmentCode = (new ProjectAgent() .getSubProjectLite(levelId) .getSubProjectDetail() .getSubProjectType() .getDepartmentCode()); payment.setDepartmentCode(departmentCode);
	 * 
	 * String[] selectedAct = paymentMgrForm.getSelectedActivity(); String activityIds = "";
	 * 
	 * if ((selectedAct != null) && (selectedAct.length > 0)) { for (int i = 0; i < selectedAct.length; i++) { financeAgent.payComboPCSubProjectBalance( levelId, payment, selectedAct[i]);
	 * 
	 * } }
	 * 
	 * logger.debug("The Paramater string before passing" + activityIds);
	 * 
	 * // financeAgent.paySubProjectBalance(levelId, payment); if (activityIds.length() > 0) { logger.debug("*** Final Step"); financeAgent.payComboPCSubProjectBalance(levelId, payment, activityIds); } } catch (Exception e) { logger.error( "Exception while inserting payment details" + e.getMessage()); }
	 * 
	 * logger.info("Exiting from saveOnlinePayment..."); }
	 */
	public static String composeEmailMessage(String username, String comboNo, String amount) {
		StringBuffer messageBuffer = new StringBuffer();
		messageBuffer.append("Hello " + username + " ,\n\n");
		messageBuffer.append("Payment Recieved\n\n");
		messageBuffer.append("Permit No : " + comboNo + "\n");
		messageBuffer.append("Amount : " + amount + "\n\n");
		messageBuffer.append("CAP- City of Burbank\n\n");
		messageBuffer.append("Enjoy!\n\n");
		messageBuffer.append("Visit The City of Burbank Online : http://www.ci.burbank.ca.us/");

		return messageBuffer.toString();
	}

	public String checkPlanCheck(String sprojId) {
		String pcChk = "Y";
		String sql = "select status from activity where act_id  =" + sprojId;
		logger.debug(sql);

		try {
			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				String status = rs.getString("status");
				if ((!(status.equals(Constants.ACTIVITY_PCREQ_STATUS)) || status.equals(Constants.APP_COMPLETE) || status.equals(Constants.APPEAL_FILED) || status.equals(Constants.APPROVAL) || status.equals(Constants.BALANCE_DUE) || status.equals(Constants.PC_APPR_FD) || status.equals(Constants.PC_APPR_FEE) || status.equals(Constants.PC_APPROVED) || status.equals(Constants.PC_HOLD) || status.equals(Constants.PC_PERMIT_RTI) || status.equals(Constants.PC_SUB_FEE) || status.equals(Constants.PC_SUBMITTED) || status.equals(Constants.PC_CORR) || status.equals(Constants.PC_FINAL) || status.equals(Constants.PENDING) || status.equals(Constants.PERMIT_READY) || status.equals(Constants.PERMIT_REQUIRED) || status.equals(Constants.PERMIT_RTI)) && !(status.equals(Constants.ACTIVITY_ISSUED_STATUS))) {
					pcChk = "N";
					break;
				}
			}
		} catch (Exception e) {
			logger.error("Error in " + e.getMessage());
		}

		return pcChk;
	}

	public String checkPrintPermit(String sprojId) {
		String pmtChk = "N";
		String sql = "select status from activity where act_id =" + sprojId;
		logger.debug(sql);

		try {
			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				if (rs.getString("status").equals(Constants.ACTIVITY_ISSUED_STATUS)) {
					pmtChk = "Y";
				} else {
					pmtChk = "N";

					break;
				}
			}
		} catch (Exception e) {
			logger.error("Error in " + e.getMessage());
		}

		return pmtChk;
	}

	public boolean checkComboMappingApproval(String sprojId, PaymentMgrForm paymentMgrForm) {
		logger.debug("AADD");

		String sql = "";
		String approval = "";
		boolean checkApproval = false;
		String sqlUpdate = "";
		String sqlUpdateActStatus = "";

		try {
			sql = "select distinct(approval) from lkup_sproj_act_mapp lsam left outer join activity  a on (lsam.act_type = a.act_type)" + " where a.act_id=" + sprojId;

			logger.debug("SQL is " + sql);

			ResultSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				approval = rs.getString("approval");
			}

			if (approval.equalsIgnoreCase("Y")) {
				checkApproval = true;
			} else {
				checkApproval = false;
				sqlUpdate = "";

				// String[] selectedAct = paymentMgrForm.getSelectedActivity();
				String[] selectedAct = { sprojId };
				String activityIds = "";

				if ((selectedAct != null) && (selectedAct.length > 0)) {
					for (int i = 0; i < selectedAct.length; i++) {
						activityIds = activityIds + selectedAct[i] + "," + activityIds;
					}

					activityIds += "0";
				}

				logger.debug("The Paramater string before passing" + activityIds);

				// financeAgent.paySubProjectBalance(levelId, payment);
				if (activityIds.length() > 0) {
					logger.debug("*** Final Step");
					sqlUpdateActStatus = "update activity set status=" + Constants.ACTIVITY_ISSUED_STATUS + " , ISSUED_DATE =current_date , EXP_DATE = add_months(current_date,6) where act_id in (" + activityIds + ")";
					logger.debug("sqlUpdateActStatus " + sqlUpdateActStatus);
					new Wrapper().update(sqlUpdateActStatus);
				}
			}
		} catch (Exception e) {
		}

		return checkApproval;
	}

	/*
	 * Getting list of combo names from LKUP_SPROJ_ACT_MAP
	 */

	public List getComboNameList() {
		logger.info("getComboNameList()");

		String sql = "";
		List comboNameList = new ArrayList();

		try {
			sql = "select distinct(sproj_name) from lkup_sproj_act_mapp where isonlinereq='Y' and ISACTIVE='Y' order by sproj_name";

			logger.debug("SQL is " + sql);

			ResultSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				ApplyOnlinePermitForm applyOnlinePermitForm = new ApplyOnlinePermitForm();
				applyOnlinePermitForm.setSubProjectName(StringUtils.properCase(rs.getString("sproj_name")));
				comboNameList.add(applyOnlinePermitForm);

			}

		} catch (Exception e) {
		}

		return comboNameList;
	}

	public String getPermitComboName(int levelId) throws Exception {
		logger.debug("getPermitComboNo");

		Wrapper db = new Wrapper();

		String comboName = "";

		String sql1 = null;

		try {
			sql1 = "select SPROJ_NBR,COMBO_NAME from SUB_PROJECT where SPROJ_ID=" + levelId;
			logger.debug(sql1);

			RowSet rs1 = db.select(sql1);

			while (rs1.next()) {
				comboName = (rs1.getString("COMBO_NAME") != null) ? rs1.getString("COMBO_NAME") : rs1.getString("SPROJ_NAME");
			}
		} catch (Exception e) {
			logger.error("Exception thrown while trying to add online Permit  :" + e.getMessage());
		}

		return comboName;
	}

	public boolean checkOnlineUser(String emailAddress) throws Exception {
		logger.info("inside checkOnlineUser...");
		try {
			String sql = "select EXT_USER_ID from EXT_USER where lower(EXT_USERNAME) = lower('" + emailAddress + "')";
			logger.debug(sql);
			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				return true;
			}

			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error("error in getting existing online users " + e.getMessage());
		}
		return false;
	}

	public void saveOnlineUser(OnlineRegisterFrom regForm) throws Exception {
		logger.info("inside saveOnlineUser...");
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet result = null;
		try {
			// create an instance of the database wrapper.
			Wrapper db = new Wrapper();
			connection = db.getConnectionForPreparedStatementOnly();

			statement = connection.prepareStatement(INSERT_EXT_USER);
			logger.debug(INSERT_EXT_USER);
			statement.setInt(1, db.getNextId("EXT_USER_ID")); // EXT_USER_ID
			statement.setInt(2, regForm.getAccountNo()); // EXT_ACCT_NBR
			statement.setString(3, regForm.getEmailAddress()); // EXT_USERNAME
			statement.setString(4, jcrypt.crypt("X6", regForm.getPwd())); // EXT_PASSWORD
			statement.setString(5, regForm.getFirstName());
			statement.setString(6, regForm.getLastName());
			statement.setString(7, regForm.getAddress()); // Address
			statement.setString(8, StringUtils.phoneFormat(regForm.getPhoneNbr())); // Phone
			statement.setString(9, regForm.getObc()); // isObc
			statement.setString(10, regForm.getDot()); // isDot
			statement.setString(11, regForm.getCompanyName()); // companyName
			statement.setString(12, regForm.getPhoneExt()); // PhoneExt
			statement.setString(13, StringUtils.phoneFormat(regForm.getWorkPhone())); // WorkPhone
			statement.setString(14, regForm.getWorkExt()); // WorkExt
			statement.setString(15, StringUtils.phoneFormat(regForm.getFax())); // fax
			statement.setString(16, regForm.getCity()); // City
			statement.setString(17, regForm.getState()); // state
			statement.setString(18, regForm.getZip()); // zip
			statement.execute();
			logger.debug("database insert successful");
		} catch (Exception e) {
			logger.error("Exception thrown while trying to insert into ext_user :" + e.getMessage());
		} finally {
			try {
				if (result != null)
					result.close();
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (Exception e) {
				// ignored
			}
		}
	}

	
	public void saveOBCUser(OnlineRegisterFrom regForm) throws Exception {
		 saveOBCUser(regForm, 0);
	}
	
	public void saveOBCUser(OnlineRegisterFrom regForm,int peopleId) throws Exception {
		logger.info("inside saveOBCUser...");
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet result = null;
		try {
			Wrapper db = new Wrapper();
			connection = db.getConnectionForPreparedStatementOnly();
			
			String[] peopleType = StringUtils.stringtoArray(regForm.getPeopleType(), ",");

			
			statement = connection.prepareStatement(INSERT_PEOPLE);
			//statement.setInt(1, peopleId); // EXT_USER_ID
			//statement.setString(2, peopleType[0]); // PeopleType
			for (int i = 0; i < peopleType.length; i++) {
				peopleId = db.getNextId("PEOPLE_ID");
				
				
				statement.setInt(1, peopleId);
				statement.setString(2, peopleType[i]);
			if (!Operator.hasValue(regForm.getCompanyName())) {
				statement.setString(3, (regForm.getFirstName() != null ? regForm.getFirstName() : "") + (regForm.getLastName() != null ? " " + regForm.getLastName() : ""));
				statement.setString(10, regForm.getCompanyName());
			} else {
				statement.setString(3, (regForm.getFirstName() != null ? regForm.getFirstName() : "") + (regForm.getLastName() != null ? " " + regForm.getLastName() : ""));
				statement.setString(10, regForm.getCompanyName());
			}

			statement.setString(4, regForm.getAddress()); // Address
			statement.setString(5, StringUtils.phoneFormat(regForm.getPhoneNbr())); // Phone
			statement.setString(6, regForm.getEmailAddress()); // EXT_USERNAME
			logger.debug(regForm.getEmailAddress());
			if (!Operator.hasValue(regForm.getLicenseNbr())) {
				regForm.setLicenseNbr(null);
				regForm.setLicExpDate(null);
			}
			if (peopleType != null) {
				if (peopleType[i].equals(Constants.PEOPLE_CONTRACTOR + "")) {
					logger.debug("peopleType  PEOPLE_CONTRACTOR "+peopleType[0]);
					java.sql.Date date = new java.sql.Date(0000 - 00 - 00);
					statement.setString(7, regForm.getLicenseNbr()); // LicenseNbr
					logger.debug("regForm.getLicenseNbr()  "+regForm.getLicenseNbr());
					statement.setDate(8, (regForm.getLicExpDate() != null && !regForm.getLicExpDate().equals("")) ? date.valueOf(StringUtils.cal2TimeStamp(StringUtils.str2cal(regForm.getLicExpDate()))) : null); // LicExpDate
					statement.setDate(16, (regForm.getStrWorkersCompExpires() != null && !regForm.getStrWorkersCompExpires().equals("")) ? date.valueOf(StringUtils.cal2TimeStamp(StringUtils.str2cal(regForm.getStrWorkersCompExpires()))) : null); // WorkersCompExpiresDate
					statement.setString(17,regForm.getWorkersCompensationWaive());
					
					logger.debug("regForm.getStrWorkersCompExpires()  "+regForm.getStrWorkersCompExpires());
					logger.debug("regForm.getWorkersCompensationWaive()  "+regForm.getWorkersCompensationWaive());
				
				} else {
					statement.setString(7, null);
					statement.setString(8, null);
					statement.setString(16, null);
					statement.setString(17, null);
				}
			}

			statement.setString(9, regForm.getPwd()); // LicExpDate
			statement.setString(11, regForm.getPhoneExt()); // PhoneExt
			statement.setString(12, StringUtils.phoneFormat(regForm.getFax())); // Fax
			statement.setString(13, regForm.getCity()); // City
			statement.setString(14, regForm.getState()); // State
			statement.setString(15, regForm.getZip()); // Zip
			//statement.execute();

			logger.debug("peopleType.length :::   "+peopleType.length);
			

				
				logger.debug("peopleType["+i+"] :::   "+peopleType[i]);
				
				
				//statement.setString(7, null);
				//statement.setString(8, null);
				statement.execute();
			}
			logger.debug("People database insert successful");
			logger.debug("Ext table database insert successful");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception thrown while trying to insert into people :" + e.getMessage());
		} finally {
			try {
				if (result != null)
					result.close();
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (Exception e) {
				// ignored
				e.printStackTrace();
			}
		}

	}

	public String checkOBCUser(String email, String people_type, OnlineRegisterFrom regForm) {
		logger.info("inside checkOBCUser...");
		String[] peopleType = StringUtils.stringtoArray(people_type, ",");
		String newPeople = "";
		logger.debug("peopleType ::  "+people_type);
		try {
			for (int i = 0; i < peopleType.length; i++) {
				String sql = "select people_id from people where people_type_id='" + peopleType[i]+ "' ";
				if (peopleType[i].equals(Constants.PEOPLE_CONTRACTOR + "")) { // contractor
					sql = sql + " and LIC_NO = " + StringUtils.checkString(regForm.getLicenseNbr()) + " and AGENT_NAME = " + StringUtils.checkString(regForm.getCompanyName());
				} else {
					sql = sql + " and lower(email_addr) = " + StringUtils.checkString(email.toLowerCase());
				}
				logger.debug("sql is : " + sql);
				RowSet rs = new Wrapper().select(sql);
				String name = regForm.getFirstName();
				String firstName = "";
				String lastName = "";
				int index = 0;
				if ((index = name.indexOf(',')) != -1) {
					lastName = name.substring(0, index);
					firstName = name.substring(index + 1);
				} else if ((index = name.indexOf(' ')) != -1) {
					firstName = name.substring(0, index);
					lastName = name.substring(index + 1);
				}
				if (rs.next()) {
					logger.debug("in side if");
					// update is done here as the same function is being used on
					// Update Profile...
					String sqlUpdate = "update PEOPLE set ADDR = " + StringUtils.checkString(regForm.getAddress()) + ", CITY = " + StringUtils.checkString(regForm.getCity()) + ", STATE = " + StringUtils.checkString(regForm.getState()) + ", ZIP = " + StringUtils.checkString(regForm.getZip()) + ", PHONE =" + StringUtils.checkString(StringUtils.stripPhone(regForm.getPhoneNbr())) + ", EMAIL_ADDR = " + StringUtils.checkString(regForm.getEmailAddress()) + ", EXT =" + StringUtils.checkString(regForm.getPhoneExt()) + ", FAX =" + StringUtils.checkString(StringUtils.stripPhone(regForm.getFax()));
					logger.debug("peopleType[i]"+peopleType[i]);
					if (peopleType[i].equals(Constants.PEOPLE_CONTRACTOR + "")) { // contractor
						
						logger.debug("in updating LIC_NO WORKERS_COMP_WAIVE");
						sqlUpdate += ", LIC_NO=" + StringUtils.checkString(regForm.getLicenseNbr()) + ", LIC_EXP_DT=" + StringUtils.toOracleDate(regForm.getLicExpDate()) + ", WORKERS_COMP_WAIVE=" + StringUtils.checkString(regForm.getWorkersCompensationWaive()) + ", WORK_COMP_EXP_DT=" + StringUtils.toOracleDate(regForm.getStrWorkersCompExpires());
					}
					if (regForm.getCompanyName() == null || regForm.getCompanyName().equals("") || regForm.getCompanyName().equals("null")) {
						sqlUpdate += ", NAME=" + StringUtils.checkString(regForm.getFirstName());
					} else {
						sqlUpdate += ", NAME=" + StringUtils.checkString(regForm.getCompanyName()) + ", AGENT_NAME=" + StringUtils.checkString(regForm.getFirstName());
					}
					if (peopleType[i].equals(Constants.PEOPLE_CONTRACTOR + "")) { // contractor
						sqlUpdate = sqlUpdate + " where LIC_NO = " + StringUtils.checkString(regForm.getLicenseNbr()) + " and AGENT_NAME = " + StringUtils.checkString(firstName + " " + lastName) + " or AGENT_NAME = " + StringUtils.checkString(lastName + "," + firstName);
					} else {
						sqlUpdate = sqlUpdate + " where lower(email_addr) = " + StringUtils.checkString(email.toLowerCase()) + " and people_type_id=" + peopleType[i];
					}
					logger.debug("update query :" + sqlUpdate);
					new Wrapper().update(sqlUpdate);
				} else {
					logger.debug("in side else-------");
					newPeople = newPeople + peopleType[i] + ",";
				}
				logger.debug("newPeople----------" + newPeople);
			}
		} catch (Exception e) {
			logger.error("error in checking existing people " + e.getMessage());
		}
		if (!newPeople.equals("")) {
			newPeople = newPeople.substring(0, newPeople.length() - 1);
		}
		return newPeople;
	}

	public OnlineRegisterFrom getOnlineUsers(String EmailAddr, String obc) {
		logger.info("inside getOnlineUsers " + EmailAddr);
		OnlineRegisterFrom regForm = null;

		try {
			String sql = "";
			if (obc.equals("Y")) {
				sql = "select "
//						+ "case when EU.PHONE IS NOT NULL THEN EU.PHONE WHEN EU.PHONE IS NULL THEN CT.PHONE END AS PHONE," + 
//						"case when EU.PHONE_EXT IS NOT NULL THEN EU.PHONE_EXT WHEN EU.PHONE_EXT IS NULL THEN CT.EXT END AS PHONE_EXT," + 
//						"case when EU.WORK_PHONE IS NOT NULL THEN EU.WORK_PHONE WHEN EU.WORK_PHONE IS NULL THEN CT.WORK_PHONE END AS WORK_PHONE," + 
//						"case when EU.WORK_EXT IS NOT NULL THEN EU.WORK_EXT WHEN EU.WORK_EXT IS NULL THEN CT.WORK_PHONE_EXT END AS WORK_PHONE_EXT,CT.*,"
						+ "EU.*,P.LIC_NO,P.PEOPLE_TYPE_ID,P.LIC_EXP_DT,P.WORKERS_COMP_WAIVE,P.WORK_COMP_EXP_DT from ext_user EU left outer join people P on EU.EXT_USERNAME = P.EMAIL_ADDR "
//						+ "left join cap_temp ct on EU.EXT_USERNAME=CT.EMAIL_ADDR  "
						+ "where lower(EU.EXT_USERNAME) = " + StringUtils.checkString(EmailAddr.toLowerCase());
			} else {
				sql = "select * from ext_user where lower(EXT_USERNAME) = " + StringUtils.checkString(EmailAddr.toLowerCase());
			}
			logger.debug(sql);
			boolean licNbr = false;
			RowSet rs = new Wrapper().select(sql);
			String peopleType = "";
			regForm = new OnlineRegisterFrom();
			while (rs.next()) {
				//regForm.setFirstName((rs.getString("FIRSTNAME") != null ? rs.getString("FIRSTNAME") : "") + " " + (rs.getString("LASTNAME") != null ? rs.getString("LASTNAME") : ""));
				regForm.setFirstName(rs.getString("FIRSTNAME"));
				regForm.setLastName(rs.getString("LASTNAME"));
				regForm.setCompanyName(rs.getString("COMPANYNAME"));
				regForm.setUserId(Integer.parseInt(rs.getString("EXT_USER_ID")));// .substring(0,rs.getString("EXT_USER_ID").indexOf('.'))));
				regForm.setAccountNo(Integer.parseInt(rs.getString("EXT_ACCT_NBR"))); // setting
				if (obc.equals("Y")) {
					peopleType = peopleType + rs.getString("PEOPLE_TYPE_ID") + ",";
					if (!licNbr) {
						if (rs.getString("LIC_NO") != null) {
							regForm.setLicenseNbr(rs.getString("LIC_NO"));
							regForm.setLicenseExpires(rs.getDate("LIC_EXP_DT"));
							regForm.setLicExpDate(StringUtils.cal2str(StringUtils.dbDate2cal(rs.getString("LIC_EXP_DT"))));
							regForm.setWorkersCompensationWaive(rs.getString("WORKERS_COMP_WAIVE"));
							regForm.setStrWorkersCompExpires(StringUtils.cal2str(StringUtils.dbDate2cal(rs.getString("WORK_COMP_EXP_DT"))));
							licNbr = true;
						}
					}
				}
				regForm.setAddress(rs.getString("ADDRESS"));
				regForm.setPhoneNbr(StringUtils.phoneFormat(rs.getString("PHONE")));
				regForm.setEmailAddress(rs.getString("EXT_USERNAME"));
				regForm.setPhoneExt(rs.getString("PHONE_EXT"));
				regForm.setWorkPhone(StringUtils.phoneFormat(rs.getString("WORK_PHONE")));
				regForm.setWorkExt(rs.getString("WORK_EXT"));
				regForm.setFax(StringUtils.phoneFormat(rs.getString("FAX")));
				regForm.setCity(rs.getString("CITY"));
				regForm.setState(rs.getString("STATE"));
				regForm.setZip(rs.getString("ZIP"));
				regForm.setVehicleNo(StringUtils.nullReplaceWithEmpty(rs.getString("VEHICLE_NO")));
				regForm.setDlNo(StringUtils.nullReplaceWithEmpty(rs.getString("LICENSE_NO")));
			}
			if (obc.equals("Y")) {
				if (!peopleType.equals("")) {
					regForm.setPeopleType(peopleType.substring(0, peopleType.length() - 1));
				}
			}
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error while fetchin values :" + e.getMessage());
		}
		return regForm;
	}

	public OnlineRegisterFrom getOnlineUsers(String EmailAddr, String obc,String tempId) {
		logger.info("inside getOnlineUsers " + EmailAddr);
		OnlineRegisterFrom regForm = null;

		try {
			String sql = "";
			if (obc.equals("Y")) {
				sql = "select case when EU.PHONE IS NOT NULL THEN EU.PHONE WHEN EU.PHONE IS NULL THEN CT.PHONE END AS PHONE," + 
						"case when EU.PHONE_EXT IS NOT NULL THEN EU.PHONE_EXT WHEN EU.PHONE_EXT IS NULL THEN CT.EXT END AS PHONE_EXT," + 
						"case when EU.WORK_PHONE IS NOT NULL THEN EU.WORK_PHONE WHEN EU.WORK_PHONE IS NULL THEN CT.WORK_PHONE END AS WORK_PHONE," + 
						"case when EU.CITY IS NOT NULL THEN EU.CITY WHEN EU.CITY IS NULL THEN CT.CITY END AS CITY," + 
						"case when EU.STATE IS NOT NULL THEN EU.STATE WHEN EU.STATE IS NULL THEN CT.STATE END AS STATE," + 
						"case when EU.ZIP IS NOT NULL THEN EU.ZIP WHEN EU.ZIP IS NULL THEN CT.ZIP END AS ZIP," + 
						"case when EU.ADDRESS IS NOT NULL THEN EU.ADDRESS WHEN EU.ADDRESS IS NULL THEN CT.ADDRESS END AS ADDRESS," + 
						"case when EU.WORK_EXT IS NOT NULL THEN EU.WORK_EXT WHEN EU.WORK_EXT IS NULL THEN CT.WORK_PHONE_EXT END AS WORK_EXT,"
						+ "EU.FIRSTNAME,EU.LASTNAME,EU.COMPANYNAME,EU.EXT_USER_ID,EU.EXT_ACCT_NBR,EU.EXT_USERNAME,EU.FAX,EU.VEHICLE_NO,EU.LICENSE_NO,P.LIC_NO,P.PEOPLE_TYPE_ID,P.LIC_EXP_DT,P.WORKERS_COMP_WAIVE,P.WORK_COMP_EXP_DT from cap_temp ct,ext_user EU left outer join people P on EU.EXT_USERNAME = P.EMAIL_ADDR where ct.temp_id="+tempId+" and lower(EU.EXT_USERNAME) = " + StringUtils.checkString(EmailAddr.toLowerCase());
			} else {
				sql = "select * from ext_user where lower(EXT_USERNAME) = " + StringUtils.checkString(EmailAddr.toLowerCase());
			}
			logger.debug(sql);
			boolean licNbr = false;
			RowSet rs = new Wrapper().select(sql);
			String peopleType = "";
			regForm = new OnlineRegisterFrom();
			while (rs.next()) {
				//regForm.setFirstName((rs.getString("FIRSTNAME") != null ? rs.getString("FIRSTNAME") : "") + " " + (rs.getString("LASTNAME") != null ? rs.getString("LASTNAME") : ""));
				regForm.setFirstName(rs.getString("FIRSTNAME"));
				regForm.setLastName(rs.getString("LASTNAME"));
				regForm.setCompanyName(rs.getString("COMPANYNAME"));
				regForm.setUserId(Integer.parseInt(rs.getString("EXT_USER_ID")));// .substring(0,rs.getString("EXT_USER_ID").indexOf('.'))));
				regForm.setAccountNo(Integer.parseInt(rs.getString("EXT_ACCT_NBR"))); // setting
				if (obc.equals("Y")) {
					peopleType = peopleType + rs.getString("PEOPLE_TYPE_ID") + ",";
					if (!licNbr) {
						if (rs.getString("LIC_NO") != null) {
							regForm.setLicenseNbr(rs.getString("LIC_NO"));
							regForm.setLicenseExpires(rs.getDate("LIC_EXP_DT"));
							regForm.setLicExpDate(StringUtils.cal2str(StringUtils.dbDate2cal(rs.getString("LIC_EXP_DT"))));
							regForm.setWorkersCompensationWaive(rs.getString("WORKERS_COMP_WAIVE"));
							regForm.setStrWorkersCompExpires(StringUtils.cal2str(StringUtils.dbDate2cal(rs.getString("WORK_COMP_EXP_DT"))));
							licNbr = true;
						}
					}
				}
				regForm.setAddress(rs.getString("ADDRESS"));
				regForm.setPhoneNbr(StringUtils.phoneFormat(rs.getString("PHONE")));
				regForm.setEmailAddress(rs.getString("EXT_USERNAME"));
				regForm.setPhoneExt(rs.getString("PHONE_EXT"));
				regForm.setWorkPhone(StringUtils.phoneFormat(rs.getString("WORK_PHONE")));
				regForm.setWorkExt(rs.getString("WORK_EXT"));
				regForm.setFax(StringUtils.phoneFormat(rs.getString("FAX")));
				regForm.setCity(rs.getString("CITY"));
				regForm.setState(rs.getString("STATE"));
				regForm.setZip(rs.getString("ZIP"));
				logger.debug(""+regForm.getCity());
				regForm.setVehicleNo(StringUtils.nullReplaceWithEmpty(rs.getString("VEHICLE_NO")));
				regForm.setDlNo(StringUtils.nullReplaceWithEmpty(rs.getString("LICENSE_NO")));
			}
			if (obc.equals("Y")) {
				if (!peopleType.equals("")) {
					regForm.setPeopleType(peopleType.substring(0, peopleType.length() - 1));
				}
			}
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error while fetchin values :" + e.getMessage());
		}
		return regForm;
	}

	public void updateOnlineUser(OnlineRegisterFrom regForm, String email) {
		logger.info("inside updateOnlineUser...");
		try {
			String name = regForm.getFirstName();
			String firstName = regForm.getFirstName();
			String lastName = regForm.getLastName();
			/*int index = 0;
			if ((index = name.indexOf(',')) != -1) {
				lastName = name.substring(0, index);
				firstName = name.substring(index + 1);
			} else if ((index = name.indexOf(' ')) != -1) {
				firstName = name.substring(0, index);
				lastName = name.substring(index + 1);
			}*/

			String sql = "update EXT_USER set ADDRESS = " + StringUtils.checkString(regForm.getAddress()) + ", PHONE =" + StringUtils.checkString(StringUtils.stripPhone(regForm.getPhoneNbr())) + ", WORK_PHONE =" + StringUtils.checkString(StringUtils.stripPhone(regForm.getWorkPhone())) + ", EXT_USERNAME = " + StringUtils.checkString(regForm.getEmailAddress()) + ", PHONE_EXT =" + StringUtils.checkString(regForm.getPhoneExt()) + ", WORK_EXT =" + StringUtils.checkString(regForm.getWorkExt()) + ", CITY =" + StringUtils.checkString(regForm.getCity()) + ", STATE =" + StringUtils.checkString(regForm.getState()) + ", ZIP =" + StringUtils.checkString(regForm.getZip()) + ", FAX =" + StringUtils.checkString(StringUtils.stripPhone(regForm.getFax()));
			if (regForm.getAccountNo() != 0) {
				sql = sql + ", EXT_ACCT_NBR = " + regForm.getAccountNo() + ", DOT=" + StringUtils.checkString((regForm.getDot() != null ? regForm.getDot() : "N"));
			}
			sql = sql + ", OBC=" + StringUtils.checkString((regForm.getObc() != null ? regForm.getObc() : "N")) + ", FIRSTNAME = " + StringUtils.checkString(firstName) + ", LASTNAME = " + StringUtils.checkString(lastName) + ", COMPANYNAME = " + StringUtils.checkString(regForm.getCompanyName()) + ", VEHICLE_NO = " + StringUtils.checkString(regForm.getVehicleNo().toUpperCase()) + ", LICENSE_NO = " + StringUtils.checkString(regForm.getDlNo()) + " where lower(EXT_USERNAME) = " + StringUtils.checkString(email.toLowerCase());
			logger.debug("update query :" + sql);
			new Wrapper().update(sql);
		} catch (Exception e) {
			logger.error("Error while updating ext_user :" + e.getMessage());
		}
	}

	public void updatePeople(String email, OnlineRegisterFrom regForm) {
		logger.info("inside updatePeople...");
		try {

			String name = regForm.getFirstName();
			String firstName = "";
			String lastName = "";
			int index = 0;
			if ((index = name.indexOf(',')) != -1) {
				lastName = name.substring(0, index);
				firstName = name.substring(index + 1);
			} else if ((index = name.indexOf(' ')) != -1) {
				firstName = name.substring(0, index);
				lastName = name.substring(index + 1);
			}

			String sql = "UPDATE PEOPLE SET NAME=";
			sql = sql + StringUtils.checkString(firstName);

			sql = sql + ",LAST_NAME=";
			sql = sql + StringUtils.checkString(lastName);
			
			sql = sql + ",EMAIL_ADDR=";
			sql = sql + StringUtils.checkString(regForm.getEmailAddress());

			sql = sql + ",ADDR=";
			sql = sql + StringUtils.checkString(StringUtils.nullReplaceWithEmpty(StringUtils.handleSingleQuot(regForm.getAddress())));

			sql = sql + ",CITY=";
			sql = sql + StringUtils.checkString(regForm.getCity());

			sql = sql + ",STATE=";
			sql = sql + StringUtils.checkString(regForm.getState());

			sql = sql + ",ZIP=";
			sql = sql + StringUtils.checkString(regForm.getZip());

			sql = sql + ",PHONE=";
			sql = sql + StringUtils.checkString(StringUtils.phoneFormat(regForm.getPhoneNbr()));
			
			sql = sql + ",LIC_NO=" + StringUtils.checkString(regForm.getLicenseNbr());

			sql = sql + ",DL_ID_NO=" + StringUtils.checkString(regForm.getDlNo());

			sql = sql + ",UPDATED=" + "current_timestamp";
			logger.debug("got Updated date : " + "current_timestamp");

			logger.debug("in updating LIC_NO WORKERS_COMP_WAIVE");
			sql = sql + ", LIC_EXP_DT=" + StringUtils.toOracleDate(regForm.getLicExpDate()) + ", WORKERS_COMP_WAIVE=" + StringUtils.checkString(regForm.getWorkersCompensationWaive()) + ", WORK_COMP_EXP_DT=" + StringUtils.toOracleDate(regForm.getStrWorkersCompExpires());
		
			
			sql = sql + " WHERE EMAIL_ADDR=";
			sql = sql + StringUtils.checkString(email);

			logger.debug("update query :" + sql);
			new Wrapper().update(sql);
		} catch (Exception e) {
			logger.error("Error while updating people :" + e.getMessage());
		}
	}

	public void updateDotUser(OnlineRegisterFrom regForm, String email) {
		logger.info("inside updateDotUser...");
		try {
			String name = regForm.getFirstName();
			String firstName = "";
			String lastName = "";
			int index = 0;
			if ((index = name.indexOf(',')) != -1) {
				lastName = name.substring(0, index);
				firstName = name.substring(index + 1);
			} else if ((index = name.indexOf(' ')) != -1) {
				firstName = name.substring(0, index);
				lastName = name.substring(index + 1);
			}

			String sql = "update DOT_PEOPLE set SPROJ_ID = " + regForm.getAccountNo() + ", PHONE_NUMBER =" + StringUtils.checkString(StringUtils.phoneFormat(regForm.getPhoneNbr())) + ", EMAIL = " + StringUtils.checkString(regForm.getEmailAddress()) + ", FIRST_NAME = " + StringUtils.checkString(firstName) + ", LAST_NAME = " + StringUtils.checkString(lastName) + " where lower(EMAIL) = " + StringUtils.checkString(email.toLowerCase());
			logger.debug("update query :" + sql);
			new Wrapper().update(sql);
		} catch (Exception e) {
			logger.error("Error while updating ext_user :" + e.getMessage());
		}
	}

	public void deActivateOnlineUser(String email) {
		logger.info("inside deActivateOnlineUser...");
		try {
			String sql = "update EXT_USER set ACTIVEDOT = 'N' where lower(EXT_USERNAME) = " + StringUtils.checkString(email.toLowerCase());
			logger.debug("update query :" + sql);
			new Wrapper().update(sql);
		} catch (Exception e) {
			logger.error("Error while updating ext_user :" + e.getMessage());
		}
	}

	public void deleteUser(String email, String people_type) {
		logger.info("inside deleteUser...");
		try {
			String sqlDelete = "delete from PEOPLE where lower(email_addr) = " + StringUtils.checkString(email.toLowerCase()) + " and PEOPLE_TYPE_ID NOT IN(" + people_type + ")";
			logger.debug("update query :" + sqlDelete);
			new Wrapper().update(sqlDelete);
		} catch (Exception e) {
			logger.error("Error while updating ext_user :" + e.getMessage());
		}
	}

	public boolean UpdateOnlineOnlyTempPassowrd(String password, String EmailAddr) {
		boolean update = false;

		try {
			EmailAddr = (EmailAddr != null) ? EmailAddr.toLowerCase() : EmailAddr;

			String sql = "UPDATE EXT_USER SET EXT_PASSWORD =" + StringUtils.checkString(password) + " WHERE lower(EXT_USERNAME)='" + EmailAddr + "'";

			int result = new Wrapper().update(sql);

			if (result > 0) {
				update = true;
			}

			logger.debug("People Manager update sql is :" + sql);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return update;
	}

	public int getLandId(int lsoId, String lsoType) {
		int landLsoID = 0;
		try {
			if (lsoType.equals("S")) { // Checking for Lsotype structure to get
				// its landID
				String sql = "select LAND_ID from LAND_STRUCTURE where STRUCTURE_ID = " + lsoId;
				logger.debug("queryString " + sql);
				RowSet rs = new Wrapper().select(sql);
				if (rs.next()) {
					landLsoID = rs.getInt("LAND_ID");
				}
			} else if (lsoType.equals("O")) { // Checking for Lsotype ocupancy
				// to get its landID
				String sql = "select LAND_ID from LAND_STRUCTURE where STRUCTURE_ID = ( select STRUCTURE_ID from STRUCTURE_OCCUPANT where occupancy_id = " + lsoId + ")";
				logger.debug("queryString " + sql);
				RowSet rs = new Wrapper().select(sql);
				if (rs.next()) {
					landLsoID = rs.getInt("LAND_ID");
				}
			}
		} catch (Exception e) {
			logger.error("error while fetching Land Id " + e.getMessage());
		}
		return landLsoID;
	}

	// This function is to update StructureName
	public void updateStructureName(int tempOnlineId, String sName) throws Exception {
		Wrapper db = new Wrapper();
		try {

			String sql = "UPDATE TEMP_ONLINE_LSO_ADDRESS	SET STRUCTURENAME = " + StringUtils.checkString(sName) + " WHERE TEMP_ONLINEID  = " + tempOnlineId;
			logger.debug(" Update Query is " + sql);
			db.insert(sql);
		} catch (Exception e) {
			logger.error("Exception thrown   :" + e.getMessage());
		}
	}

	/*public RowSet getOnlineFeeList(int activityId, String sTypeId) throws Exception {
		logger.info("Entering getFeeList() with id " + activityId);
		logger.info("Entering sTypeId with id " + sTypeId);

		int commercialUse = 2;
		RowSet rs = null;
		String sql = "";
		String useSql;

		try {
			// First get the LAND_USE (Commercial or Residential)
			sql = "SELECT U.LAND_ID FROM (LAND_USAGE U JOIN V_LSO_LAND L ON U.LSO_USE_ID=" + commercialUse + " AND L.LAND_ID=U.LAND_ID) JOIN V_PSA_LIST P ON P.LSO_ID=L.LSO_ID WHERE P.ACT_ID=" + activityId;
			logger.info(sql);
			rs = new Wrapper().select(sql);

			if (rs.next()) {
				useSql = "((f.lso_use is null) or (f.lso_use = " + commercialUse + "))";
			} else {
				useSql = "((f.lso_use is null) or (f.lso_use != " + commercialUse + "))";
			}

			rs = new CachedRowSet();
			sql = "select a.act_id,f.fee_id,f.fee_pc,fee_factor,fee_calc_1,f.fee_calc_2,'of' as feeChecked,f.fee_desc,f.lso_use,f.fee_factor,f.fee_input_fl as input,f.fee_fl_1 as required,f.tax_flag,0.0 as feeUnits,0.0 as feeAmount,0.0 as feePaid,0.0 as feeCredit,0.0 as feeAdjustment,0.0 as bouncedAmt,af.fee_id as activityFeeId,af.fee_units,af.fee_amnt,af.fee_paid,af.fee_credit,af.fee_adjustment,af.bounced_amnt,f.factor, f.fee_subt_Level as subtotalLevel, f.fee_fl_4 as feeFlagFour , f.fee_init  as feeInit from (fee f join activity a on  a.act_type=f.act_type  and fee_calc_1 is not null  and ((fee_fl_4 in ('0','2','4') and (f.fee_creation_dt <= a.permit_fee_date )) or (fee_fl_4 in '1' and (f.fee_creation_dt <= a.plan_chk_fee_date )) or (fee_fl_4 in '3' and (f.fee_creation_dt <= a.development_fee_date ))) and ((fee_fl_4 in ('0','2','4') and (f.fee_expiration_dt >= a.permit_fee_date or f.fee_expiration_dt is null)) or (fee_fl_4 in '1' and (f.fee_expiration_dt >= a.plan_chk_fee_date or f.fee_expiration_dt is null)) or (fee_fl_4 in '3' and (f.fee_expiration_dt >= a.development_fee_date or f.fee_expiration_dt is null))) and a.act_id =" + activityId + ")" + " left outer join ACT_SUBTYPE ast on a.act_id = ast.act_id join FEE_ACTSUBTYPE fa on f.fee_id = fa.fee_id and fa.ACT_SUBTYPE_ID IN " + sTypeId + " left outer join activity_fee af on af.activity_id = a.act_id and af.fee_id=f.fee_id and af.fee_amnt > 0.0" + " where (f.fee_pc in ('P','C','B','X','D'))" + " and " + useSql + " order by f.fee_sequence";
			logger.info(sql);
			rs = new Wrapper().select(sql);

			while (rs.next()) {
				if (rs.getString("activityFeeId") != null) {
					try {
						rs.updateString("feeChecked", "on");
						rs.updateDouble("feeUnits", rs.getDouble("fee_units"));
						rs.updateBigDecimal("feeAmount", rs.getBigDecimal("fee_amnt"));
						rs.updateBigDecimal("feePaid", rs.getBigDecimal("fee_paid"));
						rs.updateBigDecimal("feeCredit", rs.getBigDecimal("fee_credit"));
						rs.updateBigDecimal("feeAdjustment", rs.getBigDecimal("fee_adjustment"));
						rs.updateBigDecimal("bouncedAmt", rs.getBigDecimal("bounced_amnt"));
						rs.updateRow();
					} catch (Exception rsException) {
						logger.error("Error in FinanceAgent getFeeList() :" + rs.getInt("act_id") + ":" + rs.getInt("fee_id") + ":" + rsException.getMessage());
						throw rsException;
					}
				}
			}

			logger.info("Exiting FinanceAgent getFeeList()");

			return rs;
		} catch (Exception e) {
			logger.error("Error in FinanceAgent getFeeList() : sql : " + sql + ":" + e.getMessage());
			throw e;
		}
	}

	// end testing
*/	
	
	public RowSet getOnlineFeeList(int actId,String feeIds) throws Exception {
		int commercialUse = 2;
		RowSet rs = null;
		String sql = "";
		String useSql;

		try {
			// First get the LAND_USE (Commercial or Residential)
			rs = new CachedRowSet();
			sql = "select  a.act_id,f.fee_id,f.fee_pc,fee_factor,fee_calc_1,f.fee_calc_2,'of' as feeChecked, f.fee_desc,f.lso_use,f.fee_factor,f.fee_input_fl as input,f.fee_fl_1 as required,f.tax_flag,0.0 as feeUnits,0.0 as feeAmount,0.0 as feePaid,0.0 as feeCredit,0.0 as feeAdjustment, 0.0 as bouncedAmt,af.fee_id as activityFeeId,af.fee_units,af.fee_amnt,af.fee_paid,af.fee_credit, af.fee_adjustment,af.bounced_amnt,f.factor, f.fee_subt_Level as subtotalLevel, f.fee_fl_4 as feeFlagFour , f.fee_init  as feeInit  from fee f  left outer join activity a on  a.act_type=f.act_type and a.act_id ="+actId+" left outer join activity_fee af on af.activity_id = a.act_id and af.fee_id=f.fee_id where f.fee_id in ("+feeIds+")";
			//sql = "select * from fee f 	left outer join activity a on  a.act_type=f.act_type and a.act_id ="+actId+" left outer join activity_fee af on af.activity_id = a.act_id and af.fee_id=f.fee_id where f.ACT_SUBTYPE_ID ="+subTypeId;
			logger.info(sql);
			rs = new Wrapper().select(sql);

			while (rs.next()) {
				if (rs.getString("activityFeeId") != null) {
					try {
						rs.updateString("feeChecked", "on");
						rs.updateDouble("feeUnits", rs.getDouble("fee_units"));
						rs.updateBigDecimal("feeAmount", rs.getBigDecimal("fee_amnt"));
						rs.updateBigDecimal("feePaid", rs.getBigDecimal("fee_paid"));
						rs.updateBigDecimal("feeCredit", rs.getBigDecimal("fee_credit"));
						rs.updateBigDecimal("feeAdjustment", rs.getBigDecimal("fee_adjustment"));
						rs.updateBigDecimal("bouncedAmt", rs.getBigDecimal("bounced_amnt"));
						rs.updateRow();
					} catch (Exception rsException) {
						logger.error("Error in FinanceAgent getFeeList() :" + rs.getInt("act_id") + ":" + rs.getInt("fee_id") + ":" + rsException.getMessage());
						throw rsException;
					}
				}
			}

			logger.info("Exiting FinanceAgent getFeeList()");

			return rs;
		} catch (Exception e) {
			logger.error("Error in FinanceAgent getFeeList() : sql : " + sql + ":" + e.getMessage());
			throw e;
		}
	}

	public int getSubTypeId(int tempID) throws Exception {
		logger.info("Entering getSubTypeId() with id " + activityId);

		int subTypeId = 0;
		RowSet rs = null;
		String sql = "";

		try {
			//sql = "select ACT_SUBTYPE_ID from TEMP_ONLINE_LSO_ADDRESS  TOLS left outer join LKUP_COMBO_USE_MAPP LCUM on TOLS.USE_MAP_ID=LCUM.USE_MAPID left outer join LKUP_ACT_SUBTYPE LAS on LCUM.SPROJ_NAME=LAS.ACT_SUBTYPE where TEMP_ONLINEID=" + tempID;
			sql = "select USE_MAP_ID from TEMP_ONLINE_LSO_ADDRESS  where TEMP_ONLINEID=" + tempID;
			logger.info(sql);
			rs = new Wrapper().select(sql);

			while (rs.next()) {
				subTypeId = rs.getInt("USE_MAP_ID");
			}
		} catch (Exception e) {
			logger.error("Error in getSubTypeId " + e.getMessage());
		}

		return subTypeId;

	}
	
	public String getFeeIds(int tempID) throws Exception {
		logger.info("Entering getSubTypeId() with id " + activityId);

		String feeIds = "0";
		RowSet rs = null;
		String sql = "";

		try {
			//sql = "select ACT_SUBTYPE_ID from TEMP_ONLINE_LSO_ADDRESS  TOLS left outer join LKUP_COMBO_USE_MAPP LCUM on TOLS.USE_MAP_ID=LCUM.USE_MAPID left outer join LKUP_ACT_SUBTYPE LAS on LCUM.SPROJ_NAME=LAS.ACT_SUBTYPE where TEMP_ONLINEID=" + tempID;
			sql = "select FEE_IDS from TEMP_ONLINE_LSO_ADDRESS  where TEMP_ONLINEID=" + tempID;
			logger.info(sql);
			rs = new Wrapper().select(sql);

			while (rs.next()) {
				feeIds = rs.getString("FEE_IDS");
			}
			
			if(feeIds.indexOf("|")!=-1){
				//feeIds= feeIds.replaceAll("|", ",");
				feeIds= feeIds.replace("|", ",");
			}
		} catch (Exception e) {
			logger.error("Error in getSubTypeId " + e.getMessage());
		}

		return feeIds;

	}

	public String getFeeUnit(String feeId, int tempOnlineID) throws Exception {
		logger.info("Entering getSubTypeId() with id " + activityId);

		String feeUnit = "0";
		RowSet rs = null;
		
		String sql = "";
		String feeInput = "";

		try {
			sql = "select * from fee where fee_id=" + feeId;
			logger.info(sql);
			rs = new Wrapper().select(sql);
			
			while (rs.next()) {
				feeInput = rs.getString("ONLINE_INPUT");
			}

			if (!feeInput.equalsIgnoreCase("")) {
				sql = "select * from TEMP_ONLINE_LSO_ADDRESS where TEMP_ONLINEID=" + tempOnlineID;
				logger.info(sql);
				rs = new Wrapper().select(sql);

				while (rs.next()) {
					if (feeInput.equalsIgnoreCase("1")) {
						feeUnit = rs.getString("SQ_FOOTAGE");
					}else if (feeInput.equalsIgnoreCase("2")) {
						feeUnit = rs.getString("NO_DWELL_UNITS");
					}else if (feeInput.equalsIgnoreCase("3")) {
						feeUnit = rs.getString("NO_FLOORS");
					}
					
				}

			}
			
			if(rs!=null) rs.close();
		} catch (Exception e) {
			logger.error("Error in getSubTypeId " + e.getMessage());
		}

		return feeUnit;

	}

	public String getUName(int userId) throws Exception {
		logger.info("Entering getUName() with id ");

		String uName = "";
		RowSet rs = null;
		String sql = "";

		try {
			sql = "select * from ext_user where EXT_USER_ID=" + userId;
			logger.info(sql);
			rs = new Wrapper().select(sql);

			while (rs.next()) {

				uName = (rs.getString("FIRSTNAME")) + " " + (rs.getString("LASTNAME"));

			}
			logger.info("Entering ************* " + uName);
		} catch (Exception e) {
			logger.error("Error in getchkFeeInput " + e.getMessage());
		}

		return uName;

	}

	public OnlineRegisterFrom getForgotPassword(OnlineRegisterFrom onlineRegisterFrom) throws Exception {
		logger.info("Entering getForgotPassword() with id " + onlineRegisterFrom.getEmailAddress());

		RowSet rs = null;
		String sql = "";
		onlineRegisterFrom.setMessage("INCOMPELTE");
		try {
			sql = "select * from ext_user where EXT_USERNAME='" + onlineRegisterFrom.getEmailAddress() + "'";
			logger.info(sql);
			rs = new Wrapper().select(sql);

			String password = Operator.randomString();
			logger.debug("passwordpasswordpassword :::: " + password);

			if (rs.next()) {
				sql = "update ext_user set EXT_PASSWORD = " + StringUtils.checkString(jcrypt.crypt("X6", password)) + " where EXT_USERNAME='" + onlineRegisterFrom.getEmailAddress() + "'";
				logger.debug(sql);
				new Wrapper().update(sql);
				onlineRegisterFrom.setPwd(password);
				onlineRegisterFrom.setMessage("COMPLETE");

			}
			rs.close();

			logger.info("Entering ************* " + onlineRegisterFrom.getPwd());
		} catch (Exception e) {
			onlineRegisterFrom.setMessage("ERROR" + e.getMessage() + "");
			logger.error("Error in getchkFeeInput " + e.getMessage());
		}

		return onlineRegisterFrom;

	}

	/**
	 * Get Address for LSO Id
	 * 
	 * @param lsoId
	 * @return
	 * @throws Exception
	 */
	public String getAddressidforLsoId(String lsoId) throws Exception {
		logger.info("getAddressidforLsoId(" + lsoId + ")");
		String addrId = "";
		String sql = "select   addr_id, STR_NO,  ( sl.STR_NAME  || ' ' || sl.STR_TYPE  || ' ' || sl.PRE_DIR ) as address   from lso_address la join street_list sl  on  la.STREET_ID = sl.STREET_ID  where lso_id =" + lsoId;
		try {
			RowSet rs = new Wrapper().select(sql);

			if ((rs != null) && rs.next()) {
				addrId = rs.getString("addr_id");
			}
		} catch (Exception e) {
			logger.error("Error occured while getAddressidforLsoId " + e.getMessage());
		}

		return addrId;
	}

	/**
	 * gets list of activity subtypes for "RONPER"
	 * 
	 * @return actSubTypList
	 */
	public List getActSubTypes() {
		logger.info("Entered getActSubTypes ...");
		List actSubTypList = new ArrayList();
		String sql = "select * from LKUP_ACT_SUBTYPE where ACT_TYPE = " + StringUtils.checkString(Constants.ACT_ONLINE_RESIDENTIAL_PERMIT) + " order by ACT_SUBTYPE";
		logger.debug("sql : " + sql);
		try {
			rs = db.select(sql);
			OnlinePermitForm opf = new OnlinePermitForm();
			while (rs.next()) {
				opf = new OnlinePermitForm();
				opf.setActSubName(rs.getString("ACT_SUBTYPE"));
				opf.setActType(rs.getString("ACT_TYPE"));
				opf.setStypeId(rs.getInt("ACT_SUBTYPE_ID"));
				actSubTypList.add(opf);
			}
			rs.close();
		} catch (Exception e) {
			logger.error("error while fetching activity sub types" + e.getMessage());
		}
		return actSubTypList;
	}
	
	/**
	 * gets list of activity subtypes for type
	 * 
	 * @return actSubTypList
	 */
	public List getActSubTypes(String type) {
		logger.info("Entered getActSubTypes ...");
		List actSubTypList = new ArrayList();
		String sql = "select * from LKUP_ACT_SUBTYPE where ACT_TYPE = " + StringUtils.checkString(type) + " order by ACT_SUBTYPE";
		logger.debug("sql : " + sql);
		try {
			rs = db.select(sql);
			OnlinePermitForm opf = new OnlinePermitForm();
			while (rs.next()) {
				opf = new OnlinePermitForm();
				opf.setActSubName(rs.getString("ACT_SUBTYPE"));
				opf.setActType(rs.getString("ACT_TYPE"));
				opf.setStypeId(rs.getInt("ACT_SUBTYPE_ID"));
				actSubTypList.add(opf);
			}
			rs.close();
		} catch (Exception e) {
			logger.error("error while fetching activity sub types" + e.getMessage());
		}
		return actSubTypList;
	}

	/**
	 * gets list of fees which is not mapped for the given activity sub type
	 * 
	 * @param spjrName
	 * @return
	 */
	public List getFeesList(String spjrName,String actType) {
		logger.info("Entered getFeesList ...");
		List actFeesList = new ArrayList();
		String sql = "select distinct FEE_ID, FEE_DESC from FEE  where ACT_TYPE = " + StringUtils.checkString(actType) + " and FEE_ID NOT IN (Select FEE_ID from  FEE where ACT_SUBTYPE_ID = " + spjrName + ")  and FEE_EXPIRATION_DT is null order by FEE_DESC";
		logger.debug("sql : " + sql);
		try {
			rs = db.select(sql);
			OnlinePermitForm opf = new OnlinePermitForm();
			while (rs.next()) {
				opf = new OnlinePermitForm();
				opf.setFeeId(rs.getInt("FEE_ID"));
				opf.setDescription(rs.getString("FEE_DESC"));
				actFeesList.add(opf);
			}
			rs.close();
		} catch (Exception e) {
			logger.error("error while fetching Fees " + e.getMessage());
		}
		return actFeesList;
	}

	/**
	 * gets list of fees which is mapped for the given activity sub type
	 * 
	 * @param spjrName
	 * @return
	 */
	public List getActSubFeeMapping(String spjrName) {
		logger.info("Entered getActSubFeeMapping ...");
		List actSubFeeMap = new ArrayList();
		String sql = "select F.FEE_ID, F.FEE_DESC FROM FEE F  where F.ACT_SUBTYPE_ID = " + spjrName + " order by F.FEE_DESC";
		logger.debug("sql : " + sql);
		try {
			rs = db.select(sql);
			OnlinePermitForm opf = new OnlinePermitForm();
			while (rs.next()) {
				opf = new OnlinePermitForm();
				opf.setFeeId(rs.getInt("FEE_ID"));
				opf.setDescription(rs.getString("FEE_DESC"));
				actSubFeeMap.add(opf);
			}
			rs.close();
		} catch (Exception e) {
			logger.error("error while fetching Subtype and Fees " + e.getMessage());
		}
		return actSubFeeMap;
	}
	
	/**
	 * gets list of fees which is mapped for the given activity sub type
	 * 
	 * @param spjrName
	 * @return
	 */
	public List getQuestionaireList() {
		logger.info("Entered getActSubFeeMapping ...");
		List actSubFeeMap = new ArrayList();
		String sql = "select * FROM LKUP_QUESTIONS  where ACTIVE='Y'";
		logger.debug("sql : " + sql);
		try {
			rs = db.select(sql);
			OnlinePermitForm opf = new OnlinePermitForm();
			while (rs.next()) {
				opf = new OnlinePermitForm();
				opf.setQuestionId(rs.getInt("QUES_ID"));
				opf.setDescription(rs.getString("QUES_DESC"));
				actSubFeeMap.add(opf);
			}
			rs.close();
		} catch (Exception e) {
			logger.error("error while fetching Subtype and Fees " + e.getMessage());
		}
		return actSubFeeMap;
	}

	/**
	 * delets from FEE_ACTSUBTYPE
	 * 
	 * @param styp
	 */
	public void deleteActSubFeeMap(int styp) {
		logger.info("Entered deleteActSubFeeMap ...");
		String sql = "delete from FEE_ACTSUBTYPE where ACT_SUBTYPE_ID = " + styp;
		logger.debug("sql : " + sql);
		try {
			db.update(sql);
		} catch (Exception e) {
			logger.error("error while deleting from FEE_ACTSUBTYPE " + e.getMessage());
		}
	}
	
	public void deleteActSubFeeId(int styp) {
		logger.info("Entered deleteActSubFeeMap ...");
		String sql = "update FEE set ACT_SUBTYPE_ID =0 where ACT_SUBTYPE_ID = " + styp;
		logger.debug("sql : " + sql);
		try {
			db.update(sql);
		} catch (Exception e) {
			logger.error("error while deleting from FEE_ACTSUBTYPE " + e.getMessage());
		}
	}

	/**
	 * inserts into FEE_ACTSUBTYPE
	 * 
	 * @param styp
	 * @param feeId
	 */
	public void saveActSubMap(int styp, String[] feeId) {
		logger.info("Entered saveActSubMap ...");
		String sql = "";
		try {
			for (int i = 0; i < feeId.length; i++) {
				sql = "insert into FEE_ACTSUBTYPE values (" + styp + ", " + feeId[i] + ")";
				logger.debug("sql : " + sql);
				db.insert(sql);
			}
		} catch (Exception e) {
			logger.error("error while inserting into FEE_ACTSUBTYPE " + e.getMessage());
		}
	}
	
	/**
	 * inserts into FEE_ACTSUBTYPE
	 * 
	 * @param styp
	 * @param feeId
	 */
	public void saveFeeActSubId(int styp, String[] feeId) {
		logger.info("Entered saveActSubMap ...");
		String sql = "";
		try {
			for (int i = 0; i < feeId.length; i++) {
				sql = "update FEE set ACT_SUBTYPE_ID =" + styp + " WHERE FEE_ID IN (" + feeId[i] + ")";
				logger.debug("sql : " + sql);
				db.insert(sql);
			}
		} catch (Exception e) {
			logger.error("error while inserting into FEE_ACTSUBTYPE " + e.getMessage());
		}
	}

	public static double getAmount(String actId) {
		logger.info("Entered getAmount ...");
		String sql = "";
		RowSet rs = null;
		double amount = 0.00;
		try {

			sql = "select sum(FEE_AMNT) - sum(fee_paid) as AMT from activity_fee where activity_id=" + actId;
			logger.debug("sql : " + sql);
			rs = new Wrapper().select(sql);

			while (rs.next()) {
				amount = StringUtils.s2d(rs.getString("AMT"));
			}
			logger.debug("amount.. "+amount);
		} catch (Exception e) {
			logger.error("error while getAmount " + e.getMessage());
		}
		return amount;
	}

	public boolean checkVehicleNo(String email) {
        logger.info("inside checkVehicleNo...");
        boolean result = false;
        try {
            String sql = "select * from EXT_USER where lower(EXT_USERNAME) = lower('" + email + "')";
            logger.debug(sql);
            RowSet rs = new Wrapper().select(sql);
            if (rs!=null && rs.next()) {
                               
                if (!(StringUtils.nullReplaceWithEmpty(rs.getString("VEHICLE_NO")).equals("")) && !(StringUtils.nullReplaceWithEmpty(rs.getString("LICENSE_NO")).equals(""))) {
                    result = true;
                }
            }           
        } catch (Exception e) {
            logger.error("error in getting existing online users " + e.getMessage());
        } finally {
            try {
                if (rs != null) rs.close();               
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

	public boolean validateVehicleInfo(String vehicleNo,String licenseNo,String email) {
				logger.info("inside validateVehicleInfo...");
				if(vehicleNo!=null && !vehicleNo.equals("")) {
					vehicleNo = vehicleNo.replaceAll("[^a-zA-Z0-9]", "");         
					logger.info("vehicleNo" + vehicleNo);
				}
				boolean result = false;
				try {
					String sql = "select * from EXT_USER where lower(EXT_USERNAME) not in( lower('" + email + "')) and (lower(VEHICLE_NO)=lower('"+vehicleNo+"') OR lower(LICENSE_NO) =lower('"+licenseNo+"'))";
					logger.info(sql);
					RowSet rs = new Wrapper().select(sql);
					if (rs.next()) {
							result = true;
						
					}
					if (rs != null)
						rs.close();
				} catch (Exception e) {
					logger.error("error in getting existing online users " + e.getMessage());
				}
				return result;
		}
	// lncv permits
	public String processLncvPermit(String actId, User user, String amt, List lncvDtList, PaymentMgrForm paymentMgrForm,String lncvNextYear) throws Exception {
		logger.debug("Enter processLncvPermit ");

		try {
			ActivityAgent activityAgent = new ActivityAgent();
			if (actId.equals("0")) {
				int lsoId = -1;
				int addressId = -1;
				int subProjectTypeId = -1;
				String subProjectName = "255002";
				// get non location id for PW city wide address (hard coded)
				int nonLocationalLsoId = new AddressAgent().getLsoId(1, SaveParkingActivityTabAction.PW_CITYWIDE_STREET_ID);
				if (subProjectName.equalsIgnoreCase(SaveParkingActivityTabAction.LNCV_ID)) {
					lsoId = nonLocationalLsoId;
					addressId = AddressAgent.getLandAddressId(1, SaveParkingActivityTabAction.PW_CITYWIDE_STREET_ID);
					logger.debug("Address Id = " + addressId);
					logger.debug("Non locational address ");
				}
				int activityId = 0;
				// check lncv activityUpdate else normal process

				// check and create project
				ProjectAgent projectAgent = new ProjectAgent();
				int projectId = projectAgent.getProjectId(lsoId, Constants.PROJECT_NAME_PARKING);
				if (projectId == -1) {
					// project does not exist, create a new building project and proceed
					projectId = projectAgent.createProject(Constants.PROJECT_NAME_PARKING, Constants.DEPARTMENT_PARKING, StringUtils.i2s(lsoId), user.getUserId());
					logger.debug("New project created with project Id = " + projectId);
				}

				List<SubProjectType> subProjectTypes = LookupAgent.getSubProjectTypes(Integer.parseInt(subProjectName));
				if (subProjectTypes.size() == 1) {
					logger.debug("Only 1 Sub-Project type was obtained, automatically populating Activity Types");
					subProjectTypeId = ((SubProjectType) subProjectTypes.get(0)).getSubProjectTypeId();
					logger.debug("Sub Project Type Id = " + subProjectTypeId);

				}

				// check and create sub-project
				ProjectAgent subProjectAgent = new ProjectAgent();
				int subProjectId = subProjectAgent.getSubProjectId(projectId, StringUtils.s2i(subProjectName));
				if (subProjectId == -1) {
					// sub-project does not exist, create a new sub-project and proceed
					subProjectId = subProjectAgent.createSubProject(projectId, subProjectName, "" + subProjectTypeId, user.getUserId());
					logger.debug("New sub-project created with sub-project Id = " + subProjectId);
				}

				// create activity

				ActivityForm activityForm = new ActivityForm();
				Calendar cal = Calendar.getInstance();
				activityForm.setIssueDate(StringUtils.cal2str(cal));
				activityForm.setStartDate(StringUtils.cal2str(cal));

				cal.add(Calendar.YEAR, 1);
				cal.set(Calendar.DAY_OF_YEAR, 1);
				cal.add(Calendar.DATE, -1); // last day of the year.
				String s = (StringUtils.cal2str(cal));
				activityForm.setExpirationDate(s);
				
				activityForm.setActivityType("PKNCOM");
				activityForm.setActivityStatus("211000");
				
				if(lncvNextYear.equalsIgnoreCase("Y")){
					OBCTimekeeper j = new OBCTimekeeper();
					OBCTimekeeper d = new OBCTimekeeper();
					j.setMonth(1);
					j.setDay(1);
					j.addYear(1);
					d.setMonth(12);
					d.setDay(31);
					d.addYear(1);
					activityForm.setStartDate(j.getString("MM/DD/YYYY"));
					activityForm.setExpirationDate(d.getString("MM/DD/YYYY"));
					
				}

				String description = " ";  

				activityId = activityAgent.createParkingActivity(subProjectId, addressId, activityForm.getActivityType(), description, StringUtils.$2dbl(activityForm.getValuation()), StringUtils.b2s(activityForm.getPlanCheckRequired()), activityForm.getActivityStatus(), activityForm.getStartDate(), activityForm.getExpirationDate(), user.getUserId(), activityForm.getPlanNo(), activityForm.getSubDivision(), activityForm.getIssueDate(), "1", activityForm.getParkingZone(), activityForm.getLabel());
				rollBackTransaction(activityId);
				logger.debug("New Parking Activity created with activity id = " + activityId);

				// insert lncv
				// for print approvals if actId is 0
				actId = StringUtils.i2s(activityId);

				logger.debug("******----------------------*************"+ user.getUsername());
				// insert people,activity_fee,payment,payment_details
				if (!user.getUsername().equalsIgnoreCase("")) {
					int peopleId = insertPeopleForParkingActivity(activityId, user.getUsername());
					int feeId = getFeeIdForLncv(activityForm.getActivityType());
					updatePeopleInfo(activityId, user.getUsername());
					// temponlineId is used for payfull lncv permits flag
					insertActivity_fee(activityId, peopleId, feeId, lncvDtList.size(), paymentMgrForm.getAmount(), paymentMgrForm.getTempOnlineID());
					processInternalPayment(paymentMgrForm, StringUtils.i2s(activityId), user);
				}

				// insert parking_activity

				if (lncvDtList.size() > 0) {
					 int pblock = activityAgent.getPBlock(activityId);
					for (int i = 0; i < lncvDtList.size(); i++) {
						String lncvdt = ((People) lncvDtList.get(i)).getDate();
						if (!lncvdt.equalsIgnoreCase("")) {
							//((People) lncvDtList.get(i)).getVersionNumber()
							activityAgent.insertPkActivity(activityId, lncvdt, pblock);
						}
					}
				}

			}// end if actid >0
			else {
				rollBackTransaction(Integer.parseInt(actId));   
				updatePeopleInfo(StringUtils.s2i(actId), user.getUsername());
				updateActivity_fee(StringUtils.s2i(actId), lncvDtList.size(), paymentMgrForm.getAmount(),user.getUsername());
				processInternalPayment(paymentMgrForm, actId, user);
				if (lncvDtList.size() > 0) {
					 int pblock = activityAgent.getPBlock(StringUtils.s2i(actId));

					for (int i = 0; i < lncvDtList.size(); i++) {
						String lncvdt = ((People) lncvDtList.get(i)).getDate();
						if (!lncvdt.equalsIgnoreCase("")) {
							//((People) lncvDtList.get(i)).getVersionNumber()
							activityAgent.insertPkActivity(StringUtils.s2i(actId), lncvdt, pblock);
						}
					}
				}

			}

		} catch (Exception e) {
			logger.error("Error in processLncvPermit" + e.getMessage());
			e.printStackTrace();
		}
		return actId;
	}

	public int getFeeIdForLncv(String actType) throws Exception {
		int feeId = 0;

		String sql = "select * from fee where act_type ='" + actType + "' and  FEE_CREATION_DT <= current_timestamp  and ( fee_expiration_dt >= current_timestamp or fee_expiration_dt is null or lower(fee_desc) like '%lncv fees%')";
		logger.debug(sql);
		RowSet rs = new Wrapper().select(sql);
		if (rs.next()) {
			feeId = rs.getInt("FEE_ID");
		}
		if (rs != null)
			rs.close();

		return feeId;
	}

	public int insertPeopleForParkingActivity(int actId, String userName) throws Exception {
		int peopleId = 0;

		String sql = "select * from PEOPLE where lower(EMAIL_ADDR) = lower('" + userName + "')";
		logger.debug(sql);
		RowSet rs = new Wrapper().select(sql);
		if (rs.next()) {
			peopleId = rs.getInt("PEOPLE_ID");
		}
		if (rs != null)
			rs.close();

		if (peopleId > 0) {
			Wrapper db = new Wrapper();
			String sql1 = "INSERT INTO ACTIVITY_PEOPLE (ACT_ID,PEOPLE_ID,PSA_TYPE) VALUES (" + actId + "," + peopleId + ",'A')";
			logger.info(sql1);
			db.insert(sql1);
		}

		return peopleId;
	}

	public void insertActivity_fee(int actId, int peopleId, int feeId, int feeUnit, String feeAmt, String payfull) throws Exception {
		Wrapper db = new Wrapper();
		if (payfull.equals("Y")) {
			feeUnit = 96;
		}
		String sql = "insert into activity_fee(activity_id,fee_id,people_id,fee_units,fee_amnt) values(" + actId + "," + feeId + "," + peopleId + "," + feeUnit + "," + feeAmt.substring(1, feeAmt.length()) + ")";
		logger.debug(sql);
		//db.insert(sql);
		int queryNextId=0;
		String sqlIns="";

		queryNextId = db.getNextId("QUERY_TRANS_ID");
	    sqlIns ="INSERT INTO QUERY_TRANSACTION VALUES("+queryNextId+","+actId+",'"+Operator.sqlEscape(sql)+"','Y')";
		db.update(sqlIns);

		
	}

	public void updateActivity_fee(int actId, int feeUnit, String feeAmt, String username) throws Exception {
		logger.debug("updateActivity_fee" + feeUnit + "feeamt" + feeAmt+" username"+username);
		String sql = "select af.* from ACTIVITY_FEE  af join fee f on af.fee_id=f.fee_id where f.READY_TO_PAY='N' and ACTIVITY_ID = " + actId;
		logger.debug(sql);
		int queryNextId=0;
		String sqlIns="";
		RowSet rs = new Wrapper().select(sql);
		if (rs.next()) {
			feeUnit = feeUnit + rs.getInt("FEE_UNITS");
			logger.debug("updateActivity_fee" + feeUnit + "rs" + rs.getInt("FEE_AMNT"));
			int feeamount = rs.getInt("FEE_AMNT") + StringUtils.s2i(feeAmt.substring(1, feeAmt.length()));
			sql = "update activity_fee set fee_units = " + feeUnit + ",fee_amnt = " + feeamount + " where activity_id =" + actId;
			logger.debug(sql);
			//db.update(sql);
			queryNextId = db.getNextId("QUERY_TRANS_ID");
		    sqlIns ="INSERT INTO QUERY_TRANSACTION VALUES("+queryNextId+","+actId+",'"+Operator.sqlEscape(sql)+"','Y')";
			db.update(sqlIns);
		}
		     
		if(queryNextId <=0) {
			int peopleId = 0;
			//insertPeopleForParkingActivity(activityId, username);
			 sql = "select * from PEOPLE where lower(EMAIL_ADDR) = lower('" + username + "')";
			logger.debug(sql);
			RowSet rsPeople = new Wrapper().select(sql);
			if (rsPeople.next()) {
				peopleId = rsPeople.getInt("PEOPLE_ID");
			}
			if (rsPeople != null)
				rsPeople.close();
			int feeId = getFeeIdForLncv(LookupAgent.getActivityTypeForActId(actId).getType());
			
			insertActivity_fee(actId, peopleId, feeId, feeUnit, feeAmt, "N");
		}
		if (rs != null)
			rs.close();

	}

	public int getActId(String email) {
		logger.info("inside getActId...");
		int actId = 0;
		try {
			String sql = "select distinct(pa.act_id) from people p left outer join activity_people ap on p.people_id=ap.people_id left outer join PARKING_ACTIVITY pa on ap.act_id=pa.act_id left outer join ACTIVITY a on ap.act_id=a.act_id where lower(EMAIL_ADDR) = lower('" + email + "') AND EXTRACT( YEAR FROM LNC_DT) = EXTRACT(YEAR FROM sysdate) AND EXTRACT( YEAR FROM START_DATE) = EXTRACT(YEAR FROM sysdate) order by pa.act_id desc ";
			logger.debug(sql);
			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				actId = rs.getInt("ACT_ID");

			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error("error in getting existing online users " + e.getMessage());
		}
		return actId;
	}
	
	public int getActIdByLabel(String email) {
		logger.info("inside getActIdByLabel...");
		int actId = 0;
		String vehicleNo="";
		try {
			
			String sql = "select VEHICLE_NO from EXT_USER  where lower(EXT_USERNAME) = lower('" + email + "') ";
			logger.debug(sql);
			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				vehicleNo = rs.getString("VEHICLE_NO");

			}
			if (rs != null)
				rs.close();
			
			sql = "select max(act_id) as act_id from activity where  lower(LABEL) = lower('" + vehicleNo + "') AND STATUS="+Constants.ACTIVITY_STATUS_PARKING_ISSUED+" order by act_id desc";
			logger.debug(sql);
			rs = new Wrapper().select(sql);
			if (rs.next()) {
				actId = rs.getInt("ACT_ID");

			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error("error in getting existing online users " + e.getMessage());
		}
		return actId;
	}

	public int getFeeUnits(int actId) {
		logger.info("inside getFeeUnits...");
		int feeUnits = 0;
		try {
			String sql = "select FEE_UNITS from activity_fee where activity_id=" + actId;
			logger.debug(sql);
			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				feeUnits = rs.getInt("FEE_UNITS");
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error("error in getting existing online users " + e.getMessage());
		}
		return feeUnits;
	}

	public boolean payLess48Hrs(int actId) {
		logger.info("inside payLess48Hrs...");
		boolean result = false;
		int pymntId = 0;
		try {

			String sql1 = " select * from payment_detail where act_id = " + actId + " AND  EXTRACT( YEAR FROM PYMNT_DT) = EXTRACT(YEAR FROM sysdate)";
			logger.debug(sql1);
			RowSet rs1 = new Wrapper().select(sql1);
			if (rs1.next()) {
				pymntId = rs1.getInt("PYMNT_ID");
			}
			if (rs1 != null)
				rs1.close();

			if (pymntId > 0) {
				Date d = new Date();
				String sql = " select * from payment_detail pd left outer join payment p on pd.pymnt_id=p.pymnt_id where pd.act_id = " + actId + " and PYMNT_DT <=  current_date  and PYMNT_DT >=  current_date -2  and pd.PYMNT_ID =" + pymntId;
				logger.debug(sql);
				RowSet rs = new Wrapper().select(sql);
				if (rs.next()) {
					result = true;

				}

				if (rs != null)
					rs.close();
			}
		} catch (Exception e) {
			logger.error("error in getting existing payLess48Hrs  " + e.getMessage());
		}
		return result;
	}

	public String checkExisitingDates(int actId) {
		logger.info("inside checkExisitingDates...");
		String result = "";
		boolean check = false;

		try {
			String sql = "select * from PARKING_ACTIVITY where ACT_ID=" + actId + " AND  EXTRACT( YEAR FROM LNC_DT) = EXTRACT(YEAR FROM sysdate) ";
			logger.debug(sql);

			Set s = new HashSet();
			RowSet rs = new Wrapper().select(sql);
			while (rs.next()) {
				result += "'" + StringUtils.date2str(rs.getDate("LNC_DT")) + "',";
				result += getMaxDtPblock(actId, rs.getInt("PBLOCK"));
				result += getMaxDtPblockPreDates(actId, rs.getInt("PBLOCK"));
			}

			if (result.endsWith(",")) {
				result = result.substring(0, result.length() - 1);
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("error in getting existing online checkExisitingDates " + e.getMessage());
		}
		return result;
	}

	public String getMaxDtPblock(int actId, int pblock) {
		logger.info("inside getMaxDtPblock...");
		String result = "";

		try {
			String sql = "select max(LNC_DT) as LNC_DT from parking_activity where pblock=" + pblock + " and act_id=" + actId + " AND  EXTRACT( YEAR FROM LNC_DT) = EXTRACT(YEAR FROM sysdate) ";
			logger.debug(sql);
			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				Date d = rs.getDate("LNC_DT");
				Calendar c = Calendar.getInstance();
				c = StringUtils.str2cal(StringUtils.date2str(d));
				c.add(Calendar.DATE, 1);
				result += "'" + new SimpleDateFormat("MM/dd/yyyy").format(StringUtils.cal2SQLDate(c)) + "',";
				logger.debug("DAtes33***********" + new SimpleDateFormat("MM/dd/yyyy").format(StringUtils.cal2SQLDate(c)));
				c.add(Calendar.DATE, 1);
				logger.debug("DAtes 44***********" + new SimpleDateFormat("MM/dd/yyyy").format(StringUtils.cal2SQLDate(c)));
				result += "'" + new SimpleDateFormat("MM/dd/yyyy").format(StringUtils.cal2SQLDate(c)) + "',";
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("error in getting  online getMaxDtPblock " + e.getMessage());
		}
		return result;
	}

	public String getMaxDtPblockPreDates(int actId, int pblock) {
		logger.info("inside getMaxDtPblock...");
		String result = "";

		try {
			String sql = "select min(LNC_DT) as LNC_DT from parking_activity where pblock=" + pblock + " and act_id=" + actId + " AND  EXTRACT( YEAR FROM LNC_DT) = EXTRACT(YEAR FROM sysdate) ";
			logger.debug(sql);
			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				Date d = rs.getDate("LNC_DT");
				Calendar c = Calendar.getInstance();
				logger.debug("DAtes%%%%%FINAL *****" + d);
				c = StringUtils.str2cal(StringUtils.date2str(d));
				c.add(Calendar.DATE, -1);
				result += "'" + new SimpleDateFormat("MM/dd/yyyy").format(StringUtils.cal2SQLDate(c)) + "',";
				logger.debug("DAtes66***********" + new SimpleDateFormat("MM/dd/yyyy").format(StringUtils.cal2SQLDate(c)));
				c.add(Calendar.DATE, -1);
				logger.debug("DAtes 77***********" + new SimpleDateFormat("MM/dd/yyyy").format(StringUtils.cal2SQLDate(c)));
				result += "'" + new SimpleDateFormat("MM/dd/yyyy").format(StringUtils.cal2SQLDate(c)) + "',";
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("error in getting  online getMaxDtPblock " + e.getMessage());
		}
		return result;
	}

	public void updatePeopleInfo(int actId, String email) throws Exception {

		String sql = "select * from EXT_USER where EXT_USERNAME = " + StringUtils.checkString(email);
		logger.debug(sql);
		RowSet rs = new Wrapper().select(sql);
		if (rs.next()) {
			sql = "update people set DL_ID_NO = " + StringUtils.checkString(rs.getString("LICENSE_NO")) + " where EMAIL_ADDR =" + StringUtils.checkString(email);
			logger.debug(sql);
			db.update(sql);
			sql = "update activity set LABEL = " + StringUtils.checkString(rs.getString("VEHICLE_NO")) + " where act_id =" + actId;
			logger.debug(sql);
			db.update(sql);
		}
		if (rs != null)
			rs.close();

	}

	public boolean getIsOnlineUser(int userId) {
		logger.info("inside getIsOnlineUser...");
		boolean online = false;
		try {
			String sql = "select * from EXT_USER where EXT_USER_ID=" + userId;
			logger.debug(sql);
			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				online = true;
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error("error in getting existing online users " + e.getMessage());
		}
		return online;
	}

	public int getActivityCreatedBy(int actId) {
		logger.info("inside getActivityCreatedBy...");
		int online = 0;
		try {
			String sql = "select * from ACTIVITY where ACT_ID=" + actId;
			logger.debug(sql);
			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				if (getIsOnlineUser(rs.getInt("CREATED_BY"))) {
					online = rs.getInt("CREATED_BY");
				}
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error("error in getting existing online users " + e.getMessage());
		}
		return online;
	}

	/**
	 * set combo name
	 * 
	 * @param tempOnlineID
	 * @param useMapId
	 * @throws Exception
	 */
	public String getlkupFee(String tempId, String useMapId, String fb) throws Exception {
		Wrapper db = new Wrapper();
		StringBuffer sb = new StringBuffer();
		try {
			if (fb.equalsIgnoreCase("1")) {
				//Added FEE_EXPIRATION_DT IS NULL by Manjuprasad to fix issue EPALS-381,EPALS-361
				String sql = "select * from FEE where FEE_EXPIRATION_DT IS NULL AND ACT_SUBTYPE_ID=" + useMapId;
				logger.debug(sql);
				RowSet rs = new Wrapper().select(sql);
				while (rs.next()) { 
					sb.append("<input type=\"checkbox\" name=\"fee_name\" id=\"fee_name\" value=\"").append(rs.getString("FEE_ID")).append("\">").append(rs.getString("FEE_DESC")).append("<br>");
				}
				
				
				if (rs != null)
					rs.close();
			} else {
				String sql = "select F.FEE_ID,F.FEE_DESC,T.FEE_IDS from TEMP_ONLINE_LSO_ADDRESS T LEFT OUTER JOIN FEE F on F.ACT_SUBTYPE_ID=T.USE_MAP_ID where T.TEMP_ONLINEID=" + tempId;
				logger.debug(sql);
				RowSet rs = new Wrapper().select(sql);
				while (rs.next()) {
					String check = "";
					if (rs.getString("FEE_IDS").indexOf(rs.getString("FEE_ID")) != -1) {
						check = "checked=\"checked\"";
					}
					sb.append("<input type=\"checkbox\" name=\"fee_name\" id=\"fee_name\" ").append(check).append(" value=\"").append(rs.getString("FEE_ID")).append("\">").append(rs.getString("FEE_DESC")).append("<br>");
				}
				if (rs != null)
					rs.close();
			}
		} catch (Exception e) {
			logger.error("Exception thrown while trying to add online Permit  :" + e.getMessage());
		}
		return sb.toString();
	}

	public void setFeeNames(int tempOnlineID, String feeIds,String stepUrl) throws Exception {
		Wrapper db = new Wrapper();

		try {
			String sql = "";
			if (feeIds.endsWith("|")) {
				feeIds = feeIds.substring(0, feeIds.length() - 1);
			}
			sql = "UPDATE TEMP_ONLINE_LSO_ADDRESS	SET FEE_IDS = '" + feeIds + "', STEP_URL=" + StringUtils.checkString(stepUrl) +" WHERE TEMP_ONLINEID  = " + tempOnlineID;
			logger.debug(" Update Query is " + sql);

			db.insert(sql);

			logger.debug(sql);
		} catch (Exception e) {
			logger.error("Exception thrown while trying to add online Permit  :" + e.getMessage());
		}
	}
	
	/**
	 * Update activity name and description
	 * 
	 * @param tempOnlineId
	 * @param sName
	 * @param activityType
	 * @throws Exception
	 */
	public void updateActivityNameAndType(int tempOnlineId, String sName, String activityType,String stepUrl) throws Exception {
		logger.info("updateActivityNameAndType(" + tempOnlineId + ", " + sName + ", " + activityType + ")");

		Wrapper db = new Wrapper();
		try {

			String sql = "UPDATE TEMP_ONLINE_LSO_ADDRESS SET STRUCTURENAME = " + StringUtils.checkString(sName) + " , ACT_TYPE=" + StringUtils.checkString(activityType) + " , STEP_URL=" + StringUtils.checkString(stepUrl) +" WHERE TEMP_ONLINEID  = " + tempOnlineId;
			logger.info(sql);
			db.insert(sql);
		} catch (Exception e) {
			logger.error("Exception thrown   :" + e.getMessage());
		}
	}
	
	/**
	 * Update people
	 * 
	 * @param tempOnlineId
	 * @param peopleType
	 * @param peopleId
	 * @return
	 * @throws Exception
	 */
	public int updatePeopleId(int tempOnlineId, String peopleType, int peopleId) throws Exception {
		logger.info("updatePeopleId(" + tempOnlineId + ", " + peopleType + ", " + peopleId + ")");

		Wrapper db = new Wrapper();

		try {

			String sql = "";
			if (peopleType.equalsIgnoreCase("C")) {// contractor
				sql = "UPDATE TEMP_ONLINE_LSO_ADDRESS SET C_PEOPLE_ID=" + peopleId + " WHERE TEMP_ONLINEID  = " + tempOnlineId + " ";
			}
			if (peopleType.equalsIgnoreCase("W")) {// contractor
				sql = "UPDATE TEMP_ONLINE_LSO_ADDRESS SET W_PEOPLE_ID=" + peopleId + " WHERE TEMP_ONLINEID  = " + tempOnlineId + " ";
			}

			logger.info(sql);
			db.update(sql);

		} catch (Exception e) {
			logger.error("Exception updatePeopleId - Error  : " + e.getMessage());
		}

		return tempOnlineId;
	}
	
	
	public String getFeeList(String tempId, String useMapId, String fb) throws Exception {
		Wrapper db = new Wrapper();
		StringBuffer sb = new StringBuffer();
		try {
			if (fb.equalsIgnoreCase("1")) {
				String sql = "select * from FEE where ACT_SUBTYPE_ID=" + useMapId;
				logger.debug(sql);
				RowSet rs = new Wrapper().select(sql);
				while (rs.next()) {
					sb.append("<input type=\"checkbox\" name=\"fee_name\" id=\"fee_name\" value=\"").append(rs.getString("FEE_ID")).append("\">").append(rs.getString("FEE_DESC")).append("<br>");
				}
				if (rs != null)
					rs.close();
			} else {
				String sql = "select F.FEE_ID,F.FEE_DESC,T.FEE_IDS from TEMP_ONLINE_LSO_ADDRESS T LEFT OUTER JOIN FEE F on F.ACT_SUBTYPE_ID=T.USE_MAP_ID where T.TEMP_ONLINEID=" + tempId;
				logger.debug(sql);
				RowSet rs = new Wrapper().select(sql);
				while (rs.next()) {
					String check = "";
					if (rs.getString("FEE_IDS").indexOf(rs.getString("FEE_ID")) != -1) {
						check = "checked=\"checked\"";
					}
					sb.append("<input type=\"checkbox\" name=\"fee_name\" id=\"fee_name\" ").append(check).append(" value=\"").append(rs.getString("FEE_ID")).append("\">").append(rs.getString("FEE_DESC")).append("<br>");
				}
				if (rs != null)
					rs.close();
			}
		} catch (Exception e) {
			logger.error("Exception thrown while trying to add online Permit  :" + e.getMessage());
		}
		return sb.toString();
	}
	
	
//next year 
	public int getActNextYearId(String email) {
		logger.info("inside checkVehicleNo...");
		int actId = 0;
		try {
			String sql = "select distinct(pa.act_id) from people p left outer join activity_people ap on p.people_id=ap.people_id left outer join PARKING_ACTIVITY pa on ap.act_id=pa.act_id left outer join ACTIVITY a on ap.act_id=a.act_id where lower(EMAIL_ADDR) = lower('" + email + "') AND EXTRACT( YEAR FROM LNC_DT) = EXTRACT(YEAR FROM sysdate + interval '1' year)  AND EXTRACT( YEAR FROM START_DATE) = EXTRACT(YEAR FROM sysdate + interval '1' year)  order by pa.act_id desc ";
			logger.debug(sql);
			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				actId = rs.getInt("ACT_ID");

			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error("error in getting existing online users " + e.getMessage());
		}
		return actId;
	}
	public String checkExisitingDatesNextYear(int actId) {
		logger.info("inside checkExisitingDates...");
		String result = "";
		boolean check = false;

		try {
			String sql = "select * from PARKING_ACTIVITY where ACT_ID=" + actId + " AND  EXTRACT( YEAR FROM LNC_DT) = EXTRACT(YEAR FROM sysdate + interval '1' year) ";
			logger.debug(sql);

			Set s = new HashSet();
			RowSet rs = new Wrapper().select(sql);
			while (rs.next()) {
				result += "'" + StringUtils.date2str(rs.getDate("LNC_DT")) + "',";
				result += getMaxDtPblockNextYear(actId, rs.getInt("PBLOCK"));
				result += getMaxDtPblockPreDatesNextYear(actId, rs.getInt("PBLOCK"));
			}

			if (result.endsWith(",")) {
				result = result.substring(0, result.length() - 1);
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("error in getting existing online checkExisitingDates " + e.getMessage());
		}
		return result;
	}	

	public String getMaxDtPblockNextYear(int actId, int pblock) {
		logger.info("inside getMaxDtPblock...");
		String result = "";

		try {
			String sql = "select max(LNC_DT) as LNC_DT from parking_activity where pblock=" + pblock + " and act_id=" + actId + " AND  EXTRACT( YEAR FROM LNC_DT) = EXTRACT(YEAR FROM sysdate + interval '1' year) ";
			logger.debug(sql);
			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				Date d = rs.getDate("LNC_DT");
				Calendar c = Calendar.getInstance();
				c = StringUtils.str2cal(StringUtils.date2str(d));
				c.add(Calendar.DATE, 1);
				result += "'" + new SimpleDateFormat("MM/dd/yyyy").format(StringUtils.cal2SQLDate(c)) + "',";
				logger.debug("DAtes33***********" + new SimpleDateFormat("MM/dd/yyyy").format(StringUtils.cal2SQLDate(c)));
				c.add(Calendar.DATE, 1);
				logger.debug("DAtes 44***********" + new SimpleDateFormat("MM/dd/yyyy").format(StringUtils.cal2SQLDate(c)));
				result += "'" + new SimpleDateFormat("MM/dd/yyyy").format(StringUtils.cal2SQLDate(c)) + "',";
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("error in getting  online getMaxDtPblock " + e.getMessage());
		}
		return result;
	}	

	public String getMaxDtPblockPreDatesNextYear(int actId, int pblock) {
		logger.info("inside getMaxDtPblock...");
		String result = "";

		try {
			String sql = "select min(LNC_DT) as LNC_DT from parking_activity where pblock=" + pblock + " and act_id=" + actId + " AND  EXTRACT( YEAR FROM LNC_DT) = EXTRACT(YEAR FROM sysdate + interval '1' year) ";
			logger.debug(sql);
			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				Date d = rs.getDate("LNC_DT");
				Calendar c = Calendar.getInstance();
				logger.debug("DAtes%%%%%FINAL *****" + d);
				c = StringUtils.str2cal(StringUtils.date2str(d));
				c.add(Calendar.DATE, -1);
				result += "'" + new SimpleDateFormat("MM/dd/yyyy").format(StringUtils.cal2SQLDate(c)) + "',";
				logger.debug("DAtes66***********" + new SimpleDateFormat("MM/dd/yyyy").format(StringUtils.cal2SQLDate(c)));
				c.add(Calendar.DATE, -1);
				logger.debug("DAtes 77***********" + new SimpleDateFormat("MM/dd/yyyy").format(StringUtils.cal2SQLDate(c)));
				result += "'" + new SimpleDateFormat("MM/dd/yyyy").format(StringUtils.cal2SQLDate(c)) + "',";
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("error in getting  online getMaxDtPblock " + e.getMessage());
		}
		return result;
	}
	
	public void saveLkupQuestionaire(String type,int styp, int feeId,int questionId,String planCheck) {
		logger.info("Entered saveLkupQuestionaire ...");
		String sql = "";
		int newQuestionId = getMaxId("LKUP_QUESTIONAIRE", "QUESTION_ID");
		int sortOrder = newQuestionId;
		try {
		
				sql = "insert into LKUP_QUESTIONAIRE (QUESTION_ID,ACT_TYPE,ACT_SUBTYPE_ID,FEE_ID,QUES_ID,PC_REQ,RES_PERMITS,RES_ADDL_INFO,SORT_ORDER) values ( "+newQuestionId+ "," + StringUtils.checkString(type) + "," + styp + ", " + feeId + "," + questionId +", " + StringUtils.checkString(planCheck) + ", " + StringUtils.checkString("Y") + ", " + StringUtils.checkString("Y") + ", " + sortOrder + ")";
				logger.debug("sql : " + sql);
				db.insert(sql);
		
		} catch (Exception e) {
			logger.error("error while inserting into LKUP_QUESTIONAIRE " + e.getMessage());
		}
	}
	
	/**
	 * gets list of fees which is mapped for the given activity sub type
	 * 
	 * @param spjrName
	 * @return
	 */
	public List getLkupQuestionaire(String type,int styp, int feeId,int questionId) {
		logger.info("Entered getLkupQuestionaire ...");
		List actSubFeeMap = new ArrayList();
		StringBuffer sb = new StringBuffer();
		sb.append(" select LQ.*,F.FEE_DESC AS FEE_DESC,LAT.DESCRIPTION as ACTIVITY_TYPE,LAS.ACT_SUBTYPE as ACTIVITY_SUBTYPE,LQU.QUES_DESC as QUESTION  FROM LKUP_QUESTIONAIRE LQ ");
		sb.append(" LEFT OUTER JOIN FEE F on LQ.FEE_ID=F.FEE_ID ");
		sb.append(" LEFT OUTER JOIN LKUP_ACT_SUBTYPE LAS on LQ.ACT_SUBTYPE_ID=LAS.ACT_SUBTYPE_ID ");
		sb.append(" LEFT OUTER JOIN LKUP_ACT_TYPE LAT on LQ.ACT_TYPE=LAT.TYPE ");
		sb.append(" LEFT OUTER JOIN LKUP_QUESTIONS LQU on LQ.QUES_ID=LQU.QUES_ID ");
		sb.append(" where LQ.QUESTION_ID > 0 ");
		if(!type.equalsIgnoreCase("")){
			sb.append(" AND LQ.ACT_TYPE = ").append(StringUtils.checkString(type));
		}
		if(styp >0){
			sb.append(" AND LQ.ACT_SUBTYPE_ID = ").append(styp);
		}
		if(feeId >0){
			sb.append(" AND LQ.FEE_ID = ").append(feeId);
		}
		if(questionId >0){
			sb.append(" AND LQ.QUES_ID = ").append(questionId);
		}
		sb.append(" order by LQ.SORT_ORDER ");
		logger.debug("sql : " + sb.toString());
		try {
			rs = db.select(sb.toString());
			OnlinePermitForm opf = new OnlinePermitForm();
			while (rs.next()) {
				opf = new OnlinePermitForm();
				opf.setLkId(rs.getInt("QUESTION_ID"));
				opf.setActCode(rs.getString("FEE_DESC"));
				opf.setActSubName(rs.getString("ACTIVITY_SUBTYPE"));
				opf.setDescription(rs.getString("QUESTION"));
				opf.setActType(rs.getString("ACTIVITY_TYPE"));
				opf.setPlanCheck(rs.getString("PC_REQ"));
				
				actSubFeeMap.add(opf);
			}
			rs.close();
		} catch (Exception e) {
			logger.error("error while fetching Subtype and Fees " + e.getMessage());
		}
		return actSubFeeMap;
	}
	
	public int getMaxId(String table,String field) {
		logger.info("inside getMaxId...");
		int actId = 0;
		try {
			String sql = "select MAX("+field+") + 1 as ID from "+table+"";
			logger.debug(sql);
			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				actId = rs.getInt("ID");

			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error("error in getting existing online users " + e.getMessage());
		}
		return actId;
	}
	
	public void deleteQuestionaireId(int id) {
		logger.info("Entered deleteQuestionaireId ...");
		String sql = "delete from LKUP_QUESTIONAIRE WHERE  QUESTION_ID = " + id;
		logger.debug("sql : " + sql);
		try {
			db.update(sql);
		} catch (Exception e) {
			logger.error("error while deleting from deleteQuestionaireId " + e.getMessage());
		}
	}
	
	public void saveLkupAcknowledgment(String type,int styp, int feeId,int questionId,String planCheck) {
		logger.info("Entered saveLkupAcknowledgment ...");
		String sql = "";
		int newQuestionId = getMaxId("LKUP_ACKNOWLEDGMENT", "ACKNOWLEDGMENT_ID");
		int sortOrder = newQuestionId;
		try {
		
				sql = "insert into LKUP_ACKNOWLEDGMENT (ACKNOWLEDGMENT_ID,ACT_TYPE,ACT_SUBTYPE_ID,FEE_ID,ACK_ID,PC_REQ,RES_PERMITS,RES_ADDL_INFO,SORT_ORDER) values ( "+newQuestionId+ "," + StringUtils.checkString(type) + "," + styp + ", " + feeId + "," + questionId +", " + StringUtils.checkString(planCheck) + ", " + StringUtils.checkString("Y") + ", " + StringUtils.checkString("Y") + ", " + sortOrder + ")";
				logger.debug("sql : " + sql);
				db.insert(sql);
		
		} catch (Exception e) {
			logger.error("error while inserting into saveLkupAcknowledgment " + e.getMessage());
		}
	}
	
	public void deleteAcknowledgementId(int id) {
		logger.info("Entered deleteAcknowledgementId ...");
		String sql = "delete from LKUP_ACKNOWLEDGMENT WHERE  ACKNOWLEDGMENT_ID = " + id;
		logger.debug("sql : " + sql);
		try {
			db.update(sql);
		} catch (Exception e) {
			logger.error("error while deleting from deleteAcknowledgementId " + e.getMessage());
		}
	}
	
	/**
	 * gets list of fees which is mapped for the given activity sub type
	 * 
	 * @param spjrName
	 * @return
	 */
	public List getLkupAcknowledgement(String type,int styp, int feeId,int questionId) {
		logger.info("Entered getLkupQuestionaire ...");
		List actSubFeeMap = new ArrayList();
		StringBuffer sb = new StringBuffer();
		sb.append(" select LQ.*,F.FEE_DESC AS FEE_DESC,LAT.DESCRIPTION as ACTIVITY_TYPE,LAS.ACT_SUBTYPE as ACTIVITY_SUBTYPE,LQU.ACK_DESC as QUESTION  FROM LKUP_ACKNOWLEDGMENT LQ ");
		sb.append(" LEFT OUTER JOIN FEE F on LQ.FEE_ID=F.FEE_ID ");
		sb.append(" LEFT OUTER JOIN LKUP_ACT_SUBTYPE LAS on LQ.ACT_SUBTYPE_ID=LAS.ACT_SUBTYPE_ID ");
		sb.append(" LEFT OUTER JOIN LKUP_ACT_TYPE LAT on LQ.ACT_TYPE=LAT.TYPE ");
		sb.append(" LEFT OUTER JOIN LKUP_ACKNOWLEDGMENTS LQU on LQ.ACK_ID=LQU.ACK_ID ");
		sb.append(" where LQ.ACKNOWLEDGMENT_ID > 0 ");
		if(!type.equalsIgnoreCase("")){
			sb.append(" AND LQ.ACT_TYPE = ").append(StringUtils.checkString(type));
		}
		if(styp >0){
			sb.append(" AND LQ.ACT_SUBTYPE_ID = ").append(styp);
		}
		if(feeId >0){
			sb.append(" AND LQ.FEE_ID = ").append(feeId);
		}
		if(questionId >0){
			sb.append(" AND LQ.ACK_ID = ").append(questionId);
		}
		sb.append(" order by LQ.SORT_ORDER ");
		logger.debug("sql : " + sb.toString());
		try {
			rs = db.select(sb.toString());
			OnlinePermitForm opf = new OnlinePermitForm();
			while (rs.next()) {
				opf = new OnlinePermitForm();
				opf.setLkId(rs.getInt("ACKNOWLEDGMENT_ID"));
				opf.setActCode(rs.getString("FEE_DESC"));
				opf.setActSubName(rs.getString("ACTIVITY_SUBTYPE"));
				opf.setDescription(rs.getString("QUESTION"));
				opf.setActType(rs.getString("ACTIVITY_TYPE"));
				opf.setPlanCheck(rs.getString("PC_REQ"));
				
				actSubFeeMap.add(opf);
			}
			rs.close();
		} catch (Exception e) {
			logger.error("error while fetching Subtype and Fees " + e.getMessage());
		}
		return actSubFeeMap;
	}
	
	public List getAcknowledgementList() {
		logger.info("Entered getActSubFeeMapping ...");
		List actSubFeeMap = new ArrayList();
		String sql = "select * FROM LKUP_ACKNOWLEDGMENTS  where ACTIVE='Y'";
		logger.debug("sql : " + sql);
		try {
			rs = db.select(sql);
			OnlinePermitForm opf = new OnlinePermitForm();
			while (rs.next()) {
				opf = new OnlinePermitForm();
				opf.setQuestionId(rs.getInt("ACK_ID"));
				String ack = rs.getString("ACK_DESC");
				if(ack.length()>150){
					ack = ack.substring(0,150) + "...";
					opf.setDescription(ack);
				}else {
					opf.setDescription(rs.getString("ACK_DESC"));
				}
				actSubFeeMap.add(opf);
			}
			rs.close();
		} catch (Exception e) {
			logger.error("error while fetching Subtype and Fees " + e.getMessage());
		}
		return actSubFeeMap;
	}
	
	public void saveLkupQA(String table,String desc) {
		logger.info("Entered saveLkupQA ..."+table);
		String sql = "";
		String fieldId = "";
		String fieldDesc = "";
		if(table.equalsIgnoreCase("LKUP_ACKNOWLEDGMENTS")){
			fieldId = "ACK_ID";
			fieldDesc = "ACK_DESC";
		}else {
			fieldId = "QUES_ID";
			fieldDesc = "QUES_DESC";
		}
		int newQuestionId = getMaxId(table, fieldId);
		
		try {
		
				sql = "insert into "+table+" ("+fieldId+","+fieldDesc+",ACTIVE) values ( "+newQuestionId+ "," + StringUtils.checkString(desc) + "," +StringUtils.checkString("Y")+")";
				logger.debug("sql : " + sql);
				db.insert(sql);
		
		} catch (Exception e) {
			logger.error("error while inserting into saveLkupQA " + e.getMessage());
		}
	}
	
	public void deleteLoadQAId(String table,int id) {
		logger.info("Entered deleteLoadQAId ...");
		String fieldId = "";
		String fieldDesc = "";
		if(table.equalsIgnoreCase("LKUP_ACKNOWLEDGMENTS")){
			fieldId = "ACK_ID";
			fieldDesc = "ACK_DESC";
		}else {
			fieldId = "QUES_ID";
			fieldDesc = "QUES_DESC";
		}
		String sql = "update  "+table+"  SET ACTIVE =" +StringUtils.checkString("N")+"  WHERE  "+fieldId+" = " + id;
		logger.debug("sql : " + sql);
		try {
			db.update(sql);
		} catch (Exception e) {
			logger.error("error while deleting from deleteLoadQAId " + e.getMessage());
		}
	}
	
	public List getLkupLoadQAList(String table) {
		logger.info("Entered getActSubFeeMapping ...");
		
		List actSubFeeMap = new ArrayList();
		String fieldId = "";
		String fieldDesc = "";
		if(table.equalsIgnoreCase("LKUP_ACKNOWLEDGMENTS")){
			fieldId = "ACK_ID";
			fieldDesc = "ACK_DESC";
		}else {
			fieldId = "QUES_ID";
			fieldDesc = "QUES_DESC";
		}
		
		String sql = "select * FROM "+table+"  where ACTIVE='Y'";
		logger.debug("sql : " + sql);
		try {
			rs = db.select(sql);
			OnlinePermitForm opf = new OnlinePermitForm();
			while (rs.next()) {
				opf = new OnlinePermitForm();
				opf.setLkId(rs.getInt(fieldId));
				opf.setDescription(rs.getString(fieldDesc));
				actSubFeeMap.add(opf);
			}
			rs.close();
		} catch (Exception e) {
			logger.error("error while fetching Subtype and Fees " + e.getMessage());
		}
		return actSubFeeMap;
	}
	
	
	public boolean deleteOnlineQuestionaire(int tempId){
		boolean result = false;
		try {
			
			String sql = "delete from TEMP_ONLINE_QUESTIONAIRE where TEMP_ONLINEID = "+tempId;
			logger.debug("sql : " + sql);
			db.update(sql);
			
			result = true;
	
		} catch (Exception e) {
			result = false;
			logger.error("error while inserting into saveLkupQA " + e.getMessage());
		}
		return result;
	}
	
	public boolean saveOnlineQuestionaire(ArrayList<String> sqls){
		boolean result = false;
		try {
			db.beginTransaction();
			for(int i=0;i<sqls.size();i++){
				db.addBatch(sqls.get(i));
			}
			db.executeBatch();
			result = true;
	
		} catch (Exception e) {
			result = false;
			logger.error("error while inserting into saveLkupQA " + e.getMessage());
		}
		return result;
	}

	
	public List getPrevious(int userId) {
		logger.info("Entered getPrevious ...");
		
		List previous = new ArrayList();
	
		String sql = "select VAL.ADDRESS,STEP_URL,CREATED FROM TEMP_ONLINE_LSO_ADDRESS T left outer join V_ADDRESS_LIST VAL on VAL.LSO_ID=T.LSO_ID where STEP_URL is not null and COMBO_NBR is  null  and CREATED_BY = "+userId+" and rownum <6 ORDER BY CREATED DESC ";
		logger.debug("sql : " + sql);
		try {
			rs = db.select(sql);
			
			while (rs.next()) {
				ApplyOnlinePermitForm applyOnlinePermitForm = new ApplyOnlinePermitForm();
				
				applyOnlinePermitForm.setAddress(rs.getString("address"));
				applyOnlinePermitForm.setUrl(rs.getString("STEP_URL"));
				/*applyOnlinePermitForm.setStructureName(rs.getString("STRUCTURENAME"));
				applyOnlinePermitForm.setSubProjectName(rs.getString("ACT_SUBTYPE"));
				applyOnlinePermitForm.setValuation(rs.getString("valuation"));
				applyOnlinePermitForm.setProjectName(rs.getString("project_name"));
				applyOnlinePermitForm.setContractorName(rs.getString("contractor"));
				applyOnlinePermitForm.setOwnerName(rs.getString("owner"));
				applyOnlinePermitForm.setArchitectName(rs.getString("architect"));
				applyOnlinePermitForm.setEngineerName(rs.getString("engineer"));
				applyOnlinePermitForm.setStypeId(StringUtils.s2i(rs.getString("stype_id")));*/
				applyOnlinePermitForm.setCreated(StringUtils.cal2str(StringUtils.dbDate2cal(rs.getString("CREATED"))));
				previous.add(applyOnlinePermitForm);
			}
			rs.close();
		} catch (Exception e) {
			logger.error("error while fetching Subtype and Fees " + e.getMessage());
		}
		return previous;
	}
	
	public static Map<String, String> splitQuery(URL url) throws UnsupportedEncodingException {
	    Map<String, String> query_pairs = new LinkedHashMap<String, String>();
	    String query = url.getQuery();
	    String[] pairs = query.split("&");
	    for (String pair : pairs) {
	        int idx = pair.indexOf("=");
	        query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
	    }
	    return query_pairs;
	}
	
	
	/**
	 * get questions
	 * 
	 * @param tempOnlineId
	 * @return
	 * @throws Exception
	 */
	public static List getQuestionAnswers(String actNbr) throws Exception {
		logger.info("getQuestionAnswers(" + actNbr + ")");

		List questionList = new ArrayList();
		String tempQuestionID = "";
		String sql;
		String description = "";
		String acknowledgements = "";

		try {
			
			sql = "SELECT Q.OPTION_VALUE,LQ.QUES_DESC FROM TEMP_ONLINE_LSO_ADDRESS T JOIN TEMP_ONLINE_QUESTIONAIRE Q on T.TEMP_ONLINEID=Q.TEMP_ONLINEID join LKUP_QUESTIONS LQ on Q.QUES_ID=LQ.QUES_ID where lower(COMBO_NBR) =lower('" + actNbr + "') ";
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);
			int i = 0;
			while (rs != null && rs.next()) {
				tempQuestionID = rs.getString("OPTION_VALUE");
				description = rs.getString("QUES_DESC");
				i = i +1;
				ApplyOnlinePermitForm applyOnlinePermitForm = new ApplyOnlinePermitForm();
				applyOnlinePermitForm.setTempOnlineID(i);
				applyOnlinePermitForm.setTempQuestionId(tempQuestionID);
				applyOnlinePermitForm.setQuestionaireDescription(description);
				questionList.add(applyOnlinePermitForm);

				logger.debug("questionList.size();" + questionList.size());
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}

		return questionList;
	}
	
	
	
	public static boolean processfees(int activityId,int tempOnlineID){
		boolean result = false;
		try{
			
			
			String feeIds = new OnlineAgent().getFeeIds(tempOnlineID);
			logger.debug("Activity Id :" + activityId);

			String backUrl = "";
			String online = "Y";
			String amt = "$0.00";
			
			FeesMgrForm feesMgrForm = new FeesMgrForm();
			feesMgrForm.setActivityId(activityId);
			feesMgrForm.setBackUrl("");

			FinanceAgent financeAgent = new FinanceAgent();

			ActivityDetail activityDetail = new FinanceAgent().getActivityDetail(activityId);
			String activityFeeDate = StringUtils.cal2str(activityDetail.getActivityFeeDate());
			String valuation = StringUtils.dbl2$(activityDetail.getValuation());

			feesMgrForm.setPermitFeeDate(activityFeeDate);
			feesMgrForm.setValuation(valuation);

			FinanceSummary financeSummary = financeAgent.getActivityFinance(activityId);

			feesMgrForm.setFinanceSummary(financeSummary);
			//feesMgrForm.setActivityFeeList(onlinePermitAgent.getOnlineFeeList(activityId, sTypeId));
			feesMgrForm.setActivityFeeList(new OnlineAgent().getOnlineFeeList(activityId,feeIds));

			ActivityFee activityFee = null;
			financeAgent = new FinanceAgent();
			List activityFeeList = new ArrayList();
			Wrapper db = new Wrapper();
			double[] subTotals = new double[6];
			double permitLicenseFeeTotal = 0;
			double total = 0.0;
			subTotals[0] = 0.0;
			subTotals[1] = 0.0;
			subTotals[2] = 0.0;
			subTotals[3] = 0.0;
			subTotals[4] = 0.0;
			subTotals[5] = 0.0;

			int aFeeSTFlg;

			ActivityFeeEdit[] aFee = feesMgrForm.getActivityFeeList();

			if (aFee != null) {
				for (int i = 0; i < aFee.length; i++) {
					try {

						activityFee = new ActivityFee();
						activityFee.setActivityId(activityId);
						activityFee.setFeeId(StringUtils.s2i(aFee[i].getFeeId()));

						//String feeUnit = onlinePermitAgent.getFeeUnit(aFee[i].getFeeId(), tempOnlineID);
						//activityFee.setFeeUnits(StringUtils.s2d(feeUnit));
						String feeUnit = new OnlineAgent().getFeeUnit(aFee[i].getFeeId(), tempOnlineID);
						activityFee.setFeeUnits(StringUtils.s2d(feeUnit));

						activityFee.setFeeAmount(StringUtils.s2d(aFee[i].getFeeAmount()));
						permitLicenseFeeTotal = permitLicenseFeeTotal + activityFee.getFeeAmount();
						activityFee.setFeeAccount(StringUtils.s2i(aFee[i].getFeeAccount()));
						activityFee.setFeePaid(StringUtils.s2d(aFee[i].getFeePaid()));
						activityFee.setAdjustmentAmount(StringUtils.s2d(aFee[i].getAdjustmentAmount()));
						activityFee.setCreditAmount(StringUtils.s2d(aFee[i].getCreditAmount()));

						activityFee.setFeeInit(StringUtils.s2i(aFee[i].getFeeInit()));
						activityFee.setFeeFactor(StringUtils.s2bd(aFee[i].getFeeFactor()));
						activityFee.setFactor(StringUtils.s2bd(aFee[i].getFactor()));
						activityFee.setSubtotalLevel(StringUtils.s2i(aFee[i].getSubtotalLevel()));
						logger.debug("activity sub total::" + aFee[i].getSubtotalLevel());
						// setting subTotal array to ActivityFees
						activityFee.setSubTotals(subTotals);

						// Calculate the Fee Amount
						logger.debug("//Calculate the Fee Amount");
						financeAgent.calculateFeeAmount(db, activityFee);

						// setting array for subtotals
						if ((!(aFee[i].getSubtotalLevel() == null)) && (!(aFee[i].getFeeCalcOne().equals("J")))) {
							aFeeSTFlg = StringUtils.s2i(aFee[i].getSubtotalLevel());
							subTotals[aFeeSTFlg] += activityFee.getFeeAmount();
							subTotals[aFeeSTFlg] = Double.parseDouble(new DecimalFormat("0.00").format(subTotals[aFeeSTFlg]));
						}

						activityFeeList.add(activityFee);

					} catch (Exception e) {
						// e.printStackTrace();
						logger.error("Error in processing Activity Fees " + e.getMessage());
					}

				}

				logger.debug("Activity Fee List Size " + activityFeeList.size());
			}

			
			
			
			financeAgent.saveFeesList(activityId, activityFeeList);
			
			result = true;
		}catch(Exception e){
			result = false;
			logger.error(e.getMessage());
		}
		return result;
	}
	
	
	/**	 
	 * @param actNbr
	 * @return
	 */
	public int getTempOnlineId(String actNbr){
		logger.info("Entered getTempOnlineId(" + actNbr + ")");
		String sql = new String("select temp_onlineid from temp_Online_lso_address where combo_nbr = " + StringUtils.checkString(actNbr));
		int id = 0;
		try{
			RowSet rs = new Wrapper().select(sql);
			while(rs.next()){
				id = rs.getInt("temp_onlineid");
			}
		}catch(Exception e){
			logger.error(e.getMessage());
		}
		return id;
	}	
	
	/**
	 * @param tempOnlineId
	 * @return
	 */
	public List getQuestionsAnswered(int tempOnlineId){
		logger.info("getQuestions(" + tempOnlineId + ")");

		List questionList = new ArrayList();

		try {
			String sql = "SELECT t.temp_onlineid, t.ques_id, q.ques_desc, t.option_value from TEMP_ONLINE_QUESTIONAIRE t "
					+ "join lkup_questions q on t.ques_id=q.ques_id where t.temp_onlineid = " + tempOnlineId ;

			RowSet rs = new Wrapper().select(sql);

			while (rs != null && rs.next()) {
				ApplyOnlinePermitForm applyOnlinePermitForm = new ApplyOnlinePermitForm();

				applyOnlinePermitForm.setTempQuestionId(rs.getString("ques_id"));
				applyOnlinePermitForm.setQuestionaireDescription(rs.getString("ques_desc"));
				applyOnlinePermitForm.setQuestionAnswered(rs.getString("option_value"));
				questionList.add(applyOnlinePermitForm);
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return questionList;
	}
	
	public int getPeopleId(String email, int people_typeId){
		int id = 0;
		String sql="select people_id from people where people_type_id ="+ people_typeId +" and lower(email) = "+ StringUtils.checkString(email.toLowerCase());
		try{
			RowSet rs = new Wrapper().select(sql);
			while(rs.next()){
				id = rs.getInt("people_id");
			}
		}catch(Exception e){
			logger.error(e.getMessage());
		}
		return id;
	}
	
	
	/* Online Garage sale  */
	
	public String insertEgovTempWithAddress(String lsoId, String streetNum, String streetName, String userId, String resumeUrl, String unit) throws Exception {
		Wrapper db = new Wrapper();
		RowSet rs = null;
		int nextId = 0;
		String sql = "";
		try {
			//nextId = getNextId()+1;
			nextId = new Wrapper().getNextId("CAP_TEMP_ID");
			if(nextId != 0){
				sql = "insert into CAP_TEMP (temp_id, address, lsoid, cap_user_id, created_by, created, resume_url, unit,ACT_TYPE_ID) values ("
						+ nextId + ",'" + streetNum + " " + streetName + "','" + lsoId + "'," + userId + "," 
						+ userId + ", current_timestamp" + ",'" + resumeUrl + "','" + unit + "',"+Constants.GARAGE_SALE+")";
				
				logger.debug(sql);
				
				db.insert(sql);
				
				sql = "update nextid set idvalue=" + nextId + " where idname='CAP_TEMP_ID'";
				
				db.update(sql);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}finally{
			try{
				if(rs !=null){
					rs.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		return nextId+"";
	}
	
	public String updateEgovTempWithAddress(String lsoId, String streetNum, String streetName, String userId, String resumeUrl, String unit,String tempId) throws Exception {
		Wrapper db = new Wrapper();
		RowSet rs = null;
		int nextId = 0;
		String sql = "";
		try {
			//nextId = getNextId()+1;
			
			//if(nextId != 0){
				sql = "update cap_temp set address='" + streetNum + " " + streetName + "',lsoid='" + lsoId + "',cap_user_id=" + userId + ",resume_url='" + resumeUrl + "',"
						+ "unit='" + unit + "' where temp_id="+tempId+"";
				/*sql = "update egov_temp (temp_id, address, lsoid, cap_user_id, created_by, created, resume_url, unit) values ("
						+ nextId + ",'" + streetNum + " " + streetName + "','" + lsoId + "'," + userId + "," 
						+ userId + ", current_timestamp" + ",'" + resumeUrl + "','" + unit + "')";
				*/
				logger.debug(sql);
				
				db.insert(sql);
				
				/*sql = "update nextid set idvalue=" + nextId + " where idname='EGOV_TEMP_ID'";
				
				db.update(sql);*/
			//}
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}finally{
			try{
				if(rs !=null){
					rs.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		return nextId+"";
	}
	public void updateEgovTempWithActType(String tempId, String lsoId, String actType, String userId, String resumeUrl) throws Exception {
		Wrapper db = new Wrapper();
		RowSet rs = null;
		String sql = "";
		try {
			
			int actTypeId = getActTypeId(actType);
			
			sql = "update CAP_TEMP set act_type_id=" + actTypeId + ", updated_by=" + userId 
					+ ", updated=current_timestamp" + ", resume_url='" + resumeUrl + "'"
					+ " where temp_id=" + tempId + " and lsoid=" + lsoId; 
			
			logger.debug(sql);
			
			db.update(sql);
				
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}finally{
			try{
				if(rs !=null){
					rs.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}
	private int getActTypeId(String actType) {
		int typeId = 0;
		String sql = "";
		Wrapper db = new Wrapper();
		RowSet rs = null;
		try {
			sql = "select type_id from lkup_act_type where type='" + actType + "'";
			
			rs = db.select(sql);
			
			if(rs.next())
				typeId = rs.getInt("type_id");
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}finally{
			try{
				if(rs !=null){
					rs.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		return typeId;
	}

	/*public elms.control.beans.online.OnlinePermitForm getOnlineUser(String EmailAddr, String obc) {
		logger.info("inside getOnlineUsers " + EmailAddr);
		elms.control.beans.online.OnlinePermitForm regForm = null;

		try {
			String sql = "";
			if (obc.equals("Y")) {
				sql = "select EU.*,P.LIC_NO,P.PEOPLE_TYPE_ID,P.LIC_EXP_DT,P.WORKERS_COMP_WAIVE,P.WORK_COMP_EXP_DT from ext_user EU left outer join people P on EU.EXT_USERNAME = P.EMAIL_ADDR  where lower(EU.EXT_USERNAME) = " + StringUtils.checkString(EmailAddr.toLowerCase());
			} else {
				sql = "select * from ext_user where lower(EXT_USERNAME) = " + StringUtils.checkString(EmailAddr.toLowerCase());
			}
			logger.debug(sql);
			boolean licNbr = false;
			RowSet rs = new Wrapper().select(sql);
			String peopleType = "";
			regForm = new elms.control.beans.online.OnlinePermitForm();
			while (rs.next()) {
				//regForm.setFirstName((rs.getString("FIRSTNAME") != null ? rs.getString("FIRSTNAME") : "") + " " + (rs.getString("LASTNAME") != null ? rs.getString("LASTNAME") : ""));
				regForm.setFirstName(rs.getString("FIRSTNAME"));
				regForm.setLastName(rs.getString("LASTNAME"));
				regForm.setUserId(rs.getString("EXT_USER_ID"));// .substring(0,rs.getString("EXT_USER_ID").indexOf('.'))));
				regForm.setAccountNo(Integer.parseInt(rs.getString("EXT_ACCT_NBR"))); // setting
				
				regForm.setAddress(rs.getString("ADDRESS"));
				regForm.setPhoneNbr(StringUtils.phoneFormat(rs.getString("PHONE")));
				regForm.setEmailAddress(rs.getString("EXT_USERNAME"));
				regForm.setPhoneExt(rs.getString("PHONE_EXT"));
				regForm.setWorkPhone(StringUtils.phoneFormat(rs.getString("WORK_PHONE")));
				regForm.setWorkExt(rs.getString("WORK_EXT"));
				regForm.setCity(rs.getString("CITY"));
				regForm.setState(rs.getString("STATE"));
				regForm.setZip(rs.getString("ZIP"));
				
			}
			if (obc.equals("Y")) {
				if (!peopleType.equals("")) {
					regForm.setPeopleType(peopleType.substring(0, peopleType.length() - 1));
				}
			}
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error while fetchin values :" + e.getMessage());
		}
		return regForm;
	}*/
	public void updateOnlineApplicant(elms.control.beans.online.OnlinePermitForm regForm, String email) {
		logger.info("inside updateOnlineUser...");
		try {
			String name = regForm.getFirstName();
			String firstName = regForm.getFirstName();
			String lastName = regForm.getLastName();
			/*int index = 0;
			if ((index = name.indexOf(',')) != -1) {
				lastName = name.substring(0, index);
				firstName = name.substring(index + 1);
			} else if ((index = name.indexOf(' ')) != -1) {
				firstName = name.substring(0, index);
				lastName = name.substring(index + 1);
			}*/

			String sql = "update EXT_USER set ADDRESS = " + StringUtils.checkString(regForm.getAddress()) + ", PHONE =" + StringUtils.checkString(StringUtils.stripPhone(regForm.getPhoneNbr())) + ", WORK_PHONE =" + StringUtils.checkString(StringUtils.stripPhone(regForm.getWorkPhone())) + ", EXT_USERNAME = " + StringUtils.checkString(regForm.getEmailAddress()) + ", PHONE_EXT =" + StringUtils.checkString(regForm.getPhoneExt()) + ", WORK_EXT =" + StringUtils.checkString(regForm.getWorkExt()) + ", CITY =" + StringUtils.checkString(regForm.getCity()) + ", STATE =" + StringUtils.checkString(regForm.getState()) + ", ZIP =" + StringUtils.checkString(regForm.getZip());
			if (regForm.getAccountNo() != 0) {
				sql = sql + ", EXT_ACCT_NBR = " + regForm.getAccountNo() + ", DOT=" + StringUtils.checkString((regForm.getDot() != null ? regForm.getDot() : "N"));
			}
			sql = sql + ", OBC=" + StringUtils.checkString((regForm.getObc() != null ? regForm.getObc() : "N")) + ", FIRSTNAME = " + StringUtils.checkString(firstName) + ", LASTNAME = " + StringUtils.checkString(lastName) + " where lower(EXT_USERNAME) = " + StringUtils.checkString(email.toLowerCase());
			logger.debug("update query :" + sql);
			new Wrapper().update(sql);
		} catch (Exception e) {
			logger.error("Error while updating ext_user :" + e.getMessage());
		}
	}
	public void updatePeople(String email, elms.control.beans.online.OnlinePermitForm regForm) {
		logger.info("inside updatePeople...");
		try {

			String name = regForm.getFirstName();
			String firstName = "";
			String lastName = "";
			int index = 0;
			if ((index = name.indexOf(',')) != -1) {
				lastName = name.substring(0, index);
				firstName = name.substring(index + 1);
			} else if ((index = name.indexOf(' ')) != -1) {
				firstName = name.substring(0, index);
				lastName = name.substring(index + 1);
			}

			String sql = "UPDATE PEOPLE SET NAME=";
			sql = sql + StringUtils.checkString(regForm.getFirstName());

			sql = sql + ",LAST_NAME=";
			sql = sql + StringUtils.checkString(regForm.getLastName());
			
			sql = sql + ",EMAIL_ADDR=";
			sql = sql + StringUtils.checkString(regForm.getEmailAddress());

			sql = sql + ",ADDR=";
			sql = sql + StringUtils.checkString(regForm.getAddress());

			sql = sql + ",CITY=";
			sql = sql + StringUtils.checkString(regForm.getCity());

			sql = sql + ",STATE=";
			sql = sql + StringUtils.checkString(regForm.getState());

			sql = sql + ",ZIP=";
			sql = sql + StringUtils.checkString(regForm.getZip());

			sql = sql + ",PHONE=";
			sql = sql + StringUtils.checkString(StringUtils.phoneFormat(regForm.getPhoneNbr()));
			
			/*sql = sql + ",LIC_NO=" + StringUtils.checkString(regForm.getLicenseNbr());

			sql = sql + ",DL_ID_NO=" + StringUtils.checkString(regForm.getDlNo());
*/
			sql = sql + ",UPDATED=" + "current_timestamp";
			logger.debug("got Updated date : " + "current_timestamp");

			/*logger.debug("in updating LIC_NO WORKERS_COMP_WAIVE");
			sql = sql + ", LIC_EXP_DT=" + StringUtils.toOracleDate(regForm.getLicExpDate()) + ", WORKERS_COMP_WAIVE=" + StringUtils.checkString(regForm.getWorkersCompensationWaive()) + ", WORK_COMP_EXP_DT=" + StringUtils.toOracleDate(regForm.getStrWorkersCompExpires());
		*/
			
			sql = sql + " WHERE EMAIL_ADDR=";
			sql = sql + StringUtils.checkString(email);

			logger.debug("update query :" + sql);
			new Wrapper().update(sql);
		} catch (Exception e) {
			logger.error("Error while updating people :" + e.getMessage());
		}
	}
	public void updateEgovTempAdditonalInfo(String tempId, elms.control.beans.online.OnlinePermitForm addForm, String resumeUrl,  String userId) throws Exception {
		String sql = "";
		Wrapper db = new Wrapper();
		logger.debug("getActDescription... "+addForm.getActDescription());
		try {
			sql = "update CAP_TEMP set updated=current_timestamp, updated_by=" + userId + ",resume_url='" + resumeUrl + "', act_description='" + addForm.getActDescription() + "',ISSUE_DATE = "+StringUtils.toOracleDate(addForm.getIssueDate())+",END_DATE = "+StringUtils.toOracleDate(addForm.getEndDate())+" where temp_id=" + tempId;
			logger.debug(sql);
					
			db.update(sql);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
		
	}
	public String getTempLevelId(String tempId) {
		Wrapper db = new Wrapper();
		RowSet rs = null;
		try {
			rs = db.select("select level_id from CAP_TEMP where temp_id="+tempId);
			
			if(rs.next())
				return rs.getString("level_id");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}finally{
			try{
				if(rs != null){
				    rs.close();
				}
			}catch(Exception e){
				
			}
		}

		return null;
	}
	public void updateCAPTempWithAttachment(String tempId, String fileName,
			String attachmentPath, String description, String levelId, String userId,int attachmentId) throws Exception {
		String sql = "";
		Wrapper db = new Wrapper();
		try {
			//sql = "delete from egov_temp_attachment where egov_temp_id="+tempId;
			
		//	db.update(sql);
		
			/*sql = "update ATTACHMENTS set LEVEL_ID="+levelId+" where ATTACH_ID in (select ATTACH_ID from CAP_TEMP_ATTACHMENT where CAP_TEMP_ID="+tempId+")";
			
			logger.debug(sql);
			
			db.insert(sql);*/
			
			sql = "insert into cap_temp_attachment (cap_temp_id, file_name, file_location, description,ATTACH_ID) values ("
					+ tempId + ",'" + fileName + "','" + attachmentPath + "','" + description + "'," + attachmentId + ")";
			
			logger.debug(sql);
			
			db.insert(sql);
			
			sql = "update CAP_TEMP set updated=current_timestamp, updated_by=" + userId + ", level_id= " + levelId + " where temp_id=" + tempId;
			
			db.update(sql);
					
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			throw e;
		}
		
	}
	public void updateCAPTempWithResumeUrl(String tempId, String resumeUrl, String userId) throws Exception {
		Wrapper db = new Wrapper();
		String sql = "";
		try {
			
			sql = "update CAP_TEMP set updated=current_timestamp, updated_by=" + userId + ", resume_url='" + resumeUrl + "' where temp_id=" + tempId ; 
			
			logger.debug(sql);
			
			db.update(sql);
				
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
		
	}
	public void deleteTempPermit(String tempId) {
		String sql = "";
		Wrapper db = new Wrapper(); 
		try {
			sql = "delete from CAP_TEMP where temp_id="+tempId;
			db.update(sql);
			/*sql = "delete from egov_temp_act_subtype where egov_temp_id="+tempId;
			db.update(sql);
			sql = "delete from egov_temp_custom_fields where egov_temp_id="+tempId;
			db.update(sql);*/
			sql = "delete from cap_temp_attachment where cap_temp_id="+tempId;
			db.update(sql);
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
	}
	public int applyForGaragesalePermit(String addrId) {
		Wrapper db = new Wrapper();
		RowSet rs1 = null,rs = null;
		 Calendar date = Calendar.getInstance();
		 date.setTime(new Date());
		 //date.add(Calendar.YEAR,-1);
		 SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
		 /*String lastyear=formatter1.format(date.getTime());
		 logger.debug(formatter1.format(date.getTime()));*/
		 int count=0;
		 Date firstIssuedDate = null,lastIssuedDate = null;
		 ActivityAgent aa = new ActivityAgent();
		 String sql = "select min(ISSUED_DATE) as FIRST_DATE from ACTIVITY where ACT_TYPE ='GARSL' AND ADDR_ID="+addrId;//+"ORDER BY ISSUED_DATE ASC ";
		 //String sql1 = "select max(ISSUED_DATE) as LAST_DATE from ACTIVITY where ACT_TYPE ='GARSL' AND ADDR_ID="+addrId;
		 logger.debug(sql);
		try {
			rs = db.select(sql);
			if(rs != null && rs.next()){
				firstIssuedDate= rs.getDate("FIRST_DATE");
				logger.debug("firstIssuedDate : "+firstIssuedDate);
			}
			
			/*rs1 = db.select(sql1);
			if(rs1 != null && rs1.next()){
				lastIssuedDate= rs1.getDate("LAST_DATE");
				logger.debug("lastIssuedDate : "+lastIssuedDate);
			}*/
			if(rs == null){
				return count;
			}
			//logger.debug("firstIssuedDate : "+firstIssuedDate+": lastIssuedDate:"+lastIssuedDate);
			Calendar cal = Calendar.getInstance();
			cal.setTime(firstIssuedDate);
			int month1 = cal.get(Calendar.MONTH);
			int day1 = cal.get(Calendar.DAY_OF_MONTH);
			 Calendar today = Calendar.getInstance();
			 today.setTime(new Date());
			 int day = today.get(Calendar.DAY_OF_MONTH);
			 int month=today.get(Calendar.MONTH);
			 int year = today.get(Calendar.YEAR);
			 today.clear();
			 if(month>month1){
			 today.set(year, month1, day1); //Year, month and day of month
			 }else{
				 today.set(year-1,month1,day1);
			 }
			 String toDate = formatter1.format(today.getTime());
			 logger.debug("toDate :"+ toDate);
			 
			 String sql3= "select count(ISSUED_DATE) AS COUNT from ACTIVITY where ACT_TYPE ='GARSL' AND ADDR_ID="+addrId+" AND ISSUED_DATE >= to_date('"+toDate+"','YYYY-MM-DD') AND STATUS IN ("
			 +aa.getActivityStatusForGarageSale(Constants.ONLINE_GS_PERMIT_DEFAULT_STATUS)+","+aa.getActivityStatusForGarageSale(Constants.ONLINE_GS_PERMIT_FINAL_STATUS)+")";
			 logger.debug(sql3);
			 
			 rs1 = db.select(sql3);
			 
			 if(rs1 != null && rs1.next()){
				 count = rs1.getInt("COUNT");
			 }
			 
		} catch (Exception e) {
			logger.error(e.getMessage());
		}finally{
			try{
				if(rs != null){
				    rs.close();
				}
				if(rs1 != null){
				    rs1.close();
				}
			}catch(Exception e){
				
			}
		}
		logger.debug("count : "+count);
		return count;
		
	}

	public Date getlastIssuedDate(String addressId) {
		ActivityAgent aa = new ActivityAgent();
		 String sql = "select max(ISSUED_DATE) as LAST_DATE from ACTIVITY where ACT_TYPE ='GARSL' AND ADDR_ID="+addressId +" AND STATUS IN ("
			 +aa.getActivityStatusForGarageSale(Constants.ONLINE_GS_PERMIT_DEFAULT_STATUS)+","+aa.getActivityStatusForGarageSale(Constants.ONLINE_GS_PERMIT_FINAL_STATUS)+")";;
		 Date lastIssuedDate=null;
		 logger.debug(sql);
		 Wrapper db = new Wrapper();
		 RowSet rs = null;
		 try {
				rs = db.select(sql);
				
				if(rs != null && rs.next()){
					lastIssuedDate = rs.getDate("LAST_DATE");
					logger.debug("lastIssuedDate : "+lastIssuedDate);
				}
		 } catch (Exception e) {
				logger.error(e.getMessage());
			}finally{
				try{
					if(rs != null){
					    rs.close();
					}
					
				}catch(Exception e){
					
				}
			}
		return lastIssuedDate;
	}
	
	//alreay wtitten by Siva
	public static Map<String, String> getLkupSystemDataMap(){           
        Map<String, String> lkupSystemDataMap = new HashMap<String, String>();
        String sql = "SELECT * FROM LKUP_SYSTEM";
        logger.info(sql);
        RowSet rs = null;
        try {
            rs = new Wrapper().select(sql);
           
            if(rs != null){                   
                while(rs.next()){
                        lkupSystemDataMap.put(rs.getString("NAME"),rs.getString("VALUE"));
                    }
                }
            } catch (Exception e) {
            logger.error(""+ e+e.getMessage());
        }//closing RowSet is added by Gayathri
        finally{
            if(rs != null){
                try{
                    rs.close();
                }catch(Exception e){
                    logger.error("Unable to close the rowset");
                }
            }
        }
        return lkupSystemDataMap;
    }


	  public PaymentMgrForm saveApplicationDetails(PaymentMgrForm paymentMgrForm) throws Exception {
//		  PaymentMgrForm paymentMgrForm = new PaymentMgrForm();
		  paymentMgrForm = getActivityDetails(paymentMgrForm.getLevelId());
		  
		logger.debug("in saveApplicationDetails ..."+paymentMgrForm.toString());
		Wrapper db = new Wrapper();
	    String userName="";
	    int userId=0;
	    userName="ONLINE USER";
	    userId = getUserInfo(userName);
	    RowSet rs = null;
		String peopleID=null;
		try{
			if(paymentMgrForm.getLevelId() != null) {
//			db.beginTransaction();
			String updateActivityQuery = "";
			String qtyOther="";
			
				//UPDATE ACTIVITY TABLE
				updateActivityQuery = "UPDATE ACTIVITY SET STATUS ="+Constants.PAID_PENDING_FOR_APPROVAL_CODE+",APPLICATION_ONLINE='Y',UPDATED_BY="+userId+",UPDATED=current_date  WHERE ACT_ID ="+paymentMgrForm.getLevelId();
				logger.debug("updateActivityQuery..."+updateActivityQuery);
//				db.addBatch(updateActivityQuery);
				db.update(updateActivityQuery);
		
			//UPDATE MULTI ADDRESS TABLE
			//check and if there update it not there insert in it
			if(paymentMgrForm.getStrNo() != null) {
				String updateMultiAddressQuery = "UPDATE MULTI_ADDRESS SET STREET_NUMBER='"+paymentMgrForm.getStrNo()+"', STREET_NAME1='"+paymentMgrForm.getAddress()+"',UNIT='"+paymentMgrForm.getUnit()+"',CITY='"+paymentMgrForm.getCity()+"',STATE='"+paymentMgrForm.getState()+"',ZIP='"+paymentMgrForm.getZip()+"' WHERE ADDRESS_TYPE_ID=1 and ACT_ID="+paymentMgrForm.getLevelId();
				logger.debug("updateMultiAddressQuery... "+updateMultiAddressQuery);
//				db.addBatch(updateMultiAddressQuery);
				db.update(updateMultiAddressQuery);
			}else {
				String insertMultiAddressQuery = "INSERT INTO MULTI_ADDRESS (ID,ACT_ID,STREET_NUMBER,STREET_NAME1,UNIT,CITY,STATE,ZIP,ADDRESS_TYPE_ID) VALUES((SELECT MAX(ID)+1 FROM MULTI_ADDRESS),"+paymentMgrForm.getLevelId()+",'"+paymentMgrForm.getStrNo()+"', '"+paymentMgrForm.getAddress()+"','"+paymentMgrForm.getUnit()+"','"+paymentMgrForm.getCity()+"','"+paymentMgrForm.getState()+"','"+paymentMgrForm.getZip()+"', 1)";
				logger.debug("insertMultiAddressQuery... "+insertMultiAddressQuery);
//				db.addBatch(insertMultiAddressQuery);
				db.update(insertMultiAddressQuery);
			}

			//CHECKING PEOPLE IS EXIST OR NOT WITH PEOPLE TYPE APPLICANT(PEOPLE_TYPE_ID=2)

			logger.debug("paymentMgrForm.getEmail()  "+paymentMgrForm.getEmail());
			if(paymentMgrForm.getEmail() != null) {
				String peopleCheck="select * from people where EMAIL_ADDR='"+paymentMgrForm.getEmail()+"' and people_type_id="+Constants.PEOPLE_APPLICANT;
				logger.debug("peopleCheck.. "+peopleCheck);
				 //Checking people is exist or not
				rs=db.select(peopleCheck);
				if(rs != null && rs.next()) {
			    	peopleID=rs.getString("PEOPLE_ID");
			    	logger.debug("inside resultset $$$$ ..."+peopleID);
				}
			}		 		 		 
			 logger.debug("peopleID... "+peopleID);

			 //insert into ACTIVITY_PEOPLE table after people insertion not in updation
			String finalPeopleId=null;
			if(peopleID == null) {
				 int peopleId = db.getNextId("PEOPLE_ID");
				//INSERTING INTO PEOPLE TABLE IF RECORD DOESNT EXIST
				 //add email and phone number
				 String insertPeople="INSERT INTO PEOPLE (PEOPLE_ID, PEOPLE_TYPE_ID, NAME, ADDR, CITY, STATE, ZIP, PHONE, EMAIL_ADDR,CREATED,CREATED_BY) VALUES ("+peopleId+", "+Constants.PEOPLE_APPLICANT+",'"+paymentMgrForm.getName()+"', '"+paymentMgrForm.getStrNo()+ " "+paymentMgrForm.getAddress()+"', '"+StringUtils.nullReplaceWithEmpty(paymentMgrForm.getCity())+"', '"+StringUtils.nullReplaceWithEmpty(paymentMgrForm.getState())+"', '"+paymentMgrForm.getZip()+"', '"+paymentMgrForm.getPhone()+"',  '"+paymentMgrForm.getEmail()+"',current_date,"+userId+")";
				 logger.debug("insertPeople..."+insertPeople);
//				 db.addBatch(insertPeople);			
					db.update(insertPeople);
				 finalPeopleId=StringUtils.i2s(peopleId);
			}

			logger.debug("paymentMgrForm.getEmail()  "+paymentMgrForm.getEmail());
			String actId="";
			if(finalPeopleId != null) {
				String actPeopleCheck="select * from activity_people where people_id="+finalPeopleId;
				logger.debug("peopleCheck.. "+actPeopleCheck);
				 //Checking people is exist or not
				rs=db.select(actPeopleCheck);
				if(rs != null && rs.next()) {
			    	actId=rs.getString("ACTIVITY_ID");
				}
			}	
			if(actId == null) {
				 //INSERTING INTO activity PEOPLE TABLE IF RECORD DOESNT EXIST
				 String insertActivityPeople="INSERT INTO ACTIVITY_PEOPLE (ACT_ID,PEOPLE_ID, PSA_TYPE) VALUES ("+paymentMgrForm.getLevelId()+","+finalPeopleId+",'A')";
				 logger.debug("insertActivityPeople..."+insertActivityPeople);
//				 db.addBatch(insertActivityPeople);
					db.update(insertActivityPeople);				
			}
			//FEE UPDATES

			logger.debug("activity type..."+paymentMgrForm.toString());
			 int pymntId = db.getNextId("PAYMENT_ID");
			 logger.debug("pymntId..."+pymntId);
			 
			List<Fee> feeList =  getFees(paymentMgrForm.getLevelId());
			double totalAmnt = 0;
			for(int i=0;i<feeList.size();i++) {
				totalAmnt = totalAmnt + feeList.get(i).getFeeAmnt();
			}
			//INSERT INTO PAYMENT TABLE
			 String insertPayment="INSERT INTO PAYMENT (PYMNT_ID, PYMNT_METHOD, PYMNT_TYPE, PYMNT_DT, PEOPLE_ID, PAYEE, PYMNT_AMNT,ENTER_BY_ID, AUTHORIZATION_ID, DEPT_CODE, ONLINETXNID) "
			 		+ "select distinct "+pymntId+",'creditcard','1',current_date,"+finalPeopleId+",'"+StringUtils.nullReplaceWithEmpty(StringUtils.handleSingleQuot(paymentMgrForm.getName()))+"', "+totalAmnt+","+userId+",'"+StringUtils.nullReplaceWithEmpty(paymentMgrForm.getAuthorizedById())+"','LC','"+paymentMgrForm.getOnlineTxnId()+"' "
			 		+ "from ACTIVITY_FEE af where af.activity_id="+paymentMgrForm.getLevelId();
			 logger.debug("insertPayment..."+insertPayment);
//			 db.addBatch(insertPayment);
				db.update(insertPayment);

			logger.debug("activity type..."+paymentMgrForm.toString());
			logger.debug("feeList..."+feeList.size());
			logger.debug("qtyOther..."+qtyOther);
			
			if(StringUtils.nullReplaceWithEmpty(qtyOther).equalsIgnoreCase("") || qtyOther.equalsIgnoreCase("0")) {
				qtyOther="1";
			}
			
			String actID=null;		
			String feeAccount=null;
			for(int i=0;i<feeList.size();i++) {
				 int transsId = db.getNextId("TRANS_ID");
				 logger.debug("transsId..."+transsId);
				 
				 feeAccount = getFeesAccountForFeeId(feeList.get(i).getFeeId());
				 
				 logger.debug("fee total 630..."+feeList.get(i).getFeeTotal());
				 logger.debug("fee total 631..."+feeList.get(i).getTotalFee());
				 logger.debug("fee factor 632..."+feeList.get(i).getFeeFactor());
				 
				//INSERT INTO PAYMENT DETAIL TABLE
				String insertPaymentDetail="INSERT INTO PAYMENT_DETAIL (TRANS_ID, PYMNT_ID, ACT_ID, FEE_ID, PEOPLE_ID, AMNT, COMMENTS, FEE_ACCOUNT) "
						+ "values( "+transsId+","+pymntId+","+paymentMgrForm.getLevelId()+","+feeList.get(i).getFeeId()+","+finalPeopleId+","+feeList.get(i).getFeeAmnt()+", 'Transaction done by Online eGov','"+ feeAccount +"')";
				logger.debug("insertPaymentDetail..."+insertPaymentDetail);
//				db.addBatch(insertPaymentDetail);
				db.update(insertPaymentDetail);

				//CHECKING IN ACTIVITY_FEE TABLE ROW IS EXIST OR NOT
				if(StringUtils.s2i(paymentMgrForm.getLevelId()) != 0) {

					String activityFeeCheck="select * from ACTIVITY_FEE where ACTIVITY_ID="+paymentMgrForm.getLevelId()+" and FEE_ID = "+feeList.get(i).getFeeId();
					logger.debug("activityFeeCheck.. "+activityFeeCheck);
					 //Checking in activity fee table row is exist or not
					
					logger.debug("actID ::  "+actID);
					 actID=null;			
					 rs=db.select(activityFeeCheck);
			    	if(rs!=null && rs.next()) {
			    		actID=rs.getString("ACTIVITY_ID");
			    	}					    	
					logger.debug("inside resultset $$$$ ..."+actID);
					
					 logger.debug("activityFeeCheck outside jdbctemplate.. "+actID);
					 
				}		 		 
				 
				
				if(actID != null) {
					String updateActFee="";
					
					//UPDATE INTO ACTIVITY_FEE TABLE
					
					updateActFee = "UPDATE ACTIVITY_FEE SET FEE_PAID="+feeList.get(i).getFeeAmnt()+" ,FEE_ID="+feeList.get(i).getFeeId()+",PEOPLE_ID="+StringUtils.nullReplaceWithZero(finalPeopleId)+",FEE_UNITS="+qtyOther+",FEE_AMNT="+feeList.get(i).getFeeAmnt()+" where ACTIVITY_ID="+paymentMgrForm.getLevelId() + " AND FEE_ID="+feeList.get(i).getFeeId();
					logger.debug("update else condition of :: "+updateActFee);
					logger.debug("updateActFee..."+updateActFee);
//					db.addBatch(updateActFee);
					db.update(updateActFee);
				}else { 
					//INSERT INTO ACTIVITY_FEE TABLE
					 String insertActFee="";
					 insertActFee="INSERT INTO ACTIVITY_FEE (ACTIVITY_ID,FEE_ID,PEOPLE_ID,FEE_UNITS,FEE_AMNT,FEE_PAID) values( "+paymentMgrForm.getLevelId()+","+feeList.get(i).getFeeId()+","+finalPeopleId+","+qtyOther+","+feeList.get(i).getFeeAmnt()+","+feeList.get(i).getFeeAmnt()+" )";
					 logger.debug("insert else condition "+insertActFee);
//					 db.addBatch(insertActFee);	
					 db.update(insertActFee);
				}			
			}
			 //payment table 1 record, payment_detail table will have 2 records
//			db.executeBatch();
			}
		}catch(Exception e){
			logger.error(""+e+e.getMessage());	
			 try {
				throw e;
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}finally {
			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		logger.debug(paymentMgrForm.getComboNo() + paymentMgrForm.getLevelId());
		return paymentMgrForm;
	}


	public List<Fee> getFees(String actId) {
		String sql ="SELECT DISTINCT F.FEE_ID,AF.FEE_UNITS,AF.FEE_AMNT,F.FEE_DESC,(AF.FEE_AMNT - af.fee_paid) as baldue FROM ACTIVITY_FEE AF JOIN FEE F ON AF.FEE_ID=F.FEE_ID WHERE AF.FEE_AMNT - AF.FEE_PAID >0 AND AF.ACTIVITY_ID = "+actId;
		
		logger.debug("getFees sql :"+sql);
		List<Fee> feeList = new ArrayList<Fee>(); 
		RowSet rs = null;
		Wrapper db = new Wrapper();
		try {
			rs=db.select(sql);
			while(rs!=null && rs.next()) {
	        	Fee fee = new Fee();
	        	fee.setFeeId(rs.getInt("FEE_ID"));
	        	fee.setFeeUnits(StringUtils.s2d(rs.getString("FEE_UNITS")));
	        	fee.setFeeAmnt(StringUtils.s2d(rs.getString("FEE_AMNT")));
	        	
	        	fee.setTotalFee(StringUtils.s2d(rs.getString("baldue")));
	        	fee.setFeeDesc(rs.getString("FEE_DESC"));        	
	        	feeList.add(fee);
	        }
	     logger.debug("# of elements set in List - " + feeList.size());
		} catch (Exception e) {
			logger.error("",e);
		}finally {
			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return feeList;
	}


	/**
	 * This method is to fetch the USER ID from Users Table 
	 * @param 
	 * @throws Exception
	 * @return Int
	 * 
	 */

	public int getUserInfo(String userName) {
		String userSql = "SELECT USERID FROM USERS WHERE upper(USERNAME) ='"+userName.toUpperCase()+"'";
		
		logger.debug("getUserInfo sql :"+userSql);
		int userId = 0;
		RowSet rs = null;
		Wrapper db = new Wrapper();
		try {
			rs = db.select(userSql);
			if(rs!= null && rs.next()) {
				userId = rs.getInt("USERID");
			}
		}catch(Exception e) {
			e.printStackTrace();
			logger.debug(e.getMessage() , e);
			return 0;
		}finally {
			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return userId;
	}


	/**
	 * This method is to fetch Fee Account for each Fee id that is passed as variable to this method after payment 
	 * based on the fee Id.
	 * @param String, String
	 * @throws Exception
	 * @return List<Fee>
	 * 
	 */

	public String getFeesAccountForFeeId(int i) {
		String sql ="SELECT FEE_ACCOUNT FROM FEE WHERE FEE_ID = "+i;
		logger.debug("getFees sql :"+sql);
		String feeAccount = "";
		RowSet rs = null;
		Wrapper db = new Wrapper();
		try {
			rs = db.select(sql);
			if(rs!=null && rs.next()) {
				feeAccount = rs.getString("FEE_ACCOUNT");
			}
	     logger.debug("# feeAccount - " + feeAccount);
		} catch (Exception e) {
			logger.error("",e);
		}finally {
			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return feeAccount;
	}
	public PaymentMgrForm getActivityDetails(String tempOnlineID) throws Exception {
		Wrapper db = new Wrapper();

		String comboNo = "";
		String sql1 = null;
		PaymentMgrForm paymentMgrForm =new PaymentMgrForm(); 
		RowSet rs = null;
		try {
			sql1 = "select a.act_id,p.EMAIL_ADDR,a.act_nbr from activity a left join activity_people ap on a.act_id = ap.act_id left join people p on ap.people_id = p.people_id left join TEMP_ONLINE_LSO_ADDRESS tol on a.act_nbr = tol.combo_nbr where a.act_id=" + tempOnlineID;
			logger.debug("sql1... "+sql1);
			rs = db.select(sql1);

			while (rs.next()) {
				paymentMgrForm.setEmail(rs.getString("EMAIL_ADDR"));
				paymentMgrForm.setLevelId(rs.getString("act_id"));
				paymentMgrForm.setComboNo(rs.getString("ACT_NBR"));
			}
		} catch (Exception e) {
			logger.error("Exception thrown while trying to get activity details:" + e+e.getMessage());
		}finally {
			if(rs != null) {
				rs.close();
			}
		}

		return paymentMgrForm;
	}

	public String saveCartDetails(CartDetailsForm cartDetailsForm) throws Exception {
		Wrapper db = new Wrapper();
		String sql = null;
		String selectSql = null;
		String insertCartsql = null;
		String updateCartsql = null;
		String displayMsg="";
		RowSet rs = null;
		RowSet countRs = null;
		int count=0;
		int cartCount=0;
		double totalFeeAmount =0.00;

	    DecimalFormat df2 = new DecimalFormat("#0.00");
	    LocalDate date =  LocalDate.now().plusDays(15);
		
		logger.debug("getAmount..."+cartDetailsForm.getAmount());
	    logger.debug("******** "+formatter.format(StringUtils.s2d(cartDetailsForm.getAmount())));
		try {
			//to limit cart items to 10 items
			String countSql = "SELECT count(cart_id) as cartCount FROM CART_DETAILS WHERE CART_ID="+cartDetailsForm.getCartId();
			logger.debug("countSql... "+countSql);
			countRs=db.select(countSql);
			if(countRs!=null && countRs.next()) {
				cartCount = countRs.getInt("cartCount");
			}
			logger.debug("cartCount... "+cartCount);
			logger.debug(cartCount<10);
			if(cartCount <10) {
				logger.debug("cartDetailsForm.getAmount()... "+cartDetailsForm.getAmount());
				
				//to get the total fee amount
				selectSql = "SELECT CART_ID,ACTIVITY_ID FROM CART_DETAILS WHERE CART_ID="+cartDetailsForm.getCartId()+" AND ACTIVITY_ID="+cartDetailsForm.getActivityId();
				logger.debug("selectSql... "+selectSql);
				rs=db.select(selectSql);
				if(rs!=null && rs.next()) {
					sql = "UPDATE CART_DETAILS SET FEE_AMOUNT="+cartDetailsForm.getAmount()+", CREATED=current_date WHERE CART_ID="+rs.getString("CART_ID")+" AND ACTIVITY_ID="+rs.getString("ACTIVITY_ID");
					logger.debug("sql... "+sql);
					db.update(sql);
				}else {
					sql = "INSERT INTO CART_DETAILS (CART_ID,ACTIVITY_ID,FEE_AMOUNT,CREATED) VALUES ("+cartDetailsForm.getCartId()+","+cartDetailsForm.getActivityId()+",'"+cartDetailsForm.getAmount()+"',current_date)";
					logger.debug("sql... "+sql);
					db.insert(sql);		
				}
					
				//to get the total fee amount
				selectSql = "SELECT CART_ID,FEE_AMOUNT FROM CART_DETAILS WHERE CART_ID="+cartDetailsForm.getCartId();
				logger.debug("selectSql... "+selectSql);
				rs=db.select(selectSql);
				while(rs!=null && rs.next()) {
					totalFeeAmount = totalFeeAmount + rs.getDouble("FEE_AMOUNT");
					logger.debug("totalFeeAmount..."+totalFeeAmount);
				}
				
				// To check whether cart table has to insert or update
				selectSql = "SELECT count(CART_ID) as count FROM CART WHERE CART_ID="+cartDetailsForm.getCartId();
				logger.debug("selectSql... "+selectSql);
				rs=db.select(selectSql);
				while(rs!=null && rs.next()) {
					count = rs.getInt("count");
				}
				
				if(count >0) {
					updateCartsql = "UPDATE CART SET TOTAL_AMOUNT ='"+df2.format(totalFeeAmount)+"' WHERE CART_ID="+cartDetailsForm.getCartId();
					logger.debug("updateCartsql... "+updateCartsql);
					db.update(updateCartsql);
				}else {
					insertCartsql = "INSERT INTO CART (CART_ID,TOTAL_AMOUNT,EMAIL_ADDR,CREATED,EXPIRE_DATE,ACTIVE) VALUES ("+cartDetailsForm.getCartId()+",'"+cartDetailsForm.getAmount()+"','"+cartDetailsForm.getEmail()+"',current_date,TO_DATE('"+date+"','YYYY-MM-DD HH24:MI:SS'),'Y')";
					logger.debug("insertCartsql... "+insertCartsql);
					db.insert(insertCartsql);
				}
				displayMsg = "Item added to the cart";
			}else {
				displayMsg = "You have reached the cart limit";
			}
		} catch (Exception e) {
			logger.error("Exception thrown while trying to add item to the cart:" + e+e.getMessage());
			throw e;
		}finally {
			if(rs != null) {
				rs.close();
			}
		}
		return displayMsg;
	}


	public String removeCartDetails(String actId,String cartId) throws Exception {
		Wrapper db = new Wrapper();
		String sql = null;
		String selectSql = null;
		String insertCartsql = null;
		String updateCartsql = null;
		RowSet rs = null;
		double totalFeeAmnt=0.00;
		double feeAmount =0.00;
		String displayMsg="";
	    DecimalFormat df2 = new DecimalFormat("#.##");
		try {
			
			//to get the total fee amount
			selectSql = "SELECT CART_ID,FEE_AMOUNT FROM CART_DETAILS WHERE CART_ID="+cartId+" and activity_id="+actId;
			logger.debug("selectSql... "+selectSql);
			rs=db.select(selectSql);
			while(rs!=null && rs.next()) {
				feeAmount = rs.getDouble("FEE_AMOUNT");
				logger.debug("totalFeeAmount..."+feeAmount);
			}
			
			sql = "DELETE FROM CART_DETAILS WHERE CART_ID ="+cartId+" AND ACTIVITY_ID="+actId;
			logger.debug("sql... "+sql);
			db.update(sql);
			
			// To check whether cart table has to insert or update
			selectSql = "SELECT TOTAL_AMOUNT FROM CART WHERE CART_ID="+cartId;
			logger.debug("selectSql... "+selectSql);
			rs=db.select(selectSql);
			while(rs!=null && rs.next()) {
				totalFeeAmnt = rs.getDouble("TOTAL_AMOUNT");
			}

				updateCartsql = "UPDATE CART SET TOTAL_AMOUNT ="+df2.format(totalFeeAmnt - feeAmount)+" WHERE CART_ID="+cartId;
				logger.debug("updateCartsql... "+updateCartsql);
				db.update(updateCartsql);
				displayMsg = "Item removed from the cart";

		} catch (Exception e) {
			logger.error("Exception thrown while trying to remove item cart  :" + e+e.getMessage());
			throw e;
		}finally {
			if(rs != null) {
				rs.close();
			}
		}
		return displayMsg;
	}

	public List<CartDetailsForm> getCartDetails(String cartId) throws Exception {
		Wrapper db = new Wrapper();
		List<CartDetailsForm> list = new ArrayList<CartDetailsForm>();
		CartDetailsForm cartDetailsForm = new CartDetailsForm();
		String sql = null;
		RowSet rs = null;

		try {
			sql = "SELECT COUNT(CD.CART_ID) AS COUNT_CART_ID, C.TOTAL_AMOUNT,c.CART_ID,CD.ACTIVITY_ID,CD.FEE_AMOUNT,A.ACT_NBR FROM CART C LEFT JOIN CART_DETAILS CD ON C.CART_ID = CD.CART_ID LEFT JOIN ACTIVITY A ON CD.ACTIVITY_ID = A.ACT_ID WHERE C.CART_ID = "+cartId+" AND C.ACTIVE='Y' AND EXPIRE_DATE >= CURRENT_DATE GROUP BY CD.CART_ID,C.TOTAL_AMOUNT,c.CART_ID,CD.ACTIVITY_ID,CD.FEE_AMOUNT,A.ACT_NBR ";
			logger.debug("sql... "+sql);
			rs = db.select(sql);
			while(rs!=null && rs.next()) {
				cartDetailsForm = new CartDetailsForm();
				cartDetailsForm.setCartId(rs.getString("CART_ID"));
				cartDetailsForm.setActivityId(rs.getString("ACTIVITY_ID"));
				cartDetailsForm.setAmount(formatter.format(StringUtils.s2d(rs.getString("FEE_AMOUNT"))));
				cartDetailsForm.setTotalFeeAmount(formatter.format(StringUtils.s2d(StringUtils.nullReplaceWithZero(rs.getString("TOTAL_AMOUNT")))));
				logger.debug("getTotalFeeAmount.. "+cartDetailsForm.getTotalFeeAmount());
				cartDetailsForm.setActivityNo(rs.getString("ACT_NBR"));
				list.add(cartDetailsForm);
			}
		} catch (Exception e) {
			logger.error("Exception thrown while trying to add online Permit  :" + e+e.getMessage());
			throw e;
		}finally {
			if(rs != null) {
				rs.close();
			}
		}
		return list;
	}


	public User saveUserIfnotExistBasedonEmailAddr(String emailAddr) throws Exception {
		logger.debug("In SaveUserIfnotExistBasedonEmailAddr");
		Wrapper db = new Wrapper();
		String sql = null;
		String sql1 = null;
		String sql2 = null;
		String insertSql = null;
		RowSet rs = null;
		int peopleId = 0;
		int extUserId = 0;
		User user = null;
		try {
			sql1 = "SELECT * FROM EXT_USER WHERE UPPER(EXT_USERNAME) = UPPER('"+emailAddr+"')";
			logger.debug("sql1 "+sql1);
			rs = db.select(sql1);
			if(rs!=null && rs.next()) {
				logger.debug("email address already exists");
				if (user == null) {
					user = new User();
					user.setUserId(StringUtils.s2i(StringUtils.d2s(rs.getDouble("EXT_USER_ID"))));
					user.setAccountNbr(rs.getString("EXT_ACCT_NBR"));
					logger.debug(" userid  is set to  " + user.getUserId());
					user.setUsername(rs.getString("EXT_USERNAME"));
					logger.debug("  Username  is set to  " + user.getUsername());
					user.setFirstName(StringUtils.nullReplaceWithEmpty(rs.getString("FIRSTNAME")));
					logger.debug("  FirstName  is set to  " + user.getFirstName());
					user.setLastName(StringUtils.nullReplaceWithEmpty(rs.getString("LASTNAME")));
					logger.debug(" LastName  is set to  " + user.getLastName());
					user.setMiddleInitial("");
					logger.debug(" MiddleInitial  is set to  " + user.getMiddleInitial());
					user.setTitle(rs.getString("EXT_USER_TYPE"));
					user.setPassword(rs.getString("ext_password"));
				}
				user.setIsObc("Y");
				user.setActive("Y");
				
			}else {
				logger.debug("email address doesn't exist");
				extUserId = db.getNextId("EXT_USER_ID");
				
				sql2 =" INSERT INTO EXT_USER (EXT_USER_ID, EXT_ACCT_NBR, EXT_USERNAME, EXT_PASSWORD, PIN_EXP_DT, CREATED, CREATED_BY, EXT_USER_TYPE, FIRSTNAME, MIDDLENAME, LASTNAME, OBC, DOT, ACTIVEDOT, ACTIVEOBC, CITY, STATE, ZIP,EMAIL_VERIFIED) VALUES ("+extUserId+", 0, '"+emailAddr+"', 'X6ElgaKCqETvM', null, CURRENT_DATE, null, 0, 'ONLINE USER', null, '', 'Y', 'Y', 'Y', 'Y', 'Burbank', 'Ca', '91504','Y')";
				logger.debug("sql2... "+sql2);
				
				if (user == null) {
					user = new User();
					user.setUserId(StringUtils.s2i(StringUtils.d2s(extUserId)));
					user.setAccountNbr("0");
					logger.debug(" userid  is set to  " + user.getUserId());
					user.setUsername(emailAddr);
					logger.debug("  Username  is set to  " + user.getUsername());
					user.setFirstName(StringUtils.nullReplaceWithEmpty("Online"));
					logger.debug("  FirstName  is set to  " + user.getFirstName());
					user.setLastName(StringUtils.nullReplaceWithEmpty("User"));
					logger.debug(" LastName  is set to  " + user.getLastName());
					user.setMiddleInitial("");
					logger.debug(" MiddleInitial  is set to  " + user.getMiddleInitial());
					user.setTitle("0");
					user.setPassword("X6ElgaKCqETvM");
				}
				user.setIsObc("Y");
				user.setActive("Y");
				
				db.insert(sql2);
			}
		} catch (Exception e) {
			logger.error("Exception thrown while trying to add online Permit  :" + e+e.getMessage());
			throw e;
		}finally {
			if(rs != null) {
				rs.close();
			}
		}
		return user;
	}

	//Generates a random int with n digits
	public static int generateRandomDigits(int n) {
	 int m = (int) Math.pow(10, n - 1);
	 return m + new Random().nextInt(9 * m);
	}

	public String checkAndAddPermitToEmailAddr(String actNbr, String emailAddr) throws Exception {
		Wrapper db = new Wrapper();

		String sql = null;
		String sql1 = null;
		PaymentMgrForm paymentMgrForm =new PaymentMgrForm(); 
		RowSet rs = null;
		String actId=null;
		String statusMsg="";
		int peopleId=0;
		int count=0;
		String onlineFlag="N";
		try {
			sql= "SELECT * FROM ACTIVITY WHERE UPPER(ACT_NBR)='"+actNbr.trim().toUpperCase()+"'";
			rs= db.select(sql);
			while(rs != null && rs.next()) {
				actId = rs.getString("act_id");
			}
			if(actId != null) {
				sql1 = "select * from people where EMAIL_ADDR='"+emailAddr+"' and people_type_id="+Constants.PEOPLE_CONTACT;
				logger.debug("sql1... "+sql1);
				rs = db.select(sql1);
				if (rs.next()) {
					peopleId = rs.getInt("PEOPLE_ID");
				}
				logger.debug("peopleId... "+peopleId);
				if(peopleId == 0) {
					peopleId = db.getNextId("PEOPLE_ID");
					String name[]=emailAddr.split("@");
					logger.debug("emailAddr.... "+emailAddr);
					logger.debug("people name will be.... "+name[0]);
					sql ="INSERT INTO PEOPLE (PEOPLE_ID, PEOPLE_TYPE_ID, NAME,ADDR, CITY, STATE, ZIP,  EMAIL_ADDR, CREATED, UPDATED) VALUES ("+peopleId+", "+Constants.PEOPLE_CONTACT+", '"+name[0]+"',' ', 'BURBANK', 'CA', '91506', '"+emailAddr+"', current_date,current_date)";
					logger.debug("insert people sql... "+sql);
					db.insert(sql);
				}
				
				sql1 = "select count(*) as COUNT from ACTIVITY_PEOPLE where ACT_ID="+actId+" and PEOPLE_ID="+peopleId;
				logger.debug("sql1... "+sql1);
				rs = db.select(sql1);
				if (rs!=null && rs.next()) {
					count=rs.getInt("COUNT");
				}
				
				if(count == 0) {
					onlineFlag="Y";
					logger.debug("onlineFlag..."+onlineFlag);
					
					sql="INSERT INTO ACTIVITY_PEOPLE (ACT_ID, PEOPLE_ID, PSA_TYPE) VALUES ("+actId+","+peopleId+",'A')";
					logger.debug(sql);
					db.insert(sql);

					logger.debug("onlineFlag..."+onlineFlag);
					String apSql="INSERT INTO ONLINE_ACTIVITY_PEOPLE (ONLINE_ACT_PEOPLE_ID, ACT_ID, PEOPLE_ID, PEOPLE_TYPE, EMAIL_ADDR, ONLINE_FLAG) VALUES "
							+ "("+db.getNextId("ONLINE_ACT_PEOPLE_ID")+","+actId+","+peopleId+","+Constants.PEOPLE_CONTACT+",'"+emailAddr+"','"+onlineFlag+"')";
					logger.debug(apSql);
					db.insert(apSql);
					
					statusMsg="Permit number tagged successfully to "+emailAddr+" ";
				}else {
					statusMsg="Permit number already tagged";
				}
			}else {
				statusMsg="Entered permit number is not valid";
			}
		} catch (Exception e) {
			logger.error("Exception thrown in checkAndAddPermitToEmailAddr  :" + e+e.getMessage());
		}finally {
			if(rs != null) {
				rs.close();
			}
		}
		return statusMsg;
	}


	public List getComboActList(String emailAddr) {
		List actList = new ArrayList();
		logger.debug("in getComboActList"+emailAddr);
		MyPermitForm myPermitForm = null;
		Activity activity = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		try {
			StringBuilder sb = new StringBuilder();
			sb.append(" select distinct A.ACT_ID,A.SPROJ_ID,A.ACT_TYPE,A.ACT_NBR,A.STATUS as act_stat,P.email_addr, ");
			sb.append(" LAT.description as Type,LAS.description as Status,A.APPLIED_DATE,LA.STR_NO ,LA.STR_MOD,SL.PRE_DIR,SL.STR_NAME,SL.STR_TYPE,LA.CITY,LA.STATE,  ");
			sb.append(" LA.ZIP, LAS.ONLINE_PRINT  from Activity A  Left outer join ACT_SUBTYPE AST on A.ACT_ID=AST.ACT_ID and AST.ACT_SUBTYPE_ID > 0   ");
			sb.append(" Left outer join ACTIVITY_PEOPLE AP on A.ACT_ID=AP.ACT_ID   ");
			sb.append(" Left outer join People P on P.People_ID= Ap.PEople_ID   ");
			sb.append(" Left outer join LKUP_ACT_TYPE LAT on LAT.TYPE=A.ACT_TYPE  ");
			sb.append(" Left outer join LKUP_ACT_ST LAS on LAS.STATUS_ID=A.status  ");
			sb.append(" LEFT outer join LSO_ADDRESS LA on LA.ADDR_ID=A.ADDR_ID   ");
			sb.append(" Left outer join Street_list SL on SL.STREET_ID=LA.Street_ID  ");
//			sb.append(" Left outer join ONLINE_ACTIVITY_PEOPLE OAP on A.ACT_ID=OAP.ACT_ID AND AP.PEOPLE_ID=OAP.PEOPLE_ID AND AP.ACT_ID=OAP.ACT_ID ");
			sb.append(" Left outer join SUB_PROJECT SP  on A.SPROJ_ID = SP.SPROJ_ID left outer join ONLINE_PAYMENT_FLAG OPF on A.ACT_ID = OPF.ACT_ID where ");
			sb.append(" lower(P.email_addr) = ").append(StringUtils.checkString(emailAddr.toLowerCase())).append("     ");
			sb.append(" and a.act_nbr not in ('BL%','BT%')" ); 
//			 and p.PEOPLE_TYPE_ID="+Constants.PEOPLE_APPLICANT
			sb.append(" and OPF.READY_TO_PAY='Y' order by A.ACT_ID desc  ");	
			logger.debug(sb.toString());
			
			rs = new Wrapper().select(sb.toString());
			while (rs.next()) {
				myPermitForm = new MyPermitForm();
				myPermitForm.setActivityId(rs.getString("act_id"));
				myPermitForm.setSprojId(rs.getString("sproj_id"));
				myPermitForm.setActivityNo(rs.getString("act_nbr"));
				myPermitForm.setActivityType(rs.getString("TYPE"));
				myPermitForm.setActivityStatus(rs.getString("STATUS"));
				myPermitForm.setActivityDate(rs.getString("APPLIED_DATE"));
				myPermitForm.setActivityTypeCode(rs.getString("ACT_TYPE"));
				myPermitForm.setAddress(StringUtils.nullReplaceWithEmpty(rs.getString("STR_NO")) + " " + StringUtils.nullReplaceWithEmpty(rs.getString("STR_MOD")) + " " + StringUtils.nullReplaceWithEmpty(rs.getString("PRE_DIR")) + " " + StringUtils.nullReplaceWithEmpty(rs.getString("STR_NAME")) + " " + StringUtils.nullReplaceWithEmpty(rs.getString("STR_Type")));
				
				logger.debug("getOnlineFlag... "+myPermitForm.getOnlineFlag());
				String amt = formatter.format(getAmount(rs.getString("act_id")));
				logger.debug("amt... "+amt);
				myPermitForm.setAmount(amt);
				myPermitForm.setAmnt(StringUtils.d2s(getAmount(rs.getString("act_id"))));
				logger.debug("getAmount... "+myPermitForm.getAmount());
				myPermitForm.setEmailAddr(rs.getString("email_addr"));
				String cartSql = "";
				cartSql = "SELECT count(c.cart_id) as countCart FROM CART C LEFT JOIN CART_DETAILS CD ON C.CART_ID = CD.CART_ID WHERE C.email_addr='"+rs.getString("email_addr")+"' AND C.CART_ID="+getCartId(rs.getString("email_addr"))+" AND CD.ACTIVITY_ID="+rs.getString("act_id");
				logger.debug(""+cartSql);
				rs1 = db.select(cartSql);
				if(rs1 != null && rs1.next()) {
					if(rs1.getInt("countCart") >0) {
						myPermitForm.setAddedToCart(true);
					}else {
						myPermitForm.setAddedToCart(false);
					}
					
				}
				

				String onlineActPeople="select distinct ONLINE_ACT_PEOPLE_ID,case when ONLINE_FLAG = 'Y' then 'Y'  when ONLINE_FLAG='N' THEN 'N'  ELSE  'N' END AS ONLINE_FLAG  from ONLINE_ACTIVITY_PEOPLE where ACT_ID="+rs.getString("act_id")+" and email_addr='"+rs.getString("email_addr")+"'";
				logger.debug("onlineActPeople   "+onlineActPeople);
				rs1 = db.select(onlineActPeople);
				if(rs1 != null && rs1.next()) {
					myPermitForm.setOnlineFlag(StringUtils.nullReplaceWithEmpty(rs1.getString("ONLINE_FLAG")));
					myPermitForm.setOnlineActPeopleId(StringUtils.nullReplaceWithZero(rs1.getString("ONLINE_ACT_PEOPLE_ID")));
				}else {
					myPermitForm.setOnlineFlag("N");
					myPermitForm.setOnlineActPeopleId("0");				
				}

				logger.debug("getOnlineActPeopleId   "+myPermitForm.getOnlineActPeopleId()+"  getOnlineFlag   "+myPermitForm.getOnlineFlag());
				logger.debug("act id "+myPermitForm.getActivityId());
				logger.debug("isAddedToCart"+myPermitForm.isAddedToCart());
				actList.add(myPermitForm);
				
			}
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
		}finally {
			if (rs!=null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (rs1!=null) {
				try {
					rs1.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return actList;
	}


	public String removeActivity(String peopleId,String actId,String email) throws Exception {
		logger.debug("In removeActivity...");
		Wrapper db = new Wrapper();
		String sql = null;
		String selectSql = null;
		String activityId = null;
		RowSet rs = null;
		String displayMsg="";
		try {
			//to get the total fee amount
			selectSql = "SELECT act_id,PEOPLE_ID FROM ACTIVITY_PEOPLE WHERE PEOPLE_ID="+peopleId+" and act_id="+actId;
			logger.debug("selectSql in removeActivity... "+selectSql);
			rs=db.select(selectSql);
			while(rs!=null && rs.next()) {
				activityId = rs.getString("act_id");
			}
			if(activityId != null) {
				sql = "DELETE FROM ACTIVITY_PEOPLE WHERE PEOPLE_ID ="+peopleId+" AND ACT_ID="+actId;
				logger.debug("sql... "+sql);
				db.update(sql);				

				String onlineSql = "DELETE FROM ONLINE_ACTIVITY_PEOPLE WHERE PEOPLE_ID ="+peopleId+" AND EMAIL_ADDR='"+email+"' AND ACT_ID="+actId;
				logger.debug("onlineSql... "+onlineSql);
				db.update(onlineSql);	
				
				displayMsg = "Permit untagged successfully";
			}
			logger.debug("End of removeactivity...");
		} catch (Exception e) {
			logger.error("Exception thrown while trying to remove online Permit  :" + e+e.getMessage());
			throw e;
		}finally {
			if(rs != null) {
				rs.close();
			}
		}
		return displayMsg;	
	}


	public String getPeopleID(String emailAddr) throws Exception {
		Wrapper db = new Wrapper();
		String sql = null;
		RowSet rs = null;
		String peopleId="";
		try {
			//to get the total fee amount
			sql = "SELECT PEOPLE_ID FROM PEOPLE WHERE email_addr='"+emailAddr+"' and people_type_id="+Constants.PEOPLE_CONTACT;
			logger.debug("selectSql... "+sql);
			rs=db.select(sql);
			while(rs!=null && rs.next()) {
				peopleId = rs.getString("PEOPLE_ID");
			}
		} catch (Exception e) {
			logger.error("Exception thrown while trying to remove online Permit  :" + e+e.getMessage());
			throw e;
		}finally {
			if(rs != null) {
				rs.close();
			}
		}
		return peopleId;
	}

	public CartDetailsForm getCartInfo(String cartId) throws Exception {
		Wrapper db = new Wrapper();
		String sql = null;
		RowSet rs = null;
		CartDetailsForm cartDetailsForm = new CartDetailsForm();

		try {
			sql = "SELECT COUNT(CD.CART_ID) AS COUNT_CART_ID, C.TOTAL_AMOUNT FROM CART C LEFT JOIN CART_DETAILS CD ON C.CART_ID = CD.CART_ID WHERE c.CART_ID="+cartId+" AND C.ACTIVE='Y' AND EXPIRE_DATE >= CURRENT_DATE GROUP BY CD.CART_ID,C.TOTAL_AMOUNT";
			logger.debug("Sql... "+sql);
			rs=db.select(sql);
			while(rs!=null && rs.next()) {
				if(rs.getString("TOTAL_AMOUNT").equalsIgnoreCase("0")) {
					cartDetailsForm.setTotalFeeAmount(formatter.format(0.00));
				}else {
					cartDetailsForm.setTotalFeeAmount(formatter.format(StringUtils.s2d(rs.getString("TOTAL_AMOUNT"))));				
				}
				logger.debug("total amount... "+rs.getString("TOTAL_AMOUNT"));
				logger.debug("total getTotalFeeAmount ... "+cartDetailsForm.getTotalFeeAmount());
				cartDetailsForm.setCount(rs.getString("COUNT_CART_ID"));
			}
		} catch (Exception e) {
			logger.error("Exception thrown while trying to remove online Permit  :" + e+e.getMessage());
			throw e;
		}finally {
			if(rs != null) {
				rs.close();
			}
		}
		return cartDetailsForm;
	}

	public String getCartId(String emailAddr) throws Exception {
		Wrapper db = new Wrapper();
		String sql = null;
		RowSet rs = null;
		String cartId = null;
		try {
			sql = "SELECT CART_ID FROM CART WHERE EMAIL_ADDR='"+emailAddr+"' AND ACTIVE='Y' AND EXPIRE_DATE >= CURRENT_DATE";
			logger.debug("Sql... "+sql);
			rs=db.select(sql);
			while(rs!=null && rs.next()) {
				cartId = rs.getString("CART_ID");
			}
			logger.debug("cartId.... "+cartId);
		} catch (Exception e) {
			logger.error("Exception thrown:" + e+e.getMessage());
			throw e;
		}finally {
			if(rs != null) {
				rs.close();
			}
		}
		return cartId;
	}


	public void updateCartStatus(String cartId) throws Exception {
		logger.debug("In updateCartStatus");
		Wrapper db = new Wrapper();
		String sql = null;

		try {
			sql = "UPDATE CART SET ACTIVE='N' WHERE EXPIRE_DATE = CURRENT_DATE";
			logger.debug("sql... "+sql);
			db.update(sql);
			
		} catch (Exception e) {
			logger.error("Exception thrown while trying to updateCartStatus  :" + e+e.getMessage());
			throw e;
		}
	}


	public void updateCartInfoInitially(String cartId) throws Exception {
		logger.debug("In updateCartInfoInitially");
		Wrapper db = new Wrapper();
		String sql = null;
		String updateSql = null;
		RowSet rs = null;
		String actId="";
		double totalAmount = 0.00;
		int count=0;
	    DecimalFormat df2 = new DecimalFormat("#.##");

		try {
			sql = "SELECT * FROM CART_DETAILS WHERE CART_ID="+cartId;
			logger.debug("sql1 "+sql);
			rs = db.select(sql);
			while(rs!=null && rs.next()) {
				actId = rs.getString("ACTIVITY_ID");
				logger.debug("getAmount(actId)   "+getAmount(actId));
				if((getAmount(actId) == 0) || (getAmount(actId) == 0.00)) {
					updateSql="DELETE FROM CART_DETAILS WHERE CART_ID="+cartId;
					logger.debug("updateCartsql... "+updateSql);
					db.update(updateSql);
				}else {
					updateSql="UPDATE CART_DETAILS SET FEE_AMOUNT="+getAmount(actId)+" WHERE CART_ID="+cartId+" AND ACTIVITY_ID="+actId;
					logger.debug("updateCartsql... "+updateSql);
					db.update(updateSql);
				}
			}
			
			//to get the total fee amount
					sql = "SELECT CART_ID,FEE_AMOUNT FROM CART_DETAILS WHERE CART_ID="+cartId;
					logger.debug("selectSql... "+sql);
					rs=db.select(sql);
					while(rs!=null && rs.next()) {
						totalAmount = totalAmount + rs.getDouble("FEE_AMOUNT");
						logger.debug("totalFeeAmount..."+totalAmount);
					}
					
					// To check whether cart table has to insert or update
					sql = "SELECT count(CART_ID) as count FROM CART WHERE CART_ID="+cartId;
					logger.debug("selectSql... "+sql);
					rs=db.select(sql);
					while(rs!=null && rs.next()) {
						count = rs.getInt("count");
					}
					
					if(count >0) {
						sql = "UPDATE CART SET TOTAL_AMOUNT ="+df2.format(totalAmount)+" WHERE CART_ID="+cartId;
						logger.debug("updateCartsql... "+sql);
						db.update(sql);
					}
					
		} catch (Exception e) {
			logger.error("Exception thrown while trying to updateCartInfoInitially  :" + e+e.getMessage());
			throw e;
		}finally {
			if(rs != null) {
				rs.close();
			}
		}
	}


	public void updateCartInfoBasedOnActivities(String cartId,List actList) throws Exception {
		logger.debug("In updateCartInfoBasedOnActivities");
		Wrapper db = new Wrapper();
		String sql = null;
		RowSet rs = null;
		List<String> actIds = new ArrayList<>();
		try {
			for(int k=0;k<actList.size();k++) {
				MyPermitForm form = new MyPermitForm();
				form=(MyPermitForm) actList.get(k);
//				logger.debug(form.getActivityId());
				actIds.add(form.getActivityId());
			}
			logger.debug("actIds size.. "+actIds.size());
			for(int i=0;i<actIds.size();i++) {
				sql = "SELECT * FROM CART C LEFT JOIN CART_DETAILS CD ON C.CART_ID=CD.CART_ID WHERE c.CART_ID="+cartId;
				logger.debug("sql1 "+sql);
				rs = db.select(sql);
				while(rs!=null && rs.next()) {
					String actId = rs.getString("ACTIVITY_ID");
//					 logger.debug("actId"+actId);
//					 logger.debug(!actIds.contains(actId));
					if(!actIds.contains(actId)) {
						String deleteSql = "DELETE FROM CART_DETAILS WHERE CART_ID="+cartId+" AND ACTIVITY_ID="+actId;
						logger.debug("deleteSql.. .. "+deleteSql);
						db.update(deleteSql);
					}
				}
			}
					
		} catch (Exception e) {
			logger.error("Exception thrown while trying to updateCartInfoInitially  :" + e+e.getMessage());
			throw e;
		}finally {
			if(rs != null) {
				rs.close();
			}
		}
	}

	public int checkActNo(String actNbr,String emailAddr) throws Exception {
		Wrapper db = new Wrapper();
		String sql = null;
		String sql1 = null;
		RowSet rs = null;
		String actId=null;
		int peopleId=0;
		int actPeopleId=0;
		double amt=0.00;
		try {
			if(actNbr.startsWith("BL") || actNbr.startsWith("BT")) {
				return -1;
			}else {		
//				actNbr = actNbr.trim();
				//sql= "SELECT a.act_id,las.online_flag FROM ACTIVITY A Left outer join LKUP_ACT_ST LAS on LAS.STATUS_ID=A.status WHERE UPPER(A.ACT_NBR)='"+actNbr.trim().toUpperCase()+"' and A.ACT_NBR NOT IN ('BL%','BT%') and LAS.ONLINE_FLAG='Y' ";
				sql= "SELECT a.act_id,OPF.READY_TO_PAY FROM ACTIVITY A left outer join ONLINE_PAYMENT_FLAG OPF on A.ACT_ID = OPF.ACT_ID WHERE UPPER(A.ACT_NBR)='"+actNbr.trim().toUpperCase()+"' and A.ACT_NBR NOT IN ('BL%','BT%') and OPF.READY_TO_PAY='Y' ";
				logger.debug("sql  "+sql);
				rs= db.select(sql);
				if(rs.next()) {
					actId = rs.getString("act_id");
					logger.debug("actId... "+rs.getString("act_id"));
					logger.debug("online_flag... "+rs.getString("READY_TO_PAY"));
				}
				logger.debug("actId.. "+actId);
				if(actId != null) {
					sql1 = "select sum(FEE_AMNT) - sum(fee_paid) as AMT from activity_fee where ACTIVITY_ID="+actId;
					logger.debug("sql1... "+sql1);
					rs = db.select(sql1);		
					if (rs != null && rs.next()) {
						amt = rs.getDouble("AMT");
					}
					if(amt <=0 || amt <=0.00) {
						return -2;
					}
					
					sql1 = "select * from people p left join ACTIVITY_PEOPLE ap on p.people_id=ap.people_id where ap.ACT_ID="+actId+" and p.email_addr='"+emailAddr+"' ";
					logger.debug("sql1... "+sql1);
					rs = db.select(sql1);		
					if (rs != null && rs.next()) {
						peopleId = rs.getInt("PEOPLE_ID");
					}
					logger.debug("peopleId... "+peopleId);
					
					if(peopleId != 0) {	
						return -3;
					}else {					
						return 0;
					}
				}else {
					return -1;
			}
		}
		} catch (Exception e) {
			logger.error("Exception thrown while trying to add online Permit  :" + e+e.getMessage());
		}finally {
			if(rs != null) {
				rs.close();
			}
		}
		return actPeopleId;
	}

	public PaymentMgrForm saveApplicationCartDetails(PaymentMgrForm paymentMgrForm,String from) throws Exception {
		  
		logger.debug("in saveApplicationDetails ..."+paymentMgrForm.toString());
		Wrapper db = new Wrapper();
	    int userId=0;
	    RowSet rs = null;
	    RowSet rs1 = null;
	    RowSet contractorRs = null;
	    RowSet deptRs=null;
		ResultSet rs11 = null;
		int peopleId=-1;
		String peopleName=null;
		String levelId=null;
		String email="";
		String departmentCode="";
		List<String> actIds = new ArrayList<>();
		String active="";
		int transsId = 0;
		String comments="Online credit card payment";
		if(from != null || from != "") {
			comments = from +" "+ comments;
		}
		try{

			if(paymentMgrForm.getCartId() != null) {
				if(paymentMgrForm.getResponseMsg().equalsIgnoreCase("SUCCESS")) {
					active="N";
				}else {
					active="Y";
				}
				String cartSql = "UPDATE CART SET PAID_AMOUNT ='"+paymentMgrForm.getAmount()+"', STATUS='"+paymentMgrForm.getResponseMsg()+"', ACTIVE='"+active+"', ONLINE_TRANSACTION_ID="+paymentMgrForm.getOnlineTxnId()+" WHERE CART_ID="+paymentMgrForm.getCartId();
				logger.debug("cartSql..."+cartSql);
				db.update(cartSql);
				
				String cartDetailsSql = "SELECT * FROM CART C LEFT JOIN CART_DETAILS CD ON C.CART_ID=CD.CART_ID WHERE C.CART_ID="+paymentMgrForm.getCartId();
				logger.debug("cartDetailsSql.. "+cartDetailsSql);
				rs = db.select(cartDetailsSql);

				while(rs!=null && rs.next()) {
					logger.debug("activity_id...  "+rs.getString("activity_id"));
					String actId = rs.getString("activity_id");
					actIds.add(actId);
					email = rs.getString("EMAIL_ADDR");	
					paymentMgrForm.setEmail(email);
				}

				logger.debug("email.. "+email);
					
					for(int j=0;j<actIds.size();j++) {
						levelId = actIds.get(j);	
						if(levelId != null) {
							String updateActivityQuery = "";
							String qtyOther="";
							
							//CHECKING PEOPLE IS EXIST OR NOT WITH PEOPLE TYPE CONTACT(PEOPLE_TYPE_ID=21)

							logger.debug("paymentMgrForm.getEmail()  "+email);
							if(email != null) {
								String peopleCheck="select p.PEOPLE_ID from people p left join activity_people ap on p.people_id = ap.people_id where p.EMAIL_ADDR='"+email+"' and ap.act_id="+levelId;
								logger.debug("peopleCheck.. "+peopleCheck);
								//Checking people exist or not
								rs1=db.select(peopleCheck);
								if(rs1 != null && rs1.next()) {
									peopleId=rs1.getInt("PEOPLE_ID");
							    	logger.debug("inside resultset $$$$ ..."+peopleId);
								}
							}		 		 		 
							
							//Checking people created by online or elms
							String onlineActPeople="select * from online_activity_people  where PEOPLE_ID="+peopleId+" AND ACT_ID="+levelId+" AND EMAIL_ADDR='"+email+"' AND ONLINE_FLAG='Y'";
							logger.debug("onlineActPeople.. "+onlineActPeople);
							
							rs1=db.select(onlineActPeople);
							if(rs1 != null && rs1.next()) {
								if((rs1.getString("ONLINE_FLAG")).equalsIgnoreCase("Y")) {
									String updatePeople="UPDATE PEOPLE SET NAME='"+StringUtils.nullReplaceWithEmpty(StringUtils.handleSingleQuot(paymentMgrForm.getName()))+"' WHERE PEOPLE_ID="+peopleId+" AND EMAIL_ADDR='"+email+"' AND (NAME IS NULL OR NAME='')";
									logger.debug("updatePeople... "+updatePeople);
									db.update(updatePeople);
								}
							}
							
							 logger.debug("peopleID... "+peopleId);
							logger.debug("paymentMgrForm.getEmail()  "+email);
							//FEE UPDATES

							logger.debug("activity type..."+paymentMgrForm.toString());
							 int pymntId = db.getNextId("PAYMENT_ID");
							 logger.debug("pymntId..."+pymntId);
							 
							List<Fee> feeList =  getFees(levelId);
							double totalAmnt = 0;
							for(int i=0;i<feeList.size();i++) {
								totalAmnt = totalAmnt + feeList.get(i).getTotalFee();
							}						

							//UPDATE CART_DETAILS TABLE
							updateActivityQuery = "UPDATE CART_DETAILS SET PYMNT_ID ="+pymntId+" WHERE ACTIVITY_ID ="+levelId+" AND CART_ID="+paymentMgrForm.getCartId();
							logger.debug("updateActivityQuery..."+updateActivityQuery);
							db.update(updateActivityQuery);
							
							String deptCode = "";
							String query="SELECT LAT.DEPT_ID FROM ACTIVITY A LEFT JOIN LKUP_ACT_TYPE LAT ON A.ACT_TYPE = LAT.TYPE WHERE A.ACT_ID="+levelId;
							rs1 = db.select(query);
							if (rs1.next()) {
								String deptSql = "SELECT DEPT_CODE FROM DEPARTMENT WHERE DEPT_ID = " + rs1.getString("DEPT_ID");;
								logger.info(deptSql);
								deptRs = db.select(deptSql);
								if (deptRs.next()) {
									departmentCode = deptRs.getString("DEPT_CODE");
								}
							}
							
						    userId = getOnlineUserID(email);
			 		 		 
							if(userId != 0){

								String sql = "SELECT * FROM EXT_USER WHERE EXT_USER_ID="+userId;
								logger.debug("sql1 "+sql);
								rs = db.select(sql);
								if(rs!=null && rs.next()) {
									if(rs.getString("FIRSTNAME").equalsIgnoreCase("ONLINE USER")) {
										String updateUser="UPDATE EXT_USER SET FIRSTNAME='"+paymentMgrForm.getName()+"' WHERE EXT_USER_ID="+userId;
										logger.debug("updateUser... "+updateUser);
										db.update(updateUser);									
									}
								}
							}
							
							//INSERT INTO PAYMENT TABLE
							 String insertPayment="INSERT INTO PAYMENT (PYMNT_ID, PYMNT_METHOD, PYMNT_TYPE, PYMNT_DT, PEOPLE_ID, PAYEE, PYMNT_AMNT,COMNT,ENTER_BY_ID, AUTHORIZATION_ID, DEPT_CODE, ONLINETXNID) values("+pymntId+",'creditcard','1',current_date,"+peopleId+",'"+StringUtils.nullReplaceWithEmpty(StringUtils.handleSingleQuot(paymentMgrForm.getName()))+"', "+totalAmnt+",'"+comments+"',"+userId+",'"+StringUtils.nullReplaceWithEmpty(paymentMgrForm.getAuthorizedById())+"','"+departmentCode+"',"+paymentMgrForm.getOnlineTxnId()+")";

							 logger.debug("insertPayment..."+insertPayment);
	  						 db.update(insertPayment);

							logger.debug("activity type..."+paymentMgrForm.toString());
							logger.debug("feeList..."+feeList.size());
							logger.debug("qtyOther..."+qtyOther);
							
							if(StringUtils.nullReplaceWithEmpty(qtyOther).equalsIgnoreCase("") || qtyOther.equalsIgnoreCase("0")) {
								qtyOther="1";
							}
							
							String actID=null;		
							String feeAccount=null;

							for(int i=0;i<feeList.size();i++) {
								 transsId = db.getNextId("TRANS_ID");
								 logger.debug("transsId..."+transsId);
								 
								 feeAccount = getFeesAccountForFeeId(feeList.get(i).getFeeId());
								 
								 logger.debug("fee total 630..."+feeList.get(i).getFeeTotal());
								 logger.debug("fee total 631..."+feeList.get(i).getTotalFee());
								 logger.debug("fee factor 632..."+feeList.get(i).getFeeFactor());
								 
								//INSERT INTO PAYMENT DETAIL TABLE
								String insertPaymentDetail="INSERT INTO PAYMENT_DETAIL (TRANS_ID, PYMNT_ID, ACT_ID, FEE_ID, PEOPLE_ID, AMNT, COMMENTS, FEE_ACCOUNT) "
										+ "values( "+transsId+","+pymntId+","+levelId+","+feeList.get(i).getFeeId()+","+peopleId+","+feeList.get(i).getTotalFee()+", '"+comments+"','"+ feeAccount +"')";
								logger.debug("insertPaymentDetail..."+insertPaymentDetail);
								db.update(insertPaymentDetail);

								//CHECKING IN ACTIVITY_FEE TABLE ROW IS EXIST OR NOT
								if(StringUtils.s2i(levelId) != 0) {
									String activityFeeCheck="select * from ACTIVITY_FEE where ACTIVITY_ID="+levelId+" and FEE_ID = "+feeList.get(i).getFeeId();
									logger.debug("activityFeeCheck.. "+activityFeeCheck);
									 rs=db.select(activityFeeCheck);
							    	if(rs!=null && rs.next()) {
							    		actID=rs.getString("ACTIVITY_ID");
							    	}					    	
									logger.debug("inside resultset $$$$ ..."+actID);
								}
								if(actID != null) {
									String updateActFee="";	
									int contractorPeopleId = 0;
									//UPDATE INTO ACTIVITY_FEE TABLE
									if(feeList.get(i).getFeeId() == 0){
										contractorRs = db.select("select PEOPLE_ID from activity_fee where activity_id="+actID+" and fee_id=0 and fee_paid=0");
											while(contractorRs != null && contractorRs.next()){
												contractorPeopleId = contractorRs.getInt("PEOPLE_ID");
											}
											if(contractorPeopleId <= 0)	{
												contractorRs = db.select("select PEOPLE_ID from activity_fee where activity_id="+actID+" and fee_id=0 and  FEE_AMNT > fee_paid");
												if(contractorRs != null && contractorRs.next()){
													contractorPeopleId = contractorRs.getInt("PEOPLE_ID");
												}
											}
										updateActFee = "UPDATE ACTIVITY_FEE SET FEE_PAID="+feeList.get(i).getFeeAmnt()+" where ACTIVITY_ID="+levelId+ " AND FEE_ID="+feeList.get(i).getFeeId()+" AND PEOPLE_ID = "+contractorPeopleId;
									}else{
									updateActFee = "UPDATE ACTIVITY_FEE SET FEE_PAID="+feeList.get(i).getFeeAmnt()+" ,PEOPLE_ID="+peopleId+", COMMENTS='"+comments+"' where ACTIVITY_ID="+levelId+ " AND FEE_ID="+feeList.get(i).getFeeId();
									}
									logger.debug("update else condition of :: "+updateActFee);
									logger.debug("updateActFee..."+updateActFee);
									db.update(updateActFee);
								}			
							}
							//untag the activity after payment
							String onlineSql="select * FROM ONLINE_ACTIVITY_PEOPLE WHERE PEOPLE_ID ="+peopleId+" AND EMAIL_ADDR='"+email+"' AND ONLINE_FLAG='Y' AND ACT_ID="+levelId;
							rs11 = db.select(onlineSql);
							if(rs11 != null && rs11.next()) {
									String sql = "DELETE FROM ACTIVITY_PEOPLE WHERE PEOPLE_ID ="+peopleId+" AND ACT_ID="+levelId;
									logger.debug("sql... "+sql);
									db.update(sql);				

									String online = "DELETE FROM ONLINE_ACTIVITY_PEOPLE WHERE PEOPLE_ID ="+peopleId+" AND EMAIL_ADDR='"+email+"' AND ACT_ID="+levelId;
									logger.debug("onlineSql... "+online);
									db.update(online);
							}
							
						}
					}
			}
			

			if(email != null) {
			

				ActivityAgent activityAgent = new ActivityAgent();
				 String text=
				         "<table width='100%' border='1px solid' align='center'>"
				                 +"<tr align='center'>"
				                + "<td><b>Permit Number <b></td>"
				                + "<td><b>Amount<b></td>"
				                + "</tr>";

					String cartDetailsSql = "SELECT * FROM CART C LEFT JOIN CART_DETAILS CD ON C.CART_ID=CD.CART_ID WHERE C.CART_ID="+paymentMgrForm.getCartId();
					logger.debug("cartDetailsSql.. "+cartDetailsSql);
					rs = db.select(cartDetailsSql);

					while(rs!=null && rs.next()) {
						text=text+"<tr align='center'>"+"<td>" +activityAgent.getActivtiyNumber(rs.getInt("activity_id")) + "</td>"
		                        + "<td> "+formatter.format(StringUtils.s2d(rs.getString("FEE_AMOUNT"))) + "</td>"+"</tr>";
												
						logger.debug("activity_id...  "+rs.getString("activity_id"));
					}
					text = text+"</table>";
					logger.debug("text.. "+text);
				EmailSender emailSender = new EmailSender();
				EmailDetails emailDetails = new EmailDetails();	
				
				if(LookupAgent.getKeyValue(Constants.PROD_SERVER_FLAG).equalsIgnoreCase("N")) {
					emailDetails.setEmailId(LookupAgent.getKeyValue(Constants.EDGESOFT_SUPPORT_EMAIL));
				}else if(LookupAgent.getKeyValue(Constants.PROD_SERVER_FLAG).equalsIgnoreCase("Y")) {
	    			emailDetails.setEmailId(email);	
				}
				emailDetails.setBodyText(text);
				emailDetails.setTrans_id(paymentMgrForm.getOnlineTxnId());
				emailDetails.setLkupSystemDataMap(getLkupSystemDataMap());
				emailDetails.setEmailTemplateAdminForm(getEmailData(emailDetails,Constants.ONLINE_PAYMENT_SUCCESSFULL));
	        	emailSender.sendEmail(emailDetails);
			}
			


		}catch(Exception e){
			logger.error(""+e+e.getMessage());	
			throw new Exception();
		}finally {
			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(rs1 != null) {
				try {
					rs1.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(deptRs != null) {
				try {
					deptRs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(rs11 != null) {
				try {
					rs11.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(contractorRs != null) {
				try {
					contractorRs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return paymentMgrForm;
	}


	public String getPhoneNo(String actNbr) throws Exception {

		logger.info("Entering getPhoneNo");

		String phoneno="";
		RowSet rs =null;
		try {
			String sql = "select d.DEPT_CONTACT from (activity a join lkup_act_type l on l.type=a.act_type) join department d on d.dept_id=l.dept_id where a.act_nbr = '" + actNbr+"'";
			logger.debug(sql);
		
			rs = new Wrapper().select(sql);
		
			if (rs.next()) {
				phoneno = rs.getString("DEPT_CONTACT");
			} else {
				phoneno = "";
			}
		
			rs.close();
			logger.info("Exiting getPhoneNo");
		}catch (Exception e) {
			logger.error(e.getMessage());
		}finally {
			if(rs!=null) {
				rs.close();
			}
		}
		return phoneno;

	}


	public List<PaymentMgrForm> getPaidActivities(String cartId) throws Exception {

		logger.info("Entering getPaidActivities");
		 List<PaymentMgrForm> list = new ArrayList<PaymentMgrForm>();
		String phoneno="";
		RowSet rs =null;
		try {
			String sql = "select * from activity a left join cart_details cd on a.act_id = cd.activity_id where cd.cart_id = '" + cartId+"'";
			logger.debug(sql);
			rs = new Wrapper().select(sql);
		
			while (rs.next()) {
				PaymentMgrForm form = new PaymentMgrForm();
				form.setLevelId(rs.getString("ACT_ID"));
				form.setActNum(rs.getString("act_nbr"));
				form.setAmount(rs.getString("fee_amount"));
				list.add(form);
			}	
			rs.close();
		}catch (Exception e) {
			logger.error(e.getMessage());
		}finally {
			if(rs!=null) {
				rs.close();
			}
		}
		return list;

	}
	public int getOnlineUserID(String emailAddress) throws Exception {
		logger.info("inside getOnlineUserID...");
		RowSet rs = null;
		int userId = 0;
		try {
			String sql = "select EXT_USER_ID from EXT_USER where lower(EXT_USERNAME) = lower('" + emailAddress + "')";
			logger.debug(sql);
			rs = new Wrapper().select(sql);
			if (rs.next()) {
				userId = rs.getInt("EXT_USER_ID");
			}

			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error("error in getting existing online users " + e.getMessage());
		}finally {
			if(rs != null) {
				rs.close();
			}
		}
		return userId;
	}

	public void updateEmailVerified(String emailAddress) throws Exception {
		logger.debug("In updateEmailVerified");
		Wrapper db = new Wrapper();
		String sql = null;
		String updateSql = null;
		RowSet rs = null;
		try {
			sql = "SELECT * FROM EXT_USER WHERE  UPPER(EXT_USERNAME) = UPPER('"+emailAddress+"')";
			logger.debug("sql1 "+sql);
			rs = db.select(sql);
			if(rs!=null && rs.next()) {
				updateSql="UPDATE EXT_USER SET EMAIL_VERIFIED='Y' WHERE EXT_USER_ID="+rs.getString("EXT_USER_ID");
				logger.debug("updateSql... "+updateSql);
				db.update(updateSql);
			}			
		} catch (Exception e) {
			logger.error("Exception thrown while trying to updateSql  :" + e+e.getMessage());
			throw e;
		}finally {
			if(rs != null) {
				rs.close();
			}
		}
	}


	public String getEmailAddr(String cartId) throws Exception {
		Wrapper db = new Wrapper();
		String sql = null;
		RowSet rs = null;
		String emailAddr = null;
		try {
			sql = "SELECT EMAIL_ADDR FROM CART WHERE CART_ID='"+cartId+"' AND ACTIVE='Y' AND EXPIRE_DATE >= CURRENT_DATE";
			logger.debug("Sql... "+sql);
			rs=db.select(sql);
			while(rs!=null && rs.next()) {
				emailAddr = rs.getString("EMAIL_ADDR");
			}
			logger.debug("emailAddr.... "+emailAddr);
		} catch (Exception e) {
			logger.error("Exception thrown:" + e+e.getMessage());
			throw e;
		}finally {
			if(rs != null) {
				rs.close();
			}
		}
		return emailAddr;
	}


	/**
		 * This method is to fetch the Email Templates based on email type
		 * @param emailDetails
		 * @throws 
		 * @return EmailTemplateAdmin Form 
		 */
		public static EmailTemplateAdminForm getEmailData(EmailDetails emailDetails,String emailType) {		

			EmailTemplateAdminForm emailTemplateAdminForm = new EmailTemplateAdminForm();
			try {		    
				logger.debug("in SECURITY_CODE_VERIFICATION");
				if(emailType != null && emailType.equalsIgnoreCase(Constants.SECURITY_CODE_VERIFICATION)) {
					emailTemplateAdminForm = getEmailTempByTempTypeId(Constants.SECURITY_CODE_VERIFICATION);
					emailTemplateAdminForm.setEmailMessage(getEmailBodyWithValues(emailTemplateAdminForm.getEmailMessage(), emailDetails));			    
				}else if(emailType != null && emailType.equalsIgnoreCase(Constants.ONLINE_PAYMENT_SUCCESSFULL)){
					emailTemplateAdminForm = getEmailTempByTempTypeId(Constants.ONLINE_PAYMENT_SUCCESSFULL);
					emailTemplateAdminForm.setEmailMessage(getEmailBodyWithValues(emailTemplateAdminForm.getEmailMessage(), emailDetails));			    
				}else if(emailType != null && emailType.equalsIgnoreCase(Constants.ONLINE_PERMIT_CANCEL_SUCCESS)){
					emailTemplateAdminForm = getEmailTempByTempTypeId(Constants.ONLINE_PERMIT_CANCEL_SUCCESS);
					emailTemplateAdminForm.setEmailMessage(getEmailBodyWithValues(emailTemplateAdminForm.getEmailMessage(), emailDetails));			    
				}else if(emailType != null && emailType.equalsIgnoreCase(Constants.ONLINE_SUBMIT_SUCCESS)){
					emailTemplateAdminForm = getEmailTempByTempTypeId(Constants.ONLINE_SUBMIT_SUCCESS);
					emailTemplateAdminForm.setEmailMessage(getEmailBodyWithValues(emailTemplateAdminForm.getEmailMessage(), emailDetails));			    
				}else if(emailType != null && emailType.equalsIgnoreCase(Constants.FORGOT_PASSWORD)){
					emailTemplateAdminForm = getEmailTempByTempTypeId(Constants.FORGOT_PASSWORD);
					emailTemplateAdminForm.setEmailMessage(getEmailBodyWithValues(emailTemplateAdminForm.getEmailMessage(), emailDetails));			    
				}else if(emailType != null && emailType.equalsIgnoreCase(Constants.REGISTRATION_SUCCESS)){
					emailTemplateAdminForm = getEmailTempByTempTypeId(Constants.REGISTRATION_SUCCESS);
					emailTemplateAdminForm.setEmailMessage(getEmailBodyWithValues(emailTemplateAdminForm.getEmailMessage(), emailDetails));			    
				}else if(emailType != null && emailType.equalsIgnoreCase(Constants.INSPECTION_SCHEDULED)){
					emailTemplateAdminForm = getEmailTempByTempTypeId(Constants.INSPECTION_SCHEDULED);
					emailTemplateAdminForm.setEmailMessage(getEmailBodyWithValues(emailTemplateAdminForm.getEmailMessage(), emailDetails));			    
				}
				
				logger.debug("Email subject :: " +emailTemplateAdminForm.getEmailSubject());
				logger.debug("Email Body Message :: " +emailTemplateAdminForm.getEmailMessage());
				
				logger.error("Email message sent successfully");
				return emailTemplateAdminForm;
			}catch (Exception e) {
				logger.error("Error in sending emails "	+ e.getMessage()+e);
				e.printStackTrace();
				return new EmailTemplateAdminForm();
			}
		}


		private static String getEmailBodyWithValues(String emailBody, EmailDetails emailDetails) {
			logger.error("emailBody :"+emailBody);
					
			if(emailBody!=null) {
				if(emailDetails.getSecurityCode()!=0) {
					if(emailBody.contains("##SECURITY_CODE##")) emailBody = emailBody.replaceAll("##SECURITY_CODE##", StringUtils.i2s(emailDetails.getSecurityCode()));
				}
				if(emailDetails.getEmailId()!= null) {
					if(emailBody.contains("##EMAIL_ADDRESS##")) emailBody = emailBody.replaceAll("##EMAIL_ADDRESS##", emailDetails.getEmailId());
				}

				if(emailDetails.getBodyText()!= null) {

					emailDetails.setBodyText(emailDetails.getBodyText().replace("<", "\\<"));
					emailDetails.setBodyText(emailDetails.getBodyText().replace(">", "\\>"));
					emailDetails.setBodyText(emailDetails.getBodyText().replace("'", "\\'"));
//					emailBody= emailDetails.getBodyText().replace(">", "\\>");
					emailDetails.setBodyText(emailDetails.getBodyText().replace("\\\"", "\""));
					emailDetails.setBodyText(emailDetails.getBodyText().replace("$", "\\$"));
					emailDetails.setBodyText(emailDetails.getBodyText().replace(".", "\\."));
					logger.debug("getBodyText... "+emailDetails.getBodyText());

					logger.debug("getBodyText... "+emailDetails.getBodyText());
					if(emailBody.contains("##TEXT##")) emailBody = emailBody.replaceAll("##TEXT##", emailDetails.getBodyText());
				}
				logger.debug("after body text.. "+emailBody);
				if(emailDetails.getSecurityCode()!=0) {
					if(emailBody.contains("##SECURITY_CODE##")) emailBody = emailBody.replaceAll("##SECURITY_CODE##", StringUtils.i2s(emailDetails.getSecurityCode()));
				}
				if(emailDetails.getPermitNo()!= null) {
					if(emailBody.contains("##ACT_NUMBER##")) emailBody = emailBody.replaceAll("##ACT_NUMBER##", emailDetails.getPermitNo());
				}
				if(emailDetails.getPermitType()!= null) {
					if(emailBody.contains("##ACT_TYPE##")) emailBody = emailBody.replaceAll("##ACT_TYPE##", emailDetails.getPermitType());
				}
				if(emailDetails.getAddress()!= null) {
					if(emailBody.contains("##ADDRESS##")) emailBody = emailBody.replaceAll("##ADDRESS##", emailDetails.getAddress());
				}
				if(emailDetails.getUserName()!= null) {
					if(emailBody.contains("##USERNAME##")) emailBody = emailBody.replaceAll("##USERNAME##", emailDetails.getUserName());
				}
				if(emailDetails.getPassword()!= null) {
					if(emailBody.contains("##PASSWORD##")) emailBody = emailBody.replaceAll("##PASSWORD##", emailDetails.getPassword());
				}
				if(emailDetails.getStatus()!= null) {
					if(emailBody.contains("##STATUS##")) emailBody = emailBody.replaceAll("##STATUS##", emailDetails.getStatus());
				}
				if(emailDetails.getType()!= null) {
					if(emailBody.contains("##TYPE##")) emailBody = emailBody.replaceAll("##TYPE##", emailDetails.getType());
				}
				if(emailDetails.getDate()!= null) {
					if(emailBody.contains("##DATE##")) emailBody = emailBody.replaceAll("##DATE##", emailDetails.getDate());
				}
				if(emailDetails.getTime()!= null) {
					if(emailBody.contains("##TIME##")) emailBody = emailBody.replaceAll("##TIME##", emailDetails.getTime());
				}
			}
			emailBody= emailBody.replace("##", "");
			emailBody= emailBody.replace("\\\"", "\"");
			logger.error("emailBody before method exit: "+emailBody);
			return emailBody;
		}
		
		
		/**
		 * This method is to fetch the Email Templates based on email type
		 * @param String
		 * @throws 
		 * @return EmailTemplateAdmin Form 
		 */
		public static EmailTemplateAdminForm getEmailTempByTempTypeId(String tempType) {
			logger.info("getEmailTempByTempTypeId("+tempType+")");
			String sql = null;
	    	EmailTemplateAdminForm emailTempForm = new EmailTemplateAdminForm();
			Wrapper db =new Wrapper();
			RowSet rs = null;
			try {
				sql = "SELECT A.EMAIL_TEMP_TYPE_ID, B.EMAIL_TEMPLATE_ID, B.EMAIL_SUBJECT, B.EMAIL_BODY FROM LKUP_EMAIL_TEMPLATE_TYPE A, EMAIL_TEMPLATE B, REF_EMAIL_TEMPLATE C WHERE A.EMAIL_TEMP_TYPE_ID = C.EMAIL_TEMP_TYPE_ID AND B.EMAIL_TEMPLATE_ID = C.EMAIL_TEMPLATE_ID ";
		    	if(tempType!=null && !tempType.equalsIgnoreCase("")) {
		    		sql = sql + " AND A.EMAIL_TEMP_TYPE = '" +tempType +"'";        		
		    	}
		    	sql = sql + " ORDER BY A.EMAIL_TEMP_TYPE";
		    	logger.debug("Sql is :: " + sql.toString());       	
		    	
		    	rs = db.select(sql);
		    	if(rs!=null && rs.next()){
		            	emailTempForm.setEmailTempTypeId(rs.getInt("EMAIL_TEMP_TYPE_ID"));
		            	emailTempForm.setEmailTempId(rs.getInt("EMAIL_TEMPLATE_ID"));
		            	emailTempForm.setEmailSubject((rs.getString("EMAIL_SUBJECT") != null ? rs.getString("EMAIL_SUBJECT") : "").trim());
		            	emailTempForm.setEmailMessage((rs.getString("EMAIL_BODY") != null ? rs.getString("EMAIL_BODY") : "").trim());  
		         }
			} catch (Exception e) {
				logger.error("Unable to get email templates by id "+e);			
			}finally {
				try {
					if(rs != null) {
						rs.close();
					}
				}catch (Exception e) {
					// TODO: handle exception
				}
			}
			return emailTempForm;
		}

	
	public TempPermitForm getTempPermit(String tempId) throws Exception {

		
		String sql = "";
		TempPermitForm tempPermitForm = null;
		
		
		String[] subTypeArray = null;
		List<String> subTypeList = new ArrayList<String>();
		
		Wrapper db = new Wrapper();
		RowSet rs = null;
		try {
			
			sql = "select et.*"+ " from cap_temp et where et.TEMP_ID=" + tempId;
			
			rs = db.select(sql);
			
			tempPermitForm = new TempPermitForm();
			if(rs.next()){
				//tempPermitForm.setDepartmentType(rs.getString("dept_id"));
				
				
				tempPermitForm.setActivityType(new AdminAgent().getActivityType(StringUtils.s2i(rs.getString("act_type_id"))).getType());
				tempPermitForm.setLsoId(rs.getString("lsoid"));
				tempPermitForm.setUserId(StringUtils.i2s(rs.getInt("created_by")));
				//tempPermitForm.setInspectionRequired(rs.getString("inspection_required"));
				//tempPermitForm.setOwnerSelection(rs.getString("owner_selection"));
			//	tempPermitForm.setSubActivityType(rs.getString("act_subtype_id"));
				//tempPermitForm.setActivityType(rs.getString("act_type"));
				tempPermitForm.setActDescription(rs.getString("act_description"));
				tempPermitForm.setStreetUnit(rs.getString("unit"));
				logger.debug("Issue date::"+rs.getDate("ISSUE_DATE").toString());
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(rs.getDate("ISSUE_DATE"));
				Date issueDate = calendar.getTime();
				tempPermitForm.setIssueDate(StringUtils.date2str(issueDate));
				//Calendar calendar = Calendar.getInstance();
				calendar.setTime(rs.getDate("END_DATE"));
				Date endDate = calendar.getTime();
				tempPermitForm.setEndDate(StringUtils.date2str(endDate));
				tempPermitForm.setActId(StringUtils.i2s(rs.getInt("LEVEL_ID")));
				//tempPermitForm.setPeople(rs.getString("people"));
				//tempPermitForm.setValuation(rs.getString("VALUATION"));
				tempPermitForm.setFirstName(rs.getString("FIRST_NAME"));
				tempPermitForm.setLastName(rs.getString("LAST_NAME"));
				tempPermitForm.setAddress(rs.getString("ADDR"));
				tempPermitForm.setCity(rs.getString("CITY"));
				tempPermitForm.setState(rs.getString("STATE"));
				tempPermitForm.setZip(rs.getString("ZIP"));
				tempPermitForm.setPhoneNbr(rs.getString("PHONE"));
				tempPermitForm.setPhoneExt(rs.getString("EXT"));
				tempPermitForm.setWorkPhone(rs.getString("WORK_PHONE"));
				tempPermitForm.setWorkExt(rs.getString("WORK_PHONE_EXT"));
				tempPermitForm.setEmailAddress(rs.getString("EMAIL_ADDR"));
				
			}
			
			sql = "select sl.str_name,sl.STR_TYPE,sl.PRE_DIR, la.str_no, la.STR_MOD, la.unit from lso_address la join street_list sl on la.street_id=sl.street_id and la.lso_id="+tempPermitForm.getLsoId();
			
			rs = db.select(sql);
			
			if(rs.next()){
				tempPermitForm.setStreetNum(rs.getString("str_no"));
				String prefixDirection = rs.getString("PRE_DIR") != null ? rs.getString("PRE_DIR") : "";
				String streetName = rs.getString("str_name") != null ? rs.getString("str_name").trim() : "";
				String streetType = rs.getString("STR_TYPE") != null ? rs.getString("STR_TYPE") : "";
				tempPermitForm.setStreetName(prefixDirection+' '+streetName+' '+streetType);
				tempPermitForm.setStreetUnit("unit");
				tempPermitForm.setStreetMod("STR_MOD");
			}
			if(rs!=null){
				rs.close();
			}
			/*if(tempPermitForm.getPeople()!=null){
				String[] people = tempPermitForm.getPeople().split(",");
				List<DisplayItem> list = new ArrayList<DisplayItem>();
				DisplayItem displayItem = null;
				for(int i=0; i<people.length; i++){
					sql = "SELECT P.PEOPLE_ID,pt.PEOPLE_TYPE_ID,P.NAME,P.ADDR,P.CITY,P.STATE,P.ZIP,PT.DESCRIPTION FROM PEOPLE P JOIN PEOPLE_TYPE PT on P.PEOPLE_TYPE_ID=PT.PEOPLE_TYPE_ID where PEOPLE_ID="+people[i];
					rs = db.select(sql);
					while (rs.next()) {
						DisplayItem di = new DisplayItem();
						di.setId(rs.getString("PEOPLE_ID"));
						di.setItem1(StringUtils.nullReplaceWithEmpty(rs.getString("NAME")));
						di.setItem2(StringUtils.nullReplaceWithEmpty(rs.getString("ADDR")));
						di.setItem3(StringUtils.nullReplaceWithEmpty(rs.getString("CITY")));
						di.setItem4(StringUtils.nullReplaceWithEmpty(rs.getString("STATE")));
						di.setItem5(StringUtils.nullReplaceWithEmpty(rs.getString("ZIP")));
						di.setItem6(StringUtils.nullReplaceWithEmpty(rs.getString("DESCRIPTION")));
						di.setItem7(rs.getString("PEOPLE_TYPE_ID"));
						list.add(di);
					}
				}
				tempPermitForm.setList(list);
			}*/
		} catch (Exception e) {
			logger.error("Error getting Temp Permit data" + e);
			throw e;
		}finally{
			try{
				if(rs != null){
				    rs.close();
				}
			}catch(Exception e){
				
			}
		}

		
		return tempPermitForm;
	
	}
	public void updateEgovTempWithApplicantInfo(elms.control.beans.online.OnlinePermitForm regForm) {
		logger.info("inside updatePeople...");
		try {

			String sql = "UPDATE CAP_TEMP SET FIRST_NAME=";
			sql = sql + StringUtils.checkString(regForm.getFirstName());

			sql = sql + ",LAST_NAME=";
			sql = sql + StringUtils.checkString(regForm.getLastName());
			
			sql = sql + ",EMAIL_ADDR=";
			sql = sql + StringUtils.checkString(regForm.getEmailAddress());

			sql = sql + ",ADDR=";
			sql = sql + StringUtils.checkString(regForm.getAddress());

			sql = sql + ",CITY=";
			sql = sql + StringUtils.checkString(regForm.getCity());

			sql = sql + ",STATE=";
			sql = sql + StringUtils.checkString(regForm.getState());

			sql = sql + ",ZIP=";
			sql = sql + StringUtils.checkString(regForm.getZip());

			sql = sql + ",PHONE=";
			sql = sql + StringUtils.checkString(StringUtils.phoneFormat(regForm.getPhoneNbr()));

			sql = sql + ",EXT=";
			sql = sql + StringUtils.checkString(regForm.getPhoneExt());

			sql = sql + ",WORK_PHONE=";
			sql = sql + StringUtils.checkString(StringUtils.phoneFormat(regForm.getWorkPhone()));

			sql = sql + ",WORK_PHONE_EXT=";
			sql = sql + StringUtils.checkString(regForm.getWorkExt());
			
			/*sql = sql + ",LIC_NO=" + StringUtils.checkString(regForm.getLicenseNbr());

			sql = sql + ",DL_ID_NO=" + StringUtils.checkString(regForm.getDlNo());
*/
			sql = sql + ",UPDATED=" + "current_timestamp";
			logger.debug("got Updated date : " + "current_timestamp");

			/*logger.debug("in updating LIC_NO WORKERS_COMP_WAIVE");
			sql = sql + ", LIC_EXP_DT=" + StringUtils.toOracleDate(regForm.getLicExpDate()) + ", WORKERS_COMP_WAIVE=" + StringUtils.checkString(regForm.getWorkersCompensationWaive()) + ", WORK_COMP_EXP_DT=" + StringUtils.toOracleDate(regForm.getStrWorkersCompExpires());
		*/
			
			sql = sql + " WHERE temp_id=";
			sql = sql + regForm.getTempId();

			logger.debug("update query :" + sql);
			new Wrapper().update(sql);
		} catch (Exception e) {
			logger.error("Error while updating people :" + e.getMessage());
		}
	}

	public int insertPeopleForGarageSaleActivity(int actId, String emailAddress) {
		int peopleId = 0;

		String sql = "select * from PEOPLE where lower(EMAIL_ADDR) = lower('" + emailAddress + "') AND PEOPLE_TYPE_ID = "+Constants.PEOPLE_APPLICANT;
		logger.debug(sql);
		RowSet rs = null;
		try {
			rs = new Wrapper().select(sql);
		
		if (rs.next()) {
			peopleId = rs.getInt("PEOPLE_ID");
		}
		if (rs != null)
			rs.close();

		if (peopleId > 0) {
			Wrapper db = new Wrapper();
			String sql1 = "INSERT INTO ACTIVITY_PEOPLE (ACT_ID,PEOPLE_ID,PSA_TYPE) VALUES (" + actId + "," + peopleId + ",'A')";
			logger.info(sql1);
			db.insert(sql1);
		}
		} catch (Exception e) {
			logger.debug(e);
		}
		finally{
			try{
				if(rs != null){
				    rs.close();
				}
			}catch(Exception e){
				
			}
		}
		return peopleId;
	}

	public int checkLNCVPermitNo(String actNo) {
		int actId = 0;
		String sql = "SELECT ACT_ID FROM ACTIVITY  WHERE ACT_NBR='"+actNo.toUpperCase()+"' AND ACT_TYPE='"+Constants.LNCV_ACT_TYPE+"' ";
		logger.debug(sql);
		RowSet rs = null;
		try {
			rs = new Wrapper().select(sql);
			if (rs.next()) {
				actId = rs.getInt("ACT_ID");
			}
		} catch (Exception e) {
			logger.debug(e);
		}
		finally{
			try{
				if(rs != null){
				    rs.close();
				}
			}catch(Exception e){
				
			}
		}
		logger.debug("actId.. "+actId);
		return actId;
	}

	public List<Integer> getActIdsAddedToCart(String cartId) {
		String sql = "SELECT ACTIVITY_ID FROM CART_DETAILS WHERE CART_ID = "+cartId;
		logger.debug(sql);
		RowSet rs = null;
		List<Integer> actIdsAddedToCart = new ArrayList<Integer>();
		try {
			rs = new Wrapper().select(sql);
			while (rs != null && rs.next()) {
				actIdsAddedToCart.add(rs.getInt("ACTIVITY_ID"));
			}
		} catch (Exception e) {
			logger.debug(e);
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {

			}
		}
		return actIdsAddedToCart;
	}
	
	public void updateCartLock(String cartId, String flag) throws Exception {
		Wrapper db = new Wrapper();
		String updateCartLocksql = null;
		RowSet rs = null;	
		
		try {
			
			db.beginTransaction();
			String updateCartsql = "UPDATE CART SET CART_LOCK_FLAG = '"+flag+"' WHERE CART_ID = "+cartId;
			logger.debug("updateCartsql = "+updateCartsql);
			db.addBatch(updateCartsql);
			String updateCartDetailsSql = "UPDATE CART_DETAILS SET CART_LOCK_FLAG = '"+flag+"' WHERE CART_ID = "+cartId;
			logger.debug("updateCartDetailsSql = "+updateCartDetailsSql);
			db.addBatch(updateCartDetailsSql);
			db.executeBatch();	
			
			logger.debug("cartId.... "+cartId);
		} catch (Exception e) {
			logger.error("Exception thrown:" + e+e.getMessage());
			throw e;
		}finally {
			if(rs != null) {
				rs.close();
			}
		}		
	}	
	
	public String checkCartLockedOrNot(String cartId, long levelId) throws Exception {
		Wrapper db = new Wrapper();
		String sql = null;
		RowSet rs = null;
		int count = 0;	
		String flag = "N";
		try {
			if(cartId != null && !"".equals(cartId)){
				sql = "SELECT count(*) as COUNT FROM CART_DETAILS WHERE CART_ID="+cartId+" and ACTIVITY_ID = "+levelId+" and CART_LOCK_FLAG = 'Y'";
				logger.debug("Sql... "+sql);
				rs=db.select(sql);
				if(rs.next()) {
					count = rs.getInt("COUNT");
				}
				if(count > 0){
					flag = "Y";
				}
			}else{
				flag = "N";
			}
			logger.debug("count.... "+count);
		} catch (Exception e) {
			logger.error("Exception thrown while getting count:" +e.getMessage());
			
		}finally {
			if(rs != null) {
				rs.close();
			}
		}
		return flag;
	}	
	/**
	 * get department description based on actId
	 * 
	 * @param int
	 * @return
	 * @throws Exception
	 */
	public String getDeptDescriptionForActId(int actId) throws Exception {
		logger.debug("getDeptDescriptionForActType(" + actId + ")");
		String GET_DEPT_DESCRIPTION_SQL = "SELECT D.DESCRIPTION_ALIAS AS DESCRIPTION,D.DEPT_CONTACT AS CONTACT FROM DEPARTMENT D LEFT OUTER JOIN LKUP_ACT_TYPE LAT ON D.DEPT_ID = LAT.DEPT_ID LEFT OUTER JOIN ACTIVITY A ON LAT.TYPE = A.ACT_TYPE WHERE A.ACT_ID = " + actId;
		logger.debug(GET_DEPT_DESCRIPTION_SQL);
        StringBuilder department = new StringBuilder();
        String dept = "";
        RowSet rs = null;
		try {

			rs = new Wrapper().select(GET_DEPT_DESCRIPTION_SQL);

			if (rs != null && rs.next()) {				
				department.append(rs.getString("DESCRIPTION"));
				logger.debug("obtained department:  " + department);	
				department.append(" staff (");
				department.append(rs.getString("CONTACT"));
				department.append(").");
			}
			if(department != null){
				dept = department.toString();
			}
		logger.debug("obtained department and contact:  "+dept);
			return dept;
		}catch (Exception e) {
			logger.error("Error in getDeptDescriptionForActId " + e.getMessage());
			e.printStackTrace();
			throw e;
		}// Closing RowSet added by Gayathri
		finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException Ignore) {
					logger.warn("Not able to close the Rowset");
				}
			}
		}
	}
	public String getPhoneNoFromDepartment(int deptId) throws Exception {

		logger.info("Entering getPhoneNo");

		String phoneno="";
		RowSet rs =null;
		try {
			String sql = "SELECT DEPT_CONTACT FROM DEPARTMENT WHERE DEPT_ID = "+deptId;
			logger.debug(sql);
		
			rs = new Wrapper().select(sql);
		
			if (rs.next()) {
				phoneno = rs.getString("DEPT_CONTACT");
			} else {
				phoneno = "";
			}
		
			rs.close();
			logger.info("Exiting getPhoneNo");
		}catch (Exception e) {
			logger.error(e.getMessage());
		}finally {
			if(rs!=null) {
				rs.close();
			}
		}
		return phoneno;

	}


}
