package elms.agent;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.sql.RowSet;

import org.apache.log4j.Logger;

import elms.app.admin.ActivityStatus;
import elms.app.admin.ActivitySubType;
import elms.app.admin.ActivityType;
import elms.app.admin.FeeType;
import elms.app.admin.Landmark;
import elms.app.admin.LookupType;
import elms.app.admin.LotType;
import elms.app.admin.MicrofilmStatus;
import elms.app.admin.ParkingZone;
import elms.app.admin.PayType;
import elms.app.admin.PeopleByType;
import elms.app.admin.PeopleType;
import elms.app.admin.PlanCheckStatus;
import elms.app.admin.ProjectName;
import elms.app.admin.ProjectStatus;
import elms.app.admin.ProjectType;
import elms.app.admin.SubProjectStatus;
import elms.app.admin.SubProjectSubType;
import elms.app.admin.SubProjectType;
import elms.app.bl.ApplicationType;
import elms.app.bl.ApprovalStatus;
import elms.app.bl.ExemptionType;
import elms.app.bl.OwnershipType;
import elms.app.bl.QuantityType;
import elms.app.common.Agent;
import elms.app.common.DisplayItem;
import elms.app.common.Module;
import elms.app.common.ObcFile;
import elms.app.common.PickList;
import elms.app.finance.LookupFee;
import elms.app.lso.Street;
import elms.app.lso.Use;
import elms.app.people.EyeColorType;
import elms.app.sitedata.SiteUse;
import elms.common.Constants;
import elms.control.beans.CategoryForm;
import elms.control.beans.IncidentProblemForm;
import elms.control.beans.online.HolidayEditorForm;
import elms.control.beans.online.OnlinePermitForm;
import elms.exception.AgentException;
import elms.security.Department;
import elms.security.Group;
import elms.security.Role;
import elms.security.User;
import elms.util.Operator;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

public class LookupAgent extends Agent {
	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(LookupAgent.class.getName());

	/**
	 * The SQL String
	 */
	static String sql = "";

	//values from lkup_system table
	public static String KEY_VALUE = "UNDEFINED";
	
	public LookupAgent() {
	}

	/**
	 * Gets the Contractor's State License board URL
	 * 
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public static String getSystemURL(String urlName) throws Exception {
		logger.info("getSystemURL(" + urlName + ")");
		String urlValue = "";
		Wrapper db = new Wrapper();

		try {
			String sql = "SELECT VALUE FROM LKUP_SYSTEM WHERE NAME=" + StringUtils.checkString(urlName);
			RowSet rs = db.select(sql);
			if (rs.next()) {
				urlValue = rs.getString(1);
			}
			return urlValue;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the Contractor's State License board URL
	 * 
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public static String getContractorsStateLicenseBoardURL() throws Exception {

		return getSystemURL("CONTRACTORS_STATE_LICENSE_BOARD_URL");

	}

	public static String getArchitectsStateLicenseBoardURL() throws Exception {

		return getSystemURL("ARCHITECTS_STATE_LICENSE_BOARD_URL");

	}

	public static String getEngineersStateLicenseBoardURL() throws Exception {

		return getSystemURL("ENGINEERS_STATE_LICENSE_BOARD_URL");

	}

	public static String getCityName() throws Exception {

		return getSystemURL("CITY_NAME");

	}

	public static String getPrivateLabel() throws Exception {

		return getSystemURL("PRIVATE_LABEL");

	}

	public static String getGISURL() throws Exception {

		return getSystemURL("GIS_URL");

	}

	public static String getReportsURL() throws Exception {

		return getSystemURL("REPORTS_URL");

	}

	public static String getBusinessLicenseCertificateURL() throws Exception {

		return getSystemURL("BUSINESS_LICENSE_CERTIFICATE_URL");

	}

	public static String getBusinessTaxCertificateURL() throws Exception {

		return getSystemURL("BUSINESS_TAX_CERTIFICATE_URL");

	}

	public static String getParkingPermitURL() throws Exception {

		return getSystemURL("PARKING_PERMIT_URL");

	}

	public static String getLncvPermitURL() throws Exception {

		return getSystemURL("LNCV_PRINT");

	}

	public static String getPublicWorksPermitURL() throws Exception {

		return getSystemURL("PUBLIC_WORKS_PERMIT_URL");

	}
	
	public static String getBuildingPermitReportURL() throws Exception {

		return getSystemURL("BUILDING_PERMIT_REPORT");

	}
	
	public static String getPlaningPermitReportURL() throws Exception {

		return getSystemURL("PLANNING_PERMIT_REPORT");

	}
	
	public static String getCodeEnforcementPermitReportURL() throws Exception {

		return getSystemURL("CODE_ENFORCEMENT_PERMIT_REPORT");

	}

	/**
	 * gets the list of project types
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List<ProjectType> getSubProjectNames(String deptCode) throws Exception {
		logger.debug("getSubProjectNames(" + deptCode + ")");

		List<ProjectType> projectTypeList = new ArrayList<ProjectType>();
		ProjectType projectType = null;

		try {
			sql = "SELECT PTYPE_ID,DESCRIPTION,DEPT_CODE FROM LKUP_PTYPE where description not like '" + Constants.SUB_PROJECT_NAME_STARTS_WITH + "%' and description not like '" + Constants.BT_SUB_PROJECT_NAME_STARTS_WITH + "%'";

			if (!(deptCode.equals("ALL"))) {
				sql += (" and dept_code=" + StringUtils.checkString(deptCode));
			}

			sql += " order by dept_code,description";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs != null && rs.next()) {
				int id = rs.getInt("PTYPE_ID");
				String description = rs.getString("DESCRIPTION");
				String departmentDescription = rs.getString("DEPT_CODE")+" - "+description;
				projectType = new ProjectType(id, description, departmentDescription);
				projectTypeList.add(projectType);
			}

			rs.close();

			return projectTypeList;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * gets the list of project names
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getProjectNames() throws Exception {
		logger.debug("getProjectNames()");

		List projectNames = new ArrayList();
		ProjectName projectName = null;

		try {
			sql = "SELECT * FROM LKUP_PNAME order by description";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs != null && rs.next()) {
				projectName = new ProjectName();
				projectName.setProjectNameId(rs.getInt("PNAME_ID"));
				projectName.setName(rs.getString("NAME"));
				projectName.setDescription(rs.getString("DESCRIPTION"));
				projectName.setDepartmentCode(rs.getString("DEPT_CODE"));
				projectNames.add(projectName);
			}

			rs.close();

			return projectNames;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * gets the list of project id for activity id
	 * 
	 * @return
	 * @throws Exception
	 */
	public static int getProjectNameId(String psaType, int psaId) throws Exception {
		logger.debug("getProjectNameId(" + psaType + "," + psaId + ")");

		int projectNameId = -1;

		try {
			if (psaType.equalsIgnoreCase("P")) {
				sql = "select pname_id from lkup_pname where name in (select name from project where proj_id in(" + psaId + "))";
			} else if (psaType.equalsIgnoreCase("Q")) {
				sql = "select pname_id from lkup_pname where name in (select name from project where proj_id in(select proj_id from sub_project where sproj_id in (" + psaId + ")))";
			} else if (psaType.equalsIgnoreCase("A")) {
				sql = "select pname_id from lkup_pname where name in (select name from project where proj_id in(select proj_id from sub_project where sproj_id in (select sproj_id from activity where act_id=" + psaId + ")))";
			} else {
				logger.error("Level unidentified " + psaType);
			}

			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs != null && rs.next()) {
				projectNameId = rs.getInt("pname_id");
			}

			rs.close();

			return projectNameId;
		} catch (Exception e) {
			logger.error(e+e.getMessage());
			throw e;
		}
	}

	/**
	 * gets the project / SubProject / Activity Numbers for level Id
	 * 
	 * @return String
	 * @throws Exception
	 */
	public static String getPsaNumber(String levelType, String levelId) {
		logger.debug("getPsaNumber(" + levelId + ")");

		String psaNumber = "";

		try {
			if (levelType.equals("L") || levelType.equals("S") || levelType.equals("O")) {
				sql = "select dl_address as name from v_address_list where lso_id=" + levelId;
			} else if (levelType.equals("P")) {
				sql = "select project_nbr as name from project where proj_id=" + levelId;
			} else if (levelType.equals("Q")) {
				sql = "select sproj_nbr as name from sub_project where sproj_id=" + levelId;
			} else if (levelType.equals("A")) {
				sql = "select act_nbr as name from activity where act_id=" + levelId;
			}

			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				psaNumber = rs.getString("name");
			}

			if (rs != null) {
				rs.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return psaNumber;
	}

	/**
	 * gets the list of sub project types
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List<SubProjectType> getSubProjectTypes() throws Exception {
		logger.debug("getSubProjectTypes()");

		List<SubProjectType> subProjectTypeList = new ArrayList<SubProjectType>();
		SubProjectType subProjectType = null;

		try {
			Wrapper db = new Wrapper();
			sql = "select lst.sproj_type_id,lst.sproj_type,lst.dept_code,lst.description lstdescription, lp.description lpdescription from lkup_sproj_type lst join lkup_ptype lp on lp.ptype_id=lst.ptype_id order by lst.dept_code,lst.description";
			logger.debug(sql);

			RowSet rs = db.select(sql);

			while (rs != null && rs.next()) {
				int id = rs.getInt("SPROJ_TYPE_ID");
				String type = rs.getString("SPROJ_TYPE");
				String description = rs.getString("dept_code") +" - "+ rs.getString("lstdescription")+" - "+rs.getString("lpdescription");
				subProjectType = new SubProjectType(id, type, description);
				subProjectTypeList.add(subProjectType);
			}

			rs.close();

			return subProjectTypeList;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of sub project types for a given project type
	 * 
	 * @param pType
	 * @return
	 */
	public static List<SubProjectType> getSubProjectTypes(int pType) throws Exception {
		logger.debug("getSubProjectTypes(" + pType + ")");

		List<SubProjectType> subProjectTypeList = new ArrayList<SubProjectType>();
		SubProjectType subProjectType = null;

		try {
			sql = "select  * from lkup_sproj_type where ptype_id=" + pType + " order by description";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs != null && rs.next()) {
				int id = rs.getInt("sproj_type_id");
				String type = rs.getString("sproj_type");
				String description = rs.getString("description");
				subProjectType = new SubProjectType(id, type, description);
				subProjectTypeList.add(subProjectType);
			}

			rs.close();

			return subProjectTypeList;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}


	/**
	 * Gets the list of sub project sub types
	 * 
	 * @return
	 */
	public static List<SubProjectSubType> getSubProjectSubTypes() throws Exception {
		logger.debug("getSubProjectSubTypes()");

		List<SubProjectSubType> subProjectSubTypeList = new ArrayList<SubProjectSubType>();
		SubProjectSubType subProjectSubType = null;

		try {
			sql = "SELECT * FROM LKUP_SPROJ_STYPE ORDER BY description";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs != null && rs.next()) {
				int id = rs.getInt("SPROJ_STYPE_ID");
				String type = rs.getString("TYPE");
				String description = rs.getString("description");
				subProjectSubType = new SubProjectSubType(id, type, description);
				subProjectSubTypeList.add(subProjectSubType);
			}

			rs.close();

			return subProjectSubTypeList;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the land zone id given the lso id
	 * 
	 * @param sprojType
	 * @return
	 */
	public static int getLandZoneId(long lsoId) throws Exception {
		logger.debug("getLandZoneId(" + lsoId + ")");

		int landZoneId = -1;

		try {
			sql = "select * from land_zone where land_id=(select land_id from v_lso_land where lso_id=" + lsoId + ")";
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				landZoneId = rs.getInt("zone_id");
			}

			rs.close();

			return landZoneId;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the land zone id given the lso id
	 * 
	 * @param sprojType
	 * @return
	 */
	public static int getLandUseId(long lsoId) throws Exception {
		logger.debug("getLandUseId(" + lsoId + ")");

		int landUseId = -1;

		try {
			sql = "select * from land_usage where land_id=(select land_id from v_lso_land where lso_id=" + lsoId + ")";
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs != null && rs.next()) {
				landUseId = rs.getInt("lso_use_id");
			}

			rs.close();

			return landUseId;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the list of sub project sub types for a given sub project type id and land use id.
	 * 
	 * @param sprojType
	 * @param landUseId
	 * @return
	 */
	public static List getSubProjectSubTypes(int sprojType) throws Exception {
		logger.info("getSubProjectSubTypes(" + sprojType + ")");

		List subProjectSubTypeList = new ArrayList();
		SubProjectSubType subProjectSubType = null;

		try {
			sql = "select * from lkup_sproj_stype where sproj_stype_id in (select sproj_stype from SPTYPE_SPSTYPE_ACTTYPE where sproj_type=" + sprojType + " ) order by description";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs != null && rs.next()) {
				int id = rs.getInt("SPROJ_STYPE_ID");
				String type = rs.getString("TYPE");
				String description = rs.getString("description");
				subProjectSubType = new SubProjectSubType(id, type, description);
				subProjectSubTypeList.add(subProjectSubType);
			}

			rs.close();

			return subProjectSubTypeList;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of activity types
	 * 
	 * @return
	 */

	public static List getOnlineActivityTypes() throws Exception {
		logger.debug("getActivityTypes()");

		List activityTypes = new ArrayList();
		ActivityType activityType = new ActivityType();

		try {
			sql = "select * from lkup_act_type where type ='BLDGC' order by description";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs != null && rs.next()) {
				String type = rs.getString("type");
				String description = StringUtils.properCase(rs.getString("description"));
				activityType = new ActivityType(type, description);
				activityTypes.add(activityType);
			}

			rs.close();

			return activityTypes;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public static List getActivityTypes() throws Exception {
		logger.debug("getActivityTypes()");

		List activityTypes = new ArrayList();
		ActivityType activityType = new ActivityType();

		try {
			sql = "select lat.type,lat.description,d.dept_code from lkup_act_type lat join department d on d.dept_id=lat.dept_id order by d.dept_code,lat.description";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs != null && rs.next()) {
				String type = rs.getString("type");
				String description = StringUtils.properCase(rs.getString("description"));
				String departmentCode = rs.getString("dept_code");
				String departmentDescription = departmentCode + " - " + description;
				activityType = new ActivityType(type, description, departmentCode, departmentDescription);
				activityTypes.add(activityType);
			}

			rs.close();

			return activityTypes;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
				 
	}

	public static List getActivityTypesWithSubProjectType() throws Exception {
		logger.debug("getActivityTypesWithSubProjectType()");

		List activityTypes = new ArrayList();
		ActivityType activityType = new ActivityType();

		try {
			sql = "select lat.type,lat.description,lpt.description as ptype,d.dept_code from lkup_act_type lat join department d on d.dept_id=lat.dept_id join lkup_ptype lpt on lpt.ptype_id=lat.ptype_id order by d.dept_code,lat.description";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs != null && rs.next()) {
				String type = rs.getString("type");
				String description = StringUtils.properCase(rs.getString("description")) + " (" +StringUtils.properCase(rs.getString("ptype"))+")";
				String departmentCode = rs.getString("dept_code");
				String departmentDescription = departmentCode + " - " + description;
				activityType = new ActivityType(type, description, departmentCode, departmentDescription);
				activityTypes.add(activityType);
			}

			rs.close();

			return activityTypes;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}
	/**
	 * Gets the list of activity types
	 * 
	 * @return
	 */
	public static List getActivityTypesForFees() throws Exception {
		logger.debug("getActivityTypesForFees()");

		List activityTypes = new ArrayList();
		ActivityType activityType = new ActivityType();

		try {
			Wrapper db = new Wrapper();

			sql = "select lat.type,lat.description,lm.description as mod from lkup_act_type lat left outer join lkup_module lm on lm.id=lat.module_id order by mod,description";
			logger.debug(sql);

			RowSet rs = db.select(sql);

			boolean change = true;
			while (rs != null && rs.next()) {
				String mod = rs.getString("mod");
				if (!rs.isFirst()) {
					rs.previous();
					if ((rs.getString("mod") != null) && !(rs.getString("mod")).equalsIgnoreCase(mod)) {
						change = true;
					}
					rs.next();
				}
				if (change) {
					activityType = new ActivityType("dummy", "*****" + mod + "*****");
					activityTypes.add(activityType);
					change = false;
				}
				String type = rs.getString("type");
				String description = StringUtils.properCase(rs.getString("description"));
				activityType = new ActivityType(type, description);
				activityTypes.add(activityType);
			}

			rs.close();
			return activityTypes;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of application types
	 * 
	 * @return
	 */
	public static List getApplicationTypes() throws Exception {
		logger.debug("getApplicationTypes()");

		List applicationTypes = new ArrayList();
		ApplicationType applicationType = new ApplicationType();

		try {
			sql = "select * from lkup_appl_type order by description";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs != null && rs.next()) {
				int id = rs.getInt("id");
				String description = StringUtils.properCase(rs.getString("description"));
				applicationType = new ApplicationType(id, description);
				applicationTypes.add(applicationType);
			}

			rs.close();
			logger.debug("returning with application types list of size " + applicationTypes.size());

			return applicationTypes;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of application type
	 * 
	 * @return
	 */
	public static ApplicationType getApplicationType(int applicationTypeId) throws Exception {
		logger.debug("getApplicationType(" + applicationTypeId + ")");

		ApplicationType applicationType = null;

		try {
			sql = "select * from lkup_appl_type where id=" + applicationTypeId;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				int id = rs.getInt("id");
				String description = StringUtils.properCase(rs.getString("description"));
				applicationType = new ApplicationType(id, description);
			}

			rs.close();

			return applicationType;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of ownership types
	 * 
	 * @return
	 */
	public static List getOwnershipTypes() throws Exception {
		logger.debug("getOwnershipTypes()");

		List ownershipTypes = new ArrayList();
		OwnershipType ownershipType = new OwnershipType();

		try {
			sql = "select * from lkup_own_type order by description";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs != null && rs.next()) {
				int id = rs.getInt("id");
				String description = StringUtils.properCase(rs.getString("description"));
				ownershipType = new OwnershipType(id, description);
				ownershipTypes.add(ownershipType);
			}

			rs.close();
			logger.debug("returning with ownership types list of size " + ownershipTypes.size());

			return ownershipTypes;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of ownership type
	 * 
	 * @return
	 */
	public static OwnershipType getOwnershipType(int ownershipTypeId) throws Exception {
		logger.debug("getOwnershipType(" + ownershipTypeId + ")");

		OwnershipType ownershipType = null;

		try {
			sql = "select * from lkup_own_type where id=" + ownershipTypeId;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs != null && rs.next()) {
				int id = rs.getInt("id");
				String description = StringUtils.properCase(rs.getString("description"));
				ownershipType = new OwnershipType(id, description);
			}

			rs.close();

			return ownershipType;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of Quantity types
	 * 
	 * @return
	 */
	public static List getQuantityTypes() throws Exception {
		logger.debug("getQuantityTypes()");

		List quantityTypes = new ArrayList();
		QuantityType quantityType = new QuantityType();

		try {
			sql = "select * from lkup_qty order by description";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs != null && rs.next()) {
				int id = rs.getInt("id");
				String description = StringUtils.properCase(rs.getString("description"));
				quantityType = new QuantityType(id, description);
				quantityTypes.add(quantityType);
			}

			rs.close();
			logger.debug("returning with quantity types list of size " + quantityTypes.size());

			return quantityTypes;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of BT Quantity types
	 * 
	 * @return
	 */
	public static List getBtQuantityTypes() throws Exception {
		logger.debug("getBtQuantityTypes()");

		List quantityTypes = new ArrayList();
		QuantityType quantityType = new QuantityType();

		try {
			sql = "select * from lkup_bt_qty order by description";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs != null && rs.next()) {
				int id = rs.getInt("id");
				String description = StringUtils.properCase(rs.getString("description"));
				quantityType = new QuantityType(id, description);
				quantityTypes.add(quantityType);
			}

			rs.close();
			logger.debug("returning with BT quantity types list of size " + quantityTypes.size());

			return quantityTypes;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of Quantity type
	 * 
	 * @return
	 */
	public static QuantityType getQuantityType(int quantityId) throws Exception {
		logger.debug("getQuantityType(" + quantityId + ")");

		QuantityType quantityType = null;

		try {
			sql = "select * from lkup_qty where id=" + quantityId;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs != null && rs.next()) {
				int id = rs.getInt("id");
				String description = StringUtils.properCase(rs.getString("description"));
				quantityType = new QuantityType(id, description);
			}

			rs.close();

			return quantityType;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of BT Quantity type
	 * 
	 * @author Gayathri
	 * @return List of Quantity Types
	 */
	public static QuantityType getBTQuantityType(int quantityId) throws Exception {
		logger.debug("getQuantityType(" + quantityId + ")");

		QuantityType quantityType = null;

		try {
			sql = "select * from lkup_bt_qty where id=" + quantityId;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs != null && rs.next()) {
				int id = rs.getInt("id");
				String description = StringUtils.properCase(rs.getString("description"));
				quantityType = new QuantityType(id, description);
			}

			rs.close();

			return quantityType;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of Quantity type
	 * 
	 * @author Gayathri
	 * @return
	 */
	public static QuantityType getBtQuantityType(int quantityId) throws Exception {
		logger.debug("getQuantityType(" + quantityId + ")");

		QuantityType quantityType = null;

		try {
			sql = "select * from lkup_bt_qty where id=" + quantityId;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs != null && rs.next()) {
				int id = rs.getInt("id");
				String description = StringUtils.properCase(rs.getString("description"));
				quantityType = new QuantityType(id, description);
			}

			rs.close();

			return quantityType;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of Exemption types
	 * 
	 * @return
	 */
	public static List getExemptionTypes() throws Exception {
		logger.debug("getExemptionTypes()");

		List exemptionTypes = new ArrayList();
		ExemptionType exemptionType = new ExemptionType();

		try {
			sql = "select * from lkup_exem_type order by description";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs != null && rs.next()) {
				int id = rs.getInt("id");
				String description = StringUtils.properCase(rs.getString("description"));
				exemptionType = new ExemptionType(id, description);
				exemptionTypes.add(exemptionType);
			}

			rs.close();
			logger.debug("returning with exemption types list of size " + exemptionTypes.size());

			return exemptionTypes;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of Exemption Type
	 * 
	 * @return
	 */
	public static ExemptionType getExemptionType(int exemptionTypeId) throws Exception {
		logger.debug("getExemptionType(" + exemptionTypeId + ")");

		ExemptionType exemptionType = null;

		try {
			sql = "select * from lkup_exem_type where id=" + exemptionTypeId;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs != null && rs.next()) {
				int id = rs.getInt("id");
				String description = StringUtils.properCase(rs.getString("description"));
				exemptionType = new ExemptionType(id, description);
			}

			rs.close();

			return exemptionType;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * checks if the project and sub project are active to add an activity
	 * 
	 * @param subProjectId
	 * @return
	 * @throws Exception
	 */
	public static boolean isPsaActive(String subProjectId) {
		logger.debug("isPsaActive(" + subProjectId + ")");

		boolean psaActive = false;

		try {
			int projectNameId = -1;
			projectNameId = LookupAgent.getProjectNameId("Q", StringUtils.s2i(subProjectId));

			if (projectNameId == Constants.PROJECT_NAME_PLANNING_ID) { // this
				sql = "select * from sub_project sp,project p where sp.proj_id=p.proj_id and sp.status in (4,11,8,6,5,7) and sproj_id=" + subProjectId;
			}

			else {
				sql = "select * from sub_project sp,project p where sp.proj_id=p.proj_id and p.status_id=sp.status and p.status_id=1 and sproj_id=" + subProjectId;
			}

			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs != null && rs.next()) {
				psaActive = true;
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		logger.debug("psaActive: " + psaActive);

		return psaActive;
	}

	/**
	 * Gets the list of activity types
	 * 
	 * @return
	 */
	public static List getSearchActivityTypes() throws Exception {
		logger.debug("getActivityTypes()");

		List activityTypes = new ArrayList();
		ActivityType activityType = new ActivityType();

		try {
			sql = "select * from lkup_act_type order by description";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs != null && rs.next()) {
				String type = rs.getString("type");
				String description = StringUtils.properCase(rs.getString("description"));
				activityType = new ActivityType(type, description);
				activityTypes.add(activityType);
			}

			rs.close();

			return activityTypes;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of bl activity types
	 * 
	 * Created by Gayathri
	 * 
	 * @return
	 */
	public static List getBlActivityTypes(int moduleId) throws Exception {
		logger.debug("getBlActivityTypes(" + moduleId + ")");

		List activityTypes = new ArrayList();
		ActivityType activityType = new ActivityType();

		try {
			sql = "select * from lkup_act_type where module_id=" + moduleId + " order by description ";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs != null && rs.next()) {
				String type = rs.getString("type");
				String description = StringUtils.properCase(rs.getString("description"));
				activityType = new ActivityType(type, description);
				activityTypes.add(activityType);
			}

			rs.close();

			return activityTypes;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * gets the list of activity types for a sub project id and land use.
	 * 
	 * @param subProjectId
	 * @param landUseId
	 * @return
	 */
	public static List getActivityTypes(String subProjectId) throws Exception {
		logger.info("getActivityTypes(" + subProjectId + ")");

		List activityTypes = new ArrayList();
		ActivityType activityType = new ActivityType();
		RowSet rs = null;

		try {
			sql = "select type,description from lkup_act_type where type in (select act_type from SPTYPE_SPSTYPE_ACTTYPE ussa join sub_project sp" + " on ussa.sproj_type=sp.stype_id and ussa.sproj_stype=sp.sstype_id and sp.sproj_id=" + subProjectId + " ) order by description asc";
			logger.debug(sql);
			rs = new Wrapper().select(sql);

			while (rs != null && rs.next()) {
				String type = rs.getString("type");
				String description = rs.getString("description");
				activityType = new ActivityType(type, description);
				activityTypes.add(activityType);
			}

			rs.close();

			return activityTypes;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the list of streets
	 * 
	 * @param id
	 * @return description
	 */
	public static Street getStreet(String streetName) throws Exception {
		logger.debug("getStreet(" + streetName + ")");

		Street street = new Street();
		String sql = "select * from street_list where street_id=" + streetName;
		logger.debug(" Street List is  " + sql);

		try {
			RowSet rs = new Wrapper().select(sql);

			if (rs != null && rs.next()) {
				street.setStreetId(rs.getInt("STREET_ID"));
				street.setStreetName(rs.getString("STR_NAME"));
			} else {
				street.setStreetId(-1);
				street.setStreetName("");
			}

			rs.close();

			return street;
		} catch (SQLException e) {
			logger.error("SQL Error in Street " + e.getMessage());
			throw e;
		} catch (Exception e) {
			logger.error("Error in Street " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the activity sub type for a sub type id
	 * 
	 * @param id
	 * @return
	 */
	public static ActivitySubType getActivitySubType(String id) throws Exception {
		logger.debug("getActivitySubType(" + id + ")");

		ActivitySubType actSubType = new ActivitySubType();
		String sql = "select * from lkup_act_subtype where act_subtype_id=" + id;

		try {
			RowSet rs = new Wrapper().select(sql);

			if (rs != null && rs.next()) {
				actSubType.setId(rs.getInt("act_subtype_id"));
				actSubType.setDescription(rs.getString("act_subtype"));
			} else {
				actSubType.setId(-1);
				actSubType.setDescription("");
			}

			rs.close();

			return actSubType;
		} catch (SQLException e) {
			logger.error("SQL Error in getActivitySubType " + e.getMessage());
			throw e;
		} catch (Exception e) {
			logger.error("Error in getActivitySubType " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the list of activity sub types for an activity type.
	 * 
	 * @param activityType
	 * @return
	 */
	public static List getActivitySubTypes(String activityType) throws Exception {
		logger.debug("getActivitySubTypes(" + activityType + ")");

		List activitySubTypes = new ArrayList();
		String sql = "select * from lkup_act_subtype where act_type='" + activityType + "' order by act_subtype";
		logger.debug(sql);

		ActivitySubType subType = new ActivitySubType();

		try {
			RowSet rs = new Wrapper().select(sql);

			while (rs != null && rs.next()) {
				ActivitySubType activitySubType = new ActivitySubType(rs.getInt("act_subtype_id"), rs.getString("act_subtype"));
				activitySubTypes.add(activitySubType);
			}

			rs.close();

			return activitySubTypes;
		} catch (SQLException e) {
			logger.error("SQL Error in getActivitySubType " + e.getMessage());
			throw e;
		} catch (Exception e) {
			logger.error("Error in getActivitySubType " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the list of activity sub types given the activity type id.
	 * 
	 * @param activityTypeId
	 * @return static list of ActivitySubType objects
	 * @throws Exception
	 * @author Anand Belaguly (anand@edgesoftinc.com) April 2012
	 */
	public static List<ActivitySubType> getActivitySubTypesForActivityTypeId(String activityTypeId) throws Exception {
		logger.debug("getActivitySubTypesForActivityTypeId(" + activityTypeId + ")");

		List<ActivitySubType> activitySubTypes = new ArrayList<ActivitySubType>();
		String sql = "select * from lkup_act_subtype where act_type = (select type from lkup_act_type where type='" + activityTypeId + "') order by act_subtype";
		logger.debug(sql);

		try {
			RowSet rs = new Wrapper().select(sql);

			while (rs != null && rs.next()) {
				int activitySubTypeId = rs.getInt("act_subtype_id");
				ActivitySubType activitySubType = new ActivitySubType(activitySubTypeId, rs.getString("act_subtype"), rs.getString("label"));
				activitySubTypes.add(activitySubType);
				// TODO:add the fees related to the activity sub type

			}

			rs.close();

			return activitySubTypes;
		} catch (Exception e) {
			logger.error("Error in getActivitySubTypesForActivityTypeId " + e.getMessage());
			throw e;
		}
	}

	/**
	 * get codes for activity type
	 * 
	 * @param activityType
	 * @return
	 * @throws Exception
	 */
	public static Map getCodes(String activityType) throws Exception {
		logger.debug("getCodes(" + activityType + ")");

		Map codes = new HashMap();
		String sql = "";
		sql = "select * from lkup_act_type where type=" + StringUtils.checkString(activityType);
		logger.debug(sql);

		try {

			RowSet rs = new Wrapper().select(sql);

			if (rs != null && rs.next()) {
				logger.debug("obtained muni code of " + rs.getString("MUNI_CODE"));
				codes.put("MUNI_CODE", StringUtils.nullReplaceWithEmpty(rs.getString("MUNI_CODE")));
				codes.put("SIC_CODE", StringUtils.nullReplaceWithEmpty(rs.getString("SIC_CODE")));
				codes.put("CLASS_CODE", StringUtils.nullReplaceWithEmpty(rs.getString("CLASS_CODE")));
			}

			rs.close();

			return codes;
		} catch (SQLException e) {
			logger.error("SQL Error in getCodes " + e.getMessage());
			throw e;
		} catch (Exception e) {
			logger.error("Error in getCodes " + e.getMessage());
			throw e;
		}
	}

	/**
	 * get codes for Class Code
	 * 
	 * @param Class
	 *            Code
	 * @return
	 * @throws Exception
	 */
	public static Map getCodesForClassCode(String classCode) throws Exception {
		logger.debug("getCodesForClassCode(" + classCode + ")");

		Map codes = new HashMap();
		String sql = "";
		sql = "select * from lkup_act_type where upper(CLASS_CODE)=" + StringUtils.checkString(classCode.toUpperCase());
		logger.debug(sql);

		try {

			RowSet rs = new Wrapper().select(sql);

			if (rs != null && rs.next()) {
				codes.put("TYPE", StringUtils.nullReplaceWithEmpty(rs.getString("TYPE")));
				codes.put("MUNI_CODE", StringUtils.nullReplaceWithEmpty(rs.getString("MUNI_CODE")));
				codes.put("SIC_CODE", StringUtils.nullReplaceWithEmpty(rs.getString("SIC_CODE")));
				codes.put("CLASS_CODE", StringUtils.nullReplaceWithEmpty(rs.getString("CLASS_CODE")));
			}
			logger.debug(codes.size());
			rs.close();

			return codes;
		} catch (SQLException e) {
			logger.error("SQL Error in getCodes " + e.getMessage());
			throw e;
		} catch (Exception e) {
			logger.error("Error in getCodes " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of activity sub types concatinated with the activity types This is used in the search function on the basic search screen
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getActivitySubTypes() throws Exception {
		logger.debug("getActivitySubTypes()");

		List activitySubTypes = new ArrayList();

		String sql = "select last.act_subtype_id,last.act_subtype,lat.description from lkup_act_subtype last join lkup_act_type lat on last.act_type=lat.type order by last.act_subtype";

		ActivitySubType subType = new ActivitySubType();

		try {
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs != null && rs.next()) {
				ActivitySubType activitySubType = new ActivitySubType(rs.getInt("act_subtype_id"), StringUtils.properCase(rs.getString("act_subtype")+" - "+rs.getString("description")));
				activitySubTypes.add(activitySubType);
			}

			rs.close();

			return activitySubTypes;
		} catch (Exception e) {
			logger.error("Error in getActivitySubTypes " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the list of activity sub types for an Sub type IDs.
	 * 
	 * @param activityType
	 * @return
	 */
	public static List getSubTypes(String ids) throws Exception {
		logger.debug("getSubTypes(" + ids + ")");

		List activitySubTypes = new ArrayList();

		String sql = "select * from lkup_act_subtype where act_subtype_id in " + ids + " order by act_subtype";
		logger.debug(sql);

		try {
			Wrapper db = new Wrapper();

			RowSet rs = db.select(sql);

			while (rs != null && rs.next()) {
				String s1 = (rs.getString("act_subtype"));
				logger.debug("The multiple subtypes are: " + s1);
				activitySubTypes.add(s1);
			}

			rs.close();
			return activitySubTypes;
		} catch (SQLException e) {
			logger.error("SQL Error in getActivitySubTypes " + e.getMessage());
			throw e;
		} catch (Exception e) {
			logger.error("Error in getActivitySubTypes " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the list of activity sub type id for an Sub type description.
	 * 
	 * @param activityType
	 * @return
	 */
	public static int getSubTypeId(String description, String activityType) throws Exception {
		logger.debug("getSubTypeId(" + description + ")");

		String sql = "select * from lkup_act_subtype where act_type=" + StringUtils.checkString(activityType) + " and act_subtype=" + StringUtils.checkString(description);
		logger.debug(sql);

		int s1 = -1;

		try {
			Wrapper db = new Wrapper();

			RowSet rs = db.select(sql);

			if (rs != null && rs.next()) {
				s1 = rs.getInt("act_subtype_id");
			}

			rs.close();

			return s1;
		} catch (Exception e) {
			logger.error("Error in getSubTypeId " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the list of fee types.
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getFeeTypes() throws Exception {
		logger.debug("getFeeTypes()");

		List feeTypes = new ArrayList();

		try {
			sql = "select distinct fee_type from fee order by fee_type";
			logger.debug(sql);

			Wrapper db = new Wrapper();

			RowSet rs = db.select(sql);

			while (rs != null && rs.next()) {
				FeeType feeType = new FeeType(rs.getString("fee_type"));
				feeTypes.add(feeType);
			}

			rs.close();

			return feeTypes;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the list of fee lookups
	 * 
	 * @return
	 */
	public static List getFeeLookups() throws Exception {
		logger.debug("getFeeLookups()");

		List feeLookups = new ArrayList();

		try {
			sql = "select lkup_fee,lkup_fee_name,count(*) as nbr from lkup_fee where fee_expiration_dt is null group by lkup_fee,lkup_fee_name order by lkup_fee_name";
			logger.debug(sql);

			Wrapper db = new Wrapper();

			RowSet rs = db.select(sql);
			while (rs != null && rs.next()) {
				LookupFee feeLookup = new LookupFee(StringUtils.s2i(rs.getString("lkup_fee")), rs.getString("lkup_fee_name"));
				feeLookups.add(feeLookup);
			}

			rs.close();

			return feeLookups;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the activity type for description for a type code
	 * 
	 * @param type
	 * @return
	 */
	public static ActivityType getActivityType(String type) throws Exception {
		logger.debug("getActivityType(" + type + ")");

		ActivityType activityType = new ActivityType();

		if (type == null || type.equalsIgnoreCase("")) {
			return activityType;
		} else {
			try {
				sql = "select * from lkup_act_type where type='" + type.toUpperCase() + "'";
				logger.debug(sql);

				Wrapper db = new Wrapper();

				RowSet rs = db.select(sql);
				if (rs != null && rs.next()) {
					activityType = new ActivityType(rs.getString("type"), rs.getString("description"));
					Department department = getDepartment(rs.getInt("dept_id"));
					int typeId = rs.getInt("type_id");

					if (department != null) {
						activityType.setDepartmentId(department.getDepartmentId());
						activityType.setDepartmentCode(department.getDepartmentCode());

						activityType.setTypeId(StringUtils.i2s(typeId));
						activityType.setType(type);
					} else {
						activityType.setDepartmentId(-1);
						activityType.setDepartmentCode("");
					}
    
					activityType.setSubProjectType(rs.getInt("ptype_id"));
				}

				rs.close();

				return activityType;
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e.getMessage());
				throw e;
			}

		}
	}

	/**
	 * Get the list of project statuses
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getProjectStatuses() throws Exception {
		logger.debug("getProjectStatuses()");

		List projectStatuses = new ArrayList();
		ProjectStatus projectStatus = null;

		try {
			sql = "select * from lkup_proj_st order by  description ";
			logger.debug(sql);

			Wrapper db = new Wrapper();

			RowSet rs = db.select(sql);
			while (rs != null && rs.next()) {
				int statusId = rs.getInt("status_id");
				String description = rs.getString("description").trim();
				projectStatus = new ProjectStatus(statusId, description);
				projectStatuses.add(projectStatus);
			}

			rs.close();

			return projectStatuses;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the list of sub project statuses
	 * 
	 * @return
	 */
	public static List getSubProjectStatuses() throws Exception {
		logger.debug("getSubProjectStatuses()");

		List subProjectStatuses = new ArrayList();
		SubProjectStatus subProjectStatus = null;

		try {
			sql = "select * from lkup_sproj_st order by  description ";
			logger.debug(sql);

			Wrapper db = new Wrapper();

			RowSet rs = db.select(sql);
			while (rs != null && rs.next()) {
				int statusId = rs.getInt("status_id");
				String description = rs.getString("description").trim();
				subProjectStatus = new SubProjectStatus(statusId, description);
				subProjectStatuses.add(subProjectStatus);
			}

			rs.close();

			return subProjectStatuses;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/*
	 * 
	 * get case Log
	 */
	public static List getCaseLogList() throws Exception {
		logger.debug("getCaseLogList()");

		List caseLogList = new ArrayList();
		SubProjectStatus subProjectStatus = null;

		try {
			sql = "select * from lkup_caselog";

			Wrapper db = new Wrapper();

			RowSet rs = db.select(sql);
			while (rs != null && rs.next()) {
				int statusId = rs.getInt("caselog_id");
				String description = rs.getString("description").trim();
				subProjectStatus = new SubProjectStatus(statusId, description);
				caseLogList.add(subProjectStatus);
			}

			rs.close();
		} catch (Exception e) {
			logger.error("Excepton In getCaseLogList: " + e.getMessage());
			throw e;
		}

		return caseLogList;
	}

	/**
	 * Get the project status for a status id.
	 * 
	 * @param statusId
	 * @return
	 * @throws Exception
	 */
	public static ProjectStatus getProjectStatus(int statusId) throws Exception {
		logger.debug("getProjectStatus(" + statusId + ")");

		ProjectStatus projectStatus = new ProjectStatus();

		try {
			sql = "select * from lkup_proj_st where status_id=" + statusId;
			logger.debug("Lookupagent - getprojectStatus -- " + sql);

			Wrapper db = new Wrapper();

			RowSet rs = db.select(sql);
			if (rs != null && rs.next()) {
				projectStatus = new ProjectStatus(rs.getInt("status_id"), rs.getString("description").trim());
				logger.debug("Lookupagent - getProjectStatus -- status_id " + projectStatus.getStatusId());
				logger.debug("Lookupagent - getProjectStatus -- description" + projectStatus.getDescription());
			}

			rs.close();

			return projectStatus;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the sub project status for a sub project status id.
	 * 
	 * @param statusId
	 * @return
	 * @throws Exception
	 */
	public static SubProjectStatus getSubProjectStatus(int statusId) throws Exception {
		logger.debug("getSubProjectStatus(" + statusId + ")");

		SubProjectStatus subProjectStatus = new SubProjectStatus();

		try {
			sql = "select * from lkup_sproj_st where status_id=" + statusId;
			logger.debug("Lookupagent - getsubProjectStatus -- " + sql);

			Wrapper db = new Wrapper();

			RowSet rs = db.select(sql);
			if (rs != null && rs.next()) {
				subProjectStatus = new SubProjectStatus(rs.getInt("status_id"), rs.getString("description").trim());
				logger.debug("Lookupagent - getSubProjectStatus -- status_id " + subProjectStatus.getStatusId());
				logger.debug("Lookupagent - getSubProjectStatus -- description" + subProjectStatus.getDescription());
			}

			rs.close();

			return subProjectStatus;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the list of activity statuses
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getActivityStatuses() throws Exception {
		logger.debug("getActivityStatuses()");

		List activityStatuses = new ArrayList();
		ActivityStatus activityStatus = null;

		try {
			sql = "select * from lkup_act_st where status_id > 0 order by  status_id ";
			logger.debug(sql);

			Wrapper db = new Wrapper();

			RowSet rs = db.select(sql);
			while (rs != null && rs.next()) {
				int statusId = rs.getInt("status_id");
				String code = rs.getString("stat_code").trim();
				String description = rs.getString("description").trim();
				activityStatus = new ActivityStatus(statusId, code, description);
				activityStatuses.add(activityStatus);
			}
			rs.close();

			return activityStatuses;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get List of Activity status for a given module
	 * 
	 * @param module
	 * @return
	 * @throws Exception
	 */
	public static List<ActivityStatus> getActivityStatusList(int module) throws Exception {
		logger.debug("getActivityStatusList(" + module + ")");

		List activityStatuses = new ArrayList();
		ActivityStatus activityStatus = null;

		try {
			sql = "select * from lkup_act_st where status_id > 0 and module_id=" + module + " order by  status_id ";
			logger.debug(sql);

			Wrapper db = new Wrapper();

			RowSet rs = db.select(sql);
			while (rs != null && rs.next()) {
				int statusId = rs.getInt("status_id");
				String code = rs.getString("stat_code").trim();
				String description = rs.getString("description").trim();
				activityStatus = new ActivityStatus(statusId, code, description);
				activityStatuses.add(activityStatus);
			}
			rs.close();

			return activityStatuses;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get List of landmarks
	 * 
	 * @param module
	 * @return
	 * @throws Exception
	 */
	public static List<Landmark> getLandmarks() throws Exception {
		logger.debug("getLandmarks()");

		List landmarks = new ArrayList();
		Landmark landmark = null;

		try {
			sql = "select * from lkup_landmark order by landmark_name";
			logger.debug(sql);

			Wrapper db = new Wrapper();

			RowSet rs = db.select(sql);
			while (rs != null && rs.next()) {
				int addressId = rs.getInt("addr_id");
				String name = rs.getString("landmark_name").trim();
				String description = rs.getString("landmark_desc").trim();
				landmark = new Landmark(addressId, name, description);
				landmarks.add(landmark);
			}
			rs.close();

			return landmarks;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the list of activity statuses
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getActivityStatusesForFees() throws Exception {
		logger.debug("getActivityStatusesForFees()");

		List activityStatuses = new ArrayList();
		ActivityStatus activityStatus = new ActivityStatus();

		try {

			sql = "select las.STATUS_ID,las.STAT_CODE, las.DESCRIPTION,lm.description as mod from lkup_act_st las left outer join lkup_module lm on lm.id=las.module_id  where las.status_id > 0 and lm.description IS NOT NULL order by mod,description";
			logger.debug(sql);

			Wrapper db = new Wrapper();

			RowSet rs = db.select(sql);
			boolean change = true;

			while (rs != null && rs.next()) {

				String mod = rs.getString("mod");
				if (!rs.isFirst()) {
					rs.previous();
					if (((rs.getString("mod") != null) || !rs.getString("mod").equalsIgnoreCase(null)) && !(rs.getString("mod")).equalsIgnoreCase(mod)) {
						change = true;
					}
					rs.next();
				}
				if (change) {
					activityStatus = new ActivityStatus("dummy", "*****" + mod.toUpperCase() + "*****");
					activityStatuses.add(activityStatus);
					change = false;
				}

				int statusId = rs.getInt("status_id");
				String code = rs.getString("stat_code").trim();
				String description = rs.getString("description").trim();
				activityStatus = new ActivityStatus(statusId, code, description);
				activityStatuses.add(activityStatus);

			}

			rs.close();

			return activityStatuses;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the list of activity statuses
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getBLActivityStatuses(int moduleId) throws Exception {
		logger.debug("getActivityStatuses()");

		List activityStatuses = new ArrayList();
		ActivityStatus activityStatus = null;

		try {
			sql = "select * from lkup_act_st where module_id =" + moduleId + " order by  description ";
			logger.debug(sql);

			Wrapper db = new Wrapper();

			RowSet rs = db.select(sql);
			while (rs != null && rs.next()) {
				int statusId = rs.getInt("status_id");
				String code = rs.getString("stat_code").trim();
				String description = rs.getString("description").trim();
				activityStatus = new ActivityStatus(statusId, code, description);
				activityStatuses.add(activityStatus);

			}

			rs.close();

			return activityStatuses;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the list of activity statuses
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getActivityStatuses(int projectNameId, String sprojName) throws Exception {
		logger.debug("getActivityStatuses()");

		List activityStatuses = new ArrayList();
		ActivityStatus activityStatus = null;

		try {
			if (sprojName.equalsIgnoreCase("Regulatory Permit")) {
				sql = "select * from lkup_act_st where status_id > 0 and pname_id=" + projectNameId + " and module_id = 7 order by  stat_code ";
				logger.debug(sql);
			} else {
				sql = "select * from lkup_act_st where status_id > 0 and pname_id=" + projectNameId + " order by  stat_code ";
				logger.debug(sql);
			}

			Wrapper db = new Wrapper();

			RowSet rs = db.select(sql);
			while (rs != null && rs.next()) {
				int statusId = rs.getInt("status_id");
				String code = rs.getString("stat_code").trim();
				String description = rs.getString("description").trim();
				activityStatus = new ActivityStatus(statusId, code, description);
				activityStatuses.add(activityStatus);
			}

			rs.close();

			return activityStatuses;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}
	
	

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	
	public static ArrayList getNoticeFieldList() throws Exception {
		logger.debug("getNoticeFieldList()");
		ArrayList views = new ArrayList();
		try {
			sql = "SELECT * FROM LKUP_NOTICE_FIELDS ORDER BY FIELD_NAME ";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				DisplayItem d = new DisplayItem();
				String value = rs.getString("FIELD_ALIAS");
				String name = rs.getString("FIELD_NAME");
				//String option = rs.getString("ACTION_OPTION_1");
				String id = rs.getString("NOTICE_FIELDS_ID");
				d.setFieldOne(id);
				d.setFieldTwo(name);
				d.setFieldThree(value);
				
				views.add(d);
			}

			rs.close();
			return views;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	
	public static ArrayList getActionList() throws Exception {
		logger.debug("getActionList()");
		ArrayList views = new ArrayList();
		try {
			sql = "SELECT * from LKUP_ACTION WHERE OPTION_PRESENT='Y' ";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				DisplayItem d = new DisplayItem();
				String name = rs.getString("ACTION_NAME");
				String value = rs.getString("ACTION_VALUE");
				String option = rs.getString("ACTION_OPTION_1");
				String id = rs.getString("ID");
				d.setFieldOne(id);
				d.setFieldTwo(name);
				d.setFieldThree(value);
				d.setFieldFour(option);
				views.add(d);
			}

			rs.close();
			return views;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the activity status for a status id.
	 * 
	 * @param statusId
	 * @return
	 * @throws Exception
	 */
	public static ActivityStatus getActivityStatus(int statusId) throws Exception {
		logger.debug("getActivityStatus(" + statusId + ")");

		ActivityStatus activityStatus = null;

		try {
			sql = "select * from lkup_act_st where status_id=" + statusId;
			logger.debug("Lookupagent - getActivityStatus -- " + sql);

			Wrapper db = new Wrapper();

			RowSet rs = db.select(sql);
			if (rs != null && rs.next()) {
				activityStatus = new ActivityStatus(rs.getInt("status_id"), rs.getString("stat_code").trim(), rs.getString("description").trim());
				logger.debug("Lookupagent - getActivityStatus -- status_id " + activityStatus.getStatus());
				logger.debug("Lookupagent - getActivityStatus -- code " + activityStatus.getCode());
				logger.debug("Lookupagent - getActivityStatus -- description" + activityStatus.getDescription());
			}

			rs.close();

			return activityStatus;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of plan check status.
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getPlanCheckStatuses() throws Exception {
		logger.debug("getPlanCheckStatuses()");

		List planCheckStatuses = new ArrayList();
		PlanCheckStatus planCheckStatus = null;

		try {
			planCheckStatus = new PlanCheckStatus(-1, "Please Select", -1);
			planCheckStatuses.add(planCheckStatus);

			sql = "select * from lkup_pc_status order by pc_desc";
			logger.debug(sql);

			Wrapper db = new Wrapper();

			RowSet rs = db.select(sql);
			while (rs != null && rs.next()) {
				int code = rs.getInt("pc_code");
				String description = rs.getString("pc_desc").trim();
				int actStatusCode = rs.getInt("act_status_code");
				planCheckStatus = new PlanCheckStatus(code, description, actStatusCode);
				planCheckStatuses.add(planCheckStatus);
			}

			rs.close();

			return planCheckStatuses;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the plan check status for a pc code
	 * 
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public static PlanCheckStatus getPlanCheckStatus(int code) throws Exception {
		logger.debug("getPlanCheckStatus(" + code + ")");

		PlanCheckStatus planCheckStatus = null;

		try {
			sql = "select * from lkup_pc_status  where pc_code=" + code;
			logger.debug("Lookupagent - getPlanCheckStatus  -- " + sql);

			Wrapper db = new Wrapper();

			RowSet rs = db.select(sql);
			if (rs != null && rs.next()) {
				planCheckStatus = new PlanCheckStatus(rs.getInt("pc_code"), rs.getString("pc_desc").trim(), rs.getInt("act_status_code"));
				logger.debug("Lookupagent - getPlanCheckStatus -- code " + planCheckStatus.getCode());
				logger.debug("Lookupagent - getPlanCheckStatus--  description" + planCheckStatus.getDescription());
			}

			rs.close();

			return planCheckStatus;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the OBC file for a attachment id.
	 * 
	 * @param fileId
	 * @return
	 * @throws Exception
	 */
	public static ObcFile getObcFile(int fileId) throws Exception {
		logger.debug("getObcFile(" + fileId + ")");

		ObcFile obcFile = null;

		try {
			sql = "select * from attachments  where attach_id=" + fileId;
			logger.debug("Lookupagent - getObcFile -- " + sql);

			Wrapper db = new Wrapper();

			RowSet rs = db.select(sql);
			if (rs != null && rs.next()) {
				obcFile = new ObcFile();
				obcFile.setFileName(rs.getString("file_name"));
				logger.debug("Lookupagent - getObcFile -- FileName " + obcFile.getFileName());
				obcFile.setFileLocation(rs.getString("location"));
				logger.debug("Lookupagent - getObcFile -- FileLocation " + obcFile.getFileLocation());
				obcFile.setCreateDate(rs.getDate("created"));
				logger.debug("Lookupagent - getObcFile -- CreateDate " + obcFile.getCreateDate());
				obcFile.setFileSize(rs.getString("size"));
				logger.debug("Lookupagent - getObcFile -- FileSize " + obcFile.getFileSize());
			}

			rs.close();

			return obcFile;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of LSO Uses for a LSO Type
	 * 
	 * @param lsoType
	 * @return
	 * @throws Exception
	 */
	public static List getLsoUses(String lsoType) throws Exception {
		logger.debug("getLsoUses(" + lsoType + ")");

		List lsoUseList = new ArrayList();
		Use use = null;

		try {
			sql = "SELECT * FROM LSO_USE WHERE LSO_TYPE='" + lsoType + "'";
			logger.debug(sql);

			Wrapper db = new Wrapper();

			RowSet rs = db.select(sql);
			while (rs != null && rs.next()) {
				use = new Use(rs.getInt("LSO_USE_ID"), rs.getString("LSO_TYPE"), rs.getString("DESCRIPTION"));
				lsoUseList.add(use);
			}

			rs.close();

			return lsoUseList;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of people types
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getPeopleTypes() throws Exception {
		logger.debug("getPeopleTypes()");

		List peopleTypeList = new ArrayList();

		try {
			Wrapper db = new Wrapper();
			String sql = "SELECT * FROM PEOPLE_TYPE where people_type_id not in (23,24,25,26) ORDER BY DESCRIPTION";
			logger.debug(sql);

			RowSet rs = db.select(sql);

			while (rs != null && rs.next()) {
				PeopleType peopleType = new PeopleType(rs.getInt("people_type_id"), rs.getString("description"), rs.getString("code"));
				peopleTypeList.add(peopleType);
			}

			rs.close();

			return peopleTypeList;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of people BL types
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getBLPeopleTypes() throws Exception {
		logger.debug("getBLPeopleTypes()");

		List peopleTypeList = new ArrayList();

		try {
			Wrapper db = new Wrapper();
			String sql = "SELECT * FROM PEOPLE_TYPE where people_type_id in (9,23,24,25,26) ORDER BY DESCRIPTION";
			logger.debug(sql);

			RowSet rs = db.select(sql);

			while (rs != null && rs.next()) {
				PeopleType peopleType = new PeopleType(rs.getInt("people_type_id"), rs.getString("description"), rs.getString("code"));
				peopleTypeList.add(peopleType);
			}

			rs.close();

			return peopleTypeList;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the list of employees by groups
	 * 
	 * @param groups
	 * @return
	 * @throws Exception
	 */
	public static List getEmployeesByGroups(String groups) throws Exception {
		logger.debug("getEmployeesByGroups(" + groups + ")");

		List peopleByTypeList = new ArrayList();

		try {
			Wrapper db = new Wrapper();
			String sql = "select userid,first_name || ' ' || last_name as name from users where userid in " + " ( select distinct user_id from user_groups where group_id in (" + groups + ")) order by name";
			logger.debug(sql);

			RowSet rs = db.select(sql);

			peopleByTypeList.add(new PeopleByType(-1, "Please Select"));

			while (rs.next()) {
				PeopleByType peopleByType = new PeopleByType(rs.getInt("userId"), rs.getString("name"));
				peopleByTypeList.add(peopleByType);
			}

			rs.close();

			return peopleByTypeList;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the list of user by department
	 * 
	 * @param groups
	 * @return
	 * @throws Exception
	 */
	public static List getUsersByDepartment(int departmentId) throws Exception {
		logger.debug("getUsersByDepartment(" + departmentId + ")");

		List users = new ArrayList();

		try {
			if (departmentId == -1) {
				return users;
			}

			Wrapper db = new Wrapper();
			String sql = "select userid,first_name,last_name from users where dept_id=" + departmentId + " and active='Y' order by first_name";
			logger.debug(sql);

			RowSet rs = db.select(sql);

			while (rs.next()) {
				User user = new User();
				user.setUserId(rs.getInt("USERID"));
				user.setFirstName(rs.getString("first_name"));
				user.setLastName(rs.getString("last_name"));
				users.add(user);
			}

			rs.close();

			return users;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the list of employees by Title
	 * 
	 * @param title
	 * @return
	 * @throws Exception
	 */
	public static List getEmployeeByTitle(String title) throws Exception {
		logger.debug("getEmployeeByTitle(" + title + ")");

		List peopleByTypeList = new ArrayList();

		try {
			Wrapper db = new Wrapper();
			String sql = "select userid,first_name || ' ' || last_name as name from users where title='" + title + "' order by name";
			logger.debug(sql);

			RowSet rs = db.select(sql);

			peopleByTypeList.add(new PeopleByType(-1, "Please Select"));

			while (rs.next()) {
				PeopleByType peopleByType = new PeopleByType(rs.getInt("userId"), rs.getString("name"));
				peopleByTypeList.add(peopleByType);
			}

			rs.close();

			return peopleByTypeList;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of lookup fees
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getLookupFees() throws Exception {
		logger.debug("getLookupFees()");

		List lookupFees = new ArrayList();

		try {
			String sql = "select lkup_fee, lkup_fee_name,fee_creation_dt from lkup_fee where fee_expiration_dt is null group by lkup_fee,lkup_fee_name,fee_creation_dt order by lkup_fee_name asc,lkup_fee desc";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				LookupFee lookupFee = new LookupFee(rs.getInt("lkup_fee"), rs.getString("lkup_fee_name"));
				lookupFee.setCreationDate(rs.getDate("fee_creation_dt"));
				lookupFees.add(lookupFee);
			}

			rs.close();

			return lookupFees;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the pay type using the pay type id.
	 * 
	 * @param paytypeId
	 * @return
	 * @throws Exception
	 */
	public static PayType getPayTypeDescription(int paytypeId) throws Exception {
		logger.debug("getPayTypeDescription(" + paytypeId + ")");

		PayType payType = new PayType();

		try {
			sql = "select * from lkup_pay_type  where pay_type_id=" + paytypeId;
			logger.debug("FinanceAgent - getPayTypeDescription  -- " + sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				payType = new PayType(rs.getInt("PAY_TYPE_ID"), rs.getString("DESCRIPTION").trim());
				logger.debug("LookupAgent - getPayTypeDescription -- paytypeId " + payType.getPayTypeId());
				logger.debug("LookupAgent - getPayTypeDescription--  description" + payType.getDescription());
			}

			rs.close();

			return payType;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get list of pay types
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getPayTypes() throws Exception {
		logger.debug("getPayTypes()");

		List payTypeList = new ArrayList();
		PayType payType = null;

		try {
			sql = "SELECT * FROM LKUP_PAY_TYPE ORDER BY PAY_TYPE_ID";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				int id = rs.getInt("PAY_TYPE_ID");
				String description = rs.getString("DESCRIPTION");
				payType = new PayType(id, description);
				payTypeList.add(payType);
			}

			rs.close();

			return payTypeList;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the user details for a given user id
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public static User getUserDetails(String userId) throws Exception {
		logger.debug("getUserDetails(" + userId + ")");

		User user = new User();

		try {
			sql = "select * from users  where userid= " + userId; // .
			// equalsIgnoreCase();
			logger.debug("LookupAgent - getUserDetails  -- " + sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				user.setUserId(rs.getInt("USERID"));
				logger.debug("LookupAgent - userid" + rs.getInt("USERID"));
				user.setUsername(rs.getString("USERNAME"));
				logger.debug("LookupAgent - user name" + rs.getString("USERNAME"));
				user.setFirstName(rs.getString("FIRST_NAME"));
				logger.debug("LookupAgent - first name " + rs.getString("FIRST_NAME"));
				user.setLastName(rs.getString("LAST_NAME"));
				logger.debug("LookupAgent - last name " + rs.getString("LAST_NAME"));
				user.setMiddleInitial(rs.getString("MI"));
				logger.debug("LookupAgent - MI " + rs.getString("MI"));
				user.setTitle(rs.getString("TITLE"));
				logger.debug("LookupAgent - Title " + rs.getString("TITLE"));
				user.setPassword(rs.getString("PASSWORD"));
				logger.debug("LookupAgent - Password " + rs.getString("PASSWORD"));
				user.setDepartment(getDepartment(rs.getInt("dept_id")));
			}

			rs.close();

			return user;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the department for a given department id.
	 * 
	 * @param deptId
	 * @return
	 * @throws Exception
	 */
	public static Department getDepartment(int deptId) throws Exception {
		logger.debug("getDepartment(" + deptId + ")");

		Department department = new Department(0, "XX", "Unknown");

		try {
			sql = "select * from department  where dept_id= " + deptId; // .
			// equalsIgnoreCase();
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				department = new Department(rs.getInt("dept_id"), rs.getString("dept_code"), rs.getString("description"));
			}

			rs.close();

			return department;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the list of departments
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getDepartmentList() throws Exception {
		logger.debug("getDepartmentList()");

		List departmentList = new ArrayList();

		try {
			sql = "select * from department order by description";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				Department department = new Department(rs.getInt("dept_id"), rs.getString("dept_code"), rs.getString("description"));
				departmentList.add(department);
			}

			rs.close();

			return departmentList;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the PSA Type for a PSA ID
	 * 
	 * @param psaId
	 * @return
	 * @throws Exception
	 */
	public static String getPsaType(String psaId) throws Exception {
		logger.debug("getPsaType(" + psaId + ")");

		String sql = "";
		String psaType = "";

		try {
			sql = "SELECT PROJ_ID FROM PROJECT WHERE PROJ_ID = " + psaId;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				return "P";
			} else {
				sql = "SELECT SPROJ_ID FROM SUB_PROJECT WHERE SPROJ_ID = " + psaId;
				logger.debug(sql);
				rs = new Wrapper().select(sql);

				if (rs.next()) {
					return "S";
				} else {
					sql = "SELECT ACT_ID FROM ACTIVITY WHERE ACT_ID = " + psaId;
					logger.debug(sql);
					rs = new Wrapper().select(sql);

					if (rs.next()) {
						return "A";
					}
				}
			}

			return psaType;
		} catch (Exception e) {
			logger.error("Exception in getPsaType of LookupAgent - " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the action code description for given action code.
	 * 
	 * @param actnCode
	 * @return
	 * @throws Exception
	 */
	public static String getActionCodeDesc(String actnCode) throws Exception {
		logger.debug("getActionCodeDesc(" + actnCode + ")");

		String actionCodeDescription = "";

		try {
			String sql = "SELECT description FROM LKUP_INSPCT_CODE WHERE ACTN_CODE = " + actnCode;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				actionCodeDescription = rs.getString("description");
			}

			rs.close();

			return actionCodeDescription;
		} catch (Exception e) {
			logger.error("Exception in getActionCodeDesc of LookupAgent - " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the activity number for the activity id
	 * 
	 * @param activityId
	 * @return
	 * @throws Exception
	 */
	public static String getActivityNumberForActivityId(String activityId) {
		logger.debug("getActivityNumberForActivityId(" + activityId + ")");

		String activityNumber = "";

		try {
			String sql = "SELECT ACT_NBR FROM ACTIVITY WHERE ACT_ID = " + activityId;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				activityNumber = rs.getString("ACT_NBR");
			}

			rs.close();

			return activityNumber;
		} catch (Exception e) {
			logger.error("Exception in getActionNumberForActivityId of LookupAgent - " + e.getMessage());
			return "";

		}
	}

	/**
	 * Get the lso id given the psa id and psa type.
	 * 
	 * @param psaId
	 * @param PsaType
	 * @return int
	 * @throws Exception
	 */
	public static int getLsoIdForPsaId(String psaId, String psaType) throws Exception {
		logger.info("getLsoIdForPsaId(" + psaId + "," + psaType + ")");

		RowSet rs = null;
		int lsoId = -1;
		String sql = "";

		try {
			if (psaType.equals("P")) {
				sql = "SELECT LSO_ID FROM V_PSA_LIST WHERE PROJ_ID = " + psaId;
				logger.debug(sql);
				rs = new Wrapper().select(sql);
			} else if (psaType.equals("S")) {
				sql = "SELECT LSO_ID FROM V_PSA_LIST WHERE SPROJ_ID = " + psaId;
				logger.debug(sql);
				rs = new Wrapper().select(sql);
			} else if (psaType.equals("A")) {
				sql = "SELECT LSO_ID FROM V_PSA_LIST WHERE ACT_ID = " + psaId;
				logger.debug(sql);
				rs = new Wrapper().select(sql);
			}  
			if (rs.next()) {
				lsoId = rs.getInt(1);
			}

			logger.debug("The obtained lso id is " + lsoId);

			return lsoId;
		} catch (Exception e) {
			logger.error("Exception in getLsoIdForPsaId of LookupAgent - " + e.getMessage());

			throw e;
		}
	}

	/**
	 * Gets the project number for a given project id.
	 * 
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public static String getProjectNumberForProjectId(String projectId) throws Exception {
		logger.debug("getProjectNumberForProjectId(" + projectId + ")");

		String projectNumber = "";
		String sql = "";

		try {
			sql = "SELECT PROJECT_NBR FROM PROJECT WHERE PROJ_ID = " + projectId;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				projectNumber = rs.getString("PROJECT_NBR");
			}

			rs.close();

			return projectNumber;
		} catch (Exception e) {
			logger.error("Exception in getProjectNumberForProjectId of LookupAgent - " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Get inspection items description for a given id.
	 * 
	 * @param itemId
	 * @return
	 * @throws Exception
	 */
	public static String getInspctItemDescFromId(String itemId) throws Exception {
		logger.debug("getInspctItemDescFromId(" + itemId + ")");

		String inspectionItemDescription = "";

		try {
			sql = "SELECT description FROM LKUP_INSPCT_ITEM WHERE INSPCT_ITEM_ID = " + itemId;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				inspectionItemDescription = rs.getString("description");
			}

			rs.close();

			return inspectionItemDescription;
		} catch (Exception e) {
			logger.error("Exception in getInspctItemDescFromId of LookupAgent - " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the activity type for activity id
	 * 
	 * @param act
	 * @return
	 * @throws Exception
	 */
	public static String getActivityTypeForActId(String activityId) throws Exception {
		logger.debug("getActivityTypeForActId(" + activityId + ")");

		String activityType = "";

		try {
			sql = "SELECT ACT_TYPE FROM ACTIVITY WHERE ACT_ID = " + activityId;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				activityType = rs.getString("ACT_TYPE");
			}

			rs.close();

			return activityType;
		} catch (Exception e) {
			logger.error("Exception in getActivityType of LookupAgent - " + e.getMessage());
			throw e;
		}
	}
	
	/**
	 * Gets activity type from lookup table
	 * @param activityTypeId
	 * @return
	 * @throws Exception
	 */
	public static String getLkupActivityTypeForActId(int activityTypeId) throws Exception {
		logger.debug("getLkupActivityTypeForActId(" + activityTypeId + ")");

		String activityType = "";

		try {
			sql = "SELECT TYPE FROM LKUP_ACT_TYPE WHERE TYPE_ID = " + activityTypeId;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				activityType = rs.getString("TYPE");
			}

			rs.close();

			return activityType;
		} catch (Exception e) {
			logger.error("Exception in getActivityType of LookupAgent - " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the activity type description for the given activity type id.
	 * 
	 * @param activityTypeId
	 * @return
	 * @throws Exception
	 */
	public static String getActivityTypeForTypeId(String activityTypeId) throws Exception {
		logger.debug("getActivityTypeForTypeId(" + activityTypeId + ")");

		String activityType = "";

		try {
			sql = "SELECT DESCRIPTION FROM LKUP_ACT_TYPE WHERE TYPE = '"  + activityTypeId + "' ";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				activityType = rs.getString("DESCRIPTION");
			}

			rs.close();

			return activityType;
		} catch (Exception e) {
			logger.error("Exception in getActivityTypeForTypeId of LookupAgent - " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the lot type for a given type id.
	 * 
	 * @param typeId
	 * @return
	 * @throws Exception
	 */
	public static LotType getLotType(String typeId) throws Exception {
		logger.debug("getLotType(" + typeId + ")");

		LotType lotType = new LotType();

		try {
			sql = "select * from lkup_lot_type where type_id=" + typeId;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				lotType.setTypeId(rs.getString("type_id"));
				lotType.setDescription(rs.getString("description"));
			}

			rs.close();

			return lotType;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the list of lot types.
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getLotTypes() throws Exception {
		logger.debug("getLotTypes()");

		List lotTypes = new ArrayList();

		try {
			sql = "select * from lkup_lot_type order by description";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				LotType lotType = new LotType(rs.getString("type_id"), rs.getString("description"));
				lotTypes.add(lotType);
			}

			rs.close();

			return lotTypes;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of micro film status
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getMicrofilmStatuses() throws Exception {
		logger.debug("getMicrofilmStatuses()");

		List microfilmStatuses = new ArrayList();

		try {
			sql = "SELECT * FROM LKUP_MICROFILM_STATUS";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				MicrofilmStatus status = new MicrofilmStatus(rs.getString("CODE"), rs.getString("description"));
				microfilmStatuses.add(status);
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}

		return microfilmStatuses;
	}

	/**
	 * Get Microfilm status
	 * 
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public static MicrofilmStatus getMicrofilmStatus(String code) throws Exception {
		logger.debug("getMicrofilmStatus()");

		MicrofilmStatus status;

		try {
			sql = "SELECT * FROM LKUP_MICROFILM_STATUS WHERE CODE ='" + code + "'";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				status = new MicrofilmStatus(rs.getString("CODE"), rs.getString("description"));
			} else {
				status = new MicrofilmStatus("", "");
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}

		return status;
	}

	/**
	 * Gets the parking zone object for a parking zone id.
	 * 
	 * @param pZoneId
	 * @return
	 * @throws Exception
	 */
	public static ParkingZone getParkingZone(int pZoneId) throws Exception {
		logger.info("getParkingZone(" + pZoneId + ")");

		ParkingZone pZone = new ParkingZone();

		try {
			Wrapper db = new Wrapper();
			String sql = "select pzone_id,name from parking_zone where pzone_id=" + pZoneId;
			logger.debug(sql);

			RowSet rs = db.select(sql);

			if (rs.next()) {
				pZone = new ParkingZone(rs.getString("pzone_id"), rs.getString("name"));
			}

			rs.close();

			return pZone;
		} catch (Exception e) {
			logger.error("Error in getParkingZone " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of parking zones.
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List<ParkingZone> getParkingZones() throws Exception {
		logger.debug("getParkingZones()");

		String sql = "select pzone_id,name from parking_zone where name is not null order by name";
		logger.debug(sql);

		List parkingZones = new ArrayList();

		try {
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);

			while (rs.next()) {
				ParkingZone pZone = new ParkingZone(rs.getString("pzone_id"), rs.getString("name").trim());
				parkingZones.add(pZone);
			}

			rs.close();
			logger.debug("Returning parking zones of size " + parkingZones.size());
			return parkingZones;
		} catch (Exception e) {
			logger.error("Error in getting Parking Zones " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the list of user groups.
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getGroups() throws Exception {
		logger.debug("getGroups()");

		List groups = new ArrayList();

		try {
			sql = "select * from groups order by  name";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				Group group = new Group(rs.getInt("group_id"), StringUtils.properCase(rs.getString("name")), rs.getString("description"));
				groups.add(group);
			}

			rs.close();

			return groups;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the role object for the given role id.
	 * 
	 * @param roleId
	 * @return
	 * @throws Exception
	 */
	public static Role getRole(int roleId) throws Exception {
		logger.debug("getRole(" + roleId + ")");

		Role role = new Role();

		try {
			sql = "select * from roles where role_id=" + roleId;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				role.setRoleId(rs.getInt("role_id"));
				role.setDescription(rs.getString("description"));
			}

			rs.close();

			return role;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of roles
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getRoles() throws Exception {
		logger.debug("getRoles()");

		List roles = new ArrayList();

		try {
			sql = "select * from roles order by description";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				Role role = new Role(rs.getInt("role_id"), rs.getString("description"));
				roles.add(role);
			}

			rs.close();

			return roles;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of env det
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getEnvDetList() throws Exception {
		logger.debug("getEnvDetList()");

		List list = new ArrayList();

		try {
			sql = "select env_det_id,description from lkup_env_det order by sort_order";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				LookupType lookupType = new LookupType(rs.getString("env_det_id"), rs.getString("description"));
				list.add(lookupType);
			}

			rs.close();

			return list;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of env det actions.
	 * 
	 * @param envDetId
	 * @return
	 * @throws Exception
	 */
	public static List getEnvDetActionList(String envDetId) throws Exception {
		logger.debug("getEnvDetActionList(" + envDetId + ")");

		String sql = "";
		List list = new ArrayList();

		try {
			sql = "select env_det_action_id,description,env_det_id from lkup_env_det_action " + " where env_det_id = " + envDetId + " order by env_det_action_id";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				LookupType lookupType = new LookupType(rs.getString("env_det_action_id"), rs.getString("description"), rs.getString("env_det_id"));
				list.add(lookupType);
			}

			rs.close();

			return list;
		} catch (Exception e) {
			logger.warn(sql);
			logger.error(e.getMessage());
			throw e;
		}
	}

	public static List getActivitySubTypesList(String ids) throws Exception {
		logger.debug("getActivitySubTypeIdsList(" + ids + ")");

		List activitySubTypeLS = new ArrayList();
		Wrapper db = new Wrapper();

		String sql = "select act_subtype from lkup_act_subtype lasts where  lasts.ACT_SUBTYPE_ID in" + ids;
		RowSet subTypeRS;

		try {
			logger.debug(sql);
			subTypeRS = db.select(sql);

			while (subTypeRS.next()) {
				String s1 = subTypeRS.getString("ACT_SUBTYPE");
				activitySubTypeLS.add(s1);
			}

			subTypeRS.close();

			return activitySubTypeLS;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of Site Use
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getSiteUseList() throws Exception {
		logger.debug("getSiteUseList()");

		String sql = "select * from lkup_site_use order by description";
		List siteList = new ArrayList();

		try {
			logger.debug(sql);

			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);

			while (rs.next()) {
				SiteUse siteUse = new SiteUse(rs.getInt("SITE_USE_ID"), rs.getString("DESCRIPTION"), rs.getInt("USE_FACTOR"), rs.getString("SURVEY_TYPE"));
				siteList.add(siteUse);
			}

			rs.close();
		} catch (Exception e) {
			logger.error("Error in getting SIERRA Streets " + e.getMessage());
			throw e;
		}

		return siteList;
	}

	/**
	 * Gets the list of Roof Types
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getSiteRoofList() throws Exception {
		logger.debug("getSiteRoofList()");

		String sql = "select * from lkup_site_roofing order by description";
		List roofTypeList = new ArrayList();

		try {
			logger.debug(sql);

			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);

			while (rs.next()) {
				PickList element = new PickList(rs.getInt("ROOF_TYPE"), rs.getString("DESCRIPTION"));
				roofTypeList.add(element);
			}

			rs.close();
		} catch (Exception e) {
			logger.error("Error in getting Site Use List " + e.getMessage());
			throw e;
		}

		return roofTypeList;
	}

	/**
	 * Gets the list of Site Locations
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getSiteLocationList() throws Exception {
		logger.debug("getSiteLocationList()");

		String sql = "select * from lkup_site_location order by description";
		List locationList = new ArrayList();

		try {
			logger.debug(sql);

			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);

			while (rs.next()) {
				PickList element = new PickList(rs.getString("LOCATION"), rs.getString("DESCRIPTION"));
				locationList.add(element);
			}

			rs.close();
		} catch (Exception e) {
			logger.error("Error in getting Location List " + e.getMessage());
			throw e;
		}

		return locationList;
	}

	/**
	 * Gets the list of Site Locations
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getSiteZoneList() throws Exception {
		logger.debug("getSiteZoneList()");

		String sql = "SELECT * from zone order by ZONE";
		List zoneList = new ArrayList();

		try {
			logger.debug(sql);

			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);
			while (rs.next()) {
				PickList element = new PickList(rs.getInt("ZONE_ID"), (StringUtils.nullReplaceWithEmpty(rs.getString("ZONE"))).trim());
				zoneList.add(element);
			}

			rs.close();
		} catch (Exception e) {
			logger.error("Error in getting Zone List " + e.getMessage());
			throw e;
		}

		return zoneList;
	}

	/**
	 * Gets the list SiteConst Type
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getSiteConstTypeList() throws Exception {
		logger.debug("getSiteConstTypeList()");

		String sql = "select * from lkup_site_consttype order by description";
		List roofTypeList = new ArrayList();

		try {
			logger.debug(sql);

			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);

			while (rs.next()) {
				PickList element = new PickList(rs.getInt("CONST_TYPE"), rs.getString("DESCRIPTION"));
				roofTypeList.add(element);
			}

			rs.close();
		} catch (Exception e) {
			logger.error("Error in getting Site Construction Type List " + e.getMessage());
			throw e;
		}

		return roofTypeList;
	}

	public static String getStructureZone(String lsoId) {
		String zone = "";

		String sql = "select zone from zone where zone_id in(select zone_id from land_zone where land_id in(select land_id from v_lso_land where structure_id=" + lsoId + "))";

		try {
			logger.debug(sql);

			Wrapper db = new Wrapper();
			java.sql.ResultSet rs = db.select(sql);

			while (rs.next()) {
				zone = zone + rs.getString("zone") + ",";
			}
		} catch (Exception e) {
			logger.debug(e.getMessage());
		}

		return zone;
	}

	public static String getOcuupancyZone(String lsoId) {
		String zone = "";

		String sql = "select zone from zone where zone_id in(select zone_id from land_zone where land_id in(select land_id from v_lso_land where structure_id in(select structure_id from STRUCTURE_OCCUPANT where occupancy_id=" + lsoId + ")))";

		try {
			Wrapper db = new Wrapper();
			logger.debug(sql);

			java.sql.ResultSet rs = db.select(sql);

			while (rs.next()) {
				zone = zone + rs.getString("zone") + ",";
			}
		} catch (Exception e) {
			logger.debug(e.getMessage());
		}

		return zone;
	}

	public static String getProjectNameForActivityNumber(String activityNumber) {
		logger.debug("getProjectNameForActivityNumber(" + activityNumber + ")");

		String projectName = "";

		try {
			sql = "select name from project where proj_id =(select proj_id from sub_project where sproj_id = (select sproj_id from activity where act_nbr=" + StringUtils.checkString(activityNumber) + "))";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				projectName = rs.getString("name");
			}

			rs.close();

		} catch (Exception e) {
			logger.error(e.getMessage());

		}
		return projectName;
	}

	public static String getProjectNameForActivityId(String activityId) {
		logger.debug("getProjectNameForActivityId(" + activityId + ")");

		String projectName = "";

		try {
			sql = "select name from project where proj_id =(select proj_id from sub_project where sproj_id = (select sproj_id from activity where act_id=" + activityId + "))";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				projectName = rs.getString("name");
			}

			rs.close();

		} catch (Exception e) {
			logger.error(e.getMessage());

		}
		return projectName;
	}

	public static String getProjectNameForProjectId(String projectId) throws Exception {
		logger.debug("getProjectNameForProjectId(" + projectId + ")");

		String projectName = "";

		try {
			sql = "select name from project where proj_id =" + projectId;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				projectName = rs.getString("name");
			}

			rs.close();

			return projectName;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public static String getSubProjectNameForActivityId(String activityId) throws Exception {
		logger.debug("getSubProjectNameForActivityId(" + activityId + ")");

		String subProjectName = "";

		try {
			sql = "select lp.description as sub_project_name from activity a join sub_project sp on sp.sproj_id=a.sproj_id left outer join lkup_ptype lp on lp.ptype_id = sp.sproj_type  where act_id=" + activityId;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				subProjectName = rs.getString("sub_project_name");
			}

			rs.close();

			return subProjectName;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public static String getProjectName(int projectNameId) throws Exception {
		logger.debug("getProjectName(" + projectNameId + ")");

		String projectName = "";

		try {
			sql = "select * from lkup_pname where pname_id=" + projectNameId;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				projectName = rs.getString("name");
			}

			rs.close();

			return projectName;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}
	
	public static String getProjectName(String deptCode) throws Exception {
		logger.debug("getProjectName(" + deptCode + ")");

		String projectName = "";

		try {
			sql = "select * from lkup_pname where DEPT_CODE='" + deptCode + "'";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				projectName = rs.getString("name");
			}

			rs.close();

			return projectName;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public static String getProjectNameCode(String projectName) throws Exception {
		logger.debug("getProjectNameCode(" + projectName + ")");

		String projectNameCode = "";

		try {
			sql = "select * from lkup_pname where name=" + StringUtils.checkString(projectName);
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				projectNameCode = rs.getString("dept_code");
			}

			rs.close();

			return projectNameCode;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public static String getSubProjectTypeId(String subProjectId) throws Exception {
		logger.debug("getSubProjectTypeId(" + subProjectId + ")");

		String subProjectTypeId = "";

		try {
			sql = "select * from sub_project where sproj_id=" + subProjectId;
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				subProjectTypeId = rs.getString("stype_id");
			}

			rs.close();

			return subProjectTypeId;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get APN for activity ID.
	 * 
	 * @param activityId
	 * @return
	 * @throws Exception
	 */
	public static String getApnForActivityId(String activityId) throws Exception {
		logger.debug("getApnForActivityId(" + activityId + ")");

		String apn = "";

		try {
			sql = "select * from v_lso_owner where lso_id in (select occupancy_id from structure_occupant where structure_id in ( select lso_id from v_activity_address where act_id=" + activityId + "))";
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				apn = rs.getString("apn");
			}

			rs.close();

			return apn;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get APN for lso ID.
	 * 
	 * @param lso
	 *            id
	 * @return
	 * @throws Exception
	 */
	public static String getApnForLsoId(String lsoId) throws Exception {
		logger.debug("getApnForLsoId(" + lsoId + ")");

		String apn = "";

		try {
			sql = "select * from lso_apn where lso_id =" + lsoId;
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				apn = StringUtils.apnWithHyphens(rs.getString("apn"));
			}

			rs.close();

			return apn;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get Inspector for APN.
	 * 
	 * @param apnNo
	 * @return inspetorId
	 * @throws Exception
	 */
	public static int getInspectorForApn(String apnNo) throws Exception {
		logger.debug("getInspectorForApn(" + apnNo + ")");

		int inspetorId = 0;
		try {

			sql = "select * from lkup_apn_inspector where apn =" + StringUtils.checkString(apnNo);
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				inspetorId = rs.getInt("user_id");
			}

			rs.close();

			return inspetorId;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Function to get a user For given userId/inspectorId
	 */
	public static String getInspectorUser(int inspetorId) {

		String inspectorUser = null;
		ResultSet rs = null;

		try {
			if (inspetorId != 0) {
				String sql = "SELECT USERID, FIRST_NAME, LAST_NAME FROM  USERS WHERE USERID = " + inspetorId;

				logger.debug("inspector user for userId " + sql);

				rs = new Wrapper().select(sql);

				while (rs.next()) {
					inspectorUser = (rs.getString("FIRST_NAME") + " " + rs.getString("LAST_NAME"));
					logger.debug("User/Inspector Name " + inspectorUser);
				}
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return inspectorUser;
	}

	/**
	 * Get Owner for lso ID.
	 * 
	 * @param lso
	 *            id
	 * @return
	 * @throws Exception
	 */
	public static String getOwnerForLsoId(String lsoId) throws Exception {
		logger.debug("getOwnerForLsoId(" + lsoId + ")");

		String owner = "";

		try {
			sql = "select * from v_lso_owner where lso_id =" + lsoId;
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				owner = rs.getString("name");
			}

			rs.close();

			return owner;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get breadcrump for the given activity id, breadcrump is defined as below Address -- Activity Number -- Activity Type
	 * 
	 * @param activityId
	 * @return
	 * @throws Exception
	 */
	public static String getBreadCrump(String activityId) {
		logger.debug("getBreadCrump(" + activityId + ")");

		String breadCrump = "";

		try {
			sql = "select val.dl_address || ' -- ' || va.act_nbr || ' - ' || lat.description as breadcrump from v_activity va left outer join lkup_act_type lat on lat.type=va.act_type join v_address_list val on va.addr_id=val.addr_id where va.act_id=" + activityId;
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				breadCrump = rs.getString("breadcrump");
			}

			rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return breadCrump;
	}

	/**
	 * Gets the list of Business Name status.
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getApprovalStatuses() throws Exception {
		logger.debug("getApprovalStatuses()");

		List approvalStatuses = new ArrayList();
		ApprovalStatus approvalStatus = null;

		try {

			sql = "select * from LKUP_APPL_ST";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				int id = rs.getInt("ID");
				String description = rs.getString("DESCRIPTION").trim();
				approvalStatus = new ApprovalStatus(id, description);
				approvalStatuses.add(approvalStatus);

			}

			rs.close();

			return approvalStatuses;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of Business Name status.
	 * 
	 * @return
	 * @throws Exception
	 */
	public static String getActivityDesc(String actType) throws Exception {
		logger.debug("getApprovalStatuses()");

		try {

			sql = "select * from LKUP_ACT_TYPE where TYPE='" + actType + "'";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);
			String description = "";

			while (rs.next()) {

				description = rs.getString("DESCRIPTION").trim();

			}

			rs.close();

			return description;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of Business Name status.
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getModules() throws Exception {
		logger.debug("getModules()");

		List modules = new ArrayList();
		Module module = null;

		try {

			sql = "select * from LKUP_MODULE order by description";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				int id = rs.getInt("ID");
				String description = rs.getString("DESCRIPTION").trim();
				module = new Module(id, description);
				modules.add(module);

			}

			rs.close();

			return modules;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public static String getActivityTypeForFees() throws Exception {
		logger.debug("getActivityTypesForFees()");

		StringBuffer activityTypes = new StringBuffer();
		ActivityType activityType = new ActivityType();

		try {
			sql = "select lm.description as mod,id from lkup_module lm";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				String mod = StringUtils.properCase("***" + rs.getString("mod") + "***");
				logger.debug("mod-->" + StringUtils.properCase("***" + rs.getString("mod") + "***"));
				activityTypes.append(mod);
				activityTypes.append(",");

				sql = "select lat.description,lat.type from lkup_act_type lat where lat.module_id=" + rs.getInt("id") + " ";
				RowSet rs1 = new Wrapper().select(sql);
				while (rs1.next()) {
					String description = StringUtils.properCase(rs1.getString("description"));
					logger.debug("description-->" + StringUtils.properCase(rs1.getString("description")));
					activityTypes.append(description);
					activityTypes.append(",");

				}

			}

			rs.close();

			return activityTypes.toString();
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public static String getActivityTypeForFees1() throws Exception {
		logger.debug("getActivityTypesForFees()");

		StringBuffer activityTypes = new StringBuffer();
		ActivityType activityType = new ActivityType();
		StringBuffer act_type = new StringBuffer();
		try {
			String sql1 = "select description,id from lkup_module";
			logger.debug(sql1);

			RowSet rs = new Wrapper().select(sql1);

			while (rs.next()) {

				String mod = StringUtils.properCase("***" + rs.getString("description") + "***");
				act_type.append(mod);
				act_type.append(",");

				String sql2 = "select lat.description,lat.type from lkup_act_type lat where lat.module_id=" + rs.getInt("id") + " ";
				logger.debug("SQL****" + sql2);
				RowSet rs1 = new Wrapper().select(sql2);
				while (rs1.next()) {
					String act_types = StringUtils.properCase(rs1.getString("type"));
					act_type.append(act_types);
					act_type.append(",");

				}

			}

			rs.close();

			return act_type.toString();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			throw e;
		}
	}

	public static List getActivitydescriptionForFees() throws Exception {
		logger.debug("getActivityTypesForFees()");

		List activityTypes = new ArrayList();

		ActivityType activityType = new ActivityType();

		try {
			sql = "select lat.type,lat.description,lm.description as mod from lkup_act_type lat left outer join lkup_module lm on lm.id=lat.module_id order by mod,description";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {

				String description = StringUtils.properCase(rs.getString("description"));
				activityType.setDescription(description);
				activityTypes.add(activityType);
				logger.debug("activityTypes-->" + activityTypes.size());
				logger.debug("activityTypes-->" + activityTypes);
			}

			rs.close();

			return activityTypes;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get Address for apn.
	 * 
	 * @param apn
	 * @return
	 * @throws Exception
	 */
	public static String getAddressForApn(String apn) throws Exception {
		logger.debug("getAddressForApn(" + apn + ")");

		String address = "";

		try {
			sql = "select * from v_lso_owner where apn =" + StringUtils.checkString(apn);
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				address = rs.getString("STR_NO") + " " + rs.getString("STR_MOD") + " " + rs.getString("STR_NAME") + " " + rs.getString("UNIT");
			}
			rs.close();
			return address;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get Owner for apn.
	 * 
	 * @param apn
	 * @return
	 * @throws Exception
	 */
	public static String getOwnerForApn(String apn) throws Exception {
		logger.debug("getOwnerForApn(" + apn + ")");

		String owner = "";

		try {
			sql = "select * from v_lso_owner where apn =" + StringUtils.checkString(apn);
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				owner = rs.getString("name");
			}
			rs.close();
			return owner;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get LSO ID for apn.
	 * 
	 * @param apn
	 * @return
	 * @throws Exception
	 */
	public static int getLsoIdForApn(String apn) throws Exception {
		logger.debug("getLsoIdForApn(" + apn + ")");

		int lsoId = -1;

		try {
			sql = "select * from v_lso_apn where apn =" + StringUtils.checkString(apn);
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				lsoId = rs.getInt("lso_id");
			}
			rs.close();
			return lsoId;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of Eye color types
	 * 
	 * @return
	 */
	public static List getEyeColors() throws Exception {
		logger.debug("getQuantityTypes()");

		List eyeColors = new ArrayList();
		EyeColorType eyeColorType = new EyeColorType();

		try {
			sql = "select * from lkup_eye_color order by description";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String description = StringUtils.properCase(rs.getString("description"));
				eyeColorType = new EyeColorType(id, description);
				eyeColors.add(eyeColorType);
			}

			rs.close();
			logger.debug("returning with eye color types list of size " + eyeColors.size());

			return eyeColors;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of holds
	 * 
	 * @return
	 */
	public static int getHolds(String actId) throws Exception {
		logger.debug("getHolds()");
		int count = 0;

		try {
			sql = "select count(*) as count from holds where hold_level='A' and stat = 'A' and level_id =" + actId;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				count = rs.getInt("count");
			}

			rs.close();
			logger.debug("returning with hold count ");

			return count;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}
	
	
	/**
	 * Gets activity type count matching lkup_act_type table
	 * 
	 * @return
	 */
	public static int getDepartmentForActType(String actType) throws Exception {
		logger.debug("getDepartmentForActType()");
		int count = 0;

		try {
			sql = "select count(*) as count from lkup_act_type where type= '"+actType+ "' and dept_id =" + Constants.DEPARTMENT_HOUSING;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				count = rs.getInt("count");
			}

			rs.close();
			logger.debug("returning with activityType count ");

			return count;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}
	
	

	/**
	 * Gets the count of duplicate records of Approvals
	 * 
	 * @return boolean
	 */

	public static boolean checkForDuplicates(String activityIds, String departmentId, int userId) throws Exception {
		logger.info("checkForDuplicates(" + userId + ")");

		String sql = "";
		Wrapper db = new Wrapper();
		int count = 0;
		boolean duplicateExists = false;

		try {
			// check if the entry exists for duplicates
			sql = "select count(*)as count from bl_approval where ACT_ID=" + activityIds + " and  APPROVED_BY= " + userId + " and dept_id=" + StringUtils.s2i(departmentId);
			logger.info(sql);

			RowSet rs = db.select(sql);
			while (rs != null && rs.next()) {
				count = rs.getInt("count");
			}
			logger.debug("**** count " + count);
			if (count > 0) {
				duplicateExists = true;
			}
			return duplicateExists;

		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Get the list of activity sub types for an Sub type IDs.
	 * 
	 * @param ids
	 * @return
	 */
	public static List getActSubTypes(String ids) throws Exception {
		logger.debug("getActSubTypes(" + ids + ")");

		List activitySubTypes = new ArrayList();

		String sql = "select * from lkup_act_subtype where act_subtype_id in ( select act_subtype_id from act_subtype ast where ast.act_id= " + ids + " ) order by act_subtype";
		logger.debug(sql);

		try {
			Wrapper db = new Wrapper();

			RowSet rs = db.select(sql);

			while (rs != null && rs.next()) {
				activitySubTypes.add(rs.getString("act_subtype"));
			}
			rs.close();
			return activitySubTypes;
		} catch (SQLException e) {
			logger.error("SQL Error in getActivitySubTypes " + e.getMessage());
			throw e;
		} catch (Exception e) {
			logger.error("Error in getActivitySubTypes " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Function to find a user attached to an activity which contains hold
	 */

	public static String getMostRecentHoldStatus(String actId) throws Exception {
		logger.debug("getHolds()");
		String status = "";
		String sqlHold = "";

		try {

			sqlHold = "select stat from (select stat from HOLDS where level_id =" + actId + " order by update_dt desc) where  ROWNUM <= 1";

			RowSet rs = new Wrapper().select(sqlHold);

			while (rs.next()) {
				status = rs.getString("stat");
			}

			rs.close();
			logger.debug("Most recent hold query : " + sqlHold);
			logger.debug("returning with most recent date : " + status);

			return status;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Function to find a user attached to an activity which contains hold
	 */

	public static String getMostRecentHoldType(String actId) throws Exception {
		logger.debug("getHolds()");
		String holdInfo = "";
		String sqlHold = "";

		try {
			sqlHold = "select hold_type from (select hold_type from HOLDS where level_id =" + actId + " order by update_dt desc) where  ROWNUM <= 1";

			RowSet rs = new Wrapper().select(sqlHold);

			while (rs.next()) {
				holdInfo = rs.getString("hold_type");
			}
			rs.close();
			logger.debug("Most recent hold query : " + sqlHold);
			logger.debug("returning with most recent date :  " + holdInfo);

			return holdInfo;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the getActivityStatusId
	 * 
	 * @return
	 * @throws Exception
	 */
	public static int getActivityStatusId(String activityIdStr) throws Exception {
		logger.debug("getActivityStatusId(" + activityIdStr + ")");

		int actStatusId = 0;

		try {
			sql = "select * from activity where act_id= " + Integer.parseInt(activityIdStr);
			logger.debug("Query is :: " + sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				actStatusId = rs.getInt("status");

			}
			logger.debug("actStatusId is :: " + actStatusId);
			rs.close();

			return actStatusId;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public static String getDepartmentCode(String description) throws Exception {

		logger.debug("getDepartmentId(" + description + ")");

		// Department department = new Department(0, "XX", "Unknown");
		try {

			sql = "select * from department  where description= " + StringUtils.checkString(description); // .
			// equalsIgnoreCase
			// ();
			RowSet rs = new Wrapper().select(sql);
			String deptCode = "";
			if (rs.next()) {

				deptCode = rs.getString("dept_code");
			}

			rs.close();

			return deptCode;
		} catch (Exception e) {

			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the departmentId for a given department description.
	 * 
	 * @param code
	 * @return
	 * @throws Exception
	 */

	public static int getDepartmentId(String code) throws Exception {

		logger.debug("getDepartmentId(" + code + ")");

		try {

			sql = "select * from department  where dept_code= " + StringUtils.checkString(code); // .
			RowSet rs = new Wrapper().select(sql);
			int deptId = 0;
			if (rs.next()) {

				deptId = rs.getInt("dept_id");
			}

			rs.close();

			return deptId;
		} catch (Exception e) {

			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get SubProject Name for sproj ID.
	 * 
	 * @author Gayathri
	 * @param sprojId
	 * @return
	 * @throws Exception
	 */
	public static String getSprojNameForSprojId(String subProjectId) throws Exception {
		logger.debug("getSprojNameForSprojId(" + subProjectId + ")");

		String sprojName = "";

		try {
			sql = "select lpt.DESCRIPTION as DESCRIPTION from lkup_ptype lpt left outer join sub_project sp on sp.SPROJ_TYPE = lpt.PTYPE_ID where sp.SPROJ_ID =" + subProjectId;
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				sprojName = rs.getString("DESCRIPTION");
			}

			rs.close();

			return sprojName;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public static String[] getActivityAddress(int actId) throws Exception {

		String sql = "select  * from v_activity_address where act_id =" + actId;

		logger.info(sql);

		String s[] = new String[3];

		RowSet rs = new Wrapper().select(sql);

		if (rs.next()) {

			s[0] = rs.getString("ADDRESS");
			s[1] = rs.getString("CITYSTATEZIP");

		}

		return s;

	}

	public static int getActivityIdFromInspectionId(int inspId) throws Exception {

		String sql = "select * from Inspection where INSPECTION_ID =" + inspId;

		logger.info(sql);

		int s = 0;

		RowSet rs = new Wrapper().select(sql);

		if (rs.next()) {
			s = rs.getInt("ACT_ID");
		}

		return s;

	}

	/**
	 * Get the list of activity types for given sub project name id.
	 * 
	 * @param subProjectId
	 * @param abc
	 * @return
	 */
	public static List<ActivityType> getActivityTypes(int subProjectTypeId) throws Exception {
		logger.debug("getActivityTypes(" + subProjectTypeId + ")");

		List activityTypes = new ArrayList();
		ActivityType activityType = new ActivityType();

		try {
			sql = "SELECT TYPE,DESCRIPTION FROM LKUP_ACT_TYPE WHERE TYPE IN (SELECT ACT_TYPE FROM SPTYPE_SPSTYPE_ACTTYPE WHERE SPROJ_TYPE = " + subProjectTypeId + ")";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				String type = rs.getString("type");
				String description = rs.getString("description");
				activityType = new ActivityType(type, description);
				activityTypes.add(activityType);
			}

			rs.close();

			return activityTypes;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}
	
	/**
	 * These are keys and values from lookup system table
	 * @param keyName
	 * @return
	 * @throws Exception
	 */
	public static String getKeyValue(String keyName) throws Exception {
		String sql = "SELECT VALUE FROM LKUP_SYSTEM WHERE NAME=" + StringUtils.checkString(keyName);
		logger.info(sql);
		RowSet rs = new Wrapper().select(sql);
		if (rs.next()) {
			KEY_VALUE = rs.getString(1);
		}
		return KEY_VALUE;
	}
	

	/**
	 * @param act_typ
	 * @return module Id
	 * @throws Exception
	 */
	public static int getModuleId(String act_typ) throws AgentException {
		logger.debug("getModuleId(" + act_typ + ")");
		int id = 0;
		try {

			sql = "select MODULE_ID from LKUP_ACT_TYPE WHERE TYPE = '" + act_typ + "' order by description";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				id = rs.getInt("MODULE_ID");
			}

			rs.close();

			return id;
		} catch (Exception e) {
			logger.error("", e);
			throw new AgentException("", e);
		}
	}
	public static String getLSOType(int lsoId) throws Exception {
		logger.debug("getLSOType()");
		try {
			String sql = "";
			sql = "SELECT lso_type FROM V_LSO_TYPE where LSO_ID=" + lsoId;
			RowSet rs = new Wrapper().select(sql);
			String lsoType = "";
			while (rs.next()) {

				lsoType = rs.getString("lso_type");
				logger.debug("lso_type : " + lsoType);
			}

			rs.close();

			return lsoType;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}

	}
	
	public static List getActivitySubTypesByActTypeId(int activityTypeId) throws AgentException {
		logger.debug("getActivitySubTypesByActTypeId(" + activityTypeId + ")");

		List activitySubTypes = new ArrayList();
		String sql = "select * from lkup_act_subtype where ACT_TYPE = (select type from lkup_act_type where TYPE_id=" + activityTypeId + ")  order by act_subtype";

		ActivitySubType subType = new ActivitySubType();

		try {
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				ActivitySubType activitySubType = new ActivitySubType(rs.getInt("act_subtype_id"), rs.getString("act_subtype"));
				activitySubTypes.add(activitySubType);
			}

			rs.close();

			return activitySubTypes;
		} catch (SQLException e) {
			logger.error("SQL Error in getActivitySubType " + e.getMessage());
			throw new AgentException("", e);
		} catch (Exception e) {
			logger.error("Error in getActivitySubType " + e.getMessage());
			throw new AgentException("", e);
		}
	}
	
	public int getActivityTypeId(String type) throws Exception {
		RowSet rs = null;
		String sql = "";
		int typeId = 0;
		try {
			// rs = new CachedRowSet();
			sql = "select TYPE_ID from LKUP_ACT_TYPE where TYPE='" + type + "'";
			logger.info(sql);
			rs = new Wrapper().select(sql);
			while (rs.next()) {
				typeId = rs.getInt("TYPE_ID");

			}
			return typeId;
		} catch (Exception e) {
			logger.error("Error in getting activity type:" + e.getMessage());
			throw e;
		}
	}
	
	/**
	 * Get the lso id given the psa id and psa type.
	 * 
	 * @param psaId
	 * @param PsaType
	 * @return int
	 * @throws Exception
	 */
	public static int getAddressIdForPsaId(String lsoId) throws Exception {
		logger.info("getAddressIdForPsaId(" + lsoId + ")");

		RowSet rs = null;
		int addrId = -1;
		String sql = "";

		try {

			sql = "SELECT ADDR_ID FROM V_ADDRESS_LIST WHERE LSO_ID = " + lsoId;
			logger.debug(sql);
			rs = new Wrapper().select(sql);

			if (rs.next()) {
				addrId = rs.getInt(1);
			}

			logger.debug("The obtained lso id is " + lsoId);

			return addrId;
		} catch (Exception e) {
			logger.error("Exception in getLsoIdForPsaId of LookupAgent - " + e.getMessage());

			throw e;
		}
	}

	public static List getActivityTypesBasedOnModuleId(String moduleId, int ptypeId) throws Exception {
		logger.info("getActivityTypes(" + moduleId + "," + ptypeId + ")");

		List activityTypes = new ArrayList();
		ActivityType activityType = new ActivityType();
		RowSet rs = null;

		try {

			/*
			 * Activity type depends on Project name and Module id, and it is independent of Department
			 */
			sql = "select type,description from lkup_act_type where MODULE_ID in (" + moduleId + ") and PSA_ADD='Y' order by description asc";
			logger.debug(sql);
			rs = new Wrapper().select(sql);

			while (rs.next()) {
				String type = rs.getString("type");
				String description = rs.getString("description");
				activityType = new ActivityType(type, description);
				activityTypes.add(activityType);
			}

			rs.close();

			return activityTypes;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}
	
	public static List getActivityStatuses(int moduleId, int pnameId) throws Exception {
		logger.debug("getActivityStatuses(" + moduleId + "," + pnameId + ")");

		List activityStatuses = new ArrayList();
		ActivityStatus activityStatus = null;

		try {
			/*
			 * if(pnameId==Constants.NSD_PNAME_ID) { sql = "select * from lkup_act_st where status_id > 0 and module_id=" + moduleId + " and PNAME_ID= "+Constants.NSD_PNAME_ID+" order by  stat_code "; }else { sql = "select * from lkup_act_st where status_id > 0 and module_id=" + moduleId + " and PNAME_ID NOT IN ("+Constants.NSD_PNAME_ID+")order by  stat_code "; }
			 */

			/*
			 * Activity Status will be populated based on Project and Module Id
			 */
			sql = "select * from lkup_act_st where status_id > 0 and module_id=" + moduleId + " and active='Y' order by  stat_code ";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				int statusId = rs.getInt("status_id");
				String code = rs.getString("stat_code").trim();
				String description = rs.getString("description").trim();
				activityStatus = new ActivityStatus(statusId, code, description);
				activityStatuses.add(activityStatus);
			}

			rs.close();

			return activityStatuses;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
		
	}
	
	/*
	 * As we don't have mapping in our DB for default status. This method should be called for default activity status so that once we will have mapping we can change this method to get from db
	 */
	public static int getDefaultStatus(int moduleId, String projectName) {
		int activityStatusId = 0;
		if (moduleId == Constants.MODULEID_FIRE) {
			if (projectName.equalsIgnoreCase(Constants.PROJECT_NAME_PERMITS)) {
				activityStatusId = Constants.ACTIVITY_STATUS_FIRE_READY;
			} else if (projectName.equalsIgnoreCase(Constants.PROJECT_NAME_CASES)) {
				activityStatusId = Constants.ACTIVITY_STATUS_FIRE_CASES_OPEN;
			} else if (projectName.equalsIgnoreCase(Constants.PROJECT_NAME_SYSTEMATIC)) {
				activityStatusId = Constants.ACTIVITY_STATUS_FIRE_SYS_OPEN;
			}
		} else if (moduleId == Constants.MODULEID_BUILDING) {
			if (projectName.equalsIgnoreCase(Constants.PROJECT_NAME_PERMITS)) {
				activityStatusId = Constants.ACTIVITY_STATUS_BUILDING_PERMIT_OPEN;
			} else if (projectName.equalsIgnoreCase(Constants.PROJECT_NAME_MISCELLANEOUS_RECEIPT)) {
				activityStatusId = Constants.ACTIVITY_STATUS_BUILDING_MISCELLANEOUS_OPEN;
			} else if (projectName.equalsIgnoreCase(Constants.PROJECT_NAME_LICENSE)) {
				activityStatusId = Constants.ACTIVITY_STATUS_BUILDING_LICENSE_ACTIVE;
			} else {
				activityStatusId = Constants.ACTIVITY_STATUS_BUILDING_PERMIT_READY;
			}
		} else if (moduleId == Constants.MODULEID_NEIGHBORHOOD_SERVICES) {
			if (projectName.equalsIgnoreCase(Constants.PROJECT_NAME_SYSTEMATIC)) {
				activityStatusId = Constants.ACTIVITY_STATUS_NS_SYS_OPEN;
			} else if (projectName.equalsIgnoreCase(Constants.PROJECT_NAME_CASES)) {
				activityStatusId = Constants.ACTIVITY_STATUS_RFS_OPEN;
			}
		} else if (moduleId == Constants.MODULEID_PLANNING) {
			if (projectName.equalsIgnoreCase(Constants.PROJECT_NAME_CASES)) {
				activityStatusId = Constants.ACTIVITY_STATUS_PLANNING_INPROCESS;
			}
		} else if (moduleId == Constants.MODULEID_CITY_CLERK) {
			if (projectName.equalsIgnoreCase(Constants.PROJECT_NAME_LICENSE)) {
				activityStatusId = Constants.ACTIVITY_STATUS_CC_LICENSES_OPEN;
			} else if (projectName.equalsIgnoreCase(Constants.PROJECT_NAME_PERMITS)) {
				activityStatusId = Constants.ACTIVITY_STATUS_CC_PERMIT_OPEN;
			} else if (projectName.equalsIgnoreCase(Constants.PROJECT_NAME_MISCELLANEOUS_RECEIPT)) {
				activityStatusId = Constants.ACTIVITY_STATUS_CC_MISC_OPEN;
			}
		}
		return activityStatusId;
	}
	
	/**
	 * gets the list of project Name for psa id
	 * 
	 * @return
	 * @throws Exception
	 */
	public static String getProjectName(String psaType, int psaId) throws AgentException {
		logger.debug("getProjectNameId(" + psaType + "," + psaId + ")");

		String projectName = "";

		try {
			if (psaType.equalsIgnoreCase("P")) {
				sql = "select NAME from lkup_pname where name in (select name from project where proj_id in(" + psaId + "))";
			} else if (psaType.equalsIgnoreCase("Q")) {
				sql = "select NAME from lkup_pname where name in (select name from project where proj_id in(select proj_id from sub_project where sproj_id in (" + psaId + ")))";
			} else if (psaType.equalsIgnoreCase("A")) {
				sql = "select NAME from lkup_pname where name in (select name from project where proj_id in(select proj_id from sub_project where sproj_id in (select sproj_id from activity where act_id=" + psaId + ")))";
			} else {
				logger.error("Level unidentified " + psaType);
			}

			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				projectName = rs.getString("NAME");
			}

			rs.close();

			return projectName;
		} catch (Exception e) {
			logger.error("", e);
			throw new AgentException("", e);
		}
	}
	
	/**
	 * Get the activity type for activity id
	 * 
	 * @param act
	 * @return
	 * @throws Exception
	 */
	public static ActivityType getActivityTypeForActId(int actId) throws Exception {
		logger.debug("getActivityTypeForActId(" + actId + ")");

		ActivityType activityType = null;

		try {
			sql = "SELECT LAT.TYPE_ID,LAT.TYPE,LAT.DESCRIPTION,LAT.DEPT_ID,LAT.PTYPE_ID,LAT.MODULE_ID FROM ACTIVITY A LEFT JOIN LKUP_ACT_TYPE LAT ON A.ACT_TYPE = LAT.TYPE WHERE ACT_ID = " + actId;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);
			if (rs != null) {
				activityType = new ActivityType();
				if (rs.next()) {
					activityType.setDepartmentId(rs.getInt("DEPT_ID"));
					activityType.setDescription(rs.getString("DESCRIPTION"));
					activityType.setModuleId(rs.getInt("MODULE_ID"));
					activityType.setSubProjectType(rs.getInt("PTYPE_ID"));
					activityType.setType(rs.getString("TYPE"));
					activityType.setTypeId(rs.getString("TYPE_ID"));
				}

			}
			rs.close();
			return activityType;
		} catch (Exception e) {
			logger.error("Exception in getActivityType of LookupAgent - " + e.getMessage());
			throw e;
		}
	}

	
	/**
	 * Get the count for activity id
	 * 
	 * @param act
	 * @return
	 * @throws Exception
	 */
	public static int getActivityTypeForActIdLong(long actId) throws Exception {
		logger.debug("getActivityTypeForActId(" + actId + ")");

		int count = 0;

		try {
			sql = "SELECT count(*) as count FROM ACTIVITY A LEFT JOIN LKUP_ACT_TYPE LAT ON A.ACT_TYPE = LAT.TYPE WHERE ACT_ID = " + actId+ " AND LAT.DEPT_ID ="+Constants.DEPARTMENT_HOUSING;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);
			if (rs != null) {
				
				if (rs.next()) {
					count = rs.getInt("count");
				}

			}
			rs.close();
			return count;
		} catch (Exception e) {
			logger.error("Exception in getActivityType of LookupAgent - " + e.getMessage());
			throw e;
		}
	}

	

	/**
	 * Get the list of Category Values
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getCategoryList(String category_type) throws Exception {
		logger.debug("getCategoryList()");
		logger.debug("category_type=" + category_type);
		List categoryList = new ArrayList();

		try {
			sql = "select * from lkup_categories where category_type='" + category_type + "'";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				CategoryForm category = new CategoryForm(rs.getInt("CATEGORY_ID"), rs.getString("CATEGORY_TYPE"), rs.getString("CATEGORY_VALUE"));
				categoryList.add(category);
			}

			rs.close();

			return categoryList;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}
	
	/**
	 * Get the list of Problem form LKUP_DEPT_PROBLEM table
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getProblemList() throws Exception {
		logger.debug("getProblemList()");
		List problemList = new ArrayList();

		try {
			sql = "select * from LKUP_DEPT_PROBLEM order by PROBLEM";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				IncidentProblemForm problem = new IncidentProblemForm(rs.getString("PROBLEM"), rs.getInt("DEPT_ID"));
				problemList.add(problem);
			}

			rs.close();

			return problemList;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}
	
	public static int getLsoId(int id,String level) throws Exception {
		logger.debug("getLsoId()");
		try {
			String sql="";
			
			if(level.equals("A")){
			sql=" select ls.land_id"
					+" from activity a,sub_project sp ,project p,OCCUPANCY o,STRUCTURE_OCCUPANT so,LAND_STRUCTURE ls"
					+" where a.act_id="+id+""
					+" and a.sproj_id=sp.sproj_id"
					+" and p.proj_id=sp.proj_id"
					+" and o.occupancy_id=p.lso_id"
					+" and so.occupancy_id=o.occupancy_id"
					+" and ls.structure_id=so.structure_id";
			}
			else if(level.equals("S"))
			{
				sql="Select land_id from LAND_STRUCTURE where structure_id="+id+"";
				
			}
			logger.debug("sql : "+sql);
			RowSet rs = new Wrapper().select(sql);
			int landid = -1;
			while (rs.next()) {

				landid = rs.getInt("land_id");
				logger.debug("land_id" + landid);
			}

			rs.close();

			return landid;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}

	}
	
	
	/**
	 * Get the list of activity sub types for an activity type.
	 * 
	 * @param activityType
	 * @return
	 */
	public static String getActivitySubTypesOptions(String activityType) throws Exception {
		logger.debug("getActivitySubTypesString(" + activityType + ")");

		StringBuilder sb = new StringBuilder();
		String sql = "select * from lkup_act_subtype where act_type='" + activityType + "' order by act_subtype";
		logger.debug(sql);

		
		try {
			RowSet rs = new Wrapper().select(sql);
			sb.append(" <option value=\"\" > Please Select </option> "); 
			while (rs.next()) {
				sb.append(" <option value=\"").append(rs.getInt("act_subtype_id")).append("\" > " ).append(rs.getString("act_subtype")).append(" </option>");
			}

			if(rs != null) rs.close();

			return sb.toString();
		} catch (SQLException e) {
			logger.error("SQL Error in getActivitySubType " + e.getMessage());
			throw e;
		} catch (Exception e) {
			logger.error("Error in getActivitySubType " + e.getMessage());
			throw e;
		}
	}
	
	
	public static String getActivityNbrForActId(int actId) throws AgentException {
		ActivityType actType = null;  

		String sql = "select * from ACTIVITY where ACT_ID in (" + actId + ")";

		RowSet rs = null;
		String actNbr = "";
		try {
			rs = new Wrapper().select(sql);

			if (rs != null && rs.next()) {

				actNbr = rs.getString("ACT_NBR");

			}
			/*
			 * if(rs!=null) { rs.close(); }
			 */
		} catch (Exception e) {
			logger.error("", e);
			throw new AgentException("", e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException Ignore) {
					logger.warn("Unable to colse Rowset");
				}
			}
		}

		return actNbr;
	}
	public List<DisplayItem> getStreetNames() {
		List<DisplayItem> list = new ArrayList<DisplayItem>();
		Wrapper db = new Wrapper();
		sql = "select *  from street_list where GS_ONLINE='Y' and street_id > 0 order by str_name";	
		logger.debug(sql);
		RowSet rs = null;
		try {
			DisplayItem di = null;
			rs = db.select(sql);
			while(rs.next()) {				
				String prefixDirection = rs.getString("pre_dir") != null ? rs.getString("pre_dir") : "";
				String streetName = rs.getString("str_name") != null ? rs.getString("str_name") : "";
				String streetType = rs.getString("str_type") != null ? rs.getString("str_type") : "";
				String fullStreetName = prefixDirection + " " + streetName + " " + streetType;

				if(fullStreetName != null && fullStreetName.trim().length() > 0) {
					di = new DisplayItem();
					di.setFieldOne(fullStreetName);
					list.add(di);
				}				
			}
		} catch(Exception exp) {
			logger.debug("exception while getteing street information");
		}finally{
			try{
				if(rs != null){
				    rs.close();
				}
			}catch(Exception e){
				
			}
		}

		
		return list;
	}

	public List<DisplayItem> getStreetUnitInfo(String streetUnit) {
		List<DisplayItem> list = new ArrayList<DisplayItem>();
		Wrapper db = new Wrapper();
		sql = "select DISTINCT "+streetUnit+" from street_list order by "+streetUnit;	
		logger.debug(sql);
		RowSet rs = null;
		try {
			DisplayItem di = null;
			rs = db.select(sql);
			while(rs.next()) {				
				sql = rs.getString(streetUnit);
				if(sql != null && sql.trim().length() > 0) {
					di = new DisplayItem();
					di.setFieldOne(sql);
					list.add(di);
				}				
			}
		} catch(Exception exp) {
			logger.debug("exception while getteing street information");
		}finally{
			try{
				if(rs != null){
				    rs.close();
				}
			}catch(Exception e){
				
			}
		}

		
		return list;
	}
	public List<DisplayItem> getStreetNum(String streetName) {
		List<DisplayItem> list = new ArrayList<DisplayItem>();
		Wrapper db = new Wrapper();
		sql = "select distinct STR_NO from LSO_ADDRESS where ACTIVE='Y' AND STREET_ID=(select STREET_ID from v_street_list where trim(street_name)='"+streetName.toUpperCase()+"') ORDER BY STR_NO";	
		logger.debug(sql);
		RowSet rs = null;
		try {
			DisplayItem di = null;
			rs = db.select(sql);
			while(rs.next()) {				
				sql = rs.getString("STR_NO");
				if(sql != null && sql.trim().length() > 0) {
					di = new DisplayItem();
					di.setFieldOne(sql);
					list.add(di);
				}				
			}
		} catch(Exception exp) {
			logger.debug("exception while getteing street information");
		}finally{
			try{
				if(rs != null){
				    rs.close();
				}
			}catch(Exception e){
				
			}
		}

		
		return list;
	}
	public List<DisplayItem> getUnitLst(String streetName,String streetNo) {
		List<DisplayItem> list = new ArrayList<DisplayItem>();
		Wrapper db = new Wrapper();
		sql = "SELECT DISTINCT UNIT,LSO_TYPE from  LSO_ADDRESS where ACTIVE='Y' and STR_NO="+Integer.parseInt(streetNo)+" AND STREET_ID=(select STREET_ID from v_street_list where trim(street_name)='"+streetName.toUpperCase().trim()+"') order by UNIT";	
		logger.debug("sql... "+sql);
		String lsoType ="";
		RowSet rs = null;
		try {
			DisplayItem di = null;
			rs = db.select(sql);
			int i=1;
			while(rs.next()) {		
				di = new DisplayItem();
				sql = rs.getString("UNIT");
				lsoType = rs.getString("LSO_TYPE");
				
				if(sql != null && sql.trim().length() > 0) {
					di.setFieldOne(sql);
					list.add(di);
				}
			}
		} catch(Exception exp) {
			logger.debug("exception while getteing street information");
		}finally{
			try{
				if(rs != null){
				    rs.close();
				}
			}catch(Exception e){
				
			}
		}

		
		return list;
	}
	
	public String getLsoIdFromAddress(String strNum,String strName,String unit, String streetMod) throws Exception {
		String sql = "select DISTINCT LSO_ID,LSO_TYPE,UNIT from LSO_ADDRESS where ACTIVE='Y' and STR_NO="+strNum+" and STREET_ID in (select street_id from v_street_list where  trim(street_name)='"+strName.trim().toUpperCase()+"') AND lso_type in ('L','O')";
		if(unit!=null && !unit.equals("") ){
			   sql =sql + "  and unit = '"+unit+"'";
		}
		if(streetMod!=null && !streetMod.equals("") ){
			   sql =sql + "  and str_mod = '"+streetMod+"'";
		}
		sql = sql + " ORDER BY LSO_TYPE DESC ";
        logger.debug(sql);
        ResultSet rs=null;
        String lsoId= "";
        try {
        	int i=0;
        	
            rs = new Wrapper().select(sql);
            while(rs!=null && rs.next()){
            	String lsoType = rs.getString("LSO_TYPE");
            	String unitType = rs.getString("UNIT");
                           	 
            	if(rs.getString("LSO_TYPE")!=null && rs.getString("LSO_TYPE").equalsIgnoreCase("O")){
            			lsoId=rs.getString("LSO_ID");
            			break;
            	}
            	
            	if(rs.getString("LSO_TYPE")!=null && rs.getString("LSO_TYPE").equalsIgnoreCase("L")){
        			lsoId=rs.getString("LSO_ID");
            	}          
            }
            logger.debug(" lsoId : "+lsoId);
            if(lsoId.equalsIgnoreCase("")){
            	lsoId = getLsoIdFromAddressPrimarry(strNum, strName, unit);
            }
        } catch (Exception e) {
            logger.error(""+ e+e.getMessage());
           e.printStackTrace();
        }finally{
			try{
				if(rs != null) rs.close();
			}catch(Exception e){}
		}
        return lsoId;
    }


	public String checkLSOId(String lsoId) throws Exception {
		
		String sql = "SELECT VA.LSO_ID, LO.DESCRIPTION AS USE_DESC, VA.ADDRESS, VA.CITYSTATEZIP, va.LSO_TYPE  FROM LAND_USAGE LU JOIN LSO_USE LO ON LU.LSO_USE_ID=LO.LSO_USE_ID JOIN V_ADDRESS_LIST VA ON LU.LAND_ID=VA.LSO_ID WHERE LU.LSO_USE_ID IN(2,3,5) AND VA.LSO_ID="+lsoId+" AND VA.ACTIVE='Y' ORDER BY LO.DESCRIPTION";
		logger.debug(sql);
        ResultSet rs=null;
        String useDesc="";
        try {
            rs = new Wrapper().select(sql);
            if(rs!=null && rs.next()){
            useDesc=rs.getString("USE_DESC");
            }
            logger.debug(" useDesc : "+useDesc);
        } catch (Exception e) {
            logger.error(""+ e+e.getMessage());
           e.printStackTrace();
        }finally{
			try{
				if(rs != null){
				    rs.close();
				}
			}catch(Exception e){
				
			}
		}

        return useDesc;

    }

public String getLsoIdFromAddressPrimarry(String strNum,String strName,String unit) throws Exception {
		
		String sql = "select LAND_ID from V_LSO_LAND where LSO_ID IN (select  ADDR_PRIMARY from LSO_ADDRESS where ACTIVE='Y'  AND LSO_TYPE='O' AND ADDR_PRIMARY IS NOT NULL  and STR_NO="+strNum+" and STREET_ID in (select street_id from v_street_list where  trim(street_name)='"+strName.trim()+"') AND lso_type in ('L','O')";
		
		if(unit!=null && !unit.equals("") ){
			   sql =sql + "  and unit = '"+unit+"'";
		}
		sql = sql + "  )";
        logger.debug(sql);
        ResultSet rs=null;
        String lsoId= "";
        try {
            rs = new Wrapper().select(sql);
            if(rs!=null && rs.next()){
            lsoId=rs.getString("LAND_ID");
            }
            logger.debug(" lsoId : "+lsoId);
        } catch (Exception e) {
            logger.error(""+ e+e.getMessage());
           e.printStackTrace();
        }finally{
			try{
				if(rs != null){
				    rs.close();
				}
			}catch(Exception e){
				
			}
		}

        return lsoId;

    }
public int checkForAddressHold(String lsoId) {
	logger.debug("get hard holds.....:"+lsoId);
	int holdId = 0;
	RowSet rs = null;
	RowSet rsin = null;
	try {			
		Wrapper db = new Wrapper();
		String sb = new String("select distinct vll.*,so.OCCUPANCY_ID from v_lso_land vll,STRUCTURE_OCCUPANT so where  so.STRUCTURE_ID=vll.STRUCTURE_ID and vll.lso_id="+lsoId+"");
		logger.debug("sql...:"+sb);
		rs = db.select(sb);
		if(rs.next()) {
			
			String levelId = "0";
			
			if(rs.getString("land_id")!=null){
				levelId = levelId + ","+rs.getString("land_id");
			}
			if(rs.getString("STRUCTURE_ID")!=null){
				levelId = levelId + ","+rs.getString("STRUCTURE_ID");
			}
			if(rs.getString("OCCUPANCY_ID")!=null){
				levelId = levelId + ","+rs.getString("OCCUPANCY_ID");
			}
			
			sql = "with  Q AS (SELECT HOLD_ID,max(UPDATE_DT) as UPDATE_DT FROM HOLDS WHERE LEVEL_ID IN("+levelId+") AND HOLD_TYPE='H'  GROUP BY HOLD_ID)SELECT H.* FROM Q JOIN HOLDS  H ON Q.HOLD_ID=H.HOLD_ID AND Q.UPDATE_DT=H.UPDATE_DT WHERE H.STAT='A'";
			logger.debug("sql for hold " +sql);
			rsin = db.select(sql);
			if(rsin.next()) {
				
				holdId = rsin.getInt("hold_id");
				
			}
			
		}
	} catch(Exception exp) {
		logger.error("error in getVacation method.."+exp);
	}finally {
		try {
			if (rs != null) {
				rs.close();
			}
			if (rsin != null) {
				rsin.close();
			}
		} catch (Exception e1) {
			logger.error("Exception occured while closing rs " + e1.getMessage());
		}
	}
	return holdId;
}
public String searchAddress(String sNum, String sName, String unit) {
	unit= StringUtils.nullReplaceWithEmpty(unit);
	String lsoid = null;
	//logger.debug(preDir.toUpperCase());
	//logger.debug(unit.toUpperCase());
	sql = "select la.lso_id,la.LSO_TYPE,la.UNIT from lso_address la";
			
	/*if(!unit.equalsIgnoreCase("")){
		sql = sql +"  join occupancy o on la.lso_id=o.occupancy_id " ;
	}*/
	
     sql = sql + " where ACTIVE='Y' and la.street_id in (select street_id from v_street_list where ";
			
			/*if(!preDir.toUpperCase().equalsIgnoreCase("")){
				sql = sql +"='"+preDir.toUpperCase()+"'";
			}else{
				sql = sql+" IS NULL";
			}*/
			
	sql = sql + " trim(street_name)='"+sName.toUpperCase().trim()+"'";
			//+" and str_type";
	/*
	if(unit.toUpperCase()!=""){
		sql = sql +"='"+unit.toUpperCase()+"'";
	}else{
		sql = sql+" IS NULL";
	}*/
			
	sql = sql	+")"  +" and la.str_no="+sNum;
	if(unit!=null && !unit.equalsIgnoreCase("")){
		sql += (" and lower(la.UNIT) = (lower(" + (StringUtils.checkString(unit)) + "))");
	}
	sql += (" and lso_type in ('L','O') ORDER BY LSO_TYPE ASC ");
	
	RowSet rs = null;
	try {
		logger.debug(sql);
		int i=0;
		rs = new Wrapper().select(sql);
		while(rs.next()) {
			if(i <= 0) {
				lsoid=rs.getString("LSO_ID");
			}
//			String lsoType = rs.getString("LSO_TYPE");
//        	String unitType = rs.getString("UNIT");
//			if(lsoType!=null && lsoType.equalsIgnoreCase("O")){
//				if(unit!=null && !unit.equals("")){
//        		if(unitType!=null && !unitType.equalsIgnoreCase("") ){
//        			lsoid=rs.getString("LSO_ID");
//        			break;
//        		}
//				}else{
//					if(unit!=null && !unit.equals("") && unitType!=null && !unitType.equalsIgnoreCase("") ){
//            			lsoid=rs.getString("LSO_ID");
//            			break;
//            		}else if(unitType ==null){
//            			lsoid=rs.getString("LSO_ID");
//            			break;
//            		}
//				}
//        	}else if(lsoType!=null && lsoType.equalsIgnoreCase("L")){
//        		lsoid=rs.getString("LSO_ID");
//        		break;
//        	}
			i++;
		}
		 if(lsoid.equalsIgnoreCase("")){
			 lsoid = getLsoIdFromAddressPrimarry(sNum, sName, unit);
            }
		logger.debug(" lsoid : "+lsoid);
	} catch(Exception exp) {
		logger.error("error search address..:"+exp);			
	}finally{
		try{
			if(rs != null){
			    rs.close();
			}
		}catch(Exception e){
			
		}
	}

	return lsoid;
}
public  String getAddrIdfromLsoId(String lsoid) throws Exception {
	logger.debug("getAddrIdfromLsoId(" + lsoid + ")");
	String addrId = null;
    String sql="";
    RowSet rs = null;
	try {
		sql = "select ADDR_ID from lso_address where lso_id= " + lsoid;
		logger.debug(sql);

		rs = new Wrapper().select(sql);

		if (rs.next()) {
			addrId = rs.getString("ADDR_ID");
		}

		rs.close();

		return addrId;
	} catch (Exception e) {
		logger.error("Exception in getAddrId of LookupAgent - " + e.getMessage());
		throw e;
	}finally{
		try{
			if(rs != null){
			    rs.close();
			}
		}catch(Exception e){
			
		}
	}

}
public String getactivitydec(String act) throws Exception{

	logger.debug("get Descrition called...");
	String sql = null;
	String id=null;
													
		sql = "select description from lkup_act_type where online_yn ='Y' and type='"+act+"'";
		
		logger.debug("sql..."+sql);
		Wrapper db = new Wrapper();
		RowSet rs = null;
		try {
		 	rs = db.select(sql);
		 	if(rs.next()) {
		 		
		 		id=rs.getString("description");
		 	}
		} catch(Exception exp) {
			exp.printStackTrace();
			logger.error("error while retreiving activity:"+exp);
		}finally{
			try{
				if(rs != null){
				    rs.close();
				}
			}catch(Exception e){
				
			}
		}

		logger.debug("exit from get desc method");
		return id;
}	
//Getting deptId from actId
		public static int getDepartmentId(int actId) throws Exception {
		
			String sql = "select lat.dept_id from activity a join lkup_act_type lat on a.act_type=lat.type where a.act_id="+actId;
			logger.debug(sql);
			int deptId=0;
			RowSet rs = new Wrapper().select(sql);
			if(rs.next()) {
				deptId = rs.getInt("dept_Id");
			}
			if(rs != null){
				rs.close();
			}
			logger.debug(actId + " dept code is:"+deptId);
			return deptId;
		}

		/*public int addAttachment(OnlinePermitForm addForm, String aContentType, InputStream aData, String attachmentPath,String displayFileName) {
		logger.info("addAttachmentNew(Attachment, " + aContentType + ")");
		int attachmentId = -1;

		try {

			String fileName = addForm.getTheFile().getFileName();
			logger.debug("The obtained filename of the attached file is " + fileName);

			

			logger.debug("filename after smart-naming " + fileName);

			String location = attachmentPath +fileName;

			boolean newFile = saveStringToFile(aData, location, attachmentPath);
			logger.debug("File created in server " + newFile);

			Wrapper db = new Wrapper();
			attachmentId = db.getNextId("ATTACHMENT_ID");
			
			// parse the filename and see if the file has been smart named already.
			if (isFileSmartNamed(Integer.parseInt(addForm.getActId()), "A", fileName)) {
				logger.debug("The file has a smart name already, so using the same name");
			} else {
				logger.debug("The file has not been smart-named, so renaming the file to smart-name");
				fileName = renameTheFile(Integer.parseInt(addForm.getActId()),"A", fileName);
			}
			logger.debug("Got the Attchment ID" + attachmentId);
			String sql = "INSERT INTO ATTACHMENTS(ATTACH_ID,LEVEL_ID,ATTACH_LEVEL,FILE_NAME,DESCRIPTION,LOCATION,ATTACH_SIZE,CREATED_BY,CREATED,DELETED,STATUS,KEYWORD1,KEYWORD2,KEYWORD3,KEYWORD4) VALUES (" + attachmentId + "," +addForm.getActId()+ "," + StringUtils.checkString("A") + "," + StringUtils.checkString(fileName) + "," + StringUtils.checkString(addForm.getDescription()) + "," + StringUtils.checkString(location) + "," + addForm.getTheFile().getFileSize()+ "," + addForm.getUserId() + "," + "CURRENT_TIMESTAMP" + "," + "'N','A'" + ",'AAA','AAA','AAA','AAA')";
			logger.info(sql);
			db.update(sql);
		} catch (Exception e) {
			logger.error("Error in Add Attachment");
			attachmentId = -1;
		}

		return attachmentId;
	}
		private boolean saveStringToFile(InputStream stream, String location, String path) throws Exception {
			logger.info("saveStringToFile( file, " + location + ", " + path + ")");

			boolean didConvert = false;
			OutputStream bos = null;

			try {

				File dir = new File(path);
				logger.debug("file created");
				if (!dir.exists()) {
					logger.debug("directory does not exist, creating...");
					dir.mkdirs();
				}

				logger.debug("trying to create the input stream");
				logger.debug("obtained the input stream from the file");

				bos = new FileOutputStream(location);
				logger.debug("created new output stream for the file");

				int bytesRead = 0;
				final byte[] buffer = new byte[1024];

				while ((bytesRead = stream.read(buffer)) != -1) {
					bos.write(buffer, 0, bytesRead);
				}

				logger.debug("completed writing the file");
				didConvert = true;
				return didConvert;
			} catch (Exception e) {
				logger.error("problem while saving file " + e.getMessage());
				didConvert = false;
				throw new Exception("Problem while saving file " + e.getMessage());
			} finally {
				if (bos != null) {
					bos.close();
				}
			}
		}
*/		public List<DisplayItem> getAttachments(String id) throws Exception {
			List<DisplayItem> list = new ArrayList<DisplayItem>();
			if(id==null)
				id="0";
			String sql = "select * from ATTACHMENTS where Deleted='N' AND LEVEL_ID="+id;
			
					logger.debug("The sql is :" + sql);
			Wrapper db = new Wrapper();
			RowSet rs = null;
			try {
				rs = db.select(sql);

				while (rs.next()) {
					DisplayItem di = new DisplayItem();
					di.setFieldOne(rs.getString("LEVEL_ID"));
					//if(rs.getString("FILE_NAME") != null){
					di.setFieldTwo(rs.getString("FILE_NAME"));
					//}
					di.setFieldThree(rs.getString("DESCRIPTION"));
					//di.setFieldFour(rs.getString("DISPLAY_FILE_NAME"));
					di.setFieldFive(rs.getString("ATTACH_ID"));
					list.add(di);
				}
			} catch (Exception e) {
				logger.error(e.getMessage());
				throw new Exception("Exception occured while getting attachment results :" + e.getMessage());
			}finally{
				try{
					if(rs != null){
					    rs.close();
					}
				}catch(Exception e){
					
				}
			}


			logger.info("Exiting getAttacmentList (" + list.size() + ")");
			return list;
		}
		
		public List<DisplayItem> getEgovTempAttachments(String id) throws Exception {
			List<DisplayItem> list = new ArrayList<DisplayItem>();
			if(id==null)
				id="0";
			String sql = "select * from CAP_TEMP_ATTACHMENT where CAP_TEMP_ID="+id;
			
					logger.debug("The sql is :" + sql);
			Wrapper db = new Wrapper();
			RowSet rs = null;
			try {
				rs = db.select(sql);

				while (rs.next()) {
					DisplayItem di = new DisplayItem();
					di.setFieldOne(rs.getString("CAP_TEMP_ID"));
					//if(rs.getString("FILE_NAME") != null){
					di.setFieldTwo(rs.getString("FILE_NAME"));
					//}
					di.setFieldThree(rs.getString("DESCRIPTION"));
					list.add(di);
				}
			} catch (Exception e) {
				logger.error(e.getMessage());
				throw new Exception("Exception occured while getting owner results :" + e.getMessage());
			}finally{
				try{
					if(rs != null){
					    rs.close();
					}
				}catch(Exception e){
					
				}
			}


			logger.info("Exiting searchOwner (" + list.size() + ")");
			return list;
		}
		
		public void deleteAttachment(String attachmentId){
			 String sql = "update attachments set Deleted='Y' where ATTACH_ID = "+attachmentId;
		        logger.info(sql);
		     String sql1 = "delete from CAP_TEMP_ATTACHMENT where ATTACH_ID="+attachmentId;
		        try {
			        Wrapper db = new Wrapper(); 
			        db.insert(sql);	
			        db.insert(sql1);
		        } catch (Exception e) {
		            logger.error(""+ e+e.getMessage());
		        }
		}
		public String getLevelIdFromAttachmentId(String attachmentId) throws Exception {
		       String sql = "select LEVEL_ID from ATTACHMENTS where ATTACH_ID='"+attachmentId+"'";
		       logger.debug(sql);
		       ResultSet rs=null;
		       String levelId= "";
		       try {
		           rs = new Wrapper().select(sql);
		           if(rs!=null && rs.next()){
		           	levelId=rs.getString("LEVEL_ID");
		           }
		       } catch (Exception e) {
		           logger.error(""+ e+e.getMessage());
		          e.printStackTrace();
		       }finally{
					try{
						if(rs != null){
						    rs.close();
						}
					}catch(Exception e){
						
					}
				}

		       return levelId;

		   }
			
		/*public String renameTheFile(int levelId, String levelType, String fileName) {
			logger.debug("renameTheFile()::"+levelId+"::"+levelType+"::"+fileName);
			if (levelType == null) {
				levelType = "";
			}

			if (fileName == null) {
				fileName = "";
			}

			String newFileName = "";
			String sql = "";

			if (levelType.equals("L") || levelType.equals("S") || levelType.equals("O")) {
				sql = "select dl_address as name from v_address_list where lso_id=" + levelId;
			} else if (levelType.equals("P")) {
				sql = "select project_nbr as name from project where proj_id=" + levelId;
			} else if (levelType.equals("Q")) {
				sql = "select sproj_nbr as name from sub_project where sproj_id=" + levelId;
			} else if (levelType.equals("A")) {
				sql = "select act_nbr as name from activity where act_id=" + levelId;
			} 
			RowSet rs = null;
			try {			
				//if(!sql.equalsIgnoreCase("")){
				// rs = new Wrapper().select(sql);

				
					newFileName = ""+levelId;
				//}
				// char[] abc = newFileName.toCharArray();
				// String ella="";
				// for(int i=0;i<abc.length;i++){

				// String z = ""+abc[i];
				// logger.debug(""+(int)abc[i]);

				// z= z.intern();
				// logger.debug("ellap"+z);
				// ella = ella +elms.util.Operator.escape(z);
				// logger.debug("ella"+ella);
				// }
				// logger.debug("ella"+ella);
				// newFileName = StringUtils.replaceString(newFileName,"  ","_");
				logger.debug("newFileName::"+newFileName);
				
				if(newFileName!=null){
					
					newFileName = newFileName.trim().replace(' ', '_');
					// newFileName = StringUtils.replaceString(ella,"\n","_");
					// newFileName = StringUtils.replaceString(newFileName,"\t","_");

					// newFileName = newFileName. replace("\r\n", "_");
					// logger.debug("before"+newFileName);
					newFileName = Operator.replace(newFileName, "\r\n", "_");

					newFileName = Operator.replace(newFileName, "/", "_");

					// logger.debug("before"+newFileName);
					//newFileName = levelType + "-" + newFileName;
					// String test = elms.util.Operator.toText(newFileName);
					// logger.debug("The new file name is "+test);
					logger.debug("The new file name is " + newFileName);
					//newFileName = newFileName + "-" ;
				}
				else{
					newFileName = "" ;
				}
				if(fileName!=null){
					fileName=fileName.replace(" ", "+");
				}

				newFileName = "A-"+newFileName +"-" +fileName;
				logger.debug("newFileName::::::::"+newFileName);
			} catch (Exception e) {
				e.printStackTrace();
			}finally{
				try{
					if(rs != null){
					    rs.close();
					}
				}catch(Exception e){
					
				}
			}


			return newFileName;
		} 

		*//**
		 * Rename the filename to the smart-name of the file if it is not named smart already
		 * 
		 * @param levelId
		 * @param levelType
		 * @param fileName
		 * @return
		 *//*
		public boolean isFileSmartNamed(int levelId, String levelType, String fileName) {
			logger.debug("isFileSmartNamed(" + levelId + "," + levelType + "," + fileName + ")");

			try {
				// find the level to where the document belongs to.
				String firstTwoChars = fileName.substring(0, 2);

				StringTokenizer st = new StringTokenizer(fileName, "-");
				int numberOfTokens = st.countTokens();
				logger.debug("The number of tokens in this filename is " + numberOfTokens);

				// first check if there are expected number of tokens.
				if (numberOfTokens < 3) {
					return false;
				}

				// check if the level and level type are valid.
				String fileLevelType = st.nextToken();

				if (!(fileLevelType.equals(levelType))) {
					logger.debug("The file level type is not equal to the level type, returning false");

					return false;
				}

				char fileLevel = ' ';

				if (firstTwoChars.equalsIgnoreCase("L-")) {
					logger.debug("Document Level : Land");
					fileLevel = 'L';
				} else if (firstTwoChars.equalsIgnoreCase("S-")) {
					logger.debug("Document Level : Structure");
					fileLevel = 'S';
				} else if (firstTwoChars.equalsIgnoreCase("O-")) {
					logger.debug("Document Level : Occupancy");
					fileLevel = 'O';
				} else if (firstTwoChars.equalsIgnoreCase("P-")) {
					logger.debug("Document Level : Project");
					fileLevel = 'P';
				} else if (firstTwoChars.equalsIgnoreCase("SP")) {
					logger.debug("Document Level : Sub-Project");
					fileLevel = 'Q';
				} else if (firstTwoChars.equalsIgnoreCase("A-")) {
					logger.debug("Document Level : Activity");
					fileLevel = 'A';
				}

				logger.debug("The obtained file level is " + fileLevel);

				switch (fileLevel) {
				case 'L':
				case 'S':
				case 'O':
					logger.debug("Starting parsing for LSO document ");

					String address = st.nextToken();
					address = address.trim().replace('_', ' ');
					logger.debug("The lso address is " + address);

					int fileLevelId = getLsoId(fileLevelType, address);
					logger.debug("The file level obtained is " + fileLevelId);

					if (fileLevelId != levelId) {
						logger.debug("The lso id is not equal to the level id, returning false.");

						return false;
					}

					logger.debug("Ending parsing for LSO document");

					break;

				case 'P':
				case 'Q':
				case 'A':
					logger.debug("Starting parsing for PSA document ");

					String psaNumber = st.nextToken();
					logger.debug("The psa number obtained is " + psaNumber);

					int psaId = getPsaId(fileLevelType, psaNumber);
					logger.debug("The psa id obtained is " + psaId);

					if (psaId != levelId) {
						logger.debug("The pas id is not equal to the level id, returning false.");

						return false;
					}

					logger.debug("Ending parsing for PSA document");

					break;
				}

				return true;
			} catch (Exception e) {
				logger.error("Exception occured while parsing filename for smart-name-check " + e.getMessage());

				return false;
			}
		}
		
*/	/*	*//**
		 * gets the LSO id based on the data provided.
		 * 
		 * @return
		 * @throws Exception
		 *//*
		private int getLsoId(String level, String address) throws Exception {
			RowSet result = null;
			int lsoId = -1;
			String sql = "";

			try {
				sql = "select lso_id from v_address_list where lso_type=" + StringUtils.checkString(level) + " and dl_address =" + StringUtils.checkString(address);

				logger.debug(sql);
				result = new Wrapper().select(sql);

				if (result.next()) {
					lsoId = result.getInt(1);
				}

				result.close();

				if (lsoId == -1) {
					logger.debug("ERROR: while getting lso id from  database, it is -1 ");
					throw new Exception("Error: unable to get lso id for the address " + address);
				} else {
					return lsoId;
				}
			} catch (Exception e) {
				logger.error("ERROR: while getting lso id from  database " + e.getMessage());
				throw new Exception(e.getMessage());
			}
		}

		*//**
		 * gets the PSA ID based on the data provided
		 * 
		 * @param psaType
		 * @param psaNumber
		 * @return
		 * @throws Exception
		 *//*
		public int getPsaId(String psaType, String psaNumber) throws Exception {
			RowSet result = null;
			int id = -1;
			String sql = "";

			try {

				if (psaType.equals("P")) {
					sql = "select proj_id from project where project_nbr=" + StringUtils.checkString(psaNumber);
				}

				if (psaType.equals("SP")) {
					sql = "select sproj_id from sub_project where sproj_nbr=" + StringUtils.checkString(psaNumber);
				}

				if (psaType.equals("A")) {
					sql = "select act_id from activity where act_nbr=" + StringUtils.checkString(psaNumber);
				}

				logger.debug(sql);
				result = new Wrapper().select(sql);

				if (result.next()) {
					id = result.getInt(1);
				}

				result.close();

				if (id == -1) {
					logger.debug("ERROR: while getting  PSA id from  database, it is -1 ");
					throw new Exception("Error: unable to get id for the psaNumber " + psaNumber + " and type " + psaType);
				} else {
					return id;
				}
			} catch (Exception e) {
				logger.error("ERROR: while getting psa id from  database");
				throw new Exception(e.getMessage());
			}
		}

*/		public List<HolidayEditorForm> getHolidaysList() {
			
			String sql = "SELECT VALUE FROM LKUP_SYSTEM WHERE NAME = '"+Constants.GS_SECONDCHANCE_WEEKEND_STARTDATE+"'";
			String sql1 = "SELECT VALUE FROM LKUP_SYSTEM WHERE NAME = '"+Constants.GS_SECONDCHANCE_WEEKEND_ENDDATE+"'";
			
			List<HolidayEditorForm> holidayList= new ArrayList<HolidayEditorForm>();
			Wrapper wrapper = new Wrapper();
			RowSet rs = null,rs1 = null ;
			String issueDate = "",endDate = "";
			try {
				 rs = wrapper.select(sql);
				if (rs != null && rs.next()) {
					issueDate = rs.getString("VALUE");
				}
				rs1 = wrapper.select(sql1);
				if (rs1 != null && rs1.next()) {
						endDate = rs1.getString("VALUE");
				}
				logger.debug("issueDate: "+issueDate+" endDate :"+endDate);
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Calendar issue = StringUtils.dbDate2cal(issueDate);
				 List<Date> dates = new ArrayList<Date>();
				 
				 while (issue.getTime().before(sdf.parse(endDate)))
				    {
					 HolidayEditorForm holidayForm  = new HolidayEditorForm();
				        Date result = issue.getTime();
				        //dates.add(sdf.format(result));
				        holidayForm.setDate(sdf.format(result));
				        logger.debug("date"+sdf.format(result));
				        issue.add(Calendar.DATE, 1);
				        holidayList.add(holidayForm);
				    }
				 HolidayEditorForm holidayForm  = new HolidayEditorForm();
				 holidayForm.setDate(endDate);
				 holidayList.add(holidayForm);
			
			} catch (Exception e) {
				logger.error("", e);
			}finally {
				if (rs != null) {
					try {
						rs.close();
					} catch (SQLException Ignore) {
					}
				}
				if (rs1 != null) {
					try {
						rs1.close();
					} catch (SQLException Ignore) {
					}
				}
			}

			return holidayList;
		}

		public int getCancelPeriodInDays(){
			String sql = "SELECT VALUE FROM LKUP_SYSTEM WHERE NAME = '"+Constants.GS_PERMIT_CANCEL_PERIOD_IN_DAYS+"'";
			Wrapper wrapper = new Wrapper();
			RowSet rs = null ;
			int days=0;
			try {
				 rs = wrapper.select(sql);
				if (rs != null && rs.next()) {
					days= StringUtils.s2i(rs.getString("VALUE"));
				}
			} catch (Exception e) {
				logger.error("", e);
			}finally {
				if (rs != null) {
					try {
						rs.close();
					} catch (SQLException Ignore) {
					}
				}
			}
			return days;
		}

		/**
		 * Get CreatedBy For ActId for activity id
		 * 
		 * @param actId
		 * @return
		 * @throws Exception
		 */
		public static int getCreatedByForActId(String activityId) throws Exception {
			logger.debug("getActivityTypeForActId(" + activityId + ")");

			int createdBy = 0;

			try {
				sql = "SELECT CREATED_BY FROM ACTIVITY WHERE ACT_ID = " + activityId;
				logger.debug(sql);

				RowSet rs = new Wrapper().select(sql);

				if (rs.next()) {
					createdBy = rs.getInt("CREATED_BY");
				}

				rs.close();

				return createdBy;
			} catch (Exception e) {
				logger.error("Exception in getActivityType of LookupAgent - " + e.getMessage());
				throw e;
			}
		}
}

