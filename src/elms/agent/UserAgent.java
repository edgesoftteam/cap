package elms.agent;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import javax.sql.RowSet;
import org.apache.log4j.Logger;
import elms.security.*;
import elms.control.beans.online.OnlineRegisterFrom;
import elms.exception.AgentException;
import elms.exception.UserNotFoundException;
import elms.util.StringUtils;
import elms.util.db.Wrapper;


public class UserAgent {
	static Logger logger = Logger.getLogger(UserAgent.class.getName());
	protected int projectId;
	protected int subProjectId = -1;
	
	
	/**
	 * update password in egov user table
	 * @param username
	 * @param password
	 * @throws Exception
	 */
	public int updatePassword(String username, String oldPassword, String newPassword) throws AgentException {
		int flag = 0;
		try {
			logger.debug("enter into updatePassword method at userAgent class");
			Wrapper db = new Wrapper();
			StringBuffer sb = new StringBuffer();
			sb.append("update ext_user set EXT_PASSWORD = "+StringUtils.checkString(newPassword)+" "
					+" where ext_username="+StringUtils.checkString(username));
			logger.debug("update query in updatePassword method...:"+sb.toString());
			flag = db.update(sb.toString());				
		} catch(Exception e) {
			logger.error(" Exception in userAgent -- updatePassword() method " + e);
			throw new AgentException("",e);
		}	
		logger.debug("exit from updatePassword method at userAgent class");
		return flag;
	}
	
	/**
	 * update password when user forgot the password
	 * @param username
	 * @param newPassword
	 * @return db update rows count
	 * @throws Exception
	 */
	public int updatePassword(String username, String newPassword) throws AgentException {
		int flag = 0;
		try {
			logger.debug("enter into updatePassword method at userAgent class");
			Wrapper db = new Wrapper();
			StringBuffer sb = new StringBuffer();
			sb.append("update egov_users set PASSWORD = "+StringUtils.checkString(newPassword)
					+" where username="+StringUtils.checkString(username));
			logger.debug("update query in updatePassword method...:"+sb.toString());
			flag = db.update(sb.toString());				
		} catch(Exception e) {
			logger.error(" Exception in userAgent -- updatePassword() method " + e);
			throw new AgentException("",e);
		}	
		logger.debug("exit from updatePassword method at userAgent class");
		return flag;
	}
	


	/**
 * Get Online User
 * @param user
 * @param username
 * @return
 * @throws Exception
 */
public User getOnlineUser(User user, String username) throws Exception {
	logger.info("getOnlineDotUser(User , " + username + ")");

	try {

		Wrapper db = new Wrapper();
		String sql = "select * from ext_user where lower(ext_username) = lower('" + username + "') order by ext_password";
		logger.info(sql);
		RowSet userRs = db.select(sql);
		if (user == null) {
			user = new User();
		}
		if (userRs.next()) {
			
				user.setUserId(StringUtils.s2i(StringUtils.d2s(userRs.getDouble("EXT_USER_ID"))));
				user.setAccountNbr(userRs.getString("EXT_ACCT_NBR"));
				logger.debug(" userid  is set to  " + user.getUserId());
				user.setUsername(userRs.getString("EXT_USERNAME"));
				logger.debug("  Username  is set to  " + user.getUsername());
				user.setFirstName(StringUtils.nullReplaceWithEmpty(userRs.getString("FIRSTNAME")));
				logger.debug("  FirstName  is set to  " + user.getFirstName());
				user.setLastName(StringUtils.nullReplaceWithEmpty(userRs.getString("LASTNAME")));
				logger.debug(" LastName  is set to  " + user.getLastName());
				user.setMiddleInitial("");
				logger.debug(" MiddleInitial  is set to  " + user.getMiddleInitial());
				user.setTitle(userRs.getString("EXT_USER_TYPE"));
				user.setPassword(userRs.getString("ext_password"));
			
			user.setAccountNbr(userRs.getString("EXT_ACCT_NBR"));
			user.setIsObc("Y");
			if (user.getIsObc().equals("Y") || (user.getIsDot().equals("Y") && userRs.getString("ACTIVEDOT").equals("Y"))) {
				user.setActive("Y");
			} else {
				user.setActive("N");
			}
		}
		return user;
	} catch (Exception e) {
		logger.warn(" Exception in userAgent -- getUser() method " + e.getMessage());
		throw new UserNotFoundException("user not found " + e.getMessage());
	}
}


/**
 * Get Online User
 * @param user
 * @param username
 * @return
 * @throws Exception
 */
public User getOnlineUser(User user, int userId) throws Exception {
	logger.info("getOnlineDotUser(User , " + userId + ")");

	try {

		Wrapper db = new Wrapper();
		String sql = "select * from ext_user where ext_user_id = "+userId+" order by ext_password";
		logger.info(sql);
		RowSet userRs = db.select(sql);
		if (user == null) {
			user = new User();
		}
		if (userRs.next()) {
			
				user.setUserId(StringUtils.s2i(StringUtils.d2s(userRs.getDouble("EXT_USER_ID"))));
				user.setAccountNbr(userRs.getString("EXT_ACCT_NBR"));
				logger.debug(" userid  is set to  " + user.getUserId());
				user.setUsername(userRs.getString("EXT_USERNAME"));
				logger.debug("  Username  is set to  " + user.getUsername());
				user.setFirstName(StringUtils.nullReplaceWithEmpty(userRs.getString("FIRSTNAME")));
				logger.debug("  FirstName  is set to  " + user.getFirstName());
				user.setLastName(StringUtils.nullReplaceWithEmpty(userRs.getString("LASTNAME")));
				logger.debug(" LastName  is set to  " + user.getLastName());
				user.setMiddleInitial("");
				logger.debug(" MiddleInitial  is set to  " + user.getMiddleInitial());
				user.setTitle(userRs.getString("EXT_USER_TYPE"));
				user.setPassword(userRs.getString("ext_password"));
			
			user.setAccountNbr(userRs.getString("EXT_ACCT_NBR"));
			user.setIsObc("Y");
			if (user.getIsObc().equals("Y") || (user.getIsDot().equals("Y") && userRs.getString("ACTIVEDOT").equals("Y"))) {
				user.setActive("Y");
			} else {
				user.setActive("N");
			}
		}
		return user;
	} catch (Exception e) {
		logger.warn(" Exception in userAgent -- getUser() method " + e.getMessage());
		throw new UserNotFoundException("user not found " + e.getMessage());
	}
}


}
