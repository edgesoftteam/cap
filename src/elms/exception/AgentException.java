/**
 * 
 */
package elms.exception;

/**
 * @author Gaurav Garg
 * 
 */
public final class AgentException extends Exception {

	/**
	 * No argument constructor
	 */
	public AgentException() {
		super();
	}

	/**
	 * String argument constructor
	 * 
	 * @param message
	 */
	public AgentException(String message) {
		super(message);
	}

	public AgentException(String message, Throwable ex) {
		super(message, ex);
	}

	public AgentException(Throwable th) {
		super(th);
	}
}
