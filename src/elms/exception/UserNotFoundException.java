package elms.exception;

import org.apache.log4j.Logger;

/**
 * User not found excetion.
 * 
 */
public class UserNotFoundException extends Exception {

	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(UserNotFoundException.class.getName());

	public UserNotFoundException() {
	}

	public UserNotFoundException(String message) {
		super(message);
	}

}
