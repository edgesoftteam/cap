package elms.exception;

/**
 * @author abelaguly updated Jul 24, 2004 12:19:47 PM
 */
public class DuplicateInspectionRouteException extends Exception {

	/**
	 * default construtor
	 */
	public DuplicateInspectionRouteException() {
		super();
	}

	public DuplicateInspectionRouteException(String message) {
		super(message);
	}

}
