package elms.exception;

public class PaymentException extends Exception {

	/**
	 * Constructor for PaymentException
	 */
	public PaymentException() {
		super();
	}

	/**
	 * Constructor for PaymentException
	 */
	public PaymentException(String arg0) {
		super(arg0);
	}

}
