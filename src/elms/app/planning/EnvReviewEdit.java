package elms.app.planning;

import java.util.GregorianCalendar;

import elms.util.StringUtils;

/**
 * @author Shekhar
 */
public class EnvReviewEdit {
	private String update; // To track whether this record has been updated
	private String deleteFlag; // If checked this record has to be deleted

	private int index;
	private String envReviewId;
	private String submitDate;
	private String actionDate;
	private String determinationDate;
	private String reqDeterminationDate;
	private String envDeterminationId;
	private String envDetermination;
	private String envDeterminationActionId;
	private String envDeterminationAction;
	private String comments;
	private String added;
	private String uptSubProject;
	private String uptActivities;

	public EnvReviewEdit() {
		this.update = "";
		this.deleteFlag = "";
		this.envReviewId = "0";
		this.actionDate = StringUtils.cal2str(GregorianCalendar.getInstance()); // Set all the date's to today's date
		this.index = 1;
		this.added = "";
	}

	/**
	 * @return
	 */
	public String getActionDate() {
		return actionDate;
	}

	/**
	 * @return
	 */
	public String getDeleteFlag() {
		return deleteFlag;
	}

	/**
	 * @return
	 */
	public String getDeterminationDate() {
		return determinationDate;
	}

	/**
	 * @return
	 */
	public String getEnvDetermination() {
		return envDetermination;
	}

	/**
	 * @return
	 */
	public String getEnvDeterminationAction() {
		return envDeterminationAction;
	}

	/**
	 * @return
	 */
	public String getEnvDeterminationActionId() {
		return envDeterminationActionId;
	}

	/**
	 * @return
	 */
	public String getEnvDeterminationId() {
		return envDeterminationId;
	}

	/**
	 * @return
	 */
	public String getReqDeterminationDate() {
		return reqDeterminationDate;
	}

	/**
	 * @return
	 */
	public String getSubmitDate() {
		return submitDate;
	}

	/**
	 * @return
	 */
	public String getUpdate() {
		return update;
	}

	/**
	 * @return
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param string
	 */
	public void setActionDate(String string) {
		actionDate = string;
	}

	/**
	 * @param string
	 */
	public void setDeleteFlag(String string) {
		deleteFlag = string;
	}

	/**
	 * @param string
	 */
	public void setDeterminationDate(String string) {
		determinationDate = string;
	}

	/**
	 * @param string
	 */
	public void setEnvDetermination(String string) {
		envDetermination = string;
	}

	/**
	 * @param string
	 */
	public void setEnvDeterminationAction(String string) {
		envDeterminationAction = string;
	}

	/**
	 * @param string
	 */
	public void setEnvDeterminationActionId(String string) {
		envDeterminationActionId = string;
	}

	/**
	 * @param string
	 */
	public void setEnvDeterminationId(String string) {
		envDeterminationId = string;
	}

	/**
	 * @param string
	 */
	public void setReqDeterminationDate(String string) {
		reqDeterminationDate = string;
	}

	/**
	 * @param string
	 */
	public void setSubmitDate(String string) {
		submitDate = string;
	}

	/**
	 * @param string
	 */
	public void setUpdate(String string) {
		update = string;
	}

	/**
	 * @param string
	 */
	public void setComments(String string) {
		comments = string;
	}

	/**
	 * @return
	 */
	public String getEnvReviewId() {
		return envReviewId;
	}

	/**
	 * @param string
	 */
	public void setEnvReviewId(String string) {
		envReviewId = string;
	}

	/**
	 * @return
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @param i
	 */
	public void setIndex(int i) {
		index = i;
	}

	/**
	 * @return
	 */
	public String getAdded() {
		return added;
	}

	/**
	 * @param string
	 */
	public void setAdded(String string) {
		added = string;
	}

	/**
	 * @return
	 */
	public String getUptActivities() {
		return uptActivities;
	}

	/**
	 * @return
	 */
	public String getUptSubProject() {
		return uptSubProject;
	}

	/**
	 * @param string
	 */
	public void setUptActivities(String string) {
		uptActivities = string;
	}

	/**
	 * @param string
	 */
	public void setUptSubProject(String string) {
		uptSubProject = string;
	}

}