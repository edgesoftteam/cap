package elms.app.planning;

public class LinkActivityEdit {

	private String linked;
	private String activityId;
	private String activityNbr;
	private String description;
	private String activityType;
	private String status;

	/**
	 * @return
	 */
	public String getActivityId() {
		return activityId;
	}

	/**
	 * @return
	 */
	public String getActivityNbr() {
		return activityNbr;
	}

	/**
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return
	 */
	public String getLinked() {
		return linked;
	}

	/**
	 * @return
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param string
	 */
	public void setActivityId(String string) {
		activityId = string;
	}

	/**
	 * @param string
	 */
	public void setActivityNbr(String string) {
		activityNbr = string;
	}

	/**
	 * @param string
	 */
	public void setDescription(String string) {
		description = string;
	}

	/**
	 * @return Returns the activityType.
	 */
	public String getActivityType() {
		return activityType;
	}

	/**
	 * @param activityType
	 *            The activityType to set.
	 */
	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	/**
	 * @param string
	 */
	public void setLinked(String string) {
		linked = string;
	}

	/**
	 * @param string
	 */
	public void setStatus(String string) {
		status = string;
	}

}
