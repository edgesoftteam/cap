package elms.app.planning;

public class CalendarFeature {

	// private Calendar meetingDate;
	private String meetingDate;
	private String meetingType;
	private String actionType;
	private String comments;
	private int calendarFeatureId;
	private int subProjectId;

	public CalendarFeature() {
		// default constructor
	}

	public CalendarFeature(int calendarFeatureId, String meetingDate, String meetingType, String actionType, String comments) {
		this.calendarFeatureId = calendarFeatureId;
		this.meetingDate = meetingDate;
		this.meetingType = meetingType;
		this.actionType = actionType;
		this.comments = comments;
	}

	/**
	 * @return Returns the actionType.
	 */
	public String getActionType() {
		return actionType;
	}

	/**
	 * @param actionType
	 *            The actionType to set.
	 */
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	/**
	 * @return Returns the comments.
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments
	 *            The comments to set.
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return Returns the meetingType.
	 */
	public String getMeetingType() {
		return meetingType;
	}

	/**
	 * @param meetingType
	 *            The meetingType to set.
	 */
	public void setMeetingType(String meetingType) {
		this.meetingType = meetingType;
	}

	/**
	 * @return Returns the calendarFeatureId.
	 */
	public int getCalendarFeatureId() {
		return calendarFeatureId;
	}

	/**
	 * @param calendarFeatureId
	 *            The calendarFeatureId to set.
	 */
	public void setCalendarFeatureId(int calendarFeatureId) {
		this.calendarFeatureId = calendarFeatureId;
	}

	/**
	 * @return Returns the subProjectId.
	 */
	public int getSubProjectId() {
		return subProjectId;
	}

	/**
	 * @param subProjectId
	 *            The subProjectId to set.
	 */
	public void setSubProjectId(int subProjectId) {
		this.subProjectId = subProjectId;
	}

	/**
	 * @return Returns the meetingDate.
	 */
	public String getMeetingDate() {
		return meetingDate;
	}

	/**
	 * @param meetingDate
	 *            The meetingDate to set.
	 */
	public void setMeetingDate(String meetingDate) {
		this.meetingDate = meetingDate;
	}
}
