package elms.app.planning;

import java.util.GregorianCalendar;

import elms.util.StringUtils;

/**
 * @author Shekhar
 * 
 *         To change this generated comment edit the template variable "typecomment": Window>Preferences>Java>Templates. To enable and disable the creation of type comments go to Window>Preferences>Java>Code Generation.
 */
public class ResolutionEdit {

	private String delete;
	private String resolutionId;
	private String resolutionNbr;
	private String description;
	private String approvedDate;

	private String uptSubProject;
	private String uptActivities;
	private String added;

	public ResolutionEdit() {
		super();
		this.delete = "";
		this.resolutionId = "0";
		this.approvedDate = StringUtils.cal2str(GregorianCalendar.getInstance());
		this.added = "";
	}

	/**
	 * Returns the approvedDate.
	 * 
	 * @return String
	 */
	public String getApprovedDate() {
		return approvedDate;
	}

	/**
	 * Returns the delete.
	 * 
	 * @return String
	 */
	public String getDelete() {
		return delete;
	}

	/**
	 * Returns the description.
	 * 
	 * @return String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Returns the resolutionNbr.
	 * 
	 * @return String
	 */
	public String getResolutionNbr() {
		return resolutionNbr;
	}

	/**
	 * Sets the approvedDate.
	 * 
	 * @param approvedDate
	 *            The approvedDate to set
	 */
	public void setApprovedDate(String approvedDate) {
		this.approvedDate = approvedDate;
	}

	/**
	 * Sets the delete.
	 * 
	 * @param delete
	 *            The delete to set
	 */
	public void setDelete(String delete) {
		this.delete = delete;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Sets the resolutionNbr.
	 * 
	 * @param resolutionNbr
	 *            The resolutionNbr to set
	 */
	public void setResolutionNbr(String resolutionNbr) {
		this.resolutionNbr = resolutionNbr;
	}

	/**
	 * Returns the resolutionId.
	 * 
	 * @return String
	 */
	public String getResolutionId() {
		return resolutionId;
	}

	/**
	 * Sets the resolutionId.
	 * 
	 * @param resolutionId
	 *            The resolutionId to set
	 */
	public void setResolutionId(String resolutionId) {
		this.resolutionId = resolutionId;
	}

	/**
	 * @return
	 */
	public String getUptActivities() {
		return uptActivities;
	}

	/**
	 * @return
	 */
	public String getUptSubProject() {
		return uptSubProject;
	}

	/**
	 * @param string
	 */
	public void setUptActivities(String string) {
		uptActivities = string;
	}

	/**
	 * @param string
	 */
	public void setUptSubProject(String string) {
		uptSubProject = string;
	}

	/**
	 * @return
	 */
	public String getAdded() {
		return added;
	}

	/**
	 * @param string
	 */
	public void setAdded(String string) {
		added = string;
	}

}
