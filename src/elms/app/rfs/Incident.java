/**
 * 
 */
package elms.app.rfs;

/**
 * @author Gaurav Garg
 *
 */
public class Incident {
	
	
	private  int incidentid;
	private int rfsId;
	private String poi;
	private String priority;
	private String incidentstreetnumber;
	private String incidentstreetname;
	private String incidentunit;
	private String crossstreet1;
	private String crossstreet2;
	private String problem;
	private String incidentdepartment;
	private String description;
	private String memo;
	private String incidentcity="Glendale";
	private String incidentstate="CA";
	private String incidentzip="";
	private String incidentaddress;
	private int activityid;
	private String created;
	
	/**
	 * @return the incidentid
	 */
	public int getIncidentid() {
		return incidentid;
	}
	/**
	 * @param incidentid the incidentid to set
	 */
	public void setIncidentid(int incidentid) {
		this.incidentid = incidentid;
	}
	/**
	 * @return the poi
	 */
	public String getPoi() {
		return poi;
	}
	/**
	 * @param poi the poi to set
	 */
	public void setPoi(String poi) {
		this.poi = poi;
	}
	/**
	 * @return the priority
	 */
	public String getPriority() {
		return priority;
	}
	/**
	 * @param priority the priority to set
	 */
	public void setPriority(String priority) {
		this.priority = priority;
	}
	/**
	 * @return the incidentstreetnumber
	 */
	public String getIncidentstreetnumber() {
		return incidentstreetnumber;
	}
	/**
	 * @param incidentstreetnumber the incidentstreetnumber to set
	 */
	public void setIncidentstreetnumber(String incidentstreetnumber) {
		this.incidentstreetnumber = incidentstreetnumber;
	}
	/**
	 * @return the incidentstreetname
	 */
	public String getIncidentstreetname() {
		return incidentstreetname;
	}
	/**
	 * @param incidentstreetname the incidentstreetname to set
	 */
	public void setIncidentstreetname(String incidentstreetname) {
		this.incidentstreetname = incidentstreetname;
	}
	/**
	 * @return the incidentunit
	 */
	public String getIncidentunit() {
		return incidentunit;
	}
	/**
	 * @param incidentunit the incidentunit to set
	 */
	public void setIncidentunit(String incidentunit) {
		this.incidentunit = incidentunit;
	}
	/**
	 * @return the crossstreet1
	 */
	public String getCrossstreet1() {
		return crossstreet1;
	}
	/**
	 * @param crossstreet1 the crossstreet1 to set
	 */
	public void setCrossstreet1(String crossstreet1) {
		this.crossstreet1 = crossstreet1;
	}
	/**
	 * @return the crossstreet2
	 */
	public String getCrossstreet2() {
		return crossstreet2;
	}
	/**
	 * @param crossstreet2 the crossstreet2 to set
	 */
	public void setCrossstreet2(String crossstreet2) {
		this.crossstreet2 = crossstreet2;
	}
	/**
	 * @return the problem
	 */
	public String getProblem() {
		return problem;
	}
	/**
	 * @param problem the problem to set
	 */
	public void setProblem(String problem) {
		this.problem = problem;
	}
	/**
	 * @return the incidentdepartment
	 */
	public String getIncidentdepartment() {
		return incidentdepartment;
	}
	/**
	 * @param incidentdepartment the incidentdepartment to set
	 */
	public void setIncidentdepartment(String incidentdepartment) {
		this.incidentdepartment = incidentdepartment;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the memo
	 */
	public String getMemo() {
		return memo;
	}
	/**
	 * @param memo the memo to set
	 */
	public void setMemo(String memo) {
		this.memo = memo;
	}
	/**
	 * @return the incidentcity
	 */
	public String getIncidentcity() {
		return incidentcity;
	}
	/**
	 * @param incidentcity the incidentcity to set
	 */
	public void setIncidentcity(String incidentcity) {
		this.incidentcity = incidentcity;
	}
	/**
	 * @return the incidentstate
	 */
	public String getIncidentstate() {
		return incidentstate;
	}
	/**
	 * @param incidentstate the incidentstate to set
	 */
	public void setIncidentstate(String incidentstate) {
		this.incidentstate = incidentstate;
	}
	/**
	 * @return the incidentzip
	 */
	public String getIncidentzip() {
		return incidentzip;
	}
	/**
	 * @param incidentzip the incidentzip to set
	 */
	public void setIncidentzip(String incidentzip) {
		this.incidentzip = incidentzip;
	}
	/**
	 * @return the incidentaddress
	 */
	public String getIncidentaddress() {
		return incidentaddress;
	}
	/**
	 * @param incidentaddress the incidentaddress to set
	 */
	public void setIncidentaddress(String incidentaddress) {
		this.incidentaddress = incidentaddress;
	}
	/**
	 * @return the activityid
	 */
	public int getActivityid() {
		return activityid;
	}
	/**
	 * @param activityid the activityid to set
	 */
	public void setActivityid(int activityid) {
		this.activityid = activityid;
	}
	/**
	 * @return the rfsId
	 */
	public int getRfsId() {
		return rfsId;
	}
	/**
	 * @param rfsId the rfsId to set
	 */
	public void setRfsId(int rfsId) {
		this.rfsId = rfsId;
	}
	/**
	 * @return the created
	 */
	public String getCreated() {
		return created;
	}
	/**
	 * @param created the created to set
	 */
	public void setCreated(String created) {
		this.created = created;
	}


}
