package elms.app.sitedata;

public class SiteData {
	protected String ADDRESS;
	protected String ALARMS;
	protected String AREAOFPARKING;
	protected String BLDGCONSTTYPECODE;
	protected String BUILDINGNAME;
	protected String BUILDINGPERMITNO;
	protected String CENSUSTRACT;
	protected String COMMENTPRIMARYSYSTEM;
	protected String COMMENTSECONDARYSYSTEM;
	protected String COVENANTOFFSITE;
	protected String COVENANTONSITE;
	protected String CURRENTPRIMARYUSE;
	protected String DATEOFBLDGPERMIT;
	protected String DESIGNCODE;
	protected String DIR1;
	protected String DIR2;
	protected String ELEVATORENCLOSURE;
	protected String ESTIMATEOFWIDTHFEET;
	protected String ESTIMATEOFWIDTHINCHES;
	protected String FLOODAREA;
	protected String FOLLOWUP;
	protected String HAZARDOUSMATERIALS;
	protected String HEIGHTFEET;
	protected String HEIGHTINCHES;
	protected String HIGH1;
	protected String HIGH2;
	protected String HIGHFIRESEVERITYZONE;
	protected String INDEX;
	protected String INLIEUPARKING;
	protected String LIQUEFACTION;
	protected String LOCALFAULTZONE;
	protected String LOW1;
	protected String LOW2;
	protected String LSOID;
	protected String MAJORSTRUCTURALADDITIONS;
	protected String MAJORSTRUCTURALALTERATIONS;
	protected String MEZZANINE;
	protected String NOOFELEVATORS;
	protected String NOOFRESUNITS;
	protected String NOOFSTORIESABOVEGRADE;
	protected String NOOFSTORIESABOVEGRADEZ;
	protected String NOOFSTORIESBELOWGRADE;
	protected String NOOFSTRUCTURESONSITE;
	protected String NOTES;
	protected String OCCUPANCY;
	protected String OCCUPANTLOADCURRENT;
	protected String OCCUPANTLOADRECORDED;
	protected String ONGRADE;
	protected String ONSITEPARKING;
	protected String OTHERZONINGDETERMINATIONS;
	protected String PARCELNO;
	protected String PARCELNUMBER1;
	protected String PARCELNUMBER2;
	protected String PARCELNUMBER3;
	protected String PARCELNUMBER4;
	protected String PARCELNUMBER5;
	protected String PARKINGPROVIDED;
	protected String PARKINGREQUIRED;
	protected String PBASICSCORE;
	protected String PENTHOUSE;
	protected String PFINALSCORE;
	protected String PHIGHRISE;
	protected String PLANSONFILE;
	protected String PLARGEHEAVYCLDG;
	protected String PPLANIRREGULARITY;
	protected String PPOORCONDITION;
	protected String PPOSTBENCHMARK;
	protected String PPOSTBENCHMARKYR;
	protected String PPOUNDING;
	protected String PRETROFITTED;
	protected String PRIMARYSYSTEM;
	protected String PRIMARYUSEDATE;
	protected String PSHORTCOLUMNS;
	protected String PSL2;
	protected String PSL3;
	protected String PSOFTSTORY;
	protected String PTORSION;
	protected String PVERTICALIRREGULARITY;
	protected String RETROFITTED;
	protected String SBASICSCORE;
	protected String SECONDARYSYSTEM;
	protected String SFINALSCORE;
	protected String SHIGHRISE;
	protected String SLARGEHEAVYCLDG;
	protected String SPLANIRREGULARITY;
	protected String SPOORCONDITION;
	protected String SPOSTBENCHMARK;
	protected String SPOUNDING;
	protected String SPRINKLERED;
	protected String SRETROFITTED;
	protected String SSHORTCOLUMNS;
	protected String SSL2;
	protected String SSL3;
	protected String SSOFTSTORY;
	protected String STORSION;
	protected String STREETNAME1;
	protected String STREETNAME2;
	protected String STRIPED;
	protected String STRUCTADDRESS1;
	protected String STRUCTADDRESS2;
	protected String SUBTERRANEAN;
	protected String SVERTICALIRREGULARITY;
	protected String TOTALFLOORAREABLDGCODE;
	protected String TOTFLRAREAGRSWITHOUTPRKNG;
	protected String URM;
	protected String WITHINSTRUCTURE;
	protected String YEARPERMITAPPLIED;
	protected String ZONECURRENT;
	protected String ZONEPERMIT;
	protected SiteSetback setback;

	/**
	 * 3D73E0110376
	 */
	public SiteData() {
		setback = new SiteSetback();
	}

	/**
	 * Access method for the ADDRESS property.
	 * 
	 * @return the current value of the ADDRESS property
	 */
	public String getADDRESS() {
		return ADDRESS;
	}

	/**
	 * Sets the value of the ADDRESS property.
	 * 
	 * @param aADDRESS
	 *            the new value of the ADDRESS property
	 */
	public void setADDRESS(String aADDRESS) {
		ADDRESS = aADDRESS;
	}

	/**
	 * Access method for the ALARMS property.
	 * 
	 * @return the current value of the ALARMS property
	 */
	public String getALARMS() {
		return ALARMS;
	}

	/**
	 * Sets the value of the ALARMS property.
	 * 
	 * @param aALARMS
	 *            the new value of the ALARMS property
	 */
	public void setALARMS(String aALARMS) {
		ALARMS = aALARMS;
	}

	/**
	 * Access method for the AREAOFPARKING property.
	 * 
	 * @return the current value of the AREAOFPARKING property
	 */
	public String getAREAOFPARKING() {
		return AREAOFPARKING;
	}

	/**
	 * Sets the value of the AREAOFPARKING property.
	 * 
	 * @param aAREAOFPARKING
	 *            the new value of the AREAOFPARKING property
	 */
	public void setAREAOFPARKING(String aAREAOFPARKING) {
		AREAOFPARKING = aAREAOFPARKING;
	}

	/**
	 * Access method for the BLDGCONSTTYPECODE property.
	 * 
	 * @return the current value of the BLDGCONSTTYPECODE property
	 */
	public String getBLDGCONSTTYPECODE() {
		return BLDGCONSTTYPECODE;
	}

	/**
	 * Sets the value of the BLDGCONSTTYPECODE property.
	 * 
	 * @param aBLDGCONSTTYPECODE
	 *            the new value of the BLDGCONSTTYPECODE property
	 */
	public void setBLDGCONSTTYPECODE(String aBLDGCONSTTYPECODE) {
		BLDGCONSTTYPECODE = aBLDGCONSTTYPECODE;
	}

	/**
	 * Access method for the BUILDINGNAME property.
	 * 
	 * @return the current value of the BUILDINGNAME property
	 */
	public String getBUILDINGNAME() {
		return BUILDINGNAME;
	}

	/**
	 * Sets the value of the BUILDINGNAME property.
	 * 
	 * @param aBUILDINGNAME
	 *            the new value of the BUILDINGNAME property
	 */
	public void setBUILDINGNAME(String aBUILDINGNAME) {
		BUILDINGNAME = aBUILDINGNAME;
	}

	/**
	 * Access method for the BUILDINGPERMITNO property.
	 * 
	 * @return the current value of the BUILDINGPERMITNO property
	 */
	public String getBUILDINGPERMITNO() {
		return BUILDINGPERMITNO;
	}

	/**
	 * Sets the value of the BUILDINGPERMITNO property.
	 * 
	 * @param aBUILDINGPERMITNO
	 *            the new value of the BUILDINGPERMITNO property
	 */
	public void setBUILDINGPERMITNO(String aBUILDINGPERMITNO) {
		BUILDINGPERMITNO = aBUILDINGPERMITNO;
	}

	/**
	 * Access method for the CENSUSTRACT property.
	 * 
	 * @return the current value of the CENSUSTRACT property
	 */
	public String getCENSUSTRACT() {
		return CENSUSTRACT;
	}

	/**
	 * Sets the value of the CENSUSTRACT property.
	 * 
	 * @param aCENSUSTRACT
	 *            the new value of the CENSUSTRACT property
	 */
	public void setCENSUSTRACT(String aCENSUSTRACT) {
		CENSUSTRACT = aCENSUSTRACT;
	}

	/**
	 * Access method for the COMMENTPRIMARYSYSTEM property.
	 * 
	 * @return the current value of the COMMENTPRIMARYSYSTEM property
	 */
	public String getCOMMENTPRIMARYSYSTEM() {
		return COMMENTPRIMARYSYSTEM;
	}

	/**
	 * Sets the value of the COMMENTPRIMARYSYSTEM property.
	 * 
	 * @param aCOMMENTPRIMARYSYSTEM
	 *            the new value of the COMMENTPRIMARYSYSTEM property
	 */
	public void setCOMMENTPRIMARYSYSTEM(String aCOMMENTPRIMARYSYSTEM) {
		COMMENTPRIMARYSYSTEM = aCOMMENTPRIMARYSYSTEM;
	}

	/**
	 * Access method for the COMMENTSECONDARYSYSTEM property.
	 * 
	 * @return the current value of the COMMENTSECONDARYSYSTEM property
	 */
	public String getCOMMENTSECONDARYSYSTEM() {
		return COMMENTSECONDARYSYSTEM;
	}

	/**
	 * Sets the value of the COMMENTSECONDARYSYSTEM property.
	 * 
	 * @param aCOMMENTSECONDARYSYSTEM
	 *            the new value of the COMMENTSECONDARYSYSTEM property
	 */
	public void setCOMMENTSECONDARYSYSTEM(String aCOMMENTSECONDARYSYSTEM) {
		COMMENTSECONDARYSYSTEM = aCOMMENTSECONDARYSYSTEM;
	}

	/**
	 * Access method for the COVENANTOFFSITE property.
	 * 
	 * @return the current value of the COVENANTOFFSITE property
	 */
	public String getCOVENANTOFFSITE() {
		return COVENANTOFFSITE;
	}

	/**
	 * Sets the value of the COVENANTOFFSITE property.
	 * 
	 * @param aCOVENANTOFFSITE
	 *            the new value of the COVENANTOFFSITE property
	 */
	public void setCOVENANTOFFSITE(String aCOVENANTOFFSITE) {
		COVENANTOFFSITE = aCOVENANTOFFSITE;
	}

	/**
	 * Access method for the COVENANTONSITE property.
	 * 
	 * @return the current value of the COVENANTONSITE property
	 */
	public String getCOVENANTONSITE() {
		return COVENANTONSITE;
	}

	/**
	 * Sets the value of the COVENANTONSITE property.
	 * 
	 * @param aCOVENANTONSITE
	 *            the new value of the COVENANTONSITE property
	 */
	public void setCOVENANTONSITE(String aCOVENANTONSITE) {
		COVENANTONSITE = aCOVENANTONSITE;
	}

	/**
	 * Access method for the CURRENTPRIMARYUSE property.
	 * 
	 * @return the current value of the CURRENTPRIMARYUSE property
	 */
	public String getCURRENTPRIMARYUSE() {
		return CURRENTPRIMARYUSE;
	}

	/**
	 * Sets the value of the CURRENTPRIMARYUSE property.
	 * 
	 * @param aCURRENTPRIMARYUSE
	 *            the new value of the CURRENTPRIMARYUSE property
	 */
	public void setCURRENTPRIMARYUSE(String aCURRENTPRIMARYUSE) {
		CURRENTPRIMARYUSE = aCURRENTPRIMARYUSE;
	}

	/**
	 * Access method for the DATEOFBLDGPERMIT property.
	 * 
	 * @return the current value of the DATEOFBLDGPERMIT property
	 */
	public String getDATEOFBLDGPERMIT() {
		return DATEOFBLDGPERMIT;
	}

	/**
	 * Sets the value of the DATEOFBLDGPERMIT property.
	 * 
	 * @param aDATEOFBLDGPERMIT
	 *            the new value of the DATEOFBLDGPERMIT property
	 */
	public void setDATEOFBLDGPERMIT(String aDATEOFBLDGPERMIT) {
		DATEOFBLDGPERMIT = aDATEOFBLDGPERMIT;
	}

	/**
	 * Access method for the DESIGNCODE property.
	 * 
	 * @return the current value of the DESIGNCODE property
	 */
	public String getDESIGNCODE() {
		return DESIGNCODE;
	}

	/**
	 * Sets the value of the DESIGNCODE property.
	 * 
	 * @param aDESIGNCODE
	 *            the new value of the DESIGNCODE property
	 */
	public void setDESIGNCODE(String aDESIGNCODE) {
		DESIGNCODE = aDESIGNCODE;
	}

	/**
	 * Access method for the DIR1 property.
	 * 
	 * @return the current value of the DIR1 property
	 */
	public String getDIR1() {
		return DIR1;
	}

	/**
	 * Sets the value of the DIR1 property.
	 * 
	 * @param aDIR1
	 *            the new value of the DIR1 property
	 */
	public void setDIR1(String aDIR1) {
		DIR1 = aDIR1;
	}

	/**
	 * Access method for the DIR2 property.
	 * 
	 * @return the current value of the DIR2 property
	 */
	public String getDIR2() {
		return DIR2;
	}

	/**
	 * Sets the value of the DIR2 property.
	 * 
	 * @param aDIR2
	 *            the new value of the DIR2 property
	 */
	public void setDIR2(String aDIR2) {
		DIR2 = aDIR2;
	}

	/**
	 * Access method for the ELEVATORENCLOSURE property.
	 * 
	 * @return the current value of the ELEVATORENCLOSURE property
	 */
	public String getELEVATORENCLOSURE() {
		return ELEVATORENCLOSURE;
	}

	/**
	 * Sets the value of the ELEVATORENCLOSURE property.
	 * 
	 * @param aELEVATORENCLOSURE
	 *            the new value of the ELEVATORENCLOSURE property
	 */
	public void setELEVATORENCLOSURE(String aELEVATORENCLOSURE) {
		ELEVATORENCLOSURE = aELEVATORENCLOSURE;
	}

	/**
	 * Access method for the ESTIMATEOFWIDTHFEET property.
	 * 
	 * @return the current value of the ESTIMATEOFWIDTHFEET property
	 */
	public String getESTIMATEOFWIDTHFEET() {
		return ESTIMATEOFWIDTHFEET;
	}

	/**
	 * Sets the value of the ESTIMATEOFWIDTHFEET property.
	 * 
	 * @param aESTIMATEOFWIDTHFEET
	 *            the new value of the ESTIMATEOFWIDTHFEET property
	 */
	public void setESTIMATEOFWIDTHFEET(String aESTIMATEOFWIDTHFEET) {
		ESTIMATEOFWIDTHFEET = aESTIMATEOFWIDTHFEET;
	}

	/**
	 * Access method for the ESTIMATEOFWIDTHINCHES property.
	 * 
	 * @return the current value of the ESTIMATEOFWIDTHINCHES property
	 */
	public String getESTIMATEOFWIDTHINCHES() {
		return ESTIMATEOFWIDTHINCHES;
	}

	/**
	 * Sets the value of the ESTIMATEOFWIDTHINCHES property.
	 * 
	 * @param aESTIMATEOFWIDTHINCHES
	 *            the new value of the ESTIMATEOFWIDTHINCHES property
	 */
	public void setESTIMATEOFWIDTHINCHES(String aESTIMATEOFWIDTHINCHES) {
		ESTIMATEOFWIDTHINCHES = aESTIMATEOFWIDTHINCHES;
	}

	/**
	 * Access method for the FLOODAREA property.
	 * 
	 * @return the current value of the FLOODAREA property
	 */
	public String getFLOODAREA() {
		return FLOODAREA;
	}

	/**
	 * Sets the value of the FLOODAREA property.
	 * 
	 * @param aFLOODAREA
	 *            the new value of the FLOODAREA property
	 */
	public void setFLOODAREA(String aFLOODAREA) {
		FLOODAREA = aFLOODAREA;
	}

	/**
	 * Access method for the FOLLOWUP property.
	 * 
	 * @return the current value of the FOLLOWUP property
	 */
	public String getFOLLOWUP() {
		return FOLLOWUP;
	}

	/**
	 * Sets the value of the FOLLOWUP property.
	 * 
	 * @param aFOLLOWUP
	 *            the new value of the FOLLOWUP property
	 */
	public void setFOLLOWUP(String aFOLLOWUP) {
		FOLLOWUP = aFOLLOWUP;
	}

	/**
	 * Access method for the HAZARDOUSMATERIALS property.
	 * 
	 * @return the current value of the HAZARDOUSMATERIALS property
	 */
	public String getHAZARDOUSMATERIALS() {
		return HAZARDOUSMATERIALS;
	}

	/**
	 * Sets the value of the HAZARDOUSMATERIALS property.
	 * 
	 * @param aHAZARDOUSMATERIALS
	 *            the new value of the HAZARDOUSMATERIALS property
	 */
	public void setHAZARDOUSMATERIALS(String aHAZARDOUSMATERIALS) {
		HAZARDOUSMATERIALS = aHAZARDOUSMATERIALS;
	}

	/**
	 * Access method for the HEIGHTFEET property.
	 * 
	 * @return the current value of the HEIGHTFEET property
	 */
	public String getHEIGHTFEET() {
		return HEIGHTFEET;
	}

	/**
	 * Sets the value of the HEIGHTFEET property.
	 * 
	 * @param aHEIGHTFEET
	 *            the new value of the HEIGHTFEET property
	 */
	public void setHEIGHTFEET(String aHEIGHTFEET) {
		HEIGHTFEET = aHEIGHTFEET;
	}

	/**
	 * Access method for the HEIGHTINCHES property.
	 * 
	 * @return the current value of the HEIGHTINCHES property
	 */
	public String getHEIGHTINCHES() {
		return HEIGHTINCHES;
	}

	/**
	 * Sets the value of the HEIGHTINCHES property.
	 * 
	 * @param aHEIGHTINCHES
	 *            the new value of the HEIGHTINCHES property
	 */
	public void setHEIGHTINCHES(String aHEIGHTINCHES) {
		HEIGHTINCHES = aHEIGHTINCHES;
	}

	/**
	 * Access method for the HIGH1 property.
	 * 
	 * @return the current value of the HIGH1 property
	 */
	public String getHIGH1() {
		return HIGH1;
	}

	/**
	 * Sets the value of the HIGH1 property.
	 * 
	 * @param aHIGH1
	 *            the new value of the HIGH1 property
	 */
	public void setHIGH1(String aHIGH1) {
		HIGH1 = aHIGH1;
	}

	/**
	 * Access method for the HIGH2 property.
	 * 
	 * @return the current value of the HIGH2 property
	 */
	public String getHIGH2() {
		return HIGH2;
	}

	/**
	 * Sets the value of the HIGH2 property.
	 * 
	 * @param aHIGH2
	 *            the new value of the HIGH2 property
	 */
	public void setHIGH2(String aHIGH2) {
		HIGH2 = aHIGH2;
	}

	/**
	 * Access method for the HIGHFIRESEVERITYZONE property.
	 * 
	 * @return the current value of the HIGHFIRESEVERITYZONE property
	 */
	public String getHIGHFIRESEVERITYZONE() {
		return HIGHFIRESEVERITYZONE;
	}

	/**
	 * Sets the value of the HIGHFIRESEVERITYZONE property.
	 * 
	 * @param aHIGHFIRESEVERITYZONE
	 *            the new value of the HIGHFIRESEVERITYZONE property
	 */
	public void setHIGHFIRESEVERITYZONE(String aHIGHFIRESEVERITYZONE) {
		HIGHFIRESEVERITYZONE = aHIGHFIRESEVERITYZONE;
	}

	/**
	 * Access method for the INDEX property.
	 * 
	 * @return the current value of the INDEX property
	 */
	public String getINDEX() {
		return INDEX;
	}

	/**
	 * Sets the value of the INDEX property.
	 * 
	 * @param aINDEX
	 *            the new value of the INDEX property
	 */
	public void setINDEX(String aINDEX) {
		INDEX = aINDEX;
	}

	/**
	 * Access method for the INLIEUPARKING property.
	 * 
	 * @return the current value of the INLIEUPARKING property
	 */
	public String getINLIEUPARKING() {
		return INLIEUPARKING;
	}

	/**
	 * Sets the value of the INLIEUPARKING property.
	 * 
	 * @param aINLIEUPARKING
	 *            the new value of the INLIEUPARKING property
	 */
	public void setINLIEUPARKING(String aINLIEUPARKING) {
		INLIEUPARKING = aINLIEUPARKING;
	}

	/**
	 * Access method for the LIQUEFACTION property.
	 * 
	 * @return the current value of the LIQUEFACTION property
	 */
	public String getLIQUEFACTION() {
		return LIQUEFACTION;
	}

	/**
	 * Sets the value of the LIQUEFACTION property.
	 * 
	 * @param aLIQUEFACTION
	 *            the new value of the LIQUEFACTION property
	 */
	public void setLIQUEFACTION(String aLIQUEFACTION) {
		LIQUEFACTION = aLIQUEFACTION;
	}

	/**
	 * Access method for the LOCALFAULTZONE property.
	 * 
	 * @return the current value of the LOCALFAULTZONE property
	 */
	public String getLOCALFAULTZONE() {
		return LOCALFAULTZONE;
	}

	/**
	 * Sets the value of the LOCALFAULTZONE property.
	 * 
	 * @param aLOCALFAULTZONE
	 *            the new value of the LOCALFAULTZONE property
	 */
	public void setLOCALFAULTZONE(String aLOCALFAULTZONE) {
		LOCALFAULTZONE = aLOCALFAULTZONE;
	}

	/**
	 * Access method for the LOW1 property.
	 * 
	 * @return the current value of the LOW1 property
	 */
	public String getLOW1() {
		return LOW1;
	}

	/**
	 * Sets the value of the LOW1 property.
	 * 
	 * @param aLOW1
	 *            the new value of the LOW1 property
	 */
	public void setLOW1(String aLOW1) {
		LOW1 = aLOW1;
	}

	/**
	 * Access method for the LOW2 property.
	 * 
	 * @return the current value of the LOW2 property
	 */
	public String getLOW2() {
		return LOW2;
	}

	/**
	 * Sets the value of the LOW2 property.
	 * 
	 * @param aLOW2
	 *            the new value of the LOW2 property
	 */
	public void setLOW2(String aLOW2) {
		LOW2 = aLOW2;
	}

	/**
	 * Access method for the LSOID property.
	 * 
	 * @return the current value of the LSOID property
	 */
	public String getLSOID() {
		return LSOID;
	}

	/**
	 * Sets the value of the LSOID property.
	 * 
	 * @param aLSOID
	 *            the new value of the LSOID property
	 */
	public void setLSOID(String aLSOID) {
		LSOID = aLSOID;
	}

	/**
	 * Access method for the MAJORSTRUCTURALADDITIONS property.
	 * 
	 * @return the current value of the MAJORSTRUCTURALADDITIONS property
	 */
	public String getMAJORSTRUCTURALADDITIONS() {
		return MAJORSTRUCTURALADDITIONS;
	}

	/**
	 * Sets the value of the MAJORSTRUCTURALADDITIONS property.
	 * 
	 * @param aMAJORSTRUCTURALADDITIONS
	 *            the new value of the MAJORSTRUCTURALADDITIONS property
	 */
	public void setMAJORSTRUCTURALADDITIONS(String aMAJORSTRUCTURALADDITIONS) {
		MAJORSTRUCTURALADDITIONS = aMAJORSTRUCTURALADDITIONS;
	}

	/**
	 * Access method for the MAJORSTRUCTURALALTERATIONS property.
	 * 
	 * @return the current value of the MAJORSTRUCTURALALTERATIONS property
	 */
	public String getMAJORSTRUCTURALALTERATIONS() {
		return MAJORSTRUCTURALALTERATIONS;
	}

	/**
	 * Sets the value of the MAJORSTRUCTURALALTERATIONS property.
	 * 
	 * @param aMAJORSTRUCTURALALTERATIONS
	 *            the new value of the MAJORSTRUCTURALALTERATIONS property
	 */
	public void setMAJORSTRUCTURALALTERATIONS(String aMAJORSTRUCTURALALTERATIONS) {
		MAJORSTRUCTURALALTERATIONS = aMAJORSTRUCTURALALTERATIONS;
	}

	/**
	 * Access method for the MEZZANINE property.
	 * 
	 * @return the current value of the MEZZANINE property
	 */
	public String getMEZZANINE() {
		return MEZZANINE;
	}

	/**
	 * Sets the value of the MEZZANINE property.
	 * 
	 * @param aMEZZANINE
	 *            the new value of the MEZZANINE property
	 */
	public void setMEZZANINE(String aMEZZANINE) {
		MEZZANINE = aMEZZANINE;
	}

	/**
	 * Access method for the NOOFELEVATORS property.
	 * 
	 * @return the current value of the NOOFELEVATORS property
	 */
	public String getNOOFELEVATORS() {
		return NOOFELEVATORS;
	}

	/**
	 * Sets the value of the NOOFELEVATORS property.
	 * 
	 * @param aNOOFELEVATORS
	 *            the new value of the NOOFELEVATORS property
	 */
	public void setNOOFELEVATORS(String aNOOFELEVATORS) {
		NOOFELEVATORS = aNOOFELEVATORS;
	}

	/**
	 * Access method for the NOOFRESUNITS property.
	 * 
	 * @return the current value of the NOOFRESUNITS property
	 */
	public String getNOOFRESUNITS() {
		return NOOFRESUNITS;
	}

	/**
	 * Sets the value of the NOOFRESUNITS property.
	 * 
	 * @param aNOOFRESUNITS
	 *            the new value of the NOOFRESUNITS property
	 */
	public void setNOOFRESUNITS(String aNOOFRESUNITS) {
		NOOFRESUNITS = aNOOFRESUNITS;
	}

	/**
	 * Access method for the NOOFSTORIESABOVEGRADE property.
	 * 
	 * @return the current value of the NOOFSTORIESABOVEGRADE property
	 */
	public String getNOOFSTORIESABOVEGRADE() {
		return NOOFSTORIESABOVEGRADE;
	}

	/**
	 * Sets the value of the NOOFSTORIESABOVEGRADE property.
	 * 
	 * @param aNOOFSTORIESABOVEGRADE
	 *            the new value of the NOOFSTORIESABOVEGRADE property
	 */
	public void setNOOFSTORIESABOVEGRADE(String aNOOFSTORIESABOVEGRADE) {
		NOOFSTORIESABOVEGRADE = aNOOFSTORIESABOVEGRADE;
	}

	/**
	 * Access method for the NOOFSTORIESABOVEGRADEZ property.
	 * 
	 * @return the current value of the NOOFSTORIESABOVEGRADEZ property
	 */
	public String getNOOFSTORIESABOVEGRADEZ() {
		return NOOFSTORIESABOVEGRADEZ;
	}

	/**
	 * Sets the value of the NOOFSTORIESABOVEGRADEZ property.
	 * 
	 * @param aNOOFSTORIESABOVEGRADEZ
	 *            the new value of the NOOFSTORIESABOVEGRADEZ property
	 */
	public void setNOOFSTORIESABOVEGRADEZ(String aNOOFSTORIESABOVEGRADEZ) {
		NOOFSTORIESABOVEGRADEZ = aNOOFSTORIESABOVEGRADEZ;
	}

	/**
	 * Access method for the NOOFSTORIESBELOWGRADE property.
	 * 
	 * @return the current value of the NOOFSTORIESBELOWGRADE property
	 */
	public String getNOOFSTORIESBELOWGRADE() {
		return NOOFSTORIESBELOWGRADE;
	}

	/**
	 * Sets the value of the NOOFSTORIESBELOWGRADE property.
	 * 
	 * @param aNOOFSTORIESBELOWGRADE
	 *            the new value of the NOOFSTORIESBELOWGRADE property
	 */
	public void setNOOFSTORIESBELOWGRADE(String aNOOFSTORIESBELOWGRADE) {
		NOOFSTORIESBELOWGRADE = aNOOFSTORIESBELOWGRADE;
	}

	/**
	 * Access method for the NOOFSTRUCTURESONSITE property.
	 * 
	 * @return the current value of the NOOFSTRUCTURESONSITE property
	 */
	public String getNOOFSTRUCTURESONSITE() {
		return NOOFSTRUCTURESONSITE;
	}

	/**
	 * Sets the value of the NOOFSTRUCTURESONSITE property.
	 * 
	 * @param aNOOFSTRUCTURESONSITE
	 *            the new value of the NOOFSTRUCTURESONSITE property
	 */
	public void setNOOFSTRUCTURESONSITE(String aNOOFSTRUCTURESONSITE) {
		NOOFSTRUCTURESONSITE = aNOOFSTRUCTURESONSITE;
	}

	/**
	 * Access method for the NOTES property.
	 * 
	 * @return the current value of the NOTES property
	 */
	public String getNOTES() {
		return NOTES;
	}

	/**
	 * Sets the value of the NOTES property.
	 * 
	 * @param aNOTES
	 *            the new value of the NOTES property
	 */
	public void setNOTES(String aNOTES) {
		NOTES = aNOTES;
	}

	/**
	 * Access method for the OCCUPANCY property.
	 * 
	 * @return the current value of the OCCUPANCY property
	 */
	public String getOCCUPANCY() {
		return OCCUPANCY;
	}

	/**
	 * Sets the value of the OCCUPANCY property.
	 * 
	 * @param aOCCUPANCY
	 *            the new value of the OCCUPANCY property
	 */
	public void setOCCUPANCY(String aOCCUPANCY) {
		OCCUPANCY = aOCCUPANCY;
	}

	/**
	 * Access method for the OCCUPANTLOADCURRENT property.
	 * 
	 * @return the current value of the OCCUPANTLOADCURRENT property
	 */
	public String getOCCUPANTLOADCURRENT() {
		return OCCUPANTLOADCURRENT;
	}

	/**
	 * Sets the value of the OCCUPANTLOADCURRENT property.
	 * 
	 * @param aOCCUPANTLOADCURRENT
	 *            the new value of the OCCUPANTLOADCURRENT property
	 */
	public void setOCCUPANTLOADCURRENT(String aOCCUPANTLOADCURRENT) {
		OCCUPANTLOADCURRENT = aOCCUPANTLOADCURRENT;
	}

	/**
	 * Access method for the OCCUPANTLOADRECORDED property.
	 * 
	 * @return the current value of the OCCUPANTLOADRECORDED property
	 */
	public String getOCCUPANTLOADRECORDED() {
		return OCCUPANTLOADRECORDED;
	}

	/**
	 * Sets the value of the OCCUPANTLOADRECORDED property.
	 * 
	 * @param aOCCUPANTLOADRECORDED
	 *            the new value of the OCCUPANTLOADRECORDED property
	 */
	public void setOCCUPANTLOADRECORDED(String aOCCUPANTLOADRECORDED) {
		OCCUPANTLOADRECORDED = aOCCUPANTLOADRECORDED;
	}

	/**
	 * Access method for the ONGRADE property.
	 * 
	 * @return the current value of the ONGRADE property
	 */
	public String getONGRADE() {
		return ONGRADE;
	}

	/**
	 * Sets the value of the ONGRADE property.
	 * 
	 * @param aONGRADE
	 *            the new value of the ONGRADE property
	 */
	public void setONGRADE(String aONGRADE) {
		ONGRADE = aONGRADE;
	}

	/**
	 * Access method for the ONSITEPARKING property.
	 * 
	 * @return the current value of the ONSITEPARKING property
	 */
	public String getONSITEPARKING() {
		return ONSITEPARKING;
	}

	/**
	 * Sets the value of the ONSITEPARKING property.
	 * 
	 * @param aONSITEPARKING
	 *            the new value of the ONSITEPARKING property
	 */
	public void setONSITEPARKING(String aONSITEPARKING) {
		ONSITEPARKING = aONSITEPARKING;
	}

	/**
	 * Access method for the OTHERZONINGDETERMINATIONS property.
	 * 
	 * @return the current value of the OTHERZONINGDETERMINATIONS property
	 */
	public String getOTHERZONINGDETERMINATIONS() {
		return OTHERZONINGDETERMINATIONS;
	}

	/**
	 * Sets the value of the OTHERZONINGDETERMINATIONS property.
	 * 
	 * @param aOTHERZONINGDETERMINATIONS
	 *            the new value of the OTHERZONINGDETERMINATIONS property
	 */
	public void setOTHERZONINGDETERMINATIONS(String aOTHERZONINGDETERMINATIONS) {
		OTHERZONINGDETERMINATIONS = aOTHERZONINGDETERMINATIONS;
	}

	/**
	 * Access method for the PARCELNO property.
	 * 
	 * @return the current value of the PARCELNO property
	 */
	public String getPARCELNO() {
		return PARCELNO;
	}

	/**
	 * Sets the value of the PARCELNO property.
	 * 
	 * @param aPARCELNO
	 *            the new value of the PARCELNO property
	 */
	public void setPARCELNO(String aPARCELNO) {
		PARCELNO = aPARCELNO;
	}

	/**
	 * Access method for the PARCELNUMBER1 property.
	 * 
	 * @return the current value of the PARCELNUMBER1 property
	 */
	public String getPARCELNUMBER1() {
		return PARCELNUMBER1;
	}

	/**
	 * Sets the value of the PARCELNUMBER1 property.
	 * 
	 * @param aPARCELNUMBER1
	 *            the new value of the PARCELNUMBER1 property
	 */
	public void setPARCELNUMBER1(String aPARCELNUMBER1) {
		PARCELNUMBER1 = aPARCELNUMBER1;
	}

	/**
	 * Access method for the PARCELNUMBER2 property.
	 * 
	 * @return the current value of the PARCELNUMBER2 property
	 */
	public String getPARCELNUMBER2() {
		return PARCELNUMBER2;
	}

	/**
	 * Sets the value of the PARCELNUMBER2 property.
	 * 
	 * @param aPARCELNUMBER2
	 *            the new value of the PARCELNUMBER2 property
	 */
	public void setPARCELNUMBER2(String aPARCELNUMBER2) {
		PARCELNUMBER2 = aPARCELNUMBER2;
	}

	/**
	 * Access method for the PARCELNUMBER3 property.
	 * 
	 * @return the current value of the PARCELNUMBER3 property
	 */
	public String getPARCELNUMBER3() {
		return PARCELNUMBER3;
	}

	/**
	 * Sets the value of the PARCELNUMBER3 property.
	 * 
	 * @param aPARCELNUMBER3
	 *            the new value of the PARCELNUMBER3 property
	 */
	public void setPARCELNUMBER3(String aPARCELNUMBER3) {
		PARCELNUMBER3 = aPARCELNUMBER3;
	}

	/**
	 * Access method for the PARCELNUMBER4 property.
	 * 
	 * @return the current value of the PARCELNUMBER4 property
	 */
	public String getPARCELNUMBER4() {
		return PARCELNUMBER4;
	}

	/**
	 * Sets the value of the PARCELNUMBER4 property.
	 * 
	 * @param aPARCELNUMBER4
	 *            the new value of the PARCELNUMBER4 property
	 */
	public void setPARCELNUMBER4(String aPARCELNUMBER4) {
		PARCELNUMBER4 = aPARCELNUMBER4;
	}

	/**
	 * Access method for the PARCELNUMBER5 property.
	 * 
	 * @return the current value of the PARCELNUMBER5 property
	 */
	public String getPARCELNUMBER5() {
		return PARCELNUMBER5;
	}

	/**
	 * Sets the value of the PARCELNUMBER5 property.
	 * 
	 * @param aPARCELNUMBER5
	 *            the new value of the PARCELNUMBER5 property
	 */
	public void setPARCELNUMBER5(String aPARCELNUMBER5) {
		PARCELNUMBER5 = aPARCELNUMBER5;
	}

	/**
	 * Access method for the PARKINGPROVIDED property.
	 * 
	 * @return the current value of the PARKINGPROVIDED property
	 */
	public String getPARKINGPROVIDED() {
		return PARKINGPROVIDED;
	}

	/**
	 * Sets the value of the PARKINGPROVIDED property.
	 * 
	 * @param aPARKINGPROVIDED
	 *            the new value of the PARKINGPROVIDED property
	 */
	public void setPARKINGPROVIDED(String aPARKINGPROVIDED) {
		PARKINGPROVIDED = aPARKINGPROVIDED;
	}

	/**
	 * Access method for the PARKINGREQUIRED property.
	 * 
	 * @return the current value of the PARKINGREQUIRED property
	 */
	public String getPARKINGREQUIRED() {
		return PARKINGREQUIRED;
	}

	/**
	 * Sets the value of the PARKINGREQUIRED property.
	 * 
	 * @param aPARKINGREQUIRED
	 *            the new value of the PARKINGREQUIRED property
	 */
	public void setPARKINGREQUIRED(String aPARKINGREQUIRED) {
		PARKINGREQUIRED = aPARKINGREQUIRED;
	}

	/**
	 * Access method for the PBASICSCORE property.
	 * 
	 * @return the current value of the PBASICSCORE property
	 */
	public String getPBASICSCORE() {
		return PBASICSCORE;
	}

	/**
	 * Sets the value of the PBASICSCORE property.
	 * 
	 * @param aPBASICSCORE
	 *            the new value of the PBASICSCORE property
	 */
	public void setPBASICSCORE(String aPBASICSCORE) {
		PBASICSCORE = aPBASICSCORE;
	}

	/**
	 * Access method for the PENTHOUSE property.
	 * 
	 * @return the current value of the PENTHOUSE property
	 */
	public String getPENTHOUSE() {
		return PENTHOUSE;
	}

	/**
	 * Sets the value of the PENTHOUSE property.
	 * 
	 * @param aPENTHOUSE
	 *            the new value of the PENTHOUSE property
	 */
	public void setPENTHOUSE(String aPENTHOUSE) {
		PENTHOUSE = aPENTHOUSE;
	}

	/**
	 * Access method for the PFINALSCORE property.
	 * 
	 * @return the current value of the PFINALSCORE property
	 */
	public String getPFINALSCORE() {
		return PFINALSCORE;
	}

	/**
	 * Sets the value of the PFINALSCORE property.
	 * 
	 * @param aPFINALSCORE
	 *            the new value of the PFINALSCORE property
	 */
	public void setPFINALSCORE(String aPFINALSCORE) {
		PFINALSCORE = aPFINALSCORE;
	}

	/**
	 * Access method for the PHIGHRISE property.
	 * 
	 * @return the current value of the PHIGHRISE property
	 */
	public String getPHIGHRISE() {
		return PHIGHRISE;
	}

	/**
	 * Sets the value of the PHIGHRISE property.
	 * 
	 * @param aPHIGHRISE
	 *            the new value of the PHIGHRISE property
	 */
	public void setPHIGHRISE(String aPHIGHRISE) {
		PHIGHRISE = aPHIGHRISE;
	}

	/**
	 * Access method for the PLANSONFILE property.
	 * 
	 * @return the current value of the PLANSONFILE property
	 */
	public String getPLANSONFILE() {
		return PLANSONFILE;
	}

	/**
	 * Sets the value of the PLANSONFILE property.
	 * 
	 * @param aPLANSONFILE
	 *            the new value of the PLANSONFILE property
	 */
	public void setPLANSONFILE(String aPLANSONFILE) {
		PLANSONFILE = aPLANSONFILE;
	}

	/**
	 * Access method for the PLARGEHEAVYCLDG property.
	 * 
	 * @return the current value of the PLARGEHEAVYCLDG property
	 */
	public String getPLARGEHEAVYCLDG() {
		return PLARGEHEAVYCLDG;
	}

	/**
	 * Sets the value of the PLARGEHEAVYCLDG property.
	 * 
	 * @param aPLARGEHEAVYCLDG
	 *            the new value of the PLARGEHEAVYCLDG property
	 */
	public void setPLARGEHEAVYCLDG(String aPLARGEHEAVYCLDG) {
		PLARGEHEAVYCLDG = aPLARGEHEAVYCLDG;
	}

	/**
	 * Access method for the PPLANIRREGULARITY property.
	 * 
	 * @return the current value of the PPLANIRREGULARITY property
	 */
	public String getPPLANIRREGULARITY() {
		return PPLANIRREGULARITY;
	}

	/**
	 * Sets the value of the PPLANIRREGULARITY property.
	 * 
	 * @param aPPLANIRREGULARITY
	 *            the new value of the PPLANIRREGULARITY property
	 */
	public void setPPLANIRREGULARITY(String aPPLANIRREGULARITY) {
		PPLANIRREGULARITY = aPPLANIRREGULARITY;
	}

	/**
	 * Access method for the PPOORCONDITION property.
	 * 
	 * @return the current value of the PPOORCONDITION property
	 */
	public String getPPOORCONDITION() {
		return PPOORCONDITION;
	}

	/**
	 * Sets the value of the PPOORCONDITION property.
	 * 
	 * @param aPPOORCONDITION
	 *            the new value of the PPOORCONDITION property
	 */
	public void setPPOORCONDITION(String aPPOORCONDITION) {
		PPOORCONDITION = aPPOORCONDITION;
	}

	/**
	 * Access method for the PPOSTBENCHMARK property.
	 * 
	 * @return the current value of the PPOSTBENCHMARK property
	 */
	public String getPPOSTBENCHMARK() {
		return PPOSTBENCHMARK;
	}

	/**
	 * Sets the value of the PPOSTBENCHMARK property.
	 * 
	 * @param aPPOSTBENCHMARK
	 *            the new value of the PPOSTBENCHMARK property
	 */
	public void setPPOSTBENCHMARK(String aPPOSTBENCHMARK) {
		PPOSTBENCHMARK = aPPOSTBENCHMARK;
	}

	/**
	 * Access method for the PPOSTBENCHMARKYR property.
	 * 
	 * @return the current value of the PPOSTBENCHMARKYR property
	 */
	public String getPPOSTBENCHMARKYR() {
		return PPOSTBENCHMARKYR;
	}

	/**
	 * Sets the value of the PPOSTBENCHMARKYR property.
	 * 
	 * @param aPPOSTBENCHMARKYR
	 *            the new value of the PPOSTBENCHMARKYR property
	 */
	public void setPPOSTBENCHMARKYR(String aPPOSTBENCHMARKYR) {
		PPOSTBENCHMARKYR = aPPOSTBENCHMARKYR;
	}

	/**
	 * Access method for the PPOUNDING property.
	 * 
	 * @return the current value of the PPOUNDING property
	 */
	public String getPPOUNDING() {
		return PPOUNDING;
	}

	/**
	 * Sets the value of the PPOUNDING property.
	 * 
	 * @param aPPOUNDING
	 *            the new value of the PPOUNDING property
	 */
	public void setPPOUNDING(String aPPOUNDING) {
		PPOUNDING = aPPOUNDING;
	}

	/**
	 * Access method for the PRETROFITTED property.
	 * 
	 * @return the current value of the PRETROFITTED property
	 */
	public String getPRETROFITTED() {
		return PRETROFITTED;
	}

	/**
	 * Sets the value of the PRETROFITTED property.
	 * 
	 * @param aPRETROFITTED
	 *            the new value of the PRETROFITTED property
	 */
	public void setPRETROFITTED(String aPRETROFITTED) {
		PRETROFITTED = aPRETROFITTED;
	}

	/**
	 * Access method for the PRIMARYSYSTEM property.
	 * 
	 * @return the current value of the PRIMARYSYSTEM property
	 */
	public String getPRIMARYSYSTEM() {
		return PRIMARYSYSTEM;
	}

	/**
	 * Sets the value of the PRIMARYSYSTEM property.
	 * 
	 * @param aPRIMARYSYSTEM
	 *            the new value of the PRIMARYSYSTEM property
	 */
	public void setPRIMARYSYSTEM(String aPRIMARYSYSTEM) {
		PRIMARYSYSTEM = aPRIMARYSYSTEM;
	}

	/**
	 * Access method for the PRIMARYUSEDATE property.
	 * 
	 * @return the current value of the PRIMARYUSEDATE property
	 */
	public String getPRIMARYUSEDATE() {
		return PRIMARYUSEDATE;
	}

	/**
	 * Sets the value of the PRIMARYUSEDATE property.
	 * 
	 * @param aPRIMARYUSEDATE
	 *            the new value of the PRIMARYUSEDATE property
	 */
	public void setPRIMARYUSEDATE(String aPRIMARYUSEDATE) {
		PRIMARYUSEDATE = aPRIMARYUSEDATE;
	}

	/**
	 * Access method for the PSHORTCOLUMNS property.
	 * 
	 * @return the current value of the PSHORTCOLUMNS property
	 */
	public String getPSHORTCOLUMNS() {
		return PSHORTCOLUMNS;
	}

	/**
	 * Sets the value of the PSHORTCOLUMNS property.
	 * 
	 * @param aPSHORTCOLUMNS
	 *            the new value of the PSHORTCOLUMNS property
	 */
	public void setPSHORTCOLUMNS(String aPSHORTCOLUMNS) {
		PSHORTCOLUMNS = aPSHORTCOLUMNS;
	}

	/**
	 * Access method for the PSL2 property.
	 * 
	 * @return the current value of the PSL2 property
	 */
	public String getPSL2() {
		return PSL2;
	}

	/**
	 * Sets the value of the PSL2 property.
	 * 
	 * @param aPSL2
	 *            the new value of the PSL2 property
	 */
	public void setPSL2(String aPSL2) {
		PSL2 = aPSL2;
	}

	/**
	 * Access method for the PSL3 property.
	 * 
	 * @return the current value of the PSL3 property
	 */
	public String getPSL3() {
		return PSL3;
	}

	/**
	 * Sets the value of the PSL3 property.
	 * 
	 * @param aPSL3
	 *            the new value of the PSL3 property
	 */
	public void setPSL3(String aPSL3) {
		PSL3 = aPSL3;
	}

	/**
	 * Access method for the PSOFTSTORY property.
	 * 
	 * @return the current value of the PSOFTSTORY property
	 */
	public String getPSOFTSTORY() {
		return PSOFTSTORY;
	}

	/**
	 * Sets the value of the PSOFTSTORY property.
	 * 
	 * @param aPSOFTSTORY
	 *            the new value of the PSOFTSTORY property
	 */
	public void setPSOFTSTORY(String aPSOFTSTORY) {
		PSOFTSTORY = aPSOFTSTORY;
	}

	/**
	 * Access method for the PTORSION property.
	 * 
	 * @return the current value of the PTORSION property
	 */
	public String getPTORSION() {
		return PTORSION;
	}

	/**
	 * Sets the value of the PTORSION property.
	 * 
	 * @param aPTORSION
	 *            the new value of the PTORSION property
	 */
	public void setPTORSION(String aPTORSION) {
		PTORSION = aPTORSION;
	}

	/**
	 * Access method for the PVERTICALIRREGULARITY property.
	 * 
	 * @return the current value of the PVERTICALIRREGULARITY property
	 */
	public String getPVERTICALIRREGULARITY() {
		return PVERTICALIRREGULARITY;
	}

	/**
	 * Sets the value of the PVERTICALIRREGULARITY property.
	 * 
	 * @param aPVERTICALIRREGULARITY
	 *            the new value of the PVERTICALIRREGULARITY property
	 */
	public void setPVERTICALIRREGULARITY(String aPVERTICALIRREGULARITY) {
		PVERTICALIRREGULARITY = aPVERTICALIRREGULARITY;
	}

	/**
	 * Access method for the RETROFITTED property.
	 * 
	 * @return the current value of the RETROFITTED property
	 */
	public String getRETROFITTED() {
		return RETROFITTED;
	}

	/**
	 * Sets the value of the RETROFITTED property.
	 * 
	 * @param aRETROFITTED
	 *            the new value of the RETROFITTED property
	 */
	public void setRETROFITTED(String aRETROFITTED) {
		RETROFITTED = aRETROFITTED;
	}

	/**
	 * Access method for the SBASICSCORE property.
	 * 
	 * @return the current value of the SBASICSCORE property
	 */
	public String getSBASICSCORE() {
		return SBASICSCORE;
	}

	/**
	 * Sets the value of the SBASICSCORE property.
	 * 
	 * @param aSBASICSCORE
	 *            the new value of the SBASICSCORE property
	 */
	public void setSBASICSCORE(String aSBASICSCORE) {
		SBASICSCORE = aSBASICSCORE;
	}

	/**
	 * Access method for the SECONDARYSYSTEM property.
	 * 
	 * @return the current value of the SECONDARYSYSTEM property
	 */
	public String getSECONDARYSYSTEM() {
		return SECONDARYSYSTEM;
	}

	/**
	 * Sets the value of the SECONDARYSYSTEM property.
	 * 
	 * @param aSECONDARYSYSTEM
	 *            the new value of the SECONDARYSYSTEM property
	 */
	public void setSECONDARYSYSTEM(String aSECONDARYSYSTEM) {
		SECONDARYSYSTEM = aSECONDARYSYSTEM;
	}

	/**
	 * Access method for the SFINALSCORE property.
	 * 
	 * @return the current value of the SFINALSCORE property
	 */
	public String getSFINALSCORE() {
		return SFINALSCORE;
	}

	/**
	 * Sets the value of the SFINALSCORE property.
	 * 
	 * @param aSFINALSCORE
	 *            the new value of the SFINALSCORE property
	 */
	public void setSFINALSCORE(String aSFINALSCORE) {
		SFINALSCORE = aSFINALSCORE;
	}

	/**
	 * Access method for the SHIGHRISE property.
	 * 
	 * @return the current value of the SHIGHRISE property
	 */
	public String getSHIGHRISE() {
		return SHIGHRISE;
	}

	/**
	 * Sets the value of the SHIGHRISE property.
	 * 
	 * @param aSHIGHRISE
	 *            the new value of the SHIGHRISE property
	 */
	public void setSHIGHRISE(String aSHIGHRISE) {
		SHIGHRISE = aSHIGHRISE;
	}

	/**
	 * Access method for the SLARGEHEAVYCLDG property.
	 * 
	 * @return the current value of the SLARGEHEAVYCLDG property
	 */
	public String getSLARGEHEAVYCLDG() {
		return SLARGEHEAVYCLDG;
	}

	/**
	 * Sets the value of the SLARGEHEAVYCLDG property.
	 * 
	 * @param aSLARGEHEAVYCLDG
	 *            the new value of the SLARGEHEAVYCLDG property
	 */
	public void setSLARGEHEAVYCLDG(String aSLARGEHEAVYCLDG) {
		SLARGEHEAVYCLDG = aSLARGEHEAVYCLDG;
	}

	/**
	 * Access method for the SPLANIRREGULARITY property.
	 * 
	 * @return the current value of the SPLANIRREGULARITY property
	 */
	public String getSPLANIRREGULARITY() {
		return SPLANIRREGULARITY;
	}

	/**
	 * Sets the value of the SPLANIRREGULARITY property.
	 * 
	 * @param aSPLANIRREGULARITY
	 *            the new value of the SPLANIRREGULARITY property
	 */
	public void setSPLANIRREGULARITY(String aSPLANIRREGULARITY) {
		SPLANIRREGULARITY = aSPLANIRREGULARITY;
	}

	/**
	 * Access method for the SPOORCONDITION property.
	 * 
	 * @return the current value of the SPOORCONDITION property
	 */
	public String getSPOORCONDITION() {
		return SPOORCONDITION;
	}

	/**
	 * Sets the value of the SPOORCONDITION property.
	 * 
	 * @param aSPOORCONDITION
	 *            the new value of the SPOORCONDITION property
	 */
	public void setSPOORCONDITION(String aSPOORCONDITION) {
		SPOORCONDITION = aSPOORCONDITION;
	}

	/**
	 * Access method for the SPOSTBENCHMARK property.
	 * 
	 * @return the current value of the SPOSTBENCHMARK property
	 */
	public String getSPOSTBENCHMARK() {
		return SPOSTBENCHMARK;
	}

	/**
	 * Sets the value of the SPOSTBENCHMARK property.
	 * 
	 * @param aSPOSTBENCHMARK
	 *            the new value of the SPOSTBENCHMARK property
	 */
	public void setSPOSTBENCHMARK(String aSPOSTBENCHMARK) {
		SPOSTBENCHMARK = aSPOSTBENCHMARK;
	}

	/**
	 * Access method for the SPOUNDING property.
	 * 
	 * @return the current value of the SPOUNDING property
	 */
	public String getSPOUNDING() {
		return SPOUNDING;
	}

	/**
	 * Sets the value of the SPOUNDING property.
	 * 
	 * @param aSPOUNDING
	 *            the new value of the SPOUNDING property
	 */
	public void setSPOUNDING(String aSPOUNDING) {
		SPOUNDING = aSPOUNDING;
	}

	/**
	 * Access method for the SPRINKLERED property.
	 * 
	 * @return the current value of the SPRINKLERED property
	 */
	public String getSPRINKLERED() {
		return SPRINKLERED;
	}

	/**
	 * Sets the value of the SPRINKLERED property.
	 * 
	 * @param aSPRINKLERED
	 *            the new value of the SPRINKLERED property
	 */
	public void setSPRINKLERED(String aSPRINKLERED) {
		SPRINKLERED = aSPRINKLERED;
	}

	/**
	 * Access method for the SRETROFITTED property.
	 * 
	 * @return the current value of the SRETROFITTED property
	 */
	public String getSRETROFITTED() {
		return SRETROFITTED;
	}

	/**
	 * Sets the value of the SRETROFITTED property.
	 * 
	 * @param aSRETROFITTED
	 *            the new value of the SRETROFITTED property
	 */
	public void setSRETROFITTED(String aSRETROFITTED) {
		SRETROFITTED = aSRETROFITTED;
	}

	/**
	 * Access method for the SSHORTCOLUMNS property.
	 * 
	 * @return the current value of the SSHORTCOLUMNS property
	 */
	public String getSSHORTCOLUMNS() {
		return SSHORTCOLUMNS;
	}

	/**
	 * Sets the value of the SSHORTCOLUMNS property.
	 * 
	 * @param aSSHORTCOLUMNS
	 *            the new value of the SSHORTCOLUMNS property
	 */
	public void setSSHORTCOLUMNS(String aSSHORTCOLUMNS) {
		SSHORTCOLUMNS = aSSHORTCOLUMNS;
	}

	/**
	 * Access method for the SSL2 property.
	 * 
	 * @return the current value of the SSL2 property
	 */
	public String getSSL2() {
		return SSL2;
	}

	/**
	 * Sets the value of the SSL2 property.
	 * 
	 * @param aSSL2
	 *            the new value of the SSL2 property
	 */
	public void setSSL2(String aSSL2) {
		SSL2 = aSSL2;
	}

	/**
	 * Access method for the SSL3 property.
	 * 
	 * @return the current value of the SSL3 property
	 */
	public String getSSL3() {
		return SSL3;
	}

	/**
	 * Sets the value of the SSL3 property.
	 * 
	 * @param aSSL3
	 *            the new value of the SSL3 property
	 */
	public void setSSL3(String aSSL3) {
		SSL3 = aSSL3;
	}

	/**
	 * Access method for the SSOFTSTORY property.
	 * 
	 * @return the current value of the SSOFTSTORY property
	 */
	public String getSSOFTSTORY() {
		return SSOFTSTORY;
	}

	/**
	 * Sets the value of the SSOFTSTORY property.
	 * 
	 * @param aSSOFTSTORY
	 *            the new value of the SSOFTSTORY property
	 */
	public void setSSOFTSTORY(String aSSOFTSTORY) {
		SSOFTSTORY = aSSOFTSTORY;
	}

	/**
	 * Access method for the STORSION property.
	 * 
	 * @return the current value of the STORSION property
	 */
	public String getSTORSION() {
		return STORSION;
	}

	/**
	 * Sets the value of the STORSION property.
	 * 
	 * @param aSTORSION
	 *            the new value of the STORSION property
	 */
	public void setSTORSION(String aSTORSION) {
		STORSION = aSTORSION;
	}

	/**
	 * Access method for the STREETNAME1 property.
	 * 
	 * @return the current value of the STREETNAME1 property
	 */
	public String getSTREETNAME1() {
		return STREETNAME1;
	}

	/**
	 * Sets the value of the STREETNAME1 property.
	 * 
	 * @param aSTREETNAME1
	 *            the new value of the STREETNAME1 property
	 */
	public void setSTREETNAME1(String aSTREETNAME1) {
		STREETNAME1 = aSTREETNAME1;
	}

	/**
	 * Access method for the STREETNAME2 property.
	 * 
	 * @return the current value of the STREETNAME2 property
	 */
	public String getSTREETNAME2() {
		return STREETNAME2;
	}

	/**
	 * Sets the value of the STREETNAME2 property.
	 * 
	 * @param aSTREETNAME2
	 *            the new value of the STREETNAME2 property
	 */
	public void setSTREETNAME2(String aSTREETNAME2) {
		STREETNAME2 = aSTREETNAME2;
	}

	/**
	 * Access method for the STRIPED property.
	 * 
	 * @return the current value of the STRIPED property
	 */
	public String getSTRIPED() {
		return STRIPED;
	}

	/**
	 * Sets the value of the STRIPED property.
	 * 
	 * @param aSTRIPED
	 *            the new value of the STRIPED property
	 */
	public void setSTRIPED(String aSTRIPED) {
		STRIPED = aSTRIPED;
	}

	/**
	 * Access method for the STRUCTADDRESS1 property.
	 * 
	 * @return the current value of the STRUCTADDRESS1 property
	 */
	public String getSTRUCTADDRESS1() {
		return STRUCTADDRESS1;
	}

	/**
	 * Sets the value of the STRUCTADDRESS1 property.
	 * 
	 * @param aSTRUCTADDRESS1
	 *            the new value of the STRUCTADDRESS1 property
	 */
	public void setSTRUCTADDRESS1(String aSTRUCTADDRESS1) {
		STRUCTADDRESS1 = aSTRUCTADDRESS1;
	}

	/**
	 * Access method for the STRUCTADDRESS2 property.
	 * 
	 * @return the current value of the STRUCTADDRESS2 property
	 */
	public String getSTRUCTADDRESS2() {
		return STRUCTADDRESS2;
	}

	/**
	 * Sets the value of the STRUCTADDRESS2 property.
	 * 
	 * @param aSTRUCTADDRESS2
	 *            the new value of the STRUCTADDRESS2 property
	 */
	public void setSTRUCTADDRESS2(String aSTRUCTADDRESS2) {
		STRUCTADDRESS2 = aSTRUCTADDRESS2;
	}

	/**
	 * Access method for the SUBTERRANEAN property.
	 * 
	 * @return the current value of the SUBTERRANEAN property
	 */
	public String getSUBTERRANEAN() {
		return SUBTERRANEAN;
	}

	/**
	 * Sets the value of the SUBTERRANEAN property.
	 * 
	 * @param aSUBTERRANEAN
	 *            the new value of the SUBTERRANEAN property
	 */
	public void setSUBTERRANEAN(String aSUBTERRANEAN) {
		SUBTERRANEAN = aSUBTERRANEAN;
	}

	/**
	 * Access method for the SVERTICALIRREGULARITY property.
	 * 
	 * @return the current value of the SVERTICALIRREGULARITY property
	 */
	public String getSVERTICALIRREGULARITY() {
		return SVERTICALIRREGULARITY;
	}

	/**
	 * Sets the value of the SVERTICALIRREGULARITY property.
	 * 
	 * @param aSVERTICALIRREGULARITY
	 *            the new value of the SVERTICALIRREGULARITY property
	 */
	public void setSVERTICALIRREGULARITY(String aSVERTICALIRREGULARITY) {
		SVERTICALIRREGULARITY = aSVERTICALIRREGULARITY;
	}

	/**
	 * Access method for the TOTALFLOORAREABLDGCODE property.
	 * 
	 * @return the current value of the TOTALFLOORAREABLDGCODE property
	 */
	public String getTOTALFLOORAREABLDGCODE() {
		return TOTALFLOORAREABLDGCODE;
	}

	/**
	 * Sets the value of the TOTALFLOORAREABLDGCODE property.
	 * 
	 * @param aTOTALFLOORAREABLDGCODE
	 *            the new value of the TOTALFLOORAREABLDGCODE property
	 */
	public void setTOTALFLOORAREABLDGCODE(String aTOTALFLOORAREABLDGCODE) {
		TOTALFLOORAREABLDGCODE = aTOTALFLOORAREABLDGCODE;
	}

	/**
	 * Access method for the TOTFLRAREAGRSWITHOUTPRKNG property.
	 * 
	 * @return the current value of the TOTFLRAREAGRSWITHOUTPRKNG property
	 */
	public String getTOTFLRAREAGRSWITHOUTPRKNG() {
		return TOTFLRAREAGRSWITHOUTPRKNG;
	}

	/**
	 * Sets the value of the TOTFLRAREAGRSWITHOUTPRKNG property.
	 * 
	 * @param aTOTFLRAREAGRSWITHOUTPRKNG
	 *            the new value of the TOTFLRAREAGRSWITHOUTPRKNG property
	 */
	public void setTOTFLRAREAGRSWITHOUTPRKNG(String aTOTFLRAREAGRSWITHOUTPRKNG) {
		TOTFLRAREAGRSWITHOUTPRKNG = aTOTFLRAREAGRSWITHOUTPRKNG;
	}

	/**
	 * Access method for the URM property.
	 * 
	 * @return the current value of the URM property
	 */
	public String getURM() {
		return URM;
	}

	/**
	 * Sets the value of the URM property.
	 * 
	 * @param aURM
	 *            the new value of the URM property
	 */
	public void setURM(String aURM) {
		URM = aURM;
	}

	/**
	 * Access method for the WITHINSTRUCTURE property.
	 * 
	 * @return the current value of the WITHINSTRUCTURE property
	 */
	public String getWITHINSTRUCTURE() {
		return WITHINSTRUCTURE;
	}

	/**
	 * Sets the value of the WITHINSTRUCTURE property.
	 * 
	 * @param aWITHINSTRUCTURE
	 *            the new value of the WITHINSTRUCTURE property
	 */
	public void setWITHINSTRUCTURE(String aWITHINSTRUCTURE) {
		WITHINSTRUCTURE = aWITHINSTRUCTURE;
	}

	/**
	 * Access method for the YEARPERMITAPPLIED property.
	 * 
	 * @return the current value of the YEARPERMITAPPLIED property
	 */
	public String getYEARPERMITAPPLIED() {
		return YEARPERMITAPPLIED;
	}

	/**
	 * Sets the value of the YEARPERMITAPPLIED property.
	 * 
	 * @param aYEARPERMITAPPLIED
	 *            the new value of the YEARPERMITAPPLIED property
	 */
	public void setYEARPERMITAPPLIED(String aYEARPERMITAPPLIED) {
		YEARPERMITAPPLIED = aYEARPERMITAPPLIED;
	}

	/**
	 * Access method for the ZONECURRENT property.
	 * 
	 * @return the current value of the ZONECURRENT property
	 */
	public String getZONECURRENT() {
		return ZONECURRENT;
	}

	/**
	 * Sets the value of the ZONECURRENT property.
	 * 
	 * @param aZONECURRENT
	 *            the new value of the ZONECURRENT property
	 */
	public void setZONECURRENT(String aZONECURRENT) {
		ZONECURRENT = aZONECURRENT;
	}

	/**
	 * Access method for the ZONEPERMIT property.
	 * 
	 * @return the current value of the ZONEPERMIT property
	 */
	public String getZONEPERMIT() {
		return ZONEPERMIT;
	}

	/**
	 * Sets the value of the ZONEPERMIT property.
	 * 
	 * @param aZONEPERMIT
	 *            the new value of the ZONEPERMIT property
	 */
	public void setZONEPERMIT(String aZONEPERMIT) {
		ZONEPERMIT = aZONEPERMIT;
	}

	/**
	 * @return
	 */
	public SiteSetback getSetback() {
		return setback;
	}

	/**
	 * @param setback
	 */
	public void setSetback(SiteSetback setback) {
		this.setback = setback;
	}

}