/*
 * Created on Oct 18, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package elms.app.sitedata;

/**
 * @author Shekhar
 * 
 *         To change the template for this generated type comment go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class SiteUse {
	protected int siteUseId;
	protected String description;
	protected int factor;
	protected String surveyType;

	public SiteUse(int aSiteUseId, String aDescription, int aFactor, String aSurveyType) {
		this.siteUseId = aSiteUseId;
		this.description = aDescription;
		this.factor = aFactor;
		this.surveyType = aSurveyType;
	}

	/**
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return
	 */
	public int getFactor() {
		return factor;
	}

	/**
	 * @return
	 */
	public int getSiteUseId() {
		return siteUseId;
	}

	/**
	 * @return
	 */
	public String getSurveyType() {
		return surveyType;
	}

	/**
	 * @param string
	 */
	public void setDescription(String string) {
		description = string;
	}

	/**
	 * @param i
	 */
	public void setFactor(int i) {
		factor = i;
	}

	/**
	 * @param i
	 */
	public void setSiteUseId(int i) {
		siteUseId = i;
	}

	/**
	 * @param string
	 */
	public void setSurveyType(String string) {
		surveyType = string;
	}

}
