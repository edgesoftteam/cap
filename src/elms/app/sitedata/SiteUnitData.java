/*
 * Created on Sep 29, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package elms.app.sitedata;

import org.apache.log4j.Logger;

/**
 * @author Shekhar
 * 
 *         To change the template for this generated type comment go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class SiteUnitData {
	static Logger logger = Logger.getLogger(SiteUnitData.class.getName());

	protected int unitId;
	protected long structureId;
	protected String unit;
	protected String floor;
	protected int useType;
	protected String streetNbr;
	protected String streetId;
	protected String streetMod;
	protected String actNbr;
	protected String sqFootage;
	protected String mezzFlag;
	protected String mezzArea;
	protected int mezzUseType;

	public SiteUnitData() {
		unitId = 0;
		structureId = 0;
	}

	public SiteUnitData(long aStructureId) {
		unitId = 0;
		structureId = aStructureId;
	}

	/**
	 * @return
	 */

	/**
	 * @return
	 */
	public String getActNbr() {
		return actNbr;
	}

	/**
	 * @return
	 */
	public String getFloor() {
		return floor;
	}

	/**
	 * @return
	 */
	public String getMezzFlag() {
		return mezzFlag;
	}

	/**
	 * @return
	 */
	public String getMezzArea() {
		return mezzArea;
	}

	/**
	 * @return
	 */
	public int getMezzUseType() {
		return mezzUseType;
	}

	/**
	 * @return
	 */
	public String getSqFootage() {
		return sqFootage;
	}

	/**
	 * @return
	 */
	public String getStreetId() {
		return streetId;
	}

	/**
	 * @return
	 */
	public String getStreetMod() {
		return streetMod;
	}

	/**
	 * @return
	 */
	public String getStreetNbr() {
		return streetNbr;
	}

	/**
	 * @return
	 */
	public long getStructureId() {
		return structureId;
	}

	/**
	 * @return
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * @return
	 */
	public int getUnitId() {
		return unitId;
	}

	/**
	 * @return
	 */
	public int getUseType() {
		return useType;
	}

	/**
	 * @param string
	 */
	public void setActNbr(String string) {
		actNbr = string;
	}

	/**
	 * @param string
	 */
	public void setFloor(String string) {
		floor = string;
	}

	/**
	 * @param string
	 */
	public void setMezzFlag(String string) {
		mezzFlag = string;
	}

	/**
	 * @param string
	 */
	public void setMezzArea(String string) {
		mezzArea = string;
	}

	/**
	 * @param i
	 */
	public void setMezzUseType(int i) {
		mezzUseType = i;
	}

	/**
	 * @param string
	 */
	public void setSqFootage(String string) {
		sqFootage = string;
	}

	/**
	 * @param string
	 */
	public void setStreetId(String string) {
		streetId = string;
	}

	/**
	 * @param string
	 */
	public void setStreetMod(String string) {
		streetMod = string;
	}

	/**
	 * @param string
	 */
	public void setStreetNbr(String string) {
		streetNbr = string;
	}

	/**
	 * @param i
	 */
	public void setStructureId(long i) {
		structureId = i;
	}

	/**
	 * @param string
	 */
	public void setUnit(String string) {
		unit = string;
	}

	/**
	 * @param i
	 */
	public void setUnitId(int i) {
		unitId = i;
	}

	/**
	 * @param i
	 */
	public void setUseType(int i) {
		useType = i;
	}

}