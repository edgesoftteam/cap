package elms.app.inspection;

import org.apache.struts.action.ActionForm;

public class InspectionLibraryRecord extends ActionForm {

	/**
	 * Member variable declaration
	 */

	protected String check;
	protected String title;
	protected String comment;

	public InspectionLibraryRecord() {
		this.check = "";
		this.title = "";
		this.comment = "";
	}

	public InspectionLibraryRecord(String check, String title, String comment) {
		this.check = check;
		this.title = title;
		this.comment = comment;
	}

	/**
	 * Gets the check
	 * 
	 * @return Returns a String
	 */
	public String getCheck() {
		return check;
	}

	/**
	 * Sets the check
	 * 
	 * @param check
	 *            The check to set
	 */
	public void setCheck(String check) {
		this.check = check;
	}

	/**
	 * Gets the title
	 * 
	 * @return Returns a String
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title
	 * 
	 * @param title
	 *            The title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the comment
	 * 
	 * @return Returns a String
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * Sets the comment
	 * 
	 * @param comment
	 *            The comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

}