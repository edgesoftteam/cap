/*
 * Created on Sep 26, 2003
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package elms.app.pw;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Shekhar
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CallLogDetail implements java.io.Serializable {

    //Common Request Fields
    protected ActivityDetail callRequest;
    protected ActivityDetail[] serviceRequest;

    /**
     * @return
     */
    public ActivityDetail getCallRequest() {
        return callRequest;
    }

    /**
     * @param detail
     */
    public void setCallRequest(ActivityDetail detail) {
        callRequest = detail;
    }

    /**
     * @return
     */
    public ActivityDetail[] getServiceRequest() {
        return serviceRequest;
    }

    /**
     * @param details
     */
    public void setServiceRequest(ActivityDetail[] details) {
        serviceRequest = details;
    }

    /**
     * @param serviceRequests
     */
    public void setServiceRequest(List activities) {
        if (activities == null) activities = new ArrayList();
        ActivityDetail[] activityDetailArray = new ActivityDetail[activities.size()];
        Iterator iter = activities.iterator();
        int index = 0;
        while (iter.hasNext()) {
            activityDetailArray[index] = (ActivityDetail) iter.next();
            index++;
        }
        this.serviceRequest = (ActivityDetail[]) activityDetailArray;
    }

}
