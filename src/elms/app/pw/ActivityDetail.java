/*
 * Created on Sep 26, 2003
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package elms.app.pw;

import elms.app.finance.ActivityFinance;

import java.util.List;

/**
 * @author Shekhar
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ActivityDetail implements java.io.Serializable {

    //Common Request Fields
    protected String selected; // For screens where user can select a checkbox
    protected String activityId;
    protected String activityNbr;
    protected String type;
    protected String actTypeDesc;
    protected String subType;
    protected String responsibility;
    protected String supervisor;
    protected String enteredTime;
    protected String description;
    protected String status;
    protected String priority;
    protected String repairClass;
    protected String location;
    protected String workType;
    protected String estStartDate;
    protected String estCompletionDate;
    protected String completionDate;
    protected String action;

    protected String firstName;
    protected String lastName;
    protected String streetNbr;
    protected String streetFraction;
    protected String streetId;
    protected String unitNbr;
    protected String city;
    protected String state;
    protected String zip;
    protected String phoneNbr;
    protected boolean riskManagement;
    protected boolean claimForm;

    // Traffic Signal Fields
    protected String trafficSignalLocation;

    // Sewer Request Fields
    protected String incidentDate;
    protected String restrictionCause;
    protected String lineSize;
    protected String districtNbr;
    protected String lineType;
    protected String maintenanceCycle;
    protected String lastMaintainedDate;
    protected String nextMaintenanceDate;
    protected boolean damage;
    protected String damageNotes;

    //Common Lists for Activities
    protected ActivityFinance activityFinance;
    protected List activityTeam;
    protected List comments;
    protected List materials;
    // lookup lists
    protected List subTypes;
    protected List supervisors;
    protected List trafficSignalLocations;

    /**
     * @return
     */
    public String getAction() {
        return action;
    }

    /**
     * @return
     */
    public ActivityFinance getActivityFinance() {
        return activityFinance;
    }

    /**
     * @return
     */
    public String getActivityId() {
        return activityId;
    }

    /**
     * @return
     */
    public String getActivityNbr() {
        return activityNbr;
    }

    /**
     * @return
     */
    public List getActivityTeam() {
        return activityTeam;
    }

    /**
     * @return
     */
    public String getCity() {
        return city;
    }

    /**
     * @return
     */
    public boolean isClaimForm() {
        return claimForm;
    }

    /**
     * @return
     */
    public List getComments() {
        return comments;
    }

    /**
     * @return
     */
    public String getCompletionDate() {
        return completionDate;
    }

    /**
     * @return
     */
    public boolean isDamage() {
        return damage;
    }

    /**
     * @return
     */
    public String getDamageNotes() {
        return damageNotes;
    }

    /**
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return
     */
    public String getDistrictNbr() {
        return districtNbr;
    }

    /**
     * @return
     */
    public String getEnteredTime() {
        return enteredTime;
    }

    /**
     * @return
     */
    public String getEstCompletionDate() {
        return estCompletionDate;
    }

    /**
     * @return
     */
    public String getEstStartDate() {
        return estStartDate;
    }

    /**
     * @return
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @return
     */
    public String getIncidentDate() {
        return incidentDate;
    }

    /**
     * @return
     */
    public String getLastMaintainedDate() {
        return lastMaintainedDate;
    }

    /**
     * @return
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @return
     */
    public String getLineSize() {
        return lineSize;
    }

    /**
     * @return
     */
    public String getLineType() {
        return lineType;
    }

    /**
     * @return
     */
    public String getLocation() {
        return location;
    }

    /**
     * @return
     */
    public String getMaintenanceCycle() {
        return maintenanceCycle;
    }

    /**
     * @return
     */
    public List getMaterials() {
        return materials;
    }

    /**
     * @return
     */
    public String getNextMaintenanceDate() {
        return nextMaintenanceDate;
    }

    /**
     * @return
     */
    public String getPhoneNbr() {
        return phoneNbr;
    }

    /**
     * @return
     */
    public String getPriority() {
        return priority;
    }

    /**
     * @return
     */
    public String getRepairClass() {
        return repairClass;
    }

    /**
     * @return
     */
    public String getResponsibility() {
        return responsibility;
    }

    /**
     * @return
     */
    public String getRestrictionCause() {
        return restrictionCause;
    }

    /**
     * @return
     */
    public boolean isRiskManagement() {
        return riskManagement;
    }

    /**
     * @return
     */
    public String getState() {
        return state;
    }

    /**
     * @return
     */
    public String getStatus() {
        return status;
    }

    /**
     * @return
     */
    public String getStreetFraction() {
        return streetFraction;
    }

    /**
     * @return
     */
    public String getStreetId() {
        return streetId;
    }

    /**
     * @return
     */
    public String getStreetNbr() {
        return streetNbr;
    }

    /**
     * @return
     */
    public String getSubType() {
        return subType;
    }

    /**
     * @return
     */
    public String getSupervisor() {
        return supervisor;
    }

    /**
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     * @return
     */
    public String getUnitNbr() {
        return unitNbr;
    }

    /**
     * @return
     */
    public String getWorkType() {
        return workType;
    }

    /**
     * @return
     */
    public String getZip() {
        return zip;
    }

    /**
     * @param string
     */
    public void setAction(String string) {
        action = string;
    }

    /**
     * @param finance
     */
    public void setActivityFinance(ActivityFinance finance) {
        activityFinance = finance;
    }

    /**
     * @param string
     */
    public void setActivityId(String string) {
        activityId = string;
    }

    /**
     * @param string
     */
    public void setActivityNbr(String string) {
        activityNbr = string;
    }

    /**
     * @param list
     */
    public void setActivityTeam(List list) {
        activityTeam = list;
    }

    /**
     * @param string
     */
    public void setCity(String string) {
        city = string;
    }

    /**
     * @param b
     */
    public void setClaimForm(boolean b) {
        claimForm = b;
    }

    /**
     * @param list
     */
    public void setComments(List list) {
        comments = list;
    }

    /**
     * @param string
     */
    public void setCompletionDate(String string) {
        completionDate = string;
    }

    /**
     * @param b
     */
    public void setDamage(boolean b) {
        damage = b;
    }

    /**
     * @param string
     */
    public void setDamageNotes(String string) {
        damageNotes = string;
    }

    /**
     * @param string
     */
    public void setDescription(String string) {
        description = string;
    }

    /**
     * @param string
     */
    public void setDistrictNbr(String string) {
        districtNbr = string;
    }

    /**
     * @param string
     */
    public void setEnteredTime(String string) {
        enteredTime = string;
    }

    /**
     * @param string
     */
    public void setEstCompletionDate(String string) {
        estCompletionDate = string;
    }

    /**
     * @param string
     */
    public void setEstStartDate(String string) {
        estStartDate = string;
    }

    /**
     * @param string
     */
    public void setFirstName(String string) {
        firstName = string;
    }

    /**
     * @param string
     */
    public void setIncidentDate(String string) {
        incidentDate = string;
    }

    /**
     * @param string
     */
    public void setLastMaintainedDate(String string) {
        lastMaintainedDate = string;
    }

    /**
     * @param string
     */
    public void setLastName(String string) {
        lastName = string;
    }

    /**
     * @param string
     */
    public void setLineSize(String string) {
        lineSize = string;
    }

    /**
     * @param string
     */
    public void setLineType(String string) {
        lineType = string;
    }

    /**
     * @param string
     */
    public void setLocation(String string) {
        location = string;
    }

    /**
     * @param string
     */
    public void setMaintenanceCycle(String string) {
        maintenanceCycle = string;
    }

    /**
     * @param list
     */
    public void setMaterials(List list) {
        materials = list;
    }

    /**
     * @param string
     */
    public void setNextMaintenanceDate(String string) {
        nextMaintenanceDate = string;
    }

    /**
     * @param string
     */
    public void setPhoneNbr(String string) {
        phoneNbr = string;
    }

    /**
     * @param string
     */
    public void setPriority(String string) {
        priority = string;
    }

    /**
     * @param string
     */
    public void setRepairClass(String string) {
        repairClass = string;
    }

    /**
     * @param string
     */
    public void setResponsibility(String string) {
        responsibility = string;
    }

    /**
     * @param string
     */
    public void setRestrictionCause(String string) {
        restrictionCause = string;
    }

    /**
     * @param b
     */
    public void setRiskManagement(boolean b) {
        riskManagement = b;
    }

    /**
     * @param string
     */
    public void setState(String string) {
        state = string;
    }

    /**
     * @param string
     */
    public void setStatus(String string) {
        status = string;
    }

    /**
     * @param string
     */
    public void setStreetFraction(String string) {
        streetFraction = string;
    }

    /**
     * @param string
     */
    public void setStreetId(String string) {
        streetId = string;
    }

    /**
     * @param string
     */
    public void setStreetNbr(String string) {
        streetNbr = string;
    }

    /**
     * @param string
     */
    public void setSubType(String string) {
        subType = string;
    }

    /**
     * @param string
     */
    public void setSupervisor(String string) {
        supervisor = string;
    }

    /**
     * @param string
     */
    public void setType(String string) {
        type = string;
    }

    /**
     * @param string
     */
    public void setUnitNbr(String string) {
        unitNbr = string;
    }

    /**
     * @param string
     */
    public void setWorkType(String string) {
        workType = string;
    }

    /**
     * @param string
     */
    public void setZip(String string) {
        zip = string;
    }


    /**
     * @return
     */
    public String getActTypeDesc() {
        return actTypeDesc;
    }

    /**
     * @return
     */
    public String getSelected() {
        return selected;
    }

    /**
     * @param string
     */
    public void setActTypeDesc(String string) {
        actTypeDesc = string;
    }

    /**
     * @param string
     */
    public void setSelected(String string) {
        selected = string;
    }

    /**
     * @return
     */
    public List getSubTypes() {
        return subTypes;
    }

    /**
     * @return
     */
    public List getSupervisors() {
        return supervisors;
    }

    /**
     * @param list
     */
    public void setSubTypes(List list) {
        subTypes = list;
    }

    /**
     * @param list
     */
    public void setSupervisors(List list) {
        supervisors = list;
    }

    /**
     * @return
     */
    public List getTrafficSignalLocations() {
        return trafficSignalLocations;
    }

    /**
     * @param list
     */
    public void setTrafficSignalLocations(List list) {
        trafficSignalLocations = list;
    }

    /**
     * @return
     */
    public String getTrafficSignalLocation() {
        return trafficSignalLocation;
    }

    /**
     * @param string
     */
    public void setTrafficSignalLocation(String string) {
        trafficSignalLocation = string;
    }

}
