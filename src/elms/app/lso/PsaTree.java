package elms.app.lso;

import org.apache.log4j.Logger;

/**
 * @author Anand Belaguly
 */
public class PsaTree extends ObcTree {

	static Logger logger = Logger.getLogger(PsaTree.class.getName());
	protected String lsoType = "";
	protected String status = "";

	/**
	 * 3CD0322702D9
	 */
	public PsaTree() {

	}

	public PsaTree(String nodeId, String nodeLevel, String nodeText, String nodeLink, String nodeAlt, String lsoType, String status, long id, String type) {
		super(nodeId, nodeLevel, nodeText, nodeLink, nodeAlt, id, type);
		this.lsoType = lsoType;
		this.status = status;
	}

	/**
	 * Returns the lsoType.
	 * 
	 * @return String
	 */
	public String getLsoType() {
		return lsoType;
	}

	/**
	 * Sets the lsoType.
	 * 
	 * @param lsoType
	 *            The lsoType to set
	 */
	public void setLsoType(String lsoType) {
		this.lsoType = lsoType;
	}

	/**
	 * Gets the status
	 * 
	 * @return Returns a String
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status
	 * 
	 * @param status
	 *            The status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

}