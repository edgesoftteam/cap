//Source file: C:\\datafile\\source\\elms\\app\\lso\\LandApn.java

package elms.app.lso;

/**
 * @author Shekhar Jain
 */
public class LandApn extends Apn {

	public LandApn(String parcelNumber, Owner owner, String active) {
		super(parcelNumber, owner, active);
	}

	/**
	 * 3CCDDF8801AA
	 */
	public LandApn() {

	}

}
