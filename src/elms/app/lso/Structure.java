package elms.app.lso;

import java.util.List;

/**
 * @author Shekhar Jain
 */
public class Structure extends Lso {
	protected int landId;
	protected StructureDetails structureDetails;
	protected List structureAddress;
	protected List occupancyApn;
	protected StructureSiteData structureSiteData;

	/**
	 * @param structureDetails
	 * @param structureAddress
	 * @param structureOwner
	 * @param structureHold
	 * @param structureCondition
	 * @param structureAttachment
	 * @param structureSiteData
	 *            3CCD9842013B
	 */
	public Structure(int structureId, int landId, StructureDetails structureDetails, List structureAddress, List structureHold, List structureCondition, List structureAttachment, StructureSiteData structureSiteData, List structureComment) {
		super(structureId, structureHold, structureCondition, structureAttachment, structureComment);
		this.structureDetails = structureDetails;
		this.structureAddress = structureAddress;
		this.structureSiteData = structureSiteData;
		this.landId = landId;
	}

	public Structure(int structureId, int landId, StructureDetails structureDetails, List structureAddress) {
		super(structureId);
		this.structureDetails = structureDetails;
		this.structureAddress = structureAddress;
		this.landId = landId;
	}

	/**
	 * 3CCD983C0359
	 */
	public Structure() {
		super();
	}

	/**
	 * Gets the structureDetails
	 * 
	 * @return Returns a StructureDetails
	 */
	public StructureDetails getStructureDetails() {
		return structureDetails;
	}

	/**
	 * Sets the structureDetails
	 * 
	 * @param structureDetails
	 *            The structureDetails to set
	 */
	public void setStructureDetails(StructureDetails structureDetails) {
		this.structureDetails = structureDetails;
	}

	/**
	 * Gets the structureAddress
	 * 
	 * @return Returns a List
	 */
	public List getStructureAddress() {
		return structureAddress;
	}

	/**
	 * Sets the structureAddress
	 * 
	 * @param structureAddress
	 *            The structureAddress to set
	 */
	public void setStructureAddress(List structureAddress) {
		this.structureAddress = structureAddress;
	}

	/**
	 * Gets the structureSiteData
	 * 
	 * @return Returns a StructureSiteData
	 */
	public StructureSiteData getStructureSiteData() {
		return structureSiteData;
	}

	/**
	 * Sets the structureSiteData
	 * 
	 * @param structureSiteData
	 *            The structureSiteData to set
	 */
	public void setStructureSiteData(StructureSiteData structureSiteData) {
		this.structureSiteData = structureSiteData;
	}

	/**
	 * Gets the landId
	 * 
	 * @return Returns a int
	 */
	public int getLandId() {
		return landId;
	}

	/**
	 * Sets the landId
	 * 
	 * @param landId
	 *            The landId to set
	 */
	public void setLandId(int landId) {
		this.landId = landId;
	}

	/**
	 * @return
	 */
	public List getOccupancyApn() {
		return occupancyApn;
	}

	/**
	 * @param list
	 */
	public void setOccupancyApn(List list) {
		occupancyApn = list;
	}

}