package elms.app.lso;

/**
 * @author shekhar
 * 
 *         To change this generated comment edit the template variable "typecomment": Window>Preferences>Java>Templates. To enable and disable the creation of type comments go to Window>Preferences>Java>Code Generation.
 */
public class StreetModifier {
	protected String id;
	protected String streetModifier;

	public StreetModifier(String streetModifier) {
		this.id = streetModifier;
		this.streetModifier = streetModifier;
	}

	/**
	 * Returns the id.
	 * 
	 * @return String
	 */
	public String getId() {
		return id;
	}

	/**
	 * Returns the streetModifier.
	 * 
	 * @return String
	 */
	public String getStreetModifier() {
		return streetModifier;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            The id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Sets the streetModifier.
	 * 
	 * @param streetModifier
	 *            The streetModifier to set
	 */
	public void setStreetModifier(String streetModifier) {
		this.streetModifier = streetModifier;
	}

}
