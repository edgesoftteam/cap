package elms.app.lso;

import java.util.Date;

import org.apache.log4j.Logger;

/**
 * The Assessor parcel number (in custom format)
 * 
 * @author shekhar jain (shekhar@edgesoftinc.com)
 * @author Anand Belaguly (anand@edgesoftinc.com) last updated : Dec 11, 2003 - Anand Belaguly - formatted the source and added comments
 */
public class Apn {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(Apn.class.getName());

	/**
	 * The Assessor parcel number
	 */
	protected String apn;
	/**
	 * The Assessor parcel number
	 */
	protected String oldApn;
	/**
	 * The owner for this APN
	 */
	protected Owner owner;
	/**
	 * is this APN active ?
	 */
	protected String active;
	/**
	 * The last updated date
	 */
	protected Date lastUpdated;
	/**
	 * user id of the udpated user
	 */
	protected int updatedBy;

	/**
	 * mini constructor
	 * 
	 * @param parcelNumber
	 * @param owner
	 * @param active
	 */
	public Apn(String parcelNumber, Owner owner, String active) {
		this.apn = parcelNumber;
		this.owner = owner;
		this.active = active;
	}

	/**
	 * default constructor
	 */
	public Apn() {
		owner = new Owner();
		lastUpdated = new Date();

	}

	/**
	 * fully loaded constructor
	 * 
	 * @param apn
	 * @param ownerName
	 * @param ownerId
	 * @param streetNbr
	 * @param streetMod
	 * @param streetName
	 * @param unit
	 * @param city
	 * @param state
	 * @param zip
	 * @param zip4
	 * @param foreignFlag
	 * @param line1
	 * @param line2
	 * @param line3
	 * @param line4
	 * @param country
	 * @param email
	 * @param phone
	 * @param fax
	 */
	public Apn(String apn, String ownerName, int ownerId, int streetNbr, String streetMod, String streetName, String unit, String city, String state, String zip, String zip4, String foreignFlag, String line1, String line2, String line3, String line4, String country, String email, String phone, String fax) {
		Street street = new Street();
		street.setStreetName(streetName);

		Address localAddress = new Address(streetNbr, streetMod, street, city, state, zip, zip4);
		ForeignAddress foreignAddress = new ForeignAddress(line1, line2, line3, line4, country);

		this.apn = apn;

		owner = new Owner(ownerId, ownerName, localAddress, foreignFlag, foreignAddress, phone, fax, email);
	}

	/**
	 * Access method for the owner property.
	 * 
	 * @return the current value of the owner property
	 */
	public Owner getOwner() {
		return owner;
	}

	/**
	 * Sets the value of the owner property.
	 * 
	 * @param aOwner
	 *            the new value of the owner property
	 */
	public void setOwner(Owner aOwner) {
		owner = aOwner;
	}

	/**
	 * Gets the parcelNumber
	 * 
	 * @return Returns a String
	 */
	public String getParcelNumber() {
		return apn;
	}

	/**
	 * Sets the parcelNumber
	 * 
	 * @param parcelNumber
	 *            The parcelNumber to set
	 */
	public void setParcelNumber(String parcelNumber) {
		this.apn = parcelNumber;
	}

	/**
	 * Gets the active
	 * 
	 * @return Returns a String
	 */
	public String getActive() {
		return active;
	}

	/**
	 * Sets the active
	 * 
	 * @param active
	 *            The active to set
	 */
	public void setActive(String active) {
		this.active = active;
	}

	/**
	 * Gets the lastUpdated
	 * 
	 * @return Returns a Date
	 */
	public Date getLastUpdated() {
		return lastUpdated;
	}

	/**
	 * Sets the lastUpdated
	 * 
	 * @param lastUpdated
	 *            The lastUpdated to set
	 */
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	/**
	 * Gets the updatedBy
	 * 
	 * @return Returns a int
	 */
	public int getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Sets the updatedBy
	 * 
	 * @param updatedBy
	 *            The updatedBy to set
	 */
	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Gets the apn
	 * 
	 * @return Returns a String
	 */
	public String getApn() {
		return apn;
	}

	/**
	 * Sets the apn
	 * 
	 * @param apn
	 *            The apn to set
	 */
	public void setApn(String apn) {
		this.apn = apn;
	}

	/**
	 * @return
	 */
	public String getOldApn() {
		return oldApn;
	}

	/**
	 * @param string
	 */
	public void setOldApn(String string) {
		oldApn = string;
	}

}