package elms.app.lso;

/**
 * @author Shekhar Jain
 */
public class OwnerApnAddress {
	protected int lsoId;
	protected String ownerName;
	protected String apn;
	protected String address;

	protected Address siteAddress;
	protected Address mailingAddress;
	protected ForeignAddress foreignMailingAddress;
	protected String foreignAdd;

	/**
	 * Gets the lsoId
	 * 
	 * @return Returns a int
	 */
	public int getLsoId() {
		return lsoId;
	}

	/**
	 * Sets the lsoId
	 * 
	 * @param lsoId
	 *            The lsoId to set
	 */
	public void setLsoId(int lsoId) {
		this.lsoId = lsoId;
	}

	/**
	 * Gets the ownerName
	 * 
	 * @return Returns a String
	 */
	public String getOwnerName() {
		return ownerName;
	}

	/**
	 * Sets the ownerName
	 * 
	 * @param ownerName
	 *            The ownerName to set
	 */
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	/**
	 * Gets the apn
	 * 
	 * @return Returns a String
	 */
	public String getApn() {
		return apn;
	}

	/**
	 * Sets the apn
	 * 
	 * @param apn
	 *            The apn to set
	 */
	public void setApn(String apn) {
		this.apn = apn;
	}

	/**
	 * Gets the address
	 * 
	 * @return Returns a String
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the address
	 * 
	 * @param address
	 *            The address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Gets the siteAddress
	 * 
	 * @return Returns a Address
	 */
	public Address getSiteAddress() {
		return siteAddress;
	}

	/**
	 * Sets the siteAddress
	 * 
	 * @param siteAddress
	 *            The siteAddress to set
	 */
	public void setSiteAddress(Address siteAddress) {
		this.siteAddress = siteAddress;
	}

	/**
	 * Gets the mailingAddress
	 * 
	 * @return Returns a Address
	 */
	public Address getMailingAddress() {
		return mailingAddress;
	}

	/**
	 * Sets the mailingAddress
	 * 
	 * @param mailingAddress
	 *            The mailingAddress to set
	 */
	public void setMailingAddress(Address mailingAddress) {
		this.mailingAddress = mailingAddress;
	}

	/**
	 * Gets the foreignMailingAddress
	 * 
	 * @return Returns a ForeignAddress
	 * */
	public ForeignAddress getForeignMailingAddress() {
		return foreignMailingAddress;
	}

	/**
	 * Sets the foreignMailingAddress
	 * 
	 * @param foreignMailingAddress
	 *            The foreignMailingAddress to set
	 */

	public void setForeignMailingAddress(ForeignAddress foreignMailingAddress) {
		this.foreignMailingAddress = foreignMailingAddress;
	}

	// sets ang gets foreign address

	public void setForeignAdd(String foreignAdd) {
		this.foreignAdd = foreignAdd;
	}

	public String getForeignAdd() {
		return foreignAdd;
	}

}
