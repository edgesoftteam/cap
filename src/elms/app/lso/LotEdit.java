package elms.app.lso;

import java.util.Calendar;
import java.util.Date;

import elms.security.User;

/**
 * @author Shekhar Jain
 */
public class LotEdit {
	protected String lotId;
	protected String parentId;
	protected String delete;
	protected String active;
	protected String type;
	protected String description;
	protected User createdBy;
	protected Calendar created;
	protected User updatedBy;
	protected Calendar updated;

	/**
	 * Constructor for OccupancySummary.
	 */
	public LotEdit() {

	}

	public LotEdit(String lotId) {

	}

	/**
	 * Returns the active.
	 * 
	 * @return String
	 */
	public String getActive() {
		return active;
	}

	/**
	 * Returns the delete.
	 * 
	 * @return String
	 */
	public String getDelete() {
		return delete;
	}

	/**
	 * Returns the description.
	 * 
	 * @return String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Returns the type.
	 * 
	 * @return String
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the active.
	 * 
	 * @param active
	 *            The active to set
	 */
	public void setActive(String active) {
		this.active = active;
	}

	/**
	 * Sets the delete.
	 * 
	 * @param delete
	 *            The delete to set
	 */
	public void setDelete(String delete) {
		this.delete = delete;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Sets the type.
	 * 
	 * @param type
	 *            The type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Returns the lotId.
	 * 
	 * @return String
	 */
	public String getLotId() {
		return lotId;
	}

	/**
	 * Sets the lotId.
	 * 
	 * @param lotId
	 *            The lotId to set
	 */
	public void setLotId(String lotId) {
		this.lotId = lotId;
	}

	/**
	 * Returns the createdBy.
	 * 
	 * @return User
	 */
	public User getCreatedBy() {
		return createdBy;
	}

	/**
	 * Returns the updatedBy.
	 * 
	 * @return User
	 */
	public User getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Sets the createdBy.
	 * 
	 * @param createdBy
	 *            The createdBy to set
	 */
	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Sets the updatedBy.
	 * 
	 * @param updatedBy
	 *            The updatedBy to set
	 */
	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Returns the created.
	 * 
	 * @return Calendar
	 */
	public Calendar getCreated() {
		return created;
	}

	/**
	 * Returns the updated.
	 * 
	 * @return Calendar
	 */
	public Calendar getUpdated() {
		return updated;
	}

	/**
	 * Sets the created.
	 * 
	 * @param created
	 *            The created to set
	 */
	public void setCreated(Calendar created) {
		this.created = created;
	}

	/**
	 * Sets the created
	 * 
	 * @param created
	 *            The created to set
	 */
	public void setCreated(Date created) {
		if (created == null)
			this.created = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(created);
			this.created = calendar;
		}
	}

	/**
	 * Sets the updated.
	 * 
	 * @param updated
	 *            The updated to set
	 */
	public void setUpdated(Calendar updated) {
		this.updated = updated;
	}

	/**
	 * Sets the updated
	 * 
	 * @param updated
	 *            The updated to set
	 */
	public void setUpdated(Date updated) {
		if (updated == null)
			this.updated = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(updated);
			this.updated = calendar;
		}
	}

	/**
	 * Returns the parentId.
	 * 
	 * @return String
	 */
	public String getParentId() {
		return parentId;
	}

	/**
	 * Sets the parentId.
	 * 
	 * @param parentId
	 *            The parentId to set
	 */
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

}
