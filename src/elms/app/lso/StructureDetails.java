//Source file: C:\\datafile\\source\\elms\\app\\lso\\StructureDetails.java

package elms.app.lso;

import java.util.List;

/**
 * @author Shekhar Jain
 */
public class StructureDetails extends Details {
	protected String totalFloors;

	/**
	 * @param totalFloors
	 *            3CCDBCA7012E
	 */
	public StructureDetails(String alias, String description, List use, String active, String totalFloors, String label) {
		super(alias, description, use, active, label);
		this.totalFloors = totalFloors;

	}

	/**
	 * 3CCDBCA0012E
	 */
	public StructureDetails() {

	}

	/**
	 * Access method for the totalFloors property.
	 * 
	 * @return the current value of the totalFloors property
	 */
	public String getTotalFloors() {
		return totalFloors;
	}

	/**
	 * Sets the value of the totalFloors property.
	 * 
	 * @param aTotalFloors
	 *            the new value of the totalFloors property
	 */
	public void setTotalFloors(String aTotalFloors) {
		totalFloors = aTotalFloors;
	}
}
