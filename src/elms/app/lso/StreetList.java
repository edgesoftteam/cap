//Source file: C:\\datafile\\source\\elms\\app\\lso\\StreetList.java

package elms.app.lso;

public class StreetList {
	public String PRE_DIR;
	public String STR_NAME;
	public String STR_TYPE;
	public Integer STREET_ID;
	public String SUF_DIR;

	/**
	 * 3CD032320220
	 */
	public StreetList() {

	}

	/**
	 * Access method for the PRE_DIR property.
	 * 
	 * @return the current value of the PRE_DIR property
	 */
	public String getPRE_DIR() {
		return PRE_DIR;
	}

	/**
	 * Sets the value of the PRE_DIR property.
	 * 
	 * @param aPRE_DIR
	 *            the new value of the PRE_DIR property
	 */
	public void setPRE_DIR(String aPRE_DIR) {
		PRE_DIR = aPRE_DIR;
	}

	/**
	 * Access method for the STR_NAME property.
	 * 
	 * @return the current value of the STR_NAME property
	 */
	public String getSTR_NAME() {
		return STR_NAME;
	}

	/**
	 * Sets the value of the STR_NAME property.
	 * 
	 * @param aSTR_NAME
	 *            the new value of the STR_NAME property
	 */
	public void setSTR_NAME(String aSTR_NAME) {
		STR_NAME = aSTR_NAME;
	}

	/**
	 * Access method for the STR_TYPE property.
	 * 
	 * @return the current value of the STR_TYPE property
	 */
	public String getSTR_TYPE() {
		return STR_TYPE;
	}

	/**
	 * Sets the value of the STR_TYPE property.
	 * 
	 * @param aSTR_TYPE
	 *            the new value of the STR_TYPE property
	 */
	public void setSTR_TYPE(String aSTR_TYPE) {
		STR_TYPE = aSTR_TYPE;
	}

	/**
	 * Access method for the STREET_ID property.
	 * 
	 * @return the current value of the STREET_ID property
	 */
	public Integer getSTREET_ID() {
		return STREET_ID;
	}

	/**
	 * Sets the value of the STREET_ID property.
	 * 
	 * @param aSTREET_ID
	 *            the new value of the STREET_ID property
	 */
	public void setSTREET_ID(Integer aSTREET_ID) {
		STREET_ID = aSTREET_ID;
	}

	/**
	 * Access method for the SUF_DIR property.
	 * 
	 * @return the current value of the SUF_DIR property
	 */
	public String getSUF_DIR() {
		return SUF_DIR;
	}

	/**
	 * Sets the value of the SUF_DIR property.
	 * 
	 * @param aSUF_DIR
	 *            the new value of the SUF_DIR property
	 */
	public void setSUF_DIR(String aSUF_DIR) {
		SUF_DIR = aSUF_DIR;
	}
}
