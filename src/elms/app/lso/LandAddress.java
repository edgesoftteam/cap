//Source file: C:\\datafile\\source\\elms\\app\\lso\\LandAddress.java

package elms.app.lso;

/**
 * @author Shekhar Jain
 */
public class LandAddress extends LsoAddress {

	public LandAddress() {
		super();
		this.lsoType = "L";
	}

	public LandAddress(int addressId, int lsoId, int streetNbr, String streetModifier, Street street, String city, String state, String zip, String zip4, String description, String primary, String active) {

		super(addressId, lsoId, "L", streetNbr, streetModifier, street, city, state, zip, zip4, description, primary, active);
	}
}
