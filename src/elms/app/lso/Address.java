package elms.app.lso;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.log4j.Logger;

/**
 * A Base class for most of the address related operations in OBC This class defines only some of the common qualifiers
 * 
 * @author shekhar jain (shekhar@edgesoftinc.com)
 * @author anand belaguly (anand@edgesoftinc.com) last updated : Dec 11, 2003 - Anand Belaguly - formatted the source and added comments
 */
@XmlRootElement
public class Address {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(Address.class.getName());

	/**
	 * The street number
	 */
	protected int streetNbr;
	/**
	 * The street modifiers (like 1/2,1/4 etc)
	 */
	protected String streetModifier;
	/**
	 * The street object itself
	 */
	protected Street street;
	/**
	 * The unit number, generally a number like 102, 103, etc
	 */
	protected String unit;
	/**
	 * The city of the address
	 */
	protected String city;
	/**
	 * The state of which the address belongs to
	 */
	protected String state;
	/**
	 * The zip code (used as a string because, it may be 5+4, or with special characters like -
	 */
	protected String zip;
	/**
	 * The +4 part of the zip
	 */
	protected String zip4 = "";

	/**
	 * The default constructor
	 */
	public Address() {
		street = new Street();
	}

	/**
	 * The loaded constructor
	 * 
	 * @param streetNbr
	 * @param streetModifier
	 * @param street
	 * @param city
	 * @param state
	 * @param zip
	 * @param zip4
	 */
	public Address(int streetNbr, String streetModifier, Street street, String city, String state, String zip, String zip4) {
		this.streetNbr = streetNbr;
		this.streetModifier = streetModifier;
		this.street = street;
		this.city = city;
		this.state = state;
		this.zip = zip;
		this.zip4 = zip4;
	}

	/**
	 * The mini constructor
	 * 
	 * @param streetNbr
	 * @param streetModifier
	 * @param street
	 * @param unit
	 */
	public Address(int streetNbr, String streetModifier, Street street, String unit) {
		this.streetNbr = streetNbr;
		this.streetModifier = streetModifier;
		this.street = street;
		this.unit = unit;
	}

	/**
	 * Gets the streetNbr
	 * 
	 * @return Returns a int
	 */
	public int getStreetNbr() {
		return streetNbr;
	}

	/**
	 * Sets the streetNbr
	 * 
	 * @param streetNbr
	 *            The streetNbr to set
	 */
	public void setStreetNbr(int streetNbr) {
		this.streetNbr = streetNbr;
	}

	/**
	 * Gets the streetModifier
	 * 
	 * @return Returns a String
	 */
	public String getStreetModifier() {
		return streetModifier;
	}

	/**
	 * Sets the streetModifier
	 * 
	 * @param streetModifier
	 *            The streetModifier to set
	 */
	public void setStreetModifier(String streetModifier) {
		this.streetModifier = streetModifier;
	}

	/**
	 * Gets the street
	 * 
	 * @return Returns a Street
	 */
	public Street getStreet() {
		return street;
	}

	/**
	 * Sets the street
	 * 
	 * @param street
	 *            The street to set
	 */
	public void setStreet(Street street) {
		this.street = street;
	}

	/**
	 * Gets the city
	 * 
	 * @return Returns a String
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the city
	 * 
	 * @param city
	 *            The city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Gets the state
	 * 
	 * @return Returns a String
	 */
	public String getState() {
		return state;
	}

	/**
	 * Sets the state
	 * 
	 * @param state
	 *            The state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * Gets the zip
	 * 
	 * @return Returns a String
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * Sets the zip
	 * 
	 * @param zip
	 *            The zip to set
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * Gets the zip4
	 * 
	 * @return Returns a String
	 */
	public String getZip4() {
		// if(zip4.equals("")) zip4="0";
		return zip4;
	}

	/**
	 * Sets the zip4
	 * 
	 * @param zip4
	 *            The zip4 to set
	 */
	public void setZip4(String zip4) {
		this.zip4 = zip4;
	}

	/**
	 * The toString method
	 * 
	 * @return
	 */
	public String toString() {
		return (this.streetNbr + ":" + this.streetModifier + ":" + this.street.getStreetName() + ":" + this.unit + ":" + this.street.getStreetId());
	}

	/**
	 * Gets the unit
	 * 
	 * @return Returns a String
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * Sets the unit
	 * 
	 * @param unit
	 *            The unit to set
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

}
