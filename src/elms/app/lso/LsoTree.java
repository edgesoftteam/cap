//Source file: C:\\datafile\\source\\elms\\app\\lso\\LandAgent.java

package elms.app.lso;

import java.io.Serializable;
import java.util.Map;

import org.apache.log4j.Logger;

/**
 * @author Anand Belaguly
 */
public class LsoTree extends ObcTree implements Serializable {

	static Logger logger = Logger.getLogger(LsoTree.class.getName());
	private Map map;

	/**
	 * 3CD0322702D9
	 */
	public LsoTree() {

	}

	public LsoTree(String nodeId, String nodeLevel, String nodeText, String nodeLink, String nodeAlt) {

		super(nodeId, nodeLevel, nodeText, nodeLink, nodeAlt);

	}

	public LsoTree(String nodeId, String nodeLevel, String nodeText, String nodeLink, String nodeAlt, long lsoId, String lsoType) {
		super(nodeId, nodeLevel, nodeText, nodeLink, nodeAlt);
	}

	public LsoTree(String nodeId, String nodeLevel, String nodeText, String nodeLink, String nodeAlt, long lsoId, String lsoType, Map map) {
		super(nodeId, nodeLevel, nodeText, nodeLink, nodeAlt, lsoId, lsoType);
		this.map = map;
	}

	/**
	 * Returns the map.
	 * 
	 * @return Map
	 */
	public Map getMap() {
		return map;
	}

	/**
	 * Sets the map.
	 * 
	 * @param map
	 *            The map to set
	 */
	public void setMap(Map map) {
		this.map = map;
	}

}