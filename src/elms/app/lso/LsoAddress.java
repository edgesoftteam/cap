package elms.app.lso;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import elms.security.User;
import elms.util.StringUtils;

@XmlRootElement
public class LsoAddress extends Address {
	protected int addressId;
	protected int lsoId;
	protected String lsoType;
	protected String description;
	protected String primary;
	protected String active;
	protected Date updated;
	protected User updatedBy;
	protected String unit;

	/**
	 * 3CD9720503A0
	 */
	public LsoAddress() {
		street = new Street();
	}
	

	public String getAddress() {
		return streetNbr + " " + StringUtils.nullReplaceWithEmpty(street.getStreetName()) + " " + StringUtils.nullReplaceWithEmpty(StringUtils.nullReplaceWithEmpty(unit));
	}

	/**
	 * 3CD9720503A0
	 */
	public LsoAddress(int addressId, int lsoId, String lsoType, int streetNbr, String streetModifier, Street street, String city, String state, String zip, String zip4, String description, String primary, String active) {
		this.addressId = addressId;
		this.lsoId = lsoId;
		this.lsoType = lsoType;
		this.streetNbr = streetNbr;
		this.streetModifier = streetModifier;
		this.street = street;
		this.city = city;
		this.state = state;
		this.zip = zip;
		this.zip4 = zip4;
		this.primary = primary;
		this.active = active;
		this.description = description;
	}

	public LsoAddress(int addressId, String description) {
		this.addressId = addressId;
		this.description = description;

	}

	/**
	 * @return Returns the unit.
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * @param unit
	 *            The unit to set.
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * Gets the addressId
	 * 
	 * @return Returns a int
	 */
	public int getAddressId() {
		return addressId;
	}

	/**
	 * Sets the addressId
	 * 
	 * @param addressId
	 *            The addressId to set
	 */
	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}

	/**
	 * Gets the lsoId
	 * 
	 * @return Returns a int
	 */
	public int getLsoId() {
		return lsoId;
	}

	/**
	 * Sets the lsoId
	 * 
	 * @param lsoId
	 *            The lsoId to set
	 */
	public void setLsoId(int lsoId) {
		this.lsoId = lsoId;
	}

	/**
	 * Gets the lsoType
	 * 
	 * @return Returns a String
	 */
	public String getLsoType() {
		return lsoType;
	}

	/**
	 * Sets the lsoType
	 * 
	 * @param lsoType
	 *            The lsoType to set
	 */
	public void setLsoType(String lsoType) {
		this.lsoType = lsoType;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the primary
	 * 
	 * @return Returns a String
	 */
	public String getPrimary() {
		return primary;
	}

	/**
	 * Sets the primary
	 * 
	 * @param primary
	 *            The primary to set
	 */
	public void setPrimary(String primary) {
		this.primary = primary;
	}

	/**
	 * Gets the active
	 * 
	 * @return Returns a String
	 */
	public String getActive() {
		return active;
	}

	/**
	 * Sets the active
	 * 
	 * @param active
	 *            The active to set
	 */
	public void setActive(String active) {
		this.active = active;
	}

	/**
	 * Gets the updated
	 * 
	 * @return Returns a Date
	 */
	public Date getUpdated() {
		return updated;
	}

	/**
	 * Sets the updated
	 * 
	 * @param updated
	 *            The updated to set
	 */
	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	/**
	 * Gets the updatedBy
	 * 
	 * @return Returns a User
	 */
	public User getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Sets the updatedBy
	 * 
	 * @param updatedBy
	 *            The updatedBy to set
	 */
	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String toString() {
		return (addressId + "--" + lsoId + "--" + lsoType + "--" + streetNbr + "--" + streetModifier + "--" + street + "--" + city + "--" + state + "--" + zip + "--" + zip4 + "--" + description + "--" + primary + "--" + active + "--" + updated + "--" + updatedBy);
	}

}
