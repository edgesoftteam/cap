package elms.app.lso;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * This describes the generic details for an address
 * 
 * @author shekhar Jain (shekhar@edgesoftinc.com)
 * @author anand belaguly (anand@edgesoftinc.com) last updated : Dec 11, 2003 - Anand Belaguly - formatted, removed redundant type cast, commented code.
 */
public class Details {
	/**
	 * The alias
	 */
	protected String alias;
	/**
	 * The description
	 */
	protected String description;
	/**
	 * The list of uses for this land
	 */
	protected List use;
	/**
	 * The active flag for this land.
	 */
	protected String active;

	/**
	 * The label
	 */
	protected String label;
	

	protected String householdSize;
	protected String bedroomSize;
	protected String grossRent;
	protected String utilityAllowance;
	protected String propertyManager;
	protected String restrictedUnits;
	

	/**
	 * @param alias
	 * @param description
	 * @param use
	 * @param active
	 * 
	 */
	public Details(String alias, String description, List use, String active, String label) {
		this.alias = alias;
		this.description = description;
		this.use = use;
		this.active = active;
		this.label = label;
	}

	/**
	 * default constructor
	 */
	public Details() {
		use = new ArrayList();
		Use defaultUse = new Use();
		use.add(defaultUse);

	}

	/**
	 * Access method for the alias property.
	 * 
	 * @return the current value of the alias property
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * Sets the value of the alias property.
	 * 
	 * @param aAlias
	 *            the new value of the alias property
	 */
	public void setAlias(String aAlias) {
		alias = aAlias;
	}

	/**
	 * Access method for the description property.
	 * 
	 * @return the current value of the description property
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the value of the description property.
	 * 
	 * @param aDescription
	 *            the new value of the description property
	 */
	public void setDescription(String aDescription) {
		description = aDescription;
	}

	/**
	 * Access method for the use property.
	 * 
	 * @return the current value of the use property
	 */
	public List getUse() {
		return use;
	}

	/**
	 * Gets the uses of this land as a comma seperated string
	 * 
	 * @return java.lang.String
	 */
	public String getUseAsString() {
		List useList = this.getUse();
		int i = useList.size();
		int j = 0;
		String useString = "";
		Iterator iter = useList.iterator();
		while (iter.hasNext()) {
			j++;
			Use useObj = (Use) iter.next();
			if (j < i)
				useString = useString + useObj.getDescription().trim() + ",";
			else
				useString = useString + useObj.getDescription().trim();
		}
		return useString;
	}

	/**
	 * Gets the use of this address as an array
	 * 
	 * @return
	 */
	public String[] getUseAsArray() {

		List useList = this.getUse();
		String[] useArray = new String[useList.size()];
		for (int i = 0; i < useList.size(); i++) {
			Use use = (Use) useList.get(i);
			useArray[i] = use.getDescription().trim();
		}
		return useArray;
	}

	/**
	 * Sets the value of the use property.
	 * 
	 * @param aUse
	 *            the new value of the use property
	 */
	public void setUse(List aUse) {
		use = aUse;
	}

	/**
	 * Access method for the active property.
	 * 
	 * @return the current value of the active property
	 */
	public String getActive() {
		return active;
	}

	/**
	 * Sets the value of the active property.
	 * 
	 * @param aActive
	 *            the new value of the active property
	 */
	public void setActive(String aActive) {
		active = aActive;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return the householdSize
	 */
	public String getHouseholdSize() {
		return householdSize;
	}

	/**
	 * @param householdSize the householdSize to set
	 */
	public void setHouseholdSize(String householdSize) {
		this.householdSize = householdSize;
	}

	/**
	 * @return the bedroomSize
	 */
	public String getBedroomSize() {
		return bedroomSize;
	}

	/**
	 * @param bedroomSize the bedroomSize to set
	 */
	public void setBedroomSize(String bedroomSize) {
		this.bedroomSize = bedroomSize;
	}

	/**
	 * @return the grossRent
	 */
	public String getGrossRent() {
		return grossRent;
	}

	/**
	 * @param grossRent the grossRent to set
	 */
	public void setGrossRent(String grossRent) {
		this.grossRent = grossRent;
	}

	/**
	 * @return the utilityAllowance
	 */
	public String getUtilityAllowance() {
		return utilityAllowance;
	}

	/**
	 * @param utilityAllowance the utilityAllowance to set
	 */
	public void setUtilityAllowance(String utilityAllowance) {
		this.utilityAllowance = utilityAllowance;
	}

	/**
	 * @return the propertyManager
	 */
	public String getPropertyManager() {
		return propertyManager;
	}

	/**
	 * @param propertyManager the propertyManager to set
	 */
	public void setPropertyManager(String propertyManager) {
		this.propertyManager = propertyManager;
	}

	/**
	 * @return the restrictedUnits
	 */
	public String getRestrictedUnits() {
		return restrictedUnits;
	}

	/**
	 * @param restrictedUnits the restrictedUnits to set
	 */
	public void setRestrictedUnits(String restrictedUnits) {
		this.restrictedUnits = restrictedUnits;
	}
}