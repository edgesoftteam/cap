//Source file: C:\\datafile\\source\\elms\\app\\lso\\LandSiteData.java

package elms.app.lso;

import java.util.List;

import elms.app.sitedata.SiteData;

public class LandSiteData extends SiteData {
	public Integer landId;
	public float SITE_AREA;
	public float PARCEL_NO;
	public String ZONES;
	public float NO_OF_LOTS;
	public String LEGAL_DESC;
	public String COVENANT;
	public String RESOLUTION;
	public String ENCROACHMENT;
	public String FLOOD_ZONE;
	public String SEISMIC_ZONE;
	public String GROUND_CONTAMINATION;
	public float FRONT_SET_BACK;
	public float REAR_SET_BACK;
	public float SIDE1_SET_BACK;
	public float SIDE2_SET_BACK;
	public String LANDSCAPING;
	public String FENCE_WALLS;
	public float STREET_DEDICATION;
	public String STREET_DEDICATION_DESC;
	public float ALLEY_DEDICATION;
	public String ALLEY_DEDICATION_DESC;
	public float ALLOW_COVERAGE;
	public float ALLOW_HEIGHT;
	public float ALLOW_MED_AREA;
	public float TOTAL_NO_PARKING;
	public float TOTAL_NO_BEDROOMS;
	public float FRONT_YARD_PAVING;
	public String SETBACK_PROJECTION;
	public String GEO_RPT;
	private List SetbackList;

	/**
	 * 3CD02F8B00F7
	 */
	public LandSiteData() {

	}

	/**
	 * Access method for the landId property.
	 * 
	 * @return the current value of the landId property
	 */
	public Integer getLandId() {
		return landId;
	}

	/**
	 * Sets the value of the landId property.
	 * 
	 * @param aLandId
	 *            the new value of the landId property
	 */
	public void setLandId(Integer aLandId) {
		landId = aLandId;
	}

	/**
	 * Access method for the SITE_AREA property.
	 * 
	 * @return the current value of the SITE_AREA property
	 */
	public float getSITE_AREA() {
		return SITE_AREA;
	}

	/**
	 * Sets the value of the SITE_AREA property.
	 * 
	 * @param aSITE_AREA
	 *            the new value of the SITE_AREA property
	 */
	public void setSITE_AREA(float aSITE_AREA) {
		SITE_AREA = aSITE_AREA;
	}

	/**
	 * Access method for the PARCEL_NO property.
	 * 
	 * @return the current value of the PARCEL_NO property
	 */
	public float getPARCEL_NO() {
		return PARCEL_NO;
	}

	/**
	 * Sets the value of the PARCEL_NO property.
	 * 
	 * @param aPARCEL_NO
	 *            the new value of the PARCEL_NO property
	 */
	public void setPARCEL_NO(float aPARCEL_NO) {
		PARCEL_NO = aPARCEL_NO;
	}

	/**
	 * Access method for the ZONES property.
	 * 
	 * @return the current value of the ZONES property
	 */
	public String getZONES() {
		return ZONES;
	}

	/**
	 * Sets the value of the ZONES property.
	 * 
	 * @param aZONES
	 *            the new value of the ZONES property
	 */
	public void setZONES(String aZONES) {
		ZONES = aZONES;
	}

	/**
	 * Access method for the NO_OF_LOTS property.
	 * 
	 * @return the current value of the NO_OF_LOTS property
	 */
	public float getNO_OF_LOTS() {
		return NO_OF_LOTS;
	}

	/**
	 * Sets the value of the NO_OF_LOTS property.
	 * 
	 * @param aNO_OF_LOTS
	 *            the new value of the NO_OF_LOTS property
	 */
	public void setNO_OF_LOTS(float aNO_OF_LOTS) {
		NO_OF_LOTS = aNO_OF_LOTS;
	}

	/**
	 * Access method for the LEGAL_DESC property.
	 * 
	 * @return the current value of the LEGAL_DESC property
	 */
	public String getLEGAL_DESC() {
		return LEGAL_DESC;
	}

	/**
	 * Sets the value of the LEGAL_DESC property.
	 * 
	 * @param aLEGAL_DESC
	 *            the new value of the LEGAL_DESC property
	 */
	public void setLEGAL_DESC(String aLEGAL_DESC) {
		LEGAL_DESC = aLEGAL_DESC;
	}

	/**
	 * Access method for the COVENANT property.
	 * 
	 * @return the current value of the COVENANT property
	 */
	public String getCOVENANT() {
		return COVENANT;
	}

	/**
	 * Sets the value of the COVENANT property.
	 * 
	 * @param aCOVENANT
	 *            the new value of the COVENANT property
	 */
	public void setCOVENANT(String aCOVENANT) {
		COVENANT = aCOVENANT;
	}

	/**
	 * Access method for the RESOLUTION property.
	 * 
	 * @return the current value of the RESOLUTION property
	 */
	public String getRESOLUTION() {
		return RESOLUTION;
	}

	/**
	 * Sets the value of the RESOLUTION property.
	 * 
	 * @param aRESOLUTION
	 *            the new value of the RESOLUTION property
	 */
	public void setRESOLUTION(String aRESOLUTION) {
		RESOLUTION = aRESOLUTION;
	}

	/**
	 * Access method for the ENCROACHMENT property.
	 * 
	 * @return the current value of the ENCROACHMENT property
	 */
	public String getENCROACHMENT() {
		return ENCROACHMENT;
	}

	/**
	 * Sets the value of the ENCROACHMENT property.
	 * 
	 * @param aENCROACHMENT
	 *            the new value of the ENCROACHMENT property
	 */
	public void setENCROACHMENT(String aENCROACHMENT) {
		ENCROACHMENT = aENCROACHMENT;
	}

	/**
	 * Access method for the FLOOD_ZONE property.
	 * 
	 * @return the current value of the FLOOD_ZONE property
	 */
	public String getFLOOD_ZONE() {
		return FLOOD_ZONE;
	}

	/**
	 * Sets the value of the FLOOD_ZONE property.
	 * 
	 * @param aFLOOD_ZONE
	 *            the new value of the FLOOD_ZONE property
	 */
	public void setFLOOD_ZONE(String aFLOOD_ZONE) {
		FLOOD_ZONE = aFLOOD_ZONE;
	}

	/**
	 * Access method for the SEISMIC_ZONE property.
	 * 
	 * @return the current value of the SEISMIC_ZONE property
	 */
	public String getSEISMIC_ZONE() {
		return SEISMIC_ZONE;
	}

	/**
	 * Sets the value of the SEISMIC_ZONE property.
	 * 
	 * @param aSEISMIC_ZONE
	 *            the new value of the SEISMIC_ZONE property
	 */
	public void setSEISMIC_ZONE(String aSEISMIC_ZONE) {
		SEISMIC_ZONE = aSEISMIC_ZONE;
	}

	/**
	 * Access method for the GROUND_CONTAMINATION property.
	 * 
	 * @return the current value of the GROUND_CONTAMINATION property
	 */
	public String getGROUND_CONTAMINATION() {
		return GROUND_CONTAMINATION;
	}

	/**
	 * Sets the value of the GROUND_CONTAMINATION property.
	 * 
	 * @param aGROUND_CONTAMINATION
	 *            the new value of the GROUND_CONTAMINATION property
	 */
	public void setGROUND_CONTAMINATION(String aGROUND_CONTAMINATION) {
		GROUND_CONTAMINATION = aGROUND_CONTAMINATION;
	}

	/**
	 * Access method for the FRONT_SET_BACK property.
	 * 
	 * @return the current value of the FRONT_SET_BACK property
	 */
	public float getFRONT_SET_BACK() {
		return FRONT_SET_BACK;
	}

	/**
	 * Sets the value of the FRONT_SET_BACK property.
	 * 
	 * @param aFRONT_SET_BACK
	 *            the new value of the FRONT_SET_BACK property
	 */
	public void setFRONT_SET_BACK(float aFRONT_SET_BACK) {
		FRONT_SET_BACK = aFRONT_SET_BACK;
	}

	/**
	 * Access method for the REAR_SET_BACK property.
	 * 
	 * @return the current value of the REAR_SET_BACK property
	 */
	public float getREAR_SET_BACK() {
		return REAR_SET_BACK;
	}

	/**
	 * Sets the value of the REAR_SET_BACK property.
	 * 
	 * @param aREAR_SET_BACK
	 *            the new value of the REAR_SET_BACK property
	 */
	public void setREAR_SET_BACK(float aREAR_SET_BACK) {
		REAR_SET_BACK = aREAR_SET_BACK;
	}

	/**
	 * Access method for the SIDE1_SET_BACK property.
	 * 
	 * @return the current value of the SIDE1_SET_BACK property
	 */
	public float getSIDE1_SET_BACK() {
		return SIDE1_SET_BACK;
	}

	/**
	 * Sets the value of the SIDE1_SET_BACK property.
	 * 
	 * @param aSIDE1_SET_BACK
	 *            the new value of the SIDE1_SET_BACK property
	 */
	public void setSIDE1_SET_BACK(float aSIDE1_SET_BACK) {
		SIDE1_SET_BACK = aSIDE1_SET_BACK;
	}

	/**
	 * Access method for the SIDE2_SET_BACK property.
	 * 
	 * @return the current value of the SIDE2_SET_BACK property
	 */
	public float getSIDE2_SET_BACK() {
		return SIDE2_SET_BACK;
	}

	/**
	 * Sets the value of the SIDE2_SET_BACK property.
	 * 
	 * @param aSIDE2_SET_BACK
	 *            the new value of the SIDE2_SET_BACK property
	 */
	public void setSIDE2_SET_BACK(float aSIDE2_SET_BACK) {
		SIDE2_SET_BACK = aSIDE2_SET_BACK;
	}

	/**
	 * Access method for the LANDSCAPING property.
	 * 
	 * @return the current value of the LANDSCAPING property
	 */
	public String getLANDSCAPING() {
		return LANDSCAPING;
	}

	/**
	 * Sets the value of the LANDSCAPING property.
	 * 
	 * @param aLANDSCAPING
	 *            the new value of the LANDSCAPING property
	 */
	public void setLANDSCAPING(String aLANDSCAPING) {
		LANDSCAPING = aLANDSCAPING;
	}

	/**
	 * Access method for the FENCE_WALLS property.
	 * 
	 * @return the current value of the FENCE_WALLS property
	 */
	public String getFENCE_WALLS() {
		return FENCE_WALLS;
	}

	/**
	 * Sets the value of the FENCE_WALLS property.
	 * 
	 * @param aFENCE_WALLS
	 *            the new value of the FENCE_WALLS property
	 */
	public void setFENCE_WALLS(String aFENCE_WALLS) {
		FENCE_WALLS = aFENCE_WALLS;
	}

	/**
	 * Access method for the STREET_DEDICATION property.
	 * 
	 * @return the current value of the STREET_DEDICATION property
	 */
	public float getSTREET_DEDICATION() {
		return STREET_DEDICATION;
	}

	/**
	 * Sets the value of the STREET_DEDICATION property.
	 * 
	 * @param aSTREET_DEDICATION
	 *            the new value of the STREET_DEDICATION property
	 */
	public void setSTREET_DEDICATION(float aSTREET_DEDICATION) {
		STREET_DEDICATION = aSTREET_DEDICATION;
	}

	/**
	 * Access method for the STREET_DEDICATION_DESC property.
	 * 
	 * @return the current value of the STREET_DEDICATION_DESC property
	 */
	public String getSTREET_DEDICATION_DESC() {
		return STREET_DEDICATION_DESC;
	}

	/**
	 * Sets the value of the STREET_DEDICATION_DESC property.
	 * 
	 * @param aSTREET_DEDICATION_DESC
	 *            the new value of the STREET_DEDICATION_DESC property
	 */
	public void setSTREET_DEDICATION_DESC(String aSTREET_DEDICATION_DESC) {
		STREET_DEDICATION_DESC = aSTREET_DEDICATION_DESC;
	}

	/**
	 * Access method for the ALLEY_DEDICATION property.
	 * 
	 * @return the current value of the ALLEY_DEDICATION property
	 */
	public float getALLEY_DEDICATION() {
		return ALLEY_DEDICATION;
	}

	/**
	 * Sets the value of the ALLEY_DEDICATION property.
	 * 
	 * @param aALLEY_DEDICATION
	 *            the new value of the ALLEY_DEDICATION property
	 */
	public void setALLEY_DEDICATION(float aALLEY_DEDICATION) {
		ALLEY_DEDICATION = aALLEY_DEDICATION;
	}

	/**
	 * Access method for the ALLEY_DEDICATION_DESC property.
	 * 
	 * @return the current value of the ALLEY_DEDICATION_DESC property
	 */
	public String getALLEY_DEDICATION_DESC() {
		return ALLEY_DEDICATION_DESC;
	}

	/**
	 * Sets the value of the ALLEY_DEDICATION_DESC property.
	 * 
	 * @param aALLEY_DEDICATION_DESC
	 *            the new value of the ALLEY_DEDICATION_DESC property
	 */
	public void setALLEY_DEDICATION_DESC(String aALLEY_DEDICATION_DESC) {
		ALLEY_DEDICATION_DESC = aALLEY_DEDICATION_DESC;
	}

	/**
	 * Access method for the ALLOW_COVERAGE property.
	 * 
	 * @return the current value of the ALLOW_COVERAGE property
	 */
	public float getALLOW_COVERAGE() {
		return ALLOW_COVERAGE;
	}

	/**
	 * Sets the value of the ALLOW_COVERAGE property.
	 * 
	 * @param aALLOW_COVERAGE
	 *            the new value of the ALLOW_COVERAGE property
	 */
	public void setALLOW_COVERAGE(float aALLOW_COVERAGE) {
		ALLOW_COVERAGE = aALLOW_COVERAGE;
	}

	/**
	 * Access method for the ALLOW_HEIGHT property.
	 * 
	 * @return the current value of the ALLOW_HEIGHT property
	 */
	public float getALLOW_HEIGHT() {
		return ALLOW_HEIGHT;
	}

	/**
	 * Sets the value of the ALLOW_HEIGHT property.
	 * 
	 * @param aALLOW_HEIGHT
	 *            the new value of the ALLOW_HEIGHT property
	 */
	public void setALLOW_HEIGHT(float aALLOW_HEIGHT) {
		ALLOW_HEIGHT = aALLOW_HEIGHT;
	}

	/**
	 * Access method for the ALLOW_MED_AREA property.
	 * 
	 * @return the current value of the ALLOW_MED_AREA property
	 */
	public float getALLOW_MED_AREA() {
		return ALLOW_MED_AREA;
	}

	/**
	 * Sets the value of the ALLOW_MED_AREA property.
	 * 
	 * @param aALLOW_MED_AREA
	 *            the new value of the ALLOW_MED_AREA property
	 */
	public void setALLOW_MED_AREA(float aALLOW_MED_AREA) {
		ALLOW_MED_AREA = aALLOW_MED_AREA;
	}

	/**
	 * Access method for the TOTAL_NO_PARKING property.
	 * 
	 * @return the current value of the TOTAL_NO_PARKING property
	 */
	public float getTOTAL_NO_PARKING() {
		return TOTAL_NO_PARKING;
	}

	/**
	 * Sets the value of the TOTAL_NO_PARKING property.
	 * 
	 * @param aTOTAL_NO_PARKING
	 *            the new value of the TOTAL_NO_PARKING property
	 */
	public void setTOTAL_NO_PARKING(float aTOTAL_NO_PARKING) {
		TOTAL_NO_PARKING = aTOTAL_NO_PARKING;
	}

	/**
	 * Access method for the TOTAL_NO_BEDROOMS property.
	 * 
	 * @return the current value of the TOTAL_NO_BEDROOMS property
	 */
	public float getTOTAL_NO_BEDROOMS() {
		return TOTAL_NO_BEDROOMS;
	}

	/**
	 * Sets the value of the TOTAL_NO_BEDROOMS property.
	 * 
	 * @param aTOTAL_NO_BEDROOMS
	 *            the new value of the TOTAL_NO_BEDROOMS property
	 */
	public void setTOTAL_NO_BEDROOMS(float aTOTAL_NO_BEDROOMS) {
		TOTAL_NO_BEDROOMS = aTOTAL_NO_BEDROOMS;
	}

	/**
	 * Access method for the FRONT_YARD_PAVING property.
	 * 
	 * @return the current value of the FRONT_YARD_PAVING property
	 */
	public float getFRONT_YARD_PAVING() {
		return FRONT_YARD_PAVING;
	}

	/**
	 * Sets the value of the FRONT_YARD_PAVING property.
	 * 
	 * @param aFRONT_YARD_PAVING
	 *            the new value of the FRONT_YARD_PAVING property
	 */
	public void setFRONT_YARD_PAVING(float aFRONT_YARD_PAVING) {
		FRONT_YARD_PAVING = aFRONT_YARD_PAVING;
	}

	/**
	 * Access method for the SETBACK_PROJECTION property.
	 * 
	 * @return the current value of the SETBACK_PROJECTION property
	 */
	public String getSETBACK_PROJECTION() {
		return SETBACK_PROJECTION;
	}

	/**
	 * Sets the value of the SETBACK_PROJECTION property.
	 * 
	 * @param aSETBACK_PROJECTION
	 *            the new value of the SETBACK_PROJECTION property
	 */
	public void setSETBACK_PROJECTION(String aSETBACK_PROJECTION) {
		SETBACK_PROJECTION = aSETBACK_PROJECTION;
	}

	/**
	 * Access method for the GEO_RPT property.
	 * 
	 * @return the current value of the GEO_RPT property
	 */
	public String getGEO_RPT() {
		return GEO_RPT;
	}

	/**
	 * Sets the value of the GEO_RPT property.
	 * 
	 * @param aGEO_RPT
	 *            the new value of the GEO_RPT property
	 */
	public void setGEO_RPT(String aGEO_RPT) {
		GEO_RPT = aGEO_RPT;
	}

	/**
	 * @return
	 */
	public List getSetbackList() {
		return SetbackList;
	}

	/**
	 * @param list
	 */
	public void setSetbackList(List list) {
		SetbackList = list;
	}

}
