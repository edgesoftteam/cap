package elms.app.lso;

/**
 * The assessor details - taken as-is from the form.
 * 
 * @author Shekhar Jain (shekhar@edgesoftinc.com)
 * @author Anand Belaguly (anand@edgesoftinc.com) last updated : Dec 11, 2003 - Anand Belaguly - formatted the source and added comments
 */
public class Assessor {

	public String parcelNbr;
	public String SITUS_HOUSE_NO;
	public String SITUS_FRACTION;
	public String SITUS_STREET_NAME;
	public String SITUS_DIRECTION;
	public String SITUS_UNIT;
	public String SITUS_CITY_STATE;
	public String SITUS_ZIP;
	public String MAIL_ADDRESS_HOUSE_NO;
	public String MAIL_ADDRESS_FRACTION;
	public String MAIL_ADDRESS_STREET_NAME;
	public String MAIL_ADDRESS_DIRECTION;
	public String MAIL_ADDRESS_UNIT;
	public String MAIL_ADDRESS_CITY_STATE;
	public String MAIL_ADDRESS_ZIP;
	public String FIRST_OWNER_ASSESSEE_NAME;
	public String FIRST_OWNER_NAME_OVERFLOW;

	public String LEGAL_DESCRIPTION_1;
	public String LEGAL_DESCRIPTION_2;
	public String LEGAL_DESCRIPTION_3;
	public String LEGAL_DESCRIPTION_4;
	public String LEGAL_DESCRIPTION_5;
	public String LEGAL_DESCRIPTION_6;

	public String TAX_RATE_AREA;
	public String AGENCY_CLASS_NO;
	public String ZONING_CODE;
	public String USE_CODE;
	public String LAND_CURRENT_ROLL_YEAR;
	public String LAND_CURRENT_VALUE;
	public String IMPROVE_CURRENT_ROLL_YEAR;
	public String IMPROVE_CURRENT_VALUE;

	public String SPECIAL_NAME_ASSESSEE;
	public String SECOND_OWNER_ASSESSEE_NAME;
	public String TAX_STATUS_KEY;
	public String RECORDING_DATE;
	public String HAZARD_ABATEMENT_CITY_KEY;
	public String TAX_STATUS_YEAR_SOLD;
	public String PARTIAL_INTEREST;
	public String HAZARD_ABATEMENT_INFORMATION;
	public String OWNERSHIP_CODE;
	public String DOCUMENT_REASON_CODE;
	public String PERSONAL_PROPERTY_KEY;
	public String EXEMPTION_CLAIM_TYPE;
	public String PERSONAL_PROPERTY_EXEMPTION_VA;
	public String PERSONAL_PROPERTY_VALUE;
	public String FIXTURE_EXEMPTION_VALUE;
	public String FIXTURE_VALUE;
	public String HOMEOWNER_EXEMPTION_VALUE;
	public String HOMEOWNER_NO_OF_EXEMPTIONS;
	public String REAL_ESTATE_EXEMPTION_VALUE;

	public String LAST_SALE_ONE_VERIFY_KEY;
	public String LAST_SALE_ONE_DATE;
	public String LAST_SALE_ONE_SALE_AMOUNT;
	public String LAST_SALE_TWO_VERIFY_KEY;
	public String LAST_SALE_TWO_SALE_DATE;
	public String LAST_SALE_TWO_SALE_AMOUNT;
	public String LAST_SALE_THREE_VERIFY_KEY;
	public String LAST_SALE_THREE_SALE_DATE;
	public String LAST_SALE_THREE_SALE_AMOUNT;

	public String BUILDING_DATA_LINE_1_YEAR_BUIL;
	public String BUILDING_DATA_LINE_1_NO_OF_UNI;
	public String BUILDING_DATA_LINE_1_NO_OF_BED;
	public String BUILDING_DATA_LINE_1_NO_OF_BAT;
	public String BUILDING_DATA_LINE_1_SQUARE_FE;
	public String BUILDING_DATA_LINE_1_SUBPART;
	public String BUILDING_DATA_LINE_1_DESIGN_TY;
	public String BUILDING_DATA_LINE_1_QUALITY;

	public String BUILDING_DATA_LINE_2_YEAR_BUIL;
	public String BUILDING_DATA_LINE_2_NO_OF_UNI;
	public String BUILDING_DATA_LINE_2_NO_OF_BED;
	public String BUILDING_DATA_LINE_2_NO_OF_BAT;
	public String BUILDING_DATA_LINE_2_SQUARE_FE;
	public String BUILDING_DATA_LINE_2_SUBPART;
	public String BUILDING_DATA_LINE_2_DESIGN_TY;
	public String BUILDING_DATA_LINE_2_QUALITY;

	public String BUILDING_DATA_LINE_3_YEAR_BUIL;
	public String BUILDING_DATA_LINE_3_NO_OF_UNI;
	public String BUILDING_DATA_LINE_3_NO_OF_BED;
	public String BUILDING_DATA_LINE_3_NO_OF_BAT;
	public String BUILDING_DATA_LINE_3_SQUARE_FE;
	public String BUILDING_DATA_LINE_3_SUBPART;
	public String BUILDING_DATA_LINE_3_DESIGN_TY;
	public String BUILDING_DATA_LINE_3_QUALITY;

	public String BUILDING_DATA_LINE_4_YEAR_BUIL;
	public String BUILDING_DATA_LINE_4_NO_OF_UNI;
	public String BUILDING_DATA_LINE_4_NO_OF_BED;
	public String BUILDING_DATA_LINE_4_NO_OF_BAT;
	public String BUILDING_DATA_LINE_4_SQUARE_FE;
	public String BUILDING_DATA_LINE_4_SUBPART;
	public String BUILDING_DATA_LINE_4_DESIGN_TY;
	public String BUILDING_DATA_LINE_4_QUALITY;

	public String BUILDING_DATA_LINE_5_YEAR_BUIL;
	public String BUILDING_DATA_LINE_5_NO_OF_UNI;
	public String BUILDING_DATA_LINE_5_NO_OF_BED;
	public String BUILDING_DATA_LINE_5_NO_OF_BAT;
	public String BUILDING_DATA_LINE_5_SQUARE_FE;
	public String BUILDING_DATA_LINE_5_SUBPART;
	public String BUILDING_DATA_LINE_5_DESIGN_TY;
	public String BUILDING_DATA_LINE_5_QUALITY;

	public String SPECIAL_NAME_LEGEND;

	/**
	 * 3CD0322502FE
	 */
	public Assessor() {

	}

	/**
	 * Access method for the parcelNbr property.
	 * 
	 * @return the current value of the parcelNbr property
	 */
	public String getParcelNbr() {
		return parcelNbr;
	}

	/**
	 * Sets the value of the parcelNbr property.
	 * 
	 * @param aParcelNbr
	 *            the new value of the parcelNbr property
	 */
	public void setParcelNbr(String aParcelNbr) {
		parcelNbr = aParcelNbr;
	}

	/**
	 * Access method for the TAX_RATE_AREA property.
	 * 
	 * @return the current value of the TAX_RATE_AREA property
	 */
	public String getTAX_RATE_AREA() {
		return TAX_RATE_AREA;
	}

	/**
	 * Sets the value of the TAX_RATE_AREA property.
	 * 
	 * @param aTAX_RATE_AREA
	 *            the new value of the TAX_RATE_AREA property
	 */
	public void setTAX_RATE_AREA(String aTAX_RATE_AREA) {
		TAX_RATE_AREA = aTAX_RATE_AREA;
	}

	/**
	 * Access method for the AGENCY_CLASS_NO property.
	 * 
	 * @return the current value of the AGENCY_CLASS_NO property
	 */
	public String getAGENCY_CLASS_NO() {
		return AGENCY_CLASS_NO;
	}

	/**
	 * Sets the value of the AGENCY_CLASS_NO property.
	 * 
	 * @param aAGENCY_CLASS_NO
	 *            the new value of the AGENCY_CLASS_NO property
	 */
	public void setAGENCY_CLASS_NO(String aAGENCY_CLASS_NO) {
		AGENCY_CLASS_NO = aAGENCY_CLASS_NO;
	}

	/**
	 * Access method for the LAND_CURRENT_ROLL_YEAR property.
	 * 
	 * @return the current value of the LAND_CURRENT_ROLL_YEAR property
	 */
	public String getLAND_CURRENT_ROLL_YEAR() {
		return LAND_CURRENT_ROLL_YEAR;
	}

	/**
	 * Sets the value of the LAND_CURRENT_ROLL_YEAR property.
	 * 
	 * @param aLAND_CURRENT_ROLL_YEAR
	 *            the new value of the LAND_CURRENT_ROLL_YEAR property
	 */
	public void setLAND_CURRENT_ROLL_YEAR(String aLAND_CURRENT_ROLL_YEAR) {
		LAND_CURRENT_ROLL_YEAR = aLAND_CURRENT_ROLL_YEAR;
	}

	/**
	 * Access method for the LAND_CURRENT_VALUE property.
	 * 
	 * @return the current value of the LAND_CURRENT_VALUE property
	 */
	public String getLAND_CURRENT_VALUE() {
		return LAND_CURRENT_VALUE;
	}

	/**
	 * Sets the value of the LAND_CURRENT_VALUE property.
	 * 
	 * @param aLAND_CURRENT_VALUE
	 *            the new value of the LAND_CURRENT_VALUE property
	 */
	public void setLAND_CURRENT_VALUE(String aLAND_CURRENT_VALUE) {
		LAND_CURRENT_VALUE = aLAND_CURRENT_VALUE;
	}

	/**
	 * Access method for the IMPROVE_CURRENT_ROLL_YEAR property.
	 * 
	 * @return the current value of the IMPROVE_CURRENT_ROLL_YEAR property
	 */
	public String getIMPROVE_CURRENT_ROLL_YEAR() {
		return IMPROVE_CURRENT_ROLL_YEAR;
	}

	/**
	 * Sets the value of the IMPROVE_CURRENT_ROLL_YEAR property.
	 * 
	 * @param aIMPROVE_CURRENT_ROLL_YEAR
	 *            the new value of the IMPROVE_CURRENT_ROLL_YEAR property
	 */
	public void setIMPROVE_CURRENT_ROLL_YEAR(String aIMPROVE_CURRENT_ROLL_YEAR) {
		IMPROVE_CURRENT_ROLL_YEAR = aIMPROVE_CURRENT_ROLL_YEAR;
	}

	/**
	 * Access method for the IMPROVE_CURRENT_VALUE property.
	 * 
	 * @return the current value of the IMPROVE_CURRENT_VALUE property
	 */
	public String getIMPROVE_CURRENT_VALUE() {
		return IMPROVE_CURRENT_VALUE;
	}

	/**
	 * Sets the value of the IMPROVE_CURRENT_VALUE property.
	 * 
	 * @param aIMPROVE_CURRENT_VALUE
	 *            the new value of the IMPROVE_CURRENT_VALUE property
	 */
	public void setIMPROVE_CURRENT_VALUE(String aIMPROVE_CURRENT_VALUE) {
		IMPROVE_CURRENT_VALUE = aIMPROVE_CURRENT_VALUE;
	}

	/**
	 * Access method for the SITUS_HOUSE_NO property.
	 * 
	 * @return the current value of the SITUS_HOUSE_NO property
	 */
	public String getSITUS_HOUSE_NO() {
		return SITUS_HOUSE_NO;
	}

	/**
	 * Sets the value of the SITUS_HOUSE_NO property.
	 * 
	 * @param aSITUS_HOUSE_NO
	 *            the new value of the SITUS_HOUSE_NO property
	 */
	public void setSITUS_HOUSE_NO(String aSITUS_HOUSE_NO) {
		SITUS_HOUSE_NO = aSITUS_HOUSE_NO;
	}

	/**
	 * Access method for the SITUS_FRACTION property.
	 * 
	 * @return the current value of the SITUS_FRACTION property
	 */
	public String getSITUS_FRACTION() {
		return SITUS_FRACTION;
	}

	/**
	 * Sets the value of the SITUS_FRACTION property.
	 * 
	 * @param aSITUS_FRACTION
	 *            the new value of the SITUS_FRACTION property
	 */
	public void setSITUS_FRACTION(String aSITUS_FRACTION) {
		SITUS_FRACTION = aSITUS_FRACTION;
	}

	/**
	 * Access method for the SITUS_DIRECTION property.
	 * 
	 * @return the current value of the SITUS_DIRECTION property
	 */
	public String getSITUS_DIRECTION() {
		return SITUS_DIRECTION;
	}

	/**
	 * Sets the value of the SITUS_DIRECTION property.
	 * 
	 * @param aSITUS_DIRECTION
	 *            the new value of the SITUS_DIRECTION property
	 */
	public void setSITUS_DIRECTION(String aSITUS_DIRECTION) {
		SITUS_DIRECTION = aSITUS_DIRECTION;
	}

	/**
	 * Access method for the SITUS_STREET_NAME property.
	 * 
	 * @return the current value of the SITUS_STREET_NAME property
	 */
	public String getSITUS_STREET_NAME() {
		return SITUS_STREET_NAME;
	}

	/**
	 * Sets the value of the SITUS_STREET_NAME property.
	 * 
	 * @param aSITUS_STREET_NAME
	 *            the new value of the SITUS_STREET_NAME property
	 */
	public void setSITUS_STREET_NAME(String aSITUS_STREET_NAME) {
		SITUS_STREET_NAME = aSITUS_STREET_NAME;
	}

	/**
	 * Access method for the SITUS_UNIT property.
	 * 
	 * @return the current value of the SITUS_UNIT property
	 */
	public String getSITUS_UNIT() {
		return SITUS_UNIT;
	}

	/**
	 * Sets the value of the SITUS_UNIT property.
	 * 
	 * @param aSITUS_UNIT
	 *            the new value of the SITUS_UNIT property
	 */
	public void setSITUS_UNIT(String aSITUS_UNIT) {
		SITUS_UNIT = aSITUS_UNIT;
	}

	/**
	 * Access method for the SITUS_CITY_STATE property.
	 * 
	 * @return the current value of the SITUS_CITY_STATE property
	 */
	public String getSITUS_CITY_STATE() {
		return SITUS_CITY_STATE;
	}

	/**
	 * Sets the value of the SITUS_CITY_STATE property.
	 * 
	 * @param aSITUS_CITY_STATE
	 *            the new value of the SITUS_CITY_STATE property
	 */
	public void setSITUS_CITY_STATE(String aSITUS_CITY_STATE) {
		if (aSITUS_CITY_STATE == null)
			aSITUS_CITY_STATE = "";
		SITUS_CITY_STATE = aSITUS_CITY_STATE;
	}

	/**
	 * Access method for the SITUS_ZIP property.
	 * 
	 * @return the current value of the SITUS_ZIP property
	 */
	public String getSITUS_ZIP() {
		return SITUS_ZIP;
	}

	/**
	 * Sets the value of the SITUS_ZIP property.
	 * 
	 * @param aSITUS_ZIP
	 *            the new value of the SITUS_ZIP property
	 */
	public void setSITUS_ZIP(String aSITUS_ZIP) {
		SITUS_ZIP = aSITUS_ZIP;
	}

	/**
	 * Access method for the MAIL_ADDRESS_HOUSE_NO property.
	 * 
	 * @return the current value of the MAIL_ADDRESS_HOUSE_NO property
	 */
	public String getMAIL_ADDRESS_HOUSE_NO() {
		return MAIL_ADDRESS_HOUSE_NO;
	}

	/**
	 * Sets the value of the MAIL_ADDRESS_HOUSE_NO property.
	 * 
	 * @param aMAIL_ADDRESS_HOUSE_NO
	 *            the new value of the MAIL_ADDRESS_HOUSE_NO property
	 */
	public void setMAIL_ADDRESS_HOUSE_NO(String aMAIL_ADDRESS_HOUSE_NO) {
		MAIL_ADDRESS_HOUSE_NO = aMAIL_ADDRESS_HOUSE_NO;
	}

	/**
	 * Access method for the MAIL_ADDRESS_FRACTION property.
	 * 
	 * @return the current value of the MAIL_ADDRESS_FRACTION property
	 */
	public String getMAIL_ADDRESS_FRACTION() {
		return MAIL_ADDRESS_FRACTION;
	}

	/**
	 * Sets the value of the MAIL_ADDRESS_FRACTION property.
	 * 
	 * @param aMAIL_ADDRESS_FRACTION
	 *            the new value of the MAIL_ADDRESS_FRACTION property
	 */
	public void setMAIL_ADDRESS_FRACTION(String aMAIL_ADDRESS_FRACTION) {
		MAIL_ADDRESS_FRACTION = aMAIL_ADDRESS_FRACTION;
	}

	/**
	 * Access method for the MAIL_ADDRESS_DIRECTION property.
	 * 
	 * @return the current value of the MAIL_ADDRESS_DIRECTION property
	 */
	public String getMAIL_ADDRESS_DIRECTION() {
		return MAIL_ADDRESS_DIRECTION;
	}

	/**
	 * Sets the value of the MAIL_ADDRESS_DIRECTION property.
	 * 
	 * @param aMAIL_ADDRESS_DIRECTION
	 *            the new value of the MAIL_ADDRESS_DIRECTION property
	 */
	public void setMAIL_ADDRESS_DIRECTION(String aMAIL_ADDRESS_DIRECTION) {
		MAIL_ADDRESS_DIRECTION = aMAIL_ADDRESS_DIRECTION;
	}

	/**
	 * Access method for the MAIL_ADDRESS_STREET_NAME property.
	 * 
	 * @return the current value of the MAIL_ADDRESS_STREET_NAME property
	 */
	public String getMAIL_ADDRESS_STREET_NAME() {
		return MAIL_ADDRESS_STREET_NAME;
	}

	/**
	 * Sets the value of the MAIL_ADDRESS_STREET_NAME property.
	 * 
	 * @param aMAIL_ADDRESS_STREET_NAME
	 *            the new value of the MAIL_ADDRESS_STREET_NAME property
	 */
	public void setMAIL_ADDRESS_STREET_NAME(String aMAIL_ADDRESS_STREET_NAME) {
		MAIL_ADDRESS_STREET_NAME = aMAIL_ADDRESS_STREET_NAME;
	}

	/**
	 * Access method for the MAIL_ADDRESS_UNIT property.
	 * 
	 * @return the current value of the MAIL_ADDRESS_UNIT property
	 */
	public String getMAIL_ADDRESS_UNIT() {
		return MAIL_ADDRESS_UNIT;
	}

	/**
	 * Sets the value of the MAIL_ADDRESS_UNIT property.
	 * 
	 * @param aMAIL_ADDRESS_UNIT
	 *            the new value of the MAIL_ADDRESS_UNIT property
	 */
	public void setMAIL_ADDRESS_UNIT(String aMAIL_ADDRESS_UNIT) {
		MAIL_ADDRESS_UNIT = aMAIL_ADDRESS_UNIT;
	}

	/**
	 * Access method for the MAIL_ADDRESS_CITY_STATE property.
	 * 
	 * @return the current value of the MAIL_ADDRESS_CITY_STATE property
	 */
	public String getMAIL_ADDRESS_CITY_STATE() {
		return MAIL_ADDRESS_CITY_STATE;
	}

	/**
	 * Sets the value of the MAIL_ADDRESS_CITY_STATE property.
	 * 
	 * @param aMAIL_ADDRESS_CITY_STATE
	 *            the new value of the MAIL_ADDRESS_CITY_STATE property
	 */
	public void setMAIL_ADDRESS_CITY_STATE(String aMAIL_ADDRESS_CITY_STATE) {
		MAIL_ADDRESS_CITY_STATE = aMAIL_ADDRESS_CITY_STATE;
	}

	/**
	 * Access method for the MAIL_ADDRESS_ZIP property.
	 * 
	 * @return the current value of the MAIL_ADDRESS_ZIP property
	 */
	public String getMAIL_ADDRESS_ZIP() {
		return MAIL_ADDRESS_ZIP;
	}

	/**
	 * Sets the value of the MAIL_ADDRESS_ZIP property.
	 * 
	 * @param aMAIL_ADDRESS_ZIP
	 *            the new value of the MAIL_ADDRESS_ZIP property
	 */
	public void setMAIL_ADDRESS_ZIP(String aMAIL_ADDRESS_ZIP) {
		MAIL_ADDRESS_ZIP = aMAIL_ADDRESS_ZIP;
	}

	/**
	 * Access method for the FIRST_OWNER_ASSESSEE_NAME property.
	 * 
	 * @return the current value of the FIRST_OWNER_ASSESSEE_NAME property
	 */
	public String getFIRST_OWNER_ASSESSEE_NAME() {
		return FIRST_OWNER_ASSESSEE_NAME;
	}

	/**
	 * Sets the value of the FIRST_OWNER_ASSESSEE_NAME property.
	 * 
	 * @param aFIRST_OWNER_ASSESSEE_NAME
	 *            the new value of the FIRST_OWNER_ASSESSEE_NAME property
	 */
	public void setFIRST_OWNER_ASSESSEE_NAME(String aFIRST_OWNER_ASSESSEE_NAME) {
		FIRST_OWNER_ASSESSEE_NAME = aFIRST_OWNER_ASSESSEE_NAME;
	}

	/**
	 * Access method for the FIRST_OWNER_NAME_OVERFLOW property.
	 * 
	 * @return the current value of the FIRST_OWNER_NAME_OVERFLOW property
	 */
	public String getFIRST_OWNER_NAME_OVERFLOW() {
		return FIRST_OWNER_NAME_OVERFLOW;
	}

	/**
	 * Sets the value of the FIRST_OWNER_NAME_OVERFLOW property.
	 * 
	 * @param aFIRST_OWNER_NAME_OVERFLOW
	 *            the new value of the FIRST_OWNER_NAME_OVERFLOW property
	 */
	public void setFIRST_OWNER_NAME_OVERFLOW(String aFIRST_OWNER_NAME_OVERFLOW) {
		FIRST_OWNER_NAME_OVERFLOW = aFIRST_OWNER_NAME_OVERFLOW;
	}

	/**
	 * Access method for the SPECIAL_NAME_LEGEND property.
	 * 
	 * @return the current value of the SPECIAL_NAME_LEGEND property
	 */
	public String getSPECIAL_NAME_LEGEND() {
		return SPECIAL_NAME_LEGEND;
	}

	/**
	 * Sets the value of the SPECIAL_NAME_LEGEND property.
	 * 
	 * @param aSPECIAL_NAME_LEGEND
	 *            the new value of the SPECIAL_NAME_LEGEND property
	 */
	public void setSPECIAL_NAME_LEGEND(String aSPECIAL_NAME_LEGEND) {
		SPECIAL_NAME_LEGEND = aSPECIAL_NAME_LEGEND;
	}

	/**
	 * Access method for the SPECIAL_NAME_ASSESSEE property.
	 * 
	 * @return the current value of the SPECIAL_NAME_ASSESSEE property
	 */
	public String getSPECIAL_NAME_ASSESSEE() {
		return SPECIAL_NAME_ASSESSEE;
	}

	/**
	 * Sets the value of the SPECIAL_NAME_ASSESSEE property.
	 * 
	 * @param aSPECIAL_NAME_ASSESSEE
	 *            the new value of the SPECIAL_NAME_ASSESSEE property
	 */
	public void setSPECIAL_NAME_ASSESSEE(String aSPECIAL_NAME_ASSESSEE) {
		SPECIAL_NAME_ASSESSEE = aSPECIAL_NAME_ASSESSEE;
	}

	/**
	 * Access method for the SECOND_OWNER_ASSESSEE_NAME property.
	 * 
	 * @return the current value of the SECOND_OWNER_ASSESSEE_NAME property
	 */
	public String getSECOND_OWNER_ASSESSEE_NAME() {
		return SECOND_OWNER_ASSESSEE_NAME;
	}

	/**
	 * Sets the value of the SECOND_OWNER_ASSESSEE_NAME property.
	 * 
	 * @param aSECOND_OWNER_ASSESSEE_NAME
	 *            the new value of the SECOND_OWNER_ASSESSEE_NAME property
	 */
	public void setSECOND_OWNER_ASSESSEE_NAME(String aSECOND_OWNER_ASSESSEE_NAME) {
		SECOND_OWNER_ASSESSEE_NAME = aSECOND_OWNER_ASSESSEE_NAME;
	}

	/**
	 * Access method for the RECORDING_DATE property.
	 * 
	 * @return the current value of the RECORDING_DATE property
	 */
	public String getRECORDING_DATE() {
		return RECORDING_DATE;
	}

	/**
	 * Sets the value of the RECORDING_DATE property.
	 * 
	 * @param aRECORDING_DATE
	 *            the new value of the RECORDING_DATE property
	 */
	public void setRECORDING_DATE(String aRECORDING_DATE) {
		RECORDING_DATE = aRECORDING_DATE;
	}

	/**
	 * Access method for the TAX_STATUS_KEY property.
	 * 
	 * @return the current value of the TAX_STATUS_KEY property
	 */
	public String getTAX_STATUS_KEY() {
		return TAX_STATUS_KEY;
	}

	/**
	 * Sets the value of the TAX_STATUS_KEY property.
	 * 
	 * @param aTAX_STATUS_KEY
	 *            the new value of the TAX_STATUS_KEY property
	 */
	public void setTAX_STATUS_KEY(String aTAX_STATUS_KEY) {
		TAX_STATUS_KEY = aTAX_STATUS_KEY;
	}

	/**
	 * Access method for the TAX_STATUS_YEAR_SOLD property.
	 * 
	 * @return the current value of the TAX_STATUS_YEAR_SOLD property
	 */
	public String getTAX_STATUS_YEAR_SOLD() {
		return TAX_STATUS_YEAR_SOLD;
	}

	/**
	 * Sets the value of the TAX_STATUS_YEAR_SOLD property.
	 * 
	 * @param aTAX_STATUS_YEAR_SOLD
	 *            the new value of the TAX_STATUS_YEAR_SOLD property
	 */
	public void setTAX_STATUS_YEAR_SOLD(String aTAX_STATUS_YEAR_SOLD) {
		TAX_STATUS_YEAR_SOLD = aTAX_STATUS_YEAR_SOLD;
	}

	/**
	 * Access method for the HAZARD_ABATEMENT_CITY_KEY property.
	 * 
	 * @return the current value of the HAZARD_ABATEMENT_CITY_KEY property
	 */
	public String getHAZARD_ABATEMENT_CITY_KEY() {
		return HAZARD_ABATEMENT_CITY_KEY;
	}

	/**
	 * Sets the value of the HAZARD_ABATEMENT_CITY_KEY property.
	 * 
	 * @param aHAZARD_ABATEMENT_CITY_KEY
	 *            the new value of the HAZARD_ABATEMENT_CITY_KEY property
	 */
	public void setHAZARD_ABATEMENT_CITY_KEY(String aHAZARD_ABATEMENT_CITY_KEY) {
		HAZARD_ABATEMENT_CITY_KEY = aHAZARD_ABATEMENT_CITY_KEY;
	}

	/**
	 * Access method for the HAZARD_ABATEMENT_INFORMATION property.
	 * 
	 * @return the current value of the HAZARD_ABATEMENT_INFORMATION property
	 */
	public String getHAZARD_ABATEMENT_INFORMATION() {
		return HAZARD_ABATEMENT_INFORMATION;
	}

	/**
	 * Sets the value of the HAZARD_ABATEMENT_INFORMATION property.
	 * 
	 * @param aHAZARD_ABATEMENT_INFORMATION
	 *            the new value of the HAZARD_ABATEMENT_INFORMATION property
	 */
	public void setHAZARD_ABATEMENT_INFORMATION(String aHAZARD_ABATEMENT_INFORMATION) {
		HAZARD_ABATEMENT_INFORMATION = aHAZARD_ABATEMENT_INFORMATION;
	}

	/**
	 * Access method for the ZONING_CODE property.
	 * 
	 * @return the current value of the ZONING_CODE property
	 */
	public String getZONING_CODE() {
		return ZONING_CODE;
	}

	/**
	 * Sets the value of the ZONING_CODE property.
	 * 
	 * @param aZONING_CODE
	 *            the new value of the ZONING_CODE property
	 */
	public void setZONING_CODE(String aZONING_CODE) {
		ZONING_CODE = aZONING_CODE;
	}

	/**
	 * Access method for the USE_CODE property.
	 * 
	 * @return the current value of the USE_CODE property
	 */
	public String getUSE_CODE() {
		return USE_CODE;
	}

	/**
	 * Sets the value of the USE_CODE property.
	 * 
	 * @param aUSE_CODE
	 *            the new value of the USE_CODE property
	 */
	public void setUSE_CODE(String aUSE_CODE) {
		USE_CODE = aUSE_CODE;
	}

	/**
	 * Access method for the PARTIAL_INTEREST property.
	 * 
	 * @return the current value of the PARTIAL_INTEREST property
	 */
	public String getPARTIAL_INTEREST() {
		return PARTIAL_INTEREST;
	}

	/**
	 * Sets the value of the PARTIAL_INTEREST property.
	 * 
	 * @param aPARTIAL_INTEREST
	 *            the new value of the PARTIAL_INTEREST property
	 */
	public void setPARTIAL_INTEREST(String aPARTIAL_INTEREST) {
		PARTIAL_INTEREST = aPARTIAL_INTEREST;
	}

	/**
	 * Access method for the DOCUMENT_REASON_CODE property.
	 * 
	 * @return the current value of the DOCUMENT_REASON_CODE property
	 */
	public String getDOCUMENT_REASON_CODE() {
		return DOCUMENT_REASON_CODE;
	}

	/**
	 * Sets the value of the DOCUMENT_REASON_CODE property.
	 * 
	 * @param aDOCUMENT_REASON_CODE
	 *            the new value of the DOCUMENT_REASON_CODE property
	 */
	public void setDOCUMENT_REASON_CODE(String aDOCUMENT_REASON_CODE) {
		DOCUMENT_REASON_CODE = aDOCUMENT_REASON_CODE;
	}

	/**
	 * Access method for the OWNERSHIP_CODE property.
	 * 
	 * @return the current value of the OWNERSHIP_CODE property
	 */
	public String getOWNERSHIP_CODE() {
		return OWNERSHIP_CODE;
	}

	/**
	 * Sets the value of the OWNERSHIP_CODE property.
	 * 
	 * @param aOWNERSHIP_CODE
	 *            the new value of the OWNERSHIP_CODE property
	 */
	public void setOWNERSHIP_CODE(String aOWNERSHIP_CODE) {
		OWNERSHIP_CODE = aOWNERSHIP_CODE;
	}

	/**
	 * Access method for the EXEMPTION_CLAIM_TYPE property.
	 * 
	 * @return the current value of the EXEMPTION_CLAIM_TYPE property
	 */
	public String getEXEMPTION_CLAIM_TYPE() {
		return EXEMPTION_CLAIM_TYPE;
	}

	/**
	 * Sets the value of the EXEMPTION_CLAIM_TYPE property.
	 * 
	 * @param aEXEMPTION_CLAIM_TYPE
	 *            the new value of the EXEMPTION_CLAIM_TYPE property
	 */
	public void setEXEMPTION_CLAIM_TYPE(String aEXEMPTION_CLAIM_TYPE) {
		EXEMPTION_CLAIM_TYPE = aEXEMPTION_CLAIM_TYPE;
	}

	/**
	 * Access method for the PERSONAL_PROPERTY_KEY property.
	 * 
	 * @return the current value of the PERSONAL_PROPERTY_KEY property
	 */
	public String getPERSONAL_PROPERTY_KEY() {
		return PERSONAL_PROPERTY_KEY;
	}

	/**
	 * Sets the value of the PERSONAL_PROPERTY_KEY property.
	 * 
	 * @param aPERSONAL_PROPERTY_KEY
	 *            the new value of the PERSONAL_PROPERTY_KEY property
	 */
	public void setPERSONAL_PROPERTY_KEY(String aPERSONAL_PROPERTY_KEY) {
		PERSONAL_PROPERTY_KEY = aPERSONAL_PROPERTY_KEY;
	}

	/**
	 * Access method for the PERSONAL_PROPERTY_VALUE property.
	 * 
	 * @return the current value of the PERSONAL_PROPERTY_VALUE property
	 */
	public String getPERSONAL_PROPERTY_VALUE() {
		return PERSONAL_PROPERTY_VALUE;
	}

	/**
	 * Sets the value of the PERSONAL_PROPERTY_VALUE property.
	 * 
	 * @param aPERSONAL_PROPERTY_VALUE
	 *            the new value of the PERSONAL_PROPERTY_VALUE property
	 */
	public void setPERSONAL_PROPERTY_VALUE(String aPERSONAL_PROPERTY_VALUE) {
		PERSONAL_PROPERTY_VALUE = aPERSONAL_PROPERTY_VALUE;
	}

	/**
	 * Access method for the PERSONAL_PROPERTY_EXEMPTION_VA property.
	 * 
	 * @return the current value of the PERSONAL_PROPERTY_EXEMPTION_VA property
	 */
	public String getPERSONAL_PROPERTY_EXEMPTION_VA() {
		return PERSONAL_PROPERTY_EXEMPTION_VA;
	}

	/**
	 * Sets the value of the PERSONAL_PROPERTY_EXEMPTION_VA property.
	 * 
	 * @param aPERSONAL_PROPERTY_EXEMPTION_VA
	 *            the new value of the PERSONAL_PROPERTY_EXEMPTION_VA property
	 */
	public void setPERSONAL_PROPERTY_EXEMPTION_VA(String aPERSONAL_PROPERTY_EXEMPTION_VA) {
		PERSONAL_PROPERTY_EXEMPTION_VA = aPERSONAL_PROPERTY_EXEMPTION_VA;
	}

	/**
	 * Access method for the FIXTURE_VALUE property.
	 * 
	 * @return the current value of the FIXTURE_VALUE property
	 */
	public String getFIXTURE_VALUE() {
		return FIXTURE_VALUE;
	}

	/**
	 * Sets the value of the FIXTURE_VALUE property.
	 * 
	 * @param aFIXTURE_VALUE
	 *            the new value of the FIXTURE_VALUE property
	 */
	public void setFIXTURE_VALUE(String aFIXTURE_VALUE) {
		FIXTURE_VALUE = aFIXTURE_VALUE;
	}

	/**
	 * Access method for the FIXTURE_EXEMPTION_VALUE property.
	 * 
	 * @return the current value of the FIXTURE_EXEMPTION_VALUE property
	 */
	public String getFIXTURE_EXEMPTION_VALUE() {
		return FIXTURE_EXEMPTION_VALUE;
	}

	/**
	 * Sets the value of the FIXTURE_EXEMPTION_VALUE property.
	 * 
	 * @param aFIXTURE_EXEMPTION_VALUE
	 *            the new value of the FIXTURE_EXEMPTION_VALUE property
	 */
	public void setFIXTURE_EXEMPTION_VALUE(String aFIXTURE_EXEMPTION_VALUE) {
		FIXTURE_EXEMPTION_VALUE = aFIXTURE_EXEMPTION_VALUE;
	}

	/**
	 * Access method for the HOMEOWNER_NO_OF_EXEMPTIONS property.
	 * 
	 * @return the current value of the HOMEOWNER_NO_OF_EXEMPTIONS property
	 */
	public String getHOMEOWNER_NO_OF_EXEMPTIONS() {
		return HOMEOWNER_NO_OF_EXEMPTIONS;
	}

	/**
	 * Sets the value of the HOMEOWNER_NO_OF_EXEMPTIONS property.
	 * 
	 * @param aHOMEOWNER_NO_OF_EXEMPTIONS
	 *            the new value of the HOMEOWNER_NO_OF_EXEMPTIONS property
	 */
	public void setHOMEOWNER_NO_OF_EXEMPTIONS(String aHOMEOWNER_NO_OF_EXEMPTIONS) {
		HOMEOWNER_NO_OF_EXEMPTIONS = aHOMEOWNER_NO_OF_EXEMPTIONS;
	}

	/**
	 * Access method for the HOMEOWNER_EXEMPTION_VALUE property.
	 * 
	 * @return the current value of the HOMEOWNER_EXEMPTION_VALUE property
	 */
	public String getHOMEOWNER_EXEMPTION_VALUE() {
		return HOMEOWNER_EXEMPTION_VALUE;
	}

	/**
	 * Sets the value of the HOMEOWNER_EXEMPTION_VALUE property.
	 * 
	 * @param aHOMEOWNER_EXEMPTION_VALUE
	 *            the new value of the HOMEOWNER_EXEMPTION_VALUE property
	 */
	public void setHOMEOWNER_EXEMPTION_VALUE(String aHOMEOWNER_EXEMPTION_VALUE) {
		HOMEOWNER_EXEMPTION_VALUE = aHOMEOWNER_EXEMPTION_VALUE;
	}

	/**
	 * Access method for the REAL_ESTATE_EXEMPTION_VALUE property.
	 * 
	 * @return the current value of the REAL_ESTATE_EXEMPTION_VALUE property
	 */
	public String getREAL_ESTATE_EXEMPTION_VALUE() {
		return REAL_ESTATE_EXEMPTION_VALUE;
	}

	/**
	 * Sets the value of the REAL_ESTATE_EXEMPTION_VALUE property.
	 * 
	 * @param aREAL_ESTATE_EXEMPTION_VALUE
	 *            the new value of the REAL_ESTATE_EXEMPTION_VALUE property
	 */
	public void setREAL_ESTATE_EXEMPTION_VALUE(String aREAL_ESTATE_EXEMPTION_VALUE) {
		REAL_ESTATE_EXEMPTION_VALUE = aREAL_ESTATE_EXEMPTION_VALUE;
	}

	/**
	 * Access method for the LAST_SALE_ONE_VERIFY_KEY property.
	 * 
	 * @return the current value of the LAST_SALE_ONE_VERIFY_KEY property
	 */
	public String getLAST_SALE_ONE_VERIFY_KEY() {
		return LAST_SALE_ONE_VERIFY_KEY;
	}

	/**
	 * Sets the value of the LAST_SALE_ONE_VERIFY_KEY property.
	 * 
	 * @param aLAST_SALE_ONE_VERIFY_KEY
	 *            the new value of the LAST_SALE_ONE_VERIFY_KEY property
	 */
	public void setLAST_SALE_ONE_VERIFY_KEY(String aLAST_SALE_ONE_VERIFY_KEY) {
		LAST_SALE_ONE_VERIFY_KEY = aLAST_SALE_ONE_VERIFY_KEY;
	}

	/**
	 * Access method for the LAST_SALE_ONE_SALE_AMOUNT property.
	 * 
	 * @return the current value of the LAST_SALE_ONE_SALE_AMOUNT property
	 */
	public String getLAST_SALE_ONE_SALE_AMOUNT() {
		return LAST_SALE_ONE_SALE_AMOUNT;
	}

	/**
	 * Sets the value of the LAST_SALE_ONE_SALE_AMOUNT property.
	 * 
	 * @param aLAST_SALE_ONE_SALE_AMOUNT
	 *            the new value of the LAST_SALE_ONE_SALE_AMOUNT property
	 */
	public void setLAST_SALE_ONE_SALE_AMOUNT(String aLAST_SALE_ONE_SALE_AMOUNT) {
		LAST_SALE_ONE_SALE_AMOUNT = aLAST_SALE_ONE_SALE_AMOUNT;
	}

	/**
	 * Access method for the LAST_SALE_ONE_DATE property.
	 * 
	 * @return the current value of the LAST_SALE_ONE_DATE property
	 */
	public String getLAST_SALE_ONE_DATE() {
		return LAST_SALE_ONE_DATE;
	}

	/**
	 * Sets the value of the LAST_SALE_ONE_DATE property.
	 * 
	 * @param aLAST_SALE_ONE_DATE
	 *            the new value of the LAST_SALE_ONE_DATE property
	 */
	public void setLAST_SALE_ONE_DATE(String aLAST_SALE_ONE_DATE) {
		LAST_SALE_ONE_DATE = aLAST_SALE_ONE_DATE;
	}

	/**
	 * Access method for the LAST_SALE_TWO_VERIFY_KEY property.
	 * 
	 * @return the current value of the LAST_SALE_TWO_VERIFY_KEY property
	 */
	public String getLAST_SALE_TWO_VERIFY_KEY() {
		return LAST_SALE_TWO_VERIFY_KEY;
	}

	/**
	 * Sets the value of the LAST_SALE_TWO_VERIFY_KEY property.
	 * 
	 * @param aLAST_SALE_TWO_VERIFY_KEY
	 *            the new value of the LAST_SALE_TWO_VERIFY_KEY property
	 */
	public void setLAST_SALE_TWO_VERIFY_KEY(String aLAST_SALE_TWO_VERIFY_KEY) {
		LAST_SALE_TWO_VERIFY_KEY = aLAST_SALE_TWO_VERIFY_KEY;
	}

	/**
	 * Access method for the LAST_SALE_TWO_SALE_AMOUNT property.
	 * 
	 * @return the current value of the LAST_SALE_TWO_SALE_AMOUNT property
	 */
	public String getLAST_SALE_TWO_SALE_AMOUNT() {
		return LAST_SALE_TWO_SALE_AMOUNT;
	}

	/**
	 * Sets the value of the LAST_SALE_TWO_SALE_AMOUNT property.
	 * 
	 * @param aLAST_SALE_TWO_SALE_AMOUNT
	 *            the new value of the LAST_SALE_TWO_SALE_AMOUNT property
	 */
	public void setLAST_SALE_TWO_SALE_AMOUNT(String aLAST_SALE_TWO_SALE_AMOUNT) {
		LAST_SALE_TWO_SALE_AMOUNT = aLAST_SALE_TWO_SALE_AMOUNT;
	}

	/**
	 * Access method for the LAST_SALE_TWO_SALE_DATE property.
	 * 
	 * @return the current value of the LAST_SALE_TWO_SALE_DATE property
	 */
	public String getLAST_SALE_TWO_SALE_DATE() {
		return LAST_SALE_TWO_SALE_DATE;
	}

	/**
	 * Sets the value of the LAST_SALE_TWO_SALE_DATE property.
	 * 
	 * @param aLAST_SALE_TWO_SALE_DATE
	 *            the new value of the LAST_SALE_TWO_SALE_DATE property
	 */
	public void setLAST_SALE_TWO_SALE_DATE(String aLAST_SALE_TWO_SALE_DATE) {
		LAST_SALE_TWO_SALE_DATE = aLAST_SALE_TWO_SALE_DATE;
	}

	/**
	 * Access method for the LAST_SALE_THREE_VERIFY_KEY property.
	 * 
	 * @return the current value of the LAST_SALE_THREE_VERIFY_KEY property
	 */
	public String getLAST_SALE_THREE_VERIFY_KEY() {
		return LAST_SALE_THREE_VERIFY_KEY;
	}

	/**
	 * Sets the value of the LAST_SALE_THREE_VERIFY_KEY property.
	 * 
	 * @param aLAST_SALE_THREE_VERIFY_KEY
	 *            the new value of the LAST_SALE_THREE_VERIFY_KEY property
	 */
	public void setLAST_SALE_THREE_VERIFY_KEY(String aLAST_SALE_THREE_VERIFY_KEY) {
		LAST_SALE_THREE_VERIFY_KEY = aLAST_SALE_THREE_VERIFY_KEY;
	}

	/**
	 * Access method for the LAST_SALE_THREE_SALE_AMOUNT property.
	 * 
	 * @return the current value of the LAST_SALE_THREE_SALE_AMOUNT property
	 */
	public String getLAST_SALE_THREE_SALE_AMOUNT() {
		return LAST_SALE_THREE_SALE_AMOUNT;
	}

	/**
	 * Sets the value of the LAST_SALE_THREE_SALE_AMOUNT property.
	 * 
	 * @param aLAST_SALE_THREE_SALE_AMOUNT
	 *            the new value of the LAST_SALE_THREE_SALE_AMOUNT property
	 */
	public void setLAST_SALE_THREE_SALE_AMOUNT(String aLAST_SALE_THREE_SALE_AMOUNT) {
		LAST_SALE_THREE_SALE_AMOUNT = aLAST_SALE_THREE_SALE_AMOUNT;
	}

	/**
	 * Access method for the LAST_SALE_THREE_SALE_DATE property.
	 * 
	 * @return the current value of the LAST_SALE_THREE_SALE_DATE property
	 */
	public String getLAST_SALE_THREE_SALE_DATE() {
		return LAST_SALE_THREE_SALE_DATE;
	}

	/**
	 * Sets the value of the LAST_SALE_THREE_SALE_DATE property.
	 * 
	 * @param aLAST_SALE_THREE_SALE_DATE
	 *            the new value of the LAST_SALE_THREE_SALE_DATE property
	 */
	public void setLAST_SALE_THREE_SALE_DATE(String aLAST_SALE_THREE_SALE_DATE) {
		LAST_SALE_THREE_SALE_DATE = aLAST_SALE_THREE_SALE_DATE;
	}

	/**
	 * Access method for the BUILDING_DATE_LINE_1_SUBPART property.
	 * 
	 * @return the current value of the BUILDING_DATE_LINE_1_SUBPART property
	 */
	public String getBUILDING_DATA_LINE_1_SUBPART() {
		return BUILDING_DATA_LINE_1_SUBPART;
	}

	/**
	 * Sets the value of the BUILDING_DATE_LINE_1_SUBPART property.
	 * 
	 * @param aBUILDING_DATA_LINE_1_SUBPART
	 */
	public void setBUILDING_DATA_LINE_1_SUBPART(String aBUILDING_DATA_LINE_1_SUBPART) {
		BUILDING_DATA_LINE_1_SUBPART = aBUILDING_DATA_LINE_1_SUBPART;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_1_DESIGN_TY property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_1_DESIGN_TY property
	 */
	public String getBUILDING_DATA_LINE_1_DESIGN_TY() {
		return BUILDING_DATA_LINE_1_DESIGN_TY;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_1_DESIGN_TY property.
	 * 
	 * @param aBUILDING_DATA_LINE_1_DESIGN_TY
	 *            the new value of the BUILDING_DATA_LINE_1_DESIGN_TY property
	 */
	public void setBUILDING_DATA_LINE_1_DESIGN_TY(String aBUILDING_DATA_LINE_1_DESIGN_TY) {
		BUILDING_DATA_LINE_1_DESIGN_TY = aBUILDING_DATA_LINE_1_DESIGN_TY;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_1_QUALITY property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_1_QUALITY property
	 */
	public String getBUILDING_DATA_LINE_1_QUALITY() {
		return BUILDING_DATA_LINE_1_QUALITY;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_1_QUALITY property.
	 * 
	 * @param aBUILDING_DATA_LINE_1_QUALITY
	 *            the new value of the BUILDING_DATA_LINE_1_QUALITY property
	 */
	public void setBUILDING_DATA_LINE_1_QUALITY(String aBUILDING_DATA_LINE_1_QUALITY) {
		BUILDING_DATA_LINE_1_QUALITY = aBUILDING_DATA_LINE_1_QUALITY;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_1_YEAR_BUIL property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_1_YEAR_BUIL property
	 */
	public String getBUILDING_DATA_LINE_1_YEAR_BUIL() {
		return BUILDING_DATA_LINE_1_YEAR_BUIL;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_1_YEAR_BUIL property.
	 * 
	 * @param aBUILDING_DATA_LINE_1_YEAR_BUIL
	 *            the new value of the BUILDING_DATA_LINE_1_YEAR_BUIL property
	 */
	public void setBUILDING_DATA_LINE_1_YEAR_BUIL(String aBUILDING_DATA_LINE_1_YEAR_BUIL) {
		BUILDING_DATA_LINE_1_YEAR_BUIL = aBUILDING_DATA_LINE_1_YEAR_BUIL;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_1_NO_OF_UNI property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_1_NO_OF_UNI property
	 */
	public String getBUILDING_DATA_LINE_1_NO_OF_UNI() {
		return BUILDING_DATA_LINE_1_NO_OF_UNI;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_1_NO_OF_UNI property.
	 * 
	 * @param aBUILDING_DATA_LINE_1_NO_OF_UNI
	 *            the new value of the BUILDING_DATA_LINE_1_NO_OF_UNI property
	 */
	public void setBUILDING_DATA_LINE_1_NO_OF_UNI(String aBUILDING_DATA_LINE_1_NO_OF_UNI) {
		BUILDING_DATA_LINE_1_NO_OF_UNI = aBUILDING_DATA_LINE_1_NO_OF_UNI;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_1_NO_OF_BED property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_1_NO_OF_BED property
	 */
	public String getBUILDING_DATA_LINE_1_NO_OF_BED() {
		return BUILDING_DATA_LINE_1_NO_OF_BED;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_1_NO_OF_BED property.
	 * 
	 * @param aBUILDING_DATA_LINE_1_NO_OF_BED
	 *            the new value of the BUILDING_DATA_LINE_1_NO_OF_BED property
	 */
	public void setBUILDING_DATA_LINE_1_NO_OF_BED(String aBUILDING_DATA_LINE_1_NO_OF_BED) {
		BUILDING_DATA_LINE_1_NO_OF_BED = aBUILDING_DATA_LINE_1_NO_OF_BED;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_1_NO_OF_BAT property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_1_NO_OF_BAT property
	 */
	public String getBUILDING_DATA_LINE_1_NO_OF_BAT() {
		return BUILDING_DATA_LINE_1_NO_OF_BAT;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_1_NO_OF_BAT property.
	 * 
	 * @param aBUILDING_DATA_LINE_1_NO_OF_BAT
	 *            the new value of the BUILDING_DATA_LINE_1_NO_OF_BAT property
	 */
	public void setBUILDING_DATA_LINE_1_NO_OF_BAT(String aBUILDING_DATA_LINE_1_NO_OF_BAT) {
		BUILDING_DATA_LINE_1_NO_OF_BAT = aBUILDING_DATA_LINE_1_NO_OF_BAT;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_1_SQUARE_FE property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_1_SQUARE_FE property
	 */
	public String getBUILDING_DATA_LINE_1_SQUARE_FE() {
		return BUILDING_DATA_LINE_1_SQUARE_FE;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_1_SQUARE_FE property.
	 * 
	 * @param aBUILDING_DATA_LINE_1_SQUARE_FE
	 *            the new value of the BUILDING_DATA_LINE_1_SQUARE_FE property
	 */
	public void setBUILDING_DATA_LINE_1_SQUARE_FE(String aBUILDING_DATA_LINE_1_SQUARE_FE) {
		BUILDING_DATA_LINE_1_SQUARE_FE = aBUILDING_DATA_LINE_1_SQUARE_FE;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_2_SUBPART property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_2_SUBPART property
	 */
	public String getBUILDING_DATA_LINE_2_SUBPART() {
		return BUILDING_DATA_LINE_2_SUBPART;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_2_SUBPART property.
	 * 
	 * @param aBUILDING_DATA_LINE_2_SUBPART
	 *            the new value of the BUILDING_DATA_LINE_2_SUBPART property
	 */
	public void setBUILDING_DATA_LINE_2_SUBPART(String aBUILDING_DATA_LINE_2_SUBPART) {
		BUILDING_DATA_LINE_2_SUBPART = aBUILDING_DATA_LINE_2_SUBPART;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_2_DESIGN_TY property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_2_DESIGN_TY property
	 */
	public String getBUILDING_DATA_LINE_2_DESIGN_TY() {
		return BUILDING_DATA_LINE_2_DESIGN_TY;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_2_DESIGN_TY property.
	 * 
	 * @param aBUILDING_DATA_LINE_2_DESIGN_TY
	 *            the new value of the BUILDING_DATA_LINE_2_DESIGN_TY property
	 */
	public void setBUILDING_DATA_LINE_2_DESIGN_TY(String aBUILDING_DATA_LINE_2_DESIGN_TY) {
		BUILDING_DATA_LINE_2_DESIGN_TY = aBUILDING_DATA_LINE_2_DESIGN_TY;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_2_QUALITY property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_2_QUALITY property
	 */
	public String getBUILDING_DATA_LINE_2_QUALITY() {
		return BUILDING_DATA_LINE_2_QUALITY;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_2_QUALITY property.
	 * 
	 * @param aBUILDING_DATA_LINE_2_QUALITY
	 *            the new value of the BUILDING_DATA_LINE_2_QUALITY property
	 */
	public void setBUILDING_DATA_LINE_2_QUALITY(String aBUILDING_DATA_LINE_2_QUALITY) {
		BUILDING_DATA_LINE_2_QUALITY = aBUILDING_DATA_LINE_2_QUALITY;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_2_YEAR_BUIL property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_2_YEAR_BUIL property
	 */
	public String getBUILDING_DATA_LINE_2_YEAR_BUIL() {
		return BUILDING_DATA_LINE_2_YEAR_BUIL;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_2_YEAR_BUIL property.
	 * 
	 * @param aBUILDING_DATA_LINE_2_YEAR_BUIL
	 *            the new value of the BUILDING_DATA_LINE_2_YEAR_BUIL property
	 */
	public void setBUILDING_DATA_LINE_2_YEAR_BUIL(String aBUILDING_DATA_LINE_2_YEAR_BUIL) {
		BUILDING_DATA_LINE_2_YEAR_BUIL = aBUILDING_DATA_LINE_2_YEAR_BUIL;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_2_NO_OF_UNI property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_2_NO_OF_UNI property
	 */
	public String getBUILDING_DATA_LINE_2_NO_OF_UNI() {
		return BUILDING_DATA_LINE_2_NO_OF_UNI;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_2_NO_OF_UNI property.
	 * 
	 * @param aBUILDING_DATA_LINE_2_NO_OF_UNI
	 *            the new value of the BUILDING_DATA_LINE_2_NO_OF_UNI property
	 */
	public void setBUILDING_DATA_LINE_2_NO_OF_UNI(String aBUILDING_DATA_LINE_2_NO_OF_UNI) {
		BUILDING_DATA_LINE_2_NO_OF_UNI = aBUILDING_DATA_LINE_2_NO_OF_UNI;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_2_NO_OF_BED property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_2_NO_OF_BED property
	 */
	public String getBUILDING_DATA_LINE_2_NO_OF_BED() {
		return BUILDING_DATA_LINE_2_NO_OF_BED;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_2_NO_OF_BED property.
	 * 
	 * @param aBUILDING_DATA_LINE_2_NO_OF_BED
	 *            the new value of the BUILDING_DATA_LINE_2_NO_OF_BED property
	 */
	public void setBUILDING_DATA_LINE_2_NO_OF_BED(String aBUILDING_DATA_LINE_2_NO_OF_BED) {
		BUILDING_DATA_LINE_2_NO_OF_BED = aBUILDING_DATA_LINE_2_NO_OF_BED;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_2_NO_OF_BAT property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_2_NO_OF_BAT property
	 */
	public String getBUILDING_DATA_LINE_2_NO_OF_BAT() {
		return BUILDING_DATA_LINE_2_NO_OF_BAT;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_2_NO_OF_BAT property.
	 * 
	 * @param aBUILDING_DATA_LINE_2_NO_OF_BAT
	 *            the new value of the BUILDING_DATA_LINE_2_NO_OF_BAT property
	 */
	public void setBUILDING_DATA_LINE_2_NO_OF_BAT(String aBUILDING_DATA_LINE_2_NO_OF_BAT) {
		BUILDING_DATA_LINE_2_NO_OF_BAT = aBUILDING_DATA_LINE_2_NO_OF_BAT;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_2_SQUARE_FE property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_2_SQUARE_FE property
	 */
	public String getBUILDING_DATA_LINE_2_SQUARE_FE() {
		return BUILDING_DATA_LINE_2_SQUARE_FE;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_2_SQUARE_FE property.
	 * 
	 * @param aBUILDING_DATA_LINE_2_SQUARE_FE
	 *            the new value of the BUILDING_DATA_LINE_2_SQUARE_FE property
	 */
	public void setBUILDING_DATA_LINE_2_SQUARE_FE(String aBUILDING_DATA_LINE_2_SQUARE_FE) {
		BUILDING_DATA_LINE_2_SQUARE_FE = aBUILDING_DATA_LINE_2_SQUARE_FE;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_3_SUBPART property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_3_SUBPART property
	 */
	public String getBUILDING_DATA_LINE_3_SUBPART() {
		return BUILDING_DATA_LINE_3_SUBPART;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_3_SUBPART property.
	 * 
	 * @param aBUILDING_DATA_LINE_3_SUBPART
	 *            the new value of the BUILDING_DATA_LINE_3_SUBPART property
	 */
	public void setBUILDING_DATA_LINE_3_SUBPART(String aBUILDING_DATA_LINE_3_SUBPART) {
		BUILDING_DATA_LINE_3_SUBPART = aBUILDING_DATA_LINE_3_SUBPART;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_3_DESIGN_TY property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_3_DESIGN_TY property
	 */
	public String getBUILDING_DATA_LINE_3_DESIGN_TY() {
		return BUILDING_DATA_LINE_3_DESIGN_TY;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_3_DESIGN_TY property.
	 * 
	 * @param aBUILDING_DATA_LINE_3_DESIGN_TY
	 *            the new value of the BUILDING_DATA_LINE_3_DESIGN_TY property
	 */
	public void setBUILDING_DATA_LINE_3_DESIGN_TY(String aBUILDING_DATA_LINE_3_DESIGN_TY) {
		BUILDING_DATA_LINE_3_DESIGN_TY = aBUILDING_DATA_LINE_3_DESIGN_TY;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_3_QUALITY property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_3_QUALITY property
	 */
	public String getBUILDING_DATA_LINE_3_QUALITY() {
		return BUILDING_DATA_LINE_3_QUALITY;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_3_QUALITY property.
	 * 
	 * @param aBUILDING_DATA_LINE_3_QUALITY
	 *            the new value of the BUILDING_DATA_LINE_3_QUALITY property
	 */
	public void setBUILDING_DATA_LINE_3_QUALITY(String aBUILDING_DATA_LINE_3_QUALITY) {
		BUILDING_DATA_LINE_3_QUALITY = aBUILDING_DATA_LINE_3_QUALITY;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_3_YEAR_BUIL property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_3_YEAR_BUIL property
	 */
	public String getBUILDING_DATA_LINE_3_YEAR_BUIL() {
		return BUILDING_DATA_LINE_3_YEAR_BUIL;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_3_YEAR_BUIL property.
	 * 
	 * @param aBUILDING_DATA_LINE_3_YEAR_BUIL
	 *            the new value of the BUILDING_DATA_LINE_3_YEAR_BUIL property
	 */
	public void setBUILDING_DATA_LINE_3_YEAR_BUIL(String aBUILDING_DATA_LINE_3_YEAR_BUIL) {
		BUILDING_DATA_LINE_3_YEAR_BUIL = aBUILDING_DATA_LINE_3_YEAR_BUIL;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_3_NO_OF_UNI property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_3_NO_OF_UNI property
	 */
	public String getBUILDING_DATA_LINE_3_NO_OF_UNI() {
		return BUILDING_DATA_LINE_3_NO_OF_UNI;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_3_NO_OF_UNI property.
	 * 
	 * @param aBUILDING_DATA_LINE_3_NO_OF_UNI
	 *            the new value of the BUILDING_DATA_LINE_3_NO_OF_UNI property
	 */
	public void setBUILDING_DATA_LINE_3_NO_OF_UNI(String aBUILDING_DATA_LINE_3_NO_OF_UNI) {
		BUILDING_DATA_LINE_3_NO_OF_UNI = aBUILDING_DATA_LINE_3_NO_OF_UNI;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_3_NO_OF_BED property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_3_NO_OF_BED property
	 */
	public String getBUILDING_DATA_LINE_3_NO_OF_BED() {
		return BUILDING_DATA_LINE_3_NO_OF_BED;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_3_NO_OF_BED property.
	 * 
	 * @param aBUILDING_DATA_LINE_3_NO_OF_BED
	 *            the new value of the BUILDING_DATA_LINE_3_NO_OF_BED property
	 */
	public void setBUILDING_DATA_LINE_3_NO_OF_BED(String aBUILDING_DATA_LINE_3_NO_OF_BED) {
		BUILDING_DATA_LINE_3_NO_OF_BED = aBUILDING_DATA_LINE_3_NO_OF_BED;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_3_NO_OF_BAT property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_3_NO_OF_BAT property
	 */
	public String getBUILDING_DATA_LINE_3_NO_OF_BAT() {
		return BUILDING_DATA_LINE_3_NO_OF_BAT;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_3_NO_OF_BAT property.
	 * 
	 * @param aBUILDING_DATA_LINE_3_NO_OF_BAT
	 *            the new value of the BUILDING_DATA_LINE_3_NO_OF_BAT property
	 */
	public void setBUILDING_DATA_LINE_3_NO_OF_BAT(String aBUILDING_DATA_LINE_3_NO_OF_BAT) {
		BUILDING_DATA_LINE_3_NO_OF_BAT = aBUILDING_DATA_LINE_3_NO_OF_BAT;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_3_SQUARE_FE property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_3_SQUARE_FE property
	 */
	public String getBUILDING_DATA_LINE_3_SQUARE_FE() {
		return BUILDING_DATA_LINE_3_SQUARE_FE;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_3_SQUARE_FE property.
	 * 
	 * @param aBUILDING_DATA_LINE_3_SQUARE_FE
	 *            the new value of the BUILDING_DATA_LINE_3_SQUARE_FE property
	 */
	public void setBUILDING_DATA_LINE_3_SQUARE_FE(String aBUILDING_DATA_LINE_3_SQUARE_FE) {
		BUILDING_DATA_LINE_3_SQUARE_FE = aBUILDING_DATA_LINE_3_SQUARE_FE;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_4_SUBPART property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_4_SUBPART property
	 */
	public String getBUILDING_DATA_LINE_4_SUBPART() {
		return BUILDING_DATA_LINE_4_SUBPART;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_4_SUBPART property.
	 * 
	 * @param aBUILDING_DATA_LINE_4_SUBPART
	 *            the new value of the BUILDING_DATA_LINE_4_SUBPART property
	 */
	public void setBUILDING_DATA_LINE_4_SUBPART(String aBUILDING_DATA_LINE_4_SUBPART) {
		BUILDING_DATA_LINE_4_SUBPART = aBUILDING_DATA_LINE_4_SUBPART;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_4_DESIGN_TY property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_4_DESIGN_TY property
	 */
	public String getBUILDING_DATA_LINE_4_DESIGN_TY() {
		return BUILDING_DATA_LINE_4_DESIGN_TY;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_4_DESIGN_TY property.
	 * 
	 * @param aBUILDING_DATA_LINE_4_DESIGN_TY
	 *            the new value of the BUILDING_DATA_LINE_4_DESIGN_TY property
	 */
	public void setBUILDING_DATA_LINE_4_DESIGN_TY(String aBUILDING_DATA_LINE_4_DESIGN_TY) {
		BUILDING_DATA_LINE_4_DESIGN_TY = aBUILDING_DATA_LINE_4_DESIGN_TY;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_4_QUALITY property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_4_QUALITY property
	 */
	public String getBUILDING_DATA_LINE_4_QUALITY() {
		return BUILDING_DATA_LINE_4_QUALITY;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_4_QUALITY property.
	 * 
	 * @param aBUILDING_DATA_LINE_4_QUALITY
	 *            the new value of the BUILDING_DATA_LINE_4_QUALITY property
	 */
	public void setBUILDING_DATA_LINE_4_QUALITY(String aBUILDING_DATA_LINE_4_QUALITY) {
		BUILDING_DATA_LINE_4_QUALITY = aBUILDING_DATA_LINE_4_QUALITY;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_4_YEAR_BUIL property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_4_YEAR_BUIL property
	 */
	public String getBUILDING_DATA_LINE_4_YEAR_BUIL() {
		return BUILDING_DATA_LINE_4_YEAR_BUIL;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_4_YEAR_BUIL property.
	 * 
	 * @param aBUILDING_DATA_LINE_4_YEAR_BUIL
	 *            the new value of the BUILDING_DATA_LINE_4_YEAR_BUIL property
	 */
	public void setBUILDING_DATA_LINE_4_YEAR_BUIL(String aBUILDING_DATA_LINE_4_YEAR_BUIL) {
		BUILDING_DATA_LINE_4_YEAR_BUIL = aBUILDING_DATA_LINE_4_YEAR_BUIL;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_4_NO_OF_UNI property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_4_NO_OF_UNI property
	 */
	public String getBUILDING_DATA_LINE_4_NO_OF_UNI() {
		return BUILDING_DATA_LINE_4_NO_OF_UNI;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_4_NO_OF_UNI property.
	 * 
	 * @param aBUILDING_DATA_LINE_4_NO_OF_UNI
	 *            the new value of the BUILDING_DATA_LINE_4_NO_OF_UNI property
	 */
	public void setBUILDING_DATA_LINE_4_NO_OF_UNI(String aBUILDING_DATA_LINE_4_NO_OF_UNI) {
		BUILDING_DATA_LINE_4_NO_OF_UNI = aBUILDING_DATA_LINE_4_NO_OF_UNI;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_4_NO_OF_BED property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_4_NO_OF_BED property
	 */
	public String getBUILDING_DATA_LINE_4_NO_OF_BED() {
		return BUILDING_DATA_LINE_4_NO_OF_BED;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_4_NO_OF_BED property.
	 * 
	 * @param aBUILDING_DATA_LINE_4_NO_OF_BED
	 *            the new value of the BUILDING_DATA_LINE_4_NO_OF_BED property
	 */
	public void setBUILDING_DATA_LINE_4_NO_OF_BED(String aBUILDING_DATA_LINE_4_NO_OF_BED) {
		BUILDING_DATA_LINE_4_NO_OF_BED = aBUILDING_DATA_LINE_4_NO_OF_BED;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_4_NO_OF_BAT property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_4_NO_OF_BAT property
	 */
	public String getBUILDING_DATA_LINE_4_NO_OF_BAT() {
		return BUILDING_DATA_LINE_4_NO_OF_BAT;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_4_NO_OF_BAT property.
	 * 
	 * @param aBUILDING_DATA_LINE_4_NO_OF_BAT
	 *            the new value of the BUILDING_DATA_LINE_4_NO_OF_BAT property
	 */
	public void setBUILDING_DATA_LINE_4_NO_OF_BAT(String aBUILDING_DATA_LINE_4_NO_OF_BAT) {
		BUILDING_DATA_LINE_4_NO_OF_BAT = aBUILDING_DATA_LINE_4_NO_OF_BAT;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_4_SQUARE_FE property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_4_SQUARE_FE property
	 */
	public String getBUILDING_DATA_LINE_4_SQUARE_FE() {
		return BUILDING_DATA_LINE_4_SQUARE_FE;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_4_SQUARE_FE property.
	 * 
	 * @param aBUILDING_DATA_LINE_4_SQUARE_FE
	 *            the new value of the BUILDING_DATA_LINE_4_SQUARE_FE property
	 */
	public void setBUILDING_DATA_LINE_4_SQUARE_FE(String aBUILDING_DATA_LINE_4_SQUARE_FE) {
		BUILDING_DATA_LINE_4_SQUARE_FE = aBUILDING_DATA_LINE_4_SQUARE_FE;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_5_SUBPART property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_5_SUBPART property
	 */
	public String getBUILDING_DATA_LINE_5_SUBPART() {
		return BUILDING_DATA_LINE_5_SUBPART;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_5_SUBPART property.
	 * 
	 * @param aBUILDING_DATA_LINE_5_SUBPART
	 *            the new value of the BUILDING_DATA_LINE_5_SUBPART property
	 */
	public void setBUILDING_DATA_LINE_5_SUBPART(String aBUILDING_DATA_LINE_5_SUBPART) {
		BUILDING_DATA_LINE_5_SUBPART = aBUILDING_DATA_LINE_5_SUBPART;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_5_DESIGN_TY property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_5_DESIGN_TY property
	 */
	public String getBUILDING_DATA_LINE_5_DESIGN_TY() {
		return BUILDING_DATA_LINE_5_DESIGN_TY;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_5_DESIGN_TY property.
	 * 
	 * @param aBUILDING_DATA_LINE_5_DESIGN_TY
	 *            the new value of the BUILDING_DATA_LINE_5_DESIGN_TY property
	 */
	public void setBUILDING_DATA_LINE_5_DESIGN_TY(String aBUILDING_DATA_LINE_5_DESIGN_TY) {
		BUILDING_DATA_LINE_5_DESIGN_TY = aBUILDING_DATA_LINE_5_DESIGN_TY;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_5_QUALITY property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_5_QUALITY property
	 */
	public String getBUILDING_DATA_LINE_5_QUALITY() {
		return BUILDING_DATA_LINE_5_QUALITY;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_5_QUALITY property.
	 * 
	 * @param aBUILDING_DATA_LINE_5_QUALITY
	 *            the new value of the BUILDING_DATA_LINE_5_QUALITY property
	 */
	public void setBUILDING_DATA_LINE_5_QUALITY(String aBUILDING_DATA_LINE_5_QUALITY) {
		BUILDING_DATA_LINE_5_QUALITY = aBUILDING_DATA_LINE_5_QUALITY;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_5_YEAR_BUIL property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_5_YEAR_BUIL property
	 */
	public String getBUILDING_DATA_LINE_5_YEAR_BUIL() {
		return BUILDING_DATA_LINE_5_YEAR_BUIL;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_5_YEAR_BUIL property.
	 * 
	 * @param aBUILDING_DATA_LINE_5_YEAR_BUIL
	 *            the new value of the BUILDING_DATA_LINE_5_YEAR_BUIL property
	 */
	public void setBUILDING_DATA_LINE_5_YEAR_BUIL(String aBUILDING_DATA_LINE_5_YEAR_BUIL) {
		BUILDING_DATA_LINE_5_YEAR_BUIL = aBUILDING_DATA_LINE_5_YEAR_BUIL;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_5_NO_OF_UNI property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_5_NO_OF_UNI property
	 */
	public String getBUILDING_DATA_LINE_5_NO_OF_UNI() {
		return BUILDING_DATA_LINE_5_NO_OF_UNI;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_5_NO_OF_UNI property.
	 * 
	 * @param aBUILDING_DATA_LINE_5_NO_OF_UNI
	 *            the new value of the BUILDING_DATA_LINE_5_NO_OF_UNI property
	 */
	public void setBUILDING_DATA_LINE_5_NO_OF_UNI(String aBUILDING_DATA_LINE_5_NO_OF_UNI) {
		BUILDING_DATA_LINE_5_NO_OF_UNI = aBUILDING_DATA_LINE_5_NO_OF_UNI;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_5_NO_OF_BED property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_5_NO_OF_BED property
	 */
	public String getBUILDING_DATA_LINE_5_NO_OF_BED() {
		return BUILDING_DATA_LINE_5_NO_OF_BED;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_5_NO_OF_BED property.
	 * 
	 * @param aBUILDING_DATA_LINE_5_NO_OF_BED
	 *            the new value of the BUILDING_DATA_LINE_5_NO_OF_BED property
	 */
	public void setBUILDING_DATA_LINE_5_NO_OF_BED(String aBUILDING_DATA_LINE_5_NO_OF_BED) {
		BUILDING_DATA_LINE_5_NO_OF_BED = aBUILDING_DATA_LINE_5_NO_OF_BED;
	}

	/**
	 * Access method for the BUILDING_DATA_LINE_5_NO_OF_BAT property.
	 * 
	 * @return the current value of the BUILDING_DATA_LINE_5_NO_OF_BAT property
	 */
	public String getBUILDING_DATA_LINE_5_NO_OF_BAT() {
		return BUILDING_DATA_LINE_5_NO_OF_BAT;
	}

	/**
	 * Sets the value of the BUILDING_DATA_LINE_5_NO_OF_BAT property.
	 * 
	 * @param aBUILDING_DATA_LINE_5_NO_OF_BAT
	 *            the new value of the BUILDING_DATA_LINE_5_NO_OF_BAT property
	 */
	public void setBUILDING_DATA_LINE_5_NO_OF_BAT(String aBUILDING_DATA_LINE_5_NO_OF_BAT) {
		BUILDING_DATA_LINE_5_NO_OF_BAT = aBUILDING_DATA_LINE_5_NO_OF_BAT;
	}

	/**
	 * Access method for the LEGAL_DESCRIPTION_1 property.
	 * 
	 * @return the current value of the LEGAL_DESCRIPTION_1 property
	 */
	public String getLEGAL_DESCRIPTION_1() {
		return LEGAL_DESCRIPTION_1;
	}

	/**
	 * Sets the value of the LEGAL_DESCRIPTION_1 property.
	 * 
	 * @param aLEGAL_DESCRIPTION_1
	 *            the new value of the LEGAL_DESCRIPTION_1 property
	 */
	public void setLEGAL_DESCRIPTION_1(String aLEGAL_DESCRIPTION_1) {
		LEGAL_DESCRIPTION_1 = aLEGAL_DESCRIPTION_1;
	}

	/**
	 * Access method for the LEGAL_DESCRIPTION_2 property.
	 * 
	 * @return the current value of the LEGAL_DESCRIPTION_2 property
	 */
	public String getLEGAL_DESCRIPTION_2() {
		return LEGAL_DESCRIPTION_2;
	}

	/**
	 * Sets the value of the LEGAL_DESCRIPTION_2 property.
	 * 
	 * @param aLEGAL_DESCRIPTION_2
	 *            the new value of the LEGAL_DESCRIPTION_2 property
	 */
	public void setLEGAL_DESCRIPTION_2(String aLEGAL_DESCRIPTION_2) {
		LEGAL_DESCRIPTION_2 = aLEGAL_DESCRIPTION_2;
	}

	/**
	 * Access method for the LEGAL_DESCRIPTION_3 property.
	 * 
	 * @return the current value of the LEGAL_DESCRIPTION_3 property
	 */
	public String getLEGAL_DESCRIPTION_3() {
		return LEGAL_DESCRIPTION_3;
	}

	/**
	 * Sets the value of the LEGAL_DESCRIPTION_3 property.
	 * 
	 * @param aLEGAL_DESCRIPTION_3
	 *            the new value of the LEGAL_DESCRIPTION_3 property
	 */
	public void setLEGAL_DESCRIPTION_3(String aLEGAL_DESCRIPTION_3) {
		LEGAL_DESCRIPTION_3 = aLEGAL_DESCRIPTION_3;
	}

	/**
	 * Access method for the LEGAL_DESCRIPTION_4 property.
	 * 
	 * @return the current value of the LEGAL_DESCRIPTION_4 property
	 */
	public String getLEGAL_DESCRIPTION_4() {
		return LEGAL_DESCRIPTION_4;
	}

	/**
	 * Sets the value of the LEGAL_DESCRIPTION_4 property.
	 * 
	 * @param aLEGAL_DESCRIPTION_4
	 *            the new value of the LEGAL_DESCRIPTION_4 property
	 */
	public void setLEGAL_DESCRIPTION_4(String aLEGAL_DESCRIPTION_4) {
		LEGAL_DESCRIPTION_4 = aLEGAL_DESCRIPTION_4;
	}

	/**
	 * Access method for the LEGAL_DESCRIPTION_5 property.
	 * 
	 * @return the current value of the LEGAL_DESCRIPTION_5 property
	 */
	public String getLEGAL_DESCRIPTION_5() {
		return LEGAL_DESCRIPTION_5;
	}

	/**
	 * Sets the value of the LEGAL_DESCRIPTION_5 property.
	 * 
	 * @param aLEGAL_DESCRIPTION_5
	 *            the new value of the LEGAL_DESCRIPTION_5 property
	 */
	public void setLEGAL_DESCRIPTION_5(String aLEGAL_DESCRIPTION_5) {
		LEGAL_DESCRIPTION_5 = aLEGAL_DESCRIPTION_5;
	}

	/**
	 * Access method for the LEGAL_DESCRIPTION_6 property.
	 * 
	 * @return the current value of the LEGAL_DESCRIPTION_6 property
	 */
	public String getLEGAL_DESCRIPTION_6() {
		return LEGAL_DESCRIPTION_6;
	}

	/**
	 * Sets the value of the LEGAL_DESCRIPTION_6 property.
	 * 
	 * @param aLEGAL_DESCRIPTION_6
	 *            the new value of the LEGAL_DESCRIPTION_6 property
	 */
	public void setLEGAL_DESCRIPTION_6(String aLEGAL_DESCRIPTION_6) {
		LEGAL_DESCRIPTION_6 = aLEGAL_DESCRIPTION_6;
	}

	/**
	 * Gets the bUILDING_DATA_LINE_5_SQUARE_FE
	 * 
	 * @return Returns a String
	 */
	public String getBUILDING_DATA_LINE_5_SQUARE_FE() {
		return BUILDING_DATA_LINE_5_SQUARE_FE;
	}

	/**
	 * Sets the bUILDING_DATA_LINE_5_SQUARE_FE
	 * 
	 * @param bUILDING_DATA_LINE_5_SQUARE_FE
	 *            The bUILDING_DATA_LINE_5_SQUARE_FE to set
	 */
	public void setBUILDING_DATA_LINE_5_SQUARE_FE(String bUILDING_DATA_LINE_5_SQUARE_FE) {
		BUILDING_DATA_LINE_5_SQUARE_FE = bUILDING_DATA_LINE_5_SQUARE_FE;
	}

}
