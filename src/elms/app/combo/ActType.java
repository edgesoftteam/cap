/*
 * Created on Nov 14, 2007
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package elms.app.combo;

/**
 * @author rgovind
 * 
 *         To change the template for this generated type comment go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ActType {

	protected int deptId;
	protected String type;
	protected int ptypeId;
	protected String desc;
	protected int typeId;

	/**
	 * @return
	 */
	public int getDeptId() {
		return deptId;
	}

	/**
	 * @return
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * @return
	 */
	public int getPtypeId() {
		return ptypeId;
	}

	/**
	 * @return
	 */
	public String getType() {
		return type;
	}

	/**
	 * @return
	 */
	public int getTypeId() {
		return typeId;
	}

	/**
	 * @param i
	 */
	public void setDeptId(int i) {
		deptId = i;
	}

	/**
	 * @param string
	 */
	public void setDesc(String string) {
		desc = string;
	}

	/**
	 * @param i
	 */
	public void setPtypeId(int i) {
		ptypeId = i;
	}

	/**
	 * @param string
	 */
	public void setType(String string) {
		type = string;
	}

	/**
	 * @param i
	 */
	public void setTypeId(int i) {
		typeId = i;
	}

}
