/*
 * Created on Nov 14, 2007
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package elms.app.combo;

/**
 * @author rgovind
 * 
 *         To change the template for this generated type comment go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class SubProject {

	protected String sprojId;
	protected String sprojName;

	/**
	 * @return
	 */
	public String getSprojId() {
		return sprojId;
	}

	/**
	 * @return
	 */
	public String getSprojName() {
		return sprojName;
	}

	/**
	 * @param string
	 */
	public void setSprojId(String string) {
		sprojId = string;
	}

	/**
	 * @param string
	 */
	public void setSprojName(String string) {
		sprojName = string;
	}

}
