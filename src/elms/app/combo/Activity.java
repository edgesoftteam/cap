/*
 * Created on Nov 14, 2007
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package elms.app.combo;

/**
 * @author rgovind
 * 
 *         To change the template for this generated type comment go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class Activity {

	protected String actName;
	protected String mapd;
	protected String deptCode;
	protected String actType;
	protected String actId;
	protected String feeAmt;
	protected String amountDue;
	protected String actNbr;

	public String getActName() {
		return actName;
	}

	/**
	 * @return
	 */
	public String getDeptCode() {
		return deptCode;
	}

	/**
	 * @param string
	 */
	public void setActName(String string) {
		actName = string;
	}

	/**
	 * @param string
	 */
	public void setDeptCode(String string) {
		deptCode = string;
	}

	/**
	 * @return
	 */
	public String getMapd() {
		return mapd;
	}

	/**
	 * @param string
	 */
	public void setMapd(String string) {
		mapd = string;
	}

	/**
	 * @return
	 */
	public String getActType() {
		return actType;
	}

	/**
	 * @param string
	 */
	public void setActType(String string) {
		actType = string;
	}

	/**
	 * @return
	 */
	public String getActId() {
		return actId;
	}

	/**
	 * @param string
	 */
	public void setActId(String string) {
		actId = string;
	}

	/**
	 * @return
	 */
	public String getAmountDue() {
		return amountDue;
	}

	/**
	 * @return
	 */
	public String getFeeAmt() {
		return feeAmt;
	}

	/**
	 * @param string
	 */
	public void setAmountDue(String string) {
		amountDue = string;
	}

	/**
	 * @param string
	 */
	public void setFeeAmt(String string) {
		feeAmt = string;
	}

	/**
	 * @return
	 */
	public String getActNbr() {
		return actNbr;
	}

	/**
	 * @param string
	 */
	public void setActNbr(String string) {
		actNbr = string;
	}

}
