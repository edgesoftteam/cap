/*
 * Created on Nov 14, 2007
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package elms.app.combo;

/**
 * @author rgovind
 * 
 *         To change the template for this generated type comment go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class Project {

	protected String projId;
	protected String projName;

	/**
	 * @return
	 */
	public String getProjId() {
		return projId;
	}

	/**
	 * @return
	 */
	public String getProjName() {
		return projName;
	}

	/**
	 * @param string
	 */
	public void setProjId(String string) {
		projId = string;
	}

	/**
	 * @param string
	 */
	public void setProjName(String string) {
		projName = string;
	}

}
