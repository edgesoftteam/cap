package elms.app.common;

public class DisplayItem {
	private String fieldOne;
	private String fieldTwo;
	private String fieldThree;
	private String fieldFour;
	private String fieldFive;
	private String fieldSix;
	private String fieldSeven;

	/**
	 * Returns the fieldFive.
	 * 
	 * @return String
	 */
	public DisplayItem() {
	}

	public DisplayItem(String aFieldOne) {
		fieldOne = aFieldOne;
	}

	public String getFieldFive() {
		return fieldFive;
	}

	/**
	 * Returns the fieldFour.
	 * 
	 * @return String
	 */
	public String getFieldFour() {
		return fieldFour;
	}

	/**
	 * Returns the fieldSix.
	 * 
	 * @return String
	 */
	public String getFieldSix() {
		return fieldSix;
	}

	/**
	 * Returns the fieldThree.
	 * 
	 * @return String
	 */
	public String getFieldThree() {
		return fieldThree;
	}

	/**
	 * Returns the fieldTwo.
	 * 
	 * @return String
	 */
	public String getFieldTwo() {
		return fieldTwo;
	}

	/**
	 * Sets the fieldFive.
	 * 
	 * @param fieldFive
	 *            The fieldFive to set
	 */
	public void setFieldFive(String fieldFive) {
		this.fieldFive = fieldFive;
	}

	/**
	 * Sets the fieldFour.
	 * 
	 * @param fieldFour
	 *            The fieldFour to set
	 */
	public void setFieldFour(String fieldFour) {
		this.fieldFour = fieldFour;
	}

	/**
	 * Sets the fieldSix.
	 * 
	 * @param fieldSix
	 *            The fieldSix to set
	 */
	public void setFieldSix(String fieldSix) {
		this.fieldSix = fieldSix;
	}

	/**
	 * Sets the fieldThree.
	 * 
	 * @param fieldThree
	 *            The fieldThree to set
	 */
	public void setFieldThree(String fieldThree) {
		this.fieldThree = fieldThree;
	}

	/**
	 * Sets the fieldTwo.
	 * 
	 * @param fieldTwo
	 *            The fieldTwo to set
	 */
	public void setFieldTwo(String fieldTwo) {
		this.fieldTwo = fieldTwo;
	}

	/**
	 * Returns the fieldOne.
	 * 
	 * @return String
	 */
	public String getFieldOne() {
		return fieldOne;
	}

	/**
	 * Sets the fieldOne.
	 * 
	 * @param fieldOne
	 *            The fieldOne to set
	 */
	public void setFieldOne(String fieldOne) {
		this.fieldOne = fieldOne;
	}

	/**
	 * Returns the fieldSeven.
	 * 
	 * @return String
	 */
	public String getFieldSeven() {
		return fieldSeven;
	}

	/**
	 * Sets the fieldSeven.
	 * 
	 * @param fieldSeven
	 *            The fieldSeven to set
	 */
	public void setFieldSeven(String fieldSeven) {
		this.fieldSeven = fieldSeven;
	}
}
