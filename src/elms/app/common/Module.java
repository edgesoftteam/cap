/*
 * Created on Dec 10, 2007
 *
 * Window - Preferences - Java - Code Style - Code Templates
 */
package elms.app.common;

/**
 * @author Gayathri Window - Preferences - Java - Code Style - Code Templates
 */
public class Module {

	public int id;
	public String description;

	/**
		 * 
		 */
	public Module() {
		super();
	}

	/**
	 * @param id
	 * @param description
	 */
	public Module(int id, String description) {
		super();
		this.id = id;
		this.description = description;
	}

	/**
	 * @return Returns the description.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return Returns the id.
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            The id to set.
	 */
	public void setId(int id) {
		this.id = id;
	}
}