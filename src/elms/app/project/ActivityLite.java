package elms.app.project;

public class ActivityLite {

	protected int activityId;
	protected int lsoId;
	protected String activityNumber;
	protected String activityType;
	protected String activityDescription;
	protected String activityStatus;
	protected String address;

	public ActivityLite() {
	}

	public int getLsoId() {
		return lsoId;
	}

	public void setLsoId(int lsoId) {
		this.lsoId = lsoId;
	}

	public int getActivityId() {
		return activityId;
	}

	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}

	public String getActivityNumber() {
		return activityNumber;
	}

	public void setActivityNumber(String activityNumber) {
		this.activityNumber = activityNumber;
	}

	public String getActivityType() {
		return activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	public String getActivityDescription() {
		return activityDescription;
	}

	public void setActivityDescription(String activityDescription) {
		this.activityDescription = activityDescription;
	}

	public String getActivityStatus() {
		return activityStatus;
	}

	public void setActivityStatus(String activityStatus) {
		this.activityStatus = activityStatus;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}