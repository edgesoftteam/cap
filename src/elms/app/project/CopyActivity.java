package elms.app.project;

/**
 * @author Shekhar
 * 
 *         To change this generated comment edit the template variable "typecomment": Window>Preferences>Java>Templates. To enable and disable the creation of type comments go to Window>Preferences>Java>Code Generation.
 */
public class CopyActivity {

	private Activity activity;
	private boolean conditions;
	private boolean holds;
	private boolean comments;
	private boolean fees;
	private boolean activityPeople;
	private boolean activityTeam;
	private boolean planChecks;
	private int userId;

	/**
	 * Constructor for CopyActivity.
	 */
	public CopyActivity() {
		super();
		this.conditions = true;
		this.holds = true;
		this.comments = true;

	}

	/**
	 * Returns the activity.
	 * 
	 * @return Activity
	 */
	public Activity getActivity() {
		return activity;
	}

	/**
	 * Returns the comments.
	 * 
	 * @return boolean
	 */
	public boolean isComments() {
		return comments;
	}

	/**
	 * Returns the conditions.
	 * 
	 * @return boolean
	 */
	public boolean isConditions() {
		return conditions;
	}

	/**
	 * Returns the fees.
	 * 
	 * @return boolean
	 */
	public boolean isFees() {
		return fees;
	}

	/**
	 * Returns the holds.
	 * 
	 * @return boolean
	 */
	public boolean isHolds() {
		return holds;
	}

	/**
	 * Returns the planChecks.
	 * 
	 * @return boolean
	 */
	public boolean isPlanChecks() {
		return planChecks;
	}

	/**
	 * Sets the activity.
	 * 
	 * @param activity
	 *            The activity to set
	 */
	public void setActivity(Activity activity) {
		this.activity = activity;

		if (this.activity.getActivityDetail().getPlanCheckRequired() != null) {

			if (("Y").equalsIgnoreCase(this.activity.getActivityDetail().getPlanCheckRequired())) {

				this.planChecks = true;
			}
		}
	}

	/**
	 * Sets the comments.
	 * 
	 * @param comments
	 *            The comments to set
	 */
	public void setComments(boolean comments) {
		this.comments = comments;
	}

	/**
	 * Sets the conditions.
	 * 
	 * @param conditions
	 *            The conditions to set
	 */
	public void setConditions(boolean conditions) {
		this.conditions = conditions;
	}

	/**
	 * Sets the fees.
	 * 
	 * @param fees
	 *            The fees to set
	 */
	public void setFees(boolean fees) {
		this.fees = fees;
	}

	/**
	 * Sets the holds.
	 * 
	 * @param holds
	 *            The holds to set
	 */
	public void setHolds(boolean holds) {
		this.holds = holds;
	}

	/**
	 * Sets the planChecks.
	 * 
	 * @param planChecks
	 *            The planChecks to set
	 */
	public void setPlanChecks(boolean planChecks) {
		this.planChecks = planChecks;
	}

	/**
	 * Returns the activityPeople.
	 * 
	 * @return boolean
	 */
	public boolean isActivityPeople() {
		return activityPeople;
	}

	/**
	 * Sets the activityPeople.
	 * 
	 * @param activityPeople
	 *            The activityPeople to set
	 */
	public void setActivityPeople(boolean activityPeople) {
		this.activityPeople = activityPeople;
	}

	/**
	 * Returns the userId.
	 * 
	 * @return int
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * Sets the userId.
	 * 
	 * @param userId
	 *            The userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * Returns the activityTeam.
	 * 
	 * @return boolean
	 */
	public boolean isActivityTeam() {
		return activityTeam;
	}

	/**
	 * Sets the activityTeam.
	 * 
	 * @param activityTeam
	 *            The activityTeam to set
	 */
	public void setActivityTeam(boolean activityTeam) {
		this.activityTeam = activityTeam;
	}

}
