package elms.app.project;

import java.util.Calendar;
import java.util.Date;

import elms.app.admin.ProjectStatus;
import elms.security.User;
import elms.util.StringUtils;

/**
 * @author Anand Belaguly
 */
public class ProjectDetail {

	protected String projectNumber;
	protected String name;
	protected String description;
	protected ProjectStatus projectStatus;
	protected Calendar appliedDate;
	protected String appliedDateString;
	protected Calendar startDate;
	protected Calendar expriationDate;
	protected Calendar completionDate;
	private User createdBy;
	private Calendar created;
	private User updatedBy;
	private Calendar updated;
	private double valuation;
	private String microfilm;
	private int openActivitiesCount;
	protected String label;

	private String Address;

	public ProjectDetail(String projectNumber, String name, String description, ProjectStatus projectStatus, Calendar startDate, Calendar expirationDate, Calendar completionDate, User createdBy, Calendar created, User updatedBy, Calendar updated) {
		this.projectNumber = projectNumber;
		this.name = name;
		this.description = description;
		this.projectStatus = projectStatus;
		this.startDate = startDate;
		this.completionDate = completionDate;
		this.createdBy = createdBy;
		this.created = created;
		this.updatedBy = updatedBy;
		this.updated = updated;

	}

	/**
	 * 3CD17506032B
	 */
	public ProjectDetail() {
		createdBy = new User();
		updatedBy = new User();

	}

	/**
	 * Gets the projectNumber
	 * 
	 * @return Returns a String
	 */
	public String getProjectNumber() {
		if (projectNumber == null)
			projectNumber = "";
		return projectNumber;
	}

	/**
	 * Sets the projectNumber
	 * 
	 * @param projectNumber
	 *            The projectNumber to set
	 */
	public void setProjectNumber(String projectNumber) {
		this.projectNumber = projectNumber;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * Gets the name
	 * 
	 * @return Returns a String
	 */
	public String getName() {
		if (name == null)
			name = "";
		return name;
	}

	/**
	 * Sets the name
	 * 
	 * @param name
	 *            The name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		if (description == null)
			description = "";
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the startDate
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getStartDate() {
		return startDate;
	}

	/**
	 * Sets the startDate
	 * 
	 * @param startDate
	 *            The startDate to set
	 */
	public void setStartDate(Calendar startDate) {
		this.startDate = startDate;
	}

	/**
	 * Sets the startDate
	 * 
	 * @param startDate
	 *            The startDate to set
	 */
	public void setStartDate(Date startDate) {
		if (startDate == null)
			this.startDate = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(startDate);
			this.startDate = calendar;
		}

	}

	/**
	 * Gets the completionDate
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getCompletionDate() {
		return completionDate;
	}

	/**
	 * Sets the completionDate
	 * 
	 * @param completionDate
	 *            The completionDate to set
	 */
	public void setCompletionDate(Calendar completionDate) {
		this.completionDate = completionDate;
	}

	/**
	 * Sets the completionDate
	 * 
	 * @param completionDate
	 *            The completionDate to set
	 */
	public void setCompletionDate(Date completionDate) {
		if (completionDate == null)
			this.completionDate = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(completionDate);
			this.completionDate = calendar;
		}

	}

	/**
	 * Gets the createdBy
	 * 
	 * @return Returns a User
	 */
	public User getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the createdBy
	 * 
	 * @param createdBy
	 *            The createdBy to set
	 */
	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Gets the created
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getCreated() {
		return created;
	}

	/**
	 * Sets the created
	 * 
	 * @param created
	 *            The created to set
	 */
	public void setCreated(Calendar created) {
		this.created = created;
	}

	/**
	 * Sets the created
	 * 
	 * @param created
	 *            The created to set
	 */
	public void setCreated(Date created) {
		if (created == null)
			this.created = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(created);
			this.created = calendar;
		}
	}

	/**
	 * Gets the updatedBy
	 * 
	 * @return Returns a User
	 */
	public User getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Sets the updatedBy
	 * 
	 * @param updatedBy
	 *            The updatedBy to set
	 */
	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Gets the updated
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getUpdated() {
		return updated;
	}

	/**
	 * Sets the updated
	 * 
	 * @param updated
	 *            The updated to set
	 */
	public void setUpdated(Calendar updated) {
		this.updated = updated;
	}

	/**
	 * Sets the updated
	 * 
	 * @param updated
	 *            The updated to set
	 */
	public void setUpdated(Date updated) {
		if (updated == null)
			this.updated = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(updated);
			this.updated = calendar;
		}
	}

	/**
	 * Gets the expriationDate
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getExpriationDate() {
		return expriationDate;
	}

	/**
	 * Sets the expriationDate
	 * 
	 * @param expriationDate
	 *            The expriationDate to set
	 */
	public void setExpriationDate(Calendar expriationDate) {
		this.expriationDate = expriationDate;
	}

	/**
	 * Sets the expriationDate
	 * 
	 * @param expriationDate
	 *            The expriationDate to set
	 */
	public void setExpriationDate(Date expriationDate) {
		if (expriationDate == null)
			this.expriationDate = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(expriationDate);
			this.expriationDate = calendar;
		}
	}

	/**
	 * Gets the valuation
	 * 
	 * @return Returns a double
	 */
	public double getValuation() {
		return valuation;
	}

	/**
	 * Sets the valuation
	 * 
	 * @param valuation
	 *            The valuation to set
	 */
	public void setValuation(double valuation) {
		this.valuation = valuation;
	}

	/**
	 * Gets the projectStatus
	 * 
	 * @return Returns a ProjectStatus
	 */
	public ProjectStatus getProjectStatus() {
		return projectStatus;
	}

	/**
	 * Sets the projectStatus
	 * 
	 * @param projectStatus
	 *            The projectStatus to set
	 */
	public void setProjectStatus(ProjectStatus projectStatus) {
		this.projectStatus = projectStatus;
	}

	/**
	 * Gets the openActivitiesCount
	 * 
	 * @return Returns a int
	 */
	public int getOpenActivitiesCount() {
		return openActivitiesCount;
	}

	/**
	 * Sets the openActivitiesCount
	 * 
	 * @param openActivitiesCount
	 *            The openActivitiesCount to set
	 */
	public void setOpenActivitiesCount(int openActivitiesCount) {
		this.openActivitiesCount = openActivitiesCount;
	}

	/**
	 * Returns the appliedDate.
	 * 
	 * @return Calendar
	 */
	public Calendar getAppliedDate() {
		return appliedDate;
	}

	/**
	 * Sets the appliedDate.
	 * 
	 * @param appliedDate
	 *            The appliedDate to set
	 */
	public void setAppliedDate(Calendar appliedDate) {
		this.appliedDate = appliedDate;
		this.appliedDateString = StringUtils.cal2str(appliedDate);
	}

	/**
	 * Sets the expriationDate
	 * 
	 * @param expriationDate
	 *            The expriationDate to set
	 */
	public void setAppliedDate(Date appliedDate) {
		if (appliedDate == null)
			this.appliedDate = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(appliedDate);
			setAppliedDate(calendar);
		}
	}

	/**
	 * Returns the appliedDateString.
	 * 
	 * @return String
	 */
	public String getAppliedDateString() {
		return appliedDateString;
	}

	/**
	 * Returns the address.
	 * 
	 * @return String
	 */
	public String getAddress() {
		return Address;
	}

	/**
	 * Sets the address.
	 * 
	 * @param address
	 *            The address to set
	 */
	public void setAddress(String address) {
		Address = address;
	}

	/**
	 * Returns the microfilm.
	 * 
	 * @return String
	 */
	public String getMicrofilm() {
		return microfilm;
	}

	/**
	 * Sets the microfilm.
	 * 
	 * @param microfilm
	 *            The microfilm to set
	 */
	public void setMicrofilm(String microfilm) {
		this.microfilm = microfilm;
	}

}