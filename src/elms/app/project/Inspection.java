//Source file: C:\\datafile\\source\\elms\\app\\project\\Inspection.java

package elms.app.project;

import java.util.Calendar;

public class Inspection {
	private int inspectionId;
	private int activityId;
	private int inspectionItemId;
	private int inspectorId;
	private Calendar dateRequested;
	private String actionCode;
	private int routeSequence = 0;
	private Calendar faxSent;
	private String comments;
	private String createdBy;
	private Calendar created;
	private String updatedBy;
	private Calendar updated;

	/**
	 * 3CAA47EB02CF
	 */
	public Inspection() {

	}

	public Inspection(int aInspectionId, int aActivityId, int aInspectionItemId, Calendar aDateRequested, int aInspectorId, String aActionCode, String aComments) {
		this.inspectionId = aInspectionId;
		this.activityId = aActivityId;
		this.inspectionItemId = aInspectionItemId;
		this.dateRequested = aDateRequested;
		this.inspectorId = aInspectorId;
		this.actionCode = aActionCode;
		this.comments = aComments;
	}

	/**
	 * Access method for the inspectionId property.
	 * 
	 * @return the current value of the inspectionId property
	 */

	/**
	 * Gets the inspectionId
	 * 
	 * @return Returns a int
	 */
	public int getInspectionId() {
		return inspectionId;
	}

	/**
	 * Sets the inspectionId
	 * 
	 * @param inspectionId
	 *            The inspectionId to set
	 */
	public void setInspectionId(int inspectionId) {
		this.inspectionId = inspectionId;
	}

	/**
	 * Gets the activityId
	 * 
	 * @return Returns a int
	 */
	public int getActivityId() {
		return activityId;
	}

	/**
	 * Sets the activityId
	 * 
	 * @param activityId
	 *            The activityId to set
	 */
	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}

	/**
	 * Gets the inspectionItemId
	 * 
	 * @return Returns a int
	 */
	public int getInspectionItemId() {
		return inspectionItemId;
	}

	/**
	 * Sets the inspectionItemId
	 * 
	 * @param inspectionItemId
	 *            The inspectionItemId to set
	 */
	public void setInspectionItemId(int inspectionItemId) {
		this.inspectionItemId = inspectionItemId;
	}

	/**
	 * Gets the inspectorId
	 * 
	 * @return Returns a int
	 */
	public int getInspectorId() {
		return inspectorId;
	}

	/**
	 * Sets the inspectorId
	 * 
	 * @param inspectorId
	 *            The inspectorId to set
	 */
	public void setInspectorId(int inspectorId) {
		this.inspectorId = inspectorId;
	}

	/**
	 * Gets the dateRequested
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getDateRequested() {
		return dateRequested;
	}

	/**
	 * Sets the dateRequested
	 * 
	 * @param dateRequested
	 *            The dateRequested to set
	 */
	public void setDateRequested(Calendar dateRequested) {
		this.dateRequested = dateRequested;
	}

	/**
	 * Gets the actionCode
	 * 
	 * @return Returns a String
	 */
	public String getActionCode() {
		return actionCode;
	}

	/**
	 * Sets the actionCode
	 * 
	 * @param actionCode
	 *            The actionCode to set
	 */
	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}

	/**
	 * Gets the routeSequence
	 * 
	 * @return Returns a int
	 */
	public int getRouteSequence() {
		return routeSequence;
	}

	/**
	 * Sets the routeSequence
	 * 
	 * @param routeSequence
	 *            The routeSequence to set
	 */
	public void setRouteSequence(int routeSequence) {
		this.routeSequence = routeSequence;
	}

	/**
	 * Gets the faxSent
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getFaxSent() {
		return faxSent;
	}

	/**
	 * Sets the faxSent
	 * 
	 * @param faxSent
	 *            The faxSent to set
	 */
	public void setFaxSent(Calendar faxSent) {
		this.faxSent = faxSent;
	}

	/**
	 * Gets the comments
	 * 
	 * @return Returns a String
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * Sets the comments
	 * 
	 * @param comments
	 *            The comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * Gets the createdBy
	 * 
	 * @return Returns a String
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the createdBy
	 * 
	 * @param createdBy
	 *            The createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Gets the created
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getCreated() {
		return created;
	}

	/**
	 * Sets the created
	 * 
	 * @param created
	 *            The created to set
	 */
	public void setCreated(Calendar created) {
		this.created = created;
	}

	/**
	 * Gets the updatedBy
	 * 
	 * @return Returns a String
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Sets the updatedBy
	 * 
	 * @param updatedBy
	 *            The updatedBy to set
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Gets the updated
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getUpdated() {
		return updated;
	}

	/**
	 * Sets the updated
	 * 
	 * @param updated
	 *            The updated to set
	 */
	public void setUpdated(Calendar updated) {
		this.updated = updated;
	}

}
