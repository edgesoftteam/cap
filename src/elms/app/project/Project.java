package elms.app.project;

import java.util.ArrayList;
import java.util.List;

import elms.app.finance.ProjectFinance;
import elms.app.lso.Lso;
import elms.app.lso.Use;

public class Project extends Psa {

	protected Lso lso;
	protected int projectId;
	protected String projectType;
	protected ProjectDetail projectDetail;
	protected ProjectFinance projectFinance;
	protected List subProject;
	protected List processTeam;
	protected List people;
	protected Use use;

	/**
	 * @param lso
	 * @param projectDetail
	 * @param subProject
	 * @param projectFinance
	 * @param projectCondition
	 * @param projectHold
	 * @param projectAttachment
	 * @param processTeam
	 * @param people
	 * @param projectType
	 *            3CD03598034E
	 */
	public Project(int aProjectId, Lso aLso, ProjectDetail aProjectDetail, List aSubProject, ProjectFinance aProjectFinance, List aProjectHold, List aProjectCondition, List aProjectAttachment, List aProjectComment, List aProcessTeam, List aPeople, String aProjectType, Use use) {
		super(aProjectHold, aProjectCondition, aProjectAttachment, aProjectComment);
		this.projectId = aProjectId;
		this.lso = aLso;
		this.projectType = aProjectType;
		this.projectDetail = aProjectDetail;
		this.projectFinance = aProjectFinance;
		this.subProject = aSubProject;
		this.processTeam = aProcessTeam;
		this.people = aPeople;
		this.use = use;
	}

	/**
	 * 3CC7468601F5
	 */
	public Project() {
		lso = new Lso();
		projectDetail = new ProjectDetail();
		projectFinance = new ProjectFinance();
		subProject = new ArrayList();
		processTeam = new ArrayList();
		people = new ArrayList();
		use = new Use();
	}

	public Project(int aProjectId, ProjectDetail aProjectDetail) {
		super();
		this.projectId = aProjectId;
		this.projectDetail = aProjectDetail;
		this.projectFinance = new ProjectFinance();
		this.use = new Use();
		this.lso = new Lso();

	}

	public Project(int aProjectId) {
		this.projectId = aProjectId;
	}

	/**
	 * Gets the lso
	 * 
	 * @return Returns a Lso
	 */
	public Lso getLso() {
		return lso;
	}

	/**
	 * Sets the lso
	 * 
	 * @param lso
	 *            The lso to set
	 */
	public void setLso(Lso lso) {
		this.lso = lso;
	}

	/**
	 * Gets the projectType
	 * 
	 * @return Returns a String
	 */
	public String getProjectType() {
		return projectType;
	}

	/**
	 * Sets the projectType
	 * 
	 * @param projectType
	 *            The projectType to set
	 */
	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	/**
	 * Gets the projectDetail
	 * 
	 * @return Returns a ProjectDetail
	 */
	public ProjectDetail getProjectDetail() {
		return projectDetail;
	}

	/**
	 * Sets the projectDetail
	 * 
	 * @param projectDetail
	 *            The projectDetail to set
	 */
	public void setProjectDetail(ProjectDetail projectDetail) {
		this.projectDetail = projectDetail;
	}

	/**
	 * Gets the projectFinance
	 * 
	 * @return Returns a ProjectFinance
	 */
	public ProjectFinance getProjectFinance() {
		return projectFinance;
	}

	/**
	 * Sets the projectFinance
	 * 
	 * @param projectFinance
	 *            The projectFinance to set
	 */
	public void setProjectFinance(ProjectFinance projectFinance) {
		this.projectFinance = projectFinance;
	}

	/**
	 * Gets the subProject
	 * 
	 * @return Returns a List
	 */
	public List getSubProject() {
		return subProject;
	}

	/**
	 * Sets the subProject
	 * 
	 * @param subProject
	 *            The subProject to set
	 */
	public void setSubProject(List subProject) {
		this.subProject = subProject;
	}

	/**
	 * Gets the processTeam
	 * 
	 * @return Returns a List
	 */
	public List getProcessTeam() {
		return processTeam;
	}

	/**
	 * Sets the processTeam
	 * 
	 * @param processTeam
	 *            The processTeam to set
	 */
	public void setProcessTeam(List processTeam) {
		this.processTeam = processTeam;
	}

	/**
	 * Gets the people
	 * 
	 * @return Returns a List
	 */
	public List getPeople() {
		return people;
	}

	/**
	 * Sets the people
	 * 
	 * @param people
	 *            The people to set
	 */
	public void setPeople(List people) {
		this.people = people;
	}

	/**
	 * Gets the use
	 * 
	 * @return Returns a Use
	 */
	public Use getUse() {
		return use;
	}

	/**
	 * Sets the use
	 * 
	 * @param use
	 *            The use to set
	 */
	public void setUse(Use use) {
		this.use = use;
	}

	/**
	 * Gets the projectId
	 * 
	 * @return Returns a int
	 */
	public int getProjectId() {
		return projectId;
	}

	/**
	 * Sets the projectId
	 * 
	 * @param projectId
	 *            The projectId to set
	 */
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

}