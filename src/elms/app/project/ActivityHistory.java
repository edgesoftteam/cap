/*
 * Created on Apr 2, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package elms.app.project;



import org.apache.log4j.Logger;



/**
 * @author Sushitha 
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ActivityHistory {
	private String status; // Status Description
	private String changedDate;
    private String updatedBy;
    private String statusExpDate;
    private String permitExpDate;
    private String comments;
    private String update;  // To track whether this record has been updated
    private String deleteFlag; // If checked this record has to be deleted
    private String updateActivities;
    private String updateSubProject;
    private String added;
    private String activityStatus;
    private String actStatusId;
    private int userId;
    private String selectedUserId;
    private String updateFlag;
    
    
	/**
	 * @return Returns the updateFlag.
	 */
	public String getUpdateFlag() {
		return updateFlag;
	}
	/**
	 * @param updateFlag The updateFlag to set.
	 */
	public void setUpdateFlag(String updateFlag) {
		this.updateFlag = updateFlag;
	}
    static Logger logger = Logger.getLogger(ActivityHistory.class.getName());
    
	/**
	 * @return Returns the selectedUserId.
	 */
	public String getSelectedUserId() {
		return selectedUserId;
	}
	/**
	 * @param selectedUserId The selectedUserId to set.
	 */
	public void setSelectedUserId(String selectedUserId) {
		this.selectedUserId = selectedUserId;
	}
    public ActivityHistory() {
        
        this.update = "";
        this.deleteFlag = "";
        this.added = "";
        this.actStatusId= "0";
    }
    
	/**
	 * @return Returns the added.
	 */
	public String getAdded() {
		return added;
	}
	/**
	 * @param added The added to set.
	 */
	public void setAdded(String added) {
		if (added != null)
		this.added = added;
	}
   

	/**
	 * @return Returns the deleteFlag.
	 */
	public String getDeleteFlag() {
		return deleteFlag;
	}
	/**
	 * @param deleteFlag The deleteFlag to set.
	 */
	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}
	/**
	 * @return Returns the update.
	 */
	public String getUpdate() {
		return update;
	}
	/**
	 * @param update The update to set.
	 */
	public void setUpdate(String update) {
		this.update = update;
	}
	/**
	 * @return Returns the updateActivities.
	 */
	public String getUpdateActivities() {
		return updateActivities;
	}
	/**
	 * @param updateActivities The updateActivities to set.
	 */
	public void setUpdateActivities(String updateActivities) {
		this.updateActivities = updateActivities;
	}
	/**
	 * @return Returns the updateSubProject.
	 */
	public String getUpdateSubProject() {
		return updateSubProject;
	}
	/**
	 * @param updateSubProject The updateSubProject to set.
	 */
	public void setUpdateSubProject(String updateSubProject) {
		this.updateSubProject = updateSubProject;
	}
	
	/**
	 * @return Returns the comments.
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments The comments to set.
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	/**
	 * @return Returns the status.
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * @return Returns the changedDate.
	 */
	public String getChangedDate() {
		return changedDate;
	}
	/**
	 * @param changedDate The changedDate to set.
	 */
	public void setChangedDate(String changedDate) {
		this.changedDate = changedDate;
	}
	/**
	 * @return Returns the permitExpDate.
	 */
	public String getPermitExpDate() {
		return permitExpDate;
	}
	/**
	 * @param permitExpDate The permitExpDate to set.
	 */
	public void setPermitExpDate(String permitExpDate) {
		this.permitExpDate = permitExpDate;
	}
	/**
	 * @return Returns the statusExpDate.
	 */
	public String getStatusExpDate() {
		return statusExpDate;
	}
	/**
	 * @param statusExpDate The statusExpDate to set.
	 */
	public void setStatusExpDate(String statusExpDate) {
		logger.debug("date changed"+statusExpDate);
		this.statusExpDate = statusExpDate;
	}
	/**
	 * @return Returns the updatedBy.
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}
	/**
	 * @param updatedBy The updatedBy to set.
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	/**
	 * @return Returns the actStatusId.
	 */
	public String getActStatusId() {
		return actStatusId;
	}
	/**
	 * @param actStatusId The actStatusId to set.
	 */
	public void setActStatusId(String actStatusId) {
		this.actStatusId = actStatusId;
	}
	
	/**
	 * @return Returns the userId.
	 */
	public int getUserId() {
		return userId;
	}
	/**
	 * @param userId The userId to set.
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	
	
	/**
	 * @return Returns the activityStatus.
	 */
	public String getActivityStatus() {
		return activityStatus;
	}
	/**
	 * @param activityStatus The activityStatus to set.
	 */
	public void setActivityStatus(String activityStatus) {
		this.activityStatus = activityStatus;
	}
}
