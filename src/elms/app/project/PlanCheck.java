//Source file: C:\\datafile\\source\\elms\\app\\project\\PlanCheck.java

package elms.app.project;

import java.util.Calendar;
import java.util.Date;

import elms.security.User;

public class PlanCheck {
	private int planCheckId;
	private int activityId;
	private Calendar planCheckDate;
	private String date;
	private int statusCode;
	private String statusDesc;
	private int engineer;
	private String engineerName;
	private int categoryCode;
	private int titleCode;
	private String comments;
	private User createdBy;
	private Calendar created;
	private User updatedBy;
	private Calendar updated;

	/**
	 * 3CAA43E00263
	 */
	public PlanCheck(int aPlanCheckId, int aActivityId, Calendar aPlanCheckDate, int aStatusCode, int aCategoryCode, int aTitleCode, String aComments) {
		this.planCheckId = aPlanCheckId;
		this.activityId = aActivityId;
		this.planCheckDate = aPlanCheckDate;
		this.statusCode = aStatusCode;
		this.categoryCode = aCategoryCode;
		this.titleCode = aTitleCode;
		this.comments = aComments;
	}

	public PlanCheck() {
		this.planCheckId = 0;
	}

	/**
	 * Gets the pcId
	 * 
	 * @return Returns a int
	 */
	public int getPlanCheckId() {
		return planCheckId;
	}

	/**
	 * Sets the pcId
	 * 
	 * @param pcId
	 *            The pcId to set
	 */
	public void setPlanCheckId(int planCheckId) {
		this.planCheckId = planCheckId;
	}

	/**
	 * Gets the activityId
	 * 
	 * @return Returns a int
	 */
	public int getActivityId() {
		return activityId;
	}

	/**
	 * Sets the activityId
	 * 
	 * @param activityId
	 *            The activityId to set
	 */
	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}

	/**
	 * Gets the planCheckDate
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getPlanCheckDate() {
		return planCheckDate;
	}

	/**
	 * Sets the planCheckDate
	 * 
	 * @param planCheckDate
	 *            The planCheckDate to set
	 */
	public void setPlanCheckDate(Calendar planCheckDate) {
		this.planCheckDate = planCheckDate;
	}

	public void setPlanCheckDate(Date planCheckDate) {
		if (planCheckDate == null)
			this.planCheckDate = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(planCheckDate);
			this.planCheckDate = calendar;
		}
	}

	/**
	 * Gets the statusCode
	 * 
	 * @return Returns a int
	 */
	public int getStatusCode() {
		return statusCode;
	}

	/**
	 * Sets the statusCode
	 * 
	 * @param statusCode
	 *            The statusCode to set
	 */
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * Gets the categoryCode
	 * 
	 * @return Returns a int
	 */
	public int getCategoryCode() {
		return categoryCode;
	}

	/**
	 * Sets the categoryCode
	 * 
	 * @param categoryCode
	 *            The categoryCode to set
	 */
	public void setCategoryCode(int categoryCode) {
		this.categoryCode = categoryCode;
	}

	/**
	 * Gets the titleCode
	 * 
	 * @return Returns a int
	 */
	public int getTitleCode() {
		return titleCode;
	}

	/**
	 * Sets the titleCode
	 * 
	 * @param titleCode
	 *            The titleCode to set
	 */
	public void setTitleCode(int titleCode) {
		this.titleCode = titleCode;
	}

	/**
	 * Gets the comments
	 * 
	 * @return Returns a String
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * Sets the comments
	 * 
	 * @param comments
	 *            The comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * Gets the createdBy
	 * 
	 * @return Returns a User
	 */
	public User getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the createdBy
	 * 
	 * @param createdBy
	 *            The createdBy to set
	 */
	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Gets the created
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getCreated() {
		return created;
	}

	/**
	 * Sets the created
	 * 
	 * @param created
	 *            The created to set
	 */
	public void setCreated(Calendar created) {
		this.created = created;
	}

	public void setCreated(Date created) {
		if (created == null)
			this.created = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(created);
			this.created = calendar;
		}
	}

	/**
	 * Gets the updatedBy
	 * 
	 * @return Returns a User
	 */
	public User getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Sets the updatedBy
	 * 
	 * @param updatedBy
	 *            The updatedBy to set
	 */
	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Gets the updated
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getUpdated() {
		return updated;
	}

	/**
	 * Sets the updated
	 * 
	 * @param updated
	 *            The updated to set
	 */
	public void setUpdated(Calendar updated) {
		this.updated = updated;
	}

	public void setUpdated(Date updated) {
		if (updated == null)
			this.updated = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(updated);
			this.updated = calendar;
		}
	}

	/**
	 * Gets the engineer
	 * 
	 * @return Returns a int
	 */
	public int getEngineer() {
		return engineer;
	}

	/**
	 * Sets the engineer
	 * 
	 * @param engineer
	 *            The engineer to set
	 */
	public void setEngineer(int engineer) {
		this.engineer = engineer;
	}

	/**
	 * Gets the statusDesc
	 * 
	 * @return Returns a String
	 */
	public String getStatusDesc() {
		return statusDesc;
	}

	/**
	 * Sets the statusDesc
	 * 
	 * @param statusDesc
	 *            The statusDesc to set
	 */
	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	/**
	 * Gets the engineerName
	 * 
	 * @return Returns a String
	 */
	public String getEngineerName() {
		return engineerName;
	}

	/**
	 * Sets the engineerName
	 * 
	 * @param engineerName
	 *            The engineerName to set
	 */
	public void setEngineerName(String engineerName) {
		this.engineerName = engineerName;
	}

	/**
	 * Gets the date
	 * 
	 * @return Returns a String
	 */
	public String getDate() {
		return date;
	}

	/**
	 * Sets the date
	 * 
	 * @param date
	 *            The date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

}
