package elms.app.finance;

public class FeeLookup {
	/**
	 * Constructor for FeeLookup
	 */

	protected int feeId;
	protected int feeLookup;

	public FeeLookup() {
	}

	/**
	 * Gets the feeId
	 * 
	 * @return Returns a int
	 */
	public int getFeeId() {
		return feeId;
	}

	/**
	 * Sets the feeId
	 * 
	 * @param feeId
	 *            The feeId to set
	 */
	public void setFeeId(int feeId) {
		this.feeId = feeId;
	}

	/**
	 * Gets the feeLookup
	 * 
	 * @return Returns a int
	 */
	public int getFeeLookup() {
		return feeLookup;
	}

	/**
	 * Sets the feeLookup
	 * 
	 * @param feeLookup
	 *            The feeLookup to set
	 */
	public void setFeeLookup(int feeLookup) {
		this.feeLookup = feeLookup;
	}

}
