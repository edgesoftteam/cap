//Source file: C:\\datafile\\source\\elms\\app\\finance\\ActivityFee.java
package elms.app.finance;

import java.math.BigDecimal;

import elms.util.StringUtils;

public class ActivityFee {
	protected char type = 'P';
	protected int activityId;
	protected int feeId = 0;;
	protected int peopleId = 0;
	protected double feeUnits = 0;
	protected int feeValuation = 0;
	protected double feeAmount;
	protected int feeAccount = 0;;
	protected double feePaid;
	protected double adjustmentAmount;
	protected double creditAmount;
	protected double bouncedAmount;
	protected String comments = "";
	protected int subtotalLevel = 0;
	protected BigDecimal feeFactor;
	protected BigDecimal factor;
	protected int feeFlagFour = 0;
	protected int feeInit = 0;
	protected int tax = 0;
	protected double permitLicenseFeeTotal = 0;// added for penalty

	/**
	 * @return Returns the permitLicenseFeeTotal.
	 */
	public double getPermitLicenseFeeTotal() {
		return permitLicenseFeeTotal;
	}

	/**
	 * @param permitLicenseFeeTotal
	 *            The permitLicenseFeeTotal to set.
	 */
	public void setPermitLicenseFeeTotal(double permitLicenseFeeTotal) {
		this.permitLicenseFeeTotal = permitLicenseFeeTotal;
	}

	protected double[] subTotals = {};

	/**
	 * @roseuid 3CA8B2DC0159
	 */
	public ActivityFee() {

	}

	/**
	 * @roseuid 3CA8B2DC0159
	 */
	public ActivityFee(int aActivityId, int aFeeId, int aFeeUnits, double aFeeAmount, double aFeePaid) {
		this.activityId = aActivityId;
		this.feeId = aFeeId;
		this.feeUnits = aFeeUnits;
		this.feeAmount = aFeeAmount;
		this.feePaid = aFeePaid;
	}

	public ActivityFee(String aActivityId, String aFeeId, String aFeeUnits, String aFeeAmount) {
		this.activityId = StringUtils.s2i(aActivityId);
		this.feeId = StringUtils.s2i(aFeeId);
		this.feeUnits = StringUtils.s2d(aFeeUnits);
		this.feeAmount = (double) StringUtils.s2d(aFeeAmount);
	}

	/**
	 * Access method for the activityId property.
	 * 
	 * @return the current value of the activityId property
	 */
	public int getActivityId() {
		return activityId;
	}

	/**
	 * Sets the value of the activityId property.
	 * 
	 * @param aActivityId
	 *            the new value of the activityId property
	 */
	public void setActivityId(int aActivityId) {
		activityId = aActivityId;
	}

	/**
	 * Access method for the feeId property.
	 * 
	 * @return the current value of the feeId property
	 */
	public int getFeeId() {
		return feeId;
	}

	/**
	 * Sets the value of the feeId property.
	 * 
	 * @param aFeeId
	 *            the new value of the feeId property
	 */
	public void setFeeId(int aFeeId) {
		feeId = aFeeId;
	}

	/**
	 * Access method for the feeUnits property.
	 * 
	 * @return the current value of the feeUnits property
	 */
	public double getFeeUnits() {
		return feeUnits;
	}

	/**
	 * Sets the value of the feeUnits property.
	 * 
	 * @param aFeeUnits
	 *            the new value of the feeUnits property
	 */
	public void setFeeUnits(double aFeeUnits) {
		feeUnits = aFeeUnits;
	}

	public String toString() {
		return "ActivityFee : " + getActivityId() + " : " + getFeeId() + ":" + getFeeUnits();
	}

	/**
	 * Gets the peopleId
	 * 
	 * @return Returns a int
	 */
	public int getPeopleId() {
		return peopleId;
	}

	/**
	 * Sets the peopleId
	 * 
	 * @param peopleId
	 *            The peopleId to set
	 */
	public void setPeopleId(int peopleId) {
		this.peopleId = peopleId;
	}

	/**
	 * Gets the feeValuation
	 * 
	 * @return Returns a int
	 */
	public int getFeeValuation() {
		return feeValuation;
	}

	/**
	 * Sets the feeValuation
	 * 
	 * @param feeValuation
	 *            The feeValuation to set
	 */
	public void setFeeValuation(int feeValuation) {
		this.feeValuation = feeValuation;
	}

	/**
	 * Gets the comments
	 * 
	 * @return Returns a String
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * Sets the comments
	 * 
	 * @param comments
	 *            The comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * Gets the type
	 * 
	 * @return Returns a char
	 */
	public char getType() {
		return type;
	}

	/**
	 * Sets the type
	 * 
	 * @param type
	 *            The type to set
	 */
	public void setType(char type) {
		this.type = type;
	}

	/**
	 * Returns the subtotalLevel.
	 * 
	 * @return int
	 */
	public int getSubtotalLevel() {
		return subtotalLevel;
	}

	/**
	 * Sets the subtotalLevel.
	 * 
	 * @param subtotalLevel
	 *            The subtotalLevel to set
	 */
	public void setSubtotalLevel(int subtotalLevel) {
		this.subtotalLevel = subtotalLevel;
	}

	/**
	 * Returns the feeInit.
	 * 
	 * @return int
	 */
	public int getFeeInit() {
		return feeInit;
	}

	/**
	 * Sets the feeInit.
	 * 
	 * @param feeInit
	 *            The feeInit to set
	 */
	public void setFeeInit(int feeInit) {
		this.feeInit = feeInit;
	}

	/**
	 * Returns the factor.
	 * 
	 * @return BigDecimal
	 */
	public BigDecimal getFactor() {
		return factor;
	}

	/**
	 * Returns the feeFactor.
	 * 
	 * @return BigDecimal
	 */
	public BigDecimal getFeeFactor() {
		return feeFactor;
	}

	/**
	 * Sets the factor.
	 * 
	 * @param factor
	 *            The factor to set
	 */
	public void setFactor(BigDecimal factor) {
		this.factor = factor;
	}

	/**
	 * Sets the feeFactor.
	 * 
	 * @param feeFactor
	 *            The feeFactor to set
	 */
	public void setFeeFactor(BigDecimal feeFactor) {
		this.feeFactor = feeFactor;
	}

	/**
	 * Gets the feeFlagFour
	 * 
	 * @return Returns a int
	 */
	public int getFeeFlagFour() {
		return feeFlagFour;
	}

	/**
	 * Sets the feeFlagFour
	 * 
	 * @param feeFlagFour
	 *            The feeFlagFour to set
	 */
	public void setFeeFlagFour(int feeFlagFour) {
		this.feeFlagFour = feeFlagFour;
	}

	/**
	 * Gets the feeAmount
	 * 
	 * @return Returns a double
	 */
	public double getFeeAmount() {
		return feeAmount;
	}

	/**
	 * Sets the feeAmount
	 * 
	 * @param feeAmount
	 *            The feeAmount to set
	 */
	public void setFeeAmount(double feeAmount) {
		this.feeAmount = feeAmount;
	}

	/**
	 * Gets the feePaid
	 * 
	 * @return Returns a double
	 */
	public double getFeePaid() {
		return feePaid;
	}

	/**
	 * Sets the feePaid
	 * 
	 * @param feePaid
	 *            The feePaid to set
	 */
	public void setFeePaid(double feePaid) {
		this.feePaid = feePaid;
	}

	/**
	 * Gets the adjustmentAmount
	 * 
	 * @return Returns a double
	 */
	public double getAdjustmentAmount() {
		return adjustmentAmount;
	}

	/**
	 * Sets the adjustmentAmount
	 * 
	 * @param adjustmentAmount
	 *            The adjustmentAmount to set
	 */
	public void setAdjustmentAmount(double adjustmentAmount) {
		this.adjustmentAmount = adjustmentAmount;
	}

	/**
	 * Gets the creditAmount
	 * 
	 * @return Returns a double
	 */
	public double getCreditAmount() {
		return creditAmount;
	}

	/**
	 * Sets the creditAmount
	 * 
	 * @param creditAmount
	 *            The creditAmount to set
	 */
	public void setCreditAmount(double creditAmount) {
		this.creditAmount = creditAmount;
	}

	/**
	 * Gets the subTotals
	 * 
	 * @return Returns a double[]
	 */
	public double[] getSubTotals() {
		return subTotals;
	}

	/**
	 * Sets the subTotals
	 * 
	 * @param subTotals
	 *            The subTotals to set
	 */
	public void setSubTotals(double[] subTotals) {
		this.subTotals = subTotals;
	}

	/**
	 * Returns the tax.
	 * 
	 * @return int
	 */
	public int getTax() {
		return tax;
	}

	/**
	 * Sets the tax.
	 * 
	 * @param tax
	 *            The tax to set
	 */
	public void setTax(int tax) {
		this.tax = tax;
	}

	/**
	 * Returns the bouncedAmount.
	 * 
	 * @return double
	 */
	public double getBouncedAmount() {
		return bouncedAmount;
	}

	/**
	 * Sets the bouncedAmount.
	 * 
	 * @param bouncedAmount
	 *            The bouncedAmount to set
	 */
	public void setBouncedAmount(double bouncedAmount) {
		this.bouncedAmount = bouncedAmount;
	}

	/**
	 * @return
	 */
	public int getFeeAccount() {
		return feeAccount;
	}

	/**
	 * @param i
	 */
	public void setFeeAccount(int i) {
		feeAccount = i;
	}

}
