package elms.app.finance;

import java.io.Serializable;

public class ActivityFeeEdit implements Serializable {
	protected String type;
	protected String activityId;
	protected String feeId;
	protected String feeDescription;
	protected String peopleId;
	protected String feeUnits;
	protected String feeValuation;
	protected String feeAmount;
	protected String feeAmountValue;
	protected String feePaid;
	protected String adjustmentAmount;
	protected String creditAmount;
	protected String bouncedAmount;
	protected String comments;
	protected String feePc;
	protected String planCheckFees;
	protected String developmentFees;
	protected String lsoType;
	protected String tax;
	protected String feeAccount;

	protected String name;
	protected String licenseNbr;

	protected String checked;
	protected String required;
	protected String unitPrice;
	protected String factor;
	protected String feeFactor;
	protected String input;
	protected String subtotalLevel;
	protected String feeFlagFour;
	protected String feeCalcOne;
	protected String feeCalcTwo;
	protected String feeInit;
	protected String paymentAmount;

	/**
	 * @roseuid 3CA8B2DC0159
	 */
	public ActivityFeeEdit() {

	}

	public String getFeeAmountValue() {
		return feeAmountValue;
	}

	public void setFeeAmountValue(String feeAmountValue) {
		this.feeAmountValue = feeAmountValue;
	}

	/**
	 * Gets the type
	 * 
	 * @return Returns a String
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type
	 * 
	 * @param type
	 *            The type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the activityId
	 * 
	 * @return Returns a String
	 */
	public String getActivityId() {
		return activityId;
	}

	/**
	 * Sets the activityId
	 * 
	 * @param activityId
	 *            The activityId to set
	 */
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	/**
	 * Gets the feeId
	 * 
	 * @return Returns a String
	 */
	public String getFeeId() {
		return feeId;
	}

	/**
	 * Sets the feeId
	 * 
	 * @param feeId
	 *            The feeId to set
	 */
	public void setFeeId(String feeId) {
		this.feeId = feeId;
	}

	/**
	 * Gets the feeDescription
	 * 
	 * @return Returns a String
	 */
	public String getFeeDescription() {
		return feeDescription;
	}

	/**
	 * Sets the feeDescription
	 * 
	 * @param feeDescription
	 *            The feeDescription to set
	 */
	public void setFeeDescription(String feeDescription) {
		this.feeDescription = feeDescription;
	}

	/**
	 * Gets the peopleId
	 * 
	 * @return Returns a String
	 */
	public String getPeopleId() {
		return peopleId;
	}

	/**
	 * Sets the peopleId
	 * 
	 * @param peopleId
	 *            The peopleId to set
	 */
	public void setPeopleId(String peopleId) {
		this.peopleId = peopleId;
	}

	/**
	 * Gets the feeUnits
	 * 
	 * @return Returns a String
	 */
	public String getFeeUnits() {
		return feeUnits;
	}

	/**
	 * Sets the feeUnits
	 * 
	 * @param feeUnits
	 *            The feeUnits to set
	 */
	public void setFeeUnits(String feeUnits) {
		this.feeUnits = feeUnits;
	}

	/**
	 * Gets the feeValuation
	 * 
	 * @return Returns a String
	 */
	public String getFeeValuation() {
		return feeValuation;
	}

	/**
	 * Sets the feeValuation
	 * 
	 * @param feeValuation
	 *            The feeValuation to set
	 */
	public void setFeeValuation(String feeValuation) {
		this.feeValuation = feeValuation;
	}

	/**
	 * Gets the feeAmount
	 * 
	 * @return Returns a String
	 */
	public String getFeeAmount() {
		return feeAmount;
	}

	/**
	 * Sets the feeAmount
	 * 
	 * @param feeAmount
	 *            The feeAmount to set
	 */
	public void setFeeAmount(String feeAmount) {
		this.feeAmount = feeAmount;
	}

	/**
	 * Gets the feePaid
	 * 
	 * @return Returns a String
	 */
	public String getFeePaid() {
		return feePaid;
	}

	/**
	 * Sets the feePaid
	 * 
	 * @param feePaid
	 *            The feePaid to set
	 */
	public void setFeePaid(String feePaid) {
		this.feePaid = feePaid;
	}

	/**
	 * Gets the adjustmentAmount
	 * 
	 * @return Returns a String
	 */
	public String getAdjustmentAmount() {
		return adjustmentAmount;
	}

	/**
	 * Sets the adjustmentAmount
	 * 
	 * @param adjustmentAmount
	 *            The adjustmentAmount to set
	 */
	public void setAdjustmentAmount(String adjustmentAmount) {
		this.adjustmentAmount = adjustmentAmount;
	}

	/**
	 * Gets the creditAmount
	 * 
	 * @return Returns a String
	 */
	public String getCreditAmount() {
		return creditAmount;
	}

	/**
	 * Sets the creditAmount
	 * 
	 * @param creditAmount
	 *            The creditAmount to set
	 */
	public void setCreditAmount(String creditAmount) {
		this.creditAmount = creditAmount;
	}

	/**
	 * Gets the comments
	 * 
	 * @return Returns a String
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * Sets the comments
	 * 
	 * @param comments
	 *            The comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * Gets the checked
	 * 
	 * @return Returns a String
	 */
	public String getChecked() {
		return checked;
	}

	/**
	 * Sets the checked
	 * 
	 * @param checked
	 *            The checked to set
	 */
	public void setChecked(String checked) {
		this.checked = checked;
	}

	/**
	 * Gets the name
	 * 
	 * @return Returns a String
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name
	 * 
	 * @param name
	 *            The name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the licenseNbr
	 * 
	 * @return Returns a String
	 */
	public String getLicenseNbr() {
		return licenseNbr;
	}

	/**
	 * Sets the licenseNbr
	 * 
	 * @param licenseNbr
	 *            The licenseNbr to set
	 */
	public void setLicenseNbr(String licenseNbr) {
		this.licenseNbr = licenseNbr;
	}

	/**
	 * Gets the required
	 * 
	 * @return Returns a String
	 */
	public String getRequired() {
		return required;
	}

	/**
	 * Sets the required
	 * 
	 * @param required
	 *            The required to set
	 */
	public void setRequired(String required) {
		this.required = required;
	}

	/**
	 * Gets the input
	 * 
	 * @return Returns a String
	 */
	public String getInput() {
		return input;
	}

	/**
	 * Sets the input
	 * 
	 * @param input
	 *            The input to set
	 */
	public void setInput(String input) {
		this.input = input;
	}

	/**
	 * Returns the unitPrice.
	 * 
	 * @return String
	 */
	public String getUnitPrice() {
		return unitPrice;
	}

	/**
	 * Sets the unitPrice.
	 * 
	 * @param unitPrice
	 *            The unitPrice to set
	 */
	public void setUnitPrice(String unitPrice) {
		this.unitPrice = unitPrice;
	}

	/**
	 * Gets the feePc
	 * 
	 * @return Returns a String
	 */
	public String getFeePc() {
		return feePc;
	}

	/**
	 * Sets the feePc
	 * 
	 * @param feePc
	 *            The feePc to set
	 */
	public void setFeePc(String feePc) {
		this.feePc = feePc;
	}

	/**
	 * Returns the subtotalLevel.
	 * 
	 * @return String
	 */
	public String getSubtotalLevel() {
		return subtotalLevel;
	}

	/**
	 * Sets the subtotalLevel.
	 * 
	 * @param subtotalLevel
	 *            The subtotalLevel to set
	 */
	public void setSubtotalLevel(String subtotalLevel) {
		this.subtotalLevel = subtotalLevel;
	}

	/**
	 * Returns the feeInit.
	 * 
	 * @return String
	 */
	public String getFeeInit() {
		return feeInit;
	}

	/**
	 * Sets the feeInit.
	 * 
	 * @param feeInit
	 *            The feeInit to set
	 */
	public void setFeeInit(String feeInit) {
		this.feeInit = feeInit;
	}

	/**
	 * Returns the factor.
	 * 
	 * @return String
	 */
	public String getFactor() {
		return factor;
	}

	/**
	 * Sets the factor.
	 * 
	 * @param factor
	 *            The factor to set
	 */
	public void setFactor(String factor) {
		this.factor = factor;
	}

	/**
	 * Returns the paymentAmount.
	 * 
	 * @return String
	 */
	public String getPaymentAmount() {
		return paymentAmount;
	}

	/**
	 * Sets the paymentAmount.
	 * 
	 * @param paymentAmount
	 *            The paymentAmount to set
	 */
	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	/**
	 * Gets the feeFactor
	 * 
	 * @return Returns a String
	 */
	public String getFeeFactor() {
		return feeFactor;
	}

	/**
	 * Sets the feeFactor
	 * 
	 * @param feeFactor
	 *            The feeFactor to set
	 */
	public void setFeeFactor(String feeFactor) {
		this.feeFactor = feeFactor;
	}

	/**
	 * Gets the feeFlagFour
	 * 
	 * @return Returns a String
	 */
	public String getFeeFlagFour() {
		return feeFlagFour;
	}

	/**
	 * Sets the feeFlagFour
	 * 
	 * @param feeFlagFour
	 *            The feeFlagFour to set
	 */
	public void setFeeFlagFour(String feeFlagFour) {
		this.feeFlagFour = feeFlagFour;
	}

	/**
	 * Returns the planCheckFees.
	 * 
	 * @return String
	 */
	public String getPlanCheckFees() {
		return planCheckFees;
	}

	/**
	 * Sets the planCheckFees.
	 * 
	 * @param planCheckFees
	 *            The planCheckFees to set
	 */
	public void setPlanCheckFees(String planCheckFees) {
		this.planCheckFees = planCheckFees;
	}

	/**
	 * @return Returns the developmentFees.
	 */
	public String getDevelopmentFees() {
		return developmentFees;
	}

	/**
	 * @param developmentFees
	 *            The developmentFees to set.
	 */
	public void setDevelopmentFees(String developmentFees) {
		this.developmentFees = developmentFees;
	}

	/**
	 * Returns the tax.
	 * 
	 * @return String
	 */
	public String getTax() {
		return tax;
	}

	/**
	 * Sets the tax.
	 * 
	 * @param tax
	 *            The tax to set
	 */
	public void setTax(String tax) {
		this.tax = tax;
	}

	/**
	 * Returns the bouncedAmount.
	 * 
	 * @return String
	 */
	public String getBouncedAmount() {
		return bouncedAmount;
	}

	/**
	 * Sets the bouncedAmount.
	 * 
	 * @param bouncedAmount
	 *            The bouncedAmount to set
	 */
	public void setBouncedAmount(String bouncedAmount) {
		this.bouncedAmount = bouncedAmount;
	}

	/**
	 * @return
	 */
	public String getLsoType() {
		return lsoType;
	}

	/**
	 * @param string
	 */
	public void setLsoType(String string) {
		lsoType = string;
	}

	/**
	 * @return
	 */
	public String getFeeAccount() {
		return feeAccount;
	}

	/**
	 * @param string
	 */
	public void setFeeAccount(String string) {
		feeAccount = string;
	}

	/**
	 * @return
	 */
	public String getFeeCalcTwo() {
		return feeCalcTwo;
	}

	/**
	 * @param string
	 */
	public void setFeeCalcTwo(String string) {
		feeCalcTwo = string;
	}

	/**
	 * @return
	 */
	public String getFeeCalcOne() {
		return feeCalcOne;
	}

	/**
	 * @param string
	 */
	public void setFeeCalcOne(String string) {
		feeCalcOne = string;
	}

}
