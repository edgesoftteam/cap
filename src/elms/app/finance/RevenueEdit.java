package elms.app.finance;

import java.io.Serializable;

import elms.util.StringUtils;

public class RevenueEdit implements Serializable {

	private String accountNbr;
	private String revenueAmt;
	private String postedAmt;
	private String comments;

	public RevenueEdit(String aAccountNbr, double aRevenueAmt, double aPostedAmt, String aComments) {
		this.accountNbr = aAccountNbr;
		this.revenueAmt = StringUtils.dbl2$(aRevenueAmt);
		this.postedAmt = StringUtils.dbl2$(aPostedAmt);
		this.comments = aComments;
	}

	public RevenueEdit(String aAccountNbr, double aRevenueAmt) {
		this(aAccountNbr, aRevenueAmt, aRevenueAmt, "");
	}

	/**
	 * Returns the accountNbr.
	 * 
	 * @return String
	 */
	public String getAccountNbr() {
		return accountNbr;
	}

	/**
	 * Returns the comments.
	 * 
	 * @return String
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * Returns the postedAmt.
	 * 
	 * @return String
	 */
	public String getPostedAmt() {
		return postedAmt;
	}

	/**
	 * Returns the revenueAmt.
	 * 
	 * @return String
	 */
	public String getRevenueAmt() {
		return revenueAmt;
	}

	/**
	 * Sets the accountNbr.
	 * 
	 * @param accountNbr
	 *            The accountNbr to set
	 */
	public void setAccountNbr(String accountNbr) {
		this.accountNbr = accountNbr;
	}

	/**
	 * Sets the comments.
	 * 
	 * @param comments
	 *            The comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * Sets the postedAmt.
	 * 
	 * @param postedAmt
	 *            The postedAmt to set
	 */
	public void setPostedAmt(String postedAmt) {
		this.postedAmt = postedAmt;
	}

	/**
	 * Sets the revenueAmt.
	 * 
	 * @param revenueAmt
	 *            The revenueAmt to set
	 */
	public void setRevenueAmt(String revenueAmt) {
		this.revenueAmt = revenueAmt;
	}

}