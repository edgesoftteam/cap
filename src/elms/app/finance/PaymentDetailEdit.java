package elms.app.finance;

/**
 * @author Shekhar
 * 
 *         To change this generated comment edit the template variable "typecomment": Window>Preferences>Java>Templates. To enable and disable the creation of type comments go to Window>Preferences>Java>Code Generation.
 */
public class PaymentDetailEdit {
	private String transactionId;
	private String accountNbr;
	private String description;
	private String amount;
	private String bouncedAmt;
	private String comment;
	private String feeType;

	/**
	 * Constructor for PaymentDetailEdit.
	 */
	public PaymentDetailEdit() {
		super();
	}

	/**
	 * Returns the accountNbr.
	 * 
	 * @return String
	 */
	public String getAccountNbr() {
		return accountNbr;
	}

	/**
	 * Returns the amount.
	 * 
	 * @return String
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * Returns the comment.
	 * 
	 * @return String
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * Returns the description.
	 * 
	 * @return String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Returns the transactionId.
	 * 
	 * @return String
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * Sets the accountNbr.
	 * 
	 * @param accountNbr
	 *            The accountNbr to set
	 */
	public void setAccountNbr(String accountNbr) {
		this.accountNbr = accountNbr;
	}

	/**
	 * Sets the amount.
	 * 
	 * @param amount
	 *            The amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * Sets the comment.
	 * 
	 * @param comment
	 *            The comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Sets the transactionId.
	 * 
	 * @param transactionId
	 *            The transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * Returns the bouncedAmt.
	 * 
	 * @return String
	 */
	public String getBouncedAmt() {
		return bouncedAmt;
	}

	/**
	 * Sets the bouncedAmt.
	 * 
	 * @param bouncedAmt
	 *            The bouncedAmt to set
	 */
	public void setBouncedAmt(String bouncedAmt) {
		this.bouncedAmt = bouncedAmt;
	}

	/**
	 * Returns the feeType.
	 * 
	 * @return String
	 */
	public String getFeeType() {
		return feeType;
	}

	/**
	 * Sets the feeType.
	 * 
	 * @param feeType
	 *            The feeType to set
	 */
	public void setFeeType(String feeType) {
		this.feeType = feeType;
	}

}
