//Source file: C:\\datafile\\source\\elms\\app\\finance\\PaymentDetail.java
package elms.app.finance;

public class PaymentDetail extends Payment {
	protected int transactionId;
	protected int paymentId;
	protected int activityId;
	protected int feeId;
	protected double amount;
	protected double bouncedAmt;
	protected int peopleId;
	protected String feeAccount;

	/**
	 * @roseuid 3CAA40DB0059
	 */
	public PaymentDetail(int aTransactionId, int aPaymentId, int aActivityId, int aFeeId, double aAmount, double aBouncedAmount, String aFeeAccount) {
		this.transactionId = aTransactionId;
		this.paymentId = aPaymentId;
		this.activityId = aActivityId;
		this.feeId = aFeeId;
		this.amount = aAmount;
		this.bouncedAmt = aBouncedAmount;
		this.feeAccount = aFeeAccount;
	}

	public PaymentDetail(int aTransactionId, int aPaymentId, int aActivityId, int aFeeId, int aPeopleId, double aAmount, double aBouncedAmount, String aFeeAccount) {
		this.transactionId = aTransactionId;
		this.paymentId = aPaymentId;
		this.activityId = aActivityId;
		this.feeId = aFeeId;
		this.amount = aAmount;
		this.peopleId = aPeopleId;
		this.bouncedAmt = aBouncedAmount;
		this.feeAccount = aFeeAccount;
	}

	public PaymentDetail() {
		this.bouncedAmt = 0.0;
	}

	/**
	 * Access method for the transactionId property.
	 * 
	 * @return the current value of the transactionId property
	 */
	public int getTransactionId() {
		return transactionId;
	}

	/**
	 * Sets the value of the transactionId property.
	 * 
	 * @param aTransactionId
	 *            the new value of the transactionId property
	 */
	public void setTransactionId(int aTransactionId) {
		transactionId = aTransactionId;
	}

	/**
	 * Access method for the paymentId property.
	 * 
	 * @return the current value of the paymentId property
	 */
	public int getPaymentId() {
		return paymentId;
	}

	/**
	 * Sets the value of the paymentId property.
	 * 
	 * @param aPaymentId
	 *            the new value of the paymentId property
	 */
	public void setPaymentId(int aPaymentId) {
		paymentId = aPaymentId;
	}

	/**
	 * Access method for the activityId property.
	 * 
	 * @return the current value of the activityId property
	 */
	public int getActivityId() {
		return activityId;
	}

	/**
	 * Sets the value of the activityId property.
	 * 
	 * @param aActivityId
	 *            the new value of the activityId property
	 */
	public void setActivityId(int aActivityId) {
		activityId = aActivityId;
	}

	/**
	 * Access method for the feeId property.
	 * 
	 * @return the current value of the feeId property
	 */
	public int getFeeId() {
		return feeId;
	}

	/**
	 * Sets the value of the feeId property.
	 * 
	 * @param aFeeId
	 *            the new value of the feeId property
	 */
	public void setFeeId(int aFeeId) {
		feeId = aFeeId;
	}

	public String toString() {
		return ("PaymentDetail[transactionId=" + this.getTransactionId() + ":paymentID=" + this.getPaymentId() + ":activityId=" + this.getActivityId() + ":FeeId=" + this.getFeeId() + ":Amount=" + this.getAmount() + "]");
	}

	/**
	 * Gets the peopleId
	 * 
	 * @return Returns a int
	 */
	public int getPeopleId() {
		return peopleId;
	}

	/**
	 * Sets the peopleId
	 * 
	 * @param peopleId
	 *            The peopleId to set
	 */
	public void setPeopleId(int peopleId) {
		this.peopleId = peopleId;
	}

	/**
	 * Gets the amount
	 * 
	 * @return Returns a double
	 */
	public double getAmount() {
		return amount;
	}

	/**
	 * Sets the amount
	 * 
	 * @param amount
	 *            The amount to set
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}

	/**
	 * Returns the bouncedAmt.
	 * 
	 * @return double
	 */
	public double getBouncedAmt() {
		return bouncedAmt;
	}

	/**
	 * Sets the bouncedAmt.
	 * 
	 * @param bouncedAmt
	 *            The bouncedAmt to set
	 */
	public void setBouncedAmt(double bouncedAmt) {
		this.bouncedAmt = bouncedAmt;
	}

	/**
	 * @return
	 */
	public String getFeeAccount() {
		return feeAccount;
	}

	/**
	 * @param string
	 */
	public void setFeeAccount(String string) {
		feeAccount = string;
	}

}
