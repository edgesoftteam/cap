/*
 * Created on Nov 15, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package elms.app.enforcement;

/**
 * @author Shekhar
 * 
 *         To change the template for this generated type comment go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CodeEnforcement {
	protected String deleted;
	protected String changed;

	protected int id;
	protected int ceTypeId;
	private String ceType;
	protected String officerId;
	protected String name;
	protected String phone;
	protected String ceDate;
	protected String feeAmount;
	protected String subType;
	protected String category;

	public CodeEnforcement() {
		this.deleted = "";
		this.changed = "";
		this.id = 0;
		this.ceTypeId = 0;
	}

	/**
	 * @return
	 */
	public String getCeDate() {
		return ceDate;
	}

	/**
	 * @return
	 */
	public int getCeTypeId() {
		return ceTypeId;
	}

	/**
	 * @return
	 */
	public String getSubType() {
		return subType;
	}

	/**
	 * @return
	 */
	public String getFeeAmount() {
		return feeAmount;
	}

	/**
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return
	 */
	public String getOfficerId() {
		return officerId;
	}

	/**
	 * @return
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param string
	 */
	public void setCeDate(String string) {
		ceDate = string;
	}

	/**
	 * @param i
	 */
	public void setCeTypeId(int i) {
		ceTypeId = i;
	}

	/**
	 * @param string
	 */
	public void setSubType(String string) {
		subType = string;
	}

	/**
	 * @param string
	 */
	public void setFeeAmount(String string) {
		feeAmount = string;
	}

	/**
	 * @param i
	 */
	public void setId(int i) {
		id = i;
	}

	/**
	 * @param string
	 */
	public void setName(String string) {
		name = string;
	}

	/**
	 * @param string
	 */
	public void setOfficerId(String string) {
		officerId = string;
	}

	/**
	 * @param string
	 */
	public void setPhone(String string) {
		phone = string;
	}

	/**
	 * @return
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param string
	 */
	public void setCategory(String string) {
		category = string;
	}

	/**
	 * @return
	 */
	public String getChanged() {
		return changed;
	}

	/**
	 * @return
	 */
	public String getDeleted() {
		return deleted;
	}

	/**
	 * @param string
	 */
	public void setChanged(String string) {
		changed = string;
	}

	/**
	 * @param string
	 */
	public void setDeleted(String string) {
		deleted = string;
	}

	public String toString() {
		return "CodeEnforcement (" + this.ceTypeId + "," + this.category + "," + this.name + "," + this.officerId + ")";
	}

	public String getCeType() {
		return ceType;
	}

	public void setCeType(String ceType) {
		this.ceType = ceType;
	}
	
	
}
