/*
 * Created on Jul 17, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package elms.app.Calendar;

/**
 * @author aman
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Calendar {
	
	 	private String startDate;
	 	private String endDate;
	    private String description;
	    private String delete;
	    private int vacationId;
	    private int hour;
	    private int userId;

		/**
		 * @return Returns the vacationId.
		 */
		public int getVacationId() {
			return vacationId;
		}
		/**
		 * @param vacationId The vacationId to set.
		 */
		public void setVacationId(int vacationId) {
			this.vacationId = vacationId;
		}
		/**
		 * @return Returns the description.
		 */
		public String getDescription() {
			return description;
		}
		/**
		 * @param description The description to set.
		 */
		public void setDescription(String description) {
			this.description = description;
		}
		/**
		 * @return Returns the startDate.
		 */
		public String getStartDate() {
			return startDate;
		}
		/**
		 * @param startDate The startDate to set.
		 */
		public void setStartDate(String startDate) {
			this.startDate = startDate;
		}
		/**
		 * @return Returns the endDate.
		 */
		public String getEndDate() {
			return endDate;
		}
		/**
		 * @param endDate The endDate to set.
		 */
		public void setEndDate(String endDate) {
			this.endDate = endDate;
		}
		/**
		 * @return Returns the delete.
		 */
		public String getDelete() {
			return delete;
		}
		/**
		 * @param delete The delete to set.
		 */
		public void setDelete(String delete) {
			this.delete = delete;
		}
		/**
		 * @return Returns the hour.
		 */
		public int getHour() {
			return hour;
		}
		/**
		 * @param hour The hour to set.
		 */
		public void setHour(int hour) {
			this.hour = hour;
		}
		/**
		 * @return Returns the userId.
		 */
		public int getUserId() {
			return userId;
		}
		/**
		 * @param userId The userId to set.
		 */
		public void setUserId(int userId) {
			this.userId = userId;
		}
}
