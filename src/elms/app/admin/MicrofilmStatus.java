package elms.app.admin;

public class MicrofilmStatus {

	private String code;
	private String description;

	/**
	 * Constructor for ActivityStatus
	 */
	public MicrofilmStatus() {

	}

	/**
	 * Constructor for ActivityStatus
	 */
	public MicrofilmStatus(String code, String description) {
		this.code = code;
		this.description = description;
	}

	/**
	 * Gets the code
	 * 
	 * @return Returns a String
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code
	 * 
	 * @param code
	 *            The code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}
