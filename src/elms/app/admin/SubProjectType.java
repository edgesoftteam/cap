package elms.app.admin;

public class SubProjectType {

	protected int subProjectTypeId;
	protected int lsoUseId;
	protected int ptypeId;
	protected String subProjectType;
	protected String departmentId;
	protected String departmentCode;
	protected String description;

	private String delete;
	private String deletable;

	public SubProjectType(int subProjectTypeId, int lsoUseId, String subProjectType, String departmentId, String description) {
		this.subProjectTypeId = subProjectTypeId;
		this.lsoUseId = lsoUseId;
		this.subProjectType = subProjectType;
		this.departmentId = departmentId;
		this.description = description;
		this.departmentCode = "";
	}

	public SubProjectType() {
		this.departmentCode = "";
	}

	public SubProjectType(int subProjectTypeId, String subProjectType, String description) {
		this.subProjectTypeId = subProjectTypeId;
		this.subProjectType = subProjectType;
		this.description = description;
	}

	/**
	 * Gets the subProjectTypeId
	 * 
	 * @return Returns a int
	 */
	public int getSubProjectTypeId() {
		return subProjectTypeId;
	}

	/**
	 * Sets the subProjectTypeId
	 * 
	 * @param subProjectTypeId
	 *            The subProjectTypeId to set
	 */
	public void setSubProjectTypeId(int subProjectTypeId) {
		this.subProjectTypeId = subProjectTypeId;
	}

	/**
	 * Gets the lsoUseId
	 * 
	 * @return Returns a int
	 */
	public int getLsoUseId() {
		return lsoUseId;
	}

	/**
	 * Sets the lsoUseId
	 * 
	 * @param lsoUseId
	 *            The lsoUseId to set
	 */
	public void setLsoUseId(int lsoUseId) {
		this.lsoUseId = lsoUseId;
	}

	/**
	 * Gets the subProjectType
	 * 
	 * @return Returns a String
	 */
	public String getSubProjectType() {
		return subProjectType;
	}

	/**
	 * Sets the subProjectType
	 * 
	 * @param subProjectType
	 *            The subProjectType to set
	 */
	public void setSubProjectType(String subProjectType) {
		this.subProjectType = subProjectType;
	}

	/**
	 * Gets the departmentCode
	 * 
	 * @return Returns a String
	 */
	public String getDepartmentId() {
		return departmentId;
	}

	/**
	 * Sets the departmentCode
	 * 
	 * @param departmentCode
	 *            The departmentCode to set
	 */
	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Returns the delatable.
	 * 
	 * @return String
	 */
	public String getDeletable() {
		return deletable;
	}

	/**
	 * Returns the delete.
	 * 
	 * @return String
	 */
	public String getDelete() {
		return delete;
	}

	/**
	 * Sets the delatable.
	 * 
	 * @param delatable
	 *            The delatable to set
	 */
	public void setDeletable(String deletable) {
		this.deletable = deletable;
	}

	/**
	 * Sets the delete.
	 * 
	 * @param delete
	 *            The delete to set
	 */
	public void setDelete(String delete) {
		this.delete = delete;
	}

	/**
	 * Returns the ptypeId.
	 * 
	 * @return int
	 */
	public int getPtypeId() {
		return ptypeId;
	}

	/**
	 * Sets the ptypeId.
	 * 
	 * @param ptypeId
	 *            The ptypeId to set
	 */
	public void setPtypeId(int ptypeId) {
		this.ptypeId = ptypeId;
	}

	/**
	 * @return
	 */
	public String getDepartmentCode() {
		return departmentCode;
	}

	/**
	 * @param string
	 */
	public void setDepartmentCode(String string) {
		departmentCode = string;
	}

}