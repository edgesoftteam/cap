package elms.app.admin;


import java.io.Serializable;

public class NoticeType implements Serializable {
	
	private int id;
	private String description;
	private String label;

	public NoticeType() {

	}

	

	public NoticeType(int id, String description) {
		this.id = id;
		this.description = description;
	}

	
	
	public NoticeType(int id, String description, String label) {
		super();
		this.id = id;
		this.description = description;
		this.label = label;
	}



	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	} 



	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}



	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}



	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}



	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}



	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	
	


}
