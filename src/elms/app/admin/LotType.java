package elms.app.admin;

public class LotType {

	private String typeId;
	private String description;

	public LotType() {

	}

	public LotType(String typeId, String description) {
		this.typeId = typeId;
		this.description = description;
	}

	/**
	 * Returns the description.
	 * 
	 * @return String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Returns the typeId.
	 * 
	 * @return String
	 */
	public String getTypeId() {
		return typeId;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Sets the typeId.
	 * 
	 * @param typeId
	 *            The typeId to set
	 */
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

}
