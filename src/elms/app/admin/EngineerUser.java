package elms.app.admin;

public class EngineerUser {

	private int userId;
	private String engineerId;

	public EngineerUser() {
		userId = 0;
		engineerId = "";
	}

	public EngineerUser(int userId, String engineerId) {
		this.userId = userId;
		this.engineerId = engineerId;
	}

	/**
	 * Gets the userId
	 * 
	 * @return Returns a int
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * Sets the userId
	 * 
	 * @param userId
	 *            The userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * Returns the engineerId.
	 * 
	 * @return String
	 */
	public String getEngineerId() {
		return engineerId;
	}

	/**
	 * Sets the engineerId.
	 * 
	 * @param engineerId
	 *            The engineerId to set
	 */
	public void setEngineerId(String engineerId) {
		this.engineerId = engineerId;
	}

}
