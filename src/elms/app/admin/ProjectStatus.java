package elms.app.admin;

public class ProjectStatus {

	private int statusId;

	private String description;

	/**
	 * Constructor for ActivityStatus
	 */
	public ProjectStatus() {
	}

	public ProjectStatus(int statusId, String description) {
		this.statusId = statusId;
		this.description = description;
	}

	/**
	 * Gets the status
	 * 
	 * @return Returns a int
	 */
	public int getStatusId() {
		return statusId;
	}

	/**
	 * Sets the status
	 * 
	 * @param status
	 *            The status to set
	 */
	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}
