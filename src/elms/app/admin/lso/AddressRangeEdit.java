package elms.app.admin.lso;

import java.io.Serializable;

import elms.app.lso.AddressRange;


/**
 * @author shekhar
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class AddressRangeEdit extends AddressRange implements Serializable {
    protected String delete;


    public AddressRangeEdit() {
        super();
        this.delete = "";
    }

    public AddressRangeEdit(String aStartRange, String aEndRange, String aStreetId) {
        super(aStartRange, aEndRange, aStreetId);
        this.delete = "";
    }

    /**
     * Returns the delete.
     * @return String
     */
    public String getDelete() {
        return delete;
    }

    /**
     * Sets the delete.
     * @param delete The delete to set
     */
    public void setDelete(String delete) {
        this.delete = delete;
    }

}
