package elms.app.admin.lso;

/**
 * @author abelaguly
 *
 * This class contains the information on the structure that will be moved to a different address.
 * This class was introduced due to a reported bug about the move LSO where it is not showing multiple structures for an address.
 * anand belaguly - March 11,2004
 */
public class MoveStructureDetail {

    public String moveId;
    public String moveAlias;
    public String moveDescription;
    public String moveAddressOne;
    public String moveAddressTwo;

    /**
     * @return
     */
    public String getMoveAddressOne() {
        return moveAddressOne;
    }

    /**
     * @return
     */
    public String getMoveAddressTwo() {
        return moveAddressTwo;
    }

    /**
     * @return
     */
    public String getMoveAlias() {
        return moveAlias;
    }

    /**
     * @return
     */
    public String getMoveDescription() {
        return moveDescription;
    }

    /**
     * @return
     */
    public String getMoveId() {
        return moveId;
    }

    /**
     * @param string
     */
    public void setMoveAddressOne(String string) {
        moveAddressOne = string;
    }

    /**
     * @param string
     */
    public void setMoveAddressTwo(String string) {
        moveAddressTwo = string;
    }

    /**
     * @param string
     */
    public void setMoveAlias(String string) {
        moveAlias = string;
    }

    /**
     * @param string
     */
    public void setMoveDescription(String string) {
        moveDescription = string;
    }

    /**
     * @param string
     */
    public void setMoveId(String string) {
        moveId = string;
    }

}
