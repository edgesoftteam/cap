package elms.app.admin;

public class ProjectName {

	protected int projectNameId;
	protected String name;
	protected String description;
	protected String departmentCode;

	public ProjectName() {
		// default
	}

	/**
	 * @return
	 */
	public String getDepartmentCode() {
		return departmentCode;
	}

	/**
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return
	 */
	public int getProjectNameId() {
		return projectNameId;
	}

	/**
	 * @param string
	 */
	public void setDepartmentCode(String string) {
		departmentCode = string;
	}

	/**
	 * @param string
	 */
	public void setDescription(String string) {
		description = string;
	}

	/**
	 * @param string
	 */
	public void setName(String string) {
		name = string;
	}

	/**
	 * @param i
	 */
	public void setProjectNameId(int i) {
		projectNameId = i;
	}

}