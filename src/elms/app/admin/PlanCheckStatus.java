package elms.app.admin;

public class PlanCheckStatus {

	private int code;
	private int actStatusCode;
	private String description;

	/**
	 * Constructor for PlanCheckStatus
	 */
	public PlanCheckStatus() {

	}

	/**
	 * Constructor for PlanCheckStatus
	 */
	public PlanCheckStatus(int code, String description, int activityStatusCode) {
		this.code = code;
		this.description = description;
		this.actStatusCode = activityStatusCode;
	}

	/**
	 * Gets the code
	 * 
	 * @return Returns a int
	 */
	public int getCode() {
		return code;
	}

	/**
	 * Sets the code
	 * 
	 * @param code
	 *            The code to set
	 */
	public void setCode(int code) {
		this.code = code;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the actStatusCode
	 * 
	 * @return Returns a int
	 */
	public int getActStatusCode() {
		return actStatusCode;
	}

	/**
	 * Sets the actStatusCode
	 * 
	 * @param actStatusCode
	 *            The actStatusCode to set
	 */
	public void setActStatusCode(int actStatusCode) {
		this.actStatusCode = actStatusCode;
	}

}
