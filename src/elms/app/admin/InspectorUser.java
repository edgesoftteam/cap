package elms.app.admin;

public class InspectorUser {

	private int userId;
	private String inspectorId;

	public InspectorUser() {
		userId = 0;
		inspectorId = "";
	}

	public InspectorUser(int userId, String inspectorId) {
		this.userId = userId;
		this.inspectorId = inspectorId;
	}

	/**
	 * Gets the userId
	 * 
	 * @return Returns a int
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * Sets the userId
	 * 
	 * @param userId
	 *            The userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * Gets the inspectorId
	 * 
	 * @return Returns a String
	 */
	public String getInspectorId() {
		return inspectorId;
	}

	/**
	 * Sets the inspectorId
	 * 
	 * @param inspectorId
	 *            The inspectorId to set
	 */
	public void setInspectorId(String inspectorId) {
		this.inspectorId = inspectorId;
	}

}
