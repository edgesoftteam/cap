package elms.app.admin;

public class FeeCategory {

	private int code;
	private int description;

	/**
	 * Constructor for FeeCategory
	 */
	public FeeCategory() {
	}

	/**
	 * Gets the code
	 * 
	 * @return Returns a int
	 */
	public int getCode() {
		return code;
	}

	/**
	 * Sets the code
	 * 
	 * @param code
	 *            The code to set
	 */
	public void setCode(int code) {
		this.code = code;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a int
	 */
	public int getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(int description) {
		this.description = description;
	}

}
