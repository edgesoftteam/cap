package elms.app.admin;

import java.io.Serializable;

public class TrafficSignalLocation implements Serializable {
	private int id;
	private String description;

	public TrafficSignalLocation() {

	}

	public TrafficSignalLocation(int id, String description) {
		this.id = id;
		this.description = description;
	}

	/**
	 * Returns the description.
	 * 
	 * @return String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Returns the id.
	 * 
	 * @return int
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            The id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

}
