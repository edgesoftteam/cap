package elms.app.admin;

import java.io.Serializable;

public class ParkingZone implements Serializable {
	private String name;
	private String pzoneId;
	protected String code;
	protected String desc;
	protected String comnt;

	public ParkingZone() {

	}

	public ParkingZone(String pzoneId, String name) {
		this.pzoneId = pzoneId;
		this.name = name;
	}

	/**
	 * Returns the name.
	 * 
	 * @return String
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            The name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the code.
	 * 
	 * @return String
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Returns the comnt.
	 * 
	 * @return String
	 */
	public String getComnt() {
		return comnt;
	}

	/**
	 * Returns the desc.
	 * 
	 * @return String
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * Returns the pzoneId.
	 * 
	 * @return String
	 */
	public String getPzoneId() {
		return pzoneId;
	}

	/**
	 * Sets the code.
	 * 
	 * @param code
	 *            The code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Sets the comnt.
	 * 
	 * @param comnt
	 *            The comnt to set
	 */
	public void setComnt(String comnt) {
		this.comnt = comnt;
	}

	/**
	 * Sets the desc.
	 * 
	 * @param desc
	 *            The desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}

	/**
	 * Sets the pzoneId.
	 * 
	 * @param pzoneId
	 *            The pzoneId to set
	 */
	public void setPzoneId(String pzoneId) {
		this.pzoneId = pzoneId;
	}

}
