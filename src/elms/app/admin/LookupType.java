package elms.app.admin;

public class LookupType {

	private String type;
	private String filter;
	private String description;

	public LookupType(String aType, String aDescription) {
		this.type = aType;
		this.description = aDescription;
		this.filter = "";
	}

	public LookupType(String aType, String aDescription, String aFilter) {
		this.type = aType;
		this.description = aDescription;
		this.filter = aFilter;
	}

	/**
	 * Gets the type
	 * 
	 * @return Returns a String
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type
	 * 
	 * @param type
	 *            The type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return
	 */
	public String getFilter() {
		return filter;
	}

	/**
	 * @param string
	 */
	public void setFilter(String string) {
		filter = string;
	}

}
