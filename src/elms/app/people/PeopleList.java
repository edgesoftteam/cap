//Source file: C:\\datafile\\source\\elms\\app\\people\\PeopleList.java

package elms.app.people;

/**
 * @author Shekhar Jain
 */
public class PeopleList {

	protected int peopleId;
	protected String name;
	protected String deposit;
	protected String peopleType;
	protected String balanceDeposit;

	public PeopleList(int peopleId, String name) {
		this.peopleId = peopleId;
		this.name = name;
	}

	public PeopleList(int peopleId, String name, String deposit) {
		this.peopleId = peopleId;
		this.name = name;
		this.deposit = deposit;
	}

	public PeopleList(int peopleId, String name, String deposit, String peopleType, String balanceDeposit) {
		this.peopleId = peopleId;
		this.name = name;
		this.deposit = deposit;
		this.peopleType = peopleType;
		this.balanceDeposit = balanceDeposit;
	}

	/**
	 * @roseuid 3CCDCBA7036F
	 */
	public PeopleList() {

	}

	/**
	 * Gets the peopleId
	 * 
	 * @return Returns a int
	 */
	public int getPeopleId() {
		return peopleId;
	}

	/**
	 * Sets the peopleId
	 * 
	 * @param peopleId
	 *            The peopleId to set
	 */
	public void setPeopleId(int peopleId) {
		this.peopleId = peopleId;
	}

	/**
	 * Gets the name
	 * 
	 * @return Returns a String
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name
	 * 
	 * @param name
	 *            The name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the deposit
	 * 
	 * @return Returns a String
	 */
	public String getDeposit() {
		return deposit;
	}

	/**
	 * Sets the deposit
	 * 
	 * @param deposit
	 *            The deposit to set
	 */
	public void setDeposit(String deposit) {
		this.deposit = deposit;
	}

	/**
	 * @return Returns the peopleType.
	 */
	public String getPeopleType() {
		return peopleType;
	}

	/**
	 * @param peopleType
	 *            The peopleType to set.
	 */
	public void setPeopleType(String peopleType) {
		this.peopleType = peopleType;
	}

	/**
	 * @return Returns the balanceDeposit.
	 */
	public String getBalanceDeposit() {
		return balanceDeposit;
	}

	/**
	 * @param balanceDeposit
	 *            The balanceDeposit to set.
	 */
	public void setBalanceDeposit(String balanceDeposit) {
		this.balanceDeposit = balanceDeposit;
	}
}
